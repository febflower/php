<?php
// +----------------------------------------------------------------------
// | 控制台配置s
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
        'app\command\LSC\En\microcenter',
        'app\command\LSC\En\VasemarketCom',
        'app\command\LSC\En\riderRunningCom',
        'app\command\LSC\En\crystalcom',
        'app\command\LSC\En\kitchenCom',
        'app\command\LSC\En\totaltool',
        'app\command\LSC\En\betweenCom',
        'app\command\LSC\En\funsport',
        'app\command\LSC\En\sportinglife',
        'app\command\LSC\En\christmas',
        'app\command\LSC\En\xdcdepot',
        'app\command\LSC\En\magicalears',
        'app\command\LSC\En\backcountrygear',
        'app\command\LSC\En\every',
        'app\command\LSC\En\torrid',
        'app\command\LSC\En\pinklily',
        
        'app\command\LSC\De\ModyfDe',
        'app\command\LSC\De\christDe',
        'app\command\LSC\De\markenDe',
        'app\command\LSC\De\AlternateDe',
        'app\command\LSC\De\gartenmoebelDe',
        'app\command\LSC\De\plutosDe',
        'app\command\LSC\De\galeriaDe',
        'app\command\LSC\De\tischweltDe',
        'app\command\LSC\De\cloppenDe',
        'app\command\LSC\De\greatrexDe',
        'app\command\LSC\De\coledampfs',

        'app\command\LSC\Gr\buddhaGr',
        'app\command\LSC\Gr\vendoraGr',
        'app\command\LSC\Gr\evripidisGr',

        'app\command\LSC\Fr\mobistoxxFr',
        'app\command\LSC\Fr\rueducomFr',

        'app\command\LSC\Nl\hockeydirect',

        //'app\command\LPW\De\IntimissimiCom','app\command\LPW\De\KochformDe',
        'app\command\LPW\De\CulinarisDe',
        // 'app\command\LPW\Dk\BauhausDk','app\command\LPW\Dk\DavidsenshopDk','app\command\LPW\Dk\HomeshopDk','app\command\LPW\Dk\SallingDk','app\command\LPW\Dk\MagasinDk',
        'app\command\LPW\Dk\JdsportsDk',
        'app\command\LPW\Dk\KidsWorldDk',
        //'app\command\LPW\En\AdexaCoUk','app\command\LPW\En\AlchemyfinehomeCom','app\command\LPW\En\CottononCom','app\command\LPW\En\CricutCom','app\command\LPW\En\KidzglobalapparelCom','app\command\LPW\En\MonstershopCoUk','app\command\LPW\En\RogansshoesCom','app\command\LPW\En\QuillCom','app\command\LPW\En\Baseball360',//'app\command\LPW\En\Baseballtown','app\command\LPW\En\HeruniverseEn','app\command\LPW\En\Justballgloves','app\command\LPW\En\ShopDisneyEn','app\command\LPW\En\Sourceforsports','app\command\LPW\En\SuperherostuffEn','app\command\LPW\En\Big5sportinggoods','app\command\LPW\En\Hitafterhitonline',
        'app\command\LPW\En\EntropyAU',
        'app\command\LPW\En\IbeautycityUS',
        'app\command\LPW\En\OnlinetoysAU',

        'app\command\LPW\En\TradeinnEn\Bikeinn',
        'app\command\LPW\En\TradeinnEn\Diveinn',
        'app\command\LPW\En\TradeinnEn\Dressinn',
        'app\command\LPW\En\TradeinnEn\Goalinn',
        'app\command\LPW\En\TradeinnEn\Kidinn',
        'app\command\LPW\En\TradeinnEn\Motardinn',
        'app\command\LPW\En\TradeinnEn\Runnerinn',
        'app\command\LPW\En\TradeinnEn\Smashinn',

        // 'app\command\LPW\Fr\KlemanFranceCom','app\command\LPW\Fr\TriganoFr',
        'app\command\LPW\Gr\BigcharlieGr',
        // 'app\command\LPW\Nl\Cavallaronapoli',
        'app\command\LPW\Nl\CampzNl',
        //'app\command\LPW\No\AntonsportNo','app\command\LPW\No\JollyroomNo','app\command\LPW\No\ObsNo','app\command\LPW\No\SorensensportNo',
        'app\command\LPW\No\MiintoNo',
        'app\command\LPW\No\SportsdealNo',
        
        

        
        'app\command\MSK\Fr\KellerSportsFR',
        'app\command\MSK\Fr\VertbaudetFr',
        'app\command\MSK\El\BioestiaGr',
        'app\command\MSK\El\EshopelectricGR',
        'app\command\MSK\En\VaticangiftCOM',
        'app\command\MSK\En\HomewetbarCOM',
        'app\command\MSK\Zh\SelfridgesCOM',
        'app\command\MSK\En\ChristmasCOM',
        'app\command\MSK\En\SunandskiCOM',
        'app\command\MSK\En\ColoradoskishopCOM',
        'app\command\MSK\En\PurestokesportsCOM',
        'app\command\MSK\En\AppRequipperCOM',
        'app\command\MSK\El\AnswearGR',
        'app\command\MSK\En\GuitarcenterCOM',
        'app\command\MSK\De\WohnenDE',
        'app\command\MSK\El\ModivoGr',
        'app\command\MSK\Fr\TopbizFr',
        'app\command\MSK\De\Chech24DE',
        'app\command\MSK\De\FranzenDe',
        'app\command\MSK\El\ZakcretGr',
        'app\command\MSK\En\HobbylobbyCOM',
        'app\command\MSK\En\ShophqCOM',
        'app\command\MSK\No\SportenbeitostolenNo',
        'app\command\MSK\En\AllmodernCOM',
        'app\command\MSK\El\CosmossportGr',

        'app\command\MZL\Gr\ToysGr',
        'app\command\MZL\Gr\GameGr',
        'app\command\MZL\Gr\RoleGr',
        'app\command\MZL\Gr\PlaymobilGr',
        'app\command\MZL\Gr\atticad',
        'app\command\MZL\Gr\sneaker',
        'app\command\MZL\Gr\politikos',
        'app\command\MZL\Gr\factory',
        'app\command\MZL\Gr\thefash',
        

        'app\command\MZL\En\SkiingEn',
        'app\command\MZL\En\gearwest',
        'app\command\MZL\En\fraserh',
        'app\command\MZL\En\skis',
        'app\command\MZL\En\sunandski',
        'app\command\MZL\En\winter',
        'app\command\MZL\En\build',
        'app\command\MZL\En\silkfred',
        'app\command\MZL\En\smyth',
        'app\command\MZL\En\freepeople',
        

        'app\command\MZL\Fr\intersport',
        'app\command\MZL\Fr\bessec',
        'app\command\MZL\Fr\labonne',
        'app\command\MZL\Fr\sport',

        'app\command\MZL\De\ambie',
        'app\command\MZL\De\kela',
        
       
    ],
];
