<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class mdyf extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('build:wordpress:lsc:mdyf')
            ->setDescription('the build:wordpress:lsc:mdyf command');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $output->writeln('build:wordpress:lsc:mdyf');
    }
}
