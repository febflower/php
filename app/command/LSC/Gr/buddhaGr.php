<?php
namespace app\command\LSC\Gr;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use DeepCopy\Filter\Filter;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Crap4j;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class buddhaGr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:buddhaGr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.funky-buddha.com/el/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.funky-buddha.com/en/gunaikeio/roucha/mpoufan-palto.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/plekta-zaketes.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/fouter-hoodies.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/foremata.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/foustes.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/pantelonia.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/jeans.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/t-shirts.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/mplouzes.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/bermoudes-sorts.html',
        ];
        $array1=[
            'https://www.funky-buddha.com/en/gunaikeio/roucha/mpoufan-palto.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/plekta-zaketes.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/fouter-hoodies.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/foremata.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/foustes.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/pantelonia.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/jeans.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/t-shirts.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/mplouzes.html',
            'https://www.funky-buddha.com/en/gunaikeio/roucha/bermoudes-sorts.html',
        ];
        $array2=[
           'https://www.funky-buddha.com/en/andriko/roucha/mpoufan-palto.html',
           'https://www.funky-buddha.com/el/andriko/roucha/plekta-zaketes.html',
           'https://www.funky-buddha.com/el/andriko/roucha/pantelonia.html',
           'https://www.funky-buddha.com/el/andriko/roucha/poukamisa.html',
           'https://www.funky-buddha.com/el/andriko/roucha/polo-mplouzes.html',
         ];
        $array3=[
        'https://www.funky-buddha.com/el/andriko/roucha/fouter-hoodies.html',
        'https://www.funky-buddha.com/el/andriko/roucha/jeans.html',
        'https://www.funky-buddha.com/el/andriko/roucha/t-shirts.html',
        'https://www.funky-buddha.com/el/andriko/roucha/bermoudes-sorts.html',
        ];
         
         $this->sites = [
             'x' => ['source' => $array, 'target' => 'febflow/'],
             'x1' => ['source' => $array1, 'target' => 'buddha001.xms005.site/'],
             'x2' => ['source' => $array2, 'target' => 'buddha002.xms005.site/'],
             'x3' => ['source' => $array3, 'target' => 'buddha003.xms005.site/'],
             
         ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.funky-buddha.com/el/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.vaticangift.com/jewellery-crucifixes/287-the-original-pope-francis-pastoral-crucifix-silver.html');
    }

    protected function processPage($url)
    {

        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
        
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        $crawler->filter('.product-item-link')->each(function (Crawler $node, $i) use($breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('[name="title"]')->attr('content');
        $product['price'] = str_replace(',','',str_replace('€','',str_replace('$','',$crawler->filter('.price')->text())));
        $product['type'] = 'simple';
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],3);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['brand']=$crawler->filter('#product-addtocart-button')->attr('data-brand');
        $product['sku'] = $crawler->filter('#product-addtocart-button')->attr('data-id');
        $product['short_description']=" ";
        $product['description'] = $crawler->filter('.tab-labels')->html();
        $des_imgs = $crawler->filter('.icon-wrapper')->each(function(Crawler $node,$i){
             return $node->html();
        });
        // print_r($des_imgs);exit;
        foreach($des_imgs as $des_img){
            $product['description'] = str_replace($des_img,"",$product['description']);
        }
        // print_r($product['description']);exit;
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]=[];
        // $product['tags'][0]=$product['color'];
        // $product['tags'][1]=$item['breadcrumbs'][3];
        // $product['tags'] = $this->createProductTag($product['tags']);
        //图片
        $img = [];
        $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$img){
            if(strstr($node->text(),'mage/gallery/gallery')){
                $img = json_decode($node->text(),true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
            }      
        });
        for($i=0;$i<count($img);$i++){
            $images[$i]=[
                'src'=>$img[$i]['img'],
                'name'=>$product['title'],
            ];
        };
        // print_r($images);exit;
        $product['images']=$images;
        if($crawler->filter('.product-options-wrapper')->count()>0){
            $size=[];
            $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$size){
                if(strstr($node->text(),'data-role=swatch-options')){
                    $size = json_decode($node->text(),true)['[data-role=swatch-options]']['Magento_Swatches/js/swatch-renderer']['jsonConfig']['attributes'][152]['options'];
                }
            });
            for($i=0;$i<count($size);$i++){
                $option[$i]=$size[$i]['label'];
            }
            $product['variations'][]= [
                'name'=>'Size',
                'options' =>$option]; 
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }
        $product['attributes']=[];
        // $attr=[];
        // if($crawler->filter("#tab-specs")->count()) {
        //     $count = $crawler->filter(".spec-body")->count();

        //     $crawler->filter('.spec-body')->each(function(Crawler $node,$i)use(&$attr){
        //         $attr[$i]=[
        //             'name' =>$node->filter('div')->eq(1)->text(),
        //             'options' =>[$node->filter('div')->eq(2)->text()],
        //         ];
        //     });
        //     $product['attributes']=$attr;
        // }else{
        //     $product['attributes'] = [];
        // }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
