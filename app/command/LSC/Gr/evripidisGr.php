<?php
namespace app\command\LSC\Gr;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class evripidisGr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:evripidisGr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.evripidis.gr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.evripidis.gr/categories/212573/pagoyria-/'
        ];
        $array8=[
            'https://www.evripidis.gr/categories/80/simeiomataria-/'
        ];
        $array9=[
            'https://www.evripidis.gr/categories/196111/etiketes-aytokollita-/',
            'https://www.evripidis.gr/categories/157608/logistika-entypa-/',
            'https://www.evripidis.gr/categories/170/tetradia-/',
        ];
        $array10=[
            'https://www.evripidis.gr/categories/223475/atzentes-/',
            'https://www.evripidis.gr/categories/116799/organizer-/',
        ];
        $array11=[
            'https://www.evripidis.gr/categories/196185/epitrapezia-/',
            'https://www.evripidis.gr/categories/196195/toixoy-/?page=1',
            'https://www.evripidis.gr/categories/178053/ebdomadiaia-/',
        ];
        $array12=[
            'https://www.evripidis.gr/categories/180431/imerisia-/',
            'https://www.evripidis.gr/categories/178221/miniaia-/',
            'https://www.evripidis.gr/categories/211134/logotexnika-imerologia-/',
            'https://www.evripidis.gr/categories/225343/antallaktika-imerologia-filofax-/',
        ];
        $array13=[
            'https://www.evripidis.gr/categories/111630/analosima-iy-/'
        ];
        $array14=[
            'https://www.evripidis.gr/categories/116806/paidiki-zografiki-/?page=1',
            'https://www.evripidis.gr/categories/116890/paidiki-xeirotexnia-/?page=1',
        ];
        $array15=[
            'https://www.evripidis.gr/categories/116804/zografiki-/',
        ];
        $array16=[
            'https://www.evripidis.gr/categories/111620/diy-xeirotexnia-/'
        ];
        $array17=[
            'https://www.evripidis.gr/categories/161109/puzzles-/?page=1'
        ];
        $array18=[
            'https://www.evripidis.gr/categories/114835/epitrapezia-/?page=1',
            'https://www.evripidis.gr/categories/161106/eponymo-paixnidi-/?page=1',
        ];
        $array19=[
            'https://www.evripidis.gr/categories/114843/ekpaideytika-paixnidia-/?page=1'
        ];
        $array20=[
            'https://www.evripidis.gr/categories/117682/genika-/?page=1'
        ];
        $array21=[
            'https://www.evripidis.gr/categories/117705/koykles-loytrina-marionetes-/?page=1'
        ];
        $array22=[
            'https://www.evripidis.gr/categories/117691/xeirotexnias-/?page=1',
            'https://www.evripidis.gr/categories/237540/oximata-/',
        ];
        $array23=[
            'https://www.evripidis.gr/categories/157292/educational-books-/'
        ];
        $array24=[
            'https://www.evripidis.gr/categories/201/businessfinance-/',
            'https://www.evripidis.gr/categories/96/classic-literature-/'
        ];
        $array25=[
            'https://www.evripidis.gr/categories/259/fooddrink-/'
        ];
        $array26=[
            'https://www.evripidis.gr/categories/161093/gift-books-/',
            'https://www.evripidis.gr/categories/153362/computing-and-technology-/',
            'https://www.evripidis.gr/categories/150755/coffee-t-books-/',
        ];
        $array27=[
            'https://www.evripidis.gr/categories/204/history-/'
        ];
        $array28=[
            'https://www.evripidis.gr/categories/166/health-familylife-style-/',
            'https://www.evripidis.gr/categories/263/science-/'
        ];
        $array29=[
            'https://www.evripidis.gr/categories/150750/hobbiessports-/'
        ];
        $array30=[
            'https://www.evripidis.gr/categories/229/travel-/'
        ];
        $array31=[
            'https://www.evripidis.gr/categories/159420/mind-bodyspirit-/',
            'https://www.evripidis.gr/categories/156808/religion-/',
            'https://www.evripidis.gr/categories/161808/personal-development-/',
            'https://www.evripidis.gr/categories/161807/media-studies-/',
            'https://www.evripidis.gr/categories/156803/law-/',
            'https://www.evripidis.gr/categories/157290/sociology-/',
        ];
        $array32=[
            // 'https://www.evripidis.gr/categories/157139/special-education-/',
            // 'https://www.evripidis.gr/categories/157142/politics-current-affairs-/',
            // 'https://www.evripidis.gr/categories/120791/philosophy-/',
            // 'https://www.evripidis.gr/categories/170969/linguistics-/',
            // 'https://www.evripidis.gr/categories/167630/astrology-/',
            'https://www.evripidis.gr/categories/120796/psychology-/',
        ];
        $array33=[
            'https://www.evripidis.gr/categories/223/comics-and-humor-/'
        ];
        $array34=[
            'https://www.evripidis.gr/categories/235/painting-sculpture-engraving-/?page=1',
            'https://www.evripidis.gr/categories/160067/coffee-t-books-art-/',
            'https://www.evripidis.gr/categories/160052/art-general-/',
            'https://www.evripidis.gr/categories/163883/graphic-arts-/'
        ];
        $array35=[
            'https://www.evripidis.gr/categories/117731/cinema-/',
            'https://www.evripidis.gr/categories/117805/theory-and-history-of-art-/',
            'https://www.evripidis.gr/categories/117803/photography-/?page=1',
            'https://www.evripidis.gr/categories/117748/dance-/',
            'https://www.evripidis.gr/categories/117794/art-monographs-/?page=1'
        ];  
        $array36=[
            // 'https://www.evripidis.gr/categories/275/world-poetry-/',
            // 'https://www.evripidis.gr/categories/241/greek-literature-in-english-/',
            // 'https://www.evripidis.gr/categories/231/historical-novels-/?page=1',
            // 'https://www.evripidis.gr/categories/132/travel-literature-/',
            'https://www.evripidis.gr/categories/169/fiction-/?page=1',
        ];
        $array37=[
            'https://www.evripidis.gr/categories/63/crime-thrillers-mystery-/?page=1',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x8' => ['source' => $array8, 'target' => 'evripidis008.xms006.site'],#..
            'x9' => ['source' => $array9, 'target' => 'evripidis009.xms006.site'],#..
            'x10' => ['source' => $array10, 'target' => 'evripidis010.xms006.site'],#..
            'x11' => ['source' => $array11, 'target' => 'evripidis011.xms006.site/'],#..
            'x12' => ['source' => $array12, 'target' => 'evripidis012.xms006.site'],#..
            'x13' => ['source' => $array13, 'target' => 'evripidis013.xms006.site'],#---
            'x14' => ['source' => $array14, 'target' => 'evripidis014.xms006.site'],#..
            'x15' => ['source' => $array15, 'target' => 'evripidis015.xms006.site'],#..
            'x16' => ['source' => $array16, 'target' => 'evripidis016.xms006.site'],#..
            'x17' => ['source' => $array17, 'target' => 'evripidis017.xms006.site/'],#..
            'x18' => ['source' => $array18, 'target' => 'evripidis018.xms006.site'],#..
            'x19' => ['source' => $array19, 'target' => 'evripidis019.xms006.site'],#---
            'x20' => ['source' => $array20, 'target' => 'evripidis020.xms006.site'],#..
            'x21' => ['source' => $array21, 'target' => 'evripidis021.xms006.site'],#..
            'x22' => ['source' => $array22, 'target' => 'evripidis022.xms006.site/'],#..
            'x23' => ['source' => $array23, 'target' => 'evripidis023.xms006.site'],#..
            'x24' => ['source' => $array24, 'target' => 'evripidis024.xms006.site'],#---
            'x25' => ['source' => $array25, 'target' => 'evripidis025.xms006.site'],#..
            'x26' => ['source' => $array26, 'target' => 'evripidis026.xms006.site/'],#..
            'x27' => ['source' => $array27, 'target' => 'evripidis027.xms006.site'],#..
            'x28' => ['source' => $array28, 'target' => 'evripidis028.xms006.site'],#---
            'x29' => ['source' => $array29, 'target' => 'evripidis029.xms006.site'],#..
            'x30' => ['source' => $array30, 'target' => 'evripidis030.xms006.site'],#---
            'x31' => ['source' => $array31, 'target' => 'evripidis031.xms006.site/'],#..
            'x32' => ['source' => $array32, 'target' => 'evripidis032.xms006.site'],#..
            'x33' => ['source' => $array33, 'target' => 'evripidis033.xms006.site'],#---
            'x34' => ['source' => $array34, 'target' => 'evripidis034.xms006.site'],#..
            'x35' => ['source' => $array35, 'target' => 'evripidis035.xms006.site'],#---
            'x36' => ['source' => $array36, 'target' => 'evripidis036.xms006.site/'],#..
            'x37' => ['source' => $array37, 'target' => 'evripidis037.xms006.site'],#..
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.evripidis.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.next');
        if ($nextNode->count()) {
            $this->processPage('https://www.evripidis.gr'.$nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        // $breadcrumbs = array_filter($crawler->filter('.path>p a')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.books')->filter('h3 a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('https://www.evripidis.gr'.$node->attr('href')),
                    #'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $arr = "";
        $crawler->filter('script')->each(function(Crawler $node)use(&$arr){
            if(strstr($node->text(),"ViewContent")){
                $arr=$node->text();
            }
        });
        $price = explode(",",explode("value:",$arr)[1]);
        // print_r($data);exit;
        $product['title'] =$crawler->filter('.data h1')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$price[0])));
        if($crawler->filter('.company strong')->count()){
            $product['brand']=$crawler->filter('.company strong')->text();
        }else{
            $product['brand']='Hardloop';
        }
        $product['type'] = 'simple';
        
        $breadcrumbs = array_filter($crawler->filter('.path>p a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $str = $crawler->filter('.company')->text();
        $pa='/\d+/';
        preg_match_all($pa,$str,$sku);
        // print_r($sku);exit;
        $product['sku'] = $sku[0][0]; 
        $product['short_description']= "";
        if($crawler->filter('.bookdescription')->count()){
            $product['description'] = $crawler->filter('.bookdescription')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = $crawler->filter('.bigphoto a')->each(function(Crawler $node,$i)use(&$img){
            return $node->attr('href');  
        });
        foreach($img as $image){
            $images[]=[
                'src'=>'https://www.evripidis.gr'.$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        if($crawler->filter(".bookinfo li")->count()) {
            $count = $crawler->filter(".bookinfo li")->count();
            for($i=0;$i<$count;$i++){
                if($crawler->filter('.bookinfo li')->eq($i)->filter('span')->text()!=null){
                    $product['attributes'][] = [
                        'name' => $crawler->filter(".bookinfo li em")->eq($i)->text(),
                        'options' => [$crawler->filter(".bookinfo li span")->eq($i)->text()],
                    ];
                }  
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        // $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s ', $gender, $category, $name, $product['color'])));
    }

}
