<?php
namespace app\command\LSC\Gr;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class vendoraGr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:vendoraGr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://vendora.gr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://vendora.gr/browse/7vw73l/camping.html',
            'https://vendora.gr/browse/9r21qv/anichneftes-metallon.html',
            ''
        ];
        $array1=[
            'https://vendora.gr/browse/7vw73l/camping.html',
            'https://vendora.gr/browse/8v82qv/thalassia-spor.html',
            'https://vendora.gr/browse/v7y6xv/maxairia-opla-polemikwn-texnwn.html',
        ];
        $array2=[
            'https://vendora.gr/browse/4v355l/tennis-raketes.html',
            'https://vendora.gr/browse/ov7d8v/chimerina-spor.html',
            'https://vendora.gr/browse/xv42wr/athlimata-anapsichi-taxidia-alla.html',
        ];
        $array3=[
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers/niedrig',
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers/hoch',
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers/leinen',
        ];
        $array4=[
            'https://www.plutosport.de/herren/freizeit/kleidung/unterwasche',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'vendora001.xms006.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'vendora002.xms006.site/'],#..
            'x3' => ['source' => $array3, 'target' => 'vendora003.xms006.site/'],#---
            'x4' => ['source' => $array4, 'target' => 'vendora004.xms006.site/'],#..
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://vendora.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.promotionlinks a')->count()){
                $crawler->filter('.promotionlinks a')->each(function(Crawler $node1,$i){
                    $this->processPage($node1->attr('href'));
                });
            }else{
                $this->processPage($source);
            }
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.grid-ads-col')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.title')->text(),
                    'url' => sprintf($node->filter('.card-product')->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('[property="og:title"]')->attr('content');
        $product['price'] = str_replace(',','.',str_replace(' €','',str_replace('$','',$crawler->filter('[property="product:price:amount"]')->attr('content'))));
        if($crawler->filter('.notranslate')->count()){
            $product['brand']=$crawler->filter('.notranslate')->text();
        }else{
            $product['brand']='Hardloop';
        }
        $product['type'] = 'simple';
            
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $data = json_decode($crawler->filter("[type='application/ld+json']")->text(),true);
        // print_r($data);exit;  
        $product['sku'] = str_replace("..","",$data['sku']);
        $product['short_description']= "";
        if($crawler->filter('.item-details p')->count()){
            $product['description'] = $crawler->filter('.item-details p')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = $crawler->filter('.col-thumbnails img')->each(function(Crawler $node,$i)use(&$img){
            return $node->attr('data-remote');  
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        // print_r($images);exit;
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        if($crawler->filter(".characteristics-show")->count()) {
            $count = $crawler->filter(".characteristics-show dt")->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".characteristics-show dt")->eq($i)->text(),
                    'options' => [$crawler->filter(".characteristics-show dd")->eq($i)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        // $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s ', $gender, $category, $name, $product['color'])));
    }

}
