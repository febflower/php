<?php
namespace app\command\LSC\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class pro extends Command{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    function processProductList(Crawler $crawler)
    {
        $breadcrumbs = $crawler->filter('.ui-menu-item')->each(function (Crawler $node) {
            return $node->text();
        });

        $crawler->filter('.product-item-info')->each(function (Crawler $node, $i) use ($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;

                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', 'https://vasemarket.com/'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }
    function crawlerProduct($item)
    {

        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product = [];
        try{
            $product['title'] = $crawler->filter('.base')->text();
            $product['info'] = $crawler->filter('.fetured_points')->text();
            $product['price'] = $crawler->filter('.price')->text();
            $product['details'] =$crawler->filterXPath('//*[@id="product.info.description"]')->text();
            // $product['wholesale_discount'] = $crawler->filter('.nested>div>label')
            // ->reduce(function(Crawler $node, $i){
            //     $product['wholesale_discount'][$i] = $node->text();   
            // });
            $match = array();
            $file_contents = file_get_contents($item['url']);
            $pa="/\[product-gallery-role=gallery-placeholder\]\"\: \{(?P<json>.*)<\/script>/isU";
                // "[product-gallery-role=gallery-placeholder]": {

            preg_match_all($pa, $file_contents, $match);
                // echo $match;
            $html = "{".rtrim(trim($match['json'][0]),"}");
                // echo $html;
            $x = json_decode($html,true);
            $images = "";
            for($i=0;$i<count($x['mage/gallery/gallery']['data']);$i++){
                    $images=$images.$x['mage/gallery/gallery']['data'][$i]['img'].";";
                }
            $product['images'] = $images;
                // echo $images;
            $crawler = $crawler
            ->filter('.breadcrumbs>ul>li')->children()
            ->reduce(function (Crawler $node, $i) {
                echo $node->text().">";
            });
        }catch (\Exception $e) {

        }
        print_r($product);
            $this->createProduct($product);
            echo "\r\n";
            echo "1";
    }
}