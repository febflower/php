<?php
namespace app\command\LSC\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class crystalcom extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:crystalcom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.crystalclassics.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
                'https://www.crystalclassics.com/'
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'crystalcom001.seo067.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.crystalclassics.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
            // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.365rider.com/zapatillas-trail/12678-zapatillas-asics-gel-trabuco-terra-azul-verde-aw22.html');
    }

    protected function processPage($url)
    {
        // echo $url;exit;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $crawler->filter('.menu>li')->eq(2)->filter('.sub-menu a')->each(function(Crawler $node,$i){
            $url = 'https://www.crystalclassics.com'.$node->attr('href');
            $breadcrumbs=array($node->text());
            // print_r($breadcrumbs);exit;
            $response = $this->guzzleHttpClient->request('GET', $url);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            $this->processProductList($crawler,$breadcrumbs);
        });       
    }

    protected function processProductList(Crawler $crawler,$breadcrumbs)
    {
        $crawler->filter('.txtNormal_cart_title a')->each(function (Crawler $node, $i)use ($breadcrumbs){
            try {
                $this->discount = rand(70, 90) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,          
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            sleep(1);
            flush();
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('[name="title"]')->attr('content');
        $product['price'] = str_replace(',','.',str_replace(' €','',str_replace('$','',$crawler->filter('[name="price"]')->attr('content'))));
        $product['brand']=$crawler->filter('[itemprop="brand"]')->text();
        $product['type'] = 'simple';
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] = $crawler->filter('[name="sku"]')->attr('content');
        $product['short_description']= $crawler->filter('[name="info"]')->attr('content');
        $product['description'] = $crawler->filter('.footer_brandtext')->html();
        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][] =[];
        //图片
        $images=$crawler->filter('.additional_images a')->each(function(Crawler $node,$i){
            return $node->attr('href');
        });
        foreach ($images as $image) {
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
        }
        print_r($product['images']);exit;
        $product['variations']=[];
        // if($crawler->filter('.attribute_radio')->count()>0){$product['variations'][]= [
        //         'name'=>'sizes',
        //         'options' =>$crawler->filter('.attribute_radio')->each(function (Crawler $node, $i) {
        //             return $node->attr('data-attname');})];  
        // }else{$product['variations'][]=[
        //         'name'=>'',
        //         'options' =>""];
        // }
        // if ($product['variations'][0]['options']){
        //     $product['type'] = 'variable';
        // }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        // echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}