<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class kitchenCom extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:kitchenCom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://food52.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.everythingkitchens.com/coffeemakers.html',
            'https://www.everythingkitchens.com/coffee-accessories.html',
            'https://www.everythingkitchens.com/coffee-grinder.html',
            'https://www.everythingkitchens.com/espresso-machine.html',
            'https://www.everythingkitchens.com/milk-frothers.html',
            'https://www.everythingkitchens.com/tea-pot-tea-kettle-index.html',
        ];
        $array1=[
            // 'https://www.everythingkitchens.com/cups-mugs.html',
            // 'https://www.everythingkitchens.com/drinking-glasses.html',
            'https://www.everythingkitchens.com/whiskey-cocktails-spirits-glassware.html?p=4',
            'https://www.everythingkitchens.com/wine-glasses-and-stemware.html',
            'https://www.everythingkitchens.com/jars-bottles.html',
            'https://www.everythingkitchens.com/water-bottles-travel-mugs.html',
            'https://www.everythingkitchens.com/coasters.html',
        ];
        $array2=[
            // 'https://www.everythingkitchens.com/butter-dishes-crocks.html',
            // 'https://www.everythingkitchens.com/serving-entertaining-trays.html',
            // 'https://www.everythingkitchens.com/serveware-pitchers.html',
            'https://www.everythingkitchens.com/serving-bowls-plates.html?p=3',
            'https://www.everythingkitchens.com/serveware-teapots.html',
            'https://www.everythingkitchens.com/gravy-and-sauce-boats.html',
            'https://www.everythingkitchens.com/specialty-serveware.html',
            'https://www.everythingkitchens.com/trivets.html',
            'https://www.everythingkitchens.com/cake-stands.html',
        ];
        $array3=[
            'https://www.everythingkitchens.com/tablecloths-runners.html',
            'https://www.everythingkitchens.com/placemats.html',
            'https://www.everythingkitchens.com/napkins.html',
            'https://www.everythingkitchens.com/kitchen-towels.html',
            'https://www.everythingkitchens.com/oven-mitts-pads.html',
            'https://www.everythingkitchens.com/kitchen-aprons.html',
        ];
        $array4=[
            'https://www.everythingkitchens.com/meat-grinder.html',
            'https://www.everythingkitchens.com/foodslicers.html',
            'https://www.everythingkitchens.com/indoor-meat-smokers-outdoor-meat-smokers.html',
            'https://www.everythingkitchens.com/sausage-stuffers.html',
            'https://www.everythingkitchens.com/meat-processing-supplies.html',
            'https://www.everythingkitchens.com/foodprocessors.html',
            'https://www.everythingkitchens.com/foodstrainers.html',
            'https://www.everythingkitchens.com/fooddehydrators.html',
            'https://www.everythingkitchens.com/grainmills.html',
            'https://www.everythingkitchens.com/magicvac.html',
            'https://www.everythingkitchens.com/food-processor-parts-attachments-and-accessories.html',
        ];
        $array5=[
            'https://www.everythingkitchens.com/coffeemakers.html',
            'https://www.everythingkitchens.com/coffee-accessories.html',
            'https://www.everythingkitchens.com/coffee-grinder.html',
            'https://www.everythingkitchens.com/espresso-machine.html',
            'https://www.everythingkitchens.com/milk-frothers.html',
            'https://www.everythingkitchens.com/tea-pot-tea-kettle-index.html',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'kitchen002.xms005.site/'],#..
            'x1' => ['source' => $array1, 'target' => 'kitchen007.xms005.site/'],#..3
            'x2' => ['source' => $array2, 'target' => 'kitchen008.xms005.site/'],#..2
            'x3' => ['source' => $array3, 'target' => 'kitchen010.xms005.site/'],#..
            'x4' => ['source' => $array4, 'target' => 'kitchen004.xms005.site/'],
            'x5' => ['source' => $array5, 'target' => 'www.milkfrothersshop.com/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://food52.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'https://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[title="Next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-item-name a')->each(function (Crawler $node,$i) use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.page-title')->text();
        $product['price'] = str_replace(',','',str_replace(' €','',str_replace('$','',$crawler->filter('.price')->text())));
        $product['brand']=$crawler->filter('[data-th="Brands"]')->text();
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],3);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] = str_replace("..","",$crawler->filter('[itemprop="sku"]')->text());
        $product['short_description']= "";
        $product['description'] = $crawler->filter('[itemprop="description"]')->html();
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('[data-th="Color"]')->count()){
            $product['color'] = $crawler->filter('[data-th="Color"]')->text();
        }else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        // print_r($product);exit;
        //图片   
        $arr = $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i){
            if(strstr($node->text(),'mage/gallery/gallery')){
                return $node->text();
            }      
        });
        foreach($arr as $arr1){
            if(strstr($arr1,'mage/gallery/gallery')){
                $data = $arr1;
            } 
        }
        $x=json_decode($data,true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
        for($i=0;$i<count($x);$i++){
            $images[$i]=[
                'src'=>$x[$i]['full'],
                'name'=>$product['title'],
            ];
        };
        // print_r($images);exit;
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        if($crawler->filter('.super-attribute-select')->count()>0){
            $color=[];
            $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$color){
                if(strstr($node->text(),'spConfig')){
                    $color = json_decode($node->text(),true)['#product_addtocart_form']['configurable']['spConfig']['attributes'][92]['options'];
                }
            });
            for($i=0;$i<count($color);$i++){
                $option[$i]=$color[$i]['label'];
            }
            $product['variations'][]= [
                'name'=>'Color',
                'options' =>$option]; 
            $product['type'] = 'variable';
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }
        if($crawler->filter(".additional-attributes-wrapper")->count()) {
            $count = $crawler->filter(".additional-attributes-wrapper")->filter('tbody>tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".additional-attributes-wrapper")->filter('tbody>tr>th')->eq($i)->text(),
                    'options' => [$crawler->filter(".additional-attributes-wrapper")->filter('tbody>tr>td')->eq($i)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
