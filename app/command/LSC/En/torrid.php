<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class torrid extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:torrid')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.torrid.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.torrid.com/clothing/tops/t-shirts/?',
        ];
        $array1=[
            'https://www.torrid.com/clothing/tops/t-shirts/?',
            'https://www.torrid.com/clothing/tops/tanks-camis/?',
        ];
        $array2=[
            'https://www.torrid.com/clothing/tops/tunics/?',
            'https://www.torrid.com/clothing/tops/sweatshirts-hoodies/?',
            'https://www.torrid.com/clothing/tops/sweaters/?',
        ];
        $array3=[
            'https://www.torrid.com/clothing/tops/shirts-blouses/?',
            'https://www.torrid.com/clothing/tops/kimonos/?',
            'https://www.torrid.com/clothing/tops/bodysuits/?',
        ];
        $array4=[
            'https://www.torrid.com/clothing/tops/graphic-t-shirts/verbiage/?',
            'https://www.torrid.com/clothing/tops/graphic-t-shirts/pop-culture/',
            'https://www.torrid.com/clothing/tops/graphic-t-shirts/art-design/',
            'https://www.torrid.com/clothing/tops/graphic-t-shirts/music/',
            'https://www.torrid.com/clothing/tops/graphic-t-shirts/sports/',
            'https://www.torrid.com/clothing/tops/graphic-t-shirts/disney/',
        ];
        $array5=[
            'https://www.torrid.com/clothing/dresses/wrap/?',
'https://www.torrid.com/clothing/dresses/wedding/?',
'https://www.torrid.com/clothing/dresses/skater/?',
'https://www.torrid.com/clothing/dresses/shop-by-occasion/?',
'https://www.torrid.com/clothing/dresses/shirt/?',
'https://www.torrid.com/clothing/dresses/printed-floral/?',
'https://www.torrid.com/clothing/dresses/mini/?',
'https://www.torrid.com/clothing/dresses/midi/?',
'https://www.torrid.com/clothing/dresses/maxi/?',
'https://www.torrid.com/clothing/dresses/little-black-dresses/?',
'https://www.torrid.com/clothing/dresses/cozy/?',
'https://www.torrid.com/clothing/dresses/a-line/?',
'https://www.torrid.com/clothing/jeans/straight/?',
'https://www.torrid.com/clothing/jeans/skinny/?',
'https://www.torrid.com/clothing/jeans/flare/?',
'https://www.torrid.com/clothing/jeans/bootcut-flare/?',
'https://www.torrid.com/clothing/jeans/bootcut/?',
'https://www.torrid.com/clothing/tops/sweaters/shrugs/?',
'https://www.torrid.com/clothing/tops/sweaters/pullovers/?',
'https://www.torrid.com/clothing/tops/sweaters/cardigans/?',
        ];
        $array6=[
            'https://www.torrid.com/clothing/bottoms/work-ready-pants/?',
'https://www.torrid.com/clothing/bottoms/skirts/?',
'https://www.torrid.com/clothing/bottoms/pants/?',
'https://www.torrid.com/clothing/bottoms/pant-fit-guide/?',
'https://www.torrid.com/clothing/bottoms/lounge-bottoms/?',
'https://www.torrid.com/clothing/bottoms/leggings/?',
'https://www.torrid.com/clothing/bottoms/joggers/?',
'https://www.torrid.com/clothing/tops/sweaters/shrugs/?',
'https://www.torrid.com/clothing/tops/sweaters/pullovers/?',
'https://www.torrid.com/clothing/tops/sweaters/cardigans/?',
'https://www.torrid.com/clothing/jeans/straight/?',
'https://www.torrid.com/clothing/jeans/skinny/?',
'https://www.torrid.com/clothing/jeans/flare/?',
'https://www.torrid.com/clothing/jeans/bootcut-flare/?',
'https://www.torrid.com/clothing/jeans/bootcut/?',
        ];
        $array7=[
            'https://www.torrid.com/clothing/dresses/wedding/?',
'https://www.torrid.com/clothing/dresses/skater/?',
'https://www.torrid.com/clothing/dresses/shop-by-occasion/?',
'https://www.torrid.com/clothing/dresses/shirt/?',
'https://www.torrid.com/clothing/dresses/printed-floral/?',
'https://www.torrid.com/clothing/dresses/mini/?',
'https://www.torrid.com/clothing/dresses/midi/?',
'https://www.torrid.com/clothing/dresses/maxi/?',
'https://www.torrid.com/clothing/dresses/little-black-dresses/?',
'https://www.torrid.com/clothing/dresses/cozy/?',
'https://www.torrid.com/clothing/dresses/a-line/?',
            'https://www.torrid.com/active-swim/swim/swim-tops/?',
'https://www.torrid.com/active-swim/swim/swim-dresses/?',
'https://www.torrid.com/active-swim/swim/swim-bottoms/?',
'https://www.torrid.com/active-swim/swim/one-pieces/?',
'https://www.torrid.com/active-swim/swim/cover-ups/?',
'https://www.torrid.com/active-swim/swim/active-swim/?',
'https://www.torrid.com/active-swim/active/sports-bras/?',
'https://www.torrid.com/active-swim/active/jackets-hoodies/?',
'https://www.torrid.com/active-swim/active/active-tops/?',
'https://www.torrid.com/active-swim/active/active-bottoms/?',
'https://www.torrid.com/clothing/jackets/moto/?',
'https://www.torrid.com/clothing/jackets/kimonos-wraps/?',
'https://www.torrid.com/clothing/jackets/jean-jackets-vests/?',
'https://www.torrid.com/clothing/jackets/bomber/?',
'https://www.torrid.com/clothing/jackets/blazers/?',
'https://www.torrid.com/clothing/jackets/anorak/?',
        ];
        $array8=[
            'https://www.torrid.com/torrid-curve-intimates/bras/t-shirt/?',
'https://www.torrid.com/torrid-curve-intimates/bras/strapless/?',
'https://www.torrid.com/torrid-curve-intimates/bras/sports-bras/?',
'https://www.torrid.com/torrid-curve-intimates/bras/plunge/?',
'https://www.torrid.com/torrid-curve-intimates/bras/new-push-up-t-shirt/?',
'https://www.torrid.com/torrid-curve-intimates/bras/everyday-wire-free-bras/?',
'https://www.torrid.com/torrid-curve-intimates/bras/bralettes-bandeaus/?',
'https://www.torrid.com/torrid-curve-intimates/bras/balconette/?',
'https://www.torrid.com/torrid-curve-intimates/bras/360-back-smoothing/?',
'https://www.torrid.com/active-swim/swim/swim-tops/?',
'https://www.torrid.com/active-swim/swim/swim-dresses/?',
'https://www.torrid.com/active-swim/swim/swim-bottoms/?',
'https://www.torrid.com/active-swim/swim/one-pieces/?',
'https://www.torrid.com/active-swim/swim/cover-ups/?',
'https://www.torrid.com/active-swim/swim/active-swim/?',
'https://www.torrid.com/active-swim/active/sports-bras/?',
'https://www.torrid.com/active-swim/active/jackets-hoodies/?',
'https://www.torrid.com/active-swim/active/active-tops/?',
'https://www.torrid.com/active-swim/active/active-bottoms/?',
'https://www.torrid.com/clothing/jackets/moto/?',
'https://www.torrid.com/clothing/jackets/kimonos-wraps/?',
'https://www.torrid.com/clothing/jackets/jean-jackets-vests/?',
'https://www.torrid.com/clothing/jackets/bomber/?',
'https://www.torrid.com/clothing/jackets/blazers/?',
'https://www.torrid.com/clothing/jackets/anorak/?',
        ];
        $array9=[
            'https://www.torrid.com/shoes-accessories/shoes/wedges/?',
'https://www.torrid.com/shoes-accessories/shoes/sneakers/?',
'https://www.torrid.com/shoes-accessories/shoes/slippers/?',
'https://www.torrid.com/shoes-accessories/shoes/sandals/?',
'https://www.torrid.com/shoes-accessories/shoes/heels/?',
'https://www.torrid.com/shoes-accessories/shoes/flats/?',
'https://www.torrid.com/shoes-accessories/shoes/boots-booties/?',
'https://www.torrid.com/shoes-accessories/jewelry/rings/?',
'https://www.torrid.com/shoes-accessories/jewelry/necklaces/?',
'https://www.torrid.com/shoes-accessories/jewelry/earrings/?',
'https://www.torrid.com/shoes-accessories/jewelry/bracelets/?',
'https://www.torrid.com/shoes-accessories/accessories/sunglasses-eyewear/?',
'https://www.torrid.com/shoes-accessories/accessories/reusable-masks/?',
'https://www.torrid.com/shoes-accessories/accessories/mugs-candles-gifts/?',
'https://www.torrid.com/shoes-accessories/accessories/hosiery-socks/?',
'https://www.torrid.com/shoes-accessories/accessories/hats/?',
'https://www.torrid.com/shoes-accessories/accessories/hair-accessories/?',
'https://www.torrid.com/shoes-accessories/accessories/gloves-scarves-wraps/?',
'https://www.torrid.com/shoes-accessories/accessories/belts/?',
        ];

        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'torrid001.xms012.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'torrid002.xms012.site/'],
            'x3' => ['source' => $array3, 'target' => 'torrid003.xms012.site/'],
            'x4' => ['source' => $array4, 'target' => 'torrid004.xms012.site/'],
            'x5' => ['source' => $array5, 'target' => 'torrid005.xms013.site/'],
            'x6' => ['source' => $array6, 'target' => 'torrid006.xms013.site/'],
            'x7' => ['source' => $array7, 'target' => 'torrid007.xms013.site/'],
            'x8' => ['source' => $array8, 'target' => 'torrid008.xms013.site/'],
            'x9' => ['source' => $array9, 'target' => 'torrid009.xms013.site/'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.torrid.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
            // $response = $this->guzzleHttpClient->request('GET', $source);
            // $contents = $response->getBody()->getContents();
            // $crawler = new Crawler($contents);
            // $crawler->filter('#facetedSearch-content--category a')->each(function(Crawler $node){
            //     $this->processPage($node->attr('href'));
            // });   
        }
    }

    protected function processPage($url)
    {
        // echo "'".$url."',".PHP_EOL;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.page-next');
        if($nextNode->count()){
            if($nextNode->attr('href')!=null){
                $this->processPage($nextNode->attr('href'));
            }
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb-wrap a')->each(function (Crawler $node) {
            return $node->text();
        }));
        array_push($breadcrumbs,$crawler->filter('.last_breadcrumb')->text());
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.name-link')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => $node->attr('href'),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.torrid.com/product/bejeweled-tree-pendant-necklace.-/19141374.html?cgid=ShoesAccessories_Jewelry_Necklaces';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('[itemprop="name"]')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('[title="Sale Price"]')->text()))); 
        if($crawler->filter('.stylitics-add-to-cart-disabled')->count()){
            $product['brand']=$crawler->filter('.stylitics-add-to-cart-disabled')->attr('data-brand');
        }else{
            $product['brand']=$crawler->filter('#add-to-cart')->attr('data-brand');
        }
        $product['type'] = 'simple';
        // print_r($product);exit;
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        
        $product['sku'] = $crawler->filter('.product-variations ')->attr('data-itemsku')."-".$crawler->filter('.product-variations ')->attr('data-colorcode');
        $product['short_description']="";
        if($crawler->filter('.product-detail-tabs ')->count()){
            $product['description'] =$crawler->filter('[itemprop="description"]')->html();
        }else{
            $product['description'] = "";
        } 
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        // $product['tags']="";
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=[];
        if($crawler->filter('.productthumbnail')->count()){
            $crawler->filter('.productthumbnail')->each(function(Crawler $node)use(&$img){
                $img[]=json_decode($node->attr('data-lgimg'),true)['url'];
            });
        }else{
            $img[]=$crawler->filter('[itemprop="image"]')->attr('src');
        }
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        // $product['images']=$images;
        $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.size-variation')->count()){
            $product['variations'][]=[
                'name'=>"Size",
                'options' =>$crawler->filter('.size-variation .emptyswatch')->each(function(Crawler $node){
                    return $node->text();
                }),
            ];
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
                'name'=>"",
                'options' =>""];
        }
        // print_r($product['variations']);exit;
        // if($crawler->filter('#product-attribute-specs-table')->count()){
        //     $count=$crawler->filter('#product-attribute-specs-table tr')->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name'=>$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('th')->text(),
        //             'options'=>[$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('td')->text()]
        //         ];
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }
        $product['attributes'] = [];
      
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
