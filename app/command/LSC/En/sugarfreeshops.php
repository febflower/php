<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class sugarfreeshops extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:sugarfreeshops')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.sportinglife.ca/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.sugarfreeshops.com/el/catalog/mplouzes?_=1670311031609'
        ];
        $array1=[
            'https://www.hockeydirect.nl/hockeysticks',
        ];
        
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'hockey001.xms012.site/'],#..
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sportinglife.ca/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        // 'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        // 'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
            // $response = $this->guzzleHttpClient->request('GET', $source);
            // $contents = $response->getBody()->getContents();
            // $crawler = new Crawler($contents);
            // $crawler->filter('#facetedSearch-content--category a')->each(function(Crawler $node){
            //     $this->processPage($node->attr('href'));
            // });   
        }
    }

    protected function processPage($url)
    {
        // echo "'".$url."',".PHP_EOL;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage('https://www.sugarfreeshops.com'.$nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.slides-container a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->attr('title'),
                    'url' => sprintf('https://www.sugarfreeshops.com'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $item['url']='https://www.sugarfreeshops.com/el/products/21832109?option_variant_id=144';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.product-title')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('.product-price')->text())));  
        $product['brand']="";
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
       
        $product['sku'] = $crawler->filter('.product-code')->text(); 
        $product['short_description']= $crawler->filter('.product-description')->text();
        if($crawler->filter('.product-info-tab')->eq(1)->count()){
            $product['description'] =$crawler->filter('.product-info-tab')->eq(1)->html();
        }
        else{
            $product['description'] = $product['short_description'];
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags']="";
        // $product['tags'][]= $product['brand'];
        // $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $data="";
        $crawler->filter('[type="text/javascript"]')->each(function(Crawler $node)use(&$data){
            if(strstr($node->text(),"var gy_data")){
                $data = $node->text();
            }
        });
        $img = json_decode("{".explode("]};",explode("var gy_data = {",$data)[1])[0]."]}",true)['items'];
        foreach($img as $image){
            $images[]=[
                'src'=>$image['image'],
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.product-size')->count()){
            // $var=[];
            // $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$var){
            //     if(strstr($node->text(),'Magento_Swatches/js/swatch-renderer')){
            //         $var= json_decode($node->text(),true)['[data-role=swatch-options]']['Magento_Swatches/js/swatch-renderer']['jsonConfig']['attributes']['295']['options'];
            //     }      
            // });
            // foreach($var as $option){
            //     $options[]=$option['label'];
            // }
            $product['variations'][]=[
                'name'=>"ΜΕΓΕΘΟΣ",
                'options' =>$crawler->filter('.product-size li')->each(function(Crawler $node){
                    return $node->text();
                }),
            ];
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
                'name'=>"",
                'options' =>""];
        }
        print_r($product['variations']);exit;
        if($crawler->filter('#product-attribute-specs-table')->count()){
            $count=$crawler->filter('#product-attribute-specs-table tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name'=>$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('th')->text(),
                    'options'=>[$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('td')->text()]
                ];
            }
        }else{
            $product['attributes'] = [];
        }
       
        print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
