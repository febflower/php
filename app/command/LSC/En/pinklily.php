<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class pinklily extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:pinklily')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://pinklily.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://pinklily.com/collections/cozy-christmas',
            'https://pinklily.com/collections/pajama-shop',
            'https://pinklily.com/collections/christmas-graphics'
        ];
        $array1=[
            // 'https://pinklily.com/collections/cozy-christmas',
            // 'https://pinklily.com/collections/pajama-shop',
            // 'https://pinklily.com/collections/christmas-graphics',
            'https://pinklily.com/collections/glam-girl',
            'https://pinklily.com/collections/cozy-queen',
        ];
        $array2=[
            'https://pinklily.com/collections/pajama-shop',
            'https://pinklily.com/collections/christmas-graphics'
            // 'https://pinklily.com/collections/glam-girl',
            // 'https://pinklily.com/collections/cozy-queen',
            // 'https://pinklily.com/collections/the-trendsetter'
        ];
        $array3=[
            // 'https://pinklily.com/collections/girl-on-the-go',
            // 'https://pinklily.com/collections/must-have-items',
            // 'https://pinklily.com/collections/mama-mini',
            'https://pinklily.com/collections/cozy-christmas',
            'https://pinklily.com/collections/pajama-shop',
        ];
        $array4=[
            // 'https://pinklily.com/collections/cardigans-and-kimonos',
            // 'https://pinklily.com/collections/sweater-dresses',
            // 'https://pinklily.com/collections/sweater-tanks'
            'https://pinklily.com/collections/pajama-shop',
            'https://pinklily.com/collections/christmas-graphics'
        ];
        $array5=[
            'https://pinklily.com/collections/athletic-tops',
            'https://pinklily.com/collections/basics',
            'https://pinklily.com/collections/lounge-tops',  
        ];
        $array6=[
            // 'https://pinklily.com/collections/blouses#/filter:tags_print:Solid',
            // 'https://pinklily.com/collections/blouses?page=2#/filter:tags_print:Solid',
            // 'https://pinklily.com/collections/blouses?page=3#/filter:tags_print:Solid',
            'https://pinklily.com/collections/blouses#/filter:tags_print:Plaid',
            // 'https://pinklily.com/collections/blouses#/filter:tags_print:Floral',
            // 'https://pinklily.com/collections/blouses#/filter:tags_print:Stripes',
            // 'https://pinklily.com/collections/blouses#/filter:tags_print:Tropical',
        ];
        $array7=[
            'https://pinklily.com/collections/bodysuits',
            'https://pinklily.com/collections/cardigans-and-kimonos',
            'https://pinklily.com/collections/hoodies-and-pullovers' 
        ];
        $array8=[
            // 'https://pinklily.com/collections/custom-graphic-tees?page=3',
            // 'https://pinklily.com/collections/custom-graphic-tees'
            // 'https://pinklily.com/collections/custom-graphic-tees?page=8',
            'https://pinklily.com/collections/custom-graphic-tees?page=11'
        ];
        $array9=[
            'https://pinklily.com/collections/lounge-tops',
            'https://pinklily.com/collections/shackets',
        ];
        $array10=[
            'https://pinklily.com/collections/sweaters',
            'https://pinklily.com/collections/tanks',
        ];
        $array11=[
            'https://pinklily.com/collections/jeans',
            'https://pinklily.com/collections/pants',
        ];
        $array12=[
            'https://pinklily.com/collections/leggings',
            'https://pinklily.com/collections/skirts',
            'https://pinklily.com/collections/athletic-bottoms',
            'https://pinklily.com/collections/lounge-bottoms'
        ];
        $array13=[
            'https://pinklily.com/collections/shorts',
            'https://pinklily.com/collections/jean-shorts',
            'https://pinklily.com/collections/skirts'
        ];
        $array14=[
            'https://pinklily.com/collections/swim-one-piece',
            'https://pinklily.com/collections/swim-two-piece',
            'https://pinklily.com/collections/plus-size-swimwear',
        ];
        $array15=[
            'https://pinklily.com/collections/maxi-dresses',
            'https://pinklily.com/collections/midi-dresses',
            'https://pinklily.com/collections/short-dresses',
            'https://pinklily.com/collections/sweater-dresses',
            'https://pinklily.com/collections/bodycon-dresses',
            'https://pinklily.com/collections/t-shirt-dresses',
            'https://pinklily.com/collections/tank-dresses',
            'https://pinklily.com/collections/solid-dresses',
            'https://pinklily.com/collections/print-dresses',
            'https://pinklily.com/collections/floral-dresses'
        ];
        $array16=[
            'https://pinklily.com/collections/winter-graphics',
            'https://pinklily.com/collections/christmas-graphics',
            'https://pinklily.com/collections/fall-graphic-tees',
            'https://pinklily.com/collections/gameday-graphics',
            'https://pinklily.com/collections/teacher-shop',
            'https://pinklily.com/collections/southern-concert-graphics',
            'https://pinklily.com/collections/positive-vibes-tees',
            'https://pinklily.com/collections/mommy-me-graphics',
            'https://pinklily.com/collections/mom-graphic-tees'
        ];
        $array17=[
            'https://pinklily.com/collections/purses-bags',
            'https://pinklily.com/collections/scarves',
            'https://pinklily.com/collections/hats-beanies',
            'https://pinklily.com/collections/blankets',
            'https://pinklily.com/collections/sunglasses',
            'https://pinklily.com/collections/belts',
            'https://pinklily.com/collections/bralettes',
            'https://pinklily.com/collections/hair-accessories',
            'https://pinklily.com/collections/makeup-bags-brushes',
            'https://pinklily.com/collections/slippers',
        ];
        $array18=[
            'https://pinklily.com/collections/booties',
            'https://pinklily.com/collections/boots',
            'https://pinklily.com/collections/heels',
            'https://pinklily.com/collections/sneakers',
            'https://pinklily.com/collections/flats',
            'https://pinklily.com/collections/wedges',
            'https://pinklily.com/collections/sandals',
            'https://pinklily.com/collections/slippers',
            'https://pinklily.com/collections/bracelets',
            'https://pinklily.com/collections/necklaces',
            'https://pinklily.com/collections/earrings',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'pinklily001.seo080.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'torrid002.seo080.site/'],
            'x3' => ['source' => $array3, 'target' => 'torrid003.seo080.site/'],
            'x4' => ['source' => $array4, 'target' => 'torrid004.seo080.site/'],
            'x5' => ['source' => $array5, 'target' => 'pinklily005.seo080.site/'],
            'x6' => ['source' => $array6, 'target' => 'pinklily006.seo080.site/'],
            'x7' => ['source' => $array7, 'target' => 'pinklily007.seo080.site/'],
            'x8' => ['source' => $array8, 'target' => 'pinklily008.seo080.site/'],
            'x9' => ['source' => $array9, 'target' => 'torrid009.seo080.site/'],
            'x10' => ['source' => $array10, 'target' => 'pinklily010.seo081.site/'],
            'x11' => ['source' => $array11, 'target' => 'pinklily011.seo081.site/'],
            'x12' => ['source' => $array12, 'target' => 'pinklily012.seo081.site/'],
            'x13' => ['source' => $array13, 'target' => 'pinklily013.seo081.site/'],
            'x14' => ['source' => $array14, 'target' => 'pinklily014.seo081.site/'],
            'x15' => ['source' => $array15, 'target' => 'pinklily015.seo081.site/'],
            'x16' => ['source' => $array16, 'target' => 'pinklily016.seo081.site/'],
            'x17' => ['source' => $array17, 'target' => 'pinklily017.seo081.site/'],
            'x18' => ['source' => $array18, 'target' => 'pinklily018.seo081.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://pinklily.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
            // $response = $this->guzzleHttpClient->request('GET', $source);
            // $contents = $response->getBody()->getContents();
            // $crawler = new Crawler($contents);
            // $crawler->filter('#category-level-1 a')->each(function(Crawler $node){
            //     $this->processPage($node->attr('href'));
            // });   
        }
    }

    protected function processPage($url)
    {
        // echo "'".$url."',".PHP_EOL;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if($nextNode->count()){
            if($nextNode->attr('href')!=null){
                $this->processPage('https://pinklily.com'.$nextNode->attr('href'));
            }
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb a')->each(function (Crawler $node) {
            return $node->text();
        }));
        array_push($breadcrumbs,$crawler->filter('.breadcrumb span')->last()->text());
        array_push($breadcrumbs,"Mommy And Me");
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.grid-view-item__link')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => 'https://pinklily.com'.$node->attr('href'),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.torrid.com/product/super-soft-rib-henley-elbow-sleeve-tee/19553605.html?dwvar_19553605_color=BLACK%20STAR&cgid=Clothing_Tops_Tshirts';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data = "";
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$data){
            if(strstr($node->text(),"Product")){
                $data=json_decode($node->text(),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$data['name'];
        $product['sku'] = $data['sku'];
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$data['offers'][0]['price']))); 
        $product['brand']=$data['brand']['name'];
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        if($crawler->filter('.size-chart ul')->count()){
            $product['short_description']=str_replace($crawler->filter('.size-chart')->filter('style')->html(),"",$crawler->filter('.size-chart')->html());
        }else{
            $product['short_description']="";
        }
        if($crawler->filter('.size-chart table')->count()){
            $product['description'] =$crawler->filter('.product-single__description')->html().str_replace($crawler->filter('.size-chart')->filter('style')->html(),"",$crawler->filter('.size-chart')->html());
        }else{
            $product['description'] =$crawler->filter('.product-single__description')->html();
        } 
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        // $product['tags']="";
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=$crawler->filter('.custom-product-slider img')->each(function(Crawler $node){
            return 'https:'.$node->attr('src');
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.single-option-selector option')->count()){
            $product['variations'][]=[
                'name'=>"Size",
                'options' =>$crawler->filter('.single-option-selector option')->each(function(Crawler $node){
                    return $node->text();
                }),
            ];
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
                'name'=>"",
                'options' =>""];
        }
        // print_r($product['variations']);exit;
        // if($crawler->filter('#product-attribute-specs-table')->count()){
        //     $count=$crawler->filter('#product-attribute-specs-table tr')->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name'=>$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('th')->text(),
        //             'options'=>[$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('td')->text()]
        //         ];
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }
        $product['attributes'] = [];
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
