<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class funsport extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:funsport')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://food52.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://fun-sport-vision.com/outdoor-funsport/',
        ];
        $array1=[
            'https://fun-sport-vision.com/snowboardjacken/?p=7',
            // 'https://fun-sport-vision.com/snowboardjacken/?p=5',
            'https://fun-sport-vision.com/snowboardhandschuhe/',
        ];
        $array2=[
            
        ];
        $array3=[
            'https://fun-sport-vision.com/snowboard-zubehoer/',
            'https://fun-sport-vision.com/snowboardhelme/',
            'https://fun-sport-vision.com/snowboardhosen/',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'funsport014.xms005.site/'],#..
            'x1' => ['source' => $array1, 'target' => 'funsport0.xms005.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'funsport007.xms005.site/'],#..
            'x3' => ['source' => $array3, 'target' => 'funsport003.xms005.site/'],#..
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://food52.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);    
    }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[title="Nächste Seite"]');
        if ($nextNode->count()) {
            $this->processPage('https://fun-sport-vision.com'.$nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        // $breadcrumbs = $crawler->filter('.breadcrumb--list a')->each(function(Crawler $node,$i){
        //     return $node->text();
        // });
        $crawler->filter('.product--title')->each(function (Crawler $node,$i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        if($crawler->filter('.gender-name')->count()>0){
            $product['title'] =$crawler->filter('.product--title')->text()." ".$crawler->filter('.gender-name')->text();
        }else{
            $product['title'] =$crawler->filter('.product--title')->text();
        }
        $product['price'] = str_replace(',','',str_replace(' €','',str_replace('$','',$crawler->filter('[itemprop="price"]')->attr('content'))));
        $product['brand']=$crawler->filter('.product--supplier img')->attr('alt');
        $product['type'] = 'simple';

        $breadcrumbs = $crawler->filter('.breadcrumb--list a')->each(function(Crawler $node,$i){
            return $node->text();
        });
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        
        $product['sku'] = str_replace("..","",$crawler->filter('[itemprop="sku"]')->attr('content'));
        $product['short_description']= "";
        $product['description'] = $crawler->filter('[itemprop="description"]')->html();
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][]=[];
        // print_r($product);exit;
        //图片   
        if($crawler->filter('.thumbnail--link')->count()){
            $img = $crawler->filter('.thumbnail--link')->each(function(Crawler $node,$i){
                return $node->attr('href');
            });
        }elseif($crawler->filter('.image-slider--slide')->count()){
            $img = $crawler->filter('.image-slider--slide')->filter('.image--element')->each(function(Crawler $node,$i){
                return $node->attr('data-img-large');
            });
        }
        foreach($img as $image){
            $images[]=[
                'name'=>$product['title'],
                'src'=>$image,];
        }
        $product['images']=$this->toEncryptImage($images);
        // $product['images']=$images;

        if($crawler->filter('.product--configurator')->count()){
            if($crawler->filter('.product--configurator')->filter('.variant--option')->count()>1){
                $product['variations'][]= [
                    'name'=>$crawler->filter('.product--configurator')->filter('.variant--name')->text(),
                    'options' =>$crawler->filter('.product--configurator')->filter('.variant--option')->each(function(Crawler $node){
                        return $node->text();
                    })]; 
                $product['type'] = 'variable';
            }elseif($crawler->filter('.product--configurator')->filter('.select-field')->count()){
                $product['variations'][]= [
                    'name'=>$crawler->filter('.product--configurator')->filter('.select-field option')->eq(0)->text(),
                    'options' =>$crawler->filter('.product--configurator')->filter('.select-field option')->eq(0)->nextAll()->each(function(Crawler $node){
                        return $node->text();
                    })]; 
                $product['type'] = 'variable';
            }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];}
        }else{$product['variations'][]=[
                    'name'=>'',
                    'options' =>""];
        }

        if($crawler->filter('.product--properties-table')->count()>0) {
            $count = $crawler->filter('.product--properties-table tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => str_replace(':','',$crawler->filter('.product--properties-table tr')->eq($i)->filter('td')->eq(0)->text()),
                    'options' => [$crawler->filter('.product--properties-table tr')->eq($i)->filter('td')->eq(1)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
