<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class every extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:every')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.sportinglife.ca/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.tritschler.com/themenwelten/'
        ];
        $array1=[
            'https://www.tritschler.com/kueche/'
        ];
        $array2=[
'https://www.tritschler.com/themenwelten/geschenke-fuer-ihn/',
'https://www.tritschler.com/themenwelten/geschenke-fuer-sie/',
'https://www.tritschler.com/themenwelten/grillen/',
'https://www.tritschler.com/themenwelten/herbstliche-tage/',
'https://www.tritschler.com/themenwelten/japanische-kochmesser/',
'https://www.tritschler.com/themenwelten/kaffeewelt/',
'https://www.tritschler.com/themenwelten/kuerbis-st-martin-Tritschler/',
        ];
        $array3=[
            'https://www.tritschler.com/themenwelten/kids/',
'https://www.tritschler.com/themenwelten/silvester/',
'https://www.tritschler.com/themenwelten/sommerzeit/',
        ];
        $array4=[
            'https://www.tritschler.com/themenwelten/schulbedarf/',
            'https://www.tritschler.com/themenwelten/ostern/',
            'https://www.tritschler.com/themenwelten/erntezeit/',
            'https://www.tritschler.com/themenwelten/souvenirs/',
'https://www.tritschler.com/themenwelten/spargelzeit/',
'https://www.tritschler.com/themenwelten/trinken/',
'https://www.tritschler.com/themenwelten/unsere-neuheiten/',
'https://www.tritschler.com/themenwelten/valentinstag/',
'https://www.tritschler.com/themenwelten/weindorf/',

        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'tritschler001.seo080.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'tritschler002.seo080.site/'],
            'x3' => ['source' => $array3, 'target' => 'tritschler003.seo080.site/'],
            'x4' => ['source' => $array4, 'target' => 'tritschler004.seo080.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sportinglife.ca/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.panel-heading a')->count()){
                $crawler->filter('.panel-heading a')->each(function(Crawler $node){
                    $this->processPage($node->attr('href'));
                });
            }else{
                $this->processPage($source);
            }
        }
    }

    protected function processPage($url)
    {
        // echo "'".$url."',".PHP_EOL;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.next a');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('#breadcrumb li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.title a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.backcountrygear.com/targhee-45-1/';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        // $arrs = [];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$arrs){
        //     if(strstr($node->text(),"potentialAction")){
        //         $arrs=json_decode($node->text(),true);
        //     }
        // });
        // foreach($arrs as $arr){
        //     if(strstr(json_encode($arr),"aggregateRating")){
        //         $data=$arr;
        //     }
        // }
        // print_r($data);exit;
        $product['title'] =$crawler->filter('[itemprop="name"]')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace(' ','',$crawler->filter('[itemprop="price"]')->text()))); 
        $product['brand']=$crawler->filter('.brandLogo img')->attr('alt');
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
       
        $product['sku'] = str_replace("Artikelnummer: ","",$crawler->filter('.mpn')->text()); 
        $product['short_description']= "";
        
        if($crawler->filter('#description')->count()){
            $product['description'] =$crawler->filter('#description')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=[];
        if($crawler->filter('.otherPictures')->count()){
            $crawler->filter('.otherPictures a')->each(function(Crawler $node)use(&$img){
                    $img[] = $node->attr('data-zoom-url');
            });
        }else{
            $crawler->filter('#zoom1')->each(function(Crawler $node)use(&$img){
                    $img[] = $node->attr('href');
            });
        }
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        $product['attributes'] = [];
       
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
