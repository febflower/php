<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class sportinglife extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:sportinglife')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.sportinglife.ca/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            // 'https://parisfashionshops.com/fr/femme/categorie/bagues'
            'https://www.sportinglife.ca/en-CA/women/lifestyles/running/'
        ];
        $array10=[
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/coats-jackets/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/vests/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/hoodies-sweats/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/tops/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/sports-bras/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/pants/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/tights-leggings/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/running/shorts/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/snowboard/coats-jackets/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/snowboard/base-mid-layering/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/snowboard/hoodies-sweats/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/snowboard/tops/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/snowboard/pants/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/snowboard/tights-leggings/',
        ];
        $array11=[
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/coats-jackets/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/vests/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/sweaters/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/hoodies-sweats/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/tops/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/pants/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/tights-leggings/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/denim/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/dresses/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/skirts/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/fashion/shorts/',
        ];
        $array12=[
            'https://www.sportinglife.ca/en-CA/women/lifestyles/cycle/jackets/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/cycle/tops/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/cycle/pants/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/cycle/tights-leggings/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/cycle/shorts/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/swim/1-piece-swimsuits/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/swim/2-piece-swimsuits/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/swim/coverups-rashguards/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/swim/swim-shorts/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/coats-jackets/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/sweaters/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/tops/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/pants/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/dresses/',
            // 'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/skirts/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/tennis/shorts/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/golf/tops/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/golf/pants/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/golf/dresses/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/golf/skirts/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/golf/shorts/',
        ];
        $array20=[
            'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/coats-jackets/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/vests/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/base-mid-layering/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/hoodies-sweats/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/sweaters/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/ski/tights-leggings/',
        ];
        $array21=[
            'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/coats-jackets/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/vests/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/base-mid-layering/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/hoodies-sweats/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/snowboard/tights-leggings/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/coats-jackets/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/vests/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/hoodies-sweats/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/tights-leggings/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/running/shorts/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/cycle/jackets/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/cycle/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/cycle/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/cycle/shorts/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/cycle/tights-leggings/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/swim/rashguards/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/swim/swim-shorts/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/tennis/jackets/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/tennis/sweaters/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/tennis/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/tennis/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/tennis/shorts/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/golf/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/golf/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/golf/shorts/',
        ];
        $array22=[
            'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/coats-jackets/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/vests/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/hoodies-sweats/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/sweaters/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/tops/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/pants/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/denim/',
'https://www.sportinglife.ca/en-CA/men/lifestyles/fashion/shorts/',
        ];
        $array23=[
            'https://www.sportinglife.ca/en-CA/men/accessories/fitness-accessories/',
            'https://www.sportinglife.ca/en-CA/men/accessories/hats/baseball-caps/',
'https://www.sportinglife.ca/en-CA/men/accessories/hats/fashion-hats/',
'https://www.sportinglife.ca/en-CA/men/accessories/hats/sun-technical-hats/',
'https://www.sportinglife.ca/en-CA/men/accessories/eyewear/',
'https://www.sportinglife.ca/en-CA/men/accessories/watches/',
'https://www.sportinglife.ca/en-CA/men/accessories/more-accessories/',
        ];
        $array24=[
            'https://www.sportinglife.ca/en-CA/men/accessories/socks/',
            'https://www.sportinglife.ca/en-CA/men/accessories/belts/',
            'https://www.sportinglife.ca/en-CA/men/accessories/winter-accessories/winter-hats/',
'https://www.sportinglife.ca/en-CA/men/accessories/winter-accessories/scarfs-balaclavas/',
'https://www.sportinglife.ca/en-CA/men/accessories/winter-accessories/gloves-mitts/',
        ];
        $array25=[
            'https://www.sportinglife.ca/en-CA/kids/girls/coats-jackets/',
            'https://www.sportinglife.ca/en-CA/kids/girls/vests/',
            'https://www.sportinglife.ca/en-CA/kids/girls/hoodies-sweaters/',
            'https://www.sportinglife.ca/en-CA/kids/girls/swimwear/1-piece-swimsuits/',
'https://www.sportinglife.ca/en-CA/kids/girls/swimwear/2-piece-swimsuits/',
'https://www.sportinglife.ca/en-CA/kids/girls/swimwear/rashguards/',
'https://www.sportinglife.ca/en-CA/kids/girls/swimwear/swim-shorts/',
        ];
        $array26=[
            'https://www.sportinglife.ca/en-CA/kids/girls/1-piece-2-piece-sets/',
            'https://www.sportinglife.ca/en-CA/kids/girls/base-mid-layering/',
            'https://www.sportinglife.ca/en-CA/kids/girls/tops/',
            'https://www.sportinglife.ca/en-CA/kids/girls/dresses/',
            'https://www.sportinglife.ca/en-CA/kids/girls/pants/joggers-sweatpants/',
'https://www.sportinglife.ca/en-CA/kids/girls/pants/tights-leggings/',
'https://www.sportinglife.ca/en-CA/kids/girls/pants/snow-pants/',
'https://www.sportinglife.ca/en-CA/kids/girls/denim/',
'https://www.sportinglife.ca/en-CA/kids/girls/skirts/',
'https://www.sportinglife.ca/en-CA/kids/girls/shorts/'
        ];
        $array27=[
            'https://www.sportinglife.ca/en-CA/kids/boys/coats-jackets/',
            'https://www.sportinglife.ca/en-CA/kids/boys/vests/',
            'https://www.sportinglife.ca/en-CA/kids/boys/hoodies-sweaters/',
            'https://www.sportinglife.ca/en-CA/kids/boys/1-piece-2-piece-sets/',
            'https://www.sportinglife.ca/en-CA/kids/boys/shorts/',
        ];
        $array28=[
            'https://www.sportinglife.ca/en-CA/kids/boys/base-mid-layering/',
            'https://www.sportinglife.ca/en-CA/kids/boys/tops/',
            'https://www.sportinglife.ca/en-CA/kids/boys/pants/joggers-sweatpants/',
'https://www.sportinglife.ca/en-CA/kids/boys/pants/tights-leggings/',
'https://www.sportinglife.ca/en-CA/kids/boys/pants/snow-pants/',
'https://www.sportinglife.ca/en-CA/kids/boys/swimwear/swim-shorts/',
'https://www.sportinglife.ca/en-CA/kids/boys/swimwear/rashguards/',
        ];
        $array29=[
            'https://www.sportinglife.ca/en-CA/women/lifestyles/ski/sweaters/',
            'https://www.sportinglife.ca/en-CA/women/lifestyles/ski/pants/',
//             'https://www.sportinglife.ca/en-CA/kids/lifestyles/ski-snowboard/jackets/',
// 'https://www.sportinglife.ca/en-CA/kids/lifestyles/ski-snowboard/base-mid-layering/',
// 'https://www.sportinglife.ca/en-CA/kids/lifestyles/ski-snowboard/sweaters/',
// 'https://www.sportinglife.ca/en-CA/kids/lifestyles/ski-snowboard/pants/',

        ];
        $array30=[
            'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/jackets/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/sweaters/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/tops/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/pants/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/denim/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/dresses/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/skirts/',
'https://www.sportinglife.ca/en-CA/kids/lifestyles/fashion/shorts/',
        ];
        $array31=[
            'https://www.sportinglife.ca/en-CA/kids/accessories/hats/',
'https://www.sportinglife.ca/en-CA/kids/accessories/gloves-mitts/',
'https://www.sportinglife.ca/en-CA/kids/accessories/scarfs-balaclavas/',
'https://www.sportinglife.ca/en-CA/kids/accessories/socks/',
'https://www.sportinglife.ca/en-CA/kids/accessories/bags/',
'https://www.sportinglife.ca/en-CA/kids/accessories/eyewear/',
'https://www.sportinglife.ca/en-CA/kids/accessories/more-accessories/',
        ];
        $array32=[
            'https://www.sportinglife.ca/en-CA/shoes/women/running-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/women/rain-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/women/hiking-shoes/',
        ];
        $array33=[
            'https://www.sportinglife.ca/en-CA/shoes/women/casual-sneakers/',
            'https://www.sportinglife.ca/en-CA/shoes/women/casual-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/women/casual-shoes/',
        ];
        $array34=[
            'https://www.sportinglife.ca/en-CA/shoes/women/winter-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/women/tennis-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/women/fitness-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/women/sandals/',
        ];
        $array35=[
            'https://www.sportinglife.ca/en-CA/shoes/men/running-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/men/casual-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/men/hiking-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/men/sandals/',
        ];
        $array36=[
            'https://www.sportinglife.ca/en-CA/shoes/men/casual-sneakers/',
            'https://www.sportinglife.ca/en-CA/shoes/men/winter-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/men/casual-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/men/slippers/',
        ];
        $array37=[
            'https://www.sportinglife.ca/en-CA/shoes/kids/junior-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/kids/preschool-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/kids/slippers/',
        ];
        $array38=[
            'https://www.sportinglife.ca/en-CA/shoes/kids/infant-shoes/',
            'https://www.sportinglife.ca/en-CA/shoes/kids/winter-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/kids/rain-boots/',
            'https://www.sportinglife.ca/en-CA/shoes/kids/sandals/'
        ];
        $array39=[
            'https://www.sportinglife.ca/en-CA/equipment/cycle/active-bikes/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/road-bikes/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/mountain-bikes/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/e-bikes/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/kids-bikes/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/cycle-helmets/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/cycle-apparel/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/cycle-shoes/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/cycle-accessories/',
'https://www.sportinglife.ca/en-CA/equipment/cycle/cycle-components/',
        ];
        $array40=[
            'https://www.sportinglife.ca/en-CA/equipment/racquets/tennis-racquets/',
'https://www.sportinglife.ca/en-CA/equipment/racquets/tennis-balls/',
'https://www.sportinglife.ca/en-CA/equipment/racquets/strings-accessories/',
'https://www.sportinglife.ca/en-CA/equipment/racquets/racquet-bags/',
'https://www.sportinglife.ca/en-CA/equipment/racquets/squash/',
'https://www.sportinglife.ca/en-CA/equipment/racquets/badminton/',
'https://www.sportinglife.ca/en-CA/equipment/racquets/pickleball/',
        ];
        $array41=[
            'https://www.sportinglife.ca/en-CA/equipment/ski/alpine-skis/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-racing/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-bindings/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-boots/',
'https://www.sportinglife.ca/en-CA/equipment/ski/alpine-ski-poles/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-helmets/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-goggles/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-bags/',
'https://www.sportinglife.ca/en-CA/equipment/ski/ski-accessories/',
        ];
        $array42=[
            'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboards/',
'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboard-bindings/',
'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboard-boots/',
'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboard-helmets/',
'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboard-goggles/',
'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboard-bags/',
'https://www.sportinglife.ca/en-CA/equipment/snowboard/snowboard-accessories/',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x10' => ['source' => $array10, 'target' => 'sporting010.xms011.site/'],
            'x11' => ['source' => $array11, 'target' => 'sporting011.xms011.site/'],
            'x12' => ['source' => $array12, 'target' => 'sporting012.xms011.site/'],
            'x20' => ['source' => $array20, 'target' => 'sporting020.xms011.site/'],
            'x21' => ['source' => $array21, 'target' => 'sporting021.xms011.site/'],
            'x22' => ['source' => $array22, 'target' => 'sporting022.xms011.site/'],
            'x23' => ['source' => $array23, 'target' => 'sporting023.xms011.site/'],
            'x24' => ['source' => $array24, 'target' => 'sporting024.xms011.site/'],
            'x25' => ['source' => $array25, 'target' => 'sporting025.xms011.site/'],
            'x26' => ['source' => $array26, 'target' => 'sporting026.xms011.site/'],
            'x27' => ['source' => $array27, 'target' => 'sporting027.xms011.site/'],
            'x28' => ['source' => $array28, 'target' => 'sporting028.xms011.site/'],
            'x29' => ['source' => $array29, 'target' => 'sporting029.xms011.site/'],
            'x30' => ['source' => $array30, 'target' => 'sporting030.xms011.site/'],
            'x31' => ['source' => $array31, 'target' => 'sporting031.xms011.site/'],
            'x32' => ['source' => $array32, 'target' => 'sporting032.xms011.site/'],
            'x33' => ['source' => $array33, 'target' => 'sporting033.xms011.site/'],
            'x34' => ['source' => $array34, 'target' => 'sporting034.xms011.site/'],
            'x35' => ['source' => $array35, 'target' => 'sporting035.xms011.site/'],
            'x36' => ['source' => $array36, 'target' => 'sporting036.xms011.site/'],
            'x37' => ['source' => $array37, 'target' => 'sporting037.xms011.site/'],
            'x38' => ['source' => $array38, 'target' => 'sporting038.xms011.site/'],
            'x39' => ['source' => $array39, 'target' => 'sporting039.xms011.site/'],
            'x40' => ['source' => $array40, 'target' => 'sporting040.xms011.site/'],
            'x41' => ['source' => $array41, 'target' => 'sporting041.xms011.site/'],
            'x42' => ['source' => $array42, 'target' => 'sporting042.xms011.site/'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sportinglife.ca/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            // if($crawler->filter('#category-level-1')->count()){
            //     $crawler->filter('#category-level-1 a')->each(function(Crawler $node){
            //         $this->processPage($node->attr('href'));
            //     });
            // }else{
            //     $this->processPage($source);
            // }
            $this->processPage($source);
        }
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.infinite-scroll-placeholder');
        if ($nextNode->count()) {
            if($nextNode->attr('data-grid-url')!=$url){
                $this->processPage($nextNode->attr('data-grid-url'));
            }
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        // $breadcrumbs = array_filter($crawler->filter('#breadcrumbs a')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.name-link')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                if(strstr($node->attr('href'),"https://www.sportinglife.ca")){
                    $url=$node->attr('href');
                }else{
                    $url="https://www.sportinglife.ca".$node->attr('href');
                }
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.product-name')->text(),
                    'url' => sprintf($url),
                    #'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.sportinglife.ca/en-CA/men/accessories/winter-accessories/scarfs-balaclavas/unisex-fastech-gaiter-25632373.html?dwvar_25632373_color=738&dwvar_25632373_size=000&cgid=women-accessories-winter-accessories';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('[itemprop="name"]')->text(); 
        if($crawler->filter('.has-standard-price')->count()){
            $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('.has-standard-price')->text())));
        }else{
            $product['price'] = str_replace(',','',str_replace('€','',str_replace('$','',$crawler->filter('.price-sales')->text())));
        }
        // print_r($product);exit;
        $product['brand']=$crawler->filter('[itemprop="brand"]')->text();
        $product['type'] = 'simple';
       
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] = $crawler->filter('.product-number span')->attr('data-masterid'); 
        $product['short_description']= str_replace($crawler->filter('.product-short-description a')->text(),"",str_replace($crawler->filter('.product-short-description h3')->text(),"",$crawler->filter('.product-short-description')->text()));  
        if($crawler->filter('.tab-content__2-col-wrap')->count()){
            $product['description'] =$crawler->filter('.tab-content__2-col-wrap')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('.selected-value')->count()){
            $product['color'] = $crawler->filter('.selected-value')->text();
        }else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=[];
        if($crawler->filter('.swatchanchor')->count()){
            $crawler->filter('.swatchanchor')->each(function(Crawler $node)use(&$img){
                $response = $this->guzzleHttpClient->request('GET', $node->attr('href'));
                $contents = $response->getBody()->getContents();
                $crawler = new Crawler($contents);
                $crawler->filter('.product-primary-image a')->each(function(Crawler $node)use(&$img){
                    $img[] = $node->attr('href');
                });
            });
        }else{
            $crawler->filter('.product-primary-image a')->each(function(Crawler $node)use(&$img){
                $img[] = $node->attr('href');
            });
        }
        
        // print_r($img);exit;
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.product-variations')->count()){
            if($crawler->filter('.swatchanchor')->count()>1){
                $product['variations'][]= [
                'name'=>'Color',
                'options' =>$crawler->filter('.swatchanchor')->each(function(Crawler $node){
                    return $node->attr('data-color');
                })]; 
                $product['type'] = 'variable';
            }else{
                $product['variations'][]=[
                'name'=>"",
                'options' =>""];
            }
            if($crawler->filter('#va-size option')->count()>1){
                $product['variations'][]= [
                'name'=>'Size',
                'options' =>$crawler->filter('#va-size option')->eq(0)->nextAll()->each(function(Crawler $node){
                    return $node->text();
                })]; 
                $product['type'] = 'variable';
            }else{
                $product['variations'][]=[
                'name'=>"",
                'options' =>""];
            }       
        }else{
            $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        }
        $product['attributes'] = [];
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
