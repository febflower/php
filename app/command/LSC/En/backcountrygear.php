<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class backcountrygear extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:backcountrygear')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.backcountrygear.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.backcountrygear.com/camping-and-hiking/backpacks/backpacking-packs/',
            'https://www.backcountrygear.com/camping-and-hiking/backpacks/ultralight-packs/',
            'https://www.backcountrygear.com/camping-and-hiking/backpacks/climbing-packs/',
        ];
        $array11=[
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/utensils/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/water-treatment/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/backpacking-meals/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/coolers/'
        ];
        $array12=[
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/beer-growlers-and-pints/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/coffee-tea/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/bear-canisters/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/kitchen-accessories/',
            'https://www.backcountrygear.com/camping-and-hiking/kitchen/tables/'
        ];
        $array13=[
            'https://www.backcountrygear.com/camping-and-hiking/dog-gear/dog-packs-harnesses/',
'https://www.backcountrygear.com/camping-and-hiking/dog-gear/dog-coats-booties/',
'https://www.backcountrygear.com/camping-and-hiking/dog-gear/dog-leashes-collars/',
'https://www.backcountrygear.com/camping-and-hiking/dog-gear/dog-flotation/',
'https://www.backcountrygear.com/camping-and-hiking/dog-gear/dog-toys-treats/',
'https://www.backcountrygear.com/camping-and-hiking/dog-gear/dog-accessories/',
            'https://www.backcountrygear.com/camping-and-hiking/trekking-poles/trekking-poles/',
'https://www.backcountrygear.com/camping-and-hiking/trekking-poles/staffs/',
'https://www.backcountrygear.com/camping-and-hiking/trekking-poles/accessories/',
'https://www.backcountrygear.com/camp-and-hike/lighting/headlamps/',
'https://www.backcountrygear.com/camp-and-hike/lighting/lanterns/',
'https://www.backcountrygear.com/camp-and-hike/lighting/accessories/',
'https://www.backcountrygear.com/camping-and-hiking/outdoor-recreation/games-toys/',
'https://www.backcountrygear.com/camping-and-hiking/outdoor-recreation/kites/',
'https://www.backcountrygear.com/camping-and-hiking/outdoor-recreation/slacklines/',
        ];
        $array14=[
            'https://www.backcountrygear.com/camping-and-hiking/accessories/bear-protection/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/books/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/bug-nets-repellent/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/camp-chairs/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/compression-sacks/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/dry-bags/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/emergency-gear/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/first-aid/',
            'https://www.backcountrygear.com/camping-and-hiking/accessories/hammocks-accessories/',
        ];
        $array15=[
            'https://www.backcountrygear.com/camping-and-hiking/accessories/knives-and-tools/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/lighters-firestarters/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/personal-hygiene/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/power-supplies/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/repair-kits/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/stickers-and-patches/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/storage-bags/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/stuff-sacks/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/tables/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/tent-stakes/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/toiletries/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/outdoor-blankets/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/treatments-and-cleaners/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/umbrellas/',
'https://www.backcountrygear.com/camping-and-hiking/accessories/water-tight-cases/',
        ];
        $array16=[
            'https://www.backcountrygear.com/climbing/ropes-cordage/climbing-rope/',
'https://www.backcountrygear.com/climbing/ropes-cordage/static-rope/',
'https://www.backcountrygear.com/climbing/ropes-cordage/cordage/',
'https://www.backcountrygear.com/climbing/ropes-cordage/rope-accessories/',
'https://www.backcountrygear.com/climbing/slings-webbing/daisy-chains-tethers/',
'https://www.backcountrygear.com/climbing/slings-webbing/slings-runners/',
'https://www.backcountrygear.com/climbing/slings-webbing/webbing/',
'https://www.backcountrygear.com/climbing/slings-webbing/dogbones/',
        ];
        $array17=[
            'https://www.backcountrygear.com/climbing/protection-hardware/active-pro-cams/',
'https://www.backcountrygear.com/climbing/protection-hardware/passive-pro/',
'https://www.backcountrygear.com/climbing/protection-hardware/belay-rappel-devices/',
'https://www.backcountrygear.com/climbing/protection-hardware/ascenders/',
'https://www.backcountrygear.com/climbing/protection-hardware/pulleys-swivels/',
'https://www.backcountrygear.com/climbing/protection-hardware/bolts-hangers/',
'https://www.backcountrygear.com/climbing/protection-hardware/ice-snow-protection/',
'https://www.backcountrygear.com/climbing/protection-hardware/pitons-hooks-aid-pro/',
'https://www.backcountrygear.com/climbing/protection-hardware/rigging-plates/',
'https://www.backcountrygear.com/climbing/protection-hardware/crash-pads/',
        ];
        $array18=[
            'https://www.backcountrygear.com/climbing/carabiners-quickdraws/non-locking-carabiners/',
'https://www.backcountrygear.com/climbing/carabiners-quickdraws/locking-carabiners/',
'https://www.backcountrygear.com/climbing/carabiners-quickdraws/quickdraws/',
        ];
        $array19=[
            'https://www.backcountrygear.com/climbing/climbing-packs-bags/rope-bags/',
'https://www.backcountrygear.com/climbing/climbing-packs-bags/haul-bags/',
'https://www.backcountrygear.com/climbing/climbing-packs-bags/climbing-packs/',
'https://www.backcountrygear.com/climbing/climbing-packs-bags/summit-packs/',
'https://www.backcountrygear.com/climbing/climbing-shoes/shoes-womens/',
'https://www.backcountrygear.com/climbing/climbing-shoes/shoes-mens/',
'https://www.backcountrygear.com/climbing/helmets/',
        ];
        $array20=[
            'https://www.backcountrygear.com/climbing/harnesses/mens-harnesses/',
'https://www.backcountrygear.com/climbing/harnesses/womens-harnesses/',
'https://www.backcountrygear.com/climbing/harnesses/youth-harnesses/',
'https://www.backcountrygear.com/climbing/bouldering/',
'https://www.backcountrygear.com/climbing/rescue-industrial/ascenders-descenders/',
'https://www.backcountrygear.com/climbing/rescue-industrial/pulleys-swivels/',
'https://www.backcountrygear.com/climbing/rescue-industrial/carabiners/',
'https://www.backcountrygear.com/climbing/rescue-industrial/rigging-plates/',
'https://www.backcountrygear.com/climbing/rescue-industrial/harnesses/',
'https://www.backcountrygear.com/climbing/rescue-industrial/slings-aiders/',
        ];
        $array21=[
            'https://www.backcountrygear.com/climbing/ice-climbing-mountaineering/ice-axes-tools/',
            'https://www.backcountrygear.com/climbing/ice-climbing-mountaineering/ice-axe-accessories/',
            'https://www.backcountrygear.com/climbing/ice-climbing-mountaineering/crampons/',
            'https://www.backcountrygear.com/climbing/ice-climbing-mountaineering/protection/',
        ];
        $array22=[
            'https://www.backcountrygear.com/climbing/big-wall/aiders-daisies/',
'https://www.backcountrygear.com/climbing/big-wall/aid-pro/',
'https://www.backcountrygear.com/climbing/big-wall/ascenders/',
'https://www.backcountrygear.com/climbing/big-wall/bolting-gear/',
'https://www.backcountrygear.com/climbing/big-wall/pulleys-swivels/',
'https://www.backcountrygear.com/climbing/big-wall/harnesses/',
'https://www.backcountrygear.com/climbing/big-wall/haul-bags/',
'https://www.backcountrygear.com/climbing/big-wall/shelters-ledges/',
'https://www.backcountrygear.com/climbing/big-wall/accessories/',
        ];
        $array23=[
            'https://www.backcountrygear.com/climbing/climbing-accessories/climbing-guides/',
'https://www.backcountrygear.com/climbing/climbing-accessories/chalk-bags-chalk/',
'https://www.backcountrygear.com/climbing/climbing-accessories/brushes/',
'https://www.backcountrygear.com/climbing/climbing-accessories/training-holds/',
'https://www.backcountrygear.com/climbing/climbing-accessories/accessory-carabiners/',
'https://www.backcountrygear.com/climbing/climbing-accessories/climbing-belay-gloves/',
        ];
        $array24=[
            'https://www.backcountrygear.com/travel/wheeled-luggage/',
            'https://www.backcountrygear.com/travel/organizers/cubes-organizers/',
'https://www.backcountrygear.com/travel/organizers/toiletry-kits/',
'https://www.backcountrygear.com/travel/travel-packs-bags/large-travel-packs/',
'https://www.backcountrygear.com/travel/travel-packs-bags/travel-daypacks/',
'https://www.backcountrygear.com/travel/travel-packs-bags/duffels/',
'https://www.backcountrygear.com/travel/travel-packs-bags/laptop-packs-bags/',
'https://www.backcountrygear.com/travel/travel-packs-bags/shoulder-messenger-bags/',
'https://www.backcountrygear.com/travel/travel-packs-bags/travel-totes/',
        ];
        $array25=[
            'https://www.backcountrygear.com/travel/photography/',
            'https://www.backcountrygear.com/travel/electronics/',
            'https://www.backcountrygear.com/travel/water-treatment/',
            'https://www.backcountrygear.com/travel/travel-apparel/mens-tops/',
'https://www.backcountrygear.com/travel/travel-apparel/womens-tops/',
'https://www.backcountrygear.com/travel/travel-apparel/mens-bottoms/',
'https://www.backcountrygear.com/travel/travel-apparel/womens-bottoms/',
'https://www.backcountrygear.com/travel/travel-apparel/clothing-accessories/',
'https://www.backcountrygear.com/travel/personal-care/first-aid/',
'https://www.backcountrygear.com/travel/personal-care/skin-care/',
'https://www.backcountrygear.com/travel/personal-care/travel-towels/',
'https://www.backcountrygear.com/travel/travel-accessories/sleep-liners/',
'https://www.backcountrygear.com/travel/travel-accessories/pillows/',
'https://www.backcountrygear.com/travel/travel-accessories/wallets-and-money-clips/',
'https://www.backcountrygear.com/travel/travel-accessories/travel-security/',
'https://www.backcountrygear.com/travel/travel-accessories/insect-protection/',
        ];
        $array26=[
            'https://www.backcountrygear.com/snow-sports/snowsports-apparel/mens-snowsports-apparel/',
'https://www.backcountrygear.com/snow-sports/snowsports-apparel/womens-snowsports-apparel/',
'https://www.backcountrygear.com/snow-sports/hats-gloves-gaiters/mens-hats/',
'https://www.backcountrygear.com/snow-sports/hats-gloves-gaiters/mens-mitts-gloves/',
'https://www.backcountrygear.com/snow-sports/hats-gloves-gaiters/womens-hats/',
'https://www.backcountrygear.com/snow-sports/hats-gloves-gaiters/womens-mitts-gloves/',
'https://www.backcountrygear.com/snow-sports/hats-gloves-gaiters/gaiters/',
'https://www.backcountrygear.com/snow-sports/snowshoes/mens-snowshoes/',
'https://www.backcountrygear.com/snow-sports/snowshoes/womens-snowshoes/',
'https://www.backcountrygear.com/snow-sports/snowshoes/snowshoe-poles/',
'https://www.backcountrygear.com/snow-sports/snowshoes/snowshoe-accessories/',
'https://www.backcountrygear.com/snow-sports/avalanche-gear/beacons/',
'https://www.backcountrygear.com/snow-sports/avalanche-gear/probes/',
'https://www.backcountrygear.com/snow-sports/avalanche-gear/avalanche-ready-packs/',
'https://www.backcountrygear.com/snow-sports/avalanche-gear/snow-shovels-saws/',
        ];
        $array27=[
            'https://www.backcountrygear.com/paddle-sports/mens-paddle-apparel/',
            'https://www.backcountrygear.com/paddle-sports/womens-paddle-apparel/',
            'https://www.backcountrygear.com/paddle-sports/footwear/',
            'https://www.backcountrygear.com/paddle-sports/personal-floatation/',
            'https://www.backcountrygear.com/paddle/rescue-helmets/',
            'https://www.backcountrygear.com/paddle-sports/boating-accessories/',
            'https://www.backcountrygear.com/paddle-sports/dry-storage/',
            'https://www.backcountrygear.com/paddle-sports/cook-camp-lounge/',
        ];
        $array28=[
            'https://www.backcountrygear.com/mens-apparel/mens-jackets/down-jackets/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/synthetic-insulated-jackets/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/snowsports-jackets/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/technical-shells/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/soft-shells/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/wind-jackets/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/fleece-jackets/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/rain-jackets/',
'https://www.backcountrygear.com/mens-apparel/mens-jackets/vests/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/pant-shells/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/rain-pants/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/wind-pants/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/insulated-pants/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/hiking-climbing-pants/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/casual-pants-jeans/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/shorts/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/merino-leggings/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/synthetic-leggings/',
'https://www.backcountrygear.com/mens-apparel/mens-bottoms/snowsports-pants/',
        ];
        $array29=[
            'https://www.backcountrygear.com/mens-apparel/mens-shirts/casual-shirts/',
'https://www.backcountrygear.com/mens-apparel/mens-shirts/sweaters-hoodies/',
'https://www.backcountrygear.com/mens-apparel/mens-shirts/sun-protective-shirts/',
'https://www.backcountrygear.com/mens-apparel/mens-shirts/merino-bamboo-tops/',
'https://www.backcountrygear.com/mens-apparel/mens-shirts/synthetic-tops/',
'https://www.backcountrygear.com/mens-apparel/mens-headwear/caps/',
'https://www.backcountrygear.com/mens-apparel/mens-headwear/beanies/',
'https://www.backcountrygear.com/mens-apparel/mens-headwear/balaclavas/',
'https://www.backcountrygear.com/mens-apparel/mens-headwear/neck-ear-warmers/',
'https://www.backcountrygear.com/mens-apparel/mens-headwear/sun-protection/',
'https://www.backcountrygear.com/mens-apparel/mens-headwear/rain-hats/',
'https://www.backcountrygear.com/mens-apparel/baselayers-underwear/boxers-briefs/',
'https://www.backcountrygear.com/mens-apparel/baselayers-underwear/merino-bamboo-tops/',
'https://www.backcountrygear.com/mens-apparel/baselayers-underwear/merino-bottoms/',
'https://www.backcountrygear.com/mens-apparel/baselayers-underwear/synthetic-tops/',
'https://www.backcountrygear.com/mens-apparel/baselayers-underwear/synthetic-bottoms/',
        ];
        $array30=[
            'https://www.backcountrygear.com/mens-apparel/gloves/liners/',
'https://www.backcountrygear.com/mens-apparel/gloves/technical-mitts/',
'https://www.backcountrygear.com/mens-apparel/gloves/technical-gloves/',
'https://www.backcountrygear.com/mens-apparel/gloves/casual-gloves/',
'https://www.backcountrygear.com/mens-apparel/socks/running-socks/',
'https://www.backcountrygear.com/mens-apparel/socks/ski-socks/',
'https://www.backcountrygear.com/mens-apparel/socks/hiking-socks/',
'https://www.backcountrygear.com/mens-apparel/socks/cold-weather-socks/',
'https://www.backcountrygear.com/mens-apparel/socks/casual-socks/',
'https://www.backcountrygear.com/mens-apparel/footwear/casual-shoes/',
'https://www.backcountrygear.com/mens-apparel/footwear/approach-shoes/',
'https://www.backcountrygear.com/mens-apparel/footwear/mountaineering-boots/',
'https://www.backcountrygear.com/mens-apparel/footwear/hiking-backpacking/',
'https://www.backcountrygear.com/mens-apparel/footwear/trail-running/',
'https://www.backcountrygear.com/mens-apparel/footwear/booties-slippers/',
'https://www.backcountrygear.com/mens-apparel/clothing-accessories/bandanas/',
'https://www.backcountrygear.com/mens-apparel/clothing-accessories/gaiters/',
'https://www.backcountrygear.com/mens-apparel/clothing-accessories/eyewear/',
'https://www.backcountrygear.com/mens-apparel/clothing-accessories/bug-netting/',
'https://www.backcountrygear.com/mens-apparel/clothing-accessories/belts-wallets/',
        ];
        $array31=[
            'https://www.backcountrygear.com/womens-apparel/womens-bottoms/pant-shells/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/rain-pants/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/snowsports-pants/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/insulated-pants/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/casual-pants-jeans/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/shorts-skirts-dresses/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/hiking-climbing-pants/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/merino-leggings/',
'https://www.backcountrygear.com/womens-apparel/womens-bottoms/synthetic-leggings/',
'https://www.backcountrygear.com/womens-apparel/womens-shirts/casual-shirts/',
'https://www.backcountrygear.com/womens-apparel/womens-shirts/sweaters-hoodies/',
'https://www.backcountrygear.com/womens-apparel/womens-shirts/merino-bamboo-tops/',
'https://www.backcountrygear.com/womens-apparel/womens-shirts/synthetic-tops/',
'https://www.backcountrygear.com/womens-apparel/womens-shirts/sun-shirts/',
        ];
        $array32=[
            'https://www.backcountrygear.com/womens-apparel/womens-jackets/down-jackets/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/synthetic-filled-jackets/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/snowsports-jackets/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/technical-shells/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/soft-shells/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/fleece-jackets/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/wind-jackets/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/rain-jackets/',
'https://www.backcountrygear.com/womens-apparel/womens-jackets/vests/',
'https://www.backcountrygear.com/womens-apparel/headwear/caps/',
'https://www.backcountrygear.com/womens-apparel/headwear/beanies/',
'https://www.backcountrygear.com/womens-apparel/headwear/balaclavas/',
'https://www.backcountrygear.com/womens-apparel/headwear/neck-and-ear-warmers/',
'https://www.backcountrygear.com/womens-apparel/headwear/sun-protection/',
'https://www.backcountrygear.com/womens-apparel/headwear/rain-hats/',
        ];
        $array33=[
            'https://www.backcountrygear.com/womens-apparel/baselayers-underwear/bras-underwear/',
'https://www.backcountrygear.com/womens-apparel/baselayers-underwear/merino-bamboo-tops/',
'https://www.backcountrygear.com/womens-apparel/baselayers-underwear/merino-bottoms/',
'https://www.backcountrygear.com/womens-apparel/baselayers-underwear/synthetic-tops/',
'https://www.backcountrygear.com/womens-apparel/baselayers-underwear/synthetic-bottoms/',
'https://www.backcountrygear.com/womens-apparel/gloves/liners/',
'https://www.backcountrygear.com/womens-apparel/gloves/technical-mitts/',
'https://www.backcountrygear.com/womens-apparel/gloves/technical-gloves/',
'https://www.backcountrygear.com/womens-apparel/gloves/casual-gloves/',
'https://www.backcountrygear.com/womens-apparel/socks/running-socks/',
'https://www.backcountrygear.com/womens-apparel/socks/ski-socks/',
'https://www.backcountrygear.com/womens-apparel/socks/hiking-socks/',
'https://www.backcountrygear.com/womens-apparel/socks/cold-weather-socks/',
'https://www.backcountrygear.com/womens-apparel/socks/casual-socks/',
'https://www.backcountrygear.com/womens-apparel/footwear/casual-shoes-boots/',
'https://www.backcountrygear.com/womens-apparel/footwear/climbing-approach/',
'https://www.backcountrygear.com/womens-apparel/footwear/mountaineering/',
'https://www.backcountrygear.com/womens-apparel/footwear/hiking-backpacking/',
'https://www.backcountrygear.com/womens-apparel/footwear/trail-running/',
'https://www.backcountrygear.com/womens-apparel/footwear/booties-slippers/',
'https://www.backcountrygear.com/womens-apparel/clothing-accessories/bandanas/',
'https://www.backcountrygear.com/womens-apparel/clothing-accessories/gaiters/',
'https://www.backcountrygear.com/womens-apparel/clothing-accessories/eyewear/',
'https://www.backcountrygear.com/womens-apparel/clothing-accessories/bug-netting/',
'https://www.backcountrygear.com/womens-apparel/clothing-accessories/belts-wallets/',
        ];
        $array34=[
            'https://www.backcountrygear.com/footwear/mens-trail-mountain/trail-running-shoes/',
            'https://www.backcountrygear.com/footwear/mens-trail-mountain/hiking-backpacking/',
            'https://www.backcountrygear.com/footwear/mens-trail-mountain/mountaineering-boots/',
            'https://www.backcountrygear.com/footwear/womens-trail-mountain/trail-running-shoes/',
            'https://www.backcountrygear.com/footwear/womens-trail-mountain/hiking-backpacking/',
            'https://www.backcountrygear.com/footwear/womens-trail-mountain/mountaineering-boots/',
            'https://www.backcountrygear.com/footwear/mens-climbing/approach-shoes/',
            'https://www.backcountrygear.com/footwear/mens-climbing/climbing-shoes/',
            'https://www.backcountrygear.com/footwear/womens-climbing/approach-shoes/',
            'https://www.backcountrygear.com/footwear/womens-climbing/climbing-shoes/',
            'https://www.backcountrygear.com/footwear/mens-casual/casual-shoes/',
            'https://www.backcountrygear.com/footwear/mens-casual/casual-shoes-and-sandals/',
            'https://www.backcountrygear.com/footwear/mens-casual/booties-slippers/',
            'https://www.backcountrygear.com/footwear/womens-casual/casual-shoes-1/',
            'https://www.backcountrygear.com/footwear/womens-casual/casual-shoes/',
            'https://www.backcountrygear.com/footwear/womens-casual/booties-slippers/',
            'https://www.backcountrygear.com/footwear/accessories/footbeds/',
            'https://www.backcountrygear.com/footwear/accessories/traction-devices/',
            'https://www.backcountrygear.com/footwear/accessories/cleaners-and-treatments/',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x11' => ['source' => $array11, 'target' => 'bacounge011.xms011.site/'],
            'x12' => ['source' => $array12, 'target' => 'bacounge012.xms011.site/'],
            'x13' => ['source' => $array13, 'target' => 'bacounge013.xms011.site/'],
            'x14' => ['source' => $array14, 'target' => 'bacounge014.xms011.site/'],
            'x15' => ['source' => $array15, 'target' => 'bacounge015.xms011.site/'],
            'x16' => ['source' => $array16, 'target' => 'bacounge016.xms011.site/'],
            'x17' => ['source' => $array17, 'target' => 'bacounge017.xms011.site/'],
            'x18' => ['source' => $array18, 'target' => 'bacounge018.xms011.site/'],
            'x19' => ['source' => $array19, 'target' => 'bacounge019.xms011.site/'],
            'x20' => ['source' => $array20, 'target' => 'bacounge020.xms011.site/'],
            'x21' => ['source' => $array21, 'target' => 'bacounge021.xms011.site/'],
            'x22' => ['source' => $array22, 'target' => 'bacounge022.xms011.site/'],
            'x23' => ['source' => $array23, 'target' => 'bacounge023.xms011.site/'],
            'x24' => ['source' => $array24, 'target' => 'bacounge024.xms011.site/'],
            'x25' => ['source' => $array25, 'target' => 'bacounge025.xms011.site/'],
            'x26' => ['source' => $array26, 'target' => 'bacounge026.xms011.site/'],
            'x27' => ['source' => $array27, 'target' => 'bacounge027.xms011.site/'],
            'x28' => ['source' => $array28, 'target' => 'bacounge028.xms011.site/'],
            'x29' => ['source' => $array29, 'target' => 'bacounge029.xms011.site/'],
            'x30' => ['source' => $array30, 'target' => 'bacounge030.xms011.site/'],
            'x31' => ['source' => $array31, 'target' => 'bacounge031.xms011.site/'],
            'x32' => ['source' => $array32, 'target' => 'bacounge032.xms011.site/'],
            'x33' => ['source' => $array33, 'target' => 'bacounge033.xms011.site/'],
            'x34' => ['source' => $array34, 'target' => 'bacounge034.xms011.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.backcountrygear.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs a')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.card-title a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.backcountrygear.com/targhee-45-1/';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.productView-title')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('[itemprop="price"]')->attr('content'))));  
        $product['brand']=$crawler->filter('.productView')->attr('data-product-brand');
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
       
        $product['sku'] = $crawler->filter('.productView')->attr('data-entity-id'); 
        $product['short_description']= "";
     
        if($crawler->filter('.productView-description')->count()){
            $product['description'] =$crawler->filter('.productView-description')->html().$crawler->filter('.product-warranty>div')->last()->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=[];
        $crawler->filter('[data-fancybox="gallery"]')->each(function(Crawler $node)use(&$img){
            $img[] = str_replace("?c=1","",$node->attr('href'));
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);
        if($crawler->filter('.pro-option-row')->count()){
            if($crawler->filter('.pro-option-row')->filter('[data-product-attribute="swatch"]')->count()){
                $product['variations'][]=[
                    'name'=>"Color",
                    'options' =>$crawler->filter('.pro-option-row')->filter('[data-product-attribute="swatch"]')->filter('.form-option-variant--pattern')->each(function(Crawler $node){
                        return $node->attr('title');
                    })
                ];
                $product['type'] = 'variable';
            }

            if($crawler->filter('.pro-option-row')->filter('[data-product-attribute="set-rectangle"]')->count()){
                $product['variations'][]=[
                    'name'=>"Size",
                    'options' =>$crawler->filter('.pro-option-row')->filter('[data-product-attribute="set-rectangle"]')->filter('.form-option-wrapper')->each(function(Crawler $node){
                        return $node->text();
                    })
                ];
                $product['type'] = 'variable';
            }  
        }else{
            $product['variations'][]=[
                'name'=>"",
                'options' =>""];
        }
        // print_r($product['variations']);exit;
        if($crawler->filter('.product-warranty>div')->count()>1){
            $count=$crawler->filter('.product-warranty>div')->count();
            for($i=0;$i<$count-1;$i++){
                $product['attributes'][]= [
                    'name'=>$crawler->filter('.product-warranty>div')->eq($i)->filter('div')->eq(1)->text(),
                    'options'=>[$crawler->filter('.product-warranty>div')->eq($i)->filter('div')->eq(2)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
