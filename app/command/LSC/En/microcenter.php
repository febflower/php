<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class microcenter extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:microcenter')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.everythingkitchens.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.microcenter.com/search/search_results.aspx?Ntk=all&sortby=match&N=4294967262&myStore=false',
        ];
        $array1=[
           'https://www.microcenter.com/search/search_results.aspx?Ntk=all&sortby=match&N=4294967262&myStore=false',
           
           'https://www.microcenter.com/search/search_results.aspx?Ntk=all&sortby=match&N=4294967291&myStore=false',
         ];
         $array2=[
           
         ];
         $array3=[

         ];
         $this->sites = [
             'x' => ['source' => $array, 'target' => 'micro006.xms005.site/'],
             'x1' => ['source' => $array1, 'target' => 'micro006.xms005.site/'],
             'x2' => ['source' => $array2, 'target' => 'micro005.xms005.site/'],
             'x3' => ['source' => $array3, 'target' => 'micro005.xms005.site/'],
         ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.everythingkitchens.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.vaticangift.com/jewellery-crucifixes/287-the-original-pope-francis-pastoral-crucifix-silver.html');
    }

    protected function processPage($url)
    {

        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.pages a')->last();
        if ($nextNode->count()) {
            $this->processPage('https://www.microcenter.com'.$nextNode->attr('href'));
        }
        
    }

    protected function processProductList(Crawler $crawler)
    {
        // $breadcrumbs = array_filter($crawler->filter('#contentResults h1')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        $crawler->filter('.pDescription a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('https://www.microcenter.com'.$node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        if($crawler->filter('.DescSmall')->count()>0){
            $product['title'] = str_replace($crawler->filter('.DescSmall')->text(),"",$crawler->filter('#details>h1')->filter('span')->text());
        }else{
            $product['title'] = $crawler->filter('#details>h1')->filter('span')->text();
        }
        $product['price'] = str_replace(',','',str_replace(' €','',str_replace('$','',$crawler->filter('#pricing')->attr('content'))));
        $product['type'] = 'simple';
        $breadcrumbs = array_filter($crawler->filter('[aria-labelledby="breadcrumb-label"] a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product);exit; 
        if($crawler->filter('#brandContainer')->count()>0){
            $product['brand']=$crawler->filter('#brandContainer')->text();
        }else{
            $product['brand']=array_slice($breadcrumbs,-1);
        }
        $product['sku'] = $crawler->filter('.SKUNumber')->text(); 
        if($crawler->filter('.DescSmall')->count()>0){
            $product['short_description']= $crawler->filter('.DescSmall')->text();
        }else{
            $product['short_description']=" ";
        }
        $product['description'] = $crawler->filter('.features p')->html();
        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][] =[$crawler->filter('#brandContainer')->text(),$crawler->filter('.SKUNumber')->text()];
        //图片
        $imgs=$crawler->filter('.productImageZoom')->each(function(Crawler $node,$i){
            return $node->attr('data-imagezoom');
        });
        foreach ($imgs as $image) {
            $images[] = [
                'src' => $image,
                'name' => $product['title'],
            ];
        }
        // $product['images']=$this->toEncryptImage($images);
        $product['images']=$images;
        // print_r($product);exit;
        // print_r($product['images']);exit;
        // if($crawler->filter('#productSelect-option-0')->count()>0){$product['variations'][]= [
        //         'name'=>'Size',
        //         'options' =>$crawler->filter('#productSelect-option-0 option')->each(function (Crawler $node, $i) {
        //             return $node->text();})];  
        // }else{$product['variations'][]=[
        //         'name'=>'',
        //         'options' =>""];
        // }
        // if ($product['variations'][0]['options']){
        //     $product['type'] = 'variable';
        // }
        $product['variations']=[];
        // $product['attributes']=[];
        $attr=[];
        if($crawler->filter("#tab-specs")->count()) {
            $count = $crawler->filter(".spec-body")->count();

            $crawler->filter('.spec-body')->each(function(Crawler $node,$i)use(&$attr){
                $attr[$i]=[
                    'name' =>$node->filter('div')->eq(1)->text(),
                    'options' =>[$node->filter('div')->eq(2)->text()],
                ];
            });
            $product['attributes']=$attr;
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
