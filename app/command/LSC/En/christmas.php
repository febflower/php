<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class christmas extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:christmas')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.sportinglife.ca/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.kingofchristmas.com/collections/artificial-christmas-wreaths-garlands',
        ];
        $array1=[
            'https://www.kingofchristmas.com/collections/artificial-christmas-trees',
            'https://www.kingofchristmas.com/collections/artificial-christmas-wreaths-garlands',
            'https://www.kingofchristmas.com/collections/christmas-accessories',

        ];
        $array2=[
            'https://www.sportinglife.ca/en-CA/women/clothing/coats-jackets/puffers/',
            'https://www.sportinglife.ca/en-CA/women/clothing/coats-jackets/ski-jackets/',
            'https://www.sportinglife.ca/en-CA/women/clothing/coats-jackets/snowboard-jackets/',
            'https://www.sportinglife.ca/en-CA/women/clothing/coats-jackets/mid-layer-jackets/',
        ];

        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'kchristmas001.xms011.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'sporting002.xms011.site/'],#..
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sportinglife.ca/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            $this->processPage($source);
        }
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.next-pagination a');
        if ($nextNode->count()) {
            if($nextNode->attr('href')!=null){
                $this->processPage('https://www.kingofchristmas.com'.$nextNode->attr('href'));
            }
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs__list li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-vendor')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('https://www.kingofchristmas.com'.$node->filter('a')->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.sportinglife.ca/en-CA/women/clothing/coats-jackets/winter-coats/womens-calla-coat-25434416.html?dwvar_25434416_color=015&dwvar_25434416_size=006&cgid=women-clothing-coats-jackets-winter-coats';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data = "";
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$data){
            if(strstr($node->text(),"Product")){
                $data=json_decode($node->text(),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$data['name']; 
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$data['offers'][0]['price'])));
        // print_r($product);exit;
        $product['brand']=$data['brand']['name'];
        $product['type'] = 'simple';
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['sku'] = $data['offers'][0]['sku']; 
        $product['short_description']= "";  
        if($crawler->filter('.tab-1')->count()){
            $product['description'] =$data['description'].PHP_EOL.$crawler->filter('.tab-1')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('.selected-value')->count()){
            $product['color'] = $crawler->filter('.selected-value')->text();
        }else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片  
        $img=[];
        $crawler->filter('[aria-label="Product images"] img')->each(function(Crawler $node)use(&$img){
            if($node->attr('src')!=null){
                $img[]='https:'.$node->attr('src');
            }else{
                $img[]='https:'.$node->attr('data-splide-lazy');
            }
        });
        // print_r($img);exit;
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        $product['variations'][]=[
        'name'=>"",
        'options' =>""];
        if($crawler->filter('.tab-3')->count()){
            $count=$crawler->filter('.tab-3')->filter('.dimension-detailed')->count();
            // echo $count;exit;
            for($i=0;$i<$count;$i++){
                 $product['attributes'][]= [
                'name'=>$crawler->filter('.spec_key')->eq($i)->text(),
                'options'=>[$crawler->filter('.spec_value')->eq($i)->text()],
                ];
            }
        }
        // print_r($product['attributes']);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
