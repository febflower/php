<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class totaltool extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:totaltool')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://food52.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.totaltools.com.au/construction-tools/cleaning',
            'https://www.totaltools.com.au/construction-tools/glues-sealants',
        ];
        $array1=[
            'https://www.totaltools.com.au/construction-tools/construction-equipment',
            'https://www.totaltools.com.au/construction-tools/dust-management-solutions',
            'https://www.totaltools.com.au/construction-tools/metal-machinery',
        ];
        $array2=[
            'https://www.totaltools.com.au/construction-tools/woodworking-tools',
            'https://www.totaltools.com.au/construction-tools/paints',
            'https://www.totaltools.com.au/construction-tools/marking-layout-tools',
        ];
        $array3=[
            'https://www.totaltools.com.au/safety/hand-protection',
            'https://www.totaltools.com.au/safety/protective-clothing',
        ];
        $array4=[
            'https://www.totaltools.com.au/tool-boxes-storage/hard-tool-storage',
            'https://www.totaltools.com.au/tool-boxes-storage/truck-ute-boxes',
            'https://www.totaltools.com.au/tool-boxes-storage/site-boxes',

        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'totaltool050.xms005.site/'],#..
            'x1' => ['source' => $array1, 'target' => 'totaltool051.xms005.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'totaltool053.xms005.site/'],#...
            'x3' => ['source' => $array3, 'target' => 'totaltool045.xms005.site/'],
            'x4' => ['source' => $array4, 'target' => 'totaltool047.xms005.site/'],
        ];
        // git stash #封存修改
        // git pull origin master 
        // git stash pop #把修改还原


        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://food52.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.subcategory-link')->count()){
                $crawler->filter('.subcategory-link')->each(function(Crawler $node1,$i){
                    $response = $this->guzzleHttpClient->request('GET', $node1->attr('href'));
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    if($crawler->filter('.subcategory-link')->count()){
                        $crawler->filter('.subcategory-link')->each(function(Crawler $node2,$i){
                            $response = $this->guzzleHttpClient->request('GET', $node2->attr('href'));
                            $contents = $response->getBody()->getContents();
                            $crawler = new Crawler($contents);
                            $this->processPage($node2->attr('href')); 
                        });
                    }else{
                        $this->processPage($node1->attr('href'));
                    }
                });
            }else{
                $this->processPage($source);
            }
    }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[title="Next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-item-link')->each(function (Crawler $node,$i) use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.page-title')->text();
        $product['price'] = str_replace(',','',str_replace(' €','',str_replace('$','',$crawler->filter('.price')->text())));
        $product['brand']=$crawler->filter('[property="product:brand"]')->attr('content');
        $product['type'] = 'simple';
        // $breadcrumbs=['222'];
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],2);
        // print_r($product['breadcrumbs']);exit;
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['sku'] = str_replace("..","",$crawler->filter('[itemprop="sku"]')->text());
        $product['short_description']= "";
        $product['description'] = $crawler->filter('.product-description')->html();
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][]=[];
        // print_r($product);exit;
        //图片   
        $arr = $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i){
            if(strstr($node->text(),'mage/gallery/gallery')){
                return $node->text();
            }      
        });
        foreach($arr as $arr1){
            if(strstr($arr1,'mage/gallery/gallery')){
                $data = $arr1;
            } 
        }
        $x=json_decode($data,true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
        for($i=0;$i<count($x);$i++){
            if(strstr($x[$i]['img'],'.jpg')){
                $images[$i]=[
                    'src'=>str_replace('?optimize=medium&bg-color=255,255,255&fit=bounds&height=615&width=615&canvas=615:615','',$x[$i]['img']),
                    'name'=>$product['title'],
                ];
            }elseif(strstr($x[$i]['img'],'.png')){
                $images[$i]=[
                    'src'=>str_replace('?format=jpeg','',$x[$i]['img']),
                    'name'=>$product['title'],
                ];
            }
        };
        // print_r($images);exit;
        $product['images']=$this->toEncryptImage($images);
        if($crawler->filter('#attribute686')->count()>0){
            $size=[];
            $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$size){
                if(strstr($node->text(),'#product_addtocart_form')){
                    $size = json_decode($node->text(),true)['#product_addtocart_form']['configurable']['spConfig']['attributes'][686]['options'];
                }      
            });
            for($i=0;$i<count($size);$i++){
                $option[$i]=$size[$i]['label'];
            }
            $product['variations'][]= [
                'name'=>'Size',
                'options' =>$option]; 
            $product['type'] = 'variable';
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }
        if($crawler->filter(".product-specification")->count()) {
            $count = $crawler->filter(".product-specification")->filter('tbody>tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-specification")->filter('tbody>tr>th')->eq($i)->text(),
                    'options' => [$crawler->filter(".product-specification")->filter('tbody>tr>td')->eq($i)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
