<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class betweenCom extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:betweenCom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://food52.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.betweenthelinesaz.com/bats/wood/',
            'https://www.betweenthelinesaz.com/bats/bbcor/',
            'https://www.betweenthelinesaz.com/bats/usssa/',
            'https://www.betweenthelinesaz.com/bats/wood-composite/',
            'https://www.betweenthelinesaz.com/bats/fastpitch-softball/',
            'https://www.betweenthelinesaz.com/bats/usa/',
            'https://www.betweenthelinesaz.com/bats/softball/',
            'https://www.betweenthelinesaz.com/bats/trainer/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/infield/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/outfield/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/first-base/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/pitcher/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/youth/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/training/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/catchers-mitt/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/softball/',
            'https://www.betweenthelinesaz.com/catchers-gear/softball/',
            'https://www.betweenthelinesaz.com/catchers-gear/accessories/',
        ];
        $array1=[
            'https://www.betweenthelinesaz.com/bats/',
            'https://www.betweenthelinesaz.com/gloves-and-mitts/',
            'https://www.betweenthelinesaz.com/catchers-gear/',
        ];
        $array2=[
            'https://www.betweenthelinesaz.com/apparel/',
            'https://www.betweenthelinesaz.com/bags/',
            'https://www.betweenthelinesaz.com/balls/',
            'https://www.betweenthelinesaz.com/protective-gear/',
            'https://www.betweenthelinesaz.com/accessories/',
        ];
        $array3=[
            'https://www.totaltools.com.au/catalog/category/view/s/tool-trolleys/id/1513/',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#...
            'x1' => ['source' => $array1, 'target' => 'between001.xms005.site/'],#...
            'x2' => ['source' => $array2, 'target' => 'between002.xms005.site/'],#采集失败，没有子分类
            'x3' => ['source' => $array3, 'target' => 'totaltool021.xms005.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://food52.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
    }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        // $breadcrumbs = array_filter($crawler->filter('.nav-breadcrumbs li')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.list-collection li')->filter('h3 a')->each(function (Crawler $node,$i){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'sku'=>sha1($node->text()),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data=[];
        $Organization=[];
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$data,&$Organization){
                $data = json_decode($node->text(),true)[0];  
                $Organization = json_decode($node->text(),true)[1]; 
        });
        $product['title'] =$crawler->filter('.list-product img')->attr('alt');
        $product['price'] = str_replace(',','',str_replace(' €','',str_replace('$','',$data['offers']['price'])));
        $product['brand']=$data['brand'];
        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.nav-breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs,1,-1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product);exit;
        $product['sku']= $item['sku'];
        if($crawler->filter('.tabs-a p')->count()){
            $product['short_description']=$crawler->filter('.tabs-a p')->text();
        }else{
            $product['short_description']=" ";
        }
        if($crawler->filter('.product-content')->count()){
            $product['description'] = $crawler->filter('.product-content')->filter('article')->html();
        }else{
            $product['description'] = " ";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('#product_configure_option_color option')->count()){
            $product['color'] = $crawler->filter('#product_configure_option_color option')->text();
        }else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        $product['tags'][]= $data['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $images = $crawler->filter('.list-product img')->each(function(Crawler $node,$i){
            return $node->attr('src');
        });
        foreach($images as $image){
            $product['images'][]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        };
        
        // $product['images']=$this->toEncryptImage($images);
        if($crawler->filter('#product_configure_option_color option')->count()>1){
            $product['variations'][]=[
                'name'=>'Color',
                'options'=>$crawler->filter('#product_configure_option_color option')->each(function(Crawler $node,$i){
                        return str_replace("”","",$node->text());
                }),
            ];
            $product['type'] = 'variable';
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }

        if($crawler->filter('#product_configure_option_size')->count()){
            $product['variations'][]=[
                'name'=>'Size',
                'options'=>$crawler->filter('#product_configure_option_size option')->each(function(Crawler $node,$i){
                        return str_replace("”","",$node->text());
                }),
            ];
            $product['type'] = 'variable';
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }  

        if($crawler->filter('.dmws-specs')->count()){
            $count = $crawler->filter(".dmws-specs")->filter('li')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' =>  $crawler->filter(".dmws-specs li")->eq($i)->filter('span')->text(),
                    'options' => [str_replace($crawler->filter(".dmws-specs li")->eq($i)->filter('span')->text(),"",$crawler->filter(".dmws-specs li")->eq($i)->text())],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
