<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class magicalears extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:magicalears')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.magicalearscollectibles.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            // 'https://www.hayneedle.com/seasonal/christmas-trees_list_187588?categoryID=187588&page=1&searchQuery=&selectedFacets=13%3D30249%2C145%3D1049725&sortBy=',
            'https://www.magicalearscollectibles.com/WomanApparel.html',
            // 'https://www.magicalearscollectibles.com/Headbands.html',
        ];
        $array1=[
            'https://www.magicalearscollectibles.com/Headbands.html',
            'https://www.magicalearscollectibles.com/Earhats.html',
            'https://www.magicalearscollectibles.com/Seasonalhats.html',
            'https://www.magicalearscollectibles.com/CharacterHats.html',
        ];
        $array2=[
            'https://www.magicalearscollectibles.com/baseballhats.html',
            'https://www.magicalearscollectibles.com/CharacterHats.html',
        ];
        $array3=[#
            'https://www.magicalearscollectibles.com/WomanApparel.html',
            'https://www.magicalearscollectibles.com/MenApparel.html',
            'https://www.magicalearscollectibles.com/UNISEX.html',
        ];
        $array4=[#
            'https://www.magicalearscollectibles.com/Children-Apparel.html',
            'https://www.magicalearscollectibles.com/Infant-Toddlers.html',
            'https://www.magicalearscollectibles.com/Apparel-Accessories.html',
        ];
        $array5=[
            'https://www.magicalearscollectibles.com/Harveys.html',
            'https://www.magicalearscollectibles.com/VeraBradley.html',
            'https://www.magicalearscollectibles.com/DisneyTotesandBags.html'
        ];
        $array6=[
            // 'https://www.magicalearscollectibles.com/DooneyBourke.html',
            // 'https://www.magicalearscollectibles.com/Loungefly.html',
            'https://www.magicalearscollectibles.com/Luggage.html',
            // 'https://www.magicalearscollectibles.com/BagTags.html',
            // 'https://www.magicalearscollectibles.com/Bag-Tags_c_31-3.html',
        ];
        $array7=[
            'https://www.magicalearscollectibles.com/Earrings.html',
            'https://www.magicalearscollectibles.com/JewelryBoxes.html',
        ];
        $array8=[
            'https://www.magicalearscollectibles.com/Bracelets.html',
            'https://www.magicalearscollectibles.com/Charms.html',
            'https://www.magicalearscollectibles.com/Rings.html'
        ];
        $array9=[
            // 'https://www.magicalearscollectibles.com/DisneyAlexandAni.html',
            // 'https://www.magicalearscollectibles.com/magicbands.html',
            'https://www.magicalearscollectibles.com/Magic-Bands_c_191-4.html',
        ];
        $array10=[
            'https://www.magicalearscollectibles.com/Watches.html',
            'https://www.magicalearscollectibles.com/pandoracharms.html'
        ];
        $array11=[
            'https://www.magicalearscollectibles.com/Necklaces.html',
            'https://www.magicalearscollectibles.com/Brooches.html'
        ];
        $array12=[#
            'https://www.magicalearscollectibles.com/Just-For-Lil-Princesses.html',
            'https://www.magicalearscollectibles.com/Just-For-Lil-Princes.html',
            'https://www.magicalearscollectibles.com/Shoes.html',
            'https://www.magicalearscollectibles.com/Socks.html',
            'https://www.magicalearscollectibles.com/Sunglasses.html'
        ];
        $array13=[
            'https://www.magicalearscollectibles.com/ActionFigures.html',
            'https://www.magicalearscollectibles.com/BabyandToddler.html',
            'https://www.magicalearscollectibles.com/GamesandPuzzles.html',
            'https://www.magicalearscollectibles.com/PlaySets.html',
        ];
        $array14=[
            'https://www.magicalearscollectibles.com/Dolls.html',
            'https://www.magicalearscollectibles.com/MrPotatoHead.html',
            'https://www.magicalearscollectibles.com/PretendPlay.html',
            'https://www.magicalearscollectibles.com/Transportation.html',
            'https://www.magicalearscollectibles.com/Baseballs.html',
            'https://www.magicalearscollectibles.com/Basketballs.html',
            'https://www.magicalearscollectibles.com/Footballs.html',
            'https://www.magicalearscollectibles.com/Gulf.html',
        ];
        $array15=[
            'https://www.magicalearscollectibles.com/PlushesPillows.html',
            'https://www.magicalearscollectibles.com/Banks.html',
        ];
        $array16=[
            'https://www.magicalearscollectibles.com/Treats.html',
            'https://www.magicalearscollectibles.com/Drinks.html',
            'https://www.magicalearscollectibles.com/Candy.html',
            'https://www.magicalearscollectibles.com/MainStreetPopcorn.html'
        ];
        $array17=[
            'https://www.magicalearscollectibles.com/ArtPrint.html',
            'https://www.magicalearscollectibles.com/BigFigures.html',
            'https://www.magicalearscollectibles.com/Coins.html',
            'https://www.magicalearscollectibles.com/GalleryOfLights.html',
            'https://www.magicalearscollectibles.com/PokitPal.html',
            'https://www.magicalearscollectibles.com/PreciousMoments.html',
        ];
        $array18=[
            'https://www.magicalearscollectibles.com/britto.html',
            'https://www.magicalearscollectibles.com/jimshore.html',
            'https://www.magicalearscollectibles.com/Figurines.html',
            'https://www.magicalearscollectibles.com/SnowGlobes.html',
            'https://www.magicalearscollectibles.com/Vinylmations.html'
        ];
        $array19=[
            // 'https://www.magicalearscollectibles.com/AllPins.html',
            'https://www.magicalearscollectibles.com/PinSets.html',
            'https://www.magicalearscollectibles.com/PinAccessories.html'
        ];
        $array20=[
            'https://www.magicalearscollectibles.com/HolidayApparel.html',
            'https://www.magicalearscollectibles.com/HolidayPin.html',
            'https://www.magicalearscollectibles.com/Stockings.html',
            'https://www.magicalearscollectibles.com/StockingStuffers.html'
        ];
        $array21=[
            'https://www.magicalearscollectibles.com/TreeSkirts.html',
            'https://www.magicalearscollectibles.com/holidaytreats.html',
            'https://www.magicalearscollectibles.com/MoreChristmasItems.html'
        ];
        $array22=[
            'https://www.magicalearscollectibles.com/BallOrnaments.html',
            'https://www.magicalearscollectibles.com/DiscOrnaments.html',
            'https://www.magicalearscollectibles.com/MickeyEarHatOrnaments.html',
            'https://www.magicalearscollectibles.com/MickeyIconOrnaments.html',
            'https://www.magicalearscollectibles.com/OrnamentSets.html',
            'https://www.magicalearscollectibles.com/ResortOrnaments.html'
        ];
        $array23=[
            'https://www.magicalearscollectibles.com/FigurineOrnaments.html',
            'https://www.magicalearscollectibles.com/HandbagOrnaments.html',
            'https://www.magicalearscollectibles.com/ShoeOrnaments.html'
        ];
        $array24=[
            'https://www.magicalearscollectibles.com/halloweenApparel.html',
            'https://www.magicalearscollectibles.com/HalloweenDecorations.html',
            'https://www.magicalearscollectibles.com/HalloweenPins.html',
            'https://www.magicalearscollectibles.com/HalloweenCandy.html'
        ];
        $array25=[
            'https://www.magicalearscollectibles.com/HalloweenDecorations.html',
            // 'https://www.magicalearscollectibles.com/ValentinesDay.html',
            // 'https://www.magicalearscollectibles.com/St.PatricksDay.html',
            // 'https://www.magicalearscollectibles.com/Easter.html',
            // 'https://www.magicalearscollectibles.com/MothersDay.html',
            // 'https://www.magicalearscollectibles.com/FathersDay.html',
            // 'https://www.magicalearscollectibles.com/IndependenceDay.html',
            // 'https://www.magicalearscollectibles.com/Thanksgiving.html',
            // 'https://www.magicalearscollectibles.com/Hanukkah.html',
            // 'https://www.magicalearscollectibles.com/DisneyWedding.html'
        ];
        $array26=[
            'https://www.magicalearscollectibles.com/WindowClings.html',
            'https://www.magicalearscollectibles.com/LicensePlates.html',
            'https://www.magicalearscollectibles.com/KeyChains.html',
            'https://www.magicalearscollectibles.com/AntennaToppers.html',
            'https://www.magicalearscollectibles.com/AutoMagnets.html'
        ];
        $array27=[
            'https://www.magicalearscollectibles.com/BathBombs.html',
            'https://www.magicalearscollectibles.com/FreshCutSoap.html',
            'https://www.magicalearscollectibles.com/BeachTowels.html',
            'https://www.magicalearscollectibles.com/iPhonecases.html',
            'https://www.magicalearscollectibles.com/Accessories.html'
        ];
        $array28=[
            'https://www.magicalearscollectibles.com/MoreDecor.html',
            'https://www.magicalearscollectibles.com/CollarsLeashes.html',
            'https://www.magicalearscollectibles.com/PetToys.html',
            'https://www.magicalearscollectibles.com/PetAccessories.html',
        ];
        $array29=[
            'https://www.magicalearscollectibles.com/PillowsThrows.html',
            'https://www.magicalearscollectibles.com/SouvenirsandGadgets.html',
            'https://www.magicalearscollectibles.com/DVD.html',
            'https://www.magicalearscollectibles.com/CD.html',
            'https://www.magicalearscollectibles.com/Books.html',
            'https://www.magicalearscollectibles.com/H2O+.html',
            'https://www.magicalearscollectibles.com/IntheGarden.html',
            'https://www.magicalearscollectibles.com/Clocks.html',
        ];
        $array30=[
            'https://www.magicalearscollectibles.com/PhotoAlbums.html',
            'https://www.magicalearscollectibles.com/PhotoFrames.html',
            'https://www.magicalearscollectibles.com/ScrapbookingSupplies.html',
            'https://www.magicalearscollectibles.com/autographbooks.html',
            'https://www.magicalearscollectibles.com/penspencils.html',
            'https://www.magicalearscollectibles.com/journalsnotebooksmore.html',
            'https://www.magicalearscollectibles.com/postcards.html'
        ];
        $array31=[
            'https://www.magicalearscollectibles.com/Mugs.html',
            'https://www.magicalearscollectibles.com/Glassware.html',
            'https://www.magicalearscollectibles.com/Plasticware.html'
        ];
        $array32=[
            'https://www.magicalearscollectibles.com/Mugs_c_32-8-4.html',
            'https://www.magicalearscollectibles.com/Glassware_c_33-5-4.html',
            'https://www.magicalearscollectibles.com/Plasticware_c_34-5-4.html',
        ];
        $array33=[
            'https://www.magicalearscollectibles.com/KitchenMagnets.html',
            'https://www.magicalearscollectibles.com/KitchenAccessories.html'
        ];
        $array34=[];
        $array35=[];
        $array36=[];
        $array37=[];
        $array38=[];
        $array39=[];
        $array40=[];

        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'magical001.xms011.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'magical002.xms011.site/'],
            'x3' => ['source' => $array3, 'target' => 'magical003.xms011.site/'],
            'x4' => ['source' => $array4, 'target' => 'magical004.xms011.site/'],
            'x5' => ['source' => $array5, 'target' => 'magical005.xms011.site/'],
            'x6' => ['source' => $array6, 'target' => 'magical006.xms011.site/'],
            'x7' => ['source' => $array7, 'target' => 'magical007.xms011.site/'],
            'x8' => ['source' => $array8, 'target' => 'magical008.xms011.site/'],
            'x9' => ['source' => $array9, 'target' => 'magical009.xms011.site/'],
            'x10' => ['source' => $array10, 'target' => 'magical010.xms011.site/'],
            'x11' => ['source' => $array11, 'target' => 'magical011.xms011.site/'],
            'x12' => ['source' => $array12, 'target' => 'magical012.xms011.site/'],
            'x13' => ['source' => $array13, 'target' => 'magical013.xms011.site/'],
            'x14' => ['source' => $array14, 'target' => 'magical014.xms011.site/'],
            'x15' => ['source' => $array15, 'target' => 'magical015.xms011.site/'],
            'x16' => ['source' => $array16, 'target' => 'magical016.xms011.site/'],
            'x17' => ['source' => $array17, 'target' => 'magical017.xms011.site/'],
            'x18' => ['source' => $array18, 'target' => 'magical018.xms011.site/'],
            'x19' => ['source' => $array19, 'target' => 'magical019.xms011.site/'],
            'x20' => ['source' => $array20, 'target' => 'magical020.xms011.site/'],
            'x21' => ['source' => $array21, 'target' => 'magical021.xms011.site/'],
            'x22' => ['source' => $array22, 'target' => 'magical022.xms011.site/'],
            'x23' => ['source' => $array23, 'target' => 'magical023.xms011.site/'],
            'x24' => ['source' => $array24, 'target' => 'magical024.xms011.site/'],
            'x25' => ['source' => $array25, 'target' => 'magical025.xms011.site/'],
            'x26' => ['source' => $array26, 'target' => 'magical026.xms011.site/'],
            'x27' => ['source' => $array27, 'target' => 'magical027.xms011.site/'],
            'x28' => ['source' => $array28, 'target' => 'magical028.xms011.site/'],
            'x29' => ['source' => $array29, 'target' => 'magical029.xms011.site/'],
            'x30' => ['source' => $array30, 'target' => 'magical030.xms011.site/'],
            'x31' => ['source' => $array31, 'target' => 'magical031.xms011.site/'],
            'x32' => ['source' => $array32, 'target' => 'magical032.xms011.site/'],
            'x33' => ['source' => $array33, 'target' => 'magical033.xms011.site/'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.magicalearscollectibles.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
                $this->processPage($source);
        }
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumnb li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.name a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('https://www.magicalearscollectibles.com/'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.magicalearscollectibles.com/Disney-Luggage-Bag-Tag--Minnie-Mouse--Bow_p_3867.html';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.page_headers')->text(); 
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('#price')->text())));
        if($crawler->filter('[itemprop="brand"]')->attr('content')!=null){
            $product['brand']=$crawler->filter('[itemprop="brand"]')->attr('content');
        }else{
            $product['brand']="Disney";
        }
        
        $product['type'] = 'simple';
       
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text(); 
        $product['short_description']= "";
        if($crawler->filter('[itemprop="description"]')->count()){
            $product['description'] =$crawler->filter('[itemprop="description"]')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        // $product['tags'][]= $product['brand'];
        $product['tags'] = "";
        //图片   
        $img=[];
        $crawler->filter('#main-image a')->each(function(Crawler $node)use(&$img){
            $img[] = 'https://www.magicalearscollectibles.com/'.$node->attr('href');
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        // if($crawler->filter('.opt-regular')->count()){
        //     $product['variations'][]=[
        //         'name'=>"Size",
        //         'options' =>$crawler->filter('.radio-format')->each(function(Crawler $node){
        //             return $node->text();
        //         })
        //     ];
        //     $product['type'] = 'variable';
        // }else{
        //     $product['variations'][]=[
        //     'name'=>"",
        //     'options' =>""];
        // }
        $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        $product['attributes'] = [];
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
