<?php
namespace app\command\LSC\En;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class xdcdepot extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:xdcdepot')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.sportinglife.ca/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/christmas-greenery/'
        ];
        $array1=[
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/christmas-greenery/',
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/christmas-lights/',
        ];
        $array2=[
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/christmas-tree-decorations/',
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/christmas-trees/',
        ];
        $array3=[
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/indoor-christmas-decorations/',
        ];
        $array4=[
            'https://xdcdepot.com/product-category/holiday-decorations/christmas-decorations/outdoor-christmas-decorations/',
        ];
        $array5=[
            'https://xdcdepot.com/product-category/holiday-decorations/halloween-decorations/',
        ];
        $array11=[
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/artificial-flowers/',
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/artificial-garlands/',
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/artificial-hedges/',
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/artificial-plants/',
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/artificial-topiaries/',
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/artificial-trees/',
            'https://xdcdepot.com/product-category/home-decor/artificial-greenery/decorative-wreaths/',
        ];
        $array12=[
            'https://xdcdepot.com/product-category/home-decor/clocks/table-clocks/',
            'https://xdcdepot.com/product-category/home-decor/clocks/wall-clocks/',
            'https://xdcdepot.com/product-category/home-decor/mirrors/floor-mirrors/',
            'https://xdcdepot.com/product-category/home-decor/mirrors/wall-mirrors/',
        ];
        $array13=[
            'https://xdcdepot.com/product-category/home-decor/fragrance-and-candles/candle-holders/',
            'https://xdcdepot.com/product-category/home-decor/fragrance-and-candles/candles/',
            'https://xdcdepot.com/product-category/home-decor/fragrance-and-candles/flameless-candles/',
        ];
        $array14=[
            'https://xdcdepot.com/product-category/home-decor/home-accents/decorative-bottles/',
            'https://xdcdepot.com/product-category/home-decor/home-accents/decorative-trays/',
            'https://xdcdepot.com/product-category/home-decor/home-accents/globes/',
            'https://xdcdepot.com/product-category/home-decor/home-accents/jewelry-boxes/',
            'https://xdcdepot.com/product-category/home-decor/home-accents/sculptures/',
            'https://xdcdepot.com/product-category/home-decor/home-accents/storage-baskets/',
            'https://xdcdepot.com/product-category/home-decor/home-accents/vases/'
        ];
        $array15=[
            'https://xdcdepot.com/product-category/home-decor/picture-frames/',
            'https://xdcdepot.com/product-category/home-decor/throw-blankets/',
            'https://xdcdepot.com/product-category/home-decor/throw-pillows/',
            'https://xdcdepot.com/product-category/home-decor/wall-decor/decorative-letters/',
            'https://xdcdepot.com/product-category/home-decor/wall-decor/memo-boards/',
            'https://xdcdepot.com/product-category/home-decor/wall-decor/wall-accents/',
            'https://xdcdepot.com/product-category/home-decor/wall-decor/wall-signs/',
        ];
        $array16=[
            'https://xdcdepot.com/product-category/kitchen/kitchen-storage-and-organization/countertop-storage/',
            'https://xdcdepot.com/product-category/kitchen/kitchen-storage-and-organization/food-storage/',
            'https://xdcdepot.com/product-category/kitchen/kitchen-storage-and-organization/kitchen-drawer-organizers/',
        ];
        $array17=[
            'https://xdcdepot.com/product-category/kitchen/water-filters/replacement-water-filters/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/showerhead-filters/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/water-dispensers/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/water-filter-parts/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/water-filter-pitchers/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/water-filtration-systems/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/water-softeners/',
            'https://xdcdepot.com/product-category/kitchen/water-filters/water-testing-kits/',
        ];
        $array18=[
            'https://xdcdepot.com/product-category/kitchen/tableware-and-bar/bar-accessories/',
            'https://xdcdepot.com/product-category/kitchen/tableware-and-bar/dinnerware/',
            'https://xdcdepot.com/product-category/kitchen/tableware-and-bar/disposable-tableware/',
            'https://xdcdepot.com/product-category/kitchen/tableware-and-bar/table-linens-and-kitchen-linens/',
        ];
        $array19=[
            'https://xdcdepot.com/product-category/kitchen/tableware-and-bar/flatware/',
            'https://xdcdepot.com/product-category/kitchen/tableware-and-bar/drinkware/',
        ];
        $array23=[
            'https://xdcdepot.com/product-category/kitchen/kitchenware/cutlery/',
            'https://xdcdepot.com/product-category/kitchen/kitchenware/kitchen-gadgets-tools/',
            'https://xdcdepot.com/product-category/kitchen/kitchenware/kitchen-utensils/',
        ];
        $array24=[
            'https://xdcdepot.com/product-category/outdoors/garden-center/garden-tools/',
            'https://xdcdepot.com/product-category/outdoors/garden-center/outdoor-decor/',
            'https://xdcdepot.com/product-category/outdoors/outdoor-cooking/grill-accessories/',
            'https://xdcdepot.com/product-category/outdoors/outdoor-cooking/outdoor-cookers/',
            'https://xdcdepot.com/product-category/outdoors/outdoor-power-equipment/lawn-mowers/',
            'https://xdcdepot.com/product-category/outdoors/outdoor-power-equipment/leaf-blowers/',
            'https://xdcdepot.com/product-category/outdoors/outdoor-power-equipment/riding-mower-and-tractor-attachments/',
            'https://xdcdepot.com/product-category/outdoors/outdoor-power-equipment/trimmers/',
        ];
        $array25=[
            'https://xdcdepot.com/product-category/tools/air-compressor-tools/',
            'https://xdcdepot.com/product-category/tools/flashlights/flashlight-bulbs/',
            'https://xdcdepot.com/product-category/tools/flashlights/handheld-flashlights/',
            'https://xdcdepot.com/product-category/tools/flashlights/handheld-spotlights/',
            'https://xdcdepot.com/product-category/tools/flashlights/hands-free-flashlights/',
            'https://xdcdepot.com/product-category/tools/flashlights/headlamps/',
            'https://xdcdepot.com/product-category/tools/flashlights/lanterns/',
            'https://xdcdepot.com/product-category/tools/flashlights/pen-flashlights/',
            'https://xdcdepot.com/product-category/tools/shop-vacuums/wet-dry-vacuum-accessories/',
            'https://xdcdepot.com/product-category/tools/shop-vacuums/wet-dry-vacuums/',
        ];
        $array27=[
            'https://xdcdepot.com/product-category/tools/hand-tools/cutting-tools/',
            'https://xdcdepot.com/product-category/tools/hand-tools/fastening-tools/',
        ];
        $array28=[
            'https://xdcdepot.com/product-category/tools/hand-tools/chisels-files-punches/',
            'https://xdcdepot.com/product-category/tools/hand-tools/hammers/',
            'https://xdcdepot.com/product-category/tools/hand-tools/knives-blades/'
        ];
        $array31=[
            'https://xdcdepot.com/product-category/tools/hand-tools/screwdrivers-nut-drivers/',
            'https://xdcdepot.com/product-category/tools/hand-tools/specialty-hand-tools/',
            'https://xdcdepot.com/product-category/tools/hand-tools/stud-finders/',
            'https://xdcdepot.com/product-category/tools/hand-tools/hex-keys/',
            'https://xdcdepot.com/product-category/tools/hand-tools/tool-accessories/',
        ];
        $array41=[
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/oscillating-tool-attachments/',
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/drill-attachments/'
        ];
        $array42=[
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/power-tool-batteries/',
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/power-tool-battery-chargers/'
        ];
        $array44=[
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/saw-accessories/',
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/saw-tracks/',
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/specialty-power-tool-accessories/',
            'https://xdcdepot.com/product-category/tools/power-tool-accessories/sharpening-tools/'
        ];
        $array51=[
            'https://xdcdepot.com/product-category/tools/power-tools/cordless-ratchets/',
            'https://xdcdepot.com/product-category/tools/power-tools/grinders/',
            'https://xdcdepot.com/product-category/tools/power-tools/jobsite/'
        ];
        $array52=[
            'https://xdcdepot.com/product-category/tools/power-tools/impact-wrenches/',
            'https://xdcdepot.com/product-category/tools/power-tools/metalworking-tools/',
            'https://xdcdepot.com/product-category/tools/power-tools/power-cutting-tools/',
            'https://xdcdepot.com/product-category/tools/power-tools/power-crimpers/',
            'https://xdcdepot.com/product-category/tools/power-tools/polishers/'
        ];
        $array53=[
            'https://xdcdepot.com/product-category/tools/power-tools/power-multi-tools/',
            'https://xdcdepot.com/product-category/tools/power-tools/sanders/',
            'https://xdcdepot.com/product-category/tools/power-tools/metalworking-tools/'
        ];
        $array54=[
            'https://xdcdepot.com/product-category/tools/power-tools/power-tool-combo-kits/',
            'https://xdcdepot.com/product-category/tools/power-tools/specialty-power-tools/',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'xdcdepot001.seo077.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'xdcdepot002.seo077.site/'],
            'x3' => ['source' => $array3, 'target' => 'xdcdepot003.seo077.site/'],
            'x4' => ['source' => $array4, 'target' => 'xdcdepot004.seo077.site/'],
            'x11' => ['source' => $array11, 'target' => 'xdcdepot011.seo078.site/'],
            'x12' => ['source' => $array12, 'target' => 'xdcdepot012.seo078.site/'],
            'x13' => ['source' => $array13, 'target' => 'xdcdepot013.seo078.site/'],
            'x14' => ['source' => $array14, 'target' => 'xdcdepot014.seo078.site/'],
            'x15' => ['source' => $array15, 'target' => 'xdcdepot015.seo078.site/'],
            'x16' => ['source' => $array16, 'target' => 'xdcdepot016.seo078.site/'],
            'x17' => ['source' => $array17, 'target' => 'xdcdepot017.seo078.site/'],
            'x18' => ['source' => $array18, 'target' => 'xdcdepot018.seo078.site/'],
            'x19' => ['source' => $array19, 'target' => 'xdcdepot019.seo078.site/'],
            'x23' => ['source' => $array23, 'target' => 'xdcdepot023.seo078.site/'],
            'x24' => ['source' => $array24, 'target' => 'xdcdepot024.seo078.site/'],
            'x25' => ['source' => $array25, 'target' => 'xdcdepot025.seo078.site/'],
            'x27' => ['source' => $array27, 'target' => 'xdcdepot027.seo078.site/'],
            'x28' => ['source' => $array28, 'target' => 'xdcdepot028.seo078.site/'],
            'x31' => ['source' => $array31, 'target' => 'xdcdepot031.seo079.site/'],
            'x41' => ['source' => $array41, 'target' => 'xdcdepot041.seo079.site/'],
            'x42' => ['source' => $array42, 'target' => 'xdcdepot042.seo079.site/'],
            'x44' => ['source' => $array44, 'target' => 'xdcdepot044.seo079.site/'],
            'x51' => ['source' => $array51, 'target' => 'xdcdepot051.seo079.site/'],
            'x52' => ['source' => $array52, 'target' => 'xdcdepot052.seo079.site/'],
            'x53' => ['source' => $array53, 'target' => 'xdcdepot053.seo079.site/'],
            'x54' => ['source' => $array54, 'target' => 'xdcdepot054.seo079.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sportinglife.ca/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('#category-level-1')->count()){
                $crawler->filter('#category-level-1 a')->each(function(Crawler $node){
                    $this->processPage($node->attr('href'));
                });
            }else{
                $this->processPage($source);
            }
        }
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.next');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        // $breadcrumbs = array_filter($crawler->filter('.woocommerce-breadcrumb a')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.woocommerce-loop-product__link')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    #'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.sportinglife.ca/en-CA/women/clothing/coats-jackets/winter-coats/womens-calla-coat-25434416.html?dwvar_25434416_color=015&dwvar_25434416_size=006&cgid=women-clothing-coats-jackets-winter-coats';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data = "";
        $crawler->filter('#woocommerce_gpf_schema')->each(function(Crawler $node,$i)use(&$data){
            if(strstr($node->text(),"Product")){
                $data=json_decode($node->text(),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$crawler->filter('.product-title')->text(); 
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('.product-page-price')->text())));
        $product['brand']=$crawler->filter('[rel="tag"]')->text();
        $product['type'] = 'simple';
       
        $breadcrumbs = array_filter($crawler->filter('.woocommerce-breadcrumb a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] = $crawler->filter('.sku')->text(); 
        if($crawler->filter('.product-short-description')->count()){
            $product['short_description']= $crawler->filter('.product-short-description')->text(); 
        }else{
            $product['short_description']= "";
        }
        
        if($crawler->filter('.tab-panels')->count()){
            $product['description'] =$crawler->filter('#tab-description')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=[];
        $crawler->filter('.skip-lazy')->each(function(Crawler $node)use(&$img){
            $img[] = $node->attr('data-large_image');
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        // $product['images']=$images;
        $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);
        $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        if($crawler->filter('.woocommerce-product-attributes')->count()){
            $count=$crawler->filter('.woocommerce-product-attributes tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][]= [
                    'name'=>$crawler->filter('.woocommerce-product-attributes tr')->eq($i)->filter('th')->text(),
                    'options'=>[$crawler->filter('.woocommerce-product-attributes tr')->eq($i)->filter('td')->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
