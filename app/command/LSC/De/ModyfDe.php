<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ModyfDe extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:ModyfDe')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.modyf.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.modyf.de/arbeitskleidung',
            'https://www.modyf.de/sicherheitsschuhe',
            'https://www.modyf.de/winter-wetterschutz',
            'https://www.modyf.de/zubehoer'
        ];
        $array1=[
            'https://www.modyf.de/arbeitskleidung',
        ];
        $array2=[
            'https://www.modyf.de/sicherheitsschuhe',
            'https://www.modyf.de/winter-wetterschutz',
            'https://www.modyf.de/zubehoer',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],
            'x1' => ['source' => $array1, 'target' => 'modyf001.xms002.site/'],
            'x2' => ['source' => $array2, 'target' => 'modyf002.xms002.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.modyf.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.leftnav-sub-categories a')->count()){
                $crawler->filter('.leftnav-sub-categories a')->each(function(Crawler $node,$i){
                    $this->processPage($node->attr('href'));
                });
            }else{
                $this->processPage($source);
            }
        }
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        $crawler->filter(".product-item-details")->filter('.product-item-link')->each(function (Crawler $node, $i) use ($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.page-title-wrapper')->text();
        $product['price'] = (int)trim((str_replace(',',".",str_replace('€',"",$crawler->filter('.price')->text()))));
        $product['brand']= $crawler->filter('.logo')->attr('title');
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['sku'] =$crawler->filter('.display-id')->text();
        // print_r( $product);exit;
        $product['short_description']= str_replace($crawler->filter('.read-more')->text(),"",$crawler->filter('.short-description')->text());

        if($crawler->filter('.main-features-wrapper')->count()){
            $product['description'] =$crawler->filter('.value')->text().PHP_EOL.$crawler->filter('.main-features')->html().PHP_EOL.$crawler->filter('.technical-block-wrapper')->html();
        }else{
            $product['description'] = $crawler->filter('.value')->text().PHP_EOL.$crawler->filter('.technical-block-wrapper')->html();
        }
        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        // $product['color'] = $crawler->filter('.product-info-main-top-options')->filter('strong')->text();
        // print_r($product['color']);exit;
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        if($crawler->filter('.product-options-wrapper')->count()){
            $data=[];
            $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$data){
                if(strstr($node->text(),'"configurable":')){
                    $data = json_decode($node->text(),true)['#product_addtocart_form']['configurable']['spConfig']['attributes']['477']['options'];
                }
            });
            if(count($data)>1){
                for($i=0;$i<count($data);$i++){
                    $size[$i] = $data[$i]['label'];
                }
                $product['variations'][]= [
                    'name'=>"Größe",
                    'options' =>$size,
                ];
                $product['type'] = 'variable';
            }else{
                $product['variations'][]=[
                    'name'=>'',
                    'options' =>""];}   
        }else{
            $product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        $img = [];
        $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$img){
            if(strstr($node->text(),'mage/gallery/gallery')){
                $img = json_decode($node->text(),true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
            }      
        });
        for($i=0;$i<count($img);$i++){
            $images[$i]=[
                'src'=>$img[$i]['img'],
                'name'=>$product['title'],
            ];
        };
        // $product['images']=$images;
        $product['images']=$this->toEncryptImage($images);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}