<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class markenDe extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:markenDe')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.christ.de/category/swarovski/index.html');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.christ.de/category/swarovski-uhren/index.html',
            'https://www.christ.de/category/swarovski-accessoires/index.html',
        ];
        $array1=[
          'https://www.christ.de/category/special-offer-sale/f/swarovski-sale/index.html',
          'https://www.christ.de/category/skagen-nachhaltige-uhren/index.html',
        ];
        $array2=[
            'https://www.christ.de/category/swarovski-ringe/index.html',
            'https://www.christ.de/category/swarovski-armbaender/index.html',
            'https://www.christ.de/category/swarovski-ketten/index.html',
            'https://www.christ.de/category/swarovski-ohrringe/index.html',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'christde006.seo067.site/',],
            'x1' => ['source' => $array1, 'target' => 'christde006.seo067.site/',],
            'x2' => ['source' => $array2, 'target' => 'christde006.seo067.site/',],
            ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.christ.de/category/swarovski/index.html',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_6fd2c45ea9c5d1b526bf297b3cc8453b16c0a71f',
        // 'cs_3ebdee7665f66d78067468a0929967d4381fc08b',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        // foreach ($sources as $source) {
        //     $response = $this->guzzleHttpClient->request('GET', $source);
        //     $contents = $response->getBody()->getContents();
        //     $crawler = new Crawler($contents);
        //     $crawler->filter('.top-filter__item')->each(function(Crawler $node,$i){
        //         $url ='https://www.christ.de'.$node->attr('href');
        //         $this->processPage($url);
        //         echo 2;
        //     });
        // }
        foreach ($sources as $source) {
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.christ.de/product/87928179/engelsrufer-kette-ern-lilangel-g/index.html');
    }


    protected function processPage($url)
    {
        
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
        $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.category-banner__headline')->each(function (Crawler $node) {
            return $node->text();
        }));
        // $breadcrumbs = ['Casio Collection'];
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-tile__link')->each(function (Crawler $node, $i) use ($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s','https://www.christ.de'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            sleep(3);
            flush();
        });
    }


    protected function crawlerProduct($item)
    {

        // print_r($item['name']);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        if($crawler->filter('.color-tiles__color span')->count()>0){
            $product['title'] = $crawler->filter('.product-details__headline')->text()." ".$crawler->filter('.color-tiles__color span')->text()." ".$crawler->filter('.product-details-lists__definition')->eq(2)->text();
        }else{
            $product['title'] = $crawler->filter('.product-details__headline')->text()." ".$crawler->filter('.product-details-lists__definition')->eq(2)->text();
        }
        // print_r($product);exit;
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr('content');
        $product['brand']=" ";
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],0);
        // $product['breadcrumbs'] = array_slice($breadcrumbs,0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] =$crawler->filter('[itemprop="sku"]')->attr('content'); 
        $product['short_description']= "";
        $product['description'] = $crawler->filter('.maintain-list')->html();
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][] =[];
        //选项
        // if($crawler->filter('.accordion__item-header')->count()>0){$product['variations'][]= [
        //     'name'=>$crawler->filter('.accordion__item-header span')->text(),
        //     'options' =>$crawler->filter('.custom-control-label')->each(function (Crawler $node, $i) {
        //         return $node->text();})]; 
        //     $product['type'] = 'variable'; 
        // }else{$product['variations'][]=[
        //     'name'=>'',
        //     'options' =>""];
        // }
        if($crawler->filter('.color-tiles__list>li')->count()>0){$product['variations'][]= [
                'name'=>'Farbe',
                'options' =>$crawler->filter('.color-tiles__list>li')->each(function (Crawler $node, $i) {
                    return $node->attr('data-color');})]; 
            $product['type'] = 'variable'; 
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }

        if($crawler->filter('.size-tiles__tile')->count()>0){$product['variations'][]= [
                'name'=>'Größe',
                'options' =>$crawler->filter('.size-tiles__tile')->each(function (Crawler $node, $i) {
                    return $node->text();})]; 
            $product['type'] = 'variable'; 
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }

        //图片
        $images=$crawler->filter('.product-stage__hover-zoom-image')->each(function(Crawler $node,$i){
            return $node->attr('src').PHP_EOL;
        });
        foreach ($images as $image) {
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
        }
        // print_r($product);exit;
        if($crawler->filter(".product-details-lists__title")->count()) {
            $count = $crawler->filter(".product-details-lists__title")->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-details-lists__title")->eq($i)->text(),
                    'options' => $crawler->filter(".product-details-lists__definition")->eq($i)->text(),
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }

        echo "\r\n";
        // echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}