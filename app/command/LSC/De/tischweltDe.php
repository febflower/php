<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class tischweltDe extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:tischweltDe')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.tischwelt.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.tischwelt.de/shop/Geschirr/Teller/Suppenteller/',
        ];
        $array1=[
            'https://www.tischwelt.de/shop/Geschirr/Teller/Speiseteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Suppenteller/',
        ];
        $array2=[
            'https://www.tischwelt.de/shop/Geschirr/Teller/Pastateller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Fruehstuecksteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Brotteller/',
        ];
        $array3=[
            'https://www.tischwelt.de/shop/Geschirr/Teller/Platzteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Gourmetteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Dessertteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Pizzateller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Salatteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Kuchenteller/',
            'https://www.tischwelt.de/shop/Geschirr/Teller/Glasteller/',
        ];
        $array4=[
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Teetassen/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Kaffeetassen/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Cappuccinotassen/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Espressotassen/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Milchkaffeetassen/',
        ];
        $array5=[
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Becher/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Suppentassen/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Jumbotassen/',
            'https://www.tischwelt.de/shop/Geschirr/Tassen/Weihnachtstassen/',
        ];
        $array6=[
            // 'https://www.tischwelt.de/shop/Geschirr/Untertassen/',
            'https://www.tischwelt.de/shop/Geschirr/Kannen/',
        ];
        $array7=[
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Schalen/',
        ];
        $array8=[
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Schuesseln/',
        ];
        $array9=[
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Servierplatten/',
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Tortenplatten/',
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Etagere/',
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Tablett/',
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Sauciere/',
            'https://www.tischwelt.de/shop/Geschirr/Serviergeschirr/Stoevchen/',
        ];
        $array10=[
            'https://www.tischwelt.de/shop/Geschirr/Tischaccessoires/',
        ];
        $array11=[
            'https://www.tischwelt.de/shop/Geschirr/Stile/Ostern/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Bunt/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Skandi/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Landhaus/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Modern/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Retro/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Winter/',
        ];
        $array12=[
            'https://www.tischwelt.de/shop/Geschirr/Stile/Elegant/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Fruehling/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Sommer/',
            'https://www.tischwelt.de/shop/Geschirr/Stile/Herbst/',
        ];
        $array13=[
            'https://www.tischwelt.de/shop/Besteck/Bestecksets/',
            'https://www.tischwelt.de/shop/Besteck/Loeffel/',
        ];
        $array14=[
            'https://www.tischwelt.de/shop/Besteck/Gabeln/',
            'https://www.tischwelt.de/shop/Besteck/Messer/',
            'https://www.tischwelt.de/shop/Besteck/Besteckaufbewahrung/',
        ];
        $array15=[
            'https://www.tischwelt.de/shop/Besteck/Silberbesteck/',
        ];
        $array16=[
            'https://www.tischwelt.de/shop/Glaeser/Weinglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Wasserglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Bierglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Champagnerglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Saftglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Glaeser-fuer-Hochprozentiges/',
            'https://www.tischwelt.de/shop/Glaeser/Sektglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Kaffeeglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Teeglaeser/',
            'https://www.tischwelt.de/shop/Glaeser/Kristallglaeser/',
        ];
        $array17=[
            'https://www.tischwelt.de/shop/Kochen-Braten-Backen/Backen/',
        ];
        $array18=[
            'https://www.tischwelt.de/shop/Kochen-Braten-Backen/Kochen-Braten/',
        ];
        $array19=[
            'https://www.tischwelt.de/shop/Kochen-Braten-Backen/Elektrische-Kuechengeraete/',
            'https://www.tischwelt.de/shop/Kochen-Braten-Backen/Erlebniskochen/',
        ];
        $array20=[
            'https://www.tischwelt.de/shop/Schneiden-vorbereiten/Kuechenmesser/',
            'https://www.tischwelt.de/shop/Schneiden-vorbereiten/Aufbewahren/',
        ];
        $array21=[
            'https://www.tischwelt.de/shop/Schneiden-vorbereiten/Kochbuecher/',
            'https://www.tischwelt.de/shop/Schneiden-vorbereiten/Schneidebretter/',
            'https://www.tischwelt.de/shop/Schneiden-vorbereiten/Ordnungshelfer/',
        ];
        $array22=[
            'https://www.tischwelt.de/shop/Schneiden-vorbereiten/Kuechenhelfer/',
        ];
        $array23=[
            'https://www.tischwelt.de/shop/Themen/Kaffeezubehoer/',
            'https://www.tischwelt.de/shop/Themen/Unterwegs/',
            'https://www.tischwelt.de/shop/Themen/Tee/',
        ];
        $array24=[
            'https://www.tischwelt.de/shop/Themen/Kinder/',
            'https://www.tischwelt.de/shop/Themen/Kuschelige-Winterzeit/',
            'https://www.tischwelt.de/shop/Themen/Wohnen/',
        ];
        $array25=[
            'https://www.tischwelt.de/shop/Themen/Kerzen/',
            'https://www.tischwelt.de/shop/Themen/Einmachen/',
            'https://www.tischwelt.de/shop/Themen/Silvesterparty/',
            'https://www.tischwelt.de/shop/Themen/Nachhaltigkeit/',
            'https://www.tischwelt.de/shop/Themen/Spiele/',
            'https://www.tischwelt.de/shop/Themen/Smoothies-Saefte/',
            'https://www.tischwelt.de/shop/Themen/Eier/',
            'https://www.tischwelt.de/shop/Themen/Bar/',
            'https://www.tischwelt.de/shop/Themen/Vatertag/',
        ];
        $array26=[
            'https://www.tischwelt.de/shop/Themen/Muttertag/',
            'https://www.tischwelt.de/shop/Themen/Ostern/',
            'https://www.tischwelt.de/shop/Themen/Kraeutergarten/',
            'https://www.tischwelt.de/shop/Themen/Sommertafel/',
            'https://www.tischwelt.de/shop/Themen/Valentinstag/',
            'https://www.tischwelt.de/shop/Themen/Servietten/',
            'https://www.tischwelt.de/shop/Themen/Eis/',
            'https://www.tischwelt.de/shop/Themen/Spargelzeit/',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'tischwelt001.xms002.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'tischwelt002.xms002.site/'],#..
            'x3' => ['source' => $array3, 'target' => 'tischwelt003.xms002.site/'],#---
            'x4' => ['source' => $array4, 'target' => 'tischwelt004.xms002.site/'],#..
            'x5' => ['source' => $array5, 'target' => 'tischwelt005.xms002.site/'],#..
            'x6' => ['source' => $array6, 'target' => 'tischwelt006.xms002.site/'],#..
            'x7' => ['source' => $array7, 'target' => 'tischwelt007.xms002.site/'],#---
            'x8' => ['source' => $array8, 'target' => 'tischwelt008.xms002.site/'],#..
            'x9' => ['source' => $array9, 'target' => 'tischwelt009.xms002.site/'],#..
            'x10' => ['source' => $array10, 'target' => 'tischwelt010.xms002.site/'],#..
            'x11' => ['source' => $array11, 'target' => 'tischwelt011.xms002.site/'],#---
            'x12' => ['source' => $array12, 'target' => 'tischwelt012.xms002.site/'],#..
            'x13' => ['source' => $array13, 'target' => 'tischwelt013.xms002.site/'],#..
            'x14' => ['source' => $array14, 'target' => 'tischwelt014.xms002.site/'],#..
            'x15' => ['source' => $array15, 'target' => 'tischwelt015.xms002.site/'],#..
            'x16' => ['source' => $array16, 'target' => 'tischwelt016.xms002.site/'],#---
            'x17' => ['source' => $array17, 'target' => 'tischwelt017.xms002.site/'],#..
            'x18' => ['source' => $array18, 'target' => 'tischwelt018.xms002.site/'],#..
            'x19' => ['source' => $array19, 'target' => 'tischwelt019.xms002.site/'],#..
            'x20' => ['source' => $array20, 'target' => 'tischwelt020.xms002.site/'],#..
            'x21' => ['source' => $array21, 'target' => 'tischwelt021.xms002.site/'],#..
            'x22' => ['source' => $array22, 'target' => 'tischwelt022.xms002.site/'],#..
            'x23' => ['source' => $array23, 'target' => 'tischwelt023.xms002.site/'],#..
            'x24' => ['source' => $array24, 'target' => 'tischwelt024.xms002.site/'],#..
            'x25' => ['source' => $array25, 'target' => 'tischwelt025.xms002.site/'],#..
            'x26' => ['source' => $array26, 'target' => 'tischwelt026.xms002.site/'],#..
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.tischwelt.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.nameLink')->count()){
                $crawler->filter('.nameLink')->each(function(Crawler $node,$i){
                    $this->processPage('https://www.tischwelt.de'.$node->attr('href'),'https://www.tischwelt.de'.$node->attr('href'));
                });
            }else{
                $this->processPage($source,$source);
            }
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url,$source)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.next-page');
        if ($nextNode->count()) {
            $this->processPage($source.$nextNode->attr('href'),$source);
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.header .title')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('https://www.tischwelt.de'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data = "";
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$data){
            if(strstr($node->text(),"Product")){
                $data=json_decode($node->text(),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$data['name'];
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$data['offers']['price'])));
        $product['brand']=$data['brand']['name'];
        $product['type'] = 'simple';
        
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['sku'] = $data['sku']; 
        $product['short_description']= "";  
        if($crawler->filter('.description-component')->count()){
            $product['description'] = $crawler->filter('.description-component')->html();
        }else{
            $product['description'] = "";
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=$data['image'];
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        // if($crawler->filter('.imgswatches')->count()){
        //     $product['variations'][]= [
        //         'name'=>'Farbe',
        //         'options' =>$crawler->filter('.imgswatches label')->each(function(Crawler $node){
        //             return $node->attr('data-original');
        //     })]; 
        //     $product['type'] = 'variable';
        // }else{
        //     $product['variations'][]=[
        //     'name'=>"",
        //     'options' =>""];
        // }
        $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        // print_r($product['variations']);exit;
        if($crawler->filter(".attributes-component")->count()) {
            $count = $crawler->filter(".attributes-component dt")->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".attributes-component dt")->eq($i)->text(),
                    'options' => [$crawler->filter(".attributes-component dd")->eq($i)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
