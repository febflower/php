<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class cloppenDe extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:cloppenDe')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.peek-cloppenburg.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.peek-cloppenburg.de/de/damen/bekleidung/jeans/skinny-fit-jeans',
            'https://www.peek-cloppenburg.de/de/damen/bekleidung/hosen/weite-hosen?from=192&limit=48',
        ]; 
        $array244=[
            // 'https://www.peek-cloppenburg.de/de/herren/accessoires/taschen/rucksaecke?from=288&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/taschen/umhaengetaschen',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/taschen/koffer',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/taschen/bauchtaschen',
            // https://www.peek-cloppenburg.de/de/herren/accessoires/taschen/rucksaecke?from=240&limit=48
        ];
        $array251=[
            'https://www.peek-cloppenburg.de/de/herren/accessoires/guertel/leder-kunstlederguertel?from=624&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/caps?from=480&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/huete',
        ];
        $array252=[
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/caps?from=624&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/muetzen',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/muetzen?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/huete?from=192&limit=48',
        ];
        $array253=[
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/muetzen?from=336&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/krawatten-fliegen/einstecktuecher',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/krawatten-fliegen/krawatten',
        ];
        $array254=[
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/muetzen?from=480&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/krawatten-fliegen/krawatten?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/krawatten-fliegen/fliegen',
        ];
        $array255=[
            'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/muetzen?from=624&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/krawatten-fliegen/krawatten?from=336&limit=48',
            'https://www.peek-cloppenburg.de/de/herren/accessoires/krawatten-fliegen/sets',
            // 'https://www.peek-cloppenburg.de/de/herren/accessoires/kopfbedeckungen/muetzen?from=768&limit=48',
        ];
        $array256=[
            'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/schluesselanhaenger',
            'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/fun',
            'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/kueche',
            'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/home'
        ];
        $array257=[
            'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/fun',
            // 'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/fun?from=192&limit=48',
            // 'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/baby-und-kids',
            // 'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/travel',
            // 'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/buero',
            // 'https://www.peek-cloppenburg.de/de/herren/geschenkartikel/handyzubehoer',
        ];
        $array258=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/pyjamas',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/sweatshirts',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/blazer',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/t-shirts',
        ];
        $array259=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/sweatshirts?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/t-shirts?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/blusen',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/hosen?from=192&limit=48',
        ];
        $array260=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/t-shirts?from=336&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/hosen',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/roecke',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/kommunionkleider',
        ];
        $array261=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/t-shirts?from=480&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/jacken',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/maentel',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/bademode'
        ];
        $array262=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/jacken?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/waesche',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/socken',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/pullover-strickjacken',
        ];
        $array263=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/pullover-strickjacken?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/langarmshirts',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/jeans',
        ];
        $array264=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/langarmshirts?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/jeans?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/kleider',
        ];
        $array265=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/jacken?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/kleider?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/shorts-bermudas',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/shorts-bermudas?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/kleider?from=480&limit=48',
            // 'https://www.peek-cloppenburg.de/de/kinder/bekleidung/maedchen/kleider?from=336&limit=48',
        ];
        $array266=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/bademode',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/t-shirts',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/jacken',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/shorts-bermudas',
        ];
        $array267=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/t-shirts?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/jacken?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/shorts-bermudas?from=192&limit=48',
        ];
        $array268=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/t-shirts?from=336&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/jacken?from=336&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/pyjamas',
        ];
        $array269=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/t-shirts?from=480&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/jeans',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/sweatshirts',
        ];
        $array270=[
            // 'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/t-shirts?from=624&limit=48',
            // 'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/jeans?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/sweatshirts?from=192&limit=48',
        ];
        $array271=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/sweatshirts?from=336&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/westen',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/hosen',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/hemden',
        ];
        $array272=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/sweatshirts?from=480&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/hosen?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/pullover-strickjacken',
        ];
        $array273=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/kommunionanzuege',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/langarmshirts',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/anzuege-sakkos',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/socken',
        ];
        $array274=[
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/langarmshirts?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/waesche',
            'https://www.peek-cloppenburg.de/de/kinder/bekleidung/jungen/sweatshirts?from=624&limit=48',
        ];
        $array275=[
            'https://www.peek-cloppenburg.de/de/kinder/schuhe/jungen',
            'https://www.peek-cloppenburg.de/de/kinder/schuhe/maedchen',
            'https://www.peek-cloppenburg.de/de/kinder/schuhe/jungen?from=192&limit=48',
            'https://www.peek-cloppenburg.de/de/kinder/schuhe/maedchen?from=192&limit=48',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x244' => ['source' => $array244, 'target' => 'cloppen244.xms010.site/'],
            'x251' => ['source' => $array251, 'target' => 'cloppen251.xms010.site/'],
            'x252' => ['source' => $array252, 'target' => 'cloppen252.xms010.site/'],
            'x253' => ['source' => $array253, 'target' => 'cloppen253.xms010.site/'],
            'x254' => ['source' => $array254, 'target' => 'cloppen254.xms010.site/'],
            'x255' => ['source' => $array255, 'target' => 'cloppen255.xms010.site/'],
            'x256' => ['source' => $array256, 'target' => 'cloppen256.xms010.site/'],
            'x257' => ['source' => $array257, 'target' => 'cloppen257.xms010.site/'],
            'x258' => ['source' => $array258, 'target' => 'cloppen258.xms010.site/'],
            'x259' => ['source' => $array259, 'target' => 'cloppen259.xms010.site/'],
            'x260' => ['source' => $array260, 'target' => 'cloppen260.xms010.site/'],
            'x261' => ['source' => $array261, 'target' => 'cloppen261.xms010.site/'],
            'x262' => ['source' => $array262, 'target' => 'cloppen262.xms010.site/'],
            'x263' => ['source' => $array263, 'target' => 'cloppen263.xms010.site/'],
            'x264' => ['source' => $array264, 'target' => 'cloppen264.xms010.site/'],
            'x265' => ['source' => $array265, 'target' => 'cloppen265.xms010.site/'],
            'x266' => ['source' => $array266, 'target' => 'cloppen266.xms010.site/'],
            'x267' => ['source' => $array267, 'target' => 'cloppen267.xms010.site/'],
            'x268' => ['source' => $array268, 'target' => 'cloppen268.xms010.site/'],
            'x269' => ['source' => $array269, 'target' => 'cloppen269.xms010.site/'],
            'x270' => ['source' => $array270, 'target' => 'cloppen270.xms010.site/'],
            'x271' => ['source' => $array271, 'target' => 'cloppen271.xms010.site/'],
            'x272' => ['source' => $array272, 'target' => 'cloppen272.xms010.site/'],
            'x273' => ['source' => $array273, 'target' => 'cloppen273.xms010.site/'],
            'x274' => ['source' => $array274, 'target' => 'cloppen274.xms010.site/'],
            'x275' => ['source' => $array275, 'target' => 'cloppen275.xms010.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.peek-cloppenburg.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source,$source);
            // echo $source.PHP_EOL;
        }
    }

    protected function processPage($url,$source)
    {
        if(strstr($source,"?from=")){
            $total = explode('&limit',explode("?from=",$source)[1])[0];
            $page=explode('&limit',explode("?from=",$url)[1])[0];
            // print_r($total);
            if($total <= 300){
                if($page<=300){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
                
            }
            elseif($total > 300 && $total <= 450){
                if($page<=450){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }
            elseif($total > 450 && $total <= 600){
                if($page<=600){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }
            elseif($total > 600 && $total <= 750){
                if($page<=750){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }
            elseif($total > 750 && $total <= 900){
                if($page<=900){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }
            elseif($total > 900 && $total <= 1050){
                if($page<=1050){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }
            elseif($total > 1050 && $total <= 1200){
                if($page<=1200){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }
            else{
                
            }
        }else{
            if(strstr($url,"?from=")){
                $page=explode('&limit',explode("?from=",$url)[1])[0];
                if($page <= 150){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                    }    
                }
            }else{
                echo $this->output->writeln($url);
                $response = $this->guzzleHttpClient->request('GET', $url);
                $contents = $response->getBody()->getContents();
                $crawler = new Crawler($contents);
                $this->processProductList($crawler);
                $nextNode = $crawler->filter('[rel="next"]');
                if ($nextNode->count()) {
                    $this->processPage('https://www.peek-cloppenburg.de'.$nextNode->attr('href'),$source);
                }  
            }
        } 
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.space-x-1 li')->each(function (Crawler $node) {
            return str_replace("/","",$node->text());
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-card')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('[class="text-cp-sm text-body truncate"]')->text(),
                    'url' => sprintf('https://www.peek-cloppenburg.de'.$node->filter('a')->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.galeria.de/produkt/only-carmakoma-curve-bikerjacke-emmy-kunstleder-reissverschluss-fuer-damen-5714492145471';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data = "";
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$data){
            if(strstr($node->text(),"product")){
                $data=json_decode($node->text(),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$data['name'];
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$data['offers']['price'])));
        $product['brand']=$data['brand']['name'];
        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('.space-x-1 li')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['sku'] = $data['sku']; 
        $product['short_description']= $data['description'];  
        if($crawler->filter('#accordion-panel-details')->count()){
            $product['description'] =$crawler->filter('#accordion-panel-details')->html();
            // $crawler->filter('.pdp-accordion')->each(function(Crawler $node)use(&$product){
            //     $product['description'] = $product['description'].$node->html();
            // });
        }else{
            $product['description'] = "";
        }
        
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('[data-testid="productSelection"]')->filter('.jPthUr')->count()){
            $product['color'] = $crawler->filter('[data-testid="productSelection"]')->filter('.jPthUr')->text();
        }else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = $data['image'];
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.size-variants')->count()){
            $product['variations'][]= [
                'name'=>'Größe',
                'options' =>$crawler->filter('.size-variants span')->each(function(Crawler $node){
                    return $node->text();
            })]; 
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        }
        $product['attributes'] = [];
        // print_r($product['variations']);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
