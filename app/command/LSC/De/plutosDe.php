<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class plutosDe extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:plutosDe')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.plutosport.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array1=[
            // 'https://www.plutosport.de/herren/freizeit/kleidung/pullover',#7
            'https://www.plutosport.de/herren/freizeit/kleidung/pullover/sweatjacken?p=5',
            'https://www.plutosport.de/herren/freizeit/kleidung/pullover/gestrickte-pullover',
        ];
        $array2=[
            'https://www.plutosport.de/herren/freizeit/kleidung/loungewear',#8
            'https://www.plutosport.de/herren/freizeit/kleidung/casual-hemden',
            'https://www.plutosport.de/herren/freizeit/kleidung/trainingsanzuge',
            'https://www.plutosport.de/herren/freizeit/schuhe/pantoffeln',
        ];
        $array9=[
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers'
        ];
        $array54=[
            'https://www.plutosport.de/kinder/freizeit/kleidung/hosen',
            'https://www.plutosport.de/kinder/freizeit/kleidung/kleider',
            'https://www.plutosport.de/kinder/freizeit/kleidung/jacken',
        ];
        $array59=[
            'https://www.plutosport.de/kinder/freizeit/schuhe/sandalen',
        ];
        $array60=[
            'https://www.plutosport.de/kinder/fussball/kleidung',
            // 'https://www.plutosport.de/kinder/fussball/kleidung/shirts/kurze-armel?p=2',
        ];
        $array61=[
            'https://www.plutosport.de/kinder/fussball',
        ];
        $array62=[
            'https://www.plutosport.de/kinder/hockey',
        ];
        $array63=[
           'https://www.plutosport.de/kinder/schwimmen',
           'https://www.plutosport.de/kinder/skaten',
        ];
        $array64=[
            'https://www.plutosport.de/kinder/outdoor',
            'https://www.plutosport.de/kinder/laufsport',
        ];
        $array65=[
           'https://www.plutosport.de/kinder/fitness',
           'https://www.plutosport.de/kinder/radsport',
        ];
        $array66=[
           'https://www.plutosport.de/kinder/tennis',
           'https://www.plutosport.de/kinder/camping',
        ];
        $array67=[
           'https://www.plutosport.de/kinder/bad-beach',
           'https://www.plutosport.de/kinder/kampfsport',
        ];
        $array68=[
           'https://www.plutosport.de/kinder/korfball',
           'https://www.plutosport.de/kinder/handball',
            'https://www.plutosport.de/kinder/basketball',
            'https://www.plutosport.de/kinder/camping',
            'https://www.plutosport.de/kinder/indoor',
        ];
        $array69=[
            'https://www.plutosport.de/kinder/rugby',
            'https://www.plutosport.de/kinder/wintersport',
            'https://www.plutosport.de/kinder/ballett',
            'https://www.plutosport.de/kinder/indoorcycling',
        ];
        $array70=[
           'https://www.plutosport.de/kinder/wassersport',
           'https://www.plutosport.de/kinder/rugby',
        ];
        $this->sites = [
            'x' => ['source' => $array1, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'plutosde007.xms005.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'plutosde008.xms005.site/'],#..
            'x9' => ['source' => $array9, 'target' => 'plutosde009.xms005.site/'],#
            'x54' => ['source' => $array54, 'target' => 'plutosde054.xms005.site/'],#3
            'x59' => ['source' => $array59, 'target' => 'plutosde059.xms005.site/'],#3'
            'x60' => ['source' => $array60, 'target' => 'plutosde060.xms005.site/'],#3'
            'x61' => ['source' => $array61, 'target' => 'plutosde061.xms006.site/'],#3
            'x62' => ['source' => $array62, 'target' => 'plutosde062.xms006.site/'],#3
            'x63' => ['source' => $array63, 'target' => 'plutosde063.xms006.site/'],#3
            'x64' => ['source' => $array64, 'target' => 'plutosde064.xms006.site/'],#3
            'x65' => ['source' => $array65, 'target' => 'plutosde065.xms006.site/'],#3'
            'x66' => ['source' => $array66, 'target' => 'plutosde066.xms006.site/'],#3'
            'x67' => ['source' => $array67, 'target' => 'plutosde067.xms006.site/'],#3'
            'x68' => ['source' => $array68, 'target' => 'plutosde068.xms006.site/'],#3
            'x69' => ['source' => $array69, 'target' => 'plutosde069.xms006.site/'],#3
            'x70' => ['source' => $array70, 'target' => 'plutosde070.xms006.site/'],#3
        ];
        #1，2，4,7，8，9，10
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.plutosport.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.category-grid a')->count()){
                $crawler->filter('.category-grid a')->each(function(Crawler $node1,$i){
                    $response = $this->guzzleHttpClient->request('GET', $node1->attr('href'));
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    if($crawler->filter('.category-grid a')->count()){
                        $crawler->filter('.category-grid a')->each(function(Crawler $node2,$i){
                            $this->processPage($node2->attr('href'));
                        });
                    }else{
                        $this->processPage($node1->attr('href'));
                    }
                });
            }else{
                $this->processPage($source);
            }
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[title="Weiter"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-item-link')->each(function (Crawler $node,$i) use(&$breadcrumbs){
            // $response = $this->guzzleHttpClient->request('GET', $node->attr('href'));
            // $contents = $response->getBody()->getContents();
            // $crawler = new Crawler($contents);
            // if($crawler->filter('.product-variants')->count()){
            //     $crawler->filter('.product-variants')->filter('.product-item-link')->each(function(Crawler $node,$j)use(&$breadcrumbs,&$i){
            //         try {
            //         $this->discount = rand(65, 80) / 100;
            //         $this->crawlerProduct([
            //             'name' => "[ {$i}-{$j} ] " . $node->text(),
            //             'url' => sprintf($node->attr('href')),
            //             'breadcrumbs'=>$breadcrumbs,
            //         ]);
            //         } catch (\Exception $exception) {
            //             $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            //         }
            //     });
            // }else{
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.page-title')->text();
        $product['price'] = str_replace(',','.',str_replace(' €','',str_replace('$','',$crawler->filter('[property="product:price:amount"]')->attr('content'))));
        if($crawler->filter('.manufacturer-logo img')->count()){
            $product['brand']=$crawler->filter('.manufacturer-logo img')->attr('alt');
        }else{
            $product['brand']=$crawler->filter('[data-th="Marken"]')->text();
        }
        
        $product['type'] = 'simple';
        
        // $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product);exit;
        $product['sku'] = str_replace("..","",$crawler->filter('#product_addtocart_form')->attr('data-product-sku'));
        $product['short_description']= "";
        if($crawler->filter('.value')->count()){
            $product['description'] = $crawler->filter('.value')->html();
        }else{
            $product['description'] = "";
        }
        
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('[data-th="Farbe"]')->count()){
            $product['color'] = $crawler->filter('[data-th="Farbe"]')->text();
        }else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        
        //图片   
        $img = [];
        $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$img){
            if(strstr($node->text(),'mage/gallery/gallery')){
                $img = json_decode($node->text(),true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
            }      
        });
        for($i=0;$i<count($img);$i++){
            $images[$i]=[
                'src'=>$img[$i]['full'],
                'name'=>$product['title'],
            ];
        };
        // print_r($images);exit;
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        if($crawler->filter('.swatch-opt')->count()){
            $size=[];
            $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$size){
                if(strstr($node->text(),'jsonConfig')){
                    $size = json_decode($node->text(),true)['[data-role=swatch-options]']['Magento_Swatches/js/swatch-renderer']['jsonConfig']['attributes'][217]['options'];
                }
            });
            if(count($size)>1){
                for($i=0;$i<count($size);$i++){
                $option[$i]=str_replace(" - Sofort lieferbar","",$size[$i]['label']);
                }
                $product['variations'][]= [
                    'name'=>'Größe',
                    'options' =>$option]; 
                $product['type'] = 'variable';
            }else{
                $product['variations'][]=[
                    'name'=>'',
                    'options' =>""];
            }
        }else{$product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }
        // print_r($product['variations']);exit;
        if($crawler->filter(".additional-attributes-wrapper")->count()) {
            $count = $crawler->filter(".additional-attributes-wrapper")->filter('tbody>tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".additional-attributes-wrapper")->filter('tbody>tr>th')->eq($i)->text(),
                    'options' => [$crawler->filter(".additional-attributes-wrapper")->filter('tbody>tr>td')->eq($i)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
