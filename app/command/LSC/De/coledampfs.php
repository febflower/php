<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class coledampfs extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:coledampfs')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://coledampfs.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://coledampfs.de/messer-und-zubehoer',
            'https://coledampfs.de/kochen'
        ];
        $array1=[
            'https://coledampfs.de/messer-und-zubehoer',
            'https://coledampfs.de/Essig-Oel',
            'https://coledampfs.de/Kuechengeraete'
        ];
        $array2=[
            'https://coledampfs.de/Kuechenhelfer',
            'https://coledampfs.de/salz-und-pfeffermuehlen',
            'https://coledampfs.de/Moerser-sonst-Muehlen'
        ];
        $array3=[
            'https://coledampfs.de/kuechenhelfer/schneidebretter',
            'https://coledampfs.de/kuechenhelfer/aufbewahrung',
            'https://coledampfs.de/Siebe-Salatschleudern',
            'https://coledampfs.de/kuchenhelfer/waagen-und-wecker',
            'https://coledampfs.de/Reiben-Hobel',
            'https://coledampfs.de/Kuechenhelfer',
        ];
        $array4=[
            'https://coledampfs.de/kochen',
            'https://coledampfs.de/lunch-flaschen'
        ];
        $array5=[
            'https://coledampfs.de/geschirr-besteck'
        ];
        $array6=[
            'https://coledampfs.de/glaeser-barbedarf',
            'https://coledampfs.de/backen'
        ];
        $array7=[
            'https://coledampfs.de/kaffee-tee',
            'https://coledampfs.de/textiles',
            'https://coledampfs.de/kids'
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'coledamp001.xms012.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'coledamp002.xms012.site/'],
            'x3' => ['source' => $array3, 'target' => 'coledamp003.xms012.site/'],
            'x4' => ['source' => $array4, 'target' => 'coledamp004.xms012.site/'],
            'x5' => ['source' => $array5, 'target' => 'coledamp005.xms012.site/'],
            'x6' => ['source' => $array6, 'target' => 'coledamp006.xms012.site/'],
            'x7' => ['source' => $array7, 'target' => 'coledamp007.xms012.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://coledampfs.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            if($crawler->filter('.content-cats-small a')->count()){
                $crawler->filter('.content-cats-small a')->each(function(Crawler $node){
                    $this->processPage($node->attr('href'));
                });  
            }else{
                $this->processPage($source);
            }
        }
    }

    protected function processPage($url)
    {
        // echo "'".$url."',".PHP_EOL;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.text-clamp-2')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://coledampfs.de/asa-selection-coppa-kuro-teller-bowl-set';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.product-title')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('[itemprop="price"]')->attr('content'))));  
        $product['brand']=$crawler->filter('[itemprop="brand"] a')->text();
        $product['type'] = 'simple';

        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
       
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text(); 
        $product['short_description']= "";
        $crawler->filter('.product-features-row div')->each(function(Crawler $node)use(&$product){
            $product['short_description']= $product['short_description'].$node->text().PHP_EOL;
        });
        $product['description'] =$crawler->filter('.desc')->html();
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img=[];
        $crawler->filter('#gallery_wrapper img')->each(function(Crawler $node)use(&$img){
                $img[] = json_decode($node->attr('data-list'),true)['lg']['src'];
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image,
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.switch-variations')->count()){
            if($crawler->filter('.switch-variations')->count()){
                $product['variations'][]=[
                    'name'=>"Größe",
                    'options' =>$crawler->filter('.switch-variations option')->each(function(Crawler $node){
                        return str_replace("&Oslash; ","",$node->attr('data-original'));
                    })
                ];
                $product['type'] = 'variable';
            }  
        }else{
            $product['variations'][]=[
                'name'=>"",
                'options' =>""];
        }
        $product['attributes'] = [];
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
