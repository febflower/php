<?php
namespace app\command\LSC\De;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class galeriaDe extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:galeriaDe')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.galeria.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            // 'https://www.galeria.de/damen/bekleidung/maentel',
            'https://www.galeria.de/damen/bekleidung/jumpsuits',
        ];
        $array150=[
            'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=76',
            'https://www.galeria.de/taschen-koffer/taschen/brustbeutel',
            'https://www.galeria.de/sport/sportausruestung/ruecksaecke-taschen',
        ];
        $array151=[
            'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=91',
            'https://www.galeria.de/sport/sportausruestung/ruecksaecke-taschen?page=16',
            'https://www.galeria.de/taschen-koffer/taschen/einkaufstaschen',
        ];
        $array152=[
            'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=106',
            'https://www.galeria.de/sport/sportausruestung/ruecksaecke-taschen?page=31',
            'https://www.galeria.de/taschen-koffer/taschen/einkaufstaschen?page=16',
            'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=151',
        ];
        $array153=[
            'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=121',
            'https://www.galeria.de/sport/sportausruestung/ruecksaecke-taschen?page=46',
            'https://www.galeria.de/taschen-koffer/taschen/businesstaschen'
        ];
        $array154=[
            'https://www.galeria.de/taschen-koffer/taschen/businesstaschen',
            // 'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=121',
            // 'https://www.galeria.de/taschen-koffer/taschen/businesstaschen?page=16',
            // 'https://www.galeria.de/taschen-koffer/taschen/handtaschen?page=166',
            // 'https://www.galeria.de/sport/sportausruestung/ruecksaecke-taschen?page=31',
        ];
        $array155=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ringe',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe',
            'https://www.galeria.de/uhren-schmuck/schmuck/armbaender-armreifen',
        ];
        $array156=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ringe?page=16',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=16',
            'https://www.galeria.de/uhren-schmuck/schmuck/armbaender-armreifen?page=16',
        ];
        $array157=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ringe?page=31',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=31',
            'https://www.galeria.de/uhren-schmuck/schmuck/armbaender-armreifen?page=31',
        ];
        $array158=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ringe?page=46',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=46',
            'https://www.galeria.de/uhren-schmuck/schmuck/armbaender-armreifen?page=46'
        ];
        $array159=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ringe?page=61',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=61',
            'https://www.galeria.de/uhren-schmuck/schmuck/armbaender-armreifen?page=61'
        ];
        $array160=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ringe?page=76',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=76',
            'https://www.galeria.de/uhren-schmuck/schmuck/armbaender-armreifen?page=76'
        ];
        $array161=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=91',
            'https://www.galeria.de/uhren-schmuck/schmuck/aufsaetze-charms-beads',
            'https://www.galeria.de/uhren-schmuck/schmuck/aufsaetze-charms-beads?page=16',
            'https://www.galeria.de/uhren-schmuck/schmuck/fussschmuck',
        ];
        $array162=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=106',
            'https://www.galeria.de/uhren-schmuck/schmuck/piercings',
            'https://www.galeria.de/uhren-schmuck/schmuck/piercings?page=16',
            'https://www.galeria.de/uhren-schmuck/schmuck/schmuck-pflegeprodukte',
            'https://www.galeria.de/uhren-schmuck/schmuck/anhaenger?page=61',
        ];
        $array163=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=121',
            'https://www.galeria.de/uhren-schmuck/schmuck/schmuck-sets',
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=151',
            'https://www.galeria.de/uhren-schmuck/schmuck/anhaenger',
            'https://www.galeria.de/uhren-schmuck/schmuck/anhaenger?page=46',
        ];
        $array164=[
            'https://www.galeria.de/uhren-schmuck/schmuck/ohrringe?page=136',
            'https://www.galeria.de/uhren-schmuck/schmuck/schmuck-sets?page=16',
            'https://www.galeria.de/uhren-schmuck/schmuck/anhaenger?page=16',
            'https://www.galeria.de/uhren-schmuck/schmuck/anhaenger?page=31',
        ];
        $array165=[
            'https://www.galeria.de/weihnachten/weihnachtsfest/weihnachtsbaeckerei',
            'https://www.galeria.de/weihnachten/weihnachtsfest/festliche-tafel',
            'https://www.galeria.de/weihnachten/weihnachtsfest/raclette-fondue',
            'https://www.galeria.de/weihnachten/weihnachtsfest/weihnachtsdeko',
            'https://www.galeria.de/weihnachten/weihnachtsfest/weihnachtsbeleuchtung',
            'https://www.galeria.de/weihnachten/weihnachtsfest/weihnachtspullover-co',
            'https://www.galeria.de/weihnachten/weihnachtsfest/suessigkeiten',
        ];
        // https://www.galeria.de/taschen-koffer/taschen/damentaschen?page=176
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x150' => ['source' => $array150, 'target' => 'galeria150.seo076.site/'],
            'x151' => ['source' => $array151, 'target' => 'galeria151.seo076.site/'],
            'x152' => ['source' => $array152, 'target' => 'galeria152.seo076.site/'],
            'x153' => ['source' => $array153, 'target' => 'galeria153.seo076.site/'],
            'x154' => ['source' => $array154, 'target' => 'galeria154.seo076.site/'],
            'x155' => ['source' => $array155, 'target' => 'galeria155.seo076.site/'],
            'x156' => ['source' => $array156, 'target' => 'galeria156.seo076.site/'],
            'x157' => ['source' => $array157, 'target' => 'galeria157.seo076.site/'],
            'x158' => ['source' => $array158, 'target' => 'galeria158.seo076.site/'],
            'x159' => ['source' => $array159, 'target' => 'galeria159.seo076.site/'],
            'x160' => ['source' => $array160, 'target' => 'galeria160.seo076.site/'],
            'x161' => ['source' => $array161, 'target' => 'galeria161.seo076.site/'],
            'x162' => ['source' => $array162, 'target' => 'galeria162.seo076.site/'],
            'x163' => ['source' => $array163, 'target' => 'galeria163.seo076.site/'],
            'x164' => ['source' => $array164, 'target' => 'galeria164.seo076.site/'],
            'x165' => ['source' => $array165, 'target' => 'galeria165.seo076.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.galeria.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            $this->processPage($source,$source);
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url,$source)
    {
        if(strstr($source,"?page=")){
            $total = explode("?page=",$source)[1];
            $page = explode("?page=",$url)[1];
            if($total <= 15){
                if($page <= 15){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }  
            }
            elseif($total>15 && $total<=30){
                if($page<=30){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>30 && $total<=45){
                if($page<=45){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>45 && $total<=60){
                if($page<=60){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>60 && $total<=75){
                if($page<=75){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>75 && $total<=90){
                if($page<=90){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>90 && $total<=105){
                if($page<=105){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>105 && $total<=120){
                if($page<=120){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>120 && $total<=135){
                if($page<=135){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>135 && $total<=150){
                if($page<=150){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>150 && $total<=165){
                if($page<=165){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            elseif($total>165 && $total<=180){
                if($page<=180){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }
            else{

            }
        }else{
            if(strstr($url,"?page=")){
                $page = explode("?page=",$url)[1];
                if($page<=15){
                    echo $this->output->writeln($url);
                    $response = $this->guzzleHttpClient->request('GET', $url);
                    $contents = $response->getBody()->getContents();
                    $crawler = new Crawler($contents);
                    $this->processProductList($crawler);
                    $nextNode = $crawler->filter('[rel="next"]');
                    if ($nextNode->count()) {
                        if($nextNode->attr('href')!=null){
                            $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                        }
                    }    
                }
            }else{
                echo $this->output->writeln($url);
                $response = $this->guzzleHttpClient->request('GET', $url);
                $contents = $response->getBody()->getContents();
                $crawler = new Crawler($contents);
                $this->processProductList($crawler);
                $nextNode = $crawler->filter('[rel="next"]');
                if ($nextNode->count()) {
                    if($nextNode->attr('href')!=null){
                        $this->processPage('https://www.galeria.de'.$nextNode->attr('href'),$source);
                    }
                }    
            }
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('[data-testid="breadcrumbs"] a')->each(function (Crawler $node) {
            return $node->text();
        }));
        array_push($breadcrumbs,$crawler->filter('.hUauMd')->text());
        // print_r($breadcrumbs);exit;
        $crawler->filter('#productList>a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            // echo 'https://www.galeria.de'.$node->attr('href').PHP_EOL;
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.productInfoLine')->text(),
                    'url' => sprintf('https://www.galeria.de'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.galeria.de/produkt/abati-regali-abati-regali-chardonnay-friuli-grave-doc-0-75l-8034076533041';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data = "";
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$data){
            if(strstr($node->text(),"product")){
                $data=json_decode($node->text(),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$crawler->filter('.pdpInfoLine')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$data['offers']['price'])));
        // $product['brand']=$data['brand']['name'];
        $product['brand']=$crawler->filter('[data-testid="productTitle"]')->text();
        // print_r($product);exit;
        $product['type'] = 'simple';
        
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['sku'] = $data['sku']; 
        $product['short_description']= "";  
        if($crawler->filter('[role="tabpanel"]')->count()){
            $product['description'] = $crawler->filter('[role="tabpanel"]')->filter('.dYdFKf')->html();
        }else{
            $product['description'] = "";
        }
        
        $product['keywords'] = [];
        $product['gender'] = '';
        if($crawler->filter('[data-testid="productSelection"]')->filter('.dyyidv')->count()){
            $product['color'] = $crawler->filter('[data-testid="productSelection"]')->filter('.dyyidv')->text();
        }
        elseif($crawler->filter('[data-testid="productSelection"]')->filter('.jEpQcV')->count()){
            $product['color'] = $crawler->filter('[data-testid="productSelection"]')->filter('.jEpQcV')->text();
        }
        elseif($crawler->filter('[data-testid="productSelection"]')->filter('.dYFrKs')->count()){
            $product['color'] = $crawler->filter('[data-testid="productSelection"]')->filter('.dYFrKs')->text();
        }
        else{
            $product['color'] = "";
        }
        $product['subCategory']=' ';
        // $product['tags'] = "";
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = $crawler->filter('.previewImageSlider img')->each(function(Crawler $node,$i){
            return 'https://www.galeria.de'.$node->attr('src');  
        });
        // print_r($img);exit;
        foreach($img as $image){
            $images[]=[
                'src'=>str_replace("200","320",$image),
                'name'=>$product['title'],
            ];
        }
        // $product['images']=$images;
        $product['images']=$this->toEncryptImage($images);
        // print_r($product);exit;
        if($crawler->filter('#Herstellergröße')->count()){
            $product['variations'][]= [
                'name'=>'Größe',
                'options' =>$crawler->filter('#Herstellergröße label')->each(function(Crawler $node){
                    return $node->text();
            })]; 
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
            'name'=>"",
            'options' =>""];
        }
        if($crawler->filter('[role="tabpanel"]')->filter('.sc-cjrPHo')->count()){
            echo 1;
            $count = $crawler->filter('[role="tabpanel"]')->filter('table tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter('[role="tabpanel"]')->filter('table tr')->eq($i)->filter('th')->text(),
                    'options' => [$crawler->filter('[role="tabpanel"]')->filter('table tr')->eq($i)->filter('td')->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
