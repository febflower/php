<?php
namespace app\command\LSC\Nl;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class hockeydirect extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:hockeydirect')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.hockeydirect.nl/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.hockeydirect.nl/hockeyschoenen',
        ];
        $array1=[
            'https://www.hockeydirect.nl/hockeysticks',
        ];
        $array2=[
            'https://www.hockeydirect.nl/hockeyschoenen',
        ];
        $array3=[
            'https://www.hockeydirect.nl/hockeykleding/heren-c'
        ];
        $array4=[
            'https://www.hockeydirect.nl/hockeykleding/dames-c'
        ];
        $array5=[
            'https://www.hockeydirect.nl/hockeykleding/jongens-c'
        ];
        $array6=[
            'https://www.hockeydirect.nl/hockeykleding/meisjes-c'
        ];
        $array7=[
            'https://www.hockeydirect.nl/hockeytassen',
            'https://www.hockeydirect.nl/bescherming-c/bitjes-c',
            'https://www.hockeydirect.nl/bescherming-c/cornerprotectie',
            'https://www.hockeydirect.nl/bescherming-c/protectiehandschoenen-c',
            'https://www.hockeydirect.nl/bescherming-c/scheenbeschermers-c',
            'https://www.hockeydirect.nl/hockeygrips',
            'https://www.hockeydirect.nl/hockeyballen'
        ];
        $array8=[
            'https://www.hockeydirect.nl/accessoires-c/trainingsmateriaal-c',
            'https://www.hockeydirect.nl/accessoires-c/kledingaccessoires-c',
            'https://www.hockeydirect.nl/accessoires-c/verzorging-blessures-c',
            'https://www.hockeydirect.nl/accessoires-c/stickaccessoires-c',
            'https://www.hockeydirect.nl/accessoires-c/overig-c'
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'hockey001.xms012.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'hockey002.xms012.site/'],
            'x3' => ['source' => $array3, 'target' => 'hockey003.xms012.site/'],
            'x4' => ['source' => $array4, 'target' => 'hockey004.xms012.site/'],
            'x5' => ['source' => $array5, 'target' => 'hockey005.xms012.site/'],
            'x6' => ['source' => $array6, 'target' => 'hockey006.xms012.site/'],
            'x7' => ['source' => $array7, 'target' => 'hockey007.xms012.site/'],
            'x8' => ['source' => $array8, 'target' => 'hockey008.xms012.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.hockeydirect.nl/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
            // $response = $this->guzzleHttpClient->request('GET', $source);
            // $contents = $response->getBody()->getContents();
            // $crawler = new Crawler($contents);
            // $crawler->filter('#facetedSearch-content--category a')->each(function(Crawler $node){
            //     $this->processPage($node->attr('href'));
            // });   
        }
    }

    protected function processPage($url)
    {
        // echo "'".$url."',".PHP_EOL;
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit; 
        $crawler->filter('.product-item-link')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']='https://www.hockeydirect.nl/asics-field-speed-ff-junior';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.page-title')->text();
        $product['price'] = str_replace(',','.',str_replace('€','',str_replace('$','',$crawler->filter('[itemprop="price"]')->attr('content'))));  
        $product['brand']=$crawler->filter('[itemprop="brand"]')->attr('content');
        $product['type'] = 'simple';
        // $item['breadcrumbs'] = [$product['brand']];
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
       
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text(); 
        $product['short_description']= "";
        $crawler->filter('.top-product-attributes li')->each(function(Crawler $node)use(&$product){
            $product['short_description']= $product['short_description'].$node->text().PHP_EOL;
        });
        if($crawler->filter('[itemprop="description"] p')->count()){
            if($crawler->filter('[itemprop="description"]')->filter('style')->count()){
                $product['description'] =str_replace($crawler->filter('[itemprop="description"]')->filter('style')->html(),"",$crawler->filter('#tab-label-description')->html().$crawler->filter('[itemprop="description"]')->html());
            }else{
                $product['description'] =$crawler->filter('#tab-label-description')->html().$crawler->filter('[itemprop="description"]')->html();
            }
        }else{
            $product['description'] = $product['short_description'];
        }
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = [];
        $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$img){
            if(strstr($node->text(),'mage/gallery/gallery')){
                $img= json_decode($node->text(),true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
            }      
        });
        foreach($img as $image){
            $images[]=[
                'src'=>$image['img'],
                'name'=>$product['title'],
            ];
        }
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // print_r($product['images']);exit;
        if($crawler->filter('.product-options-wrapper')->count()){
            $var=[];
            $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$var){
                if(strstr($node->text(),'Magento_Swatches/js/swatch-renderer')){
                    $arrs = json_decode($node->text(),true)['[data-role=swatch-options]']['Magento_Swatches/js/swatch-renderer']['jsonConfig']['attributes'];
                    foreach($arrs as $arr){
                        $var = $arr['options'];
                    }
                    // $var= json_decode($node->text(),true)['[data-role=swatch-options]']['Magento_Swatches/js/swatch-renderer']['jsonConfig']['attributes']['227']['options'];
                }      
            });
            foreach($var as $option){
                $options[]=$option['label'];
            }
            $product['variations'][]=[
                'name'=>"maat",
                'options' =>$options,
            ];
            $product['type'] = 'variable';
        }else{
            $product['variations'][]=[
                'name'=>"",
                'options' =>""];
        }
        
        if($crawler->filter('#product-attribute-specs-table')->count()){
            $count=$crawler->filter('#product-attribute-specs-table tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name'=>$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('th')->text(),
                    'options'=>[$crawler->filter('#product-attribute-specs-table tr')->eq($i)->filter('td')->text()]
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
