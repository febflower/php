<?php
namespace app\command\LSC\Fr;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\TextUI\XmlConfiguration\CodeCoverage\Report\Crap4j;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class rueducomFr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:rueducomFr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.rueducommerce.fr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-ordinateur-portable-15-6-pouces-14756',
            'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/hp',
        ];
        $array1=[
            // 'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-ordinateur-portable-15-6-pouces-14756',
            'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/hp',
            // 'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/msi',
            // 'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/xiaomi',
            // 'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/tucano',
            // 'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/eastpak',
        ];
        $array2=[
            'https://www.rueducommerce.fr/rayon/accessoires-et-consommables-2/sacoche-housse-et-sac-a-dos-pour-ordinateur-portable-1232/acer?fournisseur=3go+4smarts+abbey+aiino+akashi+alcatel+alden+alpexe+amahousse+apple+aromas+asus',
        ];
        $array3=[
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers/niedrig',
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers/hoch',
            'https://www.plutosport.de/herren/freizeit/schuhe/sneakers/leinen',
        ];
        $array4=[
            'https://www.plutosport.de/herren/freizeit/kleidung/unterwasche',
        ];
        $array5=[
            'https://www.plutosport.de/herren/freizeit/kleidung/socken/niedrig',
            'https://www.plutosport.de/herren/freizeit/kleidung/socken/halbhoch',
            'https://www.plutosport.de/herren/freizeit/kleidung/socken/hoch',
            'https://www.plutosport.de/herren/freizeit/kleidung/socken/haussocken',
            'https://www.plutosport.de/herren/freizeit/kleidung/schwimmshorts',
            'https://www.plutosport.de/herren/freizeit/kleidung/jeans/lang',
            'https://www.plutosport.de/herren/freizeit/kleidung/jeans/kurz',
        ];
        $array6=[
            'https://www.plutosport.de/herren/freizeit/kleidung/kurze-armel/regular-fit',
            'https://www.plutosport.de/herren/freizeit/kleidung/kurze-armel/slim-fit',
            'https://www.plutosport.de/herren/freizeit/kleidung/kurze-armel/2-packs',
            'https://www.plutosport.de/herren/freizeit/kleidung/kurze-armel/lange-armel',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            'x1' => ['source' => $array1, 'target' => 'rueducom001.xms006.site/'],#..
            'x2' => ['source' => $array2, 'target' => 'plutosde008.xms005.site/'],#..
            'x3' => ['source' => $array3, 'target' => 'plutosde009.xms005.site/'],#---
            'x4' => ['source' => $array4, 'target' => 'plutosde003.xms005.site/'],#..
            'x5' => ['source' => $array5, 'target' => 'plutosde005.xms005.site/'],#..
            'x6' => ['source' => $array6, 'target' => 'plutosde006.xms005.site/'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.rueducommerce.fr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $response = $this->guzzleHttpClient->request('GET', $source);
            $contents = $response->getBody()->getContents();
            $crawler = new Crawler($contents);
            $this->processPage($source);
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb--full span')->each(function (Crawler $node) {
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $crawler->filter('.item__title a')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->attr('title'),
                    'url' => sprintf('https://www.rueducommerce.fr'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url'] = 'https://www.rueducommerce.fr/p-xiaomi-mi-casual-daypack-sacoche-dordinateurs-portables-sac-a-dos-noir-rose-xiaomi-2012699968-16939.html';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $data="";
        $crawler->filter('script')->each(function(Crawler $node)use(&$data){
            if(strstr($node->text(),"dataLayer.push(")){
                $data=json_decode(trim(explode('dataLayer.push(',$node->text())[1],")"),true);
            }
        });
        // print_r($data);exit;
        $product['title'] =$crawler->filter('.product-name')->text();
        $product['price'] = str_replace(',','.',str_replace(' €','',str_replace('$','',$crawler->filter('[itemprop="price"]')->attr('content'))));
        if($crawler->filter('.product-brand')->count()){
            $product['brand']=$crawler->filter('.product-brand')->text();
        }else{
            $product['brand']='RueduCommerce';
        }
        $product['type'] = 'simple';
        
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // if($crawler->filter('[itemprop="sku"]')->attr('content')==null){
        //     $product['sku'] = $data['product_mp'];
        // }else{
        //     $product['sku'] =$crawler->filter('[itemprop="sku"]')->attr('content');
        // }
        $product['sku'] = (string)$data['ecommerce']['detail']['products'][0]['id'];
        // print_r(var_dump($product['sku']));exit;
        $product['short_description']= "";
        if($crawler->filter('.content-left-replaced-by-flixmedia')->count()){
            $product['description'] = $crawler->filter('.content-left-replaced-by-flixmedia')->html();
        }else{
            $product['description'] = "";
        }
        // print_r($product);exit;
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = $crawler->filter('#product__thumb-3su a')->each(function(Crawler $node,$i)use(&$img){
            return $node->attr('href');  
        });
        foreach($img as $image){
            $images[]=[
                'src'=>'https://www.rueducommerce.fr'.$image,
                'name'=>$product['title'],
            ];
        }
        // print_r($images);exit;
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        // if($crawler->filter('.swatch-opt')->count()){
        //     $size=[];
        //     $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$size){
        //         if(strstr($node->text(),'jsonConfig')){
        //             $size = json_decode($node->text(),true)['[data-role=swatch-options]']['Magento_Swatches/js/swatch-renderer']['jsonConfig']['attributes'][217]['options'];
        //         }
        //     });
        //     if(count($size)>1){
        //         for($i=0;$i<count($size);$i++){
        //         $option[$i]=str_replace(" - Sofort lieferbar","",$size[$i]['label']);
        //         }
        //         $product['variations'][]= [
        //             'name'=>'Größe',
        //             'options' =>$option]; 
        //         $product['type'] = 'variable';
        //     }else{
        //         $product['variations'][]=[
        //             'name'=>'',
        //             'options' =>""];
        //     }
        // }else{$product['variations'][]=[
        //     'name'=>'',
        //     'options' =>""];
        // }
        $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        
        if($crawler->filter(".caracteristique-box")->count()) {
            $count = $crawler->filter(".group-item")->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".group-item")->eq($i)->filter('.label')->text(),
                    'options' => [$crawler->filter(".group-item")->eq($i)->filter('.value')->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
