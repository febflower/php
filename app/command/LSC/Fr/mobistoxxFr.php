<?php
namespace app\command\LSC\Fr;

use think\console\input\Argument;
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class mobistoxxFr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:lsc:mobistoxxFr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.mobistoxx.fr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            // 'https://www.mobistoxx.fr/mobilier-de-jardin/',
            'https://www.mobistoxx.fr/chambre/boxspring/',
        ];
        $array5=[
            'https://www.mobistoxx.fr/chambre/armoire/?aca_productsoort%5B0%5D=Armoire',
            'https://www.mobistoxx.fr/chambre/armoire/?aca_productsoort%5B0%5D=Armoire+de+rangement',
            'https://www.mobistoxx.fr/chambre/armoire/?aca_productsoort%5B0%5D=Biblioth%C3%A8que',
            'https://www.mobistoxx.fr/chambre/armoire/?aca_productsoort%5B0%5D=Garde-robe',
            'https://www.mobistoxx.fr/chambre/armoire/?aca_productsoort%5B0%5D=Surmeuble',
        ];
        $array12=[
            'https://www.mobistoxx.fr/salle-a-manger/buffet/?aca_productsoort%5B0%5D=Biblioth%C3%A8que',
            'https://www.mobistoxx.fr/salle-a-manger/buffet/?aca_productsoort%5B0%5D=Buffet%2Fbahut',
            'https://www.mobistoxx.fr/salle-a-manger/buffet/?aca_productsoort%5B0%5D=Commode',
            'https://www.mobistoxx.fr/salle-a-manger/buffet/?aca_productsoort%5B0%5D=Console',
            'https://www.mobistoxx.fr/salle-a-manger/buffet/?aca_productsoort%5B0%5D=Vaisselier',
            'https://www.mobistoxx.fr/salle-a-manger/buffet/?aca_productsoort%5B0%5D=Vitrine',
            'https://www.mobistoxx.fr/salle-a-manger/chaise-salle-a-manger/',
        ];
        $array13=[
            'https://www.mobistoxx.fr/salle-a-manger/table-salle-a-manger-rectangulaire/',
            'https://www.mobistoxx.fr/salle-a-manger/vitrine/',
            'https://www.mobistoxx.fr/salle-a-manger/table-salle-a-manger-avec-allonges/',
            'https://www.mobistoxx.fr/salle-a-manger/tabouret-de-bar/?aca_productsoort%5B0%5D=Tabouret+de+bar',
            'https://www.mobistoxx.fr/salle-a-manger/tabouret-de-bar/?aca_productsoort%5B0%5D=Chaise',
        ];
        $array14=[
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Armoire',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Biblioth%C3%A8que',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Bloc-tiroir',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Commode',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Buffet%2Fbahut',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Console',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Vaisselier',
            'https://www.mobistoxx.fr/salon/buffet/?aca_productsoort%5B0%5D=Vitrine',
        ];
        $array15=[
            'https://www.mobistoxx.fr/salon/canape/',
            'https://www.mobistoxx.fr/salon/canape-convertible/?aca_productsoort%5B0%5D=Canap%C3%A9',
            'https://www.mobistoxx.fr/salon/canape-convertible/?aca_productsoort%5B0%5D=Canap%C3%A9+convertible',
            'https://www.mobistoxx.fr/salon/canape-d-angle/?aca_productsoort%5B0%5D=Canap%C3%A9',
            'https://www.mobistoxx.fr/salon/canape-d-angle/?aca_productsoort%5B0%5D=Canap%C3%A9+convertible',
            'https://www.mobistoxx.fr/salon/colonne/',
        ];
        $array16=[
            'https://www.mobistoxx.fr/salon/etagere/?aca_productsoort%5B0%5D=Armoire',
            'https://www.mobistoxx.fr/salon/etagere/?aca_productsoort%5B0%5D=Biblioth%C3%A8que',
            'https://www.mobistoxx.fr/salon/etagere/?aca_productsoort%5B0%5D=%C3%89tag%C3%A8re',
            'https://www.mobistoxx.fr/salon/etagere/?aca_productsoort%5B0%5D=Portemanteau',
            'https://www.mobistoxx.fr/salon/meuble-salon/',
            'https://www.mobistoxx.fr/salon/miroir/',
            'https://www.mobistoxx.fr/salon/mur-tv/?aca_productsoort%5B0%5D=Meuble+tv-hifi',
            'https://www.mobistoxx.fr/salon/mur-tv/?aca_productsoort%5B0%5D=Mur+tv',
        ];
        $array17=[
            'https://www.mobistoxx.fr/salon/meuble-tv/?aca_productsoort%5B0%5D=Meuble+tv-hifi',
            'https://www.mobistoxx.fr/salon/meuble-tv/?aca_productsoort%5B0%5D=Mur+tv',
            'https://www.mobistoxx.fr/salon/meuble-tv/?aca_productsoort%5B0%5D=Surmeuble',
            'https://www.mobistoxx.fr/salon/meuble-tv/?aca_productsoort%5B0%5D=Table+basse',
        ];
        $array18=[
            'https://www.mobistoxx.fr/salon/plaids/',
            'https://www.mobistoxx.fr/salon/porte-separatrice-de-pieces/',
            'https://www.mobistoxx.fr/salon/table-basse/',
            'https://www.mobistoxx.fr/salon/vitrine/',
        ];
        $array19=[
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Bleu',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Orange',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Rouge',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Vert',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Noir',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Brun',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Gris',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Blanc',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Jaune',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Rose',
            'https://www.mobistoxx.fr/salon/tapis/?ak_kleurgroep%5B0%5D=Or',
        ];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'febflow/'],#..
            // 'x1' => ['source' => $array1, 'target' => 'mobistoxx001.xms006.site/'],#..
            // 'x2' => ['source' => $array2, 'target' => 'mobistoxx002.xms006.site/'],#..
            // 'x3' => ['source' => $array3, 'target' => 'mobistoxx003.xms006.site/'],#---
            // 'x4' => ['source' => $array4, 'target' => 'mobistoxx004.xms006.site/'],#..
            // 'x5' => ['source' => $array5, 'target' => 'mobistoxx005.xms006.site/'],#..
            // 'x6' => ['source' => $array6, 'target' => 'mobistoxx006.xms006.site/'],#
            // 'x7' => ['source' => $array7, 'target' => 'mobistoxx007.xms006.site/'],#
            // 'x8' => ['source' => $array8, 'target' => 'mobistoxx008.xms006.site/'],#
            // 'x9' => ['source' => $array9, 'target' => 'mobistoxx009.xms006.site/'],#
            // 'x10' => ['source' => $array10, 'target' => 'mobistoxx010.xms006.site/'],#
            // 'x11' => ['source' => $array11, 'target' => 'mobistoxx011.xms006.site/'],#
            'x12' => ['source' => $array12, 'target' => 'mobistoxx012.xms006.site/'],#
            'x13' => ['source' => $array13, 'target' => 'mobistoxx013.xms006.site/'],#
            'x14' => ['source' => $array14, 'target' => 'mobistoxx014.xms006.site/'],#
            'x15' => ['source' => $array15, 'target' => 'mobistoxx015.xms006.site/'],#
            'x16' => ['source' => $array16, 'target' => 'mobistoxx016.xms006.site/'],#
            'x17' => ['source' => $array17, 'target' => 'mobistoxx017.xms006.site/'],#
            'x18' => ['source' => $array18, 'target' => 'mobistoxx018.xms006.site/'],#
            'x19' => ['source' => $array19, 'target' => 'mobistoxx019.xms006.site/'],#
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.mobistoxx.fr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
        // 'ck_7fffef510d96e62167bcef39f98a56a9152c1846',
        // 'cs_6eb1735b7f2518fb47efba6b908410ca71c86bce',
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
            // $response = $this->guzzleHttpClient->request('GET', $source);
            // $contents = $response->getBody()->getContents();
            // $crawler = new Crawler($contents);
            // if($crawler->filter('.block-category a')->count()){
            //     $crawler->filter('.wrapper a')->each(function(Crawler $node1,$i){
            //         $response = $this->guzzleHttpClient->request('GET', $node1->attr('href'));
            //         $contents = $response->getBody()->getContents();
            //         $crawler = new Crawler($contents);
            //         if($crawler->filter('#narrow-by-list .collapsible-container')->eq(0)->filter('.filter-options-content a')->count()){
            //             $crawler->filter('#narrow-by-list .collapsible-container')->eq(0)->filter('.filter-options-content a')->each(function(Crawler $node2,$i){
            //                 $this->processPage($node2->attr('href'));
            //             });
            //         }else{
            //             $this->processPage($node1->attr('href'));
            //         }
            //     });
            // }else{
            //     if($crawler->filter('#narrow-by-list .collapsible-container')->eq(0)->filter('.filter-options-content a')->count()){
            //         $crawler->filter('#narrow-by-list .collapsible-container')->eq(0)->filter('.filter-options-content a')->each(function(Crawler $node2,$i){
            //             $this->processPage($node2->attr('href'));
            //         });
            //     }else{
            //         $this->processPage($source);
            //     }
            // }
        }
        // $this->crawlerProduct('https://www.totaltools.com.au/redemption-bonus-offers/140089-bosch-12v-2-0ah-drill-driver-kit-0615990l63');
    }

    protected function processPage($url)
    {
        echo $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[title="Suiv."]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }    
    }

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li')->each(function (Crawler $node) {
            return $node->text();
        }));
        if($crawler->filter('.filter-value')->count()){
            array_push($breadcrumbs,$crawler->filter('.filter-value')->text());
        }
        $crawler->filter('.product-item-link')->each(function (Crawler $node,$i)use(&$breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf($node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                ]);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }      
        });
    }

    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.page-title')->text();
        $product['price'] = str_replace(',','.',str_replace(' €','',str_replace('$','',$crawler->filter('[property="product:price:amount"]')->attr('content'))));
        if($crawler->filter('[data-th="Collection"]')->count()){
            $product['brand']=$crawler->filter('[data-th="Collection"]')->text();
        }else{
            $product['brand']='mobistoxx';
        }
        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('.breadcrumbs a')->each(function (Crawler $node) {
        //     return $node->text();
        // }));
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['sku'] = str_replace("..","",$crawler->filter('[data-th="Barcode"]')->text());
        // $product['short_description']= $crawler->filter('.swatch-opt')->html();
        $product['short_description']= "";
        if($crawler->filter('#description')->count()){
            $product['description'] = $crawler->filter('#description')->html();
        }else{
            $product['description'] = "";
        }
        
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = "";
        $product['subCategory']=' ';
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        //图片   
        $img = [];
        $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$img){
            if(strstr($node->text(),'mage/gallery/gallery')){
                $img = json_decode($node->text(),true)['[data-gallery-role=gallery-placeholder]']['mage/gallery/gallery']['data'];
            }      
        });
        for($i=0;$i<count($img);$i++){
            $images[$i]=[
                'src'=>$img[$i]['full'],
                'name'=>$product['title'],
            ];
        };
        // print_r($images);exit;
        $product['images']=$images;
        // $product['images']=$this->toEncryptImage($images);
        $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        // print_r($product['variations']);exit;
        if($crawler->filter("#product-attribute-specs-table")->count()) {
            $count = $crawler->filter("#product-attribute-specs-table")->filter('tbody>tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter("#product-attribute-specs-table")->filter('tbody>tr>th')->eq($i)->text(),
                    'options' => [$crawler->filter("#product-attribute-specs-table")->filter('tbody>tr>td')->eq($i)->text()],
                ];
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);exit;
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
    }

    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
