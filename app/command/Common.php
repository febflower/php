<?php

namespace app\command;

use Automattic\WooCommerce\Client;
use think\console\Input;
use think\console\Output;

trait Common
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $guzzleHttpClient;
    /**
     * @var Client
     */
    protected $woocommerce;
    /**
     * @var array
     */
    private $tags;
    /**
     * @var array
     */
    private $sites;
    /**
     * @var array
     */
    private $processSite;

    /**
     * 初始化
     * @param Input $input An InputInterface instance
     * @param Output $output An OutputInterface instance
     */
    protected function initialize(Input $input, Output $output)
    {
        $this->sites = [
            'flirtywomen' => ['source' => 'www.flirtywomen.com', 'target' => 'www.kayinhome.com'],
            'belowthebelt' => ['source' => 'www.belowthebelt.com', 'target' => 'www.onecarveonline.com'],
            'efcollection' => ['source' => 'www.efcollection.com', 'target' => 'www.katariinausa.com'],
            'arcteryx' => ['source' => 'arcteryx.co.nz', 'target' => 'www.topalissallc.com'],
            'diamond' => ['source' => 'www.diamondsportgear.com', 'target' => 'diamond.raleighsite.site'],
            'dollboxx' => ['source' => 'www.dollboxx.com.au', 'target' => 'www.dollboxxoutlet.com'],
            'andieswim' => ['source' => 'www.andieswim.com.au', 'target' => 'www.andieonline.com'],
            'swimwearworld' => ['source' => 'www.swimwearworld.com', 'target' => 'www.theswimwearworld.com'],
            'cupshe' => ['source' => 'www.cupshe.com', 'target' => 'www.thecupshe.com'],
            'pokeetackle' => ['source' => 'www.pokeetackle.com', 'target' => 'www.fishingpokeetackle.com'],
            'stio' => ['source' => 'www.stio.com', 'target' => 'www.thestioshop.com'],
            'lahana' => ['source' => 'us.lahana.co', 'target' => 'www.theuslahana.com'],
            'youthbaseballgloves' => ['source' => 'www.youthbaseballgloves.com', 'target' => 'www.youthbaseballsonline.com'],
            'sourceforsports' => ['source' => 'www.sourceforsports.ca', 'target' => 'www.sourceforonline.com'],
            'simplyhe' => ['source' => 'www.simplyhe.com', 'target' => 'www.thesimplyhe.com'],
            'upnext' => ['source' => 'en.up-next.com.hk', 'target' => 'www.theupnext.com'],
            'usagundamstore' => ['source' => 'www.usagundamstore.com', 'target' => 'www.theusagundamstore.com'],
            'heroinesport' => ['source' => 'www.heroinesport.com', 'target' => '3tm0e9m.seo014.site'],
            'faoschwarz' => ['source' => 'faoschwarz.com', 'target' => '36yvh2v.seo014.site'],
        ];
        $this->processSite = $this->sites['faoschwarz'];

        $this->guzzleHttpClient = $guzzleHttp = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => "https://{$this->processSite['source']}/",
            'headers' => [
                'Accept' => 'application/json',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
            ],
        ]);

//        $this->woocommerce = $woocommerce = new Client('https://wp.gokgarden.com', 'ck_6e7221763e2a7751cd3a9cca985660addd8aa128', 'cs_108984b2920bf09f7c4c9e7db42eb9a1fcb82997', [
        $this->woocommerce = new Client(
            'http://mkzy'  ,
            'ck_f51d8ee1eb6b472ab4cd0eb3f86639bb3baefe86',
            'cs_745b66f86fdb959646fefb0fd62b1a5c90ec5881',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function getProductTag()
    {
        for ($x = 1; $x <= 17; $x++) {
            $this->output->info($x);
            $wooProductTag = $this->woocommerce->get('products/tags', ['per_page' => 100, 'page' => $x]);
            foreach ($wooProductTag as $tag) {
                $key = slugify($tag->name);
                $this->tags[$key] = $tag->id;
            }
        }
    }

    protected function createCategory($categoryName, $parentCategory, $parent = 0)
    {
        $this->output->info($categoryName);

        if (!isset($this->categoryIds[$parent])) $this->categoryIds[$parent] = [];

        $slug = slugify(trim($categoryName));
        if (isset($this->categories[$slug])) {
            if (in_array($this->categories[$slug], $this->categoryIds[$parent])) {
                return $this->categories[$slug];
            }
            $slug = slugify(trim($parentCategory . ' ' . $categoryName));
            if (isset($this->categories[$slug]) && in_array($this->categories[$slug], $this->categoryIds[$parent])) {
                return $this->categories[$slug];
            }
        } else {
            $slug2 = slugify(trim($parentCategory . ' ' . $categoryName));
            $checkCategory = $this->woocommerce->get('products/categories', ['slug' => $slug2]);
            if ($checkCategory) {
                $slug = $slug2;
                $category = $checkCategory;
            }
        }

        if (!isset($category)) $category = $this->woocommerce->get('products/categories', ['slug' => $slug]);

        if (empty($category)) {
            $this->output->info('Creat Category');
            $category = $this->woocommerce->post('products/categories', [
                'name' => ucfirst($categoryName),
                'slug' => $slug,
                'parent' => $parent,
            ]);
            $this->categoryIds[$parent][] = $category->id;
            return $this->categories[$slug] = $category->id;
        } else {
            $this->categoryIds[$parent][] = $category[0]->id;
            return $this->categories[$slug] = $category[0]->id;
        }
    }

    protected function createProductTag($tags)
    {
        $tagIds = $data = [];

        foreach ($tags as $tagName) {
            $tt = explode('=>', $tagName);
            $tagName = trim($tt[1] ?? $tt[0]);

            $key = slugify($tagName);
            if (isset($this->tags[$key])) {
                $tagIds[] = ['id' => $this->tags[$key]];
            } else {
                $data['create'][] = ['name' => $tagName];
            }
        }

        if ($data) {
            $wooProductTag = $this->woocommerce->post('products/tags/batch', $data);
            foreach ($wooProductTag->create as $tag) {
                if (isset($tag->error)) {
                    $tagIds[] = ['id' => $tag->error->data->resource_id];
                } else {
                    $key = slugify($tag->name);
                    $this->tags[$key] = $tag->id;
                    $tagIds[] = ['id' => $tag->id];
                }
            }
        }

        return $tagIds;
    }

    protected function createProduct($product, $categoryId)
    {
        $wooProduct = $this->woocommerce->get('products', ['slug' => $product['handle']]);
        if ($wooProduct) {
            $this->output->info('>>> Product Exist');

            $exist = true;
            $data['categories'] = [];
            foreach ($wooProduct[0]->categories as $category) {
                if ($category->id !== $categoryId) $exist = false;
                $data['categories'][] = ['id' => $category->id];
            }
            if ($exist === false) {
                $data['categories'][] = ['id' => $categoryId];
                $wooProduct = $this->woocommerce->put('products/' . $wooProduct[0]->id, $data);
                $this->output->info('>>> Update Product Category');
            }

            return;
        }

        $data = [
            'name' => $product['title'],
            'slug' => $product['handle'],
            'type' => 'variable',
            'description' => $product['body_html'],
            'categories' => [],
            'attributes' => [],
            'images' => [],
        ];

        $data['tags'] = $this->createProductTag($product['tags']);

        if ($product['vendor']) {
            $data['meta_data'][] = ['key' => 'vendor', 'value' => $product['vendor']];

            if (strpos(strtolower($product['title']), strtolower($product['vendor'])) === false) {
                $data['name'] = sprintf('%s %s', $product['vendor'], $product['title']);
            }
        }

        $data['categories'][] = [
            'id' => $categoryId
        ];
        foreach ($product['options'] as $option) {
            $data['attributes'][] = [
                'name' => $option['name'],
                'position' => $option['position'],
                'visible' => true,
                'variation' => true,
                'options' => $option['values']
            ];
        }
        foreach ($product['images'] as $image) {
            $data['images'][] = [
                'src' => $image['src'],
                'name' => $image['alt'],
            ];
        }

        $this->output->info('>>> Create Product');
        $wooProduct = $this->woocommerce->post('products', $data);

        $imageMap = [];
        foreach ($wooProduct->images as $key => $image) {
            $imageId = $product['images'][$key]['id'];
            $imageMap[$imageId] = $image->id;
        }

        $variationData = [
            'create' => []
        ];

        $sale_price = $product['variants'][0]['price'] > 99 ? rand(85, 99) : $product['variants'][0]['price'] * 0.8;
        $regular_price = $product['variants'][0]['compare_at_price'] ?? $product['variants'][0]['price'];
        $regular_price = $regular_price > 800 ? rand(500, 800) : $regular_price;
        foreach ($product['variants'] as $key => $variant) {
            $variationData['create'][$key] = [
                'regular_price' => $regular_price,
                'sale_price' => $sale_price,
                'sku' => $variant['id'],
            ];

            if (isset($variant['featured_image']['id'])) {
                $variationData['create'][$key]['image'] = ['id' => $imageMap[$variant['featured_image']['id']]];
            }

            if ($variant['option1']) {
                $variationData['create'][$key]['attributes'][] = [
                    'name' => $product['options'][0]['name'],
                    'option' => $variant['option1']
                ];
            }
            if ($variant['option2']) {
                $variationData['create'][$key]['attributes'][] = [
                    'name' => $product['options'][1]['name'],
                    'option' => $variant['option2']
                ];
            }
            if ($variant['option3']) {
                $variationData['create'][$key]['attributes'][] = [
                    'name' => $product['options'][2]['name'],
                    'option' => $variant['option3']
                ];
            }
        }

        $this->output->info('>>> Create Product Variations');
        $wooProductVariations = $this->woocommerce->post(sprintf('products/%u/variations/batch', $wooProduct->id), $variationData);
//        print_r($wooProductVariations);
    }
}
