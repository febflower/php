<?php


namespace app\command\MSK\De;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class Check24DE extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:check24')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.check24.de/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array = ["https://www.check24.de/suche/?q=4k+fernseher&params=sort%3Apopularity&page=1"];
        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'msk'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.check24.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",

            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $raw_page = $crawler->filter('[type="text/javascript"]')->eq(1)->text();
        preg_match_all('/pages\":\d+/', $raw_page, $med_page);
        $page = str_replace("pages\":", "", $med_page[0])[0];
        for ($i = 2; $i <= intval($page); $i++) {
            if (strpos($url, "page=")) {
                $j = strpos($url, "page=");
                $new_url = substr($url, 0, $j + strlen("page=")).$i;
                $this->processProductList($new_url);
            } else {
                $new_url = $url.$i;
                $this->processProductList($new_url);
            }
        }
    }

    protected
    function processProductList(Crawler $crawler)
    {

        $crawler->filter('.product-list-tile__info__name a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }


    protected
    function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $product['title'] = $crawler->filter('.product-detail-page-title')->text();
        $product['price'] = json_decode($crawler->filter('[type="application/ld+json"]')->last()->text(), true)['offers']['price'];
        $product['brand'] = json_decode($crawler->filter('[type="application/ld+json"]')->last()->text(), true)['offers']['seller']['name'];
        $product['type'] = 'simple';


        $images = array_unique(array_filter($crawler->filter('.product-detail-slide-image__image picture img')->each(function (Crawler $node, $i) {
            return $node->attr("src");
        })));

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs__item')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 3);

        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

        if ($crawler->filter(".product-detail-block-attributes__table table tr")->count()) {
            $count = $crawler->filter(".product-detail-block-attributes__table table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('th')->text(),
                    'options' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('td')->text(),
                ];
            }
        }
        $del = ['1. Achetez une machine de la sélection VIPros :', 'Cumulez 15 points VIPros', '2. Créditez vos points VIPros sur ', '3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux', 'Bosch met en place le VERY IMPORTANT PROMOS', 'http://www.vipros.fr', 'Description du', 'Description'];
        $product['description'] = str_replace($del, '', $crawler->filter('.product-detail-block-description__collapse')->html());
        $product['short_description'] = $crawler->filter('.product-detail-product-features__list')->html();
        $product['sku'] = $crawler->filter('.js-product-wrapper')->attr('data-content-id');

//        print_r($product);


//        exit();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected
    function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}