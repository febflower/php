<?php

namespace app\command\MSK\De;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class FranzenDe extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:franzende')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.franzen.de/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array = [
            "https://www.franzen.de/de/produkte/lifestyle-fashion/mode-accessories/",
            "https://www.franzen.de/de/produkte/lifestyle-fashion/taschen/",
            "https://www.franzen.de/de/produkte/lifestyle-fashion/portemonnaie-geldboersen/"
        ];
        $array2 = [
            "https://www.franzen.de/de/produkte/papeterie-schreibgeraete/schreibgeraete/",
            "https://www.franzen.de/de/produkte/lifestyle-fashion/geldklammern/",
            "https://www.franzen.de/de/produkte/lifestyle-fashion/karten-visitenkartenetuis/",
            "https://www.franzen.de/de/produkte/lifestyle-fashion/schluesselanhaenger-etuis/"
        ];
        $array3 = [
            "https://www.franzen.de/de/produkte/papeterie-schreibgeraete/schreibgeraete/",
            "https://www.franzen.de/de/produkte/papeterie-schreibgeraete/stiftetuis/",
            "https://www.franzen.de/de/produkte/papeterie-schreibgeraete/schreibtischaccessoires/",
            "https://www.franzen.de/de/produkte/papeterie-schreibgeraete/schreibmappen/",
            "https://www.franzen.de/de/produkte/papeterie-schreibgeraete/notizbuecher/"
        ];
        $array4 = [
            "https://www.franzen.de/de/produkte/home-accessoires/leuchter-kerzen/?p=9",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/anhaenger/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/anhaenger-m.-kette/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/armbaender/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/armreifen/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/halsreifen/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/ketten/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/manschettenknoepfe/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/ohrhaenger/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/ohrstecker/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/ohrclips/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/ringe/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/damenuhren/",
//            "https://www.franzen.de/de/produkte/schmuck-uhren/herrenuhren/",
        ];
        $array5 = [
            "https://www.franzen.de/de/produkte/kueche-kochen/kochgeschirr/",
            "https://www.franzen.de/de/produkte/kueche-kochen/tee-co./",
            "https://www.franzen.de/de/produkte/kueche-kochen/thermobecher/",
            "https://www.franzen.de/de/produkte/kueche-kochen/trinkflaschen/",
            "https://www.franzen.de/de/produkte/kueche-kochen/vorratsdosen/",
        ];
        $array6 = [
            "https://www.franzen.de/de/produkte/kueche-kochen/kuechenhelfer/",
            "https://www.franzen.de/de/produkte/kueche-kochen/salat-co./",
            "https://www.franzen.de/de/produkte/kueche-kochen/brotkoerbe-kaesten/"
        ];
        $array7 = [
            "https://www.franzen.de/de/produkte/kueche-kochen/messer-zubehoer/",
            "https://www.franzen.de/de/produkte/kueche-kochen/kaffee-co./",
            "https://www.franzen.de/de/produkte/kueche-kochen/kuechenkleingeraete/",
            "https://www.franzen.de/de/produkte/kueche-kochen/kuechentextilien/",
        ];
        $array8 = [
            "https://www.franzen.de/de/produkte/kueche-kochen/bar-accessories/",
            "https://www.franzen.de/de/produkte/kueche-kochen/streuer-muehlen-moerser/",
            "https://www.franzen.de/de/produkte/kueche-kochen/isolierkannen/",
        ];
        $array9 = [
            "https://www.franzen.de/de/produkte/home-accessoires/bilderrahmen/",
            "https://www.franzen.de/de/produkte/home-accessoires/decken-und-kissen/",
            "https://www.franzen.de/de/produkte/home-accessoires/dosen-boxen/",
            "https://www.franzen.de/de/produkte/home-accessoires/windlichter/",
        ];
        $array10 = [
            "https://www.franzen.de/de/produkte/home-accessoires/figuren-objekte/",
            "https://www.franzen.de/de/produkte/home-accessoires/globen/",
            "https://www.franzen.de/de/produkte/home-accessoires/lampen/",
        ];
        $array11 = [
            "https://www.franzen.de/de/produkte/home-accessoires/leuchter-kerzen/",
            "https://www.franzen.de/de/produkte/home-accessoires/raucheraccessories/",
            "https://www.franzen.de/de/produkte/home-accessoires/raumduefte/",
        ];
        $array12 = [
            "https://www.franzen.de/de/produkte/home-accessoires/rund-ums-bad/",
            "https://www.franzen.de/de/produkte/home-accessoires/schalen/",
            "https://www.franzen.de/de/produkte/home-accessoires/tabletts/",
        ];
        $array13 = [
            "https://www.franzen.de/de/produkte/home-accessoires/tischsets-untersetzer/",
            "https://www.franzen.de/de/produkte/home-accessoires/vasen/",
            "https://www.franzen.de/de/produkte/home-accessoires/wanduhren-wetterstationen/",
        ];

        $array14 = [
            "https://www.franzen.de/de/produkte/tisch-tafel/trinkglaeser/",
            "https://www.franzen.de/de/produkte/tisch-tafel/glas-accessoires/",
            "https://www.franzen.de/de/produkte/tisch-tafel/pflege-reinigungsmittel/"
        ];
        $array15 = [
            "https://www.franzen.de/de/produkte/home-accessoires/leuchter-kerzen/",
            "https://www.franzen.de/de/produkte/tisch-tafel/tafelsilber/",
            "https://www.franzen.de/de/produkte/tisch-tafel/kruege-karaffen-dekanter/",
            "https://www.franzen.de/de/produkte/tisch-tafel/serviettenringe/"
        ];


        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'franzen001.xms011.site'],
            "x2" => ['source' => $array2, 'offset' => 1, 'target' => 'franzen002.xms011.site'],
            "x3" => ['source' => $array3, 'offset' => 1, 'target' => 'franzen003.xms011.site'],
            "x4" => ['source' => $array4, 'offset' => 1, 'target' => 'franzen004.xms011.site'],
            "x5" => ['source' => $array5, 'offset' => 1, 'target' => 'franzen005.xms011.site'],
            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'franzen006.xms011.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'franzen007.xms011.site'],
            "x8" => ['source' => $array8, 'offset' => 1, 'target' => 'franzen008.xms011.site'],
            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'franzen009.xms011.site'],
            "x10" => ['source' => $array10, 'offset' => 1, 'target' => 'franzen010.xms011.site'],
            "x11" => ['source' => $array11, 'offset' => 1, 'target' => 'franzen011.xms011.site'],
            "x12" => ['source' => $array12, 'offset' => 1, 'target' => 'franzen012.xms011.site'],
            "x13" => ['source' => $array13, 'offset' => 1, 'target' => 'franzen013.xms011.site'],
            "x14" => ['source' => $array14, 'offset' => 1, 'target' => 'franzen014.xms011.site'],
            "x15" => ['source' => $array15, 'offset' => 1, 'target' => 'franzen015.xms011.site'],

        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.franzen.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.product--info .product--title')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->attr("title"),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('[property="og:title"]')->attr("content");
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr("content");
        $product['brand'] = $crawler->filter('[property="product:brand"]')->attr("content");
        $product['type'] = 'simple';
        $images = array_unique(array_filter($crawler->filter('.image--box .image--element')->each(function (Crawler $node, $i) {
            return $node->attr("data-img-original");
        })));

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb--title')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 0);

        $parent = 0;
        $parentCategory = '';

        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

        if ($crawler->filter(".product-detail-block-attributes__table table tr")->count()) {
            $count = $crawler->filter(".product-detail-block-attributes__table table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('th')->text(),
                    'options' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('td')->text(),
                ];
            }
        }
        $product['description'] = $crawler->filter('.product--description')->html();
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->attr('content');

        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}