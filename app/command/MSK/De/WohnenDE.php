<?php

namespace app\command\MSK\De;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class WohnenDE extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:wohnende')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.wohnen.de/');
    }

    protected function initialize(Input $input, Output $output)
    {

        $array1 = ["https://www.wohnen.de//schlafzimmer/betten/einzelbetten.html"];
        $array2 = ["https://www.wohnen.de//schlafzimmer/betten/komfortbetten.html"];
        $array3 = ["https://www.wohnen.de//esszimmer/esszimmerstuehle/freischwinger.html"];
        $array4 = ["https://www.wohnen.de//kueche/kuechenstuehle/freischwinger.html"];
        $array5 = ["https://www.wohnen.de//moebel/designertische.html"];
        $array6 = ["https://www.wohnen.de//schlafzimmer/betten/futonbetten.html"];
        $array7 = ["https://www.wohnen.de//badezimmer/badschraenke/badhochschraenke.html"];
        $array8 = ["https://www.wohnen.de//badezimmer/badschraenke/badmehrzweckschraenke.html"];
        $array9 = ["https://www.wohnen.de//schlafzimmer/nachttische/massivholznachttische.html"];
        $array10 = ["https://www.wohnen.de//schlafzimmer/kleiderschraenke/drehtuerenschraenke.html"];
        $array11 = ["https://www.wohnen.de//wohnzimmer/sofas/3-sitzer-sofa.html"];
        $array12 = ["https://www.wohnen.de//wohnzimmer/wohnzimmerschraenke/buecherschraenke.html"];
        $array13 = ["https://www.wohnen.de//buero/buerotische/schreibtische.html"];

        $array14 = ["https://www.wohnen.de//badezimmer/komplett/badmoebel-sets.html"];
        $array15 = ["https://www.wohnen.de//wohnzimmer/paravents/spanische-wand.html"];
        $array16 = ["https://www.wohnen.de//esszimmer/esszimmertische/massivholztische.html"];
        $array17 = ["https://www.wohnen.de//schlafzimmer/betten/doppelbetten.html"];
        $array18 = ["https://www.wohnen.de//wohnzimmer/tv-hifi-moebel/tv-lowboards.html"];
        $array19 = ["https://www.wohnen.de//wohnzimmer/tv-hifi-moebel/tv-schraenke.html"];
        $array20 = ["https://www.wohnen.de//wohnzimmer/sideboards-kommoden/sideboards.html"];

        $array21 = ["https://www.wohnen.de//esszimmer/sideboards-kommoden/sideboards.html"];
        $array22 = ["https://www.wohnen.de//schlafzimmer/sideboards-kommoden/sideboards.html"];
        $array23 = ["https://www.wohnen.de//wohnzimmer/tv-hifi-moebel/fernsehtische.html"];
        $array24 = ["https://www.wohnen.de//wohnzimmer/sideboards-kommoden/lowboards.html"];
        $array25 = ["https://www.wohnen.de//wohnzimmer/tv-hifi-moebel/tv-sideboard.html"];
        $array26 = ["https://www.wohnen.de//esszimmer/sideboards-kommoden/lowboards.html"];
        $array27 = ["https://www.wohnen.de//schlafzimmer/sideboards-kommoden/lowboards.html"];
        $array28 = ["https://www.wohnen.de//schlafzimmer/betten/holzbetten.html"];
        $array29 = ["https://www.wohnen.de//schlafzimmer/kleiderschraenke/schlafzimmerschraenke.html"];
        $array30 = ["https://www.wohnen.de//esszimmer/essgruppen/essgruppen-aus-massivholz.html"];

        $array31 = ["https://www.wohnen.de//schlafzimmer/betten/massivholzbetten.html?p=3"];
        $array32 = ["https://www.wohnen.de//moebel/massivholzschraenke-1.html?p=4"];
        $array33 = ["https://www.wohnen.de//esszimmer/esszimmerstuehle/massivholzstuehle.html?p=4"];
        $array34 = ["https://www.wohnen.de//kueche/kuechenstuehle/massivholzstuehle.html?p=4"];
        $array35 = ["https://www.wohnen.de//wohnzimmer/regale/standregale.html?p=4"];
        $array36 = ["https://www.wohnen.de//esszimmer/esszimmerregale/standregale.html?p=4"];
        $array37 = ["https://www.wohnen.de//kueche/kuechenstuehle/lederstuehle.html?p=4"];
        $array38 = ["https://www.wohnen.de//kueche/barhocker/hochhocker.html?p=3"];
        $array39 = ["https://www.wohnen.de//esszimmer/essgruppen/tische-und-stuehle.html?p=4"];
        $array40 = ["https://www.wohnen.de//esszimmer/esszimmerstuehle/lederstuehle.html?p=4"];
        $array41 = ["https://www.wohnen.de//kueche/barhocker/sitzhocker.html?p=4"];
        $array42 = ["https://www.wohnen.de//wohnzimmer/sofas/wohnzimmer-couch.html?p=4"];
        $array43 = ["https://www.wohnen.de//diele/garderoben/wandgarderoben.html?p=4"];

        $array44 = ["https://www.wohnen.de//wohnzimmer/regale/buecherregale.html"];
        $array45 = ["https://www.wohnen.de//kinderzimmer/kinderbetten/kinderhochbetten.html"];
        $array46 = ["https://www.wohnen.de//wohnzimmer/wohnwaende/anbauwand.html"];
        $array47 = ["https://www.wohnen.de//wohnzimmer/vitrinen/standvitrinen.html"];
        $array48 = ["https://www.wohnen.de//diele/garderoben/garderoben-set.html"];
        $array49 = ["https://www.wohnen.de//esszimmer/essgruppen/moderne-essgruppen.html"];
        $array50 = ["https://www.wohnen.de//moebel/massivholzregale-1.html"];
        //
        $array51 = ["https://www.wohnen.de//wohnzimmer/regale/wandregale.html"];
        $array52 = ["https://www.wohnen.de//diele/flurspiegel/garderobenspiegel.html"];
        $array53 = ["https://www.wohnen.de//diele/flurspiegel/wandspiegel.html"];
        $array54 = ["https://www.wohnen.de//wohnzimmer/sofas/einzelsofa.html"];
        $array55 = ["https://www.wohnen.de//esszimmer/essgruppen/essgruppen-mit-4-stuehlen.html"];

        $array56=["https://www.wohnen.de//esszimmer/vitrinen/massivholzvitrinen.html"];
        $array57=["https://www.wohnen.de//esszimmer/esszimmerbaenke/lederbaenke.html"];
        $array58=["https://www.wohnen.de//schlafzimmer/sideboards-kommoden/kommoden.html"];
        $array59=["https://www.wohnen.de//buero/bueroschraenke/aktenschraenke.html"];
        $array60=["https://www.wohnen.de//buero/buerostuehle/buerodrehstuhl.html"];
        $array61=["https://www.wohnen.de//esszimmer/esszimmertische/ausziehbare-esstische.html"];
        $array62=["https://www.wohnen.de//kinderzimmer/kinderbetten/kinderbett-halbhoch.html"];
        $array63=["https://www.wohnen.de//wohnzimmer/vitrinen/stauraumvitrinen.html"];
        $array64=["https://www.wohnen.de//wohnzimmer/sideboards-kommoden/highboards.html"];









        $this->sites = [
            'x' => ['source' => $array1, 'offset' => 1, 'target' => 'wohnen001.xms002.site'],
            'x2' => ['source' => $array2, 'offset' => 1, 'target' => 'wohnen002.xms002.site'],
            'x3' => ['source' => $array3, 'offset' => 1, 'target' => 'wohnen003.xms002.site'],
            'x4' => ['source' => $array4, 'offset' => 1, 'target' => 'wohnen004.xms002.site'],

            'x5' => ['source' => $array5, 'offset' => 1, 'target' => 'wohnen005.xms002.site'],
            'x6' => ['source' => $array6, 'offset' => 1, 'target' => 'wohnen006.xms002.site'],
            'x7' => ['source' => $array7, 'offset' => 1, 'target' => 'wohnen007.xms002.site'],
            'x8' => ['source' => $array8, 'offset' => 1, 'target' => 'wohnen008.xms002.site'],

            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'wohnen009.xms008.site'],
            "x10" => ['source' => $array10, 'offset' => 1, 'target' => 'wohnen010.xms008.site'],
            "x11" => ['source' => $array11, 'offset' => 1, 'target' => 'wohnen011.xms008.site'],
            "x12" => ['source' => $array12, 'offset' => 1, 'target' => 'wohnen012.xms008.site'],
            "x13" => ['source' => $array13, 'offset' => 1, 'target' => 'wohnen013.xms008.site'],
            "x14" => ['source' => $array14, 'offset' => 1, 'target' => 'wohnen014.xms008.site'],
            "x15" => ['source' => $array15, 'offset' => 1, 'target' => 'wohnen015.xms008.site'],
            "x16" => ['source' => $array16, 'offset' => 1, 'target' => 'wohnen016.xms008.site'],
            "x17" => ['source' => $array17, 'offset' => 1, 'target' => 'wohnen017.xms008.site'],
            "x18" => ['source' => $array18, 'offset' => 1, 'target' => 'wohnen018.xms008.site'],
            "x19" => ['source' => $array19, 'offset' => 1, 'target' => 'wohnen019.xms008.site'],
            "x20" => ['source' => $array20, 'offset' => 1, 'target' => 'wohnen020.xms008.site'],

            "x21" => ['source' => $array21, 'offset' => 1, 'target' => 'wohnen021.xms008.site'],
            "x22" => ['source' => $array22, 'offset' => 1, 'target' => 'wohnen022.xms008.site'],
            "x23" => ['source' => $array23, 'offset' => 1, 'target' => 'wohnen023.xms008.site'],
            "x24" => ['source' => $array24, 'offset' => 1, 'target' => 'wohnen024.xms008.site'],
            "x25" => ['source' => $array25, 'offset' => 1, 'target' => 'wohnen025.xms008.site'],
            "x26" => ['source' => $array26, 'offset' => 1, 'target' => 'wohnen026.xms008.site'],
            "x27" => ['source' => $array27, 'offset' => 1, 'target' => 'wohnen027.xms008.site'],
            "x28" => ['source' => $array28, 'offset' => 1, 'target' => 'wohnen028.xms008.site'],
            "x29" => ['source' => $array29, 'offset' => 1, 'target' => 'wohnen029.xms008.site'],
            "x30" => ['source' => $array30, 'offset' => 1, 'target' => 'wohnen030.xms008.site'],

            "x31" => ['source' => $array31, 'offset' => 1, 'target' => 'wohnen031.xms008.site'],
            "x32" => ['source' => $array32, 'offset' => 1, 'target' => 'wohnen032.xms008.site'],
            "x33" => ['source' => $array33, 'offset' => 1, 'target' => 'wohnen033.xms008.site'],
            "x34" => ['source' => $array34, 'offset' => 1, 'target' => 'wohnen034.xms008.site'],
            "x35" => ['source' => $array35, 'offset' => 1, 'target' => 'wohnen035.xms008.site'],
            "x36" => ['source' => $array36, 'offset' => 1, 'target' => 'wohnen036.xms008.site'],
            "x37" => ['source' => $array37, 'offset' => 1, 'target' => 'wohnen037.xms008.site'],
            "x38" => ['source' => $array38, 'offset' => 1, 'target' => 'wohnen038.xms008.site'],
            "x39" => ['source' => $array39, 'offset' => 1, 'target' => 'wohnen039.xms008.site'],
            "x40" => ['source' => $array40, 'offset' => 1, 'target' => 'wohnen040.xms008.site'],
            "x41" => ['source' => $array41, 'offset' => 1, 'target' => 'wohnen041.xms008.site'],
            "x42" => ['source' => $array42, 'offset' => 1, 'target' => 'wohnen042.xms008.site'],
            "x43" => ['source' => $array43, 'offset' => 1, 'target' => 'wohnen043.xms008.site'],
            //
            "x44" => ['source' => $array44, 'offset' => 1, 'target' => 'wohnen044.xms009.site'],
            "x45" => ['source' => $array45, 'offset' => 1, 'target' => 'wohnen045.xms009.site'],
            "x46" => ['source' => $array46, 'offset' => 1, 'target' => 'wohnen046.xms009.site'],
            "x47" => ['source' => $array47, 'offset' => 1, 'target' => 'wohnen047.xms009.site'],
            "x48" => ['source' => $array48, 'offset' => 1, 'target' => 'wohnen048.xms009.site'],
            "x49" => ['source' => $array49, 'offset' => 1, 'target' => 'wohnen049.xms009.site'],
            "x50" => ['source' => $array50, 'offset' => 1, 'target' => 'wohnen050.xms009.site'],
            //
            "x51" => ['source' => $array51, 'offset' => 1, 'target' => 'wohnen051.xms009.site'],
            "x52" => ['source' => $array52, 'offset' => 1, 'target' => 'wohnen052.xms009.site'],
            "x53" => ['source' => $array53, 'offset' => 1, 'target' => 'wohnen053.xms009.site'],
            "x54" => ['source' => $array54, 'offset' => 1, 'target' => 'wohnen054.xms009.site'],
            "x55" => ['source' => $array55, 'offset' => 1, 'target' => 'wohnen055.xms009.site'],

            "x56" => ['source' => $array56, 'offset' => 1, 'target' => 'wohnen056.xms010.site'],
            "x57" => ['source' => $array57, 'offset' => 1, 'target' => 'wohnen057.xms010.site'],
            "x58" => ['source' => $array58, 'offset' => 1, 'target' => 'wohnen058.xms010.site'],
            "x59" => ['source' => $array59, 'offset' => 1, 'target' => 'wohnen059.xms010.site'],
            "x60" => ['source' => $array60, 'offset' => 1, 'target' => 'wohnen060.xms010.site'],
            "x61" => ['source' => $array61, 'offset' => 1, 'target' => 'wohnen061.xms010.site'],
            "x62" => ['source' => $array62, 'offset' => 1, 'target' => 'wohnen062.xms010.site'],
            "x63" => ['source' => $array63, 'offset' => 1, 'target' => 'wohnen063.xms010.site'],
            "x64" => ['source' => $array64, 'offset' => 1, 'target' => 'wohnen064.xms010.site'],


        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.wohnen.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",

            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;

        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);

        $nextNode = $crawler->filter('#p-next');
        if ($nextNode->count()) {
            if (strpos($url, "p=")) {
                $i = strpos($url, "p=");
                $orian_url = substr($url, 0, $i + 2);
            } else {
                $orian_url = $url . "?p=";
            }
            $this->processPage($orian_url . $nextNode->attr('value'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $b = $crawler->filter(".in-path")->eq(1)->text();
        $crawler->filter('.card a')->each(function (Crawler $node, $i) use (&$b) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.product-info .product-name')->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                    "b" => $b,
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.product-detail-name')->text();
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr("content");
        $product['brand'] = " ";
        $images = array_unique(array_filter($crawler->filter('.gallery-slider-thumbnails .gallery-slider-thumbnails-item .gallery-slider-thumbnails-item-inner img')->each(function (Crawler $node, $i) {
            return $node->attr("src");
        })));
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb .breadcrumb-item a')->each(function (Crawler $node) {
        return $node->text();
    }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['variations'] = [];
        $product['attributes'] = [];
        $product['tags'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
        if ($crawler->filter('[selected="selected"]')->count()) {
            $a = $crawler->filter('[selected="selected"]')->text();
            $i = strpos($a, "-");
            $opt = substr($a, 0, $i);
            $options = [$opt];
            $product['type'] = 'variable';
            $product['variations'][] = [
                'name' => 'Ausführung',
                'options' => $options,
            ];
        } else {
            $product['type'] = 'simple';
        }
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $a = $crawler->filter('.technische-daten-tab.tab-pane-container.dusHiCo')->attr("data-hico");
        if (strpos($a, "<span >Varianten</span>")) {
            $i = strpos($a, "<span >Varianten</span>");
            $raw_description = substr($a, 0, $i - strlen("<li  class=\"gruppe\">"));
            $j = strpos($raw_description, "</strong></p>");
            $product['description'] = substr($raw_description, $j + strlen("</strong></p>")) . "</ul>";
        } else {
            $k = strpos($a, "</strong></p>");
            $product['description'] = substr($a, $k + strlen("</strong></p>")) . '</ul>';
        }
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}