<?php


namespace app\command\MSK\Fr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\db\CacheItem;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class TopbizFr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:topbizfr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.topbiz.fr/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array = ["https://www.topbiz.fr/117-divers",];
        $array2 = [
            "https://www.topbiz.fr/313-câbles-fibre-optique?page=6"
        ];
        $array3 = ["https://www.topbiz.fr/78-refroidissement"];
        $array4 = ["https://www.topbiz.fr/50-micro-casques", "https://www.topbiz.fr/157-antivols", "https://www.topbiz.fr/33-tapis-de-souris"];
        $array5 = ["https://www.topbiz.fr/184-filtres?page=2", "https://www.topbiz.fr/150-chargeurs-pc-portables"];
        $array6 = [
            "https://www.topbiz.fr/937-refroidissement-pc",
            "https://www.topbiz.fr/153-points-d-accès",
            "https://www.topbiz.fr/301-c%C3%A2bles-displayport"
        ];
        $array7 = [
            "https://www.topbiz.fr/112-cables-reseau", "https://www.topbiz.fr/181-supports", "https://www.topbiz.fr/110-cables-alimentation"
        ];
        $array8 = [
            "https://www.topbiz.fr/77-boitiers", "https://www.topbiz.fr/27-adaptateurs-injecteurs", "https://www.topbiz.fr/164-onduleurs"
        ];
        $array9 = [
            "https://www.topbiz.fr/103-connecteurs", "https://www.topbiz.fr/1009-cartes-et-adaptateurs-d-interfac", "https://www.topbiz.fr/1009-cartes-et-adaptateurs-d-interfac"
        ];
        $array10 = [
            "https://www.topbiz.fr/101-cables-usb", "https://www.topbiz.fr/22-modems-routeur", "https://www.topbiz.fr/190-etuis-tablettes"
        ];
        $array11 = [
            "https://www.topbiz.fr/308-câbles-vidéo-et-adaptateurs", "https://www.topbiz.fr/34-accessoires-impression",
            "https://www.topbiz.fr/98-cartes-memoires",];
        $array12 = [
            "https://www.topbiz.fr/24-switch",
            "https://www.topbiz.fr/97-disques-durs-externes",
            "https://www.topbiz.fr/167-stations-d-accueil"
        ];

        $array13 = [
            "https://www.topbiz.fr/88-claviers",
            "https://www.topbiz.fr/131-cartes-réseau",
            "https://www.topbiz.fr/86-rack-boitiers-externes"
        ];

        $array14 = [
            "https://www.topbiz.fr/94-cles-usb",
            "https://www.topbiz.fr/36-accessoires-moniteurs",
            "https://www.topbiz.fr/287-papiers-photos"
        ];
        $array15 = [
            "https://www.topbiz.fr/119-cables-hdmi",
            "https://www.topbiz.fr/315-adaptateurs-et-connecteurs-de-câbles",
            "https://www.topbiz.fr/113-papiers"
        ];


        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'topbiz001.xms010.site'],
            'x2' => ['source' => $array2, 'offset' => 1, 'target' => 'topbiz002.xms010.site'],
            'x3' => ['source' => $array3, 'offset' => 1, 'target' => 'topbiz003.xms010.site'],
            'x4' => ['source' => $array4, 'offset' => 1, 'target' => 'topbiz004.xms010.site'],
            'x5' => ['source' => $array5, 'offset' => 1, 'target' => 'topbiz005.xms010.site'],

            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'topbiz006.xms011.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'topbiz007.xms011.site'],
            "x8" => ['source' => $array8, 'offset' => 1, 'target' => 'topbiz008.xms011.site'],
            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'topbiz009.xms011.site'],
            "x10" => ['source' => $array10, 'offset' => 1, 'target' => 'topbiz010.xms011.site'],
            "x11" => ['source' => $array11, 'offset' => 1, 'target' => 'topbiz011.xms011.site'],
            "x12" => ['source' => $array12, 'offset' => 1, 'target' => 'topbiz012.xms011.site'],
            "x13" => ['source' => $array13, 'offset' => 1, 'target' => 'topbiz013.xms011.site'],
            "x14" => ['source' => $array14, 'offset' => 1, 'target' => 'topbiz014.xms011.site'],
            "x15" => ['source' => $array15, 'offset' => 1, 'target' => 'topbiz015.xms011.site'],


        ];
//        nohup php think build:wordpress:miinto -s miinto221 > nohup/yyzz/miinto221.log 2>&1 &
//            nohup php think build:wordpress:msk:topbizfr -s x15> nohup/x15.log 2>&1 &
//            nohup php think build:wordpress:miinto -s x1> nohup/x1.log 2>&1 &

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.topbiz.fr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',

            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;

        $this->output->writeln($url);

        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.product-title')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->filter("a")->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }


    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.product-detail-name')->text();
        $price = str_replace(",", ".", $crawler->filter('.current-price-value')->last()->text());
        $raw_price = str_replace("€", "", $price);
        $product['price'] = str_replace(" ", "", $raw_price);
        $product['brand'] = $crawler->filter('.product-manufacturer a img')->attr("alt");
        $product['type'] = 'simple';
        $images = array_unique(array_filter($crawler->filter('.thumb')->each(function (Crawler $node, $i) {
            return $node->attr("data-image-large-src");
        })));
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb ol li a span')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 3);
        array_pop($product['breadcrumbs']);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

        if ($crawler->filter(".table tr")->count()) {
            $count = $crawler->filter(".table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                if ($crawler->filter(".table tr")->eq($i)->filter('td')->text() == "") {
                    continue;
                } else {
                    $product['attributes'][] = [
                        'name' => $crawler->filter(".table tr")->eq($i)->filter('th')->text(),
                        'options' => $crawler->filter(".table tr")->eq($i)->filter('td')->text(),
                    ];
                }

            }
        }

        $raw_description = explode("<br>", $crawler->filter('.product-description')->html());
        $product['description'] = end($raw_description);
//        $raw_sku = $crawler->filter('[type="application/ld+json"]')->eq(3)->text();
//        preg_match_all('/sku.*?,/', $raw_sku, $raw_choice);
//        $del = ["sku", ":",",",'"'];
//        $product['sku'] = str_replace($del,"",array_unique($raw_choice[0]));
        $product['sku'] = $crawler->filter("#product_page_product_id")->attr("value");
        $this->createProduct($product);
//        print_r($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}
