<?php

namespace app\command\MSK\Fr;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class VertbaudetFr extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:vertbaudetfr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.vertbaudet.fr/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array = ["https://www.vertbaudet.fr/bebe/body.htm",];
        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'msk'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.vertbaudet.fr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_1b1b2c578ddb9932005980a304af5e266528b190',
            'cs_af4b5f7ecbaf65180eba947f7efa67a478f9ea1c',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    public function processPage($uri)
    {

        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $nextNode = $crawler->filter('[rel="next"]');

        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }

    }

    public function processProductList(Crawler $crawler)
    {
        $crawler->filter('.product')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . json_decode($node->attr('data-tagdata'), true)['name'],
                    'url' => sprintf('%s', 'https://www.vertbaudet.fr/' . str_replace('\u0026', '&', json_decode($node->attr('data-tagdata'), true)['pageurl'])),
                     "price" => json_decode($node->attr('data-tagdata'), true)['price'],
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });

    }

//https://www.vertbaudet.fr/lot-de-2-bodies-manches-longues-bebe-ouverture-naissance-lot-bleu.htm?ProductId=700040633&FiltreCouleur=6401&t=1
///lot-de-2-bodies-manches-longues-bebe-ouverture-naissance-lot-bleu.htm?ProductId=700040633\u0026FiltreCouleur=6401\u0026t=1
    public function crawlerProduct($item)
    {
        echo "执行到这里";
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

    }

    public function getDetailData($url)
    {
//        echo "\n开始执行getDetailData函数";
        $resp = json_decode($this->request($url), true);
//        var_dump($resp['Results'][0]['Description']);
        $data = [];
        $data['name'] = $resp['Results'][0]['Name'];
        $data['description'] = str_replace("\n", "", $resp['Results'][0]['Description']);
//        echo "\n开始执行getDetailData函数";
        return $data;
    }
//https://www.vertbaudet.fr/fstrz/r/s/media.vertbaudet.fr/Pictures/vertbaudet/238256/lot-de-2-bodies-manches-longues-bebe-ouverture-naissance.jpg?width=800&frz-v=130
//https://www.vertbaudet.fr/fstrz/r/s/media.vertbaudet.fr/Pictures/vertbaudet/238336/lot-de-2-bodies-manches-longues-bebe-ouverture-naissance.jpg?width=800&frz-v=130
//https://api.bazaarvoice.com/data/products.json?passkey=srisekhlil0pycnwo3jw79er8&locale=fr_FR&allowMissing=true&apiVersion=5.4&filter=id:700040633
//https://api.bazaarvoice.com/data/products.json?passkey=srisekhlil0pycnwo3jw79er8&locale=fr_FR&allowMissing=true&apiVersion=5.4&filter=id:700040633
//https://api.bazaarvoice.com/data/products.json?passkey=srisekhlil0pycnwo3jw79er8&locale=fr_FR&allowMissing=true&apiVersion=5.4&filter=id:707001876
}