<?php

namespace app\command\MSK\Fr;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class KellerSportsFR extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:kellerSportsfr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.KellerSports.fr/');
    }
//https://www.keller-sports.fr/pd/OSCSM0A5000/?cacheResult=1666080330-2
//https://www.keller-sports.fr/pd/OSCDO04N000/?cacheResult=1666080330-2
    protected function initialize(Input $input, Output $output)
    {
        $array = ['https://www.keller-sports.fr/chaussuresdesport/femmes/nouveautes/', 'https://www.keller-sports.fr/chaussuresdesport/running-chaussuresderunning/femmes/'];
        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'msk'],
        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.keller-sports.fr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_1b1b2c578ddb9932005980a304af5e266528b190',
            'cs_af4b5f7ecbaf65180eba947f7efa67a478f9ea1c',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

//        ?
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    public function processPage($uri)
    {
        $url = $uri;

        $this->output->writeln($url);

        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);


        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');

        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    public function processProductList(Crawler $crawler)
    {
        $crawler->filter('.product-card__link')->each(function (Crawler $node, $i) {

            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', 'https://www.keller-sports.fr' . $node->attr('href')),
//                    'id'=>$node->attr('id')
                    ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    public function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $product['title'] = $crawler->filter(".dpo-product-title")->text();
        $product['price'] = $crawler->filter(".dpo-price__price-value")->text();
        $product['brand'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)["brand"]["name"];
        $product['type'] = 'simple';
//        $product['category'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)["category"];
//        $product['img'] = $crawler->filter(".product-section__product-slide>img")->attr("src");

////        print_r($product);
        $images = array_unique(array_filter($crawler->filter('.product-section__product-slide img')->each(function (Crawler $node, $i) {
            return $node->attr("src");
        })));
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
//        $product['images']=[];
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs__item')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 2,-1);

        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';

        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

        if ($crawler->filter('.shoe-size-list')->eq(0)->count()) {
            $product['variations'][] = [
                'name' => substr($crawler->filter('.product-detail-variant-selection__headline')->text(), '0', strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(), ":")),
                'options' => $crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
                    return $node->text();
                })
            ];
            $product['type'] = 'variable';
        }


        if ($crawler->filter(".keyfacts-section__table-holder table tr")->count()) {
            $count = $crawler->filter(".keyfacts-section__table-holder table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".keyfacts-section__table-holder table tr")->eq($i)->filter('.keyfacts-table__title')->text(),
                    'options' => $crawler->filter(".keyfacts-section__table-holder table tr")->eq($i)->filter('.keyfacts-table__text')->text(),
                ];
            }
        }
        $del = ['1. Achetez une machine de la sélection VIPros :', 'Cumulez 15 points VIPros', '2. Créditez vos points VIPros sur ', '3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux', 'Bosch met en place le VERY IMPORTANT PROMOS', 'http://www.vipros.fr', 'Description du', 'Description'];
        $product['description'] = $crawler->filter('[name=description]')->html();
//        $product['short_description'] = $crawler->filter('.product-detail-product-features__list')->html();
        $product['sku'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)['sku'];
        print_r($product);

        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}