<?php

namespace app\command\MSK\El;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ZakcretGr extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:zakcretgr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.zakcret.gr/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeies-mplouzes-kontomanikes/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-stivos",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-thallasis"];
        $array2 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-pantelonia/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-oreivatika",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-volley"
        ];
        $array3 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-treksimo/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/kormakia",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-trail"
        ];
        $array4 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-kolan/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-tennis",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-training"
        ];
        $array5 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeies-mplouzes/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/foustes",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-pantelonia-kapri"];
        $array6 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-sorts/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-sandalia",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-pandofla"
        ];
        $array7 = [
//            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-magio/page-8",
//            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeies-vermoudes",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-foremata/page-3"
        ];
        $array8 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-magio",
//            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeia-isothermika/page-8",
//            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/gunaikeies-zaketes",
//            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/mpoustakia"
        ];
        $array9 = [
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeia-papoutsia-eponuma/page-8",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-papoutsia/gunaikeies-pantofles-sagionares",
            "https://www.zakcret.gr/gunaikeia/gunaikeia-rouxa/mplouzes-amanikes"
        ];
        $array10 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrikes-mplouzes-kontomanikes/page-8","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-thalassis","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-stivos"];
        $array11 = ["https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-papoutsia-prosfores","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-set-formes","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-isothermika"];
        $array12 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-mplouzes/page-8","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-sandalia","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-kolan"];
        $array13 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-vermoudes/page-8","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-oreivatika","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-pantofla"];
        $array14 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrika-pantelonia-formes/page-8","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-papoutsia-tennis","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-training"];
        $array15 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-magio/page-8","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-esorouxa","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-sorts"];
        $array16 = ["https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-treksimo/page-8","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-papoutsia-basket","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-plekta"];
        $array17 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-mpoufan","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-mplouzes-amanikes","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-poukamisa"];
        $array18 = ["https://www.zakcret.gr/andrika/andrika-rouxa/andrika-rouxa-zaketes","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-pantelonia-ufasmatina","https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-pantofles-sagionares"];
        $array19 = ["https://www.zakcret.gr/andrika/andrika-papoutsia/andrika-papoutsia-modas/page-8","https://www.zakcret.gr/andrika/andrika-rouxa/andrika-magio-vermoudes","https://www.zakcret.gr/aksesouar/aksesouar-paidika/paidika-gyalia-iliou"];



        $this->sites = [
            "x1" => ['source' => $array1, 'offset' => 1, 'target' => 'zakcret001.seo077.site'],
            "x2" => ['source' => $array2, 'offset' => 1, 'target' => 'zakcret002.seo077.site'],
            "x3" => ['source' => $array3, 'offset' => 1, 'target' => 'zakcret003.seo077.site'],
            "x4" => ['source' => $array4, 'offset' => 1, 'target' => 'zakcret004.seo077.site'],
            "x5" => ['source' => $array5, 'offset' => 1, 'target' => 'zakcret005.seo077.site'],
            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'zakcret006.seo077.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'zakcret007.seo077.site'],
            "x8" => ['source' => $array8, 'offset' => 1, 'target' => 'zakcret008.seo077.site'],
            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'zakcret009.seo077.site'],

            "x10"=> ['source' => $array10, 'offset' => 1, 'target' => 'zakcret010.seo078.site'],
            "x11"=> ['source' => $array11, 'offset' => 1, 'target' => 'zakcret011.seo078.site'],
            "x12"=> ['source' => $array12, 'offset' => 1, 'target' => 'zakcret012.seo078.site'],
            "x13"=> ['source' => $array13, 'offset' => 1, 'target' => 'zakcret013.seo078.site'],
            "x14"=> ['source' => $array14, 'offset' => 1, 'target' => 'zakcret014.seo078.site'],
            "x15"=> ['source' => $array15, 'offset' => 1, 'target' => 'zakcret015.seo078.site'],
            "x16"=> ['source' => $array16, 'offset' => 1, 'target' => 'zakcret016.seo078.site'],
            "x17"=> ['source' => $array17, 'offset' => 1, 'target' => 'zakcret017.seo078.site'],
            "x18"=> ['source' => $array18, 'offset' => 1, 'target' => 'zakcret018.seo078.site'],
            "x19"=> ['source' => $array19, 'offset' => 1, 'target' => 'zakcret019.seo078.site'],


        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.zakcret.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.56',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",

            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]')->last();
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.card-title a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.tw-text-2xl')->text();
        $product['price'] = str_replace("€", "", $crawler->filter("#special-price span")->text());
        $product['brand'] = ' ';
        $images = array_unique(array_filter($crawler->filter('.col-lg-1 .swiper-container .swiper-wrapper .swiper-slide .image a')->each(function (Crawler $node, $i) {
            return 'https:' . $node->attr("href");
        })));

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb .breadcrumb-item')->each(function (Crawler $node) {
            return $node->text();
        }));
        array_pop($breadcrumbs);
        $product['breadcrumbs'] = array_slice($breadcrumbs, 1);

        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }


        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
        $product['tags'] = [];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];
        $opt = array_unique(array_filter($crawler->filter('option')->each(function (Crawler $node, $i) {
            return $node->text();
        })));
        if ($crawler->filter(".selectsize opt")) {
            $product['type'] = 'variable';
            $product['variations'][] = [
                'name' => 'Επιλογή',
                'options' => array_slice($opt, 1),
            ];
        } else {
            $product['type'] = 'simple';
        }
        if ($crawler->filter(".product-detail-block-attributes__table table tr")->count()) {
            $count = $crawler->filter(".product-detail-block-attributes__table table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('th')->text(),
                    'options' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('td')->text(),
                ];
            }
        }
        $product['description'] = $crawler->filter('#tab-description')->html();
        $sku = $crawler->filter('.productmodel')->text();
        preg_match_all('/\d.*/', $sku, $final_sku);
        $product['sku'] = $final_sku[0][0];
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}