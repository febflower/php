<?php

namespace app\command\MSK\El;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class BioestiaGr extends Command
{


    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:bioestiagr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.bioestia.gr/');
    }

    protected function initialize(Input $input, Output $output)
    {
        //2
        $array1 = [
            'https://www.bioestia.gr/250-aporrofhthres',
            'https://www.bioestia.gr/346-klimatismos',

//            'https://www.bioestia.gr/418-antlies-thermothtas',
//            'https://www.bioestia.gr/539-set-pshstariwn',

        ];
        $array01 = [
            //1
            'https://www.bioestia.gr/181-bbq-pshstaries',
        ];
        //3
        $array2 = [
            "https://www.bioestia.gr/191-mpataries-nipthra",
            "https://www.bioestia.gr/193-mpataries-mpaniou"
        ];
        //3
        $array3 = [
            "https://www.bioestia.gr/323-mpataries-ntouzieras",
            "https://www.bioestia.gr/194-mpataries-mpinte",
            "https://www.bioestia.gr/301-aksesouar-ntous",
            "https://www.bioestia.gr/266-antallaktika-mpatariwn-mpaniou"
        ];
//3
        $array4 = [
            "https://www.bioestia.gr/475-%CE%AC%CE%B3%CE%B3%CE%B9%CF%83%CF%84%CF%81%CE%B1",
            "https://www.bioestia.gr/478-%CF%87%CE%B1%CF%81%CF%84%CE%BF%CE%B8%CE%AE%CE%BA%CE%B5%CF%82",
            "https://www.bioestia.gr/476-%CF%80%CE%B5%CF%84%CF%83%CE%B5%CF%84%CE%BF%CE%BA%CF%81%CE%B5%CE%BC%CE%AC%CF%83%CF%84%CF%81%CE%B5%CF%82",
        ];
//        https://www.bioestia.gr/476-%CF%80%CE%B5%CF%84%CF%83%CE%B5%CF%84%CE%BF%CE%BA%CF%81%CE%B5%CE%BC%CE%AC%CF%83%CF%84%CF%81%CE%B5%CF%82
        //3
        $array5 = [
            'https://www.bioestia.gr/482-pothrothikes-sapounothikes',
            "https://www.bioestia.gr/477-%CF%83%CF%80%CE%BF%CE%B3%CE%B3%CE%BF%CE%B8%CE%AE%CE%BA%CE%B5%CF%82",
            "https://www.bioestia.gr/479-%CF%80%CE%B9%CE%B3%CE%BA%CE%AC%CE%BB",
            "https://www.bioestia.gr/481-%CE%BB%CE%B1%CE%B2%CE%AD%CF%82-%CF%83%CE%BA%CE%B1%CE%BC%CF%80%CF%8C",
            "https://www.bioestia.gr/485-%CE%B5%CF%84%CE%B1%CE%B6%CE%B9%CE%AD%CF%81%CE%B5%CF%82",
            "https://www.bioestia.gr/536-kalogeroi-mpaniou"
        ];
        //2
        $array6 = [
            "https://www.bioestia.gr/149-nipthres",
            "https://www.bioestia.gr/210-epipla-mpaniou"
        ];
        //2
        $array7 = [
            "https://www.bioestia.gr/147-kampines-mpaniou",
        ];
        //2
        $array8 = [
            "https://www.bioestia.gr/146-lekanes",
            "https://www.bioestia.gr/154-kazanakia",
            "https://www.bioestia.gr/369-ntouz-ydromasaz"
        ];
        //2
        $array9 = [
            "https://www.bioestia.gr/192-mpataries-koyzinas"
        ];
        //2
        $array10 = [
            "https://www.bioestia.gr/244-neroxytes-inox",
            "https://www.bioestia.gr/253-%CE%BD%CE%B5%CF%81%CE%BF%CF%87%CF%8D%CF%84%CE%B5%CF%82-%CF%83%CF%85%CE%BD%CE%B8%CE%B5%CF%84%CE%B9%CE%BA%CE%BF%CE%AF",
            "https://www.bioestia.gr/419-%CF%83%CE%BA%CE%BF%CF%85%CF%80%CE%B9%CE%B4%CE%BF%CF%86%CE%AC%CE%B3%CE%BF%CE%B9",
            "https://www.bioestia.gr/404-dispensers-neroxyth"
        ];
        //2
        $array11 = [
            "https://www.bioestia.gr/252-granitenioi-neroxytes",
        ];
        //3
        $array011 = [
            "https://www.bioestia.gr/421-%CE%BA%CE%B1%CE%B8%CE%B1%CF%81%CE%B9%CF%83%CF%84%CE%B9%CE%BA%CE%AC-%CE%B5%CF%80%CE%B9%CF%86%CE%B1%CE%BD%CE%B5%CE%B9%CF%8E%CE%BD",
            "https://www.bioestia.gr/452-%CE%B5%CF%80%CE%B9%CF%86%CE%AC%CE%BD%CE%B5%CE%B9%CE%B5%CF%82-%CE%BA%CE%BF%CF%80%CE%AE%CF%82-%CF%83%CF%84%CF%81%CE%B1%CE%B3%CE%B3%CE%B9%CF%83%CF%84%CE%AE%CF%81%CE%B9%CE%B1",
        ];

        //2
        $array12 = [
            "https://www.bioestia.gr/494-hlektrikes-syskeues-kouzinas",
            "https://www.bioestia.gr/430-mageirika-skeyh",
            "https://www.bioestia.gr/546-pagkoi-ntoulapia-koyzinas",
        ];
//2
        $array13 = [
            "https://www.bioestia.gr/251-fournoi",
            "https://www.bioestia.gr/28-levhtes",
            "https://www.bioestia.gr/24-sompes-pellet",
        ];
        //2
        $array14 = [
            "https://www.bioestia.gr/44-energeiaka-tzakia",
        ];
        //2
        $array15 = [
            "https://www.bioestia.gr/5-sompes-ksulou",
            "https://www.bioestia.gr/35-aksesouar-thermansis"
        ];
        //2
        $array16 = [
            "https://www.bioestia.gr/123-swmata-kalorifer",
        ];
        //2
        $array17 = [
            "https://www.bioestia.gr/236-swlhnwseis",
            "https://www.bioestia.gr/309-eksarthmata-diktuwn",
            "https://www.bioestia.gr/261-antlies-piestika",
        ];
        //2
        $array18 = [
            "https://www.bioestia.gr/444-epipla-khpou"
        ];
        //2
        $array19 = [
            "https://www.bioestia.gr/541-%CE%B4%CE%B9%CE%B1%CE%BC%CF%8C%CF%81%CF%86%CF%89%CF%83%CE%B7-%CE%BA%CE%AE%CF%80%CE%BF%CF%85",
            "https://www.bioestia.gr/626-fwtistika",
            "https://www.bioestia.gr/637-karekles-trapezia-catering",
            "https://www.bioestia.gr/424-%CE%B4%CE%B5%CE%BE%CE%B1%CE%BC%CE%B5%CE%BD%CE%AD%CF%82-%CF%80%CE%B1%CE%BD%CF%84%CF%8C%CF%82-%CF%84%CF%8D%CF%80%CE%BF%CF%85"
        ];
        //1
        $array019 = [
            "https://www.bioestia.gr/465-plakakia",
        ];
        //1
        $array020 = [
            "https://www.bioestia.gr/136-filtra-nerou",
            "https://www.bioestia.gr/74-ergaleia-domika-ylika",
        ];
        //2
        $array20 = [
            "https://www.bioestia.gr/459-diaxeirish-aporrimatwn",
            "https://www.bioestia.gr/49-kapnagwgoi-persides"
            /**
             *
             * https://www.eshopelectric.gr/products/eksoplismos-pisinas/fwtismos-pisinas
             * this website is not belong to www.bioestia.gr,
             * the reason why it is used here,because quantity of commodity is not enough
             */
        ];
        $this->sites = [
            //采集完成
            'x1' => ['source' => $array1, 'target' => 'bioest001.seo069.site'],
            'x01' => ['source' => $array01, 'target' => 'bioest001.seo069.site'],
            'x2' => ['source' => $array2, 'target' => 'bioest002.seo069.site'],
            'x3' => ['source' => $array3, 'target' => 'bioest003.seo069.site'],
            'x4' => ['source' => $array4, 'target' => 'bioest004.seo069.site'],
            'x5' => ['source' => $array5, 'target' => 'bioest005.seo069.site'],
            'x6' => ['source' => $array6, 'target' => 'bioest006.seo069.site'],
            'x7' => ['source' => $array7, 'target' => 'bioest007.seo069.site'],
            'x8' => ['source' => $array8, 'target' => 'bioest008.seo069.site'],
            'x9' => ['source' => $array9, 'target' => 'bioest009.seo069.site'],
            'x10' => ['source' => $array10, 'target' => 'bioest010.seo069.site'],
            'x011' => ['source' => $array011, 'target' => 'bioest011.seo069.site'],
            'x11' => ['source' => $array11, 'target' => 'bioest011.seo069.site'],
            'x12' => ['source' => $array12, 'target' => 'bioest012.seo065.site'],
            'x13' => ['source' => $array13, 'target' => 'bioest013.seo065.site'],
            'x14' => ['source' => $array14, 'target' => 'bioest014.seo065.site'],
            'x15' => ['source' => $array15, 'target' => 'bioest015.seo065.site'],
            'x17' => ['source' => $array17, 'target' => 'bioest017.seo065.site'],
            'x019' => ['source' => $array019, 'target' => 'bioest019.seo065.site'],
            'x19' => ['source' => $array19, 'target' => 'bioest019.seo065.site'],


//正在采集
            'x16' => ['source' => $array16, 'target' => 'bioest016.seo065.site'],
            'x18' => ['source' => $array18, 'target' => 'bioest018.seo065.site'],
            'x020' => ['source' => $array020, 'target' => 'bioest020.seo065.site'],
            'x20' => ['source' => $array20, 'target' => 'bioest020.seo065.site'],


//等待采集


        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.bioestia.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }

    }

    protected function processProductList(Crawler $crawler)
    {

        $crawler->filter('.product-thumbnail a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.h1')->text();
        $product['price'] = $crawler->filter('.current-price span')->attr("content");
//        $product['brand'] = $crawler->filter('.img')->attr('src');
        //品牌是图片
//        $x = 'https://www.bioestia.gr/brand/73-franke';
//        $i = strpos($x, "-");
//        print_r(substr($x, $i+1));
        $raw_brand = $crawler->filter('.product-manufacturer span a')->attr('href');
        $i = strripos($raw_brand, "-");
        $product['brand'] = substr($raw_brand, $i + 1);
//        print_r($product);exit;
        $product['type'] = 'simple';
//        print_r($product);exit;

        $images = array_unique(array_filter($crawler->filter('.thumb')->each(function (Crawler $node, $i) {
            return $node->attr("data-image-large-src");
        })));
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
//        print_r($product);exit;

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb a')->each(function (Crawler $node) {
            return $node->text();
        }));
//        print_r($breadcrumbs);exit;

        $product['breadcrumbs'] = array_slice($breadcrumbs, 2);
//        print_r($product['breadcrumbs']);exit;
        $parent = 0;
        $parentCategory = '';
//        $offset = $this->processSite['offset'];
//        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
//        print_r($product);
//exit;

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['tags'] = [];

        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] =$product['brand'];
//        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

//        var_dump($product);


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->0()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }

        if ($crawler->filter(".data-sheet .name")->count()) {
            $count = $crawler->filter(".data-sheet .name")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".data-sheet .name")->eq($i)->text(),
                    'options' => $crawler->filter(".data-sheet .value")->eq($i)->text(),
                ];
            }
        }

//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
//        $product['short_description']=$crawler->filter('.product-detail-product-features__list')->html();
        $product['description'] = $crawler->filter('#description .product-description')->html();
//        print_r($product['description']);exit;

//        $product['short_description'] = $crawler->filter('.product-description p')->text();
        if ($crawler->filter('[itemprop="sku"]')->count()) {
            $product['sku'] = $crawler->filter('[itemprop="sku"]')->text();
        } else {
            $product['sku'] = $crawler->filter('[name="id_product"]')->attr("value");
        }


        print_r($product['sku']);
//        exit();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
//        var_dump($product);
//        echo '230';
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}