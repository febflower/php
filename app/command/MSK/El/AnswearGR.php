<?php

namespace app\command\MSK\El;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class AnswearGR extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:answeargr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://answear.gr/c/gunaikeia');
    }

    protected function initialize(Input $input, Output $output)
    {
        //1
        $array1 = [
            "https://answear.gr/m/adidas?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //2
        $array2 = [
            "https://answear.gr/m/adidas?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
        ];
        //3
        $array3 = [
//3
            "https://answear.gr/m/adidas?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%9A%CE%BF%CF%81%CE%AF%CF%84%CF%83%CE%B9",
            //3
            "https://answear.gr/m/adidas?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%91%CE%B3%CF%8C%CF%81%CE%B9",
            //2
//            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=Home%20%26%20Lifestyle&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",

        ];
        //2
        $array03 = [
            "https://answear.gr/m/answear-lab?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",

        ];
        //3
        $array4 = [
//            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%B8%CE%BB%CE%B7%CF%84%CE%B9%CE%BA%CE%AC%20%CE%BA%CE%B1%CE%B9%20sneakers&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
//            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A4%CE%B6%CE%B9%CE%BD&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
//            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%BF%CF%81%CF%84%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "
        https://answear.gr/m/answear-lab?page=6&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A4%CE%B6%CE%B9%CE%BD&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1
        "
        ];
        //2
        $array5 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=Casual%20%CE%BA%CE%B1%CE%B9%20%CE%BC%CE%BF%CE%BA%CE%B1%CF%83%CE%AF%CE%BD%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%B8%CE%BB%CE%B7%CF%84%CE%B9%CE%BA%CE%AC%20%CE%BA%CE%B1%CE%B9%20sneakers&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%93%CE%B1%CE%BB%CF%8C%CF%84%CF%83%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%95%CF%83%CF%80%CE%B1%CE%BD%CF%84%CF%81%CE%AF%CE%B3%CE%B9%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CF%8C%CF%84%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //2
        $array6 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%93%CE%AC%CE%BD%CF%84%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9A%CE%BF%CF%83%CE%BC%CE%AE%CE%BC%CE%B1%CF%84%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //3
        $array06 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9A%CE%AC%CE%BB%CF%84%CF%83%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array7 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A4%CE%B1%CE%BA%CE%BF%CF%8D%CE%BD%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CF%8C%CF%84%CE%B5%CF%82%20%CF%87%CE%B9%CE%BF%CE%BD%CE%B9%CE%BF%CF%8D&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%AC%CE%BD%CE%B9%CE%BD%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%B1%CE%BB%CE%B1%CF%81%CE%AF%CE%BD%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CE%BD%CF%84%CF%8C%CF%86%CE%BB%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",

        ];
//3
        $array8 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BF%CF%84%CE%AC%CE%BA%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%93%CF%85%CE%B1%CE%BB%CE%B9%CE%AC&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%B1%CE%BA%CE%AF%CE%B4%CE%B9%CE%B1%20%CF%80%CE%BB%CE%AC%CF%84%CE%B7%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array9 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9A%CE%B1%CF%83%CE%BA%CF%8C%CE%BB%20%CE%BA%CE%B1%CE%B9%20%CF%86%CE%BF%CF%85%CE%BB%CE%AC%CF%81%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%96%CF%8E%CE%BD%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%BF%CF%81%CF%84%CE%BF%CF%86%CF%8C%CE%BB%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //3
        $array10 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%95%CF%83%CF%8E%CF%81%CE%BF%CF%85%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CE%B1%CE%B3%CE%B9%CF%8C&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%BA%CE%BF%CF%85%CF%86%CE%B9%CE%AC%20%CE%BA%CE%B1%CE%B9%20%CE%BA%CE%B1%CF%80%CE%AD%CE%BB%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",

        ];
        //3
        $array11 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BB%CE%BF%CF%8D%CE%B6%CE%B5%CF%82%20%CE%BA%CE%B1%CE%B9%20%CF%80%CE%BF%CF%85%CE%BA%CE%AC%CE%BC%CE%B9%CF%83%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9F%CE%BB%CF%8C%CF%83%CF%89%CE%BC%CE%B5%CF%82%20%CF%86%CF%8C%CF%81%CE%BC%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%BF%CF%81%CF%84%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array12 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BF%CF%85%CF%86%CE%AC%CE%BD&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];

        //3
        $array13 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%B1%CE%BA%CE%AC%CE%BA%CE%B9%CE%B1%20%CE%BA%CE%B1%CE%B9%20%CE%B3%CE%B9%CE%BB%CE%AD%CE%BA%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A6%CE%BF%CF%8D%CF%83%CF%84%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A6%CE%BF%CF%8D%CF%84%CE%B5%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%B5%CF%84%20%CE%BA%CE%B1%CE%B9%20%CF%86%CF%8C%CF%81%CE%BC%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",

        ];
        //3
        $array14 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A4%CE%BF%CF%80%20%CE%BA%CE%B1%CE%B9%20%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%AC%CE%BA%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //2
        $array15 = [
            "https://answear.gr/m/answear-lab?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CE%BB%CF%84%CE%AC&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/asics?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/asics?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
            "https://answear.gr/m/coach?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];

        //2
        $array16 = [
            "https://answear.gr/m/calvin-klein?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A1%CE%BF%CF%8D%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array17 = [
            "https://answear.gr/m/calvin-klein?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //3
        $array18 = [
            "https://answear.gr/m/calvin-klein?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A1%CE%BF%CF%8D%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //2
        $array19 = [
            "https://answear.gr/m/calvin-klein?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
            "https://answear.gr/m/calvin-klein?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
            "https://answear.gr/m/calvin-klein?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
//2
        $array20 = [
            "https://answear.gr/m/columbia?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //2
        $array21 = [
            "https://answear.gr/m/columbia?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1,%CE%A1%CE%BF%CF%8D%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
        ];
        //2
        $array021 = [
            "https://answear.gr/m/columbia?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];

        //3
        $array22 = [
            "https://answear.gr/m/columbia?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%9A%CE%BF%CF%81%CE%AF%CF%84%CF%83%CE%B9",
            "https://answear.gr/m/columbia?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%91%CE%B3%CF%8C%CF%81%CE%B9",
            "https://answear.gr/m/desigual?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%9A%CE%BF%CF%81%CE%AF%CF%84%CF%83%CE%B9",
            "https://answear.gr/m/desigual?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%91%CE%B3%CF%8C%CF%81%CE%B9"
        ];
        //2
        $array022 = [
            "https://answear.gr/m/desigual?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",

        ];
        //2
        $array23 = [
            "https://answear.gr/m/desigual?page=10&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
//            "https://answear.gr/m/desigual?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //2
        $array24 = [
            "https://answear.gr/m/dkny?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",

        ];
        //3
        $array024 = [
            "https://answear.gr/m/dkny?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%9A%CE%BF%CF%81%CE%AF%CF%84%CF%83%CE%B9",
            "https://answear.gr/m/dkny?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%91%CE%B3%CF%8C%CF%81%CE%B9",
        ];

        //2
        $array25 = [
            "https://answear.gr/m/doughnut?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/doughnut?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
            "https://answear.gr/m/fila?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array025 = [
            "https://answear.gr/m/fila?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%91%CE%B3%CF%8C%CF%81%CE%B9"
        ];
        //2
        $array0025 = [
            "https://answear.gr/m/fila?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",

        ];
        //2
        $array26 = [
            "https://answear.gr/m/fila?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A1%CE%BF%CF%8D%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
//            "https://answear.gr/m/fila?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //3
        $array27 = [
            "https://answear.gr/m/fila/gunaikeia/rouha?page=2&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%95%CF%83%CF%8E%CF%81%CE%BF%CF%85%CF%87%CE%B1",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CE%B1%CE%B3%CE%B9%CF%8C",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%BF%CF%81%CF%84%CF%82",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A6%CE%BF%CF%8D%CF%83%CF%84%CE%B5%CF%82",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A6%CE%BF%CF%81%CE%AD%CE%BC%CE%B1%CF%84%CE%B1",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%B5%CF%84%20%CE%BA%CE%B1%CE%B9%20%CF%86%CF%8C%CF%81%CE%BC%CE%B5%CF%82",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BF%CF%85%CF%86%CE%AC%CE%BD",
            "https://answear.gr/m/fila/gunaikeia/rouha?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9A%CE%AC%CE%BB%CF%84%CF%83%CE%B5%CF%82"
//            "https://answear.gr/m/fila?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A1%CE%BF%CF%8D%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //有 2
        $array28 = [
            'https://answear.gr/m/gant?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1'
        ];
        //无 3
        $array028 = [
            "https://answear.gr/m/fila?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%9A%CE%BF%CF%81%CE%AF%CF%84%CF%83%CE%B9"
        ];
        $array29 = [
            "https://answear.gr/m/gant?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
            "https://answear.gr/m/geox?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //3
        $array30 = [
            "https://answear.gr/m/guess?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //2
        $array31 = [
            "https://answear.gr/m/happy-socks?page=6&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
//            "https://answear.gr/m/hunter?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
//            "https://answear.gr/m/hunter?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
//            "https://answear.gr/m/happy-socks?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9A%CE%AC%CE%BB%CF%84%CF%83%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array32 = [
            "https://answear.gr/m/karl-lagerfeld?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //2
        $array33 = [
            "https://answear.gr/m/lacoste?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
        ];
        //3
        $array34 = [
            "https://answear.gr/m/karl-lagerfeld?&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        //3
        $array35 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"

        ];
        //3
        $array36 = [
            'https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%95%CF%83%CF%8E%CF%81%CE%BF%CF%85%CF%87%CE%B1%2C%CE%9A%CE%AC%CE%BB%CF%84%CF%83%CE%B5%CF%82%2C%CE%9C%CE%B1%CE%B3%CE%B9%CF%8C%2C%CE%9C%CF%80%CE%BB%CE%BF%CF%8D%CE%B6%CE%B5%CF%82%20%CE%BA%CE%B1%CE%B9%20%CF%80%CE%BF%CF%85%CE%BA%CE%AC%CE%BC%CE%B9%CF%83%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1'];
        //3
        $array37 = [
//            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BF%CF%85%CF%86%CE%AC%CE%BD&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
//            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9F%CE%BB%CF%8C%CF%83%CF%89%CE%BC%CE%B5%CF%82%20%CF%86%CF%8C%CF%81%CE%BC%CE%B5%CF%82&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
//            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%B1%CE%BD%CF%84%CE%B5%CE%BB%CF%8C%CE%BD%CE%B9%CE%B1%20%CE%BA%CE%B1%CE%B9%20%CE%BA%CE%BF%CE%BB%CE%AC%CE%BD&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
//            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%BF%CF%85%CE%BB%CF%8C%CE%B2%CE%B5%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
            "https://answear.gr/m/medicine?&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BF%CF%85%CF%86%CE%AC%CE%BD%2C%CE%9F%CE%BB%CF%8C%CF%83%CF%89%CE%BC%CE%B5%CF%82%20%CF%86%CF%8C%CF%81%CE%BC%CE%B5%CF%82%2C%CE%A0%CE%B1%CE%BD%CF%84%CE%B5%CE%BB%CF%8C%CE%BD%CE%B9%CE%B1%20%CE%BA%CE%B1%CE%B9%20%CE%BA%CE%BF%CE%BB%CE%AC%CE%BD%2C%CE%A0%CE%BF%CF%85%CE%BB%CF%8C%CE%B2%CE%B5%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
//            "https://answear.gr/m/medicine?page=1&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%9C%CF%80%CE%BF%CF%85%CF%86%CE%AC%CE%BD%2C%CE%9F%CE%BB%CF%8C%CF%83%CF%89%CE%BC%CE%B5%CF%82%20%CF%86%CF%8C%CF%81%CE%BC%CE%B5%CF%82%2C%CE%A0%CE%B1%CE%BD%CF%84%CE%B5%CE%BB%CF%8C%CE%BD%CE%B9%CE%B1%20%CE%BA%CE%B1%CE%B9%20%CE%BA%CE%BF%CE%BB%CE%AC%CE%BD%2C%CE%A0%CE%BF%CF%85%CE%BB%CF%8C%CE%B2%CE%B5%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        $array38 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%B1%CE%BA%CE%AC%CE%BA%CE%B9%CE%B1%20%CE%BA%CE%B1%CE%B9%20%CE%B3%CE%B9%CE%BB%CE%AD%CE%BA%CE%B1%2C%CE%A3%CE%BF%CF%81%CF%84%CF%82%2C%CE%A4%CE%B6%CE%B9%CE%BD%2C%CE%A0%CE%B1%CE%BB%CF%84%CE%AC&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
//            "https://answear.gr/m/medicine?page=1&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%B1%CE%BA%CE%AC%CE%BA%CE%B9%CE%B1%20%CE%BA%CE%B1%CE%B9%20%CE%B3%CE%B9%CE%BB%CE%AD%CE%BA%CE%B1%2C%CE%A3%CE%BF%CF%81%CF%84%CF%82%2C%CE%A4%CE%B6%CE%B9%CE%BD%2C%CE%A0%CE%B1%CE%BB%CF%84%CE%AC&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
        ];
        $array39 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A4%CE%BF%CF%80%20%CE%BA%CE%B1%CE%B9%20%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%AC%CE%BA%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"

//            "https://answear.gr/m/medicine?page=3&%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A4%CE%BF%CF%80%20%CE%BA%CE%B1%CE%B9%20%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%AC%CE%BA%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array40 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A6%CE%BF%CF%81%CE%AD%CE%BC%CE%B1%CF%84%CE%B1,%CE%A0%CE%B1%CE%BB%CF%84%CE%AC,%CE%A6%CE%BF%CF%8D%CF%83%CF%84%CE%B5%CF%82,%CE%A6%CE%BF%CF%8D%CF%84%CE%B5%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];

        $array41 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=T-shirt%20%CE%BA%CE%B1%CE%B9%20Polo%20%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%AC%CE%BA%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //3
        $array42 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%95%CF%83%CF%8E%CF%81%CE%BF%CF%85%CF%87%CE%B1,%CE%9A%CE%AC%CE%BB%CF%84%CF%83%CE%B5%CF%82,%CE%9C%CE%B1%CE%B3%CE%B9%CF%8C,%CE%9C%CF%80%CE%BF%CF%85%CF%86%CE%AC%CE%BD,%CE%A0%CE%B1%CE%BD%CF%84%CE%B5%CE%BB%CF%8C%CE%BD%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //3
        $array43 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A0%CE%BF%CF%85%CE%BA%CE%AC%CE%BC%CE%B9%CF%83%CE%B1,%CE%A0%CE%BF%CF%85%CE%BB%CF%8C%CE%B2%CE%B5%CF%81,%CE%A3%CE%B1%CE%BA%CE%AC%CE%BA%CE%B9%CE%B1,%CE%A0%CE%B1%CE%BB%CF%84%CE%AC&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //3
        $array44 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A3%CE%BF%CF%81%CF%84%CF%82,%CE%A4%CE%B6%CE%B9%CE%BD,%CE%A6%CE%BF%CF%8D%CF%84%CE%B5%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //2 无
        $array45 = [
            "https://answear.gr/m/medicine?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=Home%20%26%20Lifestyle,%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82"
        ];
        //3
        $array46 = [
            "https://answear.gr/m/moschino?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/moschino?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%86%CE%BD%CF%84%CF%81%CE%B1%CF%82",
            "https://answear.gr/m/nike?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //47
        $array47 = [
            "https://answear.gr/m/pinko?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%A1%CE%BF%CF%8D%CF%87%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //2
        $array48 = [
            "https://answear.gr/m/pinko?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81,%CE%A0%CE%B1%CF%80%CE%BF%CF%8D%CF%84%CF%83%CE%B9%CE%B1&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //2
        $array49 = [
            "https://answear.gr/m/skechers?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1",
            "https://answear.gr/m/steve-madden?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array50 = [
            "https://answear.gr/m/swarovski?%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1=%CE%91%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%AC%CF%81&%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //3
        $array51 = [
            "https://answear.gr/m/tory-burch?%CE%A6%CF%8D%CE%BB%CE%BF=%CE%93%CF%85%CE%BD%CE%B1%CE%AF%CE%BA%CE%B1"
        ];
        //2
        $array52 = [
            "https://answear.gr/m/aldo?Φύλο=Γυναίκα"

        ];
        //2
        $array53 = [
            "https://answear.gr/m/puma?Φύλο=Γυναίκα"
        ];
        //2
        $array54 = [
            "https://answear.gr/m/gap?Φύλο=Γυναίκα"
        ];
        //2
        $array55 = [
            "https://answear.gr/m/vans?Φύλο=Γυναίκα"
        ];
        //2
        $array56 = [
            "https://answear.gr/m/adidas-performance?Φύλο=Γυναίκα"
        ];
        //2
        $array57 = [
            "https://answear.gr/m/sisley?Φύλο=Γυναίκα"
        ];
        //2
        $array58 = [
            "https://answear.gr/m/max-mara-all?Φύλο=Γυναίκα"
        ];
        //2
        $array59 = [
            "https://answear.gr/m/morgan?Φύλο=Γυναίκα"
        ];
        //2
        $array60 = [
            "https://answear.gr/m/reebok?Φύλο=Γυναίκα"
        ];
        $array61 = [
            "https://answear.gr/m/hugo?Φύλο=Γυναίκα"
        ];
        $array62 = [
            "https://answear.gr/m/4f?Φύλο=Γυναίκα"
        ];
        $array63 = [
            "https://answear.gr/m/pieces?Φύλο=Γυναίκα&page=4"
        ];
        $array64 = [
            "https://answear.gr/m/love-moschino?Φύλο=Γυναίκα"

        ];
        $array65 = [
            "https://answear.gr/m/boss?Φύλο=Γυναίκα"

        ];
        $array66 = ["https://answear.gr/m/vila?Φύλο=Γυναίκα"];
        $array67 = ["https://answear.gr/m/etam?Φύλο=Γυναίκα"];
        $array68 = ["https://answear.gr/m/womensecret?Φύλο=Γυναίκα"];
        $array69 = ["https://answear.gr/m/ania-kruk?Φύλο=Γυναίκα"];
        $array70 = ["https://answear.gr/m/swarovski?Φύλο=Γυναίκα"];
        $array71 = ["https://answear.gr/m/polo-ralph-lauren?Φύλο=Γυναίκα"];
        $array72 = ["https://answear.gr/m/twinset?Φύλο=Γυναίκα"];
        $array73 = ["https://answear.gr/m/champion?Φύλο=Γυναίκα"];
        $array74 = ["https://answear.gr/m/roxy?Φύλο=Γυναίκα"];
        $array75 = ["https://answear.gr/m/noisy-may?Φύλο=Γυναίκα"];
        $array76 = ["https://answear.gr/m/big-star?Φύλο=Γυναίκα"];
        $array77 = ["https://answear.gr/m/marc-opolo?Φύλο=Γυναίκα"];
        $array78 = ["https://answear.gr/m/mustang?Φύλο=Γυναίκα"];
        $array79 = ["https://answear.gr/m/g-star-raw?Φύλο=Γυναίκα"];
        $array80 = ["https://answear.gr/m/under-armour?Φύλο=Γυναίκα"];
        $array81 = ["https://answear.gr/m/converse?Φύλο=Γυναίκα"];
        $array82 = ["https://answear.gr/m/nobo?Φύλο=Γυναίκα"];
        $array83 = ["https://answear.gr/m/columbia?Φύλο=Γυναίκα"];
        $array84 = ["https://answear.gr/m/new-balance?Φύλο=Γυναίκα"];
        $array85 = ["https://answear.gr/m/labellamafia?Φύλο=Γυναίκα"];
        $array86 = ["https://answear.gr/m/wrangler?Φύλο=Γυναίκα"];
        $array87 = ["https://answear.gr/m/marella?Φύλο=Γυναίκα"];
        $array88 = ["https://answear.gr/m/armani-all?Φύλο=Γυναίκα"];
        $array89 = ["https://answear.gr/m/emporio-armani-underwear?Φύλο=Γυναίκα"];
        $array90 = ["https://answear.gr/m/lee?Φύλο=Γυναίκα"];
        $array91 = ["https://answear.gr/m/vagabond?Φύλο=Γυναίκα"];
        $array92 = ["https://answear.gr/m/allsaints?Φύλο=Γυναίκα"];
        $array93 = ["https://answear.gr/m/chiara-ferragni?Φύλο=Γυναίκα"];
        $array94 = ["https://answear.gr/m/bloomingville?Φύλο=Γυναίκα"];
        $array95 = ["https://answear.gr/m/steve-madden?Φύλο=Γυναίκα"];
        $array96 = ["https://answear.gr/m/max-co?Φύλο=Γυναίκα"];
        $array97 = ["https://answear.gr/m/ellesse?Φύλο=Γυναίκα"];
        $array98 = ["https://answear.gr/m/tory-burch?Φύλο=Γυναίκα"];
        $array99 = ["https://answear.gr/m/the-north-face?Φύλο=Γυναίκα"];
        $array100 = ["https://answear.gr/m/47brand?Φύλο=Γυναίκα"];

        $array101 = ["https://answear.gr/m/brave-soul?Φύλο=Γυναίκα"];
        $array102 = ["https://answear.gr/m/reebok-classic?Φύλο=Γυναίκα"];
        $array103 = ["https://answear.gr/m/nike-all?Φύλο=Γυναίκα"];
        $array104 = ["https://answear.gr/m/miss-sixty?Φύλο=Γυναίκα"];
        $array105 = ["https://answear.gr/m/cmp?Φύλο=Γυναίκα"];
        $array106 = ["https://answear.gr/m/kate-spade?Φύλο=Γυναίκα"];
        $array107 = ["https://answear.gr/m/max-mara-leisure?Φύλο=Γυναίκα"];
        $array108 = ["https://answear.gr/m/rains?Φύλο=Γυναίκα"];
        $array109 = ["https://answear.gr/m/p-e-nation?Φύλο=Γυναίκα"];
        $array110 = ["https://answear.gr/m/billabong?Φύλο=Γυναίκα"];
        $array111 = ["https://answear.gr/m/femi-stories?Φύλο=Γυναίκα"];
        $array112 = ["https://answear.gr/m/michael-kors-all?Φύλο=Γυναίκα"];
        $array113 = ["https://answear.gr/m/patrizia-pepe?Φύλο=Γυναίκα"];
        $array114 = ["https://answear.gr/m/lauren-ralph-lauren?Φύλο=Γυναίκα"];
        $array115 = ["https://answear.gr/m/michael-michael-kors?Φύλο=Γυναίκα"];
        $array116 = ["https://answear.gr/m/pepe-jeans?Φύλο=Γυναίκα"];
        $array117 = ["https://answear.gr/m/ralph-lauren-all?Φύλο=Γυναίκα"];
        $array118 = ["https://answear.gr/m/hugo-boss-all?Φύλο=Γυναίκα"];
        $array119 = ["https://answear.gr/m/jdy?Φύλο=Γυναίκα"];
        $array120 = ["https://answear.gr/m/vero-moda?Φύλο=Γυναίκα"];
        $array121 = ["https://answear.gr/m/liu-jo?Φύλο=Γυναίκα"];
        $array122 = ["https://answear.gr/m/united-colors-of-benetton?Φύλο=Γυναίκα"];
        $array123 = ["https://answear.gr/m/reebok-all?Φύλο=Γυναίκα"];
        $array124 = ["https://answear.gr/m/levis?Φύλο=Γυναίκα"];

        $array125 = ["https://answear.gr/m/pepe-jeans?Φύλο=Γυναίκα&page=11"];
        $array126 = ["https://answear.gr/m/ralph-lauren-all?Φύλο=Γυναίκα&page=12"];

        $array127 = ["https://answear.gr/m/adidas-originals?Φύλο=Άντρας"];
        $array128 = ["https://answear.gr/m/only-all?Φύλο=Άντρας"];
        $array129 = ["https://answear.gr/m/only-sons?Φύλο=Άντρας"];
        $array130 = ["https://answear.gr/m/tom-tailor?Φύλο=Άντρας"];
        $array131 = ["https://answear.gr/m/4f?Φύλο=Άντρας"];
        $array132 = ["https://answear.gr/m/karl-lagerfeld?Φύλο=Άντρας"];
        $array133 = ["https://answear.gr/m/g-star-raw?Φύλο=Άντρας"];
        $array134 = ["https://answear.gr/m/wrangler?Φύλο=Άντρας"];
        $array135 = ["https://answear.gr/m/reebok-all?Φύλο=Άντρας"];
        $array136 = ["https://answear.gr/m/under-armour?Φύλο=Άντρας"];
        $array137 = ["https://answear.gr/m/adidas-performance?Φύλο=Άντρας"];
        $array138 = ["https://answear.gr/m/gap?Φύλο=Άντρας"];
        $array139 = ["https://answear.gr/m/fila?Φύλο=Άντρας"];
        $array140 = ["https://answear.gr/m/united-colors-of-benetton?Φύλο=Άντρας"];
        $array141 = ["https://answear.gr/m/sisley?Φύλο=Άντρας"];
        $array142 = ["https://answear.gr/m/lee?Φύλο=Άντρας"];
        $array143 = ["https://answear.gr/m/lyle-scott?Φύλο=Άντρας"];

        $array144 = ["https://answear.gr/m/champion?Φύλο=Άντρας"];
        $array145 = ["https://answear.gr/m/new-balance?Φύλο=Άντρας"];
        $array146 = ["https://answear.gr/m/reebok?Φύλο=Άντρας"];
        $array147 = ["https://answear.gr/m/puma?Φύλο=Άντρας"];
        $array148 = ["https://answear.gr/m/47brand?Φύλο=Άντρας"];
        //
        $array149 = ["https://answear.gr/m/selected-homme?Φύλο=Άντρας"];
        $array150 = ["https://answear.gr/m/lacoste?Φύλο=Άντρας"];
        $array151 = ["https://answear.gr/m/michael-kors?Φύλο=Άντρας"];
        $array152 = ["https://answear.gr/m/michael-kors-all?Φύλο=Άντρας"];
        $array153 = ["https://answear.gr/m/mustang?Φύλο=Άντρας"];
        $array154 = ["https://answear.gr/m/produkt-by-jack-jones?Φύλο=Άντρας"];
        $array155 = ["https://answear.gr/m/armani-all?Φύλο=Άντρας"];
        $array156 = ["https://answear.gr/m/converse?Φύλο=Άντρας"];
        $array157 = ["https://answear.gr/m/emporio-armani-underwear?Φύλο=Άντρας"];
        $array158 = ["https://answear.gr/m/nike-all?Φύλο=Άντρας"];
        $array159 = ["https://answear.gr/m/huf?Φύλο=Άντρας"];
        $array160 = ["https://answear.gr/m/quiksilver?Φύλο=Άντρας"];

        $array161=["https://answear.gr/m/nike?Φύλο=Άντρας"];
        $array162=["https://answear.gr/m/tiger-of-sweden?Φύλο=Άντρας"];
        $array163=["https://answear.gr/m/bloomingville?Φύλο=Άντρας"];
        $array164=["https://answear.gr/m/new-era?Φύλο=Άντρας"];
        $array165=["https://answear.gr/m/gant?Φύλο=Άντρας"];
        $array166=["https://answear.gr/m/marc-opolo?Φύλο=Άντρας"];
        $array167=["https://answear.gr/m/paul-smith-all?Φύλο=Άντρας"];
        $array168=["https://answear.gr/m/brave-soul?Φύλο=Άντρας"];
        $array169=["https://answear.gr/m/calvin-klein-underwear?Φύλο=Άντρας"];
        $array170=["https://answear.gr/m/solid?Φύλο=Άντρας"];


        $this->sites = [
            'x1' => ['source' => $array1, 'offset' => 1, 'target' => 'answear001.seo065.site'],
            'x2' => ['source' => $array2, 'offset' => 1, 'target' => 'answear002.seo065.site'],
            'x3' => ['source' => $array3, 'offset' => 1, 'target' => 'answear003.seo065.site'],
            'x03' => ['source' => $array03, 'offset' => 1, 'target' => 'answear003.seo065.site'],

            //answear-lab
            'x4' => ['source' => $array4, 'offset' => 1, 'target' => 'answear004.seo065.site'],
            'x5' => ['source' => $array5, 'offset' => 1, 'target' => 'answear005.seo065.site'],
            'x6' => ['source' => $array6, 'offset' => 1, 'target' => 'answear006.seo065.site'],
            'x06' => ['source' => $array06, 'offset' => 1, 'target' => 'answear006.seo065.site'],
            'x7' => ['source' => $array7, 'offset' => 1, 'target' => 'answear007.seo065.site'],
            'x8' => ['source' => $array8, 'offset' => 1, 'target' => 'answear008.seo065.site'],
            'x9' => ['source' => $array9, 'offset' => 1, 'target' => 'answear009.seo065.site'],
            'x10' => ['source' => $array10, 'offset' => 1, 'target' => 'answear010.seo065.site'],
            'x11' => ['source' => $array11, 'offset' => 1, 'target' => 'answear011.seo065.site'],
            'x12' => ['source' => $array12, 'offset' => 1, 'target' => 'answear012.seo065.site'],
            //
            'x13' => ['source' => $array13, 'offset' => 1, 'target' => 'answear013.seo065.site'],
            'x14' => ['source' => $array14, 'offset' => 1, 'target' => 'answear014.seo065.site'],

            'x15' => ['source' => $array15, 'offset' => 1, 'target' => 'answear015.seo065.site'],
            'x16' => ['source' => $array16, 'offset' => 1, 'target' => 'answear016.seo065.site'],
            'x17' => ['source' => $array17, 'offset' => 1, 'target' => 'answear017.seo070.site'],
            'x18' => ['source' => $array18, 'offset' => 1, 'target' => 'answear018.seo070.site'],
            'x19' => ['source' => $array19, 'offset' => 1, 'target' => 'answear019.seo070.site'],
            'x20' => ['source' => $array20, 'offset' => 1, 'target' => 'answear020.seo070.site'],
            //
            'x21' => ['source' => $array21, 'offset' => 1, 'target' => 'answear021.seo070.site'],
            'x021' => ['source' => $array021, 'offset' => 1, 'target' => 'answear021.seo070.site'],

            'x22' => ['source' => $array22, 'offset' => 1, 'target' => 'answear022.seo070.site'],
            'x022' => ['source' => $array022, 'offset' => 1, 'target' => 'answear022.seo070.site'],
            'x23' => ['source' => $array23, 'offset' => 1, 'target' => 'answear023.seo070.site'],
            'x24' => ['source' => $array24, 'offset' => 1, 'target' => 'answear024.seo070.site'],
            'x024' => ['source' => $array024, 'offset' => 1, 'target' => 'answear024.seo070.site'],
            'x25' => ['source' => $array25, 'offset' => 1, 'target' => 'answear025.seo070.site'],
            'x025' => ['source' => $array025, 'offset' => 1, 'target' => 'answear025.seo070.site'],
            'x0025' => ['source' => $array0025, 'offset' => 1, 'target' => 'answear025.seo070.site'],

            'x26' => ['source' => $array26, 'offset' => 1, 'target' => 'answear026.seo071.site'],
            'x27' => ['source' => $array27, 'offset' => 1, 'target' => 'answear027.seo071.site'],
            //
            'x28' => ['source' => $array28, 'offset' => 1, 'target' => 'answear028.seo071.site'],
            'x028' => ['source' => $array028, 'offset' => 1, 'target' => 'answear028.seo071.site'],
            'x29' => ['source' => $array29, 'offset' => 1, 'target' => 'answear029.seo071.site'],
            'x30' => ['source' => $array30, 'offset' => 1, 'target' => 'answear030.seo071.site'],
            'x31' => ['source' => $array31, 'offset' => 1, 'target' => 'answear031.seo071.site'],
            'x32' => ['source' => $array32, 'offset' => 1, 'target' => 'answear032.seo071.site'],
            'x33' => ['source' => $array33, 'offset' => 1, 'target' => 'answear033.seo071.site'],
            'x34' => ['source' => $array34, 'offset' => 1, 'target' => 'answear034.seo071.site'],
            'x35' => ['source' => $array35, 'offset' => 1, 'target' => 'answear035.seo071.site'],
            'x36' => ['source' => $array36, 'offset' => 1, 'target' => 'answear036.seo071.site'],
            'x37' => ['source' => $array37, 'offset' => 1, 'target' => 'answear0037.seo071.site'],
            'x38' => ['source' => $array38, 'offset' => 1, 'target' => 'answear038.seo071.site'],
            'x39' => ['source' => $array39, 'offset' => 1, 'target' => 'answear039.seo071.site'],
            //
            'x40' => ['source' => $array40, 'offset' => 1, 'target' => 'answear040.seo071.site'],
            'x41' => ['source' => $array41, 'offset' => 1, 'target' => 'answear041.seo071.site'],
            'x42' => ['source' => $array42, 'offset' => 1, 'target' => 'answear042.seo071.site'],
            'x43' => ['source' => $array43, 'offset' => 1, 'target' => 'answear043.seo071.site'],
            'x44' => ['source' => $array44, 'offset' => 1, 'target' => 'answear044.seo071.site'],
            'x45' => ['source' => $array45, 'offset' => 1, 'target' => 'answear045.seo071.site'],
            'x46' => ['source' => $array46, 'offset' => 1, 'target' => 'answear046.seo071.site'],
            'x47' => ['source' => $array47, 'offset' => 1, 'target' => 'answear047.seo071.site'],
            'x48' => ['source' => $array48, 'offset' => 1, 'target' => 'answear048.seo071.site'],
            'x49' => ['source' => $array49, 'offset' => 1, 'target' => 'answear049.seo071.site'],
            'x50' => ['source' => $array50, 'offset' => 1, 'target' => 'answear050.seo071.site'],
            'x51' => ['source' => $array51, 'offset' => 1, 'target' => 'answear051.seo071.site'],
            'x52' => ['source' => $array52, 'offset' => 1, 'target' => 'answear052.seo071.site'],
            'x53' => ['source' => $array53, 'offset' => 1, 'target' => 'answear053.seo071.site'],
            'x54' => ['source' => $array54, 'offset' => 1, 'target' => 'answear054.seo071.site'],
            //
            'x55' => ['source' => $array55, 'offset' => 1, 'target' => 'answear055.seo072.site'],
            'x56' => ['source' => $array56, 'offset' => 1, 'target' => 'answear056.seo072.site'],
            'x57' => ['source' => $array57, 'offset' => 1, 'target' => 'answear057.seo072.site'],
            'x58' => ['source' => $array58, 'offset' => 1, 'target' => 'answear058.seo072.site'],
            'x59' => ['source' => $array59, 'offset' => 1, 'target' => 'answear059.seo072.site'],
            'x60' => ['source' => $array60, 'offset' => 1, 'target' => 'answear060.seo072.site'],
            'x61' => ['source' => $array61, 'offset' => 1, 'target' => 'answear061.seo072.site'],
            'x62' => ['source' => $array62, 'offset' => 1, 'target' => 'answear062.seo072.site'],
            'x63' => ['source' => $array63, 'offset' => 1, 'target' => 'answear063.seo072.site'],
            'x64' => ['source' => $array64, 'offset' => 1, 'target' => 'answear064.seo072.site'],
            'x65' => ['source' => $array65, 'offset' => 1, 'target' => 'answear065.seo072.site'],

            "x66" => ['source' => $array66, 'offset' => 1, 'target' => 'answear066.seo072.site'],
            "x67" => ['source' => $array67, 'offset' => 1, 'target' => 'answear067.seo072.site'],
            "x68" => ['source' => $array68, 'offset' => 1, 'target' => 'answear068.seo072.site'],
            "x69" => ['source' => $array69, 'offset' => 1, 'target' => 'answear069.seo072.site'],
            "x70" => ['source' => $array70, 'offset' => 1, 'target' => 'answear070.seo072.site'],
            "x71" => ['source' => $array71, 'offset' => 1, 'target' => 'answear071.seo072.site'],
            //
            "x72" => ['source' => $array72, 'offset' => 1, 'target' => 'answear072.seo072.site'],
            "x73" => ['source' => $array73, 'offset' => 1, 'target' => 'answear073.seo072.site'],
            "x74" => ['source' => $array74, 'offset' => 1, 'target' => 'answear074.seo072.site'],
            "x75" => ['source' => $array75, 'offset' => 1, 'target' => 'answear075.seo072.site'],
            "x76" => ['source' => $array76, 'offset' => 1, 'target' => 'answear076.seo072.site'],
            "x77" => ['source' => $array77, 'offset' => 1, 'target' => 'answear077.seo072.site'],
            "x78" => ['source' => $array78, 'offset' => 1, 'target' => 'answear078.seo072.site'],
            "x79" => ['source' => $array79, 'offset' => 1, 'target' => 'answear079.seo072.site'],
            "x80" => ['source' => $array80, 'offset' => 1, 'target' => 'answear080.seo072.site'],
            "x81" => ['source' => $array81, 'offset' => 1, 'target' => 'answear081.seo072.site'],
            "x82" => ['source' => $array82, 'offset' => 1, 'target' => 'answear082.seo072.site'],
            "x83" => ['source' => $array83, 'offset' => 1, 'target' => 'answear083.seo072.site'],
            "x84" => ['source' => $array84, 'offset' => 1, 'target' => 'answear084.seo072.site'],
            "x85" => ['source' => $array85, 'offset' => 1, 'target' => 'answear085.seo072.site'],
            "x86" => ['source' => $array86, 'offset' => 1, 'target' => 'answear086.seo072.site'],
//
            "x87" => ['source' => $array87, 'offset' => 1, 'target' => 'answear087.seo072.site'],
            "x88" => ['source' => $array88, 'offset' => 1, 'target' => 'answear088.seo072.site'],
            "x89" => ['source' => $array89, 'offset' => 1, 'target' => 'answear089.seo072.site'],
            "x90" => ['source' => $array90, 'offset' => 1, 'target' => 'answear090.seo072.site'],
            "x91" => ['source' => $array91, 'offset' => 1, 'target' => 'answear091.seo072.site'],
            "x92" => ['source' => $array92, 'offset' => 1, 'target' => 'answear092.seo072.site'],
            "x93" => ['source' => $array93, 'offset' => 1, 'target' => 'answear093.seo072.site'],
            "x94" => ['source' => $array94, 'offset' => 1, 'target' => 'answear094.seo072.site'],
            "x95" => ['source' => $array95, 'offset' => 1, 'target' => 'answear095.seo072.site'],
            "x96" => ['source' => $array96, 'offset' => 1, 'target' => 'answear096.seo072.site'],
            "x97" => ['source' => $array97, 'offset' => 1, 'target' => 'answear097.seo072.site'],
            "x98" => ['source' => $array98, 'offset' => 1, 'target' => 'answear098.seo072.site'],
            "x99" => ['source' => $array99, 'offset' => 1, 'target' => 'answear099.seo072.site'],
            "x100" => ['source' => $array100, 'offset' => 1, 'target' => 'answear100.seo072.site'],
            //
            "x101" => ['source' => $array101, 'offset' => 1, 'target' => 'answear101.seo073.site'],
            "x102" => ['source' => $array102, 'offset' => 1, 'target' => 'answear102.seo073.site'],
            "x103" => ['source' => $array103, 'offset' => 1, 'target' => 'answear103.seo073.site'],
            "x104" => ['source' => $array104, 'offset' => 1, 'target' => 'answear104.seo073.site'],
            "x105" => ['source' => $array105, 'offset' => 1, 'target' => 'answear105.seo073.site'],
            "x106" => ['source' => $array106, 'offset' => 1, 'target' => 'answear106.seo073.site'],
            "x107" => ['source' => $array107, 'offset' => 1, 'target' => 'answear107.seo073.site'],
            "x108" => ['source' => $array108, 'offset' => 1, 'target' => 'answear108.seo073.site'],
            "x109" => ['source' => $array109, 'offset' => 1, 'target' => 'answear109.seo073.site'],
            "x110" => ['source' => $array110, 'offset' => 1, 'target' => 'answear110.seo073.site'],
            "x111" => ['source' => $array111, 'offset' => 1, 'target' => 'answear111.seo073.site'],
            "x112" => ['source' => $array112, 'offset' => 1, 'target' => 'answear112.seo073.site'],
            "x113" => ['source' => $array113, 'offset' => 1, 'target' => 'answear113.seo073.site'],
            "x114" => ['source' => $array114, 'offset' => 1, 'target' => 'answear114.seo073.site'],
            "x115" => ['source' => $array115, 'offset' => 1, 'target' => 'answear115.seo073.site'],

            "x116" => ['source' => $array116, 'offset' => 1, 'target' => 'answear116.seo073.site'],
            "x117" => ['source' => $array117, 'offset' => 1, 'target' => 'answear117.seo073.site'],
            "x118" => ['source' => $array118, 'offset' => 1, 'target' => 'answear118.seo073.site'],
            "x119" => ['source' => $array119, 'offset' => 1, 'target' => 'answear119.seo073.site'],
            "x120" => ['source' => $array120, 'offset' => 1, 'target' => 'answear120.seo073.site'],
            "x121" => ['source' => $array121, 'offset' => 1, 'target' => 'answear121.seo073.site'],
            "x122" => ['source' => $array122, 'offset' => 1, 'target' => 'answear122.seo073.site'],
            "x123" => ['source' => $array123, 'offset' => 1, 'target' => 'answear123.seo073.site'],
            "x124" => ['source' => $array124, 'offset' => 1, 'target' => 'answear124.seo073.site'],
            //
            "x125" => ['source' => $array125, 'offset' => 1, 'target' => 'answear125.seo073.site'],
            "x126" => ['source' => $array126, 'offset' => 1, 'target' => 'answear126.seo073.site'],
            "x127" => ['source' => $array127, 'offset' => 1, 'target' => 'answear127.seo073.site'],
            "x128" => ['source' => $array128, 'offset' => 1, 'target' => 'answear128.seo073.site'],
            "x129" => ['source' => $array129, 'offset' => 1, 'target' => 'answear129.seo073.site'],
            "x130" => ['source' => $array130, 'offset' => 1, 'target' => 'answear130.seo073.site'],
            "x131" => ['source' => $array131, 'offset' => 1, 'target' => 'answear131.seo073.site'],
            "x132" => ['source' => $array132, 'offset' => 1, 'target' => 'answear132.seo073.site'],
            "x133" => ['source' => $array133, 'offset' => 1, 'target' => 'answear133.seo073.site'],
            "x134" => ['source' => $array134, 'offset' => 1, 'target' => 'answear134.seo073.site'],
            "x135" => ['source' => $array135, 'offset' => 1, 'target' => 'answear135.seo073.site'],
            "x136" => ['source' => $array136, 'offset' => 1, 'target' => 'answear136.seo073.site'],
            "x137" => ['source' => $array137, 'offset' => 1, 'target' => 'answear137.seo073.site'],
            "x138" => ['source' => $array138, 'offset' => 1, 'target' => 'answear138.seo073.site'],
            "x139" => ['source' => $array139, 'offset' => 1, 'target' => 'answear139.seo073.site'],
            "x140" => ['source' => $array140, 'offset' => 1, 'target' => 'answear140.seo073.site'],
            "x141" => ['source' => $array141, 'offset' => 1, 'target' => 'answear141.seo073.site'],
            "x142" => ['source' => $array142, 'offset' => 1, 'target' => 'answear142.seo073.site'],
            "x143" => ['source' => $array143, 'offset' => 1, 'target' => 'answear143.seo073.site'],

            "x144" => ['source' => $array144, 'offset' => 1, 'target' => 'answear144.seo073.site'],
            "x145" => ['source' => $array145, 'offset' => 1, 'target' => 'answear145.seo073.site'],
            "x146" => ['source' => $array146, 'offset' => 1, 'target' => 'answear146.seo073.site'],
            "x147" => ['source' => $array147, 'offset' => 1, 'target' => 'answear147.seo073.site'],
            "x148" => ['source' => $array148, 'offset' => 1, 'target' => 'answear148.seo073.site'],
            //
            "x149" => ['source' => $array149, 'offset' => 1, 'target' => 'answear149.seo073.site'],
            "x150" => ['source' => $array150, 'offset' => 1, 'target' => 'answear150.seo073.site'],
            "x151" => ['source' => $array151, 'offset' => 1, 'target' => 'answear151.seo073.site'],
            "x152" => ['source' => $array152, 'offset' => 1, 'target' => 'answear152.seo073.site'],
            "x153" => ['source' => $array153, 'offset' => 1, 'target' => 'answear153.seo073.site'],
            "x154" => ['source' => $array154, 'offset' => 1, 'target' => 'answear154.seo073.site'],
            "x155" => ['source' => $array155, 'offset' => 1, 'target' => 'answear155.seo073.site'],
            "x156" => ['source' => $array156, 'offset' => 1, 'target' => 'answear156.seo073.site'],
            "x157" => ['source' => $array157, 'offset' => 1, 'target' => 'answear157.seo073.site'],
            "x158" => ['source' => $array158, 'offset' => 1, 'target' => 'answear158.seo073.site'],
            "x159" => ['source' => $array159, 'offset' => 1, 'target' => 'answear159.seo073.site'],
            "x160" => ['source' => $array160, 'offset' => 1, 'target' => 'answear160.seo073.site'],
            //
            "x161" => ['source' => $array161, 'offset' => 1, 'target' => 'answear161.seo073.site'],
            "x162" => ['source' => $array162, 'offset' => 1, 'target' => 'answear162.seo073.site'],
            "x163" => ['source' => $array163, 'offset' => 1, 'target' => 'answear163.seo073.site'],
            "x164" => ['source' => $array164, 'offset' => 1, 'target' => 'answear164.seo073.site'],
            "x165" => ['source' => $array165, 'offset' => 1, 'target' => 'answear165.seo073.site'],
            "x166" => ['source' => $array166, 'offset' => 1, 'target' => 'answear166.seo073.site'],
            "x167" => ['source' => $array167, 'offset' => 1, 'target' => 'answear167.seo073.site'],
            "x168" => ['source' => $array168, 'offset' => 1, 'target' => 'answear168.seo073.site'],
            "x169" => ['source' => $array169, 'offset' => 1, 'target' => 'answear169.seo073.site'],
            "x170" => ['source' => $array170, 'offset' => 1, 'target' => 'answear170.seo073.site'],


        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://answear.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            if (strpos($uri, 'page')) {
                $ori_uri = substr($uri, 0, strpos($uri, 'page') - 1);
            } else {
                $ori_uri = $uri;
            }
            $this->processPage($ori_uri . '&' . substr($nextNode->attr('href'), strpos($nextNode->attr('href'), 'page')));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
//        $b = $crawler->filter('.Badge__badgeLabel__e60fG span')->last()->text();
//        print_r($b);exit;
        $crawler->filter('.ProductItem__productCard__1c448')->each(function (Crawler $node, $i) use (&$b) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.ProductItem__productCardDescription__2OiTW .ProductItem__productCardName__RGRRI span')->text(),
                    'url' => sprintf('%s', 'https://answear.gr' . $node->filter('.ProductItem__productCardImageWrapper__2eewd a')->attr('href')),
//                    "breadcrumbs" => $b,
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    public function toEncryptImage($images, $original = 'original')
    {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        if ($crawler->filter('.ProductCard__controlsVerticalTrack__1t5mc .slick-slide')->count()) {
            $images1 = array_unique(array_filter($crawler->filter('.ProductCard__controlsVerticalTrack__1t5mc .slick-slide .ProductCard__slideActive__1CyP7 .Image__media__2XtKT .Image__cardImage__3eRwk img')->each(function (Crawler $node, $i) {
                return $node->attr('src');
            })));

            $images2 = array_unique(array_filter($crawler->filter('.ProductCard__controlsVerticalTrack__1t5mc .slick-slide .ProductCard__slide__27Is- .Image__media__2XtKT .Image__cardImage__3eRwk img')->each(function (Crawler $node, $i) {
                return $node->attr('src');
            })));
            $images = array_merge($images1, $images2);

        }
        if ($crawler->filter(".ProductCard__productNameAndLogo__1NudT h1 span")->count()) {
            $product['title'] = $crawler->filter(".ProductCard__productNameAndLogo__1NudT h1 span")->text();
        } else {
            $product['title'] = $crawler->filter(".ProductCard__productName__g0iPL h1 span")->text();
        }

        $product['price'] = substr($crawler->filter('.Price__wrapperMain__3jCRr .Price__salePrice__tkBzg')->text(), 0, strpos($crawler->filter('.Price__wrapperMain__3jCRr .Price__salePrice__tkBzg')->text(), " "));
        if ($crawler->filter('.ProductCard__productNameAndLogoImage__1P5if a img')->count()) {
            $product['brand'] = $crawler->filter('.ProductCard__productNameAndLogoImage__1P5if a img')->attr("alt");
        }
//        else {
//            $product['brand'] = "Produkt by Jack & Jones";
//
//        }


        $choice = explode('"sizes":', $crawler->filter('script')->eq(5)->text());
        $x = $choice[1];
        $i1 = strpos($x, "allSizes");
        $x1 = substr($x, $i1);
        $i2 = strpos($x1, 'frontendUuid"');
        $x2 = str_replace("[", " ", substr($x1, 0, $i2 - 2));
        $x3 = str_replace("]", " ", $x2);
        $x4 = str_replace("{", " ", $x3);
        $x5 = str_replace("}", " ", $x4);
        $del=["[","]","{","}",];
//        爬取尺寸是大写字母的商品
        preg_match_all('/name.*?[A-Z]+/', $x5, $matches);
//        var_dump($matches[0]);exit;
        if (strlen($matches[0][0]) > 10) {
            preg_match_all('/name.*?[0-9]+/', $x5, $matches);
        }
        $choices = $matches;
        $options = [];
        foreach ($choices[0] as $v) {
            $opt = str_replace('name":"', "", $v);
            $options[] = $opt;
        }

        $x = 1;


        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => str_replace('82x124', '540x813', $image),
                'name' => $product['title'],
            ];
            $x++;
        }

        $product['images'] = $this->toEncryptImage($product['images']);
        $breadcrumbs = array_filter($crawler->filter('#BreadcrumbsList li a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 2);

        $parent = 0;
        $parentCategory = '';

        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        if ($breadcrumbs[1] == "Γυναικεία") {
            $product['gender'] = '';
        } else {
            $product['gender'] = $breadcrumbs[1];
        }

        if ($crawler->filter('.ProductCard__productNameAndLogo__1NudT h1 span')->eq(1)->count()) {
            $product['color'] = substr($crawler->filter('.ProductCard__productNameAndLogo__1NudT h1 span')->eq(1)->text(), strpos($crawler->filter('.ProductCard__productNameAndLogo__1NudT h1 span')->eq(1)->text(), " ") + 1);
        } else {
            $product['color'] = $crawler->filter('.Image__cardImage__3eRwk')->last()->filter('img')->attr("alt");
        }
        if (end($breadcrumbs) == "Γυναικεία") {
            $product['subCategory'] = ' ';
        } else {
            $product['subCategory'] = end($breadcrumbs);
        }
//        $product['tags'][]=$product['color'];
        $product['tags'][] = $product['brand'];

        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

        if (sizeof($options) == 1) {
            $product['type'] = 'simple';
        } else {
            $product['type'] = 'variable';
            $product['variations'][] = [
                'name' => 'μεγέθους',
                'options' => $options,];
        }

        $product['description'] = $crawler->filter('.Accordion__accordionDescription__3r3nn')->html();
        $product['sku'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)['sku'];
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}