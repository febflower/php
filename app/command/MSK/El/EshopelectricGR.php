<?php

namespace app\command\MSK\El;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class EshopelectricGR extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:eshopelectricgr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.eshopelectric.gr/');
    }

    protected function initialize(Input $input, Output $output)
    {

        $array_e1 = [
//            https://www.eshopelectric.gr/products/fwtismos/eswterikoy-xwroy/monofwta
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/monofwta",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/polyfwta",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/plafonieres",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/epitoixia-aplikes",
        ];
        $array_e2 = [
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/fwtistika-orofhs",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/portatif",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/fwtistika-dapedoy",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/fwtistika-vintage-biomhxanikoy-typoy",
        ];
        $array_e3 = [
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/paidika-fwtistika",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/spot",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/xwneyta-spot",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/eswterikoy-xwroy/xartina-fwtistika",
        ];
        //室内灯

        $array_e4 = [

            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/kremasta-fwtistika",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/fanaria",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/xelwnes",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/spot",
        ];

        $array_e5 = [
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/plafonieres-aplikes-epitoixia",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/fwtistika-karfwta",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/kolwnakia-kolwnes",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/fwtistika-spa",
        ];
        $array_e6 = [
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/fwtistika-mpala",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/xwneyta-fwtistika-dapedoy-toixoy-orofhs",
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/ekswterikoy-xwroy/fwtistika-plateiwn-dromwn",
//            //户外灯
            "https://www.eshopelectric.gr/brands/brilliant/fwtismos/hliaka-fwtistika/hliaka-fwtistika",
//            //太阳能灯具
        ];
        $array_e7=[
            "https://www.eshopelectric.gr/products/fwtismos/led-pl-panel",
            "https://www.eshopelectric.gr/products/fwtismos/grammika-fwtistika-led-biomhxanikoy-typoy",
            "https://www.eshopelectric.gr/products/fwtismos/controller-dimmer/led-controllers-dimmers",
            "https://www.eshopelectric.gr/products/fwtismos/controller-dimmer/rgb-led-controllers"
        ];
        //1
        $array020 = [
            "https://www.eshopelectric.gr/products/eksoplismos-pisinas/fwtismos-pisinas"
        ];

        $this->sites = [
            'e1' => ['source' => $array_e1, 'target' => 'eshopele001.seo069.site'],
            'e2' => ['source' => $array_e2, 'target' => 'eshopele001.seo069.site'],
            'e3' => ['source' => $array_e3, 'target' => 'eshopele001.seo069.site'],
            'e4' => ['source' => $array_e4, 'target' => 'eshopele001.seo069.site'],
            'e5' => ['source' => $array_e5, 'target' => 'eshopele001.seo069.site'],
            'e6' => ['source' => $array_e6, 'target' => 'eshopele001.seo069.site'],
            'e7' => ['source' => $array_e7, 'target' => 'eshopele002.seo069.site'],
            'x020' => ['source' => $array020, 'target' => 'bioest020.seo065.site'],

        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.eshopelectric.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.paging_next');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.product a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);

        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('#product_title')->text();
        $product['price'] = str_replace("€", "", str_replace(',', '.', $crawler->filter('.price')->text()));
//        print_r($product);exit;
        if ($crawler->filter('.manufacturer a img')->count()) {
            $product['brand'] = $crawler->filter('.manufacturer a img')->attr('alt');

        } else {
            $product['brand'] = $crawler->filter('.manufacturer a')->text();

        }
        $product['type'] = 'simple';

        $images = array_unique(array_filter($crawler->filter('.lightbox')->each(function (Crawler $node, $i) {
            return $node->attr("href");
        })));
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => 'https://www.eshopelectric.gr/' . $image,
                'name' => $product['title'],
            ];
            $x++;
        }

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb-item a')->each(function (Crawler $node, $i) {
            return $node->text();
        }));

        $product['breadcrumbs'] = array_slice($breadcrumbs, 1);

        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
//        print_r($product['categories']);
//        exit;

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['tags'] = [];

        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] =$product['brand'];
//        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

//        var_dump($product);


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }

//        if($crawler->filter(".data-sheet .name")->count()) {
//            $count = $crawler->filter(".data-sheet .name")->count();
//            for($i=0;$i<$count;$i++){
//                $product['attributes'][] = [
//                    'name' => $crawler->filter(".data-sheet .name")->eq($i)->text(),
//                    'options' => $crawler->filter(".data-sheet .value")->eq($i)->text(),
//                ];
//            }
//        }

        $product['attributes'] = [];
//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
//        $product['short_description']=$crawler->filter('.product-detail-product-features__list')->html();
        if ($crawler->filter('.product_specs')->count()) {

            $product['description'] = $crawler->filter(".description")->html();
//            echo '263';
        } else {
            $product['description'] = $crawler->filter('.product_specs')->html();
//            echo '261';
        }

//        print_r($product['description']);

//        $product['short_description'] = $crawler->filter('.product-description p')->text();
        $product['sku'] = $crawler->filter('#product_code')->text();
//        print_r($product['sku']);exit;
//
//        print_r($product);

        $this->createProduct($product);
        echo "\r\n";
        echo "1";
//        var_dump($product);
//        echo '230';
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
