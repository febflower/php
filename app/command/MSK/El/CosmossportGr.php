<?php

namespace app\command\MSK\El;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class CosmossportGr extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:cosmossport')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.cosmossport.gr/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=emerson", "https://www.cosmossport.gr/el/8444-papoutsia?brand=skechers", "https://www.cosmossport.gr/el/8442-aksesouar?brand=yeti"];
        $array2 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=tommy-jeans", "https://www.cosmossport.gr/el/8444-papoutsia?brand=adidas-originals", "https://www.cosmossport.gr/el/8442-aksesouar?brand=hydro-flask"];
        $array3 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=nuff", "https://www.cosmossport.gr/el/8444-papoutsia?brand=hi-tec", "https://www.cosmossport.gr/el/8442-aksesouar?brand=vans"];
        $array4 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=jack-jones", "https://www.cosmossport.gr/el/8444-papoutsia?brand=cmp", "https://www.cosmossport.gr/el/8442-aksesouar?brand=happy-socks"];
        $array5 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=polo-ralph-lauren", "https://www.cosmossport.gr/el/8444-papoutsia?brand=columbia", "https://www.cosmossport.gr/el/8442-aksesouar?brand=adidas-originals"];
        $array6 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=vans", "https://www.cosmossport.gr/el/8444-papoutsia?brand=wilson", "https://www.cosmossport.gr/el/8442-aksesouar?brand=stance"];
        $array7 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=levi-s", "https://www.cosmossport.gr/el/8444-papoutsia?brand=the-north-face", "https://www.cosmossport.gr/el/8442-aksesouar?brand=chilly-s"];
        $array8 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=target", "https://www.cosmossport.gr/el/8444-papoutsia?brand=nuff", "https://www.cosmossport.gr/el/8442-aksesouar?brand=adidas-performance"];
        $array9 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=10600-adidas-terrex", "https://www.cosmossport.gr/el/8444-papoutsia?brand=joma", "https://www.cosmossport.gr/el/8442-aksesouar?brand=herschel"];
        $array10 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=jordan", "https://www.cosmossport.gr/el/8444-papoutsia?brand=altra", "https://www.cosmossport.gr/el/8442-aksesouar?brand=funko-pop"];
        $array11 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=champion", "https://www.cosmossport.gr/el/8444-papoutsia?brand=la-sportiva", "https://www.cosmossport.gr/el/8442-aksesouar?brand=nike"];
        $array12 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=the-north-face", "https://www.cosmossport.gr/el/8444-papoutsia?brand=nike"];
        $array13 = ["https://www.cosmossport.gr/el/8443-rouxa?brand=o-neill", "https://www.cosmossport.gr/el/8444-papoutsia?brand=adidas-performance"];


        $this->sites = [
            "x1" => ['source' => $array1, 'offset' => 1, 'target' => 'cosmoss001.seo079.site'],
            "x2" => ['source' => $array2, 'offset' => 1, 'target' => 'cosmoss002.seo079.site'],
            "x3" => ['source' => $array3, 'offset' => 1, 'target' => 'cosmoss003.seo079.site'],
            "x4" => ['source' => $array4, 'offset' => 1, 'target' => 'cosmoss004.seo079.site'],
            "x5" => ['source' => $array5, 'offset' => 1, 'target' => 'cosmoss005.seo079.site'],
            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'cosmoss006.seo079.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'cosmoss007.seo079.site'],
            "x8" => ['source' => $array8, 'offset' => 1, 'target' => 'cosmoss008.seo079.site'],
            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'cosmoss009.seo079.site'],
            "x10" => ['source' => $array10, 'offset' => 1, 'target' => 'cosmoss010.seo079.site'],
            "x11" => ['source' => $array11, 'offset' => 1, 'target' => 'cosmoss011.seo079.site'],
            "x12" => ['source' => $array12, 'offset' => 1, 'target' => 'cosmoss012.seo079.site'],
            "x13" => ['source' => $array13, 'offset' => 1, 'target' => 'cosmoss013.seo079.site'],


        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.cosmossport.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage("https://www.cosmossport.gr" . $nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.product-name')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.img-responsive')->eq(2)->attr("alt");
        $product['price'] = $crawler->filter('#our_new_price_display')->attr("content");
        $product['brand'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)['brand']['name'];
        $images = array_unique(array_filter($crawler->filter('#thumbs_list_frame li img')->each(function (Crawler $node, $i) {
            return str_replace("thumb", "medium", $node->attr("src"));
        })));

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb')->each(function (Crawler $node) {
            return $node->text();
        }));
        $breadcrumb = explode(">", $breadcrumbs[0]);

        array_pop($breadcrumb);

        $product['breadcrumbs'] = array_slice($breadcrumb, 2);
//        $product['subCategory'] = ' ';
        $product['subCategory'] = end($breadcrumb);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';


        $color = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)['color'];
        if (strpos($product['title'], $color)) {
            $product['color'] = "";
        } else {
            $product['color'] = $color;
        }
        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];
        if ($crawler->filter(".radioPickList")->filter("li")->eq(0)->count()) {
            $opt = $crawler->filter(".radioPickList")->filter("li")->eq(0)->nextAll()->each(function (Crawler $node, $i) {
                $a = $node->text();
                if (strpos($a, "€")) {
                    $j = strpos($a, '-');
                    $b = substr($a, 0, $j);
                    $c = str_replace("- Τελευταίο Κομμάτι!", "", $b);
                } else {
                    $c = str_replace("- Τελευταίο Κομμάτι!", "", $a);
                }
                return $c;
            });
            if (sizeof($opt) != 0) {
                $product['type'] = 'variable';
                $product['variations'][] = [
                    'name' => 'μεγέθους',
                    'options' => $opt,];
            }
        } else {
            $product['type'] = 'simple';
        }
        $a = $crawler->filter('#tab_content_4')->html();
        $i = strpos($a, '<div class="descriptionTabProductSKU">');
        $product['description'] = substr($a, 0, $i) . "</div>";
        $product['sku'] = $product['sku'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(), true)['sku'];
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}