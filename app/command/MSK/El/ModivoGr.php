<?php

namespace app\command\MSK\El;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ModivoGr extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:modivogr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://modivo.gr/m/gynaikes.html');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = ["https://modivo.gr/c/gynaikes/marka:love_moschino"];

        $array2 = ["https://modivo.gr/c/gynaikes/marka:vans"];
        $array3 = ["https://modivo.gr/c/gynaikes/marka:michael_michael_kors"];
        $array4 = ["https://modivo.gr/c/gynaikes/marka:big_star_shoes?p=5"];
        $array5 = ["https://modivo.gr/c/gynaikes/marka:deezee?p=6"];
        $array6 = ["https://modivo.gr/c/gynaikes/marka:patrizia_pepe?p=5"];
        $array7 = ["https://modivo.gr/c/gynaikes/marka:tamaris"];
        $array8 = ["https://modivo.gr/c/gynaikes/marka:adidas_performance"];
        $array9 = ["https://modivo.gr/c/gynaikes/marka:puma"];
        $array10 = ["https://modivo.gr/c/gynaikes/marka:karl_lagerfeld"];
        $array11 = ["https://modivo.gr/c/gynaikes/marka:united_colors_of_benetton"];
        $array12 = ["https://modivo.gr/c/gynaikes/marka:nike"];
        $array13 = ["https://modivo.gr/c/gynaikes/marka:fila"];
        $array14 = ["https://modivo.gr/c/gynaikes/marka:geox"];
        $array15 = ["https://modivo.gr/c/gynaikes/marka:rieker"];
        //
        $array16 = ["https://modivo.gr/c/gynaikes/marka:furla"];
        $array17 = ["https://modivo.gr/c/gynaikes/marka:caprice?p=9"];
        $array18 = ["https://modivo.gr/c/gynaikes/marka:vero_moda"];
        $array19 = ["https://modivo.gr/c/gynaikes/marka:reebok?p=9"];
        $array20 = ["https://modivo.gr/c/gynaikes/marka:rinascimento"];
        $array21 = ["https://modivo.gr/c/gynaikes/marka:gino_rossi"];
        $array22 = ["https://modivo.gr/c/gynaikes/marka:twinset"];
        $array23 = ["https://modivo.gr/c/gynaikes/marka:dkny"];
        $array24 = ["https://modivo.gr/c/gynaikes/marka:eva_longoria"];
        $array25 = ["https://modivo.gr/c/gynaikes/marka:calvin_klein_underwear"];
        $array26 = ["https://modivo.gr/c/gynaikes/marka:only"];
        $array27 = ["https://modivo.gr/c/gynaikes/marka:carinii"];
        $array28 = ["https://modivo.gr/c/gynaikes/marka:skechers"];
        $array29 = ["https://modivo.gr/c/gynaikes/marka:creole"];
        $array30 = ["https://modivo.gr/c/gynaikes/marka:champion"];
        //
        $array31 = ["https://modivo.gr/c/gynaikes/marka:lauren_ralph_lauren"];
        $array32 = ["https://modivo.gr/c/gynaikes/marka:eva_minge"];
        $array33 = ["https://modivo.gr/c/gynaikes/marka:desigual"];
        $array34 = ["https://modivo.gr/c/gynaikes/marka:levis_r"];
        $array35 = ["https://modivo.gr/c/gynaikes/marka:sprandi"];
        $array36 = ["https://modivo.gr/c/gynaikes/marka:4f"];
        $array37 = ["https://modivo.gr/c/gynaikes/marka:versace_jeans_couture"];
        $array38 = ["https://modivo.gr/c/gynaikes/marka:pepe_jeans"];
        $array39 = ["https://modivo.gr/c/gynaikes/marka:marella"];
        $array40 = ["https://modivo.gr/c/gynaikes/marka:triumph"];
        $array41 = ["https://modivo.gr/c/gynaikes/marka:rage_age"];
        $array42 = ["https://modivo.gr/c/gynaikes/marka:nobo"];
        $array43 = ["https://modivo.gr/c/gynaikes/marka:elisabetta_franchi"];
        $array44 = ["https://modivo.gr/c/gynaikes/marka:polo_ralph_lauren"];
        $array45 = ["https://modivo.gr/c/gynaikes/marka:converse"];

        $array46 = ["https://modivo.gr/c/gynaikes/marka:solo_femme"];
        $array47 = ["https://modivo.gr/c/gynaikes/marka:new_balance"];
        $array48 = ["https://modivo.gr/c/gynaikes/marka:new_era"];
        $array49 = ["https://modivo.gr/c/gynaikes/marka:sergio_bardi"];
        $array50 = ["https://modivo.gr/c/gynaikes/marka:cmp"];
        $array51 = ["https://modivo.gr/c/gynaikes/marka:simple?p=8"];
        $array52 = ["https://modivo.gr/c/gynaikes/marka:maciejka"];
        $array53 = ["https://modivo.gr/c/gynaikes/marka:valentino"];
        $array54 = ["https://modivo.gr/c/gynaikes/marka:baldaccini"];
        $array55 = ["https://modivo.gr/c/gynaikes/marka:reebok_classic"];
        $array56 = ["https://modivo.gr/c/gynaikes/marka:asics?p=8"];
        $array57 = ["https://modivo.gr/c/gynaikes/marka:etam?p=8"];
        $array58 = ["https://modivo.gr/c/gynaikes/marka:joop"];
        $array59 = ["https://modivo.gr/c/gynaikes/marka:melissa"];
        $array60 = ["https://modivo.gr/c/gynaikes/marka:karino"];
        $array61 = ["https://modivo.gr/c/gynaikes/marka:roxy"];
        $array62 = ["https://modivo.gr/c/gynaikes/marka:nessi"];
        $array63 = ["https://modivo.gr/c/gynaikes/marka:morgan"];
        $array64 = ["https://modivo.gr/c/gynaikes/marka:trussardi"];
        $array65 = ["https://modivo.gr/c/gynaikes/marka:monnari"];
        $array66 = ["https://modivo.gr/c/gynaikes/marka:wrangler"];
        $array67 = ["https://modivo.gr/c/gynaikes/marka:quazi"];
        $array68 = ["https://modivo.gr/c/gynaikes/marka:gabor"];
        $array69 = ["https://modivo.gr/c/gynaikes/marka:ellesse"];
        $array70 = ["https://modivo.gr/c/gynaikes/marka:columbia"];
        $array71 = ["https://modivo.gr/c/gynaikes/marka:seafolly"];
        $array72 = ["https://modivo.gr/c/gynaikes/marka:u_s_polo_assn"];
        $array73 = ["https://modivo.gr/c/gynaikes/marka:weekend_max_mara"];
        $array74 = ["https://modivo.gr/c/gynaikes/marka:s_oliver"];
        $array75 = ["https://modivo.gr/c/gynaikes/marka:vila"];
        $array76 = ["https://modivo.gr/c/gynaikes/marka:ugg"];
        $array77 = ["https://modivo.gr/c/gynaikes/marka:na_kd"];
        $array78 = ["https://modivo.gr/c/gynaikes/marka:clarks"];
        $array79 = ["https://modivo.gr/c/gynaikes/marka:chiara_ferragni"];
        $array80 = ["https://modivo.gr/c/gynaikes/marka:aldo"];

        $array81 = ["https://modivo.gr/c/gynaikes/marka:esotiq"];
        $array82 = ["https://modivo.gr/c/gynaikes/marka:under_armour"];
        $array83 = ["https://modivo.gr/c/gynaikes/marka:noisy_may"];
        $array84 = ["https://modivo.gr/c/gynaikes/marka:steve_madden"];
        $array85 = ["https://modivo.gr/c/gynaikes/marka:chantelle"];
        $array86 = ["https://modivo.gr/c/gynaikes/marka:refresh"];
        $array87 = ["https://modivo.gr/c/gynaikes/marka:dkny_sport"];
        $array88 = ["https://modivo.gr/c/gynaikes/marka:pieces"];

        $array89 = ["https://modivo.gr/c/gynaikes/marka:hogl"];
        $array90 = ["https://modivo.gr/c/gynaikes/marka:tory_burch"];
        $array91 = ["https://modivo.gr/c/gynaikes/marka:fly_london"];
        $array92 = ["https://modivo.gr/c/gynaikes/marka:birkenstock"];
        $array93 = ["https://modivo.gr/c/gynaikes/marka:crocs"];
        $array94 = ["https://modivo.gr/c/gynaikes/marka:marciano_guess"];
        $array95 = ["https://modivo.gr/c/gynaikes/marka:sisley"];
        $array96 = ["https://modivo.gr/c/gynaikes/marka:imperial"];

        $array97 = ["https://modivo.gr/c/andres/marka:only_sons"];
        $array98 = ["https://modivo.gr/c/andres/marka:vans"];
        $array99 = ["https://modivo.gr/c/andres/marka:puma"];
        $array100 = ["https://modivo.gr/c/andres/marka:levis_r"];
        $array101 = ["https://modivo.gr/c/andres/marka:champion"];
        $array102 = ["https://modivo.gr/c/andres/marka:nike"];
        $array103 = ["https://modivo.gr/c/andres/marka:new_era"];
        $array104 = ["https://modivo.gr/c/andres/marka:reebok"];
        $array105 = ["https://modivo.gr/c/andres/marka:polo_ralph_lauren"];
        $array106 = ["https://modivo.gr/c/andres/marka:fila"];
        $array107 = ["https://modivo.gr/c/andres/marka:selected_homme"];
        $array108 = ["https://modivo.gr/c/andres/marka:4f"];
        $array109 = ["https://modivo.gr/c/andres/marka:joop"];
        $array110 = ["https://modivo.gr/c/andres/marka:new_balance"];
        $array111 = ["https://modivo.gr/c/andres/marka:geox"];
        $array112 = ["https://modivo.gr/c/andres/marka:united_colors_of_benetton"];
        //
        $array113 = ["https://modivo.gr/c/andres/marka:skechers"];
        $array114 = ["https://modivo.gr/c/andres/marka:ellesse"];
        $array115 = ["https://modivo.gr/c/andres/marka:aeronautica_militare"];
        $array116 = ["https://modivo.gr/c/andres/marka:pepe_jeans"];
        $array117 = ["https://modivo.gr/c/andres/marka:quiksilver"];
        $array118 = ["https://modivo.gr/c/andres/marka:wrangler"];
        $array119 = ["https://modivo.gr/c/andres/marka:sprandi"];
        $array120 = ["https://modivo.gr/c/andres/marka:bugatti"];
        $array121 = ["https://modivo.gr/c/andres/marka:under_armour"];
        $array122 = ["https://modivo.gr/c/andres/marka:columbia"];
        $array123 = ["https://modivo.gr/c/andres/marka:big_star_shoes"];
        $array124 = ["https://modivo.gr/c/andres/marka:gino_rossi"];
        $array125 = ["https://modivo.gr/c/andres/marka:brave_soul"];
        $array126 = ["https://modivo.gr/c/andres/marka:asics?p=5"];
        $array127 = ["https://modivo.gr/c/andres/marka:tom_tailor"];
        $array128 = ["https://modivo.gr/c/andres/marka:dc"];
        $array129 = ["https://modivo.gr/c/andres/marka:helly_hansen"];
        $array130 = ["https://modivo.gr/c/andres/marka:napapijri"];
        $array131 = ["https://modivo.gr/c/andres/marka:alpha_industries"];
        $array132 = ["https://modivo.gr/c/andres/marka:converse"];
        $array133 = ["https://modivo.gr/c/andres/marka:lyle_scott"];
        $array134 = ["https://modivo.gr/c/andres/marka:reebok_classic?p=5"];
        $array135 = ["https://modivo.gr/c/paidia/marka:coccodrillo?p=5"];
        $array136 = ["https://modivo.gr/c/paidia/marka:adidas_originals?p=7"];
        $array137 = ["https://modivo.gr/c/paidia/marka:bartek?p=6"];
        $array138 = ["https://modivo.gr/c/paidia/marka:adidas_performance?p=7"];
        $array139 = ["https://modivo.gr/c/paidia/marka:vans?p=7"];
        $array140 = ["https://modivo.gr/c/paidia/marka:nelli_blu?p=12"];
        //
        $array141 = ["https://modivo.gr/c/paidia/marka:timberland"];
        $array142 = ["https://modivo.gr/c/paidia/marka:reima"];
        $array143 = ["https://modivo.gr/c/paidia/marka:puma"];
        $array144 = ["https://modivo.gr/c/paidia/marka:champion"];
        $array145 = ["https://modivo.gr/c/paidia/marka:garvalin"];
        $array146 = ["https://modivo.gr/c/paidia/marka:froddo"];
        $array147 = ["https://modivo.gr/c/paidia/marka:lego_wear"];
        $array148 = ["https://modivo.gr/c/paidia/marka:fila"];
        $array149 = ["https://modivo.gr/c/paidia/marka:bibi"];
        $array150 = ["https://modivo.gr/c/paidia/marka:biomecanics"];
        $array151 = ["https://modivo.gr/c/paidia/marka:pablosky"];
        $array152 = ["https://modivo.gr/c/paidia/marka:big_star_shoes"];
        $array153 = ["https://modivo.gr/c/paidia/marka:sprandi"];
        $array154 = ["https://modivo.gr/c/paidia/marka:boss"];
        $array155 = ["https://modivo.gr/c/paidia/marka:reebok"];
        $array156 = ["https://modivo.gr/c/paidia/marka:cmp"];
        $array157 = ["https://modivo.gr/c/paidia/marka:kickers"];
        $array158 = ["https://modivo.gr/c/paidia/marka:4f"];
        $array159 = ["https://modivo.gr/c/paidia/marka:naturino"];
        $array160 = ["https://modivo.gr/c/paidia/marka:skechers"];
        $array161 = ["https://modivo.gr/c/paidia/marka:ricosta"];
        $array162 = ["https://modivo.gr/c/paidia/marka:pepe_jeans?p=7"];
        $array163 = ["https://modivo.gr/c/paidia/marka:karl_lagerfeld"];
        $array164 = ["https://modivo.gr/c/paidia/marka:keen"];
        //
        $array165 = ["https://modivo.gr/c/paidia/marka:nike","https://modivo.gr/c/paidia/marka:the_north_face"];
        $array166 = ["https://modivo.gr/c/paidia/marka:new_era","https://modivo.gr/c/paidia/marka:buff"];
        $array167 = ["https://modivo.gr/c/paidia/marka:playshoes","https://modivo.gr/c/paidia/marka:native"];
        $array168 = ["https://modivo.gr/c/paidia/marka:new_balance","https://modivo.gr/c/paidia/marka:ellesse"];
        $array169 = ["https://modivo.gr/c/paidia/marka:tom_tailor","https://modivo.gr/c/paidia/marka:calvin_klein_underwear"];
        $array170 = ["https://modivo.gr/c/paidia/marka:crocs","https://modivo.gr/c/paidia/marka:ipanema"];
        $array171 = ["https://modivo.gr/c/paidia/marka:kangaroos","https://modivo.gr/c/paidia/marka:kappa"];
        $array172 = ["https://modivo.gr/c/paidia/marka:the_marc_jacobs","https://modivo.gr/c/paidia/marka:diadora"];
        $array173 = ["https://modivo.gr/c/paidia/marka:reebok_classic","https://modivo.gr/c/paidia/marka:birkenstock"];
        $array174 = ["https://modivo.gr/c/paidia/marka:blue_seven","https://modivo.gr/c/paidia/marka:billieblush"];
        $array175 = ["https://modivo.gr/c/paidia/marka:columbia","https://modivo.gr/c/paidia/marka:kamik"];
        $array176 = ["https://modivo.gr/c/paidia/marka:birba_trybeyond","https://modivo.gr/c/paidia/marka:deezee"];
        $array177 = ["https://modivo.gr/c/paidia/marka:jack_jones_junior","https://modivo.gr/c/paidia/marka:desigual"];
        $array178 = ["https://modivo.gr/c/paidia/marka:ugg","https://modivo.gr/c/paidia/marka:quiksilver"];
        $array179 = ["https://modivo.gr/c/paidia/marka:converse","https://modivo.gr/c/paidia/marka:roxy"];
        $array180 = ["https://modivo.gr/c/paidia/marka:dkny","https://modivo.gr/c/paidia/marka:hummel"];
        $array181 = ["https://modivo.gr/c/paidia/marka:regatta","https://modivo.gr/c/paidia/marka:asics"];
        $array182 = ["https://modivo.gr/c/paidia/marka:agatha_ruiz_de_la_prada","https://modivo.gr/c/paidia/marka:happy_socks"];
        $array183 = ["https://modivo.gr/c/paidia/marka:kids_only","https://modivo.gr/c/paidia/marka:napapijri"];
        $array184 = ["https://modivo.gr/c/paidia/marka:emu_australia","https://modivo.gr/c/paidia/marka:clarks"];
        $array185 = ["https://modivo.gr/c/paidia/marka:kenzo_kids","https://modivo.gr/c/paidia/marka:viking"];
        $array186 = ["https://modivo.gr/c/paidia/marka:hype","https://modivo.gr/c/paidia/marka:zippy"];
        $array187 = ["https://modivo.gr/c/paidia/marka:melissa","https://modivo.gr/c/paidia/marka:broel"];

        $array188=["https://modivo.gr/c/andres/marka:rieker?p=3","https://modivo.gr/c/andres/marka:etnies"];
        $array189=["https://modivo.gr/c/andres/marka:cmp","https://modivo.gr/c/andres/marka:salamander"];
        $array190=["https://modivo.gr/c/andres/marka:caterpillar","https://modivo.gr/c/andres/marka:mammut"];
        $array191=["https://modivo.gr/c/andres/marka:the_north_face","https://modivo.gr/c/andres/marka:dickies"];
        $array192=["https://modivo.gr/c/andres/marka:jack_jones_premium","https://modivo.gr/c/andres/marka:birkenstock"];
        $array193=["https://modivo.gr/c/andres/marka:lacoste","https://modivo.gr/c/andres/marka:deus_ex_machina"];
        $array194=["https://modivo.gr/c/andres/marka:rage_age","https://modivo.gr/c/andres/marka:rip_curl"];
        $array195=["https://modivo.gr/c/andres/marka:blend","https://modivo.gr/c/andres/marka:tom_tailor_denim"];
        $array196=["https://modivo.gr/c/andres/marka:s_oliver","https://modivo.gr/c/andres/marka:capslab"];
        $array197=["https://modivo.gr/c/andres/marka:calvin_klein_underwear","https://modivo.gr/c/andres/marka:petrol_industries"];
        $array198=["https://modivo.gr/c/andres/marka:u_s_polo_assn","https://modivo.gr/c/andres/marka:john_richmond"];
        $array199=["https://modivo.gr/c/andres/marka:versace_jeans_couture","https://modivo.gr/c/andres/marka:karl_kani"];
        $array200=["https://modivo.gr/c/andres/marka:la_martina","https://modivo.gr/c/andres/marka:carhartt_wip"];
        $array201=["https://modivo.gr/c/andres/marka:trussardi","https://modivo.gr/c/andres/marka:clarks"];
        $array202=["https://modivo.gr/c/andres/marka:47_brand","https://modivo.gr/c/andres/marka:casio"];
        $array203=["https://modivo.gr/c/andres/marka:mitchell_ness","https://modivo.gr/c/andres/marka:g_star_raw"];
        $array204=["https://modivo.gr/c/andres/marka:huf","https://modivo.gr/c/andres/marka:imperial"];
        $array205=["https://modivo.gr/c/andres/marka:joop_jeans","https://modivo.gr/c/andres/marka:paul_shark"];
        $array206=["https://modivo.gr/c/andres/marka:regatta","https://modivo.gr/c/andres/marka:herschel"];
        $array207=["https://modivo.gr/c/andres/marka:happy_socks","https://modivo.gr/c/andres/marka:2005"];
        $array208=["https://modivo.gr/c/andres/marka:lee","https://modivo.gr/c/andres/marka:head"];
        $array209=["https://modivo.gr/c/andres/marka:rains","https://modivo.gr/c/andres/marka:marc_opolo"];
        $array210=["https://modivo.gr/c/andres/marka:timberland","https://modivo.gr/c/andres/marka:gant"];
        $array211=["https://modivo.gr/c/andres/marka:salomon","https://modivo.gr/c/andres/marka:keen"];
        $array212=["https://modivo.gr/c/andres/marka:palladium","https://modivo.gr/c/andres/marka:jack_wolfskin"];
        $array213=["https://modivo.gr/c/andres/marka:buff","https://modivo.gr/c/andres/marka:le_coq_sportif"];
        $array214=["https://modivo.gr/c/andres/marka:blauer","https://modivo.gr/c/andres/marka:diadora"];
        $array215=["https://modivo.gr/c/andres/marka:north_sails","https://modivo.gr/c/andres/marka:billabong"];
        $this->sites = [
            'x1' => ['source' => $array1, 'offset' => 1, 'target' => 'modigr001.seo074.site'],

            "x2" => ['source' => $array2, 'offset' => 1, 'target' => 'modigr002.seo075.site'],
            "x3" => ['source' => $array3, 'offset' => 1, 'target' => 'modigr003.seo075.site'],
            "x4" => ['source' => $array4, 'offset' => 1, 'target' => 'modigr004.seo075.site'],
            "x5" => ['source' => $array5, 'offset' => 1, 'target' => 'modigr005.seo075.site'],
            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'modigr006.seo075.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'modigr007.seo075.site'],
            "x8" => ['source' => $array8, 'offset' => 1, 'target' => 'modigr008.seo075.site'],
            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'modigr009.seo075.site'],
            "x10" => ['source' => $array10, 'offset' => 1, 'target' => 'modigr010.seo075.site'],
            "x11" => ['source' => $array11, 'offset' => 1, 'target' => 'modigr011.seo075.site'],
            "x12" => ['source' => $array12, 'offset' => 1, 'target' => 'modigr012.seo075.site'],
            "x13" => ['source' => $array13, 'offset' => 1, 'target' => 'modigr013.seo075.site'],
            "x14" => ['source' => $array14, 'offset' => 1, 'target' => 'modigr014.seo075.site'],
            "x15" => ['source' => $array15, 'offset' => 1, 'target' => 'modigr015.seo075.site'],
            //
            "x16" => ['source' => $array16, 'offset' => 1, 'target' => 'modigr016.seo075.site'],
            "x17" => ['source' => $array17, 'offset' => 1, 'target' => 'modigr017.seo075.site'],
            "x18" => ['source' => $array18, 'offset' => 1, 'target' => 'modigr018.seo075.site'],
            "x19" => ['source' => $array19, 'offset' => 1, 'target' => 'modigr019.seo075.site'],
            "x20" => ['source' => $array20, 'offset' => 1, 'target' => 'modigr020.seo075.site'],
            "x21" => ['source' => $array21, 'offset' => 1, 'target' => 'modigr021.seo075.site'],
            "x22" => ['source' => $array22, 'offset' => 1, 'target' => 'modigr022.seo075.site'],
            "x23" => ['source' => $array23, 'offset' => 1, 'target' => 'modigr023.seo075.site'],
            "x24" => ['source' => $array24, 'offset' => 1, 'target' => 'modigr024.seo075.site'],
            "x25" => ['source' => $array25, 'offset' => 1, 'target' => 'modigr025.seo075.site'],
            "x26" => ['source' => $array26, 'offset' => 1, 'target' => 'modigr026.seo075.site'],
            "x27" => ['source' => $array27, 'offset' => 1, 'target' => 'modigr027.seo075.site'],
            "x28" => ['source' => $array28, 'offset' => 1, 'target' => 'modigr028.seo075.site'],
            "x29" => ['source' => $array29, 'offset' => 1, 'target' => 'modigr029.seo075.site'],
            "x30" => ['source' => $array30, 'offset' => 1, 'target' => 'modigr030.seo075.site'],
            //
            "x31" => ['source' => $array31, 'offset' => 1, 'target' => 'modigr031.seo075.site'],
            "x32" => ['source' => $array32, 'offset' => 1, 'target' => 'modigr032.seo075.site'],
            "x33" => ['source' => $array33, 'offset' => 1, 'target' => 'modigr033.seo075.site'],
            "x34" => ['source' => $array34, 'offset' => 1, 'target' => 'modigr034.seo075.site'],
            "x35" => ['source' => $array35, 'offset' => 1, 'target' => 'modigr035.seo075.site'],
            "x36" => ['source' => $array36, 'offset' => 1, 'target' => 'modigr036.seo075.site'],
            "x37" => ['source' => $array37, 'offset' => 1, 'target' => 'modigr037.seo075.site'],
            "x38" => ['source' => $array38, 'offset' => 1, 'target' => 'modigr038.seo075.site'],
            "x39" => ['source' => $array39, 'offset' => 1, 'target' => 'modigr039.seo075.site'],
            "x40" => ['source' => $array40, 'offset' => 1, 'target' => 'modigr040.seo075.site'],
            "x41" => ['source' => $array41, 'offset' => 1, 'target' => 'modigr041.seo075.site'],
            "x42" => ['source' => $array42, 'offset' => 1, 'target' => 'modigr042.seo075.site'],
            "x43" => ['source' => $array43, 'offset' => 1, 'target' => 'modigr043.seo075.site'],
            "x44" => ['source' => $array44, 'offset' => 1, 'target' => 'modigr044.seo075.site'],
            "x45" => ['source' => $array45, 'offset' => 1, 'target' => 'modigr045.seo075.site'],
            //
            "x46" => ['source' => $array46, 'offset' => 1, 'target' => 'modigr046.seo076.site'],
            "x47" => ['source' => $array47, 'offset' => 1, 'target' => 'modigr047.seo076.site'],
            "x48" => ['source' => $array48, 'offset' => 1, 'target' => 'modigr048.seo076.site'],
            "x49" => ['source' => $array49, 'offset' => 1, 'target' => 'modigr049.seo076.site'],
            "x50" => ['source' => $array50, 'offset' => 1, 'target' => 'modigr050.seo076.site'],
            "x51" => ['source' => $array51, 'offset' => 1, 'target' => 'modigr051.seo076.site'],
            "x52" => ['source' => $array52, 'offset' => 1, 'target' => 'modigr052.seo076.site'],
            "x53" => ['source' => $array53, 'offset' => 1, 'target' => 'modigr053.seo076.site'],
            "x54" => ['source' => $array54, 'offset' => 1, 'target' => 'modigr054.seo076.site'],
            "x55" => ['source' => $array55, 'offset' => 1, 'target' => 'modigr055.seo076.site'],
            "x56" => ['source' => $array56, 'offset' => 1, 'target' => 'modigr056.seo076.site'],
            "x57" => ['source' => $array57, 'offset' => 1, 'target' => 'modigr057.seo076.site'],
            "x58" => ['source' => $array58, 'offset' => 1, 'target' => 'modigr058.seo076.site'],
            "x59" => ['source' => $array59, 'offset' => 1, 'target' => 'modigr059.seo076.site'],
            "x60" => ['source' => $array60, 'offset' => 1, 'target' => 'modigr060.seo076.site'],
//
            "x61" => ['source' => $array61, 'offset' => 1, 'target' => 'modigr061.seo077.site'],
            "x62" => ['source' => $array62, 'offset' => 1, 'target' => 'modigr062.seo077.site'],
            "x63" => ['source' => $array63, 'offset' => 1, 'target' => 'modigr063.seo077.site'],
            "x64" => ['source' => $array64, 'offset' => 1, 'target' => 'modigr064.seo077.site'],
            "x65" => ['source' => $array65, 'offset' => 1, 'target' => 'modigr065.seo077.site'],
            "x66" => ['source' => $array66, 'offset' => 1, 'target' => 'modigr066.seo077.site'],
            "x67" => ['source' => $array67, 'offset' => 1, 'target' => 'modigr067.seo077.site'],
            "x68" => ['source' => $array68, 'offset' => 1, 'target' => 'modigr068.seo077.site'],
            "x69" => ['source' => $array69, 'offset' => 1, 'target' => 'modigr069.seo077.site'],
            "x70" => ['source' => $array70, 'offset' => 1, 'target' => 'modigr070.seo077.site'],
            "x71" => ['source' => $array71, 'offset' => 1, 'target' => 'modigr071.seo077.site'],
            "x72" => ['source' => $array72, 'offset' => 1, 'target' => 'modigr072.seo077.site'],
            "x73" => ['source' => $array73, 'offset' => 1, 'target' => 'modigr073.seo077.site'],
            "x74" => ['source' => $array74, 'offset' => 1, 'target' => 'modigr074.seo077.site'],
            "x75" => ['source' => $array75, 'offset' => 1, 'target' => 'modigr075.seo077.site'],
            "x76" => ['source' => $array76, 'offset' => 1, 'target' => 'modigr076.seo077.site'],
            "x77" => ['source' => $array77, 'offset' => 1, 'target' => 'modigr077.seo077.site'],
            "x78" => ['source' => $array78, 'offset' => 1, 'target' => 'modigr078.seo077.site'],
            "x79" => ['source' => $array79, 'offset' => 1, 'target' => 'modigr079.seo077.site'],
            "x80" => ['source' => $array80, 'offset' => 1, 'target' => 'modigr080.seo077.site'],

            "x81" => ['source' => $array81, 'offset' => 1, 'target' => 'modigr081.seo077.site'],
            "x82" => ['source' => $array82, 'offset' => 1, 'target' => 'modigr082.seo077.site'],
            "x83" => ['source' => $array83, 'offset' => 1, 'target' => 'modigr083.seo077.site'],
            "x84" => ['source' => $array84, 'offset' => 1, 'target' => 'modigr084.seo077.site'],
            "x85" => ['source' => $array85, 'offset' => 1, 'target' => 'modigr085.seo077.site'],
            "x86" => ['source' => $array86, 'offset' => 1, 'target' => 'modigr086.seo077.site'],
            "x87" => ['source' => $array87, 'offset' => 1, 'target' => 'modigr087.seo077.site'],
            "x88" => ['source' => $array88, 'offset' => 1, 'target' => 'modigr088.seo077.site'],

            "x89" => ['source' => $array89, 'offset' => 1, 'target' => 'modigr089.seo077.site'],
            "x90" => ['source' => $array90, 'offset' => 1, 'target' => 'modigr090.seo077.site'],
            "x91" => ['source' => $array91, 'offset' => 1, 'target' => 'modigr091.seo077.site'],
            "x92" => ['source' => $array92, 'offset' => 1, 'target' => 'modigr092.seo077.site'],
            "x93" => ['source' => $array93, 'offset' => 1, 'target' => 'modigr093.seo077.site'],
            "x94" => ['source' => $array94, 'offset' => 1, 'target' => 'modigr094.seo077.site'],
            "x95" => ['source' => $array95, 'offset' => 1, 'target' => 'modigr095.seo077.site'],
            "x96" => ['source' => $array96, 'offset' => 1, 'target' => 'modigr096.seo077.site'],
            //
            "x97" => ['source' => $array97, 'offset' => 1, 'target' => 'modigr097.seo078.site'],
            "x98" => ['source' => $array98, 'offset' => 1, 'target' => 'modigr098.seo078.site'],
            "x99" => ['source' => $array99, 'offset' => 1, 'target' => 'modigr099.seo078.site'],
            "x100" => ['source' => $array100, 'offset' => 1, 'target' => 'modigr100.seo078.site'],
            "x101" => ['source' => $array101, 'offset' => 1, 'target' => 'modigr101.seo078.site'],
            "x102" => ['source' => $array102, 'offset' => 1, 'target' => 'modigr102.seo078.site'],
            "x103" => ['source' => $array103, 'offset' => 1, 'target' => 'modigr103.seo078.site'],
            "x104" => ['source' => $array104, 'offset' => 1, 'target' => 'modigr104.seo078.site'],
            "x105" => ['source' => $array105, 'offset' => 1, 'target' => 'modigr105.seo078.site'],
            "x106" => ['source' => $array106, 'offset' => 1, 'target' => 'modigr106.seo078.site'],
            "x107" => ['source' => $array107, 'offset' => 1, 'target' => 'modigr107.seo078.site'],
            "x108" => ['source' => $array108, 'offset' => 1, 'target' => 'modigr108.seo078.site'],
            "x109" => ['source' => $array109, 'offset' => 1, 'target' => 'modigr109.seo078.site'],
            "x110" => ['source' => $array110, 'offset' => 1, 'target' => 'modigr110.seo078.site'],
            "x111" => ['source' => $array111, 'offset' => 1, 'target' => 'modigr111.seo078.site'],
            "x112" => ['source' => $array112, 'offset' => 1, 'target' => 'modigr112.seo078.site'],
            //
            "x113" => ['source' => $array113, 'offset' => 1, 'target' => 'modigr113.seo078.site'],
            "x114" => ['source' => $array114, 'offset' => 1, 'target' => 'modigr114.seo078.site'],
            "x115" => ['source' => $array115, 'offset' => 1, 'target' => 'modigr115.seo078.site'],
            "x116" => ['source' => $array116, 'offset' => 1, 'target' => 'modigr116.seo078.site'],
            "x117" => ['source' => $array117, 'offset' => 1, 'target' => 'modigr117.seo078.site'],
            "x118" => ['source' => $array118, 'offset' => 1, 'target' => 'modigr118.seo078.site'],
            "x119" => ['source' => $array119, 'offset' => 1, 'target' => 'modigr119.seo078.site'],
            "x120" => ['source' => $array120, 'offset' => 1, 'target' => 'modigr120.seo078.site'],
            "x121" => ['source' => $array121, 'offset' => 1, 'target' => 'modigr121.seo078.site'],
            "x122" => ['source' => $array122, 'offset' => 1, 'target' => 'modigr122.seo078.site'],
            "x123" => ['source' => $array123, 'offset' => 1, 'target' => 'modigr123.seo078.site'],
            "x124" => ['source' => $array124, 'offset' => 1, 'target' => 'modigr124.seo078.site'],
            "x125" => ['source' => $array125, 'offset' => 1, 'target' => 'modigr125.seo078.site'],
            "x126" => ['source' => $array126, 'offset' => 1, 'target' => 'modigr126.seo078.site'],
            "x127" => ['source' => $array127, 'offset' => 1, 'target' => 'modigr127.seo078.site'],
            "x128" => ['source' => $array128, 'offset' => 1, 'target' => 'modigr128.seo078.site'],
            "x129" => ['source' => $array129, 'offset' => 1, 'target' => 'modigr129.seo078.site'],
            "x130" => ['source' => $array130, 'offset' => 1, 'target' => 'modigr130.seo078.site'],
            "x131" => ['source' => $array131, 'offset' => 1, 'target' => 'modigr131.seo078.site'],
            "x132" => ['source' => $array132, 'offset' => 1, 'target' => 'modigr132.seo078.site'],
            "x133" => ['source' => $array133, 'offset' => 1, 'target' => 'modigr133.seo078.site'],
            "x134" => ['source' => $array134, 'offset' => 1, 'target' => 'modigr134.seo078.site'],
            //
            "x135" => ['source' => $array135, 'offset' => 1, 'target' => 'modigr135.seo078.site'],
            "x136" => ['source' => $array136, 'offset' => 1, 'target' => 'modigr136.seo078.site'],
            "x137" => ['source' => $array137, 'offset' => 1, 'target' => 'modigr137.seo078.site'],
            "x138" => ['source' => $array138, 'offset' => 1, 'target' => 'modigr138.seo078.site'],
            "x139" => ['source' => $array139, 'offset' => 1, 'target' => 'modigr139.seo078.site'],
            "x140" => ['source' => $array140, 'offset' => 1, 'target' => 'modigr140.seo078.site'],
            //
            "x141" => ['source' => $array141, 'offset' => 1, 'target' => 'modigr141.seo078.site'],
            "x142" => ['source' => $array142, 'offset' => 1, 'target' => 'modigr142.seo078.site'],
            "x143" => ['source' => $array143, 'offset' => 1, 'target' => 'modigr143.seo078.site'],
            "x144" => ['source' => $array144, 'offset' => 1, 'target' => 'modigr144.seo078.site'],
            "x145" => ['source' => $array145, 'offset' => 1, 'target' => 'modigr145.seo078.site'],
            "x146" => ['source' => $array146, 'offset' => 1, 'target' => 'modigr146.seo078.site'],
            "x147" => ['source' => $array147, 'offset' => 1, 'target' => 'modigr147.seo078.site'],
            //
            "x148" => ['source' => $array148, 'offset' => 1, 'target' => 'modigr148.seo079.site'],
            "x149" => ['source' => $array149, 'offset' => 1, 'target' => 'modigr149.seo079.site'],
            "x150" => ['source' => $array150, 'offset' => 1, 'target' => 'modigr150.seo079.site'],
            "x151" => ['source' => $array151, 'offset' => 1, 'target' => 'modigr151.seo079.site'],
            "x152" => ['source' => $array152, 'offset' => 1, 'target' => 'modigr152.seo079.site'],
            "x153" => ['source' => $array153, 'offset' => 1, 'target' => 'modigr153.seo079.site'],
            "x154" => ['source' => $array154, 'offset' => 1, 'target' => 'modigr154.seo079.site'],
            "x155" => ['source' => $array155, 'offset' => 1, 'target' => 'modigr155.seo079.site'],
            "x156" => ['source' => $array156, 'offset' => 1, 'target' => 'modigr156.seo079.site'],
            "x157" => ['source' => $array157, 'offset' => 1, 'target' => 'modigr157.seo079.site'],
            "x158" => ['source' => $array158, 'offset' => 1, 'target' => 'modigr158.seo079.site'],
            "x159" => ['source' => $array159, 'offset' => 1, 'target' => 'modigr159.seo079.site'],
            "x160" => ['source' => $array160, 'offset' => 1, 'target' => 'modigr160.seo079.site'],
            "x161" => ['source' => $array161, 'offset' => 1, 'target' => 'modigr161.seo079.site'],
            "x162" => ['source' => $array162, 'offset' => 1, 'target' => 'modigr162.seo079.site'],
            "x163" => ['source' => $array163, 'offset' => 1, 'target' => 'modigr163.seo079.site'],
            "x164" => ['source' => $array164, 'offset' => 1, 'target' => 'modigr164.seo079.site'],
            //
            "x165"=> ['source' => $array165, 'offset' => 1, 'target' => 'modigr165.seo079.site'],
            "x166"=> ['source' => $array166, 'offset' => 1, 'target' => 'modigr166.seo079.site'],
            "x167"=> ['source' => $array167, 'offset' => 1, 'target' => 'modigr167.seo079.site'],
            "x168"=> ['source' => $array168, 'offset' => 1, 'target' => 'modigr168.seo079.site'],
            "x169"=> ['source' => $array169, 'offset' => 1, 'target' => 'modigr169.seo079.site'],
            "x170"=> ['source' => $array170, 'offset' => 1, 'target' => 'modigr170.seo079.site'],
            "x171"=> ['source' => $array171, 'offset' => 1, 'target' => 'modigr171.seo079.site'],
            "x172"=> ['source' => $array172, 'offset' => 1, 'target' => 'modigr172.seo079.site'],
            "x173"=> ['source' => $array173, 'offset' => 1, 'target' => 'modigr173.seo079.site'],
            "x174"=> ['source' => $array174, 'offset' => 1, 'target' => 'modigr174.seo079.site'],
            "x175"=> ['source' => $array175, 'offset' => 1, 'target' => 'modigr175.seo079.site'],
            "x176"=> ['source' => $array176, 'offset' => 1, 'target' => 'modigr176.seo079.site'],
            "x177"=> ['source' => $array177, 'offset' => 1, 'target' => 'modigr177.seo079.site'],
            "x178"=> ['source' => $array178, 'offset' => 1, 'target' => 'modigr178.seo079.site'],
            "x179"=> ['source' => $array179, 'offset' => 1, 'target' => 'modigr179.seo079.site'],
            "x180"=> ['source' => $array180, 'offset' => 1, 'target' => 'modigr180.seo079.site'],
            "x181"=> ['source' => $array181, 'offset' => 1, 'target' => 'modigr181.seo079.site'],
            "x182"=> ['source' => $array182, 'offset' => 1, 'target' => 'modigr182.seo079.site'],
            "x183"=> ['source' => $array183, 'offset' => 1, 'target' => 'modigr183.seo079.site'],
            "x184"=> ['source' => $array184, 'offset' => 1, 'target' => 'modigr184.seo079.site'],
            "x185"=> ['source' => $array185, 'offset' => 1, 'target' => 'modigr185.seo079.site'],
            "x186"=> ['source' => $array186, 'offset' => 1, 'target' => 'modigr186.seo079.site'],
            "x187"=> ['source' => $array187, 'offset' => 1, 'target' => 'modigr187.seo079.site'],
            //
            "x188"=> ['source' => $array188, 'offset' => 1, 'target' => 'modigr188.seo079.site'],
            "x189"=> ['source' => $array189, 'offset' => 1, 'target' => 'modigr189.seo079.site'],
            "x190"=> ['source' => $array190, 'offset' => 1, 'target' => 'modigr190.seo079.site'],
            "x191"=> ['source' => $array191, 'offset' => 1, 'target' => 'modigr191.seo079.site'],
            "x192"=> ['source' => $array192, 'offset' => 1, 'target' => 'modigr192.seo079.site'],
            "x193"=> ['source' => $array193, 'offset' => 1, 'target' => 'modigr193.seo079.site'],
            "x194"=> ['source' => $array194, 'offset' => 1, 'target' => 'modigr194.seo079.site'],
            "x195"=> ['source' => $array195, 'offset' => 1, 'target' => 'modigr195.seo079.site'],
            "x196"=> ['source' => $array196, 'offset' => 1, 'target' => 'modigr196.seo079.site'],
            "x197"=> ['source' => $array197, 'offset' => 1, 'target' => 'modigr197.seo079.site'],
            "x198"=> ['source' => $array198, 'offset' => 1, 'target' => 'modigr198.seo079.site'],
            "x199"=> ['source' => $array199, 'offset' => 1, 'target' => 'modigr199.seo079.site'],
            "x200"=> ['source' => $array200, 'offset' => 1, 'target' => 'modigr200.seo079.site'],
            "x201"=> ['source' => $array201, 'offset' => 1, 'target' => 'modigr201.seo079.site'],
            "x202"=> ['source' => $array202, 'offset' => 1, 'target' => 'modigr202.seo079.site'],
            "x203"=> ['source' => $array203, 'offset' => 1, 'target' => 'modigr203.seo079.site'],
            "x204"=> ['source' => $array204, 'offset' => 1, 'target' => 'modigr204.seo079.site'],
            "x205"=> ['source' => $array205, 'offset' => 1, 'target' => 'modigr205.seo079.site'],
            "x206"=> ['source' => $array206, 'offset' => 1, 'target' => 'modigr206.seo079.site'],
            "x207"=> ['source' => $array207, 'offset' => 1, 'target' => 'modigr207.seo079.site'],
            "x208"=> ['source' => $array208, 'offset' => 1, 'target' => 'modigr208.seo079.site'],
            "x209"=> ['source' => $array209, 'offset' => 1, 'target' => 'modigr209.seo079.site'],
            "x210"=> ['source' => $array210, 'offset' => 1, 'target' => 'modigr210.seo079.site'],
            "x211"=> ['source' => $array211, 'offset' => 1, 'target' => 'modigr211.seo079.site'],
            "x212"=> ['source' => $array212, 'offset' => 1, 'target' => 'modigr212.seo079.site'],
            "x213"=> ['source' => $array213, 'offset' => 1, 'target' => 'modigr213.seo079.site'],
            "x214"=> ['source' => $array214, 'offset' => 1, 'target' => 'modigr214.seo079.site'],
            "x215"=> ['source' => $array215, 'offset' => 1, 'target' => 'modigr215.seo079.site'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://modivo.gr/m/gynaikes.html',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.42',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",

            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.content a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter(".product-card-name.product.name")->text(),
                    'url' => sprintf('%s', "https://modivo.gr" . $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.name._heading-h4')->text();
        $product['price'] = json_decode($crawler->filter("script")->eq(4)->text(), true)["offers"]["price"];
        $product['brand'] = json_decode($crawler->filter("script")->eq(4)->text(), true)["brand"]['name'];
        $images = json_decode($crawler->filter("script")->eq(4)->text(), true)["image"];
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $product['images'] = $this->toEncryptImage($product['images']);
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb-list .breadcrumb a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 3);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['variations'] = [];
        $product['attributes'] = [];
        $product['keywords'] = [];
//        $product['gender'] = " ";
//        $product['gender'] = " ";
        if ($breadcrumbs[2] == "Γυναίκες") {
            $product['gender'] = '';
        } else {
            if (strpos($product['title'], $breadcrumbs[2])) {
                $product['gender'] = " ";
            } else {
                $product['gender'] = $breadcrumbs[2];
            }
        }
        if (strpos($product['title'], end($breadcrumbs))) {
            $product['subCategory'] = " ";
        } else {
            $product['subCategory'] = end($breadcrumbs);
        }
        $color = $crawler->filter('.color-value')->text();
        if (strpos($product['title'], $color)) {
            $product['color'] = "";
        } else {
            $product['color'] = $color;
        }
//        $product['tags'][] = $product['color'];
        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $str = $crawler->filter("script")->eq(7)->text();

        if (in_array("Σκούφοι & Καπέλα", $breadcrumbs)) {
            $product['type'] = 'simple';
        } else if (in_array("Παπούτσια", $breadcrumbs) | strpos($product['title'], "Παπούτσια") | in_array("Αθλητικά Παπούτσια", $breadcrumbs) | in_array("Υποδήματα αθλητικά", $breadcrumbs) | in_array("Υποδήματα", $breadcrumbs)) {
            $opt = ["35","36","37","38","39","40","41","42","43","44"];
            $product['type'] = 'variable';
            $product['variations'][] = [
                'name' => 'μεγέθους',
                'options' => $opt,];
        } else if ($crawler->filter(".choose-size-button .content")->text() == "Επιλογή μεγέθους") {
            preg_match_all('/clothes_size\",\{\}.*?}/', $str, $raw_choice);
            if (sizeof($raw_choice[0]) == 0 | $raw_choice[0][0] == "00") {
                $product['type'] = 'simple';
            } else {
                foreach ($raw_choice[0] as $v) {
                    $a = explode(",", $v);
                }
                foreach ($a as $k) {
                    if (strpos($k, "-") and strlen(substr($k, 0, strpos($k, "-"))) == 14) {
                        $b[] = $k;
                    }
                }
                foreach ($b as $h) {
                    $i = strpos($h, "-");
                    $b = str_replace('"', "", substr($h, $i + strlen("-")));
                    $c = str_replace("\u002F", "/", $b);
                    $opt[] = $c;
                }
                $product['type'] = 'variable';
                $product['variations'][] = [
                    'name' => 'μεγέθους',
                    'options' => $opt,];
            }
        } else {
            $product['type'] = 'simple';
        }
        if ($crawler->filter(".accordion-item")->eq(2)->count()) {
            $count = $crawler->filter(".accordion-item")->eq(2)->filter(".accordion-content .content-wrapper div .product-accordion-content .product-accordion-row")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => str_replace(":", "", $crawler->filter(".accordion-item")->eq(2)->filter(".accordion-content .content-wrapper div .product-accordion-content .product-accordion-row .row-label")->eq($i)->filter('span')->text()),
                    'options' => [$crawler->filter(".accordion-item")->eq(2)->filter(".accordion-content .content-wrapper div .product-accordion-content .product-accordion-row .row-value")->eq($i)->filter('span')->text()],
                ];
            }
        }
        $description = $crawler->filter(".product-accordion-description .product-accordion-row")->each(function (Crawler $node, $i) {
            $del = ["span", "</> ", "\n"];
            return str_replace($del, "", $node->html()) . "\n";
        });
        $product['description'] = implode($description);
//        print_r($product['description']);
        $product['sku'] = json_decode($crawler->filter("script")->eq(4)->text(), true)["sku"];
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
//        print_r($product);
    }

    public function toEncryptImage($images, $original = 'original')
    {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}