<?php

namespace app\command\MSK\No;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class SportenbeitostolenNo extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:sportenbeitostolen')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://sportenbeitostolen.no/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = [
            "https://sportenbeitostolen.no/produkt-kategori/produkter/friluftsutstyr/fiskeutstyr/"
        ];
        $array2 = [
            "https://sportenbeitostolen.no/produkt-kategori/dame/klaer-til-dame/"
        ];
        $array3 = [
            "https://sportenbeitostolen.no/produkt-kategori/produkter/friluftsutstyr/fiskeutstyr/i-enden-av-snoret/"

        ];
        $array4 = [
            "https://sportenbeitostolen.no/produkt-kategori/produkter/vintersport/skismoring/"
        ];
        $array5 = [
            "https://sportenbeitostolen.no/produkt-kategori/herre/klaer-til-herre/"
        ];
        $array6 = [
            "https://sportenbeitostolen.no/produkt-kategori/dame/tilbehor/",
            "https://sportenbeitostolen.no/produkt-kategori/produkter/vintersport/langrenn/"
        ];
        $array7 = [
            "https://sportenbeitostolen.no/produkt-kategori/produkter/friluftsutstyr/fiskeutstyr/duo/",
            "https://sportenbeitostolen.no/produkt-kategori/herre/tilbehor-til-herre/"
        ];


        $this->sites = [
            "x1" => ['source' => $array1, 'offset' => 1, 'target' => 'tolenno001.seo078.site'],
            "x2" => ['source' => $array2, 'offset' => 1, 'target' => 'tolenno002.seo078.site'],
            "x3" => ['source' => $array3, 'offset' => 1, 'target' => 'tolenno003.seo078.site'],
            "x4" => ['source' => $array4, 'offset' => 1, 'target' => 'tolenno004.seo078.site'],
            "x5" => ['source' => $array5, 'offset' => 1, 'target' => 'tolenno005.seo078.site'],
            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'tolenno006.seo078.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'tolenno007.seo078.site'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://sportenbeitostolen.no/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",

            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.woocommerce-loop-product__link')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('h2')->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.entry-title')->text();

        $del = [" ", "kr"];
        $product['price'] = (float)str_replace($del, "", $crawler->filter('.price')->text()) * 0.09765;
        $product['brand'] = " ";

        $images = array_unique(array_filter($crawler->filter('.woocommerce-product-gallery__image a')->each(function (Crawler $node, $i) {
            return $node->attr("href");
        })));

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }

        $breadcrumbs = array_filter($crawler->filter('.woocommerce-breadcrumb a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = $breadcrumbs[1];
        $product['color'] = '';
        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
        $product['tags'][] = [];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];


        if ($crawler->filter('.size-variations span')->count()) {
            $opt = $crawler->filter('.size-variations span')->each(function (Crawler $node, $i) {
                return $node->text();
            });
            $product['type'] = 'variable';
            $product['variations'][] = [
                'name' => 'Størrelse',
                'options' => $opt,];
        } else {
            $product['type'] = 'simple';
        }
        if ($crawler->filter(".product-detail-block-attributes__table table tr")->count()) {
            $count = $crawler->filter(".product-detail-block-attributes__table table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('th')->text(),
                    'options' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('td')->text(),
                ];
            }
        }
        $del = ["<h2>Sporten mener</h2>", "\n"];
        $product['description'] = trim(str_replace($del, "", $crawler->filter('.woocommerce-Tabs-panel--description')->html()));
        $product['sku'] = $crawler->filter('[name="gtm4wp_sku"]')->attr('value');
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}