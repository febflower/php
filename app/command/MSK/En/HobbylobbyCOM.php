<?php


namespace app\command\MSK\En;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class HobbylobbyCOM extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:hobbylobbycom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.hobbylobby.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = ["https://www.hobbylobby.com/Scrapbook-Paper-Crafts/Stickers/c/7-153"];
        $array2 = ["https://www.hobbylobby.com/Home-Decor-Frames/Decorative-Storage/c/3-112"];
        $array3 = ["https://www.hobbylobby.com/Home-Decor-Frames/Candles-Fragrance/c/3-111"];
        $array4 = ["https://www.hobbylobby.com/Crafts-Hobbies/Hobbies-Collecting/c/9-183"];
        $array5 = ["https://www.hobbylobby.com/Beads-Jewelry/Finished-Jewelry/c/2-106"];
        $array6 = ["https://www.hobbylobby.com/Home-Decor-Frames/Frames-Photo-Albums/c/3-113"];
        $array7 = ["https://www.hobbylobby.com/Floral-Weddings/Wedding/c/4-115"];
        $array8 = ["https://www.hobbylobby.com/Fabric-Sewing/Quilting-Fabrics/c/6-136"];
        $array9 = ["https://www.hobbylobby.com/Beads-Jewelry/Jewelry-Findings/c/2-103"];
        $array10 = ["https://www.hobbylobby.com/Crafts-Hobbies/Model-Kits/c/9-184"];
        $array11 = ["https://www.hobbylobby.com/Art-Supplies/Drawing-Illustration/c/8-167"];
        $array12 = ["https://www.hobbylobby.com/Scrapbook-Paper-Crafts/Stamping/c/7-152"];
        $array13 = ["https://www.hobbylobby.com/Fabric-Sewing/Sports-Fabrics/c/6-139"];
        $array14 = ["https://www.hobbylobby.com/Art-Supplies/Painting-Canvas-Art-Surfaces/c/8-165"];
        $array15 = ["https://www.hobbylobby.com/Scrapbook-Paper-Crafts/Die-Cut-Machines-Accessories/c/7-148",
            "https://www.hobbylobby.com/Art-Supplies/Books/c/8-160"];


        $this->sites = [
            "x1" => ['source' => $array1, 'offset' => 1, 'target' => 'bylobby001.xms011.site'],
            "x2" => ['source' => $array2, 'offset' => 1, 'target' => 'bylobby002.xms011.site'],
            "x3" => ['source' => $array3, 'offset' => 1, 'target' => 'bylobby003.xms011.site'],
            "x4" => ['source' => $array4, 'offset' => 1, 'target' => 'bylobby004.xms011.site'],
            "x5" => ['source' => $array5, 'offset' => 1, 'target' => 'bylobby005.xms011.site'],
            "x6" => ['source' => $array6, 'offset' => 1, 'target' => 'bylobby006.xms011.site'],
            "x7" => ['source' => $array7, 'offset' => 1, 'target' => 'bylobby007.xms011.site'],
            "x8" => ['source' => $array8, 'offset' => 1, 'target' => 'bylobby008.xms011.site'],
            "x9" => ['source' => $array9, 'offset' => 1, 'target' => 'bylobby009.xms011.site'],
            "x10" => ['source' => $array10, 'offset' => 1, 'target' => 'bylobby010.xms011.site'],
            "x11" => ['source' => $array11, 'offset' => 1, 'target' => 'bylobby011.xms011.site'],
            "x12" => ['source' => $array12, 'offset' => 1, 'target' => 'bylobby012.xms011.site'],
            "x13" => ['source' => $array13, 'offset' => 1, 'target' => 'bylobby013.xms011.site'],
            "x14" => ['source' => $array14, 'offset' => 1, 'target' => 'bylobby014.xms011.site'],
            "x15" => ['source' => $array15, 'offset' => 1, 'target' => 'bylobby015.xms011.site'],


        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.hobbylobby.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
//            "ck_713d71b577cf93a67a38fc1782d548f75e4da922",
//            "cs_cea572c1f296a0e0d336d089e04d13b775c83c92",
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('.pagination-button')->last();
        if ($nextNode->count()) {
            $this->processPage('https://www.hobbylobby.com' . $nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {

        $crawler->filter('.col-content.product')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.card-content-wrapper .card-content .title')->text(),
                    'url' => sprintf('%s', 'https://www.hobbylobby.com' . $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.product-title')->text();
        $price = (float)$crawler->filter('.current .visuallyhidden')->last()->attr("content");
        $product['price'] = $price + 5;
        $product['brand'] = " ";
        if ($crawler->filter('.gallery-slider-nav .thumbnail-button img')->count()) {
            $images = array_unique(array_filter($crawler->filter('.gallery-slider-nav .thumbnail-button img')->each(function (Crawler $node, $i) {
                return str_replace("72Wx72H", "350Wx350H", $node->attr("src"));
            })));
        } else {
            $images = $crawler->filter('.gallery-image')->each(function (Crawler $node, $i) {
                return $node->attr("data-image-url");
            });
        }
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }

        $breadcrumbs = array_filter($crawler->filter('[aria-label="breadcrumbs"] ul li a')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 3);


        array_pop($product['breadcrumbs']);
        $parent = 0;
        $parentCategory = '';
//        print_r($breadcrumbs);
//        print_r($product['breadcrumbs']);
//        exit();
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] = $product['brand'];
        $product['tags'] = [];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

        $opt = $crawler->filter('#sizeSelect option')->each(function (Crawler $node) {
            return $node->attr("value");
        });

        if (sizeof(array_filter($opt)) != 0) {
            $product['type'] = 'variable';
            $product['variations'][] = [
                'name' => 'Size',
                'options' => array_filter($opt),
            ];
        } else {
            $product['type'] = 'simple';
        }

        if ($crawler->filter(".product-detail-block-attributes__table table tr")->count()) {
            $count = $crawler->filter(".product-detail-block-attributes__table table tr")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('th')->text(),
                    'options' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('td')->text(),
                ];
            }
        }

        $product['description'] = $crawler->filter('#collapseOne p')->html();
        $product['sku'] = explode("-", $images[0])[1];
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}
