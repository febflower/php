<?php

namespace app\command\MSK\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ColoradoskishopCOM extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:coloradoskishopcom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.coloradoskishop.com/Default.asp');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = [
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=1",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=2",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=3",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=4",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=5",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=6",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=7",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=8",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=9",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=10",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=11",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=12",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=13",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=14",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=15",
            "https://www.coloradoskishop.com/skis-s/1.htm?searching=Y&page=16",

            ];
        $array2 = [
            "https://www.coloradoskishop.com/Ski-Bindings-s/4.htm?searching=Y&page=1",
            "https://www.coloradoskishop.com/Ski-Bindings-s/4.htm?searching=Y&page=2",
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=1",
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=2",
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=3",
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=4",
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=5",
        ];
        $array3 = [
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=6",
            "https://www.coloradoskishop.com/Ski-Boots-s/2.htm?searching=Y&page=7",
            "https://www.coloradoskishop.com/Ski-Packages-s/332.htm?searching=Y&page=1",
            "https://www.coloradoskishop.com/Ski-Packages-s/332.htm?searching=Y&page=2",
            "https://www.coloradoskishop.com/Ski-Packages-s/332.htm?searching=Y&page=3",
            'https://www.coloradoskishop.com/category-s/126.htm',
        ];
        $array4=[
            "https://www.coloradoskishop.com/snowboards-s/7.htm?searching=Y&page=1",
            "https://www.coloradoskishop.com/snowboards-s/7.htm?searching=Y&page=2",
            "https://www.coloradoskishop.com/snowboards-s/7.htm?searching=Y&page=3",
            "https://www.coloradoskishop.com/snowboards-s/7.htm?searching=Y&page=4",

        ];
        $array5 = [
            "https://www.coloradoskishop.com/Snowboard-bindings-s/23.htm?searching=Y&page=1",
            "https://www.coloradoskishop.com/Snowboard-bindings-s/23.htm?searching=Y&page=2",
            'https://www.coloradoskishop.com/Snowboard-boots-s/8.htm',
        ];
        $this->sites = [
            'b1' => ['source' => $array1, 'offset' => 1, 'target' => 'bioestia001.xms005.site'],
            'b2' => ['source' => $array2, 'offset' => 1, 'target' => 'bioestia001.xms005.site'],
            'b3' => ['source' => $array3, 'offset' => 1, 'target' => 'bioestia001.xms005.site'],
            'b4' => ['source' => $array4, 'offset' => 1, 'target' => 'bioestia001.xms005.site'],
            'b5' => ['source' => $array5, 'offset' => 1, 'target' => 'bioestia001.xms005.site'],


        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.coloradoskishop.com/Default.asp',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
//        echo '87';
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($uri);
        }
//        echo "102";

    }
//https://www.coloradoskishop.com/category-s/565.htm
//https://www.coloradoskishop.com/category-s/565.htm?searching=Y&page=2
//https://www.coloradoskishop.com/category-s/565.htm?searching=Y&page=3

    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = array_filter($crawler->filter('.matching_results_text')->previousAll()->filter('a')->each(function (Crawler $node) {
            return $node->text();
        }));

        $crawler->filter('.v-product__img')->each(function (Crawler $node, $i) use ($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                    'title' => $node->filter('.v-product__img')->attr('title'),
                    'breadcrumbs' => $breadcrumbs,

                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
//        echo '123';

    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $item['title'];
//        echo '134';
        $product['price'] = $crawler->filter("[itemprop='price']")->attr("content");
//        $product['brand'] = $crawler->filter('.img')->attr('src');
        $product['brand'] = ' ';
        $product['type'] = 'simple';

//        http://cdn3.volusion.com/czncq.rpwlt/v/vspfiles/photos/DALBPANT120-3.jpg
//        https://cdn3.volusion.com/czncq.rpwlt/v/vspfiles/photos/DALBPANT120-3T.jpg
//        https://cdn3.volusion.com/czncq.rpwlt/v/vspfiles/photos/DALBPANT120-2T.jpg
//        http://cdn3.volusion.com/czncq.rpwlt/v/vspfiles/photos/DALBPANT120-2.jpg
        if ($crawler->filter('#altviews a')->count()) {
            $images = array_unique(array_filter($crawler->filter('#altviews a')->each(function (Crawler $node, $i) {
                return 'https:' . $node->attr("href");
            })));
        } else {
            $images = array_unique(array_filter($crawler->filter('#product_photo_zoom_url')->each(function (Crawler $node, $i) {
                return 'https:' . $node->attr("href");
            })));
        }

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
//        $product['images'] = [];
//        print_r($product['images']);
//        $product['images'] = $this->toEncryptImage($product['images']);
//        *[@id="v65-product-parent"]/tbody/tr[1]/td/b/a[1]
//        $breadcrumbs = array_filter($crawler->filter('.vCSS_breadcrumb_td a')->each(function (Crawler $node) {
//            return $node->text();
//        }));
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'], 1);
//        print_r($product['breadcrumbs']);
//        exit();

        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

//echo '171';
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['tags'] = [];

        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] =$product['brand'];
//        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];
        $product['attributes'] = [];

//        var_dump($product);


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }


//        if($crawler->filter("#ProductDetail_TechSpecs_div dt")->count()) {
//            $count = $crawler->filter("#ProductDetail_TechSpecs_div dt")->count();
//            for($i=0;$i<$count;$i++){
//                $product['attributes'][] = [
//                    'name' => $crawler->filter("#ProductDetail_TechSpecs_div dt")->eq($i)->filter('span')->text(),
//                    'options' => $crawler->filter("#ProductDetail_TechSpecs_div dd")->eq($i)->filter('span')->text(),
//                ];
//            }
//        }
//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
//        $product['short_description']=$crawler->filter('.product-detail-product-features__list')->html();
        $product['description'] = $crawler->filter('#product_description')->html();

//        $product['short_description'] = $crawler->filter('.product-description p')->text();
        $product['sku'] = $crawler->filter('.product_code')->text();
//
//        print_r($product);


//        print_r($product);
//        exit();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
//        var_dump($product);
//        echo '230';
    }

    public function toEncryptImage($images, $original = 'original')
    {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
