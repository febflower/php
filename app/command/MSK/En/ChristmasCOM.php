<?php

namespace app\command\MSK\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ChristmasCOM extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:charistmascom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://christmas.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = [
            "https://christmas.com/amp/christmas/ornaments/beach-tropical-nautical/",
            "https://christmas.com/amp/christmas/ornaments/christmas-ball-ornaments/",
            "https://christmas.com/amp/christmas/ornaments/collectibles/",
            "https://christmas.com/amp/christmas/ornaments/disc-ornaments/",
            "https://christmas.com/amp/christmas/ornaments/entertainment/",
            "https://christmas.com/amp/christmas/ornaments/family-friends/",
            "https://christmas.com/amp/christmas/ornaments/fantasy-sci-fi/",
            "https://christmas.com/amp/christmas/ornaments/fashion-beauty/",];
        $array2 = [
            "https://christmas.com/amp/christmas/ornaments/figurines/",
            "https://christmas.com/amp/christmas/ornaments/finials-pendants/",
            "https://christmas.com/amp/christmas/ornaments/food-beverage/",
            "https://christmas.com/amp/christmas/ornaments/garden-floral/",
            "https://christmas.com/amp/christmas/ornaments/gay-pride/",
            "https://christmas.com/amp/christmas/ornaments/gingerbread/",
            "https://christmas.com/amp/christmas/ornaments/hobbies/",
            "https://christmas.com/amp/christmas/ornaments/love-wedding-anniversary/",
            "https://christmas.com/amp/christmas/ornaments/military/",];
        $array3 = [
            "https://christmas.com/amp/christmas/ornaments/miniatures/",
            "https://christmas.com/amp/christmas/ornaments/nativity-religious/",
            "https://christmas.com/amp/christmas/ornaments/new-baby/",
            "https://christmas.com/amp/christmas/ornaments/novelty/",
            "https://christmas.com/amp/christmas/ornaments/occupations/",
            "https://christmas.com/amp/christmas/ornaments/ornament-displays-stands/",
            "https://christmas.com/amp/christmas/ornaments/ornament-hooks/",
            "https://christmas.com/amp/christmas/ornaments/ornament-storage/",];
        $array4 = [
            "https://christmas.com/amp/christmas/ornaments/personalized/",
            "https://christmas.com/amp/christmas/ornaments/photo-frames/",
            "https://christmas.com/amp/christmas/ornaments/snowflakes-icicles/",
            "https://christmas.com/amp/christmas/ornaments/special-causes/",
            "https://christmas.com/amp/christmas/ornaments/sports-activities/",
            "https://christmas.com/amp/christmas/ornaments/symbols-traditions/",
            "https://christmas.com/amp/christmas/ornaments/transportation/",
            "https://christmas.com/amp/christmas/ornaments/travel-destinations/",

        ];
        $array5 = [
            "https://christmas.com/amp/christmas/tree-accessories/christmas-tree-skirts-collars/",
            "https://christmas.com/amp/christmas/tree-accessories/christmas-tree-stands/",
            "https://christmas.com/amp/christmas/tree-accessories/christmas-tree-storage/",
            "https://christmas.com/amp/christmas/tree-accessories/christmas-tree-toppers/",
        ];
        $array6 = [
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/artificial-garland/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/artificial-wreaths/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/centerpieces-arrangements/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/christmas-swags/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/garland-storage-accessories/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/poinsettias/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/sprays-picks-branches/",
            "https://christmas.com/amp/christmas/wreaths-garland-greenery/wreath-storage-accessories/",
        ];
        $array7 = [
            "https://christmas.com/amp/christmas/decorations/accent-rugs/",
            "https://christmas.com/amp/christmas/decorations/advent-calendars/",
            "https://christmas.com/amp/christmas/decorations/animated-musical/",
            "https://christmas.com/amp/christmas/decorations/artificial-snow/",
            "https://christmas.com/amp/christmas/decorations/bells/",
            "https://christmas.com/amp/christmas/decorations/buildings/",
            "https://christmas.com/amp/christmas/decorations/candles-lanterns/",
            "https://christmas.com/amp/christmas/decorations/christmas-table-top-trees/",
        ];
        $array8 = [
            "https://christmas.com/amp/christmas/decorations/christmas-village-sets-accessories/",
            "https://christmas.com/amp/christmas/decorations/commercial-decorations/",
            "https://christmas.com/amp/christmas/decorations/door-decorations/",
            "https://christmas.com/amp/christmas/decorations/figures/",
            "https://christmas.com/amp/christmas/decorations/nativity-sets-accessories/",
            "https://christmas.com/amp/christmas/decorations/ribbons-bows/",
            "https://christmas.com/amp/christmas/decorations/sleighs/",

        ];
        $array9 = [
            "https://christmas.com/amp/christmas/decorations/snow-globes/",
            "https://christmas.com/amp/christmas/decorations/stockings-holders/",
            "https://christmas.com/amp/christmas/decorations/table-top/",
            "https://christmas.com/amp/christmas/decorations/trains/",
            "https://christmas.com/amp/christmas/decorations/wall-decorations/",
            "https://christmas.com/amp/christmas/decorations/window-decorations/",
        ];
        $array_Costumes0 = [
            'https://christmas.com/amp/christmas/decorations/ribbons-bows/',
            'https://christmas.com/amp/christmas/ornaments/symbols-traditions/',
            "https://christmas.com/amp/christmas/costumes-accessories/costume-accessory-sets/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-apparel-bottoms/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-apparel-sets/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-apparel-tops/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-hair/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-handwear/",];
        $array_Costumes1 = [
            "https://christmas.com/amp/christmas/costumes-accessories/costume-headwear/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-jewelry/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-legwear/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-masks/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-props/",
            "https://christmas.com/amp/christmas/costumes-accessories/costume-shoes/",
            "https://christmas.com/amp/christmas/costumes-accessories/pet-costumes/"];
        $array_Trees0 = [
            "https://christmas.com/amp/christmas/artificial-christmas-trees/alpine-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/colorful-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/commercial-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/fiber-optic-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/flocked-christmas-trees/",
        ];
        $array_Trees1 = [
            "https://christmas.com/amp/christmas/artificial-christmas-trees/pop-up-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/potted-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/pre-lit-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/snowing-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/spiral-trees/",
        ];
        $array_Trees2 = [
            "https://christmas.com/amp/christmas/artificial-christmas-trees/topiaries/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/twig-birch-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/unlit-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/upside-down-christmas-trees/",
            "https://christmas.com/amp/christmas/artificial-christmas-trees/wall-trees/",
        ];
        $array_Lights0 = [
            "https://christmas.com/amp/christmas/lights/battery-operated/",
            "https://christmas.com/amp/christmas/lights/candle-lamps/",
            "https://christmas.com/amp/christmas/lights/christmas-string-lights/",
            "https://christmas.com/amp/christmas/lights/commercial-lights/",
            "https://christmas.com/amp/christmas/lights/fairy-lights/",
            "https://christmas.com/amp/christmas/lights/globe-lights/",
        ];
        $array_Lights1= [
            "https://christmas.com/amp/christmas/lights/icicle-lights/",
            "https://christmas.com/amp/christmas/lights/led/",
            "https://christmas.com/amp/christmas/lights/light-accessories/",
            "https://christmas.com/amp/christmas/lights/light-storage/",
            "https://christmas.com/amp/christmas/lights/net-lights/",
            "https://christmas.com/amp/christmas/lights/night-lights/",
];
        $array_Lights2= [
            "https://christmas.com/amp/christmas/lights/novelty-lights/",
            "https://christmas.com/amp/christmas/lights/pathway-yard-lights/",
            "https://christmas.com/amp/christmas/lights/projector-lights/",
            "https://christmas.com/amp/christmas/lights/replacement-bulbs/",
            "https://christmas.com/amp/christmas/lights/retro-lights/",
            "https://christmas.com/amp/christmas/lights/rope-tape-lights/",
];


        $this->sites = [
            'x1' => ['source' => $array1, 'offset' => 1, 'target' => 'christma002.xms005.site'],
            'x2' => ['source' => $array2, 'offset' => 1, 'target' => 'christma002.xms005.site'],
            'x3' => ['source' => $array3, 'offset' => 1, 'target' => 'christma002.xms005.site'],
            'x4' => ['source' => $array4, 'offset' => 1, 'target' => 'christma002.xms005.site'],
            'y1' => ['source' => $array5, 'offset' => 1, 'target' => 'christma003.xms005.site'],
            'y2' => ['source' => $array6, 'offset' => 1, 'target' => 'christma003.xms005.site'],
            'd1' => ['source' => $array7, 'offset' => 1, 'target' => 'christma001.xms005.site'],
            'd2' => ['source' => $array8, 'offset' => 1, 'target' => 'christma001.xms005.site'],
            'd3' => ['source' => $array9, 'offset' => 1, 'target' => 'christma001.xms005.site'],

            'c1'=>['source' => $array_Costumes0, 'offset' => 1, 'target' => 'christma004.xms005.site'],
            'c2'=>['source' => $array_Costumes1, 'offset' => 1, 'target' => 'christma004.xms005.site'],
            't1'=>['source' => $array_Trees0, 'offset' => 1, 'target' => 'christma005.xms005.site'],
            't2'=>['source' => $array_Trees1, 'offset' => 1, 'target' => 'christma005.xms005.site'],
            't3'=>['source' => $array_Trees2, 'offset' => 1, 'target' => 'christma005.xms005.site'],
            'L1'=>['source' => $array_Lights0, 'offset' => 1, 'target' => 'christma006.xms005.site'],
            'L2'=>['source' => $array_Lights1, 'offset' => 1, 'target' => 'christma006.xms005.site'],
            'L3'=>['source' => $array_Lights2, 'offset' => 1, 'target' => 'christma006.xms005.site'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://christmas.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;

        $this->output->writeln($url);

        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);


        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {

            $this->processPage(str_replace("https://christmas.com/", "https://christmas.com/amp/", $nextNode->attr("href")));
        }
    }

    protected function processProductList(Crawler $crawler)
    {

        $crawler->filter('.card-figure a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->attr("data-vars-product-name"),
                    'url' => sprintf('%s', $node->attr('data-vars-product-link')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $product['title'] = $crawler->filter('.productView-product .productView-title')->text();
        $product['price'] = $crawler->filter('.productView')->attr('data-product-price');
        $product['brand'] = $crawler->filter('[itemprop="brand"]')->text();
        $product['type'] = 'simple';

        if ($crawler->filter('.productView-thumbnail-link')->count()) {
            $images = array_unique(array_filter($crawler->filter('.productView-thumbnail-link')->each(function (Crawler $node, $i) {
                return $node->attr("data-image-gallery-zoom-image-url");
            })));
        } else {
            $images = array_unique(array_filter($crawler->filter('.productView-img-container a')->each(function (Crawler $node, $i) {
                return $node->attr("href");
            })));
        }

        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs .breadcrumb .breadcrumb-label span')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 2);

        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
        $product['tags'][] = $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }
        if ($crawler->filter(".product-detail-block-attributes__table table tr")->count()) {
            $count = $crawler->filter(".product-detail-block-attributes__table table tr")->count();
//            for($i=0;$i<$count;$i++){
//                $product['attributes'][] = [
//                    'name' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('th')->text(),
//                    'options' => $crawler->filter(".product-detail-block-attributes__table table tr")->eq($i)->filter('td')->text(),
//                ];
//            }
            $product['attributes'] = [];
        }
//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
        $product['description'] = $crawler->filter('.product-description')->html();
//        print_r($product['description']);exit;
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->attr('content');
//        print_r($product);exit;

//        print_r($product);
//        exit();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }


}
