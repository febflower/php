<?php

namespace app\command\MSK\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class AppRequipperCOM extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:apprequippercom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://requippe.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array1 = ['https://app.requipper.com/s?pub_categories=snow&pub_snowSubcategories=skis'];
        $array2 = ['https://app.requipper.com/s?pub_categories=climb&pub_climbSubcategories=climbing_shoes'];
//        $array3 = ['']
        $this->sites = [
            'x' => ['source' => $array1, 'offset' => 1, 'target' => 'msk'],
//            'x' => ['source' => $array, 'offset' => 1, 'target' => 'msk'],
        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://app.requipper.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_713d71b577cf93a67a38fc1782d548f75e4da922',
            'cs_cea572c1f296a0e0d336d089e04d13b775c83c92',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[title="Next page"]');
        if ($nextNode->count()) {
            $this->processPage('https://app.requipper.com' . $nextNode->attr('href'));
        }

    }

//https://app.requipper.com/s?page=2&pub_categories=snow&pub_snowSubcategories=skis
    protected function processProductList(Crawler $crawler)
    {

//        $brand = explode('"productBrand": ',$crawler->filter('[type="text/javascript"]')->eq(4)->text());
//        $brands = explode("',",$brand[1]);
//        $product['brand']=str_replace("'",'',$brands[0]);
        $data['breadcrumb'] = $crawler->filter('.SearchPage_secondLevelCategoryTreeLink__bhh96.SearchPage_link__On0o_')->text();
        $crawler->filter('.ListingCard_root__2lBIo')->each(function (Crawler $node, $i) use (& $data) {
//            print_r($node->filter('.ListingCard_title__3y9wD')->text()."\n");
//            print_r(sprintf('%s','https://app.requipper.com'.$node->filter('a')->attr('href'))."\n");
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('.ListingCard_title__3y9wD')->text(),
                    'url' => sprintf('%s', 'https://app.requipper.com' . $node->filter('a')->attr('href')),
                    'breadcrumbs' => $data,

                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
//        print_r($item);
        echo '123';
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        echo '127';
        $product['title'] = $crawler->filter('.ListingPage_modelTitle__aRRis')->text();
        $product['price'] = str_replace("$", "", $crawler->filter('.ListingPage_priceTitle__2-Hbn')->text());
        $product['brand'] = $crawler->filter('.ListingPage_brandTitle__2CMTl')->text();
//        $product['brand']=' ';
        $product['type'] = 'simple';
//        print_r($crawler->filter('.ListingPage_listingPageImage__23YUx')->each(function (Crawler $node, $i) {
////            echo  $node->outerHtml();
////            return $node->attr("srcset");
////        }));exit();
//////        $images = array_unique(array_filter();
////        print_r($images);
        $images = array_unique(array_filter($crawler->filter('.ListingPage_listingPageImage__23YUx')->each(function (Crawler $node, $i) {
            return explode(" ", $node->attr("srcset"))[0];
        })));
//        print_r($images);exit();
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $product['images'] = $this->toEncryptImage($product['images']);
//        $product['images'] =[];
//        $breadcrumbs = array_filter($crawler->filter('.vCSS_breadcrumb_td a')->each(function (Crawler $node) {
//            return $node->text();
//        }));
//        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        $product['breadcrumbs'] = $item['breadcrumbs'];

        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['tags'] = [];

        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] =$product['brand'];
//        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

//        var_dump($product);


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }

        if ($crawler->filter(".ListingPage_productSpecsBlock__37sdh div")->count()) {
            $count = $crawler->filter(".ListingPage_productSpecsBlock__37sdh div")->count();
            for ($i = 0; $i < $count; $i++) {
                $product['attributes'][] = [
                    'name' => $crawler->filter(".ListingPage_productSpecsBlock__37sdh div h2 span")->eq($i)->text(),
                    'options' => $crawler->filter(".ListingPage_productSpecsBlock__37sdh div p")->eq($i)->text(),
                ];
            }
        }
//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
//        $product['short_description']=$crawler->filter('.product-detail-product-features__list')->html();
        $product['description'] = $crawler->filter('[name="description"]')->attr('content');
//        print_r($product['description']);

//        print_r($product);

//        $product['short_description'] = $crawler->filter('.product-description p')->text();
//        $product['sku'] =$crawler->filter('[itemprop="sku"]')->text();
//
//        print_r($product);
        if ($crawler->filter(".ListingPage_sectionDescription__1r_ac p")->eq(8)->count() > 0) {
            $product['sku'] = $crawler->filter(".ListingPage_sectionDescription__1r_ac p")->eq(8)->text();
        } else {
            $product['sku'] = date('Y-m-d-H:i:s');
        }
        print_r($product['sku']);

        $this->createProduct($product);
        echo "\r\n";
//        echo "1";
    }

    public function toEncryptImage($images, $original = 'original')
    {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
