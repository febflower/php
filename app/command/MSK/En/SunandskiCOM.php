<?php

namespace app\command\MSK\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class SunandskiCOM extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:sunandskicom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.sunandski.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array = ['https://www.sunandski.com/c/ski-snowboard-deals'];
        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'msk'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sunandski.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_713d71b577cf93a67a38fc1782d548f75e4da922',
            'cs_cea572c1f296a0e0d336d089e04d13b775c83c92',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }


    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
//            https://www.sunandski.com/c/ski-snowboard-deals
//        https://www.sunandski.com/c/ski-snowboard-deals?startIndex=24
        if ($nextNode->count()) {
            $this->processPage('https://www.sunandski.com/c/ski-snowboard-deals' . $nextNode->attr('href'));
        }
//        echo '101';

    }

    protected function processProductList(Crawler $crawler)
    {
//        https://www.sunandski.com/p/34200063840002210/salomon-mens-stance-80-skis-with-m11-gripwalk-bindings-22
//        /p/34200063840002210/salomon-mens-stance-80-skis-with-m11-gripwalk-bindings-22
        $crawler->filter('.product__name')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', 'https://www.sunandski.com' . $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.product__name--lrg')->text();
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr("content");
//        $product['brand'] = $crawler->filter('.img')->attr('src');
        $product['brand'] = ' ';
        $product['type'] = 'simple';
        if ($crawler->filter('.slider__slide .product__image')->count()) {
            $images = array_unique(array_filter($crawler->filter('.slider__slide .product__image')->each(function (Crawler $node, $i) {
                return 'https:' . $node->attr("href");
            })));
        } else {
            $images = array_unique(array_filter($crawler->filter('.product__image')->each(function (Crawler $node, $i) {
                return 'https:' . $node->attr("src");
            })));

        }
//        echo '135';
        $x = 1;
//        https://cdn-tp2.mozu.com/11961-16493/cms/16493/files/eb4f2f9c-db71-4a8d-8a86-600d95b61b23
//        https://cdn-tp2.mozu.com/11961-16493/cms/16493/files/5c12da33-96e5-4760-bacc-76116ee2f578
//        https://cdn-tp2.mozu.com/11961-16493/cms/16493/files/5c12da33-96e5-4760-bacc-76116ee2f578
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $product['images'] = $this->toEncryptImage($product['images']);
//        $product['images'] =[];
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li a span')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 2, 3);
//echo '152';
        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['tags'] = [];

        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] =$product['brand'];
//        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];
//        echo '173';

//        var_dump($product);


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }

//        if($crawler->filter(".data-sheet .name")->count()) {
//            $count = $crawler->filter(".data-sheet .name")->count();
//            for($i=0;$i<$count;$i++){
//                $product['attributes'][] = [
//                    'name' => $crawler->filter(".data-sheet .name")->eq($i)->text(),
//                    'options' => $crawler->filter(".data-sheet .value")->eq($i)->text(),
//                ];
//            }
//        }
//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
//        $product['short_description']=$crawler->filter('.product-detail-product-features__list')->html();
        $product['description'] = $crawler->filter('.product-details .container')->html();
//echo '214';
//        $product['short_description'] = $crawler->filter('.product-description p')->text();
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text();
//
//        print_r($product);
//        echo '220';


//        print_r($product);
//        exit();
        $this->createProduct($product);
        echo "\r\n";
//        echo "1";
//        var_dump($product);
//        echo '230';
    }

    public function toEncryptImage($images, $original = 'original')
    {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }

        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}