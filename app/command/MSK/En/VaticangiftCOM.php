<?php

namespace app\command\MSK\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class VaticangiftCOM extends Command
{


    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:msk:vaticangiftcom')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://www.vaticangift.com/');
    }

    protected function initialize(Input $input, Output $output)
    {
        $array = [
            'https://www.vaticangift.com/12-precious-rosaries',
            'https://www.vaticangift.com/23-sterling-silver-medals',
            'https://www.vaticangift.com/26-pope-francis',
            'https://www.vaticangift.com/41-holy-statues',
            'https://www.vaticangift.com/38-christmas-gifts',
            'https://www.vaticangift.com/39-baptism-gifts',
            'https://www.vaticangift.com/40-holy-communion',
            'https://www.vaticangift.com/42-confirmation-gifts',
            'https://www.vaticangift.com/33-silver-icons',
            'https://www.vaticangift.com/34-byzantine-icons',
            'https://www.vaticangift.com/32-via-crucis',
            'https://www.vaticangift.com/37-sterling-silver-chains',
            'https://www.vaticangift.com/24-gold-plated-medals',
            'https://www.vaticangift.com/23-sterling-silver-medals'
            ];
        $this->sites = [
            'x' => ['source' => $array, 'offset' => 1, 'target' => 'vaticang001.xms005.site'],
        ];
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.bioestia.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',

            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $this->processProductList($crawler);
        $nextNode = $crawler->filter('[rel="next"]');
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {

        $crawler->filter('.product_name a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        $product['title'] = $crawler->filter('.h1')->text();
        $product['price'] = $crawler->filter('.current-price span')->attr("content");
        $product['brand'] = ' ';
        $product['type'] = 'simple';

        $images = array_unique(array_filter($crawler->filter('.js-thumb')->each(function (Crawler $node, $i) {
            return $node->attr("data-image-large-src");
        })));
        $x = 1;
        foreach ($images as $image) {
            if ($x > 6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb ol li a span')->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs, 1, -1);


        $parent = 0;
        $parentCategory = '';
        $offset = $this->processSite['offset'];
        $offset = isset($product['breadcrumbs'][$offset]) ? $offset : $offset - 1;
        foreach (array_slice($product['breadcrumbs'], $offset) as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color'] = '';
        $product['tags'] = [];

        $product['subCategory'] = ' ';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
//        $product['tags'][]=$product['color'];
//        $product['tags'][] =$product['brand'];
//        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        $product['variations'] = [];

//        var_dump($product);


//        $dels=['color.name = ',"'",'[',']'];


//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(0)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(0)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                    return $node->text();
//                })
//            ];
//            $product['type'] = 'variable';
//        }
//
//        if ($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->count()){
//            $product['variations'][]= [
//                'name'=>substr($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),'0',strrpos($crawler->filter('.product-detail-variant-selection__headline')->eq(1)->text(),":")),
//                'options' =>$crawler->filter('.product-detail-variant-selection__list-wrapper')->eq(1)->filter('ul li span')->filter('.js-variant-value')->each(function (Crawler $node, $i) {
//                return $node->text();
//            })
//            ];
//            $product['type'] = 'variable';
//        }

//        if($crawler->filter(".data-sheet .name")->count()) {
//            $count = $crawler->filter(".data-sheet .name")->count();
//            for($i=0;$i<$count;$i++){
//                $product['attributes'][] = [
//                    'name' => $crawler->filter(".data-sheet .name")->eq($i)->text(),
//                    'options' => $crawler->filter(".data-sheet .value")->eq($i)->text(),
//                ];
//            }
//        }
        $product['attributes'] = [];

//        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
//        $product['description'] = str_replace($del,'',$crawler->filter('.product-detail-block-description__collapse')->html());
//        $product['short_description']=$crawler->filter('.product-detail-product-features__list')->html();
        $product['description'] = $crawler->filter('[itemprop="description"]')->html();

//        $product['short_description'] = $crawler->filter('.product-description p')->text();
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text();
//
//        print_r($product);


//        print_r($product);
//        exit();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
//        var_dump($product);
//        echo '230';
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender']) && $product['gender'] != '' ? strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }


}
