<?php            //https://www.campz.nl/
namespace app\command\LPW\Nl;

//调用模块
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use Exception;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class CampzNl extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    //自定义命令行  build:wordpress:lpw:-------------
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:campznl')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://www.campz.nl');
    }
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接   //?page=2&sz=48
        $array1=['https://www.campz.nl/kleding/jassen/'];//14
        $array2=['https://www.campz.nl/kleding/jassen/?page=15&sz=48'];//p29
        $array3=['https://www.campz.nl/kleding/jassen/?page=30&sz=48'];//p44
        $array4=['https://www.campz.nl/kleding/jassen/?page=45&sz=48'];//p58
        $array5=['https://www.campz.nl/kleding/jassen/?page=59&sz=48'];//p72
        $array6=['https://www.campz.nl/kleding/jassen/?page=73&sz=48'];//p86
        $array7=['https://www.campz.nl/kleding/jassen/?page=87&sz=48'];//p100
        $array8=['https://www.campz.nl/kleding/jassen/?page=101&sz=48'];//p114
        $array9=['https://www.campz.nl/kleding/jassen/?page=115&sz=48'];
        $array10=['https://www.campz.nl/kleding/accessoires/'];//p14
        $array11=['https://www.campz.nl/kleding/accessoires/?page=15&sz=48'];//p29
        $array12=['https://www.campz.nl/kleding/accessoires/?page=30&sz=48'];//p44
        $array13=['https://www.campz.nl/kleding/accessoires/?page=45&sz=48'];//p59
        $array14=['https://www.campz.nl/kleding/accessoires/?page=60&sz=48'];//p74
        $array15=['https://www.campz.nl/kleding/accessoires/?page=75&sz=48'];//p89
        $array16=['https://www.campz.nl/kleding/accessoires/?page=90&sz=48'];
        $array17=['https://www.campz.nl/kleding/jurken-rokken/','https://www.campz.nl/kleding/jumpsuits/'];
        $array18=['https://www.campz.nl/kleding/broeken/'];//p15
        $array19=['https://www.campz.nl/kleding/broeken/?page=16&sz=48'];//p31
        $array20=['https://www.campz.nl/kleding/broeken/?page=32&sz=48'];//p46
        $array21=['https://www.campz.nl/kleding/broeken/?page=47&sz=48'];//p61
        $array22=['https://www.campz.nl/kleding/broeken/?page=62&sz=48'];
        $array23=['https://www.campz.nl/kleding/functioneel-ondergoed/'];//p17
        $array24=['https://www.campz.nl/kleding/functioneel-ondergoed/?page=18&sz=48'];
        $array25=['https://www.campz.nl/kleding/pullovers-midlayers/'];//p11
        $array26=['https://www.campz.nl/kleding/pullovers-midlayers/?page=11&sz=48'];
        $array27=['https://www.campz.nl/kleding/kousen-sokken/'];//p16
        $array28=['https://www.campz.nl/kleding/kousen-sokken/?page=16&sz=48'];
        $array29=['https://www.campz.nl/kleding/shirts-overhemden/'];//p14
        $array30=['https://www.campz.nl/kleding/shirts-overhemden/?page=14&sz=48'];//p27
        $array31=['https://www.campz.nl/kleding/shirts-overhemden/?page=28&sz=48'];//p41
        $array32=['https://www.campz.nl/kleding/shirts-overhemden/?page=42&sz=48'];//p55
        $array33=['https://www.campz.nl/kleding/shirts-overhemden/?page=56&sz=48'];
        $array34=['https://www.campz.nl/kleding/zwemkleding/'];//p14
        $array35=['https://www.campz.nl/kleding/zwemkleding/?page=15&sz=48'];//p29
        $array36=['https://www.campz.nl/kleding/zwemkleding/?page=30&sz=48'];
        $array37=['https://www.campz.nl/schoenen/winterschoenen--laarzen/'];
        $array38=['https://www.campz.nl/schoenen/casual-schoenen-en-laarzen/'];
        $array39=['https://www.campz.nl/schoenen/barefoot-schoenen-minimalistische-schoenen/','https://www.campz.nl/schoenen/trekking--wandelschoenen/'];//p12
        $array40=['https://www.campz.nl/schoenen/trekking--wandelschoenen/?page=12&sz=48'];//p23
        $array41=['https://www.campz.nl/schoenen/trekking--wandelschoenen/?page=24&sz=48'];
        $array42=['https://www.campz.nl/schoenen/sportschoenen/'];//p19
        $array43=['https://www.campz.nl/schoenen/accessoires-onderhoud/','https://www.campz.nl/schoenen/sportschoenen/?page=20&sz=48'];
        $array44=['https://www.campz.nl/uitrusting/rugzakken/'];//p13
        $array45=['https://www.campz.nl/uitrusting/rugzakken/?page=14&sz=48'];//p27
        $array46=['https://www.campz.nl/uitrusting/rugzakken/?page=28&sz=48'];//p41
        $array47=['https://www.campz.nl/uitrusting/rugzakken/?page=42&sz=48'];
        $array48=['https://www.campz.nl/uitrusting/lichaamsverzorging-bescherming/','https://www.campz.nl/uitrusting/elektronische-apparatuur-opladers/','https://www.campz.nl/uitrusting/verlichting/','https://www.campz.nl/uitrusting/gereedschap/','https://www.campz.nl/uitrusting/navigatie-horloges/'];
        $array49=['https://www.campz.nl/uitrusting/tassen/'];//p12
        $array50=['https://www.campz.nl/uitrusting/tassen/?page=13&sz=48'];
        $array51=['https://www.campz.nl/camping/tenten/'];//p15
        $array52=['https://www.campz.nl/camping/tenten/?page=15&sz=48'];
        $array53=['https://www.campz.nl/camping/campingkeukens/'];//p19
        $array54=['https://www.campz.nl/camping/campingkeukens/?page=20&sz=48'];
        $array55=['https://www.campz.nl/camping/slaapzakken/'];
        $array56=['https://www.campz.nl/camping/bidons/'];
        $array57=['https://www.campz.nl/camping/handkarren-transportkarren/','https://www.campz.nl/camping/matten/','https://www.campz.nl/camping/waterzuivering/','https://www.campz.nl/camping/outdoor-voeding/'];
        $array58=['https://www.campz.nl/camping/campingmeubels/'];
        $array59=['https://www.campz.nl/wintersport/'];//p15
        $array60=['https://www.campz.nl/wintersport/?page=16&sz=48'];//p31
        $array61=['https://www.campz.nl/wintersport/?page=32&sz=48'];//p46
        $array62=['https://www.campz.nl/wintersport/?page=47&sz=48'];//p61
        $array63=['https://www.campz.nl/wintersport/?page=62&sz=48'];
        $array64=['https://www.campz.nl/kleding/kinderen/'];//p19
        $array65=['https://www.campz.nl/kleding/kinderen/?page=20&sz=48'];//p39
        $array66=['https://www.campz.nl/kleding/kinderen/?page=40&sz=48'];//p59
        $array67=['https://www.campz.nl/kleding/kinderen/?page=60&sz=48'];
        $array68=['https://www.campz.nl/schoenen/kinderen/'];
        $array69=['https://www.campz.nl/kleding/accessoires/kinderen/','https://www.campz.nl/uitrusting/kinderen/'];
        
        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'campznl001.xms007.site'],
            'x2'=>['source'=>$array2,'target'=>'campznl002.xms008.site'],
            'x3'=>['source'=>$array3,'target'=>'campznl003.xms007.site'],
            'x4'=>['source'=>$array4,'target'=>'campznl004.xms008.site'],
            'x5'=>['source'=>$array5,'target'=>'campznl005.xms007.site'],
            'x6'=>['source'=>$array6,'target'=>'campznl006.xms008.site'],
            'x7'=>['source'=>$array7,'target'=>'campznl007.xms007.site'],
            'x8'=>['source'=>$array8,'target'=>'campznl008.xms008.site'],
            'x9'=>['source'=>$array9,'target'=>'campznl009.xms007.site'],
            'x10'=>['source'=>$array10,'target'=>'campznl010.xms008.site'],
            'x11'=>['source'=>$array11,'target'=>'campznl011.xms007.site'],
            'x12'=>['source'=>$array12,'target'=>'campznl012.xms008.site'],
            'x13'=>['source'=>$array13,'target'=>'campznl013.xms007.site'],
            'x14'=>['source'=>$array14,'target'=>'campznl014.xms008.site'],
            'x15'=>['source'=>$array15,'target'=>'campznl015.xms007.site'],
            'x16'=>['source'=>$array16,'target'=>'campznl016.xms008.site'],
            'x17'=>['source'=>$array17,'target'=>'campznl017.xms007.site'],
            'x18'=>['source'=>$array18,'target'=>'campznl018.xms008.site'],
            'x19'=>['source'=>$array19,'target'=>'campznl019.xms007.site'],
            'x20'=>['source'=>$array20,'target'=>'campznl020.xms008.site'],
            'x21'=>['source'=>$array21,'target'=>'campznl021.xms007.site'],
            'x22'=>['source'=>$array22,'target'=>'campznl022.xms008.site'],
            'x23'=>['source'=>$array23,'target'=>'campznl023.xms007.site'],
            'x24'=>['source'=>$array24,'target'=>'campznl024.xms008.site'],
            'x25'=>['source'=>$array25,'target'=>'campznl025.xms007.site'],
            'x26'=>['source'=>$array26,'target'=>'campznl026.xms008.site'],
            'x27'=>['source'=>$array27,'target'=>'campznl027.xms007.site'],
            'x28'=>['source'=>$array28,'target'=>'campznl028.xms008.site'],
            'x29'=>['source'=>$array29,'target'=>'campznl029.xms007.site'],
            'x30'=>['source'=>$array30,'target'=>'campznl030.xms008.site'],
            'x31'=>['source'=>$array31,'target'=>'campznl031.xms007.site'],
            'x32'=>['source'=>$array32,'target'=>'campznl032.xms008.site'],
            'x33'=>['source'=>$array33,'target'=>'campznl033.xms008.site'],
            'x34'=>['source'=>$array34,'target'=>'campznl034.xms008.site'],
            'x35'=>['source'=>$array35,'target'=>'campznl035.xms008.site'],
            'x36'=>['source'=>$array36,'target'=>'campznl036.xms008.site'],
            'x37'=>['source'=>$array37,'target'=>'campznl037.xms008.site'],
            'x38'=>['source'=>$array38,'target'=>'campznl038.xms008.site'],
            'x39'=>['source'=>$array39,'target'=>'campznl039.xms008.site'],
            'x40'=>['source'=>$array40,'target'=>'campznl040.xms008.site'],
            'x41'=>['source'=>$array41,'target'=>'campznl041.xms008.site'],
            'x42'=>['source'=>$array42,'target'=>'campznl042.xms008.site'],
            'x43'=>['source'=>$array43,'target'=>'campznl043.xms008.site'],
            'x44'=>['source'=>$array44,'target'=>'campznl044.xms008.site'],
            'x45'=>['source'=>$array45,'target'=>'campznl045.xms008.site'],
            'x46'=>['source'=>$array46,'target'=>'campznl046.xms008.site'],
            'x47'=>['source'=>$array47,'target'=>'campznl047.xms008.site'],
            'x48'=>['source'=>$array48,'target'=>'campznl048.xms008.site'],
            'x49'=>['source'=>$array49,'target'=>'campznl049.xms009.site'],
            'x50'=>['source'=>$array50,'target'=>'campznl050.xms010.site'],
            'x51'=>['source'=>$array51,'target'=>'campznl051.xms009.site'],
            'x52'=>['source'=>$array52,'target'=>'campznl052.xms010.site'],
            'x53'=>['source'=>$array53,'target'=>'campznl053.xms009.site'],
            'x54'=>['source'=>$array54,'target'=>'campznl054.xms010.site'],
            'x55'=>['source'=>$array55,'target'=>'campznl055.xms009.site'],
            'x56'=>['source'=>$array56,'target'=>'campznl056.xms010.site'],
            'x57'=>['source'=>$array57,'target'=>'campznl057.xms009.site'],
            'x58'=>['source'=>$array58,'target'=>'campznl058.xms010.site'],
            'x59'=>['source'=>$array59,'target'=>'campznl059.xms009.site'],
            'x60'=>['source'=>$array60,'target'=>'campznl060.xms010.site'],
            'x61'=>['source'=>$array61,'target'=>'campznl061.xms009.site'],
            'x62'=>['source'=>$array62,'target'=>'campznl062.xms010.site'],
            'x63'=>['source'=>$array63,'target'=>'campznl063.xms009.site'],
            'x64'=>['source'=>$array64,'target'=>'campznl064.xms010.site'],
            'x65'=>['source'=>$array65,'target'=>'campznl065.xms009.site'],
            'x66'=>['source'=>$array66,'target'=>'campznl066.xms010.site'],
            'x67'=>['source'=>$array67,'target'=>'campznl067.xms009.site'],
            'x68'=>['source'=>$array68,'target'=>'campznl068.xms010.site'],
            'x69'=>['source'=>$array69,'target'=>'campznl069.xms009.site'],
        ];
        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',    #用了VPN可以屏蔽此段
            'base_uri'=>'https://www.campz.nl/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);
        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }
    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('.first-last')->filter('.page-next');
        // echo $nextNode->attr("href");exit;
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));                    
        }
    }
    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.product-tile-inner')->filter('.is-relative>a')->each(function(Crawler $node,$i)use(&$breadcrumbs){
                try{
                    $this->discount = rand(65,80)/100;  //rand(65,80)/100;
                    // $request = json_encode($node, JSON_UNESCAPED_UNICODE);
                    $this->crawlerProduct([
                        'name'=> "[{$i}]".$node->text(),
                        'url'=> sprintf('%s',"https://www.campz.nl".$node->attr('href')),
                    ]);
                }catch(\Exception $exception){
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
                }   
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url'] = 'https://www.campz.nl/trollkids-mandal-jacket-kids-M998997.html?vgid=G1461645';
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $json = [];
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$json){
            if(strstr($node->text(),"sku")){
                $json[] = $node->text();
            }
        });
        // print_r($json);exit;
        //其他---------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //关键词
        $product['keywords'] = [];
        //性别
        $product['gender'] = '';
        //颜色
        $product['color'] = "";
        // print_r($product['color']);exit;
        //亚类
        $product['subCategory'] = ' ';

        //SKU
        $product['sku'] = $crawler->filter('.selectable')->first()->attr('data-vgid');
        // print_r($product['sku']);exit;
        // 标题
        $product['title'] = json_decode($json[0],true)['name'];
        // print_r($product['title']);exit;
        //价格
        $product['price'] = json_decode($json[0],true)['offers']['price'];
        // print_r(var_dump($product['price']));exit;
        // 品牌
        $product['brand'] = json_decode($json[0],true)['brand']['name'];
        // print_r($product['brand']);exit;

        //ID
        $product['id'] = '';
        // print_r($product['id']);exit;
        //
        $product['type'] = 'simple';
        //图片-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $images = json_decode($json[0],true)['image'];
        $images = explode(",",$images[0]);
        // print_r($images);exit;
        foreach ($images as $image) {
            $product['images'][] = [
                'name' => $product['title'],
                'src' => $image,
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        //分类/子分类-------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $breadcrumbs = [];
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$breadcrumbs){
            if(strstr($node->text(),"itemListElement")){
                $breadcrumbs[] = $node->text();
            }
        });
        $num_breadcrumbs = count(json_decode($breadcrumbs[0],true)['itemListElement']);
        for($i=0;$i<$num_breadcrumbs;$i++){
            $breadcrumbs_s[] = json_decode($breadcrumbs[0],true)['itemListElement'][$i]['name'];
        }
        $breadcrumbs = array_slice($breadcrumbs_s,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;

        //标签--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        array_push($tags,$product['brand']);
        // print_r($tags);exit;
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;

        //选项--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['variations'] = [];
        if($crawler->filter('.variation__option')->filter('.cyc-flex')->count()){
            $options = $crawler->filter('.variation__option')->each(function(Crawler $node,$i){
                return str_replace("|","/",str_replace("- uitverkocht","",$node->attr('data-variationvalue')));
            });
            $product['variations'][]=[
                'name'=>"Size",
                'options'=>$options,
            ];
            $product['type'] = 'variable';
        }
        // print_r($product['variations']);exit;
        //顶部菜单栏
        $product['short_description']="";
        // print_r($product['short_description']);exit;
        //产品描述-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['description'] = ""; 
        if($crawler->filter('.product-info-ctr')->count()){
            $des_head = $crawler->filter('.product-info-ctr h2')->html();
            $des_meer = $crawler->filter('.product-info-ctr .is-hidden')->html();
            $product['description'] = str_replace($des_meer,"",str_replace($des_head,"",$crawler->filter('.product-info-ctr')->html()));
            if($crawler->filter('.product-info-ctr img')->count()){
                $description_new = $crawler->filter('.product-info-ctr .product-info-content')->html();
                $product['description'] = str_replace($description_new,"",$crawler->filter('.product-info-ctr .product-info-content .js-productDescriptionContainer')->html());
            }
        }
        // print_r($product['description']);exit;
        // 产品规格---------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['attributes'] = [];
        if($crawler->filter('.product-equipment-ctr')->count()){
            $num_attributes = $crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->count();
            for($i=0;$i<$num_attributes;$i++){
                if($crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->eq($i)->filter('.pdp_featurelist_feature')->count()){
                    $num_attributes_names = $crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->eq($i)->filter('.pdp_featurelist_feature')->count();
                    for($o=0;$o<$num_attributes_names;$o++){
                        $attributes_name[] = $crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->eq($i)->filter('.pdp_featurelist_feature')->eq($o)->text();
                        $attributes_options[] = $crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->eq($i)->filter('.pdp_featurelist_value')->eq($o)->text();
                    }
                }else{
                    $attributes_name[] = $crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->eq($i)->filter('.pdp_featurelist_group')->text();
                    $attributes_options[] = $crawler->filter('.product-equipment-ctr .pdp_featurelist .pdp_featureitem')->eq($i)->filter('.pdp_featurelist_value')->text();
                }
            }
            foreach($attributes_name as $options=>$name){
                $product['attributes'][]=[
                    'name'=>$name,
                    'options'=>$attributes_options[$options],
                ];
            }
        }
        // print_r($product['attributes']);exit;
        // print_r($product);exit;

        // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "Nextone".PHP_EOL;
    }
    //图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   
