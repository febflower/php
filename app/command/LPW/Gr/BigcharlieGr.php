<?php         //https://www.bigcharlie.gr/
namespace app\command\LPW\Gr;

//调用模块
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use Exception;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class BigcharlieGr extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:-------------
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:bigcharliegr')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://www.bigcharlie.gr/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接   //?sortBy=Relevance&count=
        $array1=['https://www.bigcharlie.gr/paixnidia/ps/76/'];
        $array2=['https://www.bigcharlie.gr/%C2%A0sxolika/ps/84/'];
        $array3=['https://www.bigcharlie.gr/%C2%A0eidi-partu/ps/83/','https://www.bigcharlie.gr/%C2%A0diakosmisi-spitiou/ps/86/','https://www.bigcharlie.gr/organwsi-spitiou-kathariotita/ps/88/'];
        $array4=['https://www.bigcharlie.gr/%C2%A0eidi-pet-shop/ps/89/','https://www.bigcharlie.gr/kipos-eidi-ekswterikou-xwrou/ps/90/'];
        $array5=['https://www.bigcharlie.gr/%C2%A0rouxa-aksesouar-endusis/ps/78/','https://www.bigcharlie.gr/%C2%A0valitses-eidi-taksidiou-tsantes/ps/77/','https://www.bigcharlie.gr/%C2%A0vrefika-eidi/ps/80/','https://www.bigcharlie.gr/omorfia-peripoiisi/ps/79/','https://www.bigcharlie.gr/xristougenna/ps/3956/'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'bigcharlie001.seo070.site'],
            'x2'=>['source'=>$array2,'target'=>'bigcharlie002.seo070.site'],
            'x3'=>['source'=>$array3,'target'=>'bigcharlie003.seo070.site'],
            'x4'=>['source'=>$array4,'target'=>'bigcharlie004.seo070.site'],
            'x5'=>['source'=>$array5,'target'=>'bigcharlie005.seo070.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',    #用了VPN可以屏蔽此段
            'base_uri'=>'https://www.bigcharlie.gr/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb.com
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  <--daogecvb.com
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }

    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('.my_pagination_after_text a');
        // echo $nextNode->attr("href");exit;
            if ($nextNode->count()) {
                if($uri != $nextNode->attr('href')){
                    $this->processPage($nextNode->attr('href'));                    
                }
            }
        // echo $nextNode->text();exit;
    }


    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i){
            if(strstr($node->text(),"sku")){
                try{
                    $this->discount = rand(65,80)/100;   #rand(65,80)/100;
                    // $request = json_encode($node, JSON_UNESCAPED_UNICODE);
                    $this->crawlerProduct([
                        'name'=> "[{$i}]".json_decode($node->text(),true)['name'],
                        'url'=> sprintf('%s',json_decode($node->text(),true)['url']),
                        'sku'=>json_decode($node->text(),true)['sku'],
                        'title'=>json_decode($node->text(),true)['name'],
                        'brand'=> json_decode($node->text(),true)['brand']['name'],
                        'price'=> json_decode($node->text(),true)['offers']['price'],
                        'description'=>json_decode($node->text(),true)['description'],
                        'category'=>explode(">",json_decode($node->text(),true)['category']['name']),
                    ]);
                }catch(\Exception $exception){
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
                }   
            }
        });
    }

    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他---------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //关键词
        $product['keywords'] = [];
        //性别
        $product['gender'] = '';
        //颜色
        $product['color'] = '';
        //亚类
        $product['subCategory'] = ' ';

        //SKU
        $product['sku'] = $item['sku'];
        // print_r($product['sku']);exit;
        // 标题
        $product['title'] = $item['title'];
        // print_r($product['title']);exit;
        //价格
        $product['price'] = $item['price'];#*0.003206
        // print_r($product['price']);exit;
        // 品牌
        $product['brand'] = $item['brand'];
        // print_r($product['brand']);exit;

        //ID
        $product['id'] = '';
        // print_r($product['id']);exit;
        //
        $product['type'] = 'simple';
        //图片-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $images = $crawler->filter('.zoom_id')->each(function(Crawler $node,$i)use(&$images){
            return $node->attr('href');
        });
        // print_r($images);exit;
        // print_r($data);exit;
        foreach ($images as $image) {
            // echo $image."\r\n";
            $img[]= [
                'name' => $product['title'],
                'src' => $image,
            ];
        }
        $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        //分类/子分类-------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $breadcrumbs = array_slice($item['category'],0);
        // print_r($breadcrumbs);exit;
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;

        //标签--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        // print_r($tags);exit;
        array_push($tags,$product['brand']);
        // print_r($tags);exit;
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;

        //选项--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['variations']= [];
        // print_r($product['variations']);exit;        
        
        //顶部菜单栏
        $product['short_description']="";
        // print_r($product['short_description']);exit;
        //产品描述-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['description'] = "";
        try{
            $product['description'] = $item['description'];
        }catch(Exception $e){
            $product['description'] = "";
        }
        // print_r($product['description']);exit;
        // 产品规格---------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['attributes'] = [];
        // print_r($product['attributes']);exit;

                // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "Nextone".PHP_EOL;
    }
    
    //图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   
