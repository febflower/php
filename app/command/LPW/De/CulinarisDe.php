<?php            //https://www.culinaris.eu/
namespace app\command\LPW\De;

//调用模块
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use Exception;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class CulinarisDe extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    //自定义命令行  build:wordpress:lpw:-------------
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:culinarisde')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://www.culinaris.eu/');
    }
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接   //-10875-0-1.html
        $array1=['https://www.culinaris.eu/epages/61562434.sf/de_DE/?ObjectPath=/Shops/61562434/Categories/kochen'];
        $array2=['https://www.culinaris.eu/epages/61562434.sf/de_DE/?ObjectPath=/Shops/61562434/Categories/Backen'];
        $array3=['https://www.culinaris.eu/epages/61562434.sf/de_DE/?ObjectPath=/Shops/61562434/Categories/Essen__Trinken'];


        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'culinarisde001.xms008.site'],
            'x2'=>['source'=>$array2,'target'=>'culinarisde002.xms008.site'],
            'x3'=>['source'=>$array3,'target'=>'culinarisde003.xms007.site'],
        ];
        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',    #用了VPN可以屏蔽此段
            'base_uri'=>'https://www.culinaris.eu/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);
        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }
    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');
        // echo $nextNode->attr("href");exit;
        if ($nextNode->count()) {
            $this->processPage("https://www.culinaris.eu/epages/61562434.sf/de_DE/".$nextNode->attr('href'));  
        }
    }
    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.InfoArea h3 a')->each(function(Crawler $node,$i){
                try{
                    $this->discount = rand(65,80)/100;  //rand(65,80)/100;
                    // $request = json_encode($node, JSON_UNESCAPED_UNICODE);
                    $this->crawlerProduct([
                        'name'=> "[{$i}]".$node->text(),
                        'url'=> sprintf('%s',"https://www.culinaris.eu/epages/61562434.sf/de_DE/".$node->attr('href')),
                    ]);
                }catch(\Exception $exception){
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
                }   
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他---------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //关键词
        $product['keywords'] = [];
        //性别
        $product['gender'] = '';
        //颜色
        $product['color'] = "";
        //亚类
        $product['subCategory'] = ' ';

        //SKU
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->attr('content');
        // print_r($product['sku']);exit;
        // 标题
        $product['title'] = $crawler->filter('.ProductDetails')->filter('[itemprop="name"]')->text();
        // print_r($product['title']);exit;
        //价格
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr('content');
        // print_r(var_dump($product['price']));exit;
        // 品牌
        $product['brand'] = $crawler->filter('[itemprop="brand"]')->attr('content');
        // print_r($product['brand']);exit;

        //ID
        $product['id'] = '';
        // print_r($product['id']);exit;
        //
        $product['type'] = 'simple';
        //图片-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
        if($crawler->filter('[itemprop="image"]')->count()){
            $images = $crawler->filter('[itemprop="image"]')->each(function(Crawler $node,$i){
                return str_replace("_m","",str_replace("_xs","",str_replace("_s","",str_replace("_ms","",$node->attr('src')))));
            });
        }else{
            $images[] = $crawler->filter('[id="ProductSlideshow"] div img')->attr('src');
        }
        // print_r($images);exit;
        foreach ($images as $image) {
            $img[] = [
                'name' => $product['title'],
                'src' => "https://www.culinaris.eu".$image,
            ];
        }
        $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        //分类/子分类-------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $breadcrumbs = $crawler->filter('[itemprop="itemListElement"] a')->each(function(Crawler $node,$i){
            return $node->text();
        });
        $breadcrumbs = array_slice($breadcrumbs,2);
        // print_r($breadcrumbs);exit;
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;

        //标签--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,1);
        array_push($tags,$product['brand'],$product['color']);
        // print_r($tags);exit;
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;

        //选项--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['variations'] = [];
        // $product['type'] = 'variable';
        // print_r($product['variations']);exit; 
        
        //顶部菜单栏
        // $product['short_description']="";
        // print_r($product['short_description']);exit;
        //产品描述-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['description'] = ""; 
        if($crawler->filter('[itemprop="description"]')->count()){
            if($crawler->filter('[itemprop="description"] iframe')->count()){
                $responsive_video = $crawler->filter('[itemprop="description"] iframe')->html();
                $product['description'] = str_replace('/WebRoot/','https://www.culinaris.eu/WebRoot/',str_replace($responsive_video,"",$crawler->filter('[itemprop="description"]')->html()));

            }else{
                $product['description'] = str_replace('/WebRoot/','https://www.culinaris.eu/WebRoot/',$crawler->filter('[itemprop="description"]')->html());
            }
        }
        // print_r($product['description']);exit;
        // 产品规格---------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['attributes'] = [];
        if($crawler->filter('.UserAttributes')->count()){
            $num_attributes = $crawler->filter('.UserAttributes tr')->count();
            for($i=1;$i<$num_attributes;$i++){
                $product['attributes'][]=[
                    'name' =>$crawler->filter('.UserAttributes tr')->eq($i)->filter('td')->eq(0)->text(),
                    'options'=>$crawler->filter('.UserAttributes tr')->eq($i)->filter('td')->eq(1)->text(),
                ];
            }
        }
        // print_r($product['attributes']);exit;

        // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "Nextone".PHP_EOL;
    }
    //图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }
    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   
