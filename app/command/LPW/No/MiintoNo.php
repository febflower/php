<?php                           //https://www.miinto.no/
namespace app\command\LPW\No;

//调用模块
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class MiintoNo extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:------------
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:miintono')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://www.miinto.no/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1 = ['https://www.miinto.no/dame-blazere'];
        $array2 = ['https://www.miinto.no/dame-boblevester','https://www.miinto.no/dame-bomberjakker','https://www.miinto.no/dame-dongerijakker','https://www.miinto.no/dame-kaper','https://www.miinto.no/dame-poncho','https://www.miinto.no/dame-regnjakker','https://www.miinto.no/dame-skinnjakker','https://www.miinto.no/dame-trenchcoats'];
        $array3 = ['https://www.miinto.no/dame-dunjakker'];
        $array4 = ['https://www.miinto.no/dame-parkas'];
        $array5 = ['https://www.miinto.no/dame-varjakke'];
        $array6 = ['https://www.miinto.no/dame-culottes-og-bukser-med-vide-ben'];
        $array7 = ['https://www.miinto.no/dame-chinos'];
        $array8 = ['https://www.miinto.no/dame-dressbukser','https://www.miinto.no/dame-joggebukser'];
        $array9 = ['https://www.miinto.no/dame-leggings','https://www.miinto.no/dame-skinnbukser'];
        $array10 = ['https://www.miinto.no/dame-slim-fit-bukser'];
        $array11 = ['https://www.miinto.no/dame-badetoy']; 
        $array12 = ['https://www.miinto.no/dame-loose-fit-jeans','https://www.miinto.no/dame-bootcut-jeans','https://www.miinto.no/dame-straight-leg-jeans','https://www.miinto.no/dame-jumpsuits-og-playsuits'];
        $array13 = ['https://www.miinto.no/dame-skinny-jeans'];
        $array14 = ['https://www.miinto.no/dame-festkjoler'];
        $array15 = ['https://www.miinto.no/dame-hverdagskjoler'];//跑到20页
        $array16 = ['https://www.miinto.no/dame-hverdagskjoler?page=21'];//跑到40页
        $array17 = ['https://www.miinto.no/dame-hverdagskjoler?page=41'];
        $array18 = ['https://www.miinto.no/dame-maxikjoler','https://www.miinto.no/dame-sommerkjoler'];
        $array19 = ['https://www.miinto.no/dame-kofferter-og-bager','https://www.miinto.no/dameklokker'];
        $array20 = ['https://www.miinto.no/dame-cardigans'];
        $array21 = ['https://www.miinto.no/dame-hettegensere-og-hoodies'];//跑到17页
        $array22 = ['https://www.miinto.no/dame-hettegensere-og-hoodies?page=18'];
        $array23 = ['https://www.miinto.no/dame-kashmirgensere','https://www.miinto.no/dame-tunika','https://www.miinto.no/dame-vester'];
        $array24 = ['https://www.miinto.no/dame-strikkegensere-og-jakker'];//跑到17
        $array25 = ['https://www.miinto.no/dame-strikkegensere-og-jakker?page=18'];
        $array26 = ['https://www.miinto.no/dame-shorts'];
        $array27 = ['https://www.miinto.no/dame-bluser'];//跑到22
        $array28 = ['https://www.miinto.no/dame-bluser?page=23'];
        $array29 = ['https://www.miinto.no/dame-dongeriskjorter','https://www.miinto.no/dame-kortermede-skjorter','https://www.miinto.no/dame-skjorter'];//跑到13
        $array30 = ['https://www.miinto.no/dame-skjorter?page=14'];
        $array31 = ['https://www.miinto.no/dame-skjort'];
        $array32 = ['https://www.miinto.no/damesko-ballerinasko','https://www.miinto.no/damesko-espadrillos','https://www.miinto.no/damesko-fjellsko','https://www.miinto.no/damesko-flate-sko','https://www.miinto.no/damesko-gummistovler'];
        $array33 = ['https://www.miinto.no/damesko-flip-flops'];
        $array34 = ['https://www.miinto.no/damesko-hoye-haeler','https://www.miinto.no/damesko-mokkasiner'];
        $array35 = ['https://www.miinto.no/damesko-pumps'];
        $array36 = ['https://www.miinto.no/damesko-sandaler'];
        $array37 = ['https://www.miinto.no/damesko-skoletter-og-ankelstovletter'];
        $array38 = ['https://www.miinto.no/damsko-sneakers'];//跑到22
        $array39 = ['https://www.miinto.no/damsko-sneakers?page=23'];//跑到44
        $array40 = ['https://www.miinto.no/damsko-sneakers?page=45'];
        $array41 = ['https://www.miinto.no/damesko-stovler-og-boots'];
        $array42 = ['https://www.miinto.no/damesko-tofler','https://www.miinto.no/damesko-vintersko','https://www.miinto.no/damesko-wedges-og-kilehael'];
        $array43 = ['https://www.miinto.no/dame-sokker-og-strompebukser','https://www.miinto.no/dame-undertoy'];
        $array44 = ['https://www.miinto.no/hjem'];
        $array45 = ['https://www.miinto.no/barn-badetoy','https://www.miinto.no/barn-barneutstyr','https://www.miinto.no/barn-body-og-sett','https://www.miinto.no/barn-heldresser','https://www.miinto.no/barn-jakker','https://www.miinto.no/barn-kjoler-og-skjort','https://www.miinto.no/barn-nattoy'];
        $array46 = ['https://www.miinto.no/barn-overdeler'];//跑到20
        $array47 = ['https://www.miinto.no/barn-overdeler?page=21','https://www.miinto.no/barn-penklaer','https://www.miinto.no/barn-sport','https://www.miinto.no/barn-accessories'];
        $array48 = ['https://www.miinto.no/barnesko'];
        $array49 = ['https://www.miinto.no/barn-bukser-og-jeans','https://www.miinto.no/barn-undertoy','https://www.miinto.no/barn-vesker','https://www.miinto.no/barn-ytterklaer'];
        $array50 = ['https://www.miinto.no/herre-nokkering','https://www.miinto.no/herre-lommetorkle','https://www.miinto.no/herre-paraplyer','https://www.miinto.no/herre-luer-og-hodeplagg2'];
        $array51 = ['https://www.miinto.no/herre-badeshorts','https://www.miinto.no/herre-belter-og-bukseseler'];
        $array52 = ['https://www.miinto.no/herre-cargo-pants','https://www.miinto.no/herre-chinos'];
        $array53 = ['https://www.miinto.no/herre-joggebukser'];
        $array54 = ['https://www.miinto.no/herre-dress'];
        $array55 = ['https://www.miinto.no/herre-gensere'];//跑到20
        $array56 = ['https://www.miinto.no/herre-gensere?page=21','https://www.miinto.no/herre-strikkejakker','https://www.miinto.no/herre-sweatshirts?page=21'];
        $array57 = ['https://www.miinto.no/herre-hettegensere-og-hoodies'];//跑到20
        $array58 = ['https://www.miinto.no/herre-hettejakker','https://www.miinto.no/herre-hettegensere-og-hoodies?page=21'];
        $array59 = ['https://www.miinto.no/herre-sweatshirts'];//跑到20
        $array60 = ['https://www.miinto.no/herre-hansker-og-votter','https://www.miinto.no/herreklokker','https://www.miinto.no/herre-kofferter-og-bager'];
        $array61 = ['https://www.miinto.no/herre-jeans'];
        $array62 = ['https://www.miinto.no/herre-blazere-og-dressjakker'];
        $array63 = ['https://www.miinto.no/herre-bomberjakker','https://www.miinto.no/herre-dunvester'];
        $array64 = ['https://www.miinto.no/herre-dunjakker'];
        $array65 = ['https://www.miinto.no/herre-frakker-og-trenchcoats','https://www.miinto.no/herre-dongerijakker','https://www.miinto.no/herre-parkas','https://www.miinto.no/herre-regnjakker'];
        $array66 = ['https://www.miinto.no/herre-skinnjakker','https://www.miinto.no/herre-vester','https://www.miinto.no/herre-vinterjakke'];
        $array67 = ['https://www.miinto.no/herre-varjakke'];
        $array68 = ['https://www.miinto.no/herre-lommeboker-og-kortholdere','https://www.miinto.no/herre-nattoy'];
        $array69 = ['https://www.miinto.no/herre-piqueskjorter'];
        $array70 = ['https://www.miinto.no/herre-shorts'];
        $array71 = ['https://www.miinto.no/herre-skjerf-og-sjal'];
        $array72 = ['https://www.miinto.no/herre-skjorter'];//跑到20
        $array73 = ['https://www.miinto.no/herre-slips','https://www.miinto.no/herre-sloyfer-og-bow-tie','https://www.miinto.no/herre-skjorter?page=21'];
        $array74 = ['https://www.miinto.no/herre-solbriller'];//跑到17
        $array75 = ['https://www.miinto.no/herre-teknologi-og-mobiltilbehor','https://www.miinto.no/herre-solbriller?page=15'];
        $array76 = ['https://www.miinto.no/herre-t-skjorter'];//跑到20
        $array77 = ['https://www.miinto.no/herre-t-skjorter?page=21'];//跑到40
        $array78 = ['https://www.miinto.no/herre-t-skjorter?page=41'];
        $array79 = ['https://www.miinto.no/herre-undertoy','https://www.miinto.no/herresko-boots'];
        $array80 = ['https://www.miinto.no/herre-vesker'];
        $array81 = ['https://www.miinto.no/herresko-espadrillos','https://www.miinto.no/herresko-flip-flops','https://www.miinto.no/herresko-gummistovler','https://www.miinto.no/herresko-loafers','https://www.miinto.no/herresko-penso'];
        $array82 = ['https://www.miinto.no/herresko-sandaler','https://www.miinto.no/herresko-seilersko','https://www.miinto.no/herresko-tofler','https://www.miinto.no/herresko-vintersko'];
        $array83 = ['https://www.miinto.no/herresko-sneakers'];//pa25
        $array84 = ['https://www.miinto.no/herresko-sneakers?page=26'];//50
        $array85 = ['https://www.miinto.no/herresko-sneakers?page=51'];
        $array86 = ['https://www.miinto.no/sport'];//28
        $array87 = ['https://www.miinto.no/sport?page=29'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'miintono001.seo071.site'],
            'x2'=>['source'=>$array2,'target'=>'miintono002.seo070.site'],
            'x3'=>['source'=>$array3,'target'=>'miintono003.seo071.site'],
            'x4'=>['source'=>$array4,'target'=>'miintono004.seo070.site'],
            'x5'=>['source'=>$array5,'target'=>'miintono005.seo071.site'],
            'x6'=>['source'=>$array6,'target'=>'miintono006.seo070.site'],
            'x7'=>['source'=>$array7,'target'=>'miintono007.seo071.site'],
            'x8'=>['source'=>$array8,'target'=>'miintono008.seo070.site'],
            'x9'=>['source'=>$array9,'target'=>'miintono009.seo071.site'],
            'x10'=>['source'=>$array10,'target'=>'miintono010.seo070.site'],
            'x11'=>['source'=>$array11,'target'=>'miintono011.seo071.site'],
            'x12'=>['source'=>$array12,'target'=>'miintono012.seo070.site'],
            'x13'=>['source'=>$array13,'target'=>'miintono013.seo071.site'],
            'x14'=>['source'=>$array14,'target'=>'miintono014.seo070.site'],
            'x15'=>['source'=>$array15,'target'=>'miintono015.seo071.site'],
            'x16'=>['source'=>$array16,'target'=>'miintono016.seo072.site'],
            'x17'=>['source'=>$array17,'target'=>'miintono017.seo071.site'],
            'x18'=>['source'=>$array18,'target'=>'miintono018.seo072.site'],
            'x19'=>['source'=>$array19,'target'=>'miintono019.seo071.site'],
            'x20'=>['source'=>$array20,'target'=>'miintono020.seo072.site'],
            'x21'=>['source'=>$array21,'target'=>'miintono021.seo071.site'],
            'x22'=>['source'=>$array22,'target'=>'miintono022.seo072.site'],
            'x23'=>['source'=>$array23,'target'=>'miintono023.seo071.site'],
            'x24'=>['source'=>$array24,'target'=>'miintono024.seo072.site'],
            'x25'=>['source'=>$array25,'target'=>'miintono025.seo071.site'],
            'x26'=>['source'=>$array26,'target'=>'miintono026.seo072.site'],
            'x27'=>['source'=>$array27,'target'=>'miintono027.seo071.site'],
            'x28'=>['source'=>$array28,'target'=>'miintono028.seo072.site'],
            'x29'=>['source'=>$array29,'target'=>'miintono029.seo071.site'],
            'x30'=>['source'=>$array30,'target'=>'miintono030.seo072.site'],
            'x31'=>['source'=>$array31,'target'=>'miintono031.seo071.site'],
            'x32'=>['source'=>$array32,'target'=>'miintono032.seo072.site'],
            'x33'=>['source'=>$array33,'target'=>'miintono033.seo071.site'],
            'x34'=>['source'=>$array34,'target'=>'miintono034.seo072.site'],
            'x35'=>['source'=>$array35,'target'=>'miintono035.seo071.site'],
            'x36'=>['source'=>$array36,'target'=>'miintono036.seo072.site'],
            'x37'=>['source'=>$array37,'target'=>'miintono037.seo071.site'],
            'x38'=>['source'=>$array38,'target'=>'miintono038.seo072.site'],
            'x39'=>['source'=>$array39,'target'=>'miintono039.seo071.site'],
            'x40'=>['source'=>$array40,'target'=>'miintono040.seo072.site'],
            'x41'=>['source'=>$array41,'target'=>'miintono041.seo071.site'],
            'x42'=>['source'=>$array42,'target'=>'miintono042.seo072.site'],
            'x43'=>['source'=>$array43,'target'=>'miintono043.seo071.site'],
            'x44'=>['source'=>$array44,'target'=>'miintono044.seo072.site'],
            'x45'=>['source'=>$array45,'target'=>'miintono045.seo071.site'],
            'x46'=>['source'=>$array46,'target'=>'miintono046.seo072.site'],
            'x47'=>['source'=>$array47,'target'=>'miintono047.seo071.site'],
            'x48'=>['source'=>$array48,'target'=>'miintono048.seo072.site'],
            'x49'=>['source'=>$array49,'target'=>'miintono049.seo072.site'],
            'x50'=>['source'=>$array50,'target'=>'miintono050.seo072.site'],
            'x51'=>['source'=>$array51,'target'=>'miintono051.seo072.site'],
            'x52'=>['source'=>$array52,'target'=>'miintono052.seo072.site'],
            'x53'=>['source'=>$array53,'target'=>'miintono053.seo072.site'],
            'x54'=>['source'=>$array54,'target'=>'miintono054.seo072.site'],
            'x55'=>['source'=>$array55,'target'=>'miintono055.seo072.site'],
            'x56'=>['source'=>$array56,'target'=>'miintono056.seo072.site'],
            'x57'=>['source'=>$array57,'target'=>'miintono057.seo072.site'],
            'x58'=>['source'=>$array58,'target'=>'miintono058.seo072.site'],
            'x59'=>['source'=>$array59,'target'=>'miintono059.seo072.site'],
            'x60'=>['source'=>$array60,'target'=>'miintono060.seo072.site'],
            'x61'=>['source'=>$array61,'target'=>'miintono061.seo072.site'],
            'x62'=>['source'=>$array62,'target'=>'miintono062.seo072.site'],
            'x63'=>['source'=>$array63,'target'=>'miintono063.seo072.site'],
            'x64'=>['source'=>$array64,'target'=>'miintono064.seo072.site'],
            'x65'=>['source'=>$array65,'target'=>'miintono065.seo072.site'],
            'x66'=>['source'=>$array66,'target'=>'miintono066.seo074.site'],
            'x67'=>['source'=>$array67,'target'=>'miintono067.seo073.site'],
            'x68'=>['source'=>$array68,'target'=>'miintono068.seo074.site'],
            'x69'=>['source'=>$array69,'target'=>'miintono069.seo073.site'],
            'x70'=>['source'=>$array70,'target'=>'miintono070.seo074.site'],
            'x71'=>['source'=>$array71,'target'=>'miintono071.seo073.site'],
            'x72'=>['source'=>$array72,'target'=>'miintono072.seo074.site'],
            'x73'=>['source'=>$array73,'target'=>'miintono073.seo073.site'],
            'x74'=>['source'=>$array74,'target'=>'miintono074.seo074.site'],
            'x75'=>['source'=>$array75,'target'=>'miintono075.seo073.site'],
            'x76'=>['source'=>$array76,'target'=>'miintono076.seo074.site'],
            'x77'=>['source'=>$array77,'target'=>'miintono077.seo073.site'],
            'x78'=>['source'=>$array78,'target'=>'miintono078.seo074.site'],
            'x79'=>['source'=>$array79,'target'=>'miintono079.seo073.site'],
            'x80'=>['source'=>$array80,'target'=>'miintono080.seo074.site'],
            'x81'=>['source'=>$array81,'target'=>'miintono081.seo073.site'],
            'x82'=>['source'=>$array82,'target'=>'miintono082.seo074.site'],
            'x83'=>['source'=>$array83,'target'=>'miintono083.seo073.site'],
            'x84'=>['source'=>$array84,'target'=>'miintono084.seo074.site'],
            'x85'=>['source'=>$array85,'target'=>'miintono085.seo073.site'],
            'x86'=>['source'=>$array86,'target'=>'miintono086.seo074.site'],
            'x87'=>['source'=>$array87,'target'=>'miintono087.seo073.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://www.miinto.no/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618    //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb.com
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba    //cs_ce714dc6416970b978ccf497c666a8f3185179a5  <--daogecvb.com
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }

    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);#

            $this->processProductList($crawler);

        //判断是否有翻页语句
        $nextNode = $crawler->filter('.c-listing-pagination__link--next');  //
            if($nextNode->count()){
                $this ->processPage("https://www.miinto.no".$nextNode->attr('href'));
            }
        // echo $nextNode->text();exit;              
    }


    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        // print_r($breadcrumbs);exit;
        // echo $crawler->filter('.c-product-card__text-link')->attr('href');exit;
        $crawler->filter('.c-product-card__text-link')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;    #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->text(),
                    'url'  => sprintf('%s','https://www.miinto.no'.$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }

    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];//关键词
        $product['gender'] = '';//性别
        $product['color'] = '';//颜色
        $product['subCategory'] = ' ';//亚类目
        $product['variations']=[];//选项
        $product['description'] = ' ';//描述
        $product['attributes'] = [];//规格

        //顶部菜单栏
        // if($crawler->filter('.product-subtitle')->count()){
        //     $product['short_description'] = $crawler->filter('.product-subtitle')->text();
        // };
        // print_r($product['short_description']);exit;
        //颜色
        if(json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['color'] != null){
            $product['color'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['color'];
        }
        // print_r($product['color']);exit;
        //SKU
        $product['sku'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['sku'];
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['name'];
        // print_r($product['title']);exit;
        //价格
        $product['price'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['offers']['price']*0.09759;
        // print_r($product['price']);exit;
        //品牌
        $product['brand'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];
        // print_r($product['brand']);exit;
        //类型""
        $product['type'] = 'simple';
        //
        $product['id'] = "";
        
        //图片------------------------------------------------------------------------------------------------------------------
        $images = $crawler->filter('.p-product-page__images-thumb-box img')->each(function(Crawler $node,$i)use(&$images){
            return str_replace("height=145&","height=145&amp;",str_replace("width=90&","width=90&amp;",$node->attr('data-lazy')));
        });
        // print_r($images);exit;
        // print_r($data);exit;
        foreach ($images as $image) {
            // echo $image."\r\n";
            $img[]= [
                'name' => $product['title'],
                'src' => $image,
            ];
        }
        $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        
        //子分类----------------------------------------------------------------------------------------------------------
        // $product['categories']="";
        $breadcrumbs = $crawler->filter('.p-product-page__header a')->each(function(Crawler $node,$i){
            return $node->text();
        });
        // print_r($breadcrumbs);exit;
        $breadcrumbs = array_slice($breadcrumbs,1,-1);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;
        //标签-------------------------------------------------------------------------------------------------------------
        $tags = $crawler->filter('.p-product-page__tabs-product-buttons a')->each(function(Crawler $node,$i){
            return $node->text();
        });
        // print_r($tags);exit;
        // array_push($tags,$product['brand'],$product['color']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;

        //选项--------------------------------------------------------------------------------------------
        //尺寸EU størrelser
        if($crawler->filter('[id="productEuSizesDesktop"] label')->count()){
            // print_r($var);exit;
            $product['variations'][]= [
                'name' => $crawler->filter('.c-product-select__items button')->eq(0)->text(),
                'options' => $crawler->filter('[id="productEuSizesDesktop"] input')->each(function(Crawler $node,$i){
                    return $node->attr('value');
                })];
            $product['type'] = 'variable';
        }else{
            //尺寸Merkestørrelser
            $product['variations'][]= [
                'name' => $crawler->filter('.c-product-select__items button')->eq(1)->text(),
                'options' => $crawler->filter('[id="productProducerSizesDesktop"] input')->each(function(Crawler $node,$i){
                    return $node->attr('value');
                })];
            $product['type'] = 'variable';
        }
        // 
        // print_r($product['variations']);exit;

        //产品描述-----------------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.p-product-page__description')->count()){
            $del = $crawler->filter('.p-product-page__tabs-product-buttons')->html();
            $product['description'] = str_replace("499 NOK","49 EUR",str_replace(" 499,-"," 49€",str_replace($del,'',$crawler->filter('.p-product-page__description')->html())));
        }
        // print_r($product['description']);exit;
        // 产品规格---------------------------------------------------------------------------------------------------------------------
        $product['attributes'] = [];
        // print_r($product['attributes']);exit;

        //print_r($product);exit;
        // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "NextOne".PHP_EOL;
    }

    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   