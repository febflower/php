<?php      //https://www.tradeinn.com/bikeinn/en
namespace app\command\LPW\En\TradeinnEn;
                                                                                    
//调用模块
use app\command\BuildCommon;
use Attribute;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class Bikeinn extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:bikeinn')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源//https://www.tradeinn.com/bikeinn/en');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1=['https://www.tradeinn.com/bikeinn/en/bags/4018/lf#fq=id_familia=4018&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=2928'];
        $array2=['https://www.tradeinn.com/bikeinn/en/bike-parts/4003/lf#fq=id_familia=4003&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=3024'];
        $array3=['https://www.tradeinn.com/bikeinn/en/bikes-and-frames/4005/lf#fq=id_familia=4005&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1296','https://www.tradeinn.com/bikeinn/en/carriers-and-storage/11033/lf#fq=id_familia=11033&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1920'];
        $array4=['https://www.tradeinn.com/bikeinn/en/electronics/4004/lf#fq=id_familia=4004&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=2880'];
        $array5=['https://www.tradeinn.com/bikeinn/en/glasses/11590/lf#fq=id_familia=11590&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=3120'];
        $array6=['https://www.tradeinn.com/bikeinn/en/helmets/11591/lf#fq=id_familia=11591&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=3072'];
        $array7=['https://www.tradeinn.com/bikeinn/en/kids-clothing/11050/lf#fq=id_familia=11050&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1584','https://www.tradeinn.com/bikeinn/en/kids-shoes/11064/lf#fq=id_familia=11064&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=96','https://www.tradeinn.com/bikeinn/en/merchandising/11467/lf#fq=id_familia=11467&sort=v30_sum;desc'];
        $array8=['https://www.tradeinn.com/bikeinn/en/lighting/11603/lf#fq=id_familia=11603&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1680'];
        $array9=['https://www.tradeinn.com/bikeinn/en/maintenance/11197/lf#fq=id_familia=11197&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=3120'];
        $array10=['https://www.tradeinn.com/bikeinn/en/mens-clothing/4011/lf#fq=id_familia=4011&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=3024'];
        $array11=['https://www.tradeinn.com/bikeinn/en/mens-shoes/4001/lf#fq=id_familia=4001&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1536'];
        $array12=['https://www.tradeinn.com/bikeinn/en/protections/4013/lf#fq=id_familia=4013&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=576','https://www.tradeinn.com/bikeinn/en/recovery-and-care/4007/lf#fq=id_familia=4007&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1488'];
        $array13=['https://www.tradeinn.com/bikeinn/en/spare-parts/11227/lf#fq=id_familia=11227&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=2832'];
        $array14=['https://www.tradeinn.com/bikeinn/en/training-and-competition/11221/lf#fq=id_familia=11221&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=1200'];
        $array15=['https://www.tradeinn.com/bikeinn/en/wheels-and-tyres/4002/lf#fq=id_familia=4002&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=2880'];
        $array16=['https://www.tradeinn.com/bikeinn/en/womens-clothing/4021/lf#fq=id_familia=4021&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=3024','https://www.tradeinn.com/bikeinn/en/womens-shoes/4022/lf#fq=id_familia=4022&sort=v30_sum;desc@tm4;asc&fe=&pf=&start=384'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'bikeinn001.seo075.site'],
            'x2'=>['source'=>$array2,'target'=>'bikeinn002.seo076.site'],
            'x3'=>['source'=>$array3,'target'=>'bikeinn003.seo075.site'],
            'x4'=>['source'=>$array4,'target'=>'bikeinn004.seo076.site'],
            'x5'=>['source'=>$array5,'target'=>'bikeinn005.seo075.site'],
            'x6'=>['source'=>$array6,'target'=>'bikeinn006.seo076.site'],
            'x7'=>['source'=>$array7,'target'=>'bikeinn007.seo075.site'],
            'x8'=>['source'=>$array8,'target'=>'bikeinn008.seo076.site'],
            'x9'=>['source'=>$array9,'target'=>'bikeinn009.seo075.site'],
            'x10'=>['source'=>$array10,'target'=>'bikeinn010.seo076.site'],
            'x11'=>['source'=>$array11,'target'=>'bikeinn011.seo075.site'],
            'x12'=>['source'=>$array12,'target'=>'bikeinn012.seo076.site'],
            'x13'=>['source'=>$array13,'target'=>'bikeinn013.seo075.site'],
            'x14'=>['source'=>$array14,'target'=>'bikeinn014.seo076.site'],
            'x15'=>['source'=>$array15,'target'=>'bikeinn015.seo075.site'],
            'x16'=>['source'=>$array16,'target'=>'bikeinn016.seo076.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://www.tradeinn.com/bikeinn/en',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_ce714dc6416970b978ccf497c666a8f3185179a5',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {

            $this->processPage($source);
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('POST', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);#

        $this->processProductList($crawler);
    }

    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.items_listado li .BoxPriceName a')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->text(),
                    'url'  => sprintf('%s',"https://tradeinn.com".$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];          #关键词
        $product['gender'] = '';            #性别
        $product['color'] = '';             #颜色
        $product['subCategory'] = ' ';      #亚类别
        $product['variations']=[];          #选项
        $product['description'] = '';       #描述
        $product['short_description'] = ''; #顶部菜单栏
        $product['attributes']=[];          #规格

        //color
        if($crawler->filter('[id="color_una_talla"]')->count()){
            $product['color']=trim(str_replace("Color:","",$crawler->filter('[id="color_una_talla"]')->text()));
        }
        // print_r($product['color']);exit;
        //SKU----------------------------------------------------------------------------------------------------------------
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->attr('content');
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = $crawler->filter('.info .productName')->text();
        // print_r($product['title']);exit;
        // 价格
        $product['price'] = trim(str_replace(",","",str_replace("€","",$crawler->filter('.info .contenedor-precio .price')->text())));
        // print_r($product['price']);exit;
        //品牌
        $product['brand'] = $crawler->filter('.info [itemprop="brand"] [itemprop="name"]')->attr('content');
        // print_r($product['brand']);exit;
        //类型
        $product['type'] = 'simple';
        //id
        $product['id'] = "";
        
        //图片---------------------------------------------------------------------------------------------------------------
        // $product['images'] =[];
        $images = $crawler->filter('.zona_fotoinfo .contenedor_fotosminis .foto a')->each(function(Crawler $node,$i){
            return $node->attr('href');
        });
        // print_r($images);exit;
        foreach($images as $image){
            $product['images'][]=[
                'name'=>$product['title'],
                'src'=>"https://www.tradeinn.com".$image,
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        
        //子分类--------------------------------------------------------------------------------------------------------------
        $breadcrumbs = $crawler->filter('.contenedor_detalle .path_det a')->each(function(Crawler $node,$i){
           return $node->text();
        });
        // print_r($breadcrumbs);exit;
        $breadcrumbs = array_slice($breadcrumbs,2);
        array_push($breadcrumbs,$product['brand']);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        //标签---------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        array_push($tags,$product['color']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        //尺寸选项------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.tallacant .contenedor_tallas .talla_select')->count()){
                $variations = [];
                $crawler->filter('.tallacant .contenedor_tallas .talla_select')->eq(0)->filter('option')->each(function(Crawler $node,$i)use(&$variations,&$product){
                    if(strstr($node->text(),"One Size")){
                        $variations = [];
                    }else{
                        $variations[] = $node->text();
                    }
                });
                if($variations != null){
                    $product['variations'][]=[
                        'name' => "Size",
                        'options' =>array_unique($variations),
                        ];
                        $product['type'] = 'variable';
                }
        }
        // print_r($product['variations']);exit;
        //产品描述----------------------------------------------------------------------------------------------------------------
        // $product['description'] = '';
        if($crawler->filter('.txt_programa .parrafo .addReadMore')->count()){
            $product['description'] = $crawler->filter('.txt_programa .parrafo .addReadMore')->html();
        }else{
            $product['description'] = '';
        }
        // print_r($product['description']);exit;
        //规格
        $product['attributes'] = [];
        if($crawler->filter('.especi_derecha .dt-especificacion .cuadro')->count()){
            $num_attributes = $crawler->filter('.especi_derecha .dt-especificacion .cuadro .contenidor_espec')->count();
            for($i=0;$i<$num_attributes;$i++){
                $product['attributes'][] = [
                    'name'=>$crawler->filter('.especi_derecha .dt-especificacion .cuadro .contenidor_espec')->eq($i)->filter('.izq')->text(),
                    'options'=>$crawler->filter('.especi_derecha .dt-especificacion .cuadro .contenidor_espec')->eq($i)->filter('.der')->text(),
                ];
            }
        }
        // print_r($product['attributes']);exit;
        // --------------------------------------------------------------------------------------------------------------------------
        
        // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "Next One".PHP_EOL;
    }
    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }
    protected function generateName($product): string
    {
        $name = $product['title'];
        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';
        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;
        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   