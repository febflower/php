<?php      //https://www.tradeinn.com/motardinn/en
namespace app\command\LPW\En\TradeinnEn;
                                                                                    
//调用模块
use app\command\BuildCommon;
use Attribute;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class Motardinn extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:motardinn')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源//https://www.tradeinn.com/motardinn/en');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1=[''];
        $array2=[''];
        $array3=[''];
        $array4=[''];
        $array5=[''];
        $array6=[''];
        $array7=[''];
        $array8=[''];
        $array9=[''];
        $array10=[''];
        $array11=[''];
        $array12=[''];
        $array13=[''];
        $array14=[''];
        $array15=[''];
        $array16=[''];
        $array17=[''];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'motardinn001.seo075.site'],
            'x2'=>['source'=>$array2,'target'=>'motardinn002.seo076.site'],
            'x3'=>['source'=>$array3,'target'=>'motardinn003.seo075.site'],
            'x4'=>['source'=>$array4,'target'=>'motardinn004.seo076.site'],
            'x5'=>['source'=>$array5,'target'=>'motardinn005.seo075.site'],
            'x6'=>['source'=>$array6,'target'=>'motardinn006.seo076.site'],
            'x7'=>['source'=>$array7,'target'=>'motardinn007.seo075.site'],
            'x8'=>['source'=>$array8,'target'=>'motardinn008.seo076.site'],
            'x9'=>['source'=>$array9,'target'=>'motardinn009.seo075.site'],
            'x10'=>['source'=>$array10,'target'=>'motardinn010.seo076.site'],
            'x11'=>['source'=>$array11,'target'=>'motardinn011.seo075.site'],
            'x12'=>['source'=>$array12,'target'=>'motardinn012.seo076.site'],
            'x13'=>['source'=>$array13,'target'=>'motardinn013.seo075.site'],
            'x14'=>['source'=>$array14,'target'=>'motardinn014.seo076.site'],
            'x15'=>['source'=>$array15,'target'=>'motardinn015.seo075.site'],
            'x16'=>['source'=>$array16,'target'=>'motardinn016.seo076.site'],
            'x17'=>['source'=>$array17,'target'=>'motardinn017.seo075.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://www.tradeinn.com/motardinn/en',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);#

        $this->processProductList($crawler);
    }

    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.items_listado li .BoxPriceName a')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->text(),
                    'url'  => sprintf('%s',"https://tradeinn.com".$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];          #关键词
        $product['gender'] = '';            #性别
        $product['color'] = '';             #颜色
        $product['subCategory'] = ' ';      #亚类别
        $product['variations']=[];          #选项
        $product['description'] = '';       #描述
        $product['short_description'] = ''; #顶部菜单栏
        $product['attributes']=[];          #规格

        //color
        if($crawler->filter('[id="color_una_talla"]')->count()){
            $product['color']=trim(str_replace("Color:","",$crawler->filter('[id="color_una_talla"]')->text()));
        }
        // print_r($product['color']);exit;
        //SKU----------------------------------------------------------------------------------------------------------------
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->attr('content');
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = $crawler->filter('.info .productName')->text();
        // print_r($product['title']);exit;
        // 价格
        $product['price'] = trim(str_replace(",","",str_replace("€","",$crawler->filter('.info .contenedor-precio .price')->text())));
        // print_r($product['price']);exit;
        //品牌
        $product['brand'] = $crawler->filter('.info [itemprop="brand"] [itemprop="name"]')->attr('content');
        // print_r($product['brand']);exit;
        //类型
        $product['type'] = 'simple';
        //id
        $product['id'] = "";
        
        //图片---------------------------------------------------------------------------------------------------------------
        // $product['images'] =[];
        $images = $crawler->filter('.zona_fotoinfo .contenedor_fotosminis .foto a')->each(function(Crawler $node,$i){
            return $node->attr('href');
        });
        // print_r($images);exit;
        foreach($images as $image){
            $product['images'][]=[
                'name'=>$product['title'],
                'src'=>"https://www.tradeinn.com".$image,
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        
        //子分类--------------------------------------------------------------------------------------------------------------
        $breadcrumbs = $crawler->filter('.contenedor_detalle .path_det a')->each(function(Crawler $node,$i){
           return $node->text();
        });
        // print_r($breadcrumbs);exit;
        $breadcrumbs = array_slice($breadcrumbs,2);
        array_push($breadcrumbs,$product['brand']);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        //标签---------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        array_push($tags,$product['color']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        //尺寸选项------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.tallacant .contenedor_tallas .talla_select')->count()){
            $variations = [];
            $crawler->filter('.tallacant .contenedor_tallas .talla_select')->eq(0)->filter('option')->each(function(Crawler $node,$i)use(&$variations,&$product){
                if(strstr($node->text(),"One Size")){
                    $variations = [];
                }else{
                    $variations[] = $node->text();
                }
            });
            if($variations != null){
                $product['variations'][]=[
                    'name' => "Size",
                    'options' =>array_unique($variations),
                    ];
                    $product['type'] = 'variable';
            }
        }
        // print_r($product['variations']);exit;
        //产品描述----------------------------------------------------------------------------------------------------------------
        // $product['description'] = '';
        if($crawler->filter('.txt_programa .parrafo .addReadMore')->count()){
            $product['description'] = $crawler->filter('.txt_programa .parrafo .addReadMore')->html();
        }else{
            $product['description'] = '';
        }
        // print_r($product['description']);exit;
        //规格
        $product['attributes'] = [];
        if($crawler->filter('.especi_derecha .dt-especificacion .cuadro')->count()){
            $num_attributes = $crawler->filter('.especi_derecha .dt-especificacion .cuadro .contenidor_espec')->count();
            for($i=0;$i<$num_attributes;$i++){
                $product['attributes'][] = [
                    'name'=>$crawler->filter('.especi_derecha .dt-especificacion .cuadro .contenidor_espec')->eq($i)->filter('.izq')->text(),
                    'options'=>$crawler->filter('.especi_derecha .dt-especificacion .cuadro .contenidor_espec')->eq($i)->filter('.der')->text(),
                ];
            }
        }
        // print_r($product['attributes']);exit;
        // --------------------------------------------------------------------------------------------------------------------------
        
        // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "Next One".PHP_EOL;
    }
    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }
    protected function generateName($product): string
    {
        $name = $product['title'];
        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';
        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;
        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   