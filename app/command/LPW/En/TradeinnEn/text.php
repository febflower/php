<?php
$client = new \GuzzleHttp\Client();
$headers = [
  'Cookie' => 'PHPSESSID=ldic7mauk3sn13p3eoj16l6crk; ip=183.179.107.79'
];
$options = [
  'multipart' => [
    [
      'name' => 'vars[]',
      'contents' => 'id_familia=5004'
    ],
    [
      'name' => 'vars[]',
      'contents' => 'atributos_e=5091,6017'
    ],
    [
      'name' => 'vars[]',
      'contents' => 'model.eng;model.eng;video_mp4;id_marca;precio_tachado;sostenible;productes.talla2;productes.talla_usa;productes.talla_jp;productes.talla_uk;tres_sesenta;atributos_padre.atributos.id_atribut_valor;productes.v360;productes.v180;productes.v90;productes.v30;productes.exist;productes.stock_reservat;productes.pmp;productes.id_producte;productes.color;productes.referencia;productes.brut;productes.desc_brand;image_created;id_modelo;familias.eng;familias.eng;familias.id_familia;familias.subfamilias.eng;familias.subfamilias.eng;familias.subfamilias.id_tienda;familias.subfamilias.id_subfamilia;productes.talla;productes.baja;productes.rec;precio_win_210;productes.sellers.id_seller;productes.sellers.precios_paises.precio;productes.sellers.precios_paises.id_pais;fecha_descatalogado;marca;productes.talla_usa'
    ],
    [
      'name' => 'vars[]',
      'contents' => 'v30_sum;desc@tm5;asc'
    ],
    [
      'name' => 'vars[]',
      'contents' => '48'
    ],
    [
      'name' => 'vars[]',
      'contents' => 'productos'
    ],
    [
      'name' => 'vars[]',
      'contents' => 'search'
    ],
    [
      'name' => 'vars[]',
      'contents' => '@marca=372'
    ],
    [
      'name' => 'vars[]',
      'contents' => '0'
    ],
    [
      'name' => 'texto_search',
      'contents' => ''
    ]
]];
$request = new GuzzleHttp\Psr7\Request('POST', 'https://www.tradeinn.com/index.php?action=get_info_elastic_listado&id_tienda=5&idioma=eng', $headers);
$res = $client->sendAsync($request, $options)->wait();
echo $res->getBody();