<?php      //https://www.entropy.com.au/
namespace app\command\LPW\En;
                                                                                    
//调用模块
use app\command\BuildCommon;
use app\command\MSK\En\ColoradoskishopCOM;
use Attribute;
use Automattic\WooCommerce\Client;
use Exception;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\CssSelector\Parser\Shortcut\ClassParser;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class EntropyAU extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:EntropyAU')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源//https://www.entropy.com.au/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1=['https://www.entropy.com.au/collections/arts-crafts'];
        $array2=['https://www.entropy.com.au/collections/baby-toddler-toys'];
        $array3=['https://www.entropy.com.au/collections/construction-toys','https://www.entropy.com.au/collections/back-to-school'];
        $array4=['https://www.entropy.com.au/collections/doll-houses-accessories','https://www.entropy.com.au/collections/dolls','https://www.entropy.com.au/collections/games'];
        $array5=['https://www.entropy.com.au/collections/educational-toys'];
        $array6=['https://www.entropy.com.au/collections/figurines'];
        $array7=['https://www.entropy.com.au/collections/kids-furniture','https://www.entropy.com.au/collections/kitchens-food','https://www.entropy.com.au/collections/musical-instruments'];
        $array8=['https://www.entropy.com.au/collections/open-ended-play','https://www.entropy.com.au/collections/outdoor-play'];
        $array9=['https://www.entropy.com.au/collections/pretend-play','https://www.entropy.com.au/collections/puppets-soft-toys','https://www.entropy.com.au/collections/ride-on-toys'];
        $array10=['https://www.entropy.com.au/collections/puzzles'];
        $array11=['https://www.entropy.com.au/collections/special-needs-toys'];
        $array12=['https://www.entropy.com.au/collections/stem-toys','https://www.entropy.com.au/collections/themed-playsets','https://www.entropy.com.au/collections/vehicles'];
        $array13=['https://www.entropy.com.au/collections/wooden-toys'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'entropy001.xms013.site'],
            'x2'=>['source'=>$array2,'target'=>'entropy002.xms014.site'],
            'x3'=>['source'=>$array3,'target'=>'entropy003.xms013.site'],
            'x4'=>['source'=>$array4,'target'=>'entropy004.xms014.site'],
            'x5'=>['source'=>$array5,'target'=>'entropy005.xms013.site'],
            'x6'=>['source'=>$array6,'target'=>'entropy006.xms014.site'],
            'x7'=>['source'=>$array7,'target'=>'entropy007.xms013.site'],
            'x8'=>['source'=>$array8,'target'=>'entropy008.xms014.site'],
            'x9'=>['source'=>$array9,'target'=>'entropy009.xms013.site'],
            'x10'=>['source'=>$array10,'target'=>'entropy010.xms014.site'],
            'x11'=>['source'=>$array11,'target'=>'entropy011.xms013.site'],
            'x12'=>['source'=>$array12,'target'=>'entropy012.xms014.site'],
            'x13'=>['source'=>$array13,'target'=>'entropy013.xms013.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://www.entropy.com.au/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');
        // echo $nextNode->attr("href");exit;
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));     
        }
    }

    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.productgrid--item .productitem--title a')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->text(),
                    'url'  => sprintf('%s',"https://www.entropy.com.au".$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url']="https://www.onlinetoys.com.au/product/to-all-of-you";
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $json=[];
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$json){
            if(strstr($node->text(),"sku")){
                $json[] = $node->text();
            }
        });
        // print_r(json_decode($json[0],true));exit;
        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];          #关键词
        $product['gender'] = '';            #性别
        $product['color'] = '';             #颜色
        $product['subCategory'] = ' ';      #亚类别
        $product['variations']=[];          #选项
        $product['description'] = '';       #描述
        $product['short_description'] = ''; #顶部菜单栏
        $product['attributes']=[];          #规格
        //SKU----------------------------------------------------------------------------------------------------------------
        $product['sku'] = json_decode($json[0],true)['sku'];
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = json_decode($json[0],true)['name'];
        // print_r($product['title']);exit;
        // 价格
        $product['price'] = json_decode($json[0],true)['offers']['price'];
        // print_r(var_dump($product['price']));exit;
        //品牌
        $product['brand'] = json_decode($json[0],true)['brand']['name'];
        // print_r($product['brand']);exit;
        //类型
        $product['type'] = 'simple';
        //id
        $product['id'] = "";
        
        //图片---------------------------------------------------------------------------------------------------------------
        // $product['images'] =[];
        if($crawler->filter('.gallery-navigation--scroller img')->count()){
            $images = $crawler->filter('.gallery-navigation--scroller img')->each(function(Crawler $node,$i){
                return "https:".str_replace("_75x75_","_800x800_",$node->attr('src'));
            });
        }else{
            $images[] = json_decode($json[0],true)['image'];
        }
        foreach($images as $image){
            $product['images'][]=[
                'name'=>$product['title'],
                'src'=>$image,
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        
        //子分类--------------------------------------------------------------------------------------------------------------
        $breadcrumbs = $crawler->filter('.breadcrumbs-container a')->each(function(Crawler $node,$i){
            return $node->text();
        });
        // print_r($breadcrumbs);exit;
        $breadcrumbs = array_slice($breadcrumbs,1);
        array_push($breadcrumbs,$product['brand']);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        //标签---------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        // array_push($tags,$product['brand']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        //尺寸选项------------------------------------------------------------------------------------------------------------
        // print_r($product['variations']);exit;
        //产品描述----------------------------------------------------------------------------------------------------------------
        // $product['description'] = '';
        if($crawler->filter('.product-description')->count()){
            $product['description'] = $crawler->filter('.product-description')->html();
        }
        // print_r($product['description']);exit;
        // print_r($product['short_description']);exit;
        //规格
        // print_r($product['attributes']);exit;
        // --------------------------------------------------------------------------------------------------------------------------

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // exit();
        echo "\r\n";
        echo "Next One".PHP_EOL;
    }
    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }
    protected function generateName($product): string
    {
        $name = $product['title'];
        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';
        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;
        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   