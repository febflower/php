<?php      //https://www.rogansshoes.com/
namespace app\command\LPW\En;
                                                                                    
//调用模块
use app\command\BuildCommon;
use Attribute;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class RogansshoesCom extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:MonstershopCoUk
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:rogansshoescom')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://www.rogansshoes.com/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1= [//'https://www.rogansshoes.com/womens-shoes',
        // 'https://www.rogansshoes.com/womens-athletic-shoes',
        'https://www.rogansshoes.com/womens-dress-shoes',
        'https://www.rogansshoes.com/womens-casual-shoes',
        'https://www.rogansshoes.com/womens-sandals',
        'https://www.rogansshoes.com/womens-boots',
        'https://www.rogansshoes.com/womens-work-and-safety',
        'https://www.rogansshoes.com/womens-dress-boots',
        'https://www.rogansshoes.com/womens-winter-boots',
        'https://www.rogansshoes.com/womens-slippers',];
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array2= [//'https://www.rogansshoes.com/mens-shoes',
        'https://www.rogansshoes.com/mens-athletic-shoes',
        'https://www.rogansshoes.com/mens-dress-shoes',
        'https://www.rogansshoes.com/mens-casual-shoes',
        'https://www.rogansshoes.com/mens-sandals',
        'https://www.rogansshoes.com/mens-boots',
        'https://www.rogansshoes.com/mens-work-and-safety',
        'https://www.rogansshoes.com/mens-winter-boots',
        'https://www.rogansshoes.com/mens-slippers',];
        
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array3= [//'https://www.rogansshoes.com/kids-shoes',
        'https://www.rogansshoes.com/girls-shoes',
        'https://www.rogansshoes.com/boys-shoes',
        'https://www.rogansshoes.com/baby-toddler-shoes',
        'https://www.rogansshoes.com/kids-wide-shoes',
        //'https://www.rogansshoes.com/clothing-and-accessories',
        'https://www.rogansshoes.com/bags',
        'https://www.rogansshoes.com/jackets',
        'https://www.rogansshoes.com/shirts',
        'https://www.rogansshoes.com/shorts',
        'https://www.rogansshoes.com/pants',
        'https://www.rogansshoes.com/socks',
        'https://www.rogansshoes.com/insoles',
        'https://www.rogansshoes.com/sweatshirts',
        'https://www.rogansshoes.com/shin-guards',];
    
        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'teszgj001.seo067.site'],  // teszgj001.seo067.site
            'c'=>['source'=>$array2,'target'=>'teszgj002.seo067.site'], // teszgj002.seo067.site
            'v'=>['source'=>$array3,'target'=>'teszgj003.seo067.site'],// teszgj003.seo067.site
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#不用VPN可以屏蔽此段
            'base_uri'=>'https://www.rogansshoes.com/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',            //'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',            //'cs_3ef17880456fe03098e0fb0c347869de458fc8ba'
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }

    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
        // $this->crawlerProduct('https://www.rogansshoes.com/new-balance-577-walking-shoes-womens');
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url.'?pageSize=10000&pagenumber=1');
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);#

            $this->processProductList($crawler);
    }




    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        $breadcrumbs = $crawler->filter('.main-wrapper div h1')->each(function(Crawler $node){
            return str_replace('Browse ',"", $node->text());//返回类名text格式
        });
        // print_r($breadcrumbs);exit;

        #进入产品购买页界面
        $crawler->filter('.product-img a')->each(function(Crawler $node,$i)use($breadcrumbs){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->attr('title'),
                    'url'  => sprintf('%s',$node->attr('href')),
                    'breadcrumbs' => $breadcrumbs,
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }

    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name']).'>>>'.$item['url'];
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);


        //SKU----------------------------------------------------------------------------------------------------------------
        $product['sku'] = $crawler->filter('.product-number h2')->text();
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = $crawler->filter('.product-name')->text();
        // print_r($product['title']);exit;
        // 价格
        $product['price'] = str_replace(',',"",str_replace('$',"",$crawler->filter('.price h3')->text()));
        // print_r($product['price']);exit;
        //品牌
        $product['brand'] = str_replace('Shop All ',"",str_replace(' Shoes',"",$crawler->filter('.shopallbrand a')->text()));
        // print_r($product['brand']);exit;
        //类型
        $product['type'] = 'simple';
        //id
        $product['id'] = "";
        // -----------------------------------------------------------------------------------------------------------------
        
        //图片---------------------------------------------------------------------------------------------------------------
        // $product['images'] =[];
        $colors = $crawler->filter('.configimage input')->each(function(Crawler $node,$i){
          # explode("||",str_replace(',',"||",$node->attr('data-gallery')));
            $imagestr = explode(",",trim($node->attr('data-gallery')));
            $gallery = [];
            foreach($imagestr as $img){
                $image = explode("||",trim($img));
                if (strstr($image[0],".JPG")){
                    $gallery[]='https://images.rogansshoes.com/'.$image[0];//str_replace('.JPG',"",$image[0].'.jpg');
                }
            }
            return [
                'color'=> $node->attr('value'),
                'images'=> $gallery
            ];
        });
        // foreach($colors as $color){
        //     $product['images'] = $color['images'];
        // }
        // print_r($product['images']);exit;
        // print_r($colors);exit;
        // ------------------------------------------------------------------------------------------------------------------
        
        //子分类--------------------------------------------------------------------------------------------------------------
        // $product['categories']="";
        array_push($item['breadcrumbs'],$product['brand']);
        // print_r($item['breadcrumbs']);exit;
        $parent = 0;
        $parentCategory = '';
        foreach ($item['breadcrumbs'] as $item['breadcrumbs'] => $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;
        // ------------------------------------------------------------------------------------------------------------------

        //关键词-------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.product-highlight-list img')->count()){
            $product['keywords'] = $crawler->filter('.product-highlight-list img')->attr('title');
        }else{
            $product['keywords'] = [];
        }
        // print_r($product['keywords']);exit;
        // ------------------------------------------------------------------------------------------------------------------

        //其他----------------------------------------------------------------------------------------------------------------
        $product['gender'] = '';
        $product['subCategory'] =' ';
        // -----------------------------------------------------------------------------------------------------------------

        //标签---------------------------------------------------------------------------------------------------------------
        // $product['tags'] = $colors['color'];
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;
        // -------------------------------------------------------------------------------------------------------------------


        //尺寸选项------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.configsize input')->count()>0){
            $product['variations'][]=[
            'name'=>$crawler->filter('.configsize input')->attr('name'),
            'options'=>$crawler->filter('.configsize input')->each(function(Crawler $node,$i){
                return $node->attr('value');
                })];
        }else{
            $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }
        //鞋宽选项
        if($crawler->filter('.configwidth input')->count()>0){
            $product['variations'][]=[
            'name'=>$crawler->filter('.configwidth input')->attr('name'),
            'options'=>$crawler->filter('.configwidth input')->each(function(Crawler $node,$i){
                return $node->attr('value');
                })];
        }else{
            $product['variations'][]=[
            'name'=>'',
            'options' =>""];
        }
        if($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }//判断语句
        // print_r($product['variations']);exit;
        // ----------------------------------------------------------------------------------------------------------------------


        //产品描述----------------------------------------------------------------------------------------------------------------
        // $product['description'] = '';
        try{
            $product['description'] = $crawler->filter('.product-description')->html();#.$crawler->filter('-----')->html();
        }catch(\Exception $e){
            $product['description'] = $crawler->filter('.product-description')->html();
        }
        // print_r($product['description']);exit;
        //规格
        if($crawler->filter(".tab-pane tr")->count()) {
            $count = $crawler->filter(".tab-pane tr")->count();
            for($i=0;$i<$count;$i++){ 
                if($crawler->filter(".tab-pane tr")->eq($i)->filter('td')->eq(0)->text()!='UPC'){
                $product['attributes'][] = [
                    'name' => $crawler->filter(".tab-pane tr")->eq($i)->filter('td')->eq(0)->text(),
                    'options' => [$crawler->filter(".tab-pane tr")->eq($i)->filter('td')->eq(1)->text()],
                    ];
                }
            }
        }
        // print_r($product['attributes']);exit;
        // --------------------------------------------------------------------------------------------------------------------------

        // $this->createProduct($product);
        // print_r($product);exit;
        
       if(count($colors)>0){
            foreach($colors as $item){
                // print_r($item);exit;
                $color = $item['color'];
                $images = $item['images'];
                $newproduct = $product;
                $newproduct['color'] = $color;
                //SKU
                $newproduct['sku'] = $product['sku']."-".$color;
                //图片
                foreach ($images as $image) {
                    $newproduct['images'][] = [
                        'src' => $image,
                        'name' => $newproduct['title'],
                    ];
                }
                // print_r($newproduct['images']);exit;
                //标签
                $newproduct['tags'][0]= $color;
                $newproduct['tags'][1] = $product['brand'];
                $newproduct['tags'] = $this->createProductTag(array_unique(array_filter($newproduct['tags'])));
                // print_r($newproduct['tags']);exit;
                // print_r($newproduct);exit;                
                try {
                    $this->createProduct($newproduct);
                } catch (\Throwable $th) {
                    var_dump($th->getMessage());
                    var_dump($th->getLine());
                    var_dump($th->getFile());
                    //throw $th;
                }
            }
        }else{
            // $product['images'][] = $colors['images'];
            // print_r($product['images']);exit;
            try {
                $this->createProduct($product);
            } catch (\Throwable $th) {
                var_dump($th->getMessage());
                var_dump($th->getLine());
                var_dump($th->getFile());
                //throw $th;
            }
        }
        // exit();
        echo "\r\n";
        echo "Next One".PHP_EOL;
    
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        // var_dump($brand);
        // var_dump($gender);
        // var_dump($category);
        // var_dump($name);
        // var_dump($product['color']);
        // $x =
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
       
        // return $x;
    }
}   