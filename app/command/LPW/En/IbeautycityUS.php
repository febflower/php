<?php      //https://ibeautycity.com/
namespace app\command\LPW\En;
                                                                                    
//调用模块
use app\command\BuildCommon;
use app\command\MSK\En\ColoradoskishopCOM;
use Attribute;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class IbeautycityUS extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:IbeautycityUS')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源//https://ibeautycity.com/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1=['https://ibeautycity.com/t/categories/makeup'];
        $array2=['https://ibeautycity.com/t/categories/makeup?page=31'];
        $array3=['https://ibeautycity.com/t/categories/makeup?page=61'];
        $array4=['https://ibeautycity.com/t/categories/makeup?page=91'];
        $array5=['https://ibeautycity.com/t/categories/makeup?page=121'];
        $array6=['https://ibeautycity.com/t/categories/makeup?page=151'];
        $array7=['https://ibeautycity.com/t/categories/makeup?page=181'];
        $array8=['https://ibeautycity.com/t/categories/makeup?page=211'];
        $array9=['https://ibeautycity.com/t/categories/makeup?page=241'];
        $array10=['https://ibeautycity.com/t/categories/makeup?page=271'];
        $array11=['https://ibeautycity.com/t/categories/makeup?page=301'];
        $array12=['https://ibeautycity.com/t/categories/skin-care'];
        $array13=['https://ibeautycity.com/t/categories/skin-care?page=31'];
        $array14=['https://ibeautycity.com/t/categories/skin-care?page=61'];
        $array15=['https://ibeautycity.com/t/categories/skin-care?page=91'];
        $array16=['https://ibeautycity.com/t/categories/skin-care?page=121'];
        $array17=['https://ibeautycity.com/t/categories/skin-care?page=151'];
        $array18=['https://ibeautycity.com/t/categories/skin-care?page=181'];
        $array19=['https://ibeautycity.com/t/categories/skin-care?page=211'];
        $array20=['https://ibeautycity.com/t/categories/skin-care?page=241'];
        $array21=['https://ibeautycity.com/t/categories/skin-care?page=271'];
        $array22=['https://ibeautycity.com/t/categories/skin-care?page=301'];
        $array23=['https://ibeautycity.com/t/categories/skin-care?page=331'];
        $array24=['https://ibeautycity.com/t/categories/skin-care?page=361'];
        $array25=['https://ibeautycity.com/t/categories/skin-care?page=391'];
        $array26=['https://ibeautycity.com/t/categories/skin-care?page=421'];
        $array27=['https://ibeautycity.com/t/categories/skin-care?page=441'];
        $array28=['https://ibeautycity.com/t/categories/hair-care'];
        $array29=['https://ibeautycity.com/t/categories/hair-care?page=31'];
        $array30=['https://ibeautycity.com/t/categories/hair-care?page=61'];
        $array31=['https://ibeautycity.com/t/categories/hair-care?page=91'];
        $array32=['https://ibeautycity.com/t/categories/hair-care?page=121'];
        $array33=['https://ibeautycity.com/t/categories/hair-care?page=151'];
        $array34=['https://ibeautycity.com/t/categories/hair-care?page=181'];
        $array35=['https://ibeautycity.com/t/categories/hair-care?page=211'];
        $array36=['https://ibeautycity.com/t/categories/hair-care?page=241'];
        $array37=['https://ibeautycity.com/t/categories/hair-care?page=271'];
        $array38=['https://ibeautycity.com/t/categories/hair-care?page=301'];
        $array39=['https://ibeautycity.com/t/categories/hair-care?page=331'];
        $array40=['https://ibeautycity.com/t/categories/hair-care?page=361'];
        $array41=['https://ibeautycity.com/t/categories/hair-care?page=391'];
        $array42=['https://ibeautycity.com/t/categories/hair-care?page=421'];
        $array43=['https://ibeautycity.com/t/categories/hair-care?page=451'];
        $array44=['https://ibeautycity.com/t/categories/hair-care?page=471'];
        $array45=['https://ibeautycity.com/t/categories/fragrance'];
        $array46=['https://ibeautycity.com/t/categories/fragrance?page=31'];
        $array47=['https://ibeautycity.com/t/categories/fragrance?page=61'];
        $array48=['https://ibeautycity.com/t/categories/fragrance?page=78'];
        $array49=['https://ibeautycity.com/t/categories/tools-and-accessories'];
        $array50=['https://ibeautycity.com/t/categories/tools-and-accessories?page=31'];
        $array51=['https://ibeautycity.com/t/categories/tools-and-accessories?page=61'];
        $array52=['https://ibeautycity.com/t/categories/tools-and-accessories?page=91'];
        $array53=['https://ibeautycity.com/t/categories/tools-and-accessories?page=121'];
        $array54=['https://ibeautycity.com/t/categories/tools-and-accessories?page=151'];
        $array55=['https://ibeautycity.com/t/categories/tools-and-accessories?page=181'];
        $array56=['https://ibeautycity.com/t/categories/tools-and-accessories?page=211'];
        $array57=['https://ibeautycity.com/t/categories/tools-and-accessories?page=241'];
        $array58=['https://ibeautycity.com/t/categories/tools-and-accessories?page=251'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'ibeauty001.seo079.site'],
            'x2'=>['source'=>$array2,'target'=>'ibeauty002.seo080.site'],
            'x3'=>['source'=>$array3,'target'=>'ibeauty003.seo079.site'],
            'x4'=>['source'=>$array4,'target'=>'ibeauty004.seo080.site'],
            'x5'=>['source'=>$array5,'target'=>'ibeauty005.seo079.site'],
            'x6'=>['source'=>$array6,'target'=>'ibeauty006.seo080.site'],
            'x7'=>['source'=>$array7,'target'=>'ibeauty007.seo079.site'],
            'x8'=>['source'=>$array8,'target'=>'ibeauty008.seo080.site'],
            'x9'=>['source'=>$array9,'target'=>'ibeauty009.seo079.site'],
            'x10'=>['source'=>$array10,'target'=>'ibeauty010.seo080.site'],
            'x11'=>['source'=>$array11,'target'=>'ibeauty011.seo079.site'],
            'x12'=>['source'=>$array12,'target'=>'ibeauty012.seo080.site'],
            'x13'=>['source'=>$array13,'target'=>'ibeauty013.seo079.site'],
            'x14'=>['source'=>$array14,'target'=>'ibeauty014.seo080.site'],
            'x15'=>['source'=>$array15,'target'=>'ibeauty015.seo079.site'],
            'x16'=>['source'=>$array16,'target'=>'ibeauty016.seo080.site'],
            'x17'=>['source'=>$array17,'target'=>'ibeauty017.seo079.site'],
            'x18'=>['source'=>$array18,'target'=>'ibeauty018.seo080.site'],
            'x19'=>['source'=>$array19,'target'=>'ibeauty019.seo079.site'],
            'x20'=>['source'=>$array20,'target'=>'ibeauty020.seo080.site'],
            'x21'=>['source'=>$array21,'target'=>'ibeauty021.seo079.site'],
            'x22'=>['source'=>$array22,'target'=>'ibeauty022.seo080.site'],
            'x23'=>['source'=>$array23,'target'=>'ibeauty023.seo079.site'],
            'x24'=>['source'=>$array24,'target'=>'ibeauty024.seo080.site'],
            'x25'=>['source'=>$array25,'target'=>'ibeauty025.seo079.site'],
            'x26'=>['source'=>$array26,'target'=>'ibeauty026.seo080.site'],
            'x27'=>['source'=>$array27,'target'=>'ibeauty027.seo079.site'],
            'x28'=>['source'=>$array28,'target'=>'ibeauty028.seo080.site'],
            'x29'=>['source'=>$array29,'target'=>'ibeauty029.seo079.site'],
            'x30'=>['source'=>$array30,'target'=>'ibeauty030.seo080.site'],
            'x31'=>['source'=>$array31,'target'=>'ibeauty031.seo079.site'],
            'x32'=>['source'=>$array32,'target'=>'ibeauty032.seo080.site'],
            'x33'=>['source'=>$array33,'target'=>'ibeauty033.seo081.site'],
            'x34'=>['source'=>$array34,'target'=>'ibeauty034.seo080.site'],
            'x35'=>['source'=>$array35,'target'=>'ibeauty035.seo081.site'],
            'x36'=>['source'=>$array36,'target'=>'ibeauty036.seo080.site'],
            'x37'=>['source'=>$array37,'target'=>'ibeauty037.seo081.site'],
            'x38'=>['source'=>$array38,'target'=>'ibeauty038.seo080.site'],
            'x39'=>['source'=>$array39,'target'=>'ibeauty039.seo081.site'],
            'x40'=>['source'=>$array40,'target'=>'ibeauty040.seo080.site'],
            'x41'=>['source'=>$array41,'target'=>'ibeauty041.seo081.site'],
            'x42'=>['source'=>$array42,'target'=>'ibeauty042.seo080.site'],
            'x43'=>['source'=>$array43,'target'=>'ibeauty043.seo081.site'],
            'x44'=>['source'=>$array44,'target'=>'ibeauty044.seo080.site'],
            'x45'=>['source'=>$array45,'target'=>'ibeauty045.seo081.site'],
            'x46'=>['source'=>$array46,'target'=>'ibeauty046.seo080.site'],
            'x47'=>['source'=>$array47,'target'=>'ibeauty047.seo081.site'],
            'x48'=>['source'=>$array48,'target'=>'ibeauty048.seo080.site'],
            'x49'=>['source'=>$array49,'target'=>'ibeauty049.seo081.site'],
            'x50'=>['source'=>$array50,'target'=>'ibeauty050.seo082.site'],
            'x51'=>['source'=>$array51,'target'=>'ibeauty051.seo081.site'],
            'x52'=>['source'=>$array52,'target'=>'ibeauty052.seo082.site'],
            'x53'=>['source'=>$array53,'target'=>'ibeauty053.seo081.site'],
            'x54'=>['source'=>$array54,'target'=>'ibeauty054.seo082.site'],
            'x55'=>['source'=>$array55,'target'=>'ibeauty055.seo081.site'],
            'x56'=>['source'=>$array56,'target'=>'ibeauty056.seo082.site'],
            'x57'=>['source'=>$array57,'target'=>'ibeauty057.seo081.site'],
            'x58'=>['source'=>$array58,'target'=>'ibeauty058.seo082.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://ibeautycity.com/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');
        // echo $nextNode->attr("href");exit;
        if ($nextNode->count()) {
            $this->processPage("https://ibeautycity.com".$nextNode->attr('href'));               
        }
    }

    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.container .row>div>a')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->filter('.product-component-name')->text(),
                    'url'  => sprintf('%s','https://ibeautycity.com'.$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url'] = 'https://ibeautycity.com/products/jolie-super-hydrating-luxury-lip-gloss-intense-pigment-w-superior-shine-nude-nature?taxon_id=5';
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $json = [];
        $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node,$i)use(&$json){
            if(strstr($node->text(),'sku')){
                $json[] = $node->text();
            }
        });
        // print_r(json_decode($json[0],true));exit;
        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];          #关键词
        $product['gender'] = '';            #性别
        $product['color'] = '';             #颜色
        $product['subCategory'] = ' ';      #亚类别
        $product['variations']=[];          #选项
        $product['description'] = '';       #描述
        $product['short_description'] = ''; #顶部菜单栏
        $product['attributes']=[];          #规格
        //SKU----------------------------------------------------------------------------------------------------------------
        $product['sku'] = json_decode($json[0],true)['sku'];
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = json_decode($json[0],true)['name'];
        // print_r($product['title']);exit;
        // 价格
        $product['price'] = json_decode($json[0],true)['offers']['price'];
        // print_r($product['price']).exit;
        //品牌
        if($crawler->filter('.d-md-block .product-rating')->text() != null ){
            $product['brand'] = str_replace("brand: ","",$crawler->filter('.d-md-block .product-rating')->text());
        }else{
            $product['brand'] = "IBeauty";
        }
        // print_r($product['brand']);exit;
        //类型
        $product['type'] = 'simple';
        //id
        $product['id'] = "";
        
        //图片---------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.product-thumbnails-carousel-item-content img')->count()>1){
            $images = $crawler->filter('.product-thumbnails-carousel-item-content img')->each(function(Crawler $node,$i){
                return $node->attr('data-src');
            });
        }else{
            $images[] = $crawler->filter('.carousel-inner .carousel-item img')->attr('data-src');
        }
        foreach(array_unique($images) as $image){
            $product['images'][]=[
                'name'=>$product['title'],
                'src'=>str_replace("width=100&height=100","width=650&height=650",$image),
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        //子分类--------------------------------------------------------------------------------------------------------------
        $breadcrumbs = $crawler->filter('[id="breadcrumbs"] a')->each(function(Crawler $node,$i){
           return $node->text();
        });
        $breadcrumbs = array_slice($breadcrumbs,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb,$parentCategory,$parent);
        }
        //标签---------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        array_push($tags,$product['brand']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        //尺寸选项------------------------------------------------------------------------------------------------------------
        // print_r($product['variations']);exit;
        //产品描述----------------------------------------------------------------------------------------------------------------
        // $product['short_description'] ='';
        // $product['description'] = '';
        if($crawler->filter('[id="description"]')->count()){
            $product['description'] = $crawler->filter('[id="description"]')->html();
        }
        // print_r($product['description']);exit;
        //规格
        if($crawler->filter('.table-responsive-sm .bolderless')->eq(0)->filter('tr')->count()){
            $num_attributes = $crawler->filter('.table-responsive-sm .bolderless')->eq(0)->filter('tr')->count();
            $name_attributes = [];
            $option_attributes = [];
            for($i=0;$i<$num_attributes;$i++){
                $product['attributes'][]=[
                    'name' => $crawler->filter('.table-responsive-sm .bolderless')->eq(0)->filter('tr')->eq($i)->filter('.font-weight-bold')->text(),
                    'options' => [$crawler->filter('.table-responsive-sm .bolderless')->eq(0)->filter('tr')->eq($i)->filter('.text-break')->text()],
                ];
            }
        }
        // print_r($product['attributes']);exit;
        // --------------------------------------------------------------------------------------------------------------------------
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // exit();
        echo "\r\n";
        echo "Next One".PHP_EOL;
    }
    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }
    protected function generateName($product): string
    {
        $name = $product['title'];
        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';
        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;
        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   