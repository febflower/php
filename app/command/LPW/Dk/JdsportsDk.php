<?php         //https://www.jdsports.dk/
namespace app\command\LPW\Dk;

//调用模块
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use Exception;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class JdsportsDk extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:-------------
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:jdsportsdk')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://www.jdsports.dk/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接   //?from=
        $array1=['https://www.jdsports.dk/herrer/herresko/'];
        $array2=['https://www.jdsports.dk/herrer/herretoj/'];
        $array3=['https://www.jdsports.dk/herrer/herretoj/?from=2232'];
        $array4=['https://www.jdsports.dk/damer/damesko/'];
        $array5=['https://www.jdsports.dk/damer/dametoj/'];
        $array6=['https://www.jdsports.dk/born/c/sko/'];
        $array7=['https://www.jdsports.dk/born/latest/gender/piger,girls/c/tj/'];
        $array8=['https://www.jdsports.dk/born/c/tj/gender/drenge,boys/'];
        $array9=['https://www.jdsports.dk/campaign/Premier+League/?facet-football-league=premier-league','https://www.jdsports.dk/campaign/European+Leagues/?facet-football-league=bundesliga%2Cla-liga%2Cserie-a%2Cligue-1,eredivisie','https://www.jdsports.dk/campaign/International+Football+Teams/?facet-football-league=international-teams'];
        $array10=['https://www.jdsports.dk/campaign/Football+Essentials/?facet-category=tilbehr&facet-sport=football','https://www.jdsports.dk/p/fodboldstovler/','https://www.jdsports.dk/sport/lb/'];
        $array11=['https://www.jdsports.dk/sport/gym,trning/','https://www.jdsports.dk/sport/american-football/','https://www.jdsports.dk/sport/basketball/','https://www.jdsports.dk/sport/baseball/','https://www.jdsports.dk/sport/boxing/','https://www.jdsports.dk/sport/mma/','https://www.jdsports.dk/sport/rugby/','https://www.jdsports.dk/sport/cricket/','https://www.jdsports.dk/sport/hockey/','https://www.jdsports.dk/sport/motorsport/','https://www.jdsports.dk/sport/netball/'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'jdsports001.xms006.site'],
            'x2'=>['source'=>$array2,'target'=>'jdsports002.xms006.site'],
            'x3'=>['source'=>$array3,'target'=>'jdsports003.xms006.site'],
            'x4'=>['source'=>$array4,'target'=>'jdsports004.xms006.site'],
            'x5'=>['source'=>$array5,'target'=>'jdsports005.xms006.site'],
            'x6'=>['source'=>$array6,'target'=>'jdsports006.xms006.site'],
            'x7'=>['source'=>$array7,'target'=>'jdsports007.xms006.site'],
            'x8'=>['source'=>$array8,'target'=>'jdsports008.xms006.site'],
            'x9'=>['source'=>$array9,'target'=>'jdsports009.xms006.site'],
            'x10'=>['source'=>$array10,'target'=>'jdsports010.xms006.site'],
            'x11'=>['source'=>$array11,'target'=>'jdsports011.xms006.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',    #用了VPN可以屏蔽此段
            'base_uri'=>'https://www.jdsports.dk/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb.com
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  <--daogecvb.com
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }

    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]')->last();
        // echo $nextNode->attr("href");exit;
        if ($nextNode->count()) {
            $this->processPage('https://www.jdsports.dk'.$nextNode->attr('href'));  
        }
        // echo $nextNode->text();exit;
    }


    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('.itemTitle a')->each(function(Crawler $node,$i){
                try{
                    $this->discount = rand(65,80)/100;  //rand(65,80)/100;
                    // $request = json_encode($node, JSON_UNESCAPED_UNICODE);
                    $this->crawlerProduct([
                        'name'=> "[{$i}]".$node->text(),
                        'url'=> sprintf('%s',"https://www.jdsports.dk".$node->filter('a')->attr('href')),
                    ]);
                }catch(\Exception $exception){
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
                }   
        });
    }

    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他---------------------------------------------------------------------------------------------------------------------------------------------------------------------
        //关键词
        $product['keywords'] = [];
        //性别
        $product['gender'] = '';
        //颜色
        $product['color'] = "";
        if(json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['color'] != null){
            $product['color'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['color'];            
        }
        // print_r($product['color']);exit;
        //亚类
        $product['subCategory'] = ' ';

        //SKU
        $product['sku'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['id'];
        // print_r($product['sku']);exit;
        // 标题
        $product['title'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['name'];
        // print_r($product['title']);exit;
        //价格
        $product['price'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['offers']['price']*0.1344;
        // print_r($product['price']);exit;
        // 品牌
        $product['brand'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['brand']['name'];
        // print_r($product['brand']);exit;

        //ID
        $product['id'] = '';
        // print_r($product['id']);exit;
        //
        $product['type'] = 'simple';
        //图片-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $images = json_decode($crawler->filter('[type="application/ld+json"]')->eq(0)->text(),true)['image'];
        // print_r($images);exit;
        // print_r($data);exit;
        foreach ($images as $image) {
            // echo $image."\r\n";
            $product['images'][] = [
                'name' => $product['title'],
                'src' => $image,
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        //分类/子分类-------------------------------------------------------------------------------------------------------------------------------------------------------------------
        for($i=0;$i<count(json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['itemListElement']);$i++){
            $breadcrumbs[] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['itemListElement'][$i]['name'];
        }
        // print_r($breadcrumbs);exit;
        $breadcrumbs = array_slice($breadcrumbs,0,-1);
        // print_r($breadcrumbs);exit;
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;

        //标签--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        // print_r($tags);exit;
        array_push($tags,$product['brand'],$product['color']);
        // print_r($tags);exit;
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;

        //选项--------------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['variations']= [];
        $variations = "";
        $crawler->filter('[type="text/javascript"]')->each(function(Crawler $node,$i)use(&$variations,&$product){
            if(strstr($node->text(),"var dataObject")){
                $variations = $node->text();
                $variations_s = str_replace('", upc:','{ name:"',explode('{ name:"',$variations));
                    for($i=1;$i<count($variations_s);$i++){
                        $arr[$i-1]=explode('{ name:"',$variations_s[$i])[0];
                    }
                    $product['variations'] [] =[
                        'name'=>"Size",
                        'options'=> $arr,
                    ];
                    $product['type'] = 'variable';
            }
        });
        // print_r($product['variations']);exit; 
        
        //顶部菜单栏
        $product['short_description']="";
        // print_r($product['short_description']);exit;
        //产品描述-----------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['description'] = ""; 
        if($crawler->filter('[id="itemInfoContainer"]')->filter('.tab-info')->count()){
            $product['description'] = str_replace(" to JD","",str_replace(" hos JD","",str_replace("JD-","",str_replace("jdsportsdk","sports",$crawler->filter('[id="itemInfoContainer"]')->filter('.tab-info')->html()))));
        }
        // print_r($product['description']);exit;
        // 产品规格---------------------------------------------------------------------------------------------------------------------------------------------------------------
        $product['attributes'] = [];
        // print_r($product['attributes']);exit;

                // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "Nextone".PHP_EOL;
    }
    
    //图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   
