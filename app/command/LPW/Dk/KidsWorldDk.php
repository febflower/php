<?php      //https://www.kids-world.dk/
namespace app\command\LPW\Dk;
                                                                                    
//调用模块
use app\command\BuildCommon;
use app\command\MSK\En\ColoradoskishopCOM;
use Attribute;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class KidsWorldDk extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:kidsworlddk')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源//https://www.kids-world.dk/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接  //?page=2
        $array1=['https://www.kids-world.dk/drengetoej-c-22.html'];
        $array2=['https://www.kids-world.dk/drengetoej-c-22.html?page=21'];
        $array3=['https://www.kids-world.dk/drengetoej-c-22.html?page=41'];
        $array4=['https://www.kids-world.dk/drengetoej-c-22.html?page=61'];
        $array5=['https://www.kids-world.dk/drengetoej-c-22.html?page=81'];
        $array6=['https://www.kids-world.dk/drengetoej-c-22.html?page=101'];
        $array7=['https://www.kids-world.dk/drengetoej-c-22.html?page=121'];
        $array8=['https://www.kids-world.dk/drengetoej-c-22.html?page=141'];
        $array9=['https://www.kids-world.dk/drengetoej-c-22.html?page=161'];
        $array10=['https://www.kids-world.dk/drengetoej-c-22.html?page=181'];
        $array11=['https://www.kids-world.dk/drengetoej-c-22.html?page=201'];
        $array12=['https://www.kids-world.dk/drengetoej-c-22.html?page=221'];
        $array13=['https://www.kids-world.dk/drengetoej-c-22.html?page=241'];
        $array14=['https://www.kids-world.dk/drengetoej-c-22.html?page=261'];
        $array15=['https://www.kids-world.dk/drengetoej-c-22.html?page=281'];
        $array16=['https://www.kids-world.dk/drengetoej-c-22.html?page=301'];
        $array17=['https://www.kids-world.dk/drengetoej-c-22.html?page=321'];
        $array18=['https://www.kids-world.dk/drengetoej-c-22.html?page=341'];
        $array19=['https://www.kids-world.dk/drengetoej-c-22.html?page=361'];
        $array20=['https://www.kids-world.dk/drengetoej-c-22.html?page=381'];
        $array21=['https://www.kids-world.dk/drengetoej-c-22.html?page=401'];
        $array22=['https://www.kids-world.dk/drengetoej-c-22.html?page=407'];
        $array23=['https://www.kids-world.dk/pigetoej-c-21.html'];
        $array24=['https://www.kids-world.dk/pigetoej-c-21.html?page=21'];
        $array25=['https://www.kids-world.dk/pigetoej-c-21.html?page=41'];
        $array26=['https://www.kids-world.dk/pigetoej-c-21.html?page=61'];
        $array27=['https://www.kids-world.dk/pigetoej-c-21.html?page=81'];
        $array28=['https://www.kids-world.dk/pigetoej-c-21.html?page=101'];
        $array29=['https://www.kids-world.dk/pigetoej-c-21.html?page=121'];
        $array30=['https://www.kids-world.dk/pigetoej-c-21.html?page=141'];
        $array31=['https://www.kids-world.dk/pigetoej-c-21.html?page=161'];
        $array32=['https://www.kids-world.dk/pigetoej-c-21.html?page=181'];
        $array33=['https://www.kids-world.dk/pigetoej-c-21.html?page=201'];
        $array34=['https://www.kids-world.dk/pigetoej-c-21.html?page=221'];
        $array35=['https://www.kids-world.dk/pigetoej-c-21.html?page=241'];
        $array36=['https://www.kids-world.dk/pigetoej-c-21.html?page=261'];
        $array37=['https://www.kids-world.dk/pigetoej-c-21.html?page=281'];
        $array38=['https://www.kids-world.dk/pigetoej-c-21.html?page=301'];
        $array39=['https://www.kids-world.dk/pigetoej-c-21.html?page=321'];
        $array40=['https://www.kids-world.dk/pigetoej-c-21.html?page=341'];
        $array41=['https://www.kids-world.dk/pigetoej-c-21.html?page=361'];
        $array42=['https://www.kids-world.dk/pigetoej-c-21.html?page=381'];
        $array43=['https://www.kids-world.dk/pigetoej-c-21.html?page=401'];
        $array44=['https://www.kids-world.dk/pigetoej-c-21.html?page=421'];
        $array45=['https://www.kids-world.dk/pigetoej-c-21.html?page=441'];
        $array46=['https://www.kids-world.dk/pigetoej-c-21.html?page=461'];
        $array47=['https://www.kids-world.dk/pigetoej-c-21.html?page=481'];
        $array48=['https://www.kids-world.dk/pigetoej-c-21.html?page=501'];
        $array49=['https://www.kids-world.dk/pigetoej-c-21.html?page=521'];
        $array50=['https://www.kids-world.dk/pigetoej-c-21.html?page=541'];
        $array51=['https://www.kids-world.dk/pigetoej-c-21.html?page=561'];
        $array52=['https://www.kids-world.dk/unisextoej-c-94.html'];
        $array53=['https://www.kids-world.dk/unisextoej-c-94.html?page=21'];
        $array54=['https://www.kids-world.dk/unisextoej-c-94.html?page=41'];
        $array55=['https://www.kids-world.dk/unisextoej-c-94.html?page=61'];
        $array56=['https://www.kids-world.dk/unisextoej-c-94.html?page=81'];
        $array57=['https://www.kids-world.dk/unisextoej-c-94.html?page=101'];
        $array58=['https://www.kids-world.dk/unisextoej-c-94.html?page=121'];
        $array59=['https://www.kids-world.dk/unisextoej-c-94.html?page=141'];
        $array60=['https://www.kids-world.dk/unisextoej-c-94.html?page=161'];
        $array61=['https://www.kids-world.dk/basis-c-3228.html'];
        $array62=['https://www.kids-world.dk/basis-c-3228.html?page=21'];
        $array63=['https://www.kids-world.dk/basis-c-3228.html?page=41'];
        $array64=['https://www.kids-world.dk/basis-c-3228.html?page=61'];
        $array65=['https://www.kids-world.dk/sko-c-3226_2313.html'];
        $array66=['https://www.kids-world.dk/sko-c-3226_2313.html?page=21'];
        $array67=['https://www.kids-world.dk/sko-c-3226_2313.html?page=41'];
        $array68=['https://www.kids-world.dk/sko-c-3226_2313.html?page=61'];
        $array69=['https://www.kids-world.dk/snorkling-c-8922.html','https://www.kids-world.dk/svoemning-c-7943.html'];
        $array70=['https://www.kids-world.dk/fodbold-c-12236.html','https://www.kids-world.dk/skate-c-13081.html','https://www.kids-world.dk/traening-gymnastik-c-13082.html'];
        $array71=['https://www.kids-world.dk/uld-c-3226_237.html'];
        $array72=['https://www.kids-world.dk/uld-c-3226_237.html?page=21'];
        $array73=['https://www.kids-world.dk/uld-c-3226_237.html?page=41'];
        $array74=['https://www.kids-world.dk/uld-c-3226_237.html?page=61'];
        $array75=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html'];
        $array76=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=21'];
        $array77=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=41'];
        $array78=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=61'];
        $array79=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=81'];
        $array80=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=101'];
        $array81=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=121'];
        $array82=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=141'];
        $array83=['https://www.kids-world.dk/tilbehoer-c-3226_3233.html?page=161'];
        $array84=['https://www.kids-world.dk/udstyr-c-3226_3227.html'];
        $array85=['https://www.kids-world.dk/udstyr-c-3226_3227.html?page=21'];
        $array86=['https://www.kids-world.dk/udstyr-c-3226_3227.html?page=41'];
        $array87=['https://www.kids-world.dk/udstyr-c-3226_3227.html?page=61'];
        $array88=['https://www.kids-world.dk/udstyr-c-3226_3227.html?page=81'];
        $array89=['https://www.kids-world.dk/udstyr-c-3226_3227.html?page=101'];
        $array90=['https://www.kids-world.dk/udstyr-c-3226_3227.html?page=121'];
        $array91=['https://www.kids-world.dk/indretning-c-3226_3230.html'];
        $array92=['https://www.kids-world.dk/indretning-c-3226_3230.html?page=21'];
        $array93=['https://www.kids-world.dk/indretning-c-3226_3230.html?page=41'];
        $array94=['https://www.kids-world.dk/indretning-c-3226_3230.html?page=61'];
        $array95=['https://www.kids-world.dk/legetoej-c-3226_123.html'];
        $array96=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=21'];
        $array97=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=41'];
        $array98=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=61'];
        $array99=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=81'];
        $array100=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=101'];
        $array101=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=121'];
        $array102=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=141'];
        $array103=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=161'];
        $array104=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=181'];
        $array105=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=201'];
        $array106=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=221'];
        $array107=['https://www.kids-world.dk/legetoej-c-3226_123.html?page=241'];
        $array108=['https://www.kids-world.dk/sko-c-3226_2313.html?page=81'];

        $this->sites=[
            'x'=>['source'=>$array1,'target'=>'daogecvb.com'],
            'x1'=>['source'=>$array1,'target'=>'kidsworld001.seo079.site'],
            'x2'=>['source'=>$array2,'target'=>'kidsworld002.seo080.site'],
            'x3'=>['source'=>$array3,'target'=>'kidsworld003.seo079.site'],
            'x4'=>['source'=>$array4,'target'=>'kidsworld004.seo080.site'],
            'x5'=>['source'=>$array5,'target'=>'kidsworld005.seo079.site'],
            'x6'=>['source'=>$array6,'target'=>'kidsworld006.seo080.site'],
            'x7'=>['source'=>$array7,'target'=>'kidsworld007.seo079.site'],
            'x8'=>['source'=>$array8,'target'=>'kidsworld008.seo080.site'],
            'x9'=>['source'=>$array9,'target'=>'kidsworld009.seo079.site'],
            'x10'=>['source'=>$array10,'target'=>'kidsworld010.seo080.site'],
            'x11'=>['source'=>$array11,'target'=>'kidsworld011.seo079.site'],
            'x12'=>['source'=>$array12,'target'=>'kidsworld012.seo080.site'],
            'x13'=>['source'=>$array13,'target'=>'kidsworld013.seo079.site'],
            'x14'=>['source'=>$array14,'target'=>'kidsworld014.seo080.site'],
            'x15'=>['source'=>$array15,'target'=>'kidsworld015.seo079.site'],
            'x16'=>['source'=>$array16,'target'=>'kidsworld016.seo080.site'],
            'x17'=>['source'=>$array17,'target'=>'kidsworld017.seo079.site'],
            'x18'=>['source'=>$array18,'target'=>'kidsworld018.seo080.site'],
            'x19'=>['source'=>$array19,'target'=>'kidsworld019.seo079.site'],
            'x20'=>['source'=>$array20,'target'=>'kidsworld020.seo080.site'],
            'x21'=>['source'=>$array21,'target'=>'kidsworld021.seo079.site'],
            'x22'=>['source'=>$array22,'target'=>'kidsworld022.seo080.site'],
            'x23'=>['source'=>$array23,'target'=>'kidsworld023.seo079.site'],
            'x24'=>['source'=>$array24,'target'=>'kidsworld024.seo080.site'],
            'x25'=>['source'=>$array25,'target'=>'kidsworld025.seo079.site'],
            'x26'=>['source'=>$array26,'target'=>'kidsworld026.seo080.site'],
            'x27'=>['source'=>$array27,'target'=>'kidsworld027.seo081.site'],
            'x28'=>['source'=>$array28,'target'=>'kidsworld028.seo080.site'],
            'x29'=>['source'=>$array29,'target'=>'kidsworld029.seo081.site'],
            'x30'=>['source'=>$array30,'target'=>'kidsworld030.seo080.site'],
            'x31'=>['source'=>$array31,'target'=>'kidsworld031.seo081.site'],
            'x32'=>['source'=>$array32,'target'=>'kidsworld032.seo082.site'],
            'x33'=>['source'=>$array33,'target'=>'kidsworld033.seo081.site'],
            'x34'=>['source'=>$array34,'target'=>'kidsworld034.seo082.site'],
            'x35'=>['source'=>$array35,'target'=>'kidsworld035.seo081.site'],
            'x36'=>['source'=>$array36,'target'=>'kidsworld036.seo082.site'],
            'x37'=>['source'=>$array37,'target'=>'kidsworld037.seo081.site'],
            'x38'=>['source'=>$array38,'target'=>'kidsworld038.seo082.site'],
            'x39'=>['source'=>$array39,'target'=>'kidsworld039.seo081.site'],
            'x40'=>['source'=>$array40,'target'=>'kidsworld040.seo082.site'],
            'x41'=>['source'=>$array41,'target'=>'kidsworld041.seo081.site'],
            'x42'=>['source'=>$array42,'target'=>'kidsworld042.seo082.site'],
            'x43'=>['source'=>$array43,'target'=>'kidsworld043.seo081.site'],
            'x44'=>['source'=>$array44,'target'=>'kidsworld044.seo082.site'],
            'x45'=>['source'=>$array45,'target'=>'kidsworld045.seo081.site'],
            'x46'=>['source'=>$array46,'target'=>'kidsworld046.seo082.site'],
            'x47'=>['source'=>$array47,'target'=>'kidsworld047.seo081.site'],
            'x48'=>['source'=>$array48,'target'=>'kidsworld048.seo082.site'],
            'x49'=>['source'=>$array49,'target'=>'kidsworld049.seo081.site'],
            'x50'=>['source'=>$array50,'target'=>'kidsworld050.seo082.site'],
            'x51'=>['source'=>$array51,'target'=>'kidsworld051.seo081.site'],
            'x52'=>['source'=>$array52,'target'=>'kidsworld052.seo082.site'],
            'x53'=>['source'=>$array53,'target'=>'kidsworld053.seo081.site'],
            'x54'=>['source'=>$array54,'target'=>'kidsworld054.seo082.site'],
            'x55'=>['source'=>$array55,'target'=>'kidsworld055.seo081.site'],
            'x56'=>['source'=>$array56,'target'=>'kidsworld056.seo082.site'],
            'x57'=>['source'=>$array57,'target'=>'kidsworld057.seo081.site'],
            'x58'=>['source'=>$array58,'target'=>'kidsworld058.seo082.site'],
            'x59'=>['source'=>$array59,'target'=>'kidsworld059.seo081.site'],
            'x60'=>['source'=>$array60,'target'=>'kidsworld060.seo082.site'],
            'x61'=>['source'=>$array61,'target'=>'kidsworld061.seo081.site'],
            'x62'=>['source'=>$array62,'target'=>'kidsworld062.seo082.site'],
            'x63'=>['source'=>$array63,'target'=>'kidsworld063.seo081.site'],
            'x64'=>['source'=>$array64,'target'=>'kidsworld064.seo082.site'],
            'x65'=>['source'=>$array65,'target'=>'kidsworld065.seo081.site'],
            'x66'=>['source'=>$array66,'target'=>'kidsworld066.seo082.site'],
            'x67'=>['source'=>$array67,'target'=>'kidsworld067.seo081.site'],
            'x68'=>['source'=>$array68,'target'=>'kidsworld068.seo082.site'],
            'x69'=>['source'=>$array69,'target'=>'kidsworld069.seo081.site'],
            'x70'=>['source'=>$array70,'target'=>'kidsworld070.seo082.site'],
            'x71'=>['source'=>$array71,'target'=>'kidsworld071.seo081.site'],
            'x72'=>['source'=>$array72,'target'=>'kidsworld072.seo082.site'],
            'x73'=>['source'=>$array73,'target'=>'kidsworld073.seo081.site'],
            'x74'=>['source'=>$array74,'target'=>'kidsworld074.seo082.site'],
            'x75'=>['source'=>$array75,'target'=>'kidsworld075.seo081.site'],
            'x76'=>['source'=>$array76,'target'=>'kidsworld076.seo082.site'],
            'x77'=>['source'=>$array77,'target'=>'kidsworld077.seo081.site'],
            'x78'=>['source'=>$array78,'target'=>'kidsworld078.seo082.site'],
            'x79'=>['source'=>$array79,'target'=>'kidsworld079.seo081.site'],
            'x80'=>['source'=>$array80,'target'=>'kidsworld080.seo082.site'],
            'x81'=>['source'=>$array81,'target'=>'kidsworld081.seo081.site'],
            'x82'=>['source'=>$array82,'target'=>'kidsworld082.seo082.site'],
            'x83'=>['source'=>$array83,'target'=>'kidsworld083.seo081.site'],
            'x84'=>['source'=>$array84,'target'=>'kidsworld084.seo082.site'],
            'x85'=>['source'=>$array85,'target'=>'kidsworld085.seo081.site'],
            'x86'=>['source'=>$array86,'target'=>'kidsworld086.seo082.site'],
            'x87'=>['source'=>$array87,'target'=>'kidsworld087.seo081.site'],
            'x88'=>['source'=>$array88,'target'=>'kidsworld088.seo082.site'],
            'x89'=>['source'=>$array89,'target'=>'kidsworld089.seo081.site'],
            'x90'=>['source'=>$array90,'target'=>'kidsworld090.seo082.site'],
            'x91'=>['source'=>$array91,'target'=>'kidsworld091.seo081.site'],
            'x92'=>['source'=>$array92,'target'=>'kidsworld092.seo082.site'],
            'x93'=>['source'=>$array93,'target'=>'kidsworld093.seo081.site'],
            'x94'=>['source'=>$array94,'target'=>'kidsworld094.seo082.site'],
            'x95'=>['source'=>$array95,'target'=>'kidsworld095.seo081.site'],
            'x96'=>['source'=>$array96,'target'=>'kidsworld096.seo082.site'],
            'x97'=>['source'=>$array97,'target'=>'kidsworld097.seo081.site'],
            'x98'=>['source'=>$array98,'target'=>'kidsworld098.seo082.site'],
            'x99'=>['source'=>$array99,'target'=>'kidsworld099.seo081.site'],
            'x100'=>['source'=>$array100,'target'=>'kidsworld100.seo082.site'],
            'x101'=>['source'=>$array101,'target'=>'kidsworld101.seo081.site'],
            'x102'=>['source'=>$array102,'target'=>'kidsworld102.seo082.site'],
            'x103'=>['source'=>$array103,'target'=>'kidsworld103.seo081.site'],
            'x104'=>['source'=>$array104,'target'=>'kidsworld104.seo082.site'],
            'x105'=>['source'=>$array105,'target'=>'kidsworld105.seo081.site'],
            'x106'=>['source'=>$array106,'target'=>'kidsworld106.seo082.site'],
            'x107'=>['source'=>$array107,'target'=>'kidsworld107.seo081.site'],
            'x108'=>['source'=>$array108,'target'=>'kidsworld108.seo082.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://www.kids-world.dk/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618', //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618   //ck_4c03d8dc728cdfb15cde17355e2c5da5e8caf9b1  <--daogecvb
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',//cs_3ef17880456fe03098e0fb0c347869de458fc8ba   //cs_ce714dc6416970b978ccf497c666a8f3185179a5  
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');
        // echo $nextNode->attr("href");exit;
        if ($nextNode->count()) {
            $this->processPage("https://www.kids-world.dk".$nextNode->attr('href'));               
        }
    }

    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        $crawler->filter('[id="products"] .product .title')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->text(),
                    'url'  => sprintf('%s',"https://www.kids-world.dk".$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }
    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        // $item['url'] = 'https://www.kids-world.dk/name-it-stroemper-nkmvaks-5pak-grey-melange-p-287621.html';
        $this->output->info($item['name'].' >>> '.$item['url']);
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        // print_r(json_decode($json[0],true));exit;
        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];          #关键词
        $product['gender'] = '';            #性别
        $product['color'] = '';             #颜色
        $product['subCategory'] = ' ';      #亚类别
        $product['variations']=[];          #选项
        $product['description'] = '';       #描述
        $product['short_description'] = ''; #顶部菜单栏
        $product['attributes']=[];          #规格
        //SKU----------------------------------------------------------------------------------------------------------------
        $product['sku'] = $crawler->filter('[name="product_model"]')->attr('value');
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = $crawler->filter('[name="name"]')->attr('value');
        // print_r($product['title']);exit
        // 价格
        $product['price'] = $crawler->filter('[name="price_incl_vat"]')->attr('value')*0.14;
        // print_r($product['price']);//exit;
        //品牌
        $product['brand'] = $crawler->filter('[name="manufacturer"]')->attr('value');
        // print_r($product['brand']);exit;
        //类型
        $product['type'] = 'simple';
        //id
        $product['id'] = "";
        
        //图片---------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.image-container .main-image-container img')->count()){
            $images = $crawler->filter('.image-container .main-image-container img')->each(function(Crawler $node,$i){
                return $node->attr('src');
            });
        }
        // print_r($images);exit;
        if(count($images)>0){
            foreach($images as $image){
                $product['images'][]=[
                    'name'=>$product['title'],
                    'src'=>"https://www.kids-world.dk/".$image,
                ];
            }
        }else{
            $product['images'][]=[
                'name'=>$product['title'],
                'src'=>"https://www.kids-world.dk/".$images,
            ];
        }
        // $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        //子分类--------------------------------------------------------------------------------------------------------------
        $breadcrumbs = $crawler->filter('.breadcrumb li')->each(function(Crawler $node,$i){
           return $node->text();
        });
        $new_breadcrumbs = explode(' - ',str_replace($product['brand'],"",$product['title']));
        $breadcrumbs = array_slice(array_filter($breadcrumbs),1);
        array_push($breadcrumbs,trim($new_breadcrumbs[0]));
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        //标签---------------------------------------------------------------------------------------------------------------
        $tags = explode(",",str_replace("Tags:","",$crawler->filter('[id="product_info_details"] .speakable-description .col-xs-6')->last()->text()));
        array_push($tags,$product['brand']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;
        //尺寸选项------------------------------------------------------------------------------------------------------------
        if($crawler->filter('.size-modal-overlay tbody')->count()){
            $variations = [];
            $crawler->filter('.size-modal-overlay tbody')->filter('tr')->each(function(Crawler $node,$i)use(&$variations,&$product){
                if(strstr($node->attr('data-text'),"One Size")){
                    $variations = [];
                }else{
                    $variations_s = explode("(",$node->attr('data-text'));
                    $variations[] = $variations_s[0];
                }
            });
            if($variations != null){
                $product['variations'][]=[
                    'name' => $crawler->filter('.size-modal-overlay thead')->filter('tr')->attr('data-text'),
                    'options' =>array_filter(array_unique($variations)),
                    ];
                    $product['type'] = 'variable';
            }
        }
        // print_r($product['variations']);exit;
        //产品描述----------------------------------------------------------------------------------------------------------------
        // $product['short_description'] ='';
        // $product['description'] = '';   //$crawler->filter('[id="product_info_details"] .speakable-description')->html()
        if($crawler->filter('[id="product_info_description"] .speakable-description')->count()){
            if(strstr($crawler->filter('[id="product_info_description"] .speakable-description')->text(),'kundeservice@kids-world.dk')){
                $product['description'] = $crawler->filter('[id="product_info_details"] .speakable-description')->html();
            }else{
                $product['description'] = $crawler->filter('[id="product_info_description"] .speakable-description')->html().$crawler->filter('[id="product_info_details"] .speakable-description')->html();
            }
        }
        // print_r($product['description']);exit;
        //规格
        // print_r($product['attributes']);exit;
        // --------------------------------------------------------------------------------------------------------------------------
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // exit();
        echo "\r\n";
        echo "Next One".PHP_EOL;
    }
    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }
    protected function generateName($product): string
    {
        $name = $product['title'];
        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';
        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;
        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';
        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   