<?php                            //https://kleman-france.com/
namespace app\command\LPW\Fr;

//调用模块
use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

//封装
class KlemanFranceCom extends Command
{
    use BuildCommon;
    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    //自定义命令行  build:wordpress:lpw:adexacouk
    protected function configure()
    {
        $this->setName('build:wordpress:lpw:klemanfrancecom')
             ->addOption('site','s',Option::VALUE_OPTIONAL,'站点')
             ->setDescription('创建Wordpress站点，数据源https://kleman-france.com/');
    }

    //
    protected function initialize(Input $input,Output $output)
    {   
        #添加$array>>>>>>>>>>>>爬取网页链接
        $array1 = ['https://kleman-france.com/fr/homme.html','https://kleman-france.com/fr/femme.html','https://kleman-france.com/fr/eco-responsable.html'];
        
        $this->sites=[
            'x1'=>['source'=>$array1,'target'=>'kleman001.seo069.site'],
        ];

        //添加请求头
        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' =>false,
            'verify'=>false,
            'cookies'=>$this->cookieJar,
            // 'proxy'=>'socks5h://127.0.0.1:7890',#用了VPN可以屏蔽此段
            'base_uri'=>'https://kleman-france.com/',
            'headers'=>['User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'],
        ]);

        //添加Wordpress网页域名api，以及密钥
        $this->woocommerce = new Client
        (
            'http://'.$this->processSite['target'],
        //     'wordpress网页域名api>>>密钥'
        'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',  //ck_eda5cc1c661c038a8aa02276b5c0d708fd135618
        'cs_3ef17880456fe03098e0fb0c347869de458fc8ba', //cs_3ef17880456fe03098e0fb0c347869de458fc8ba
            [
                'timeout' => 600,
                'wp_api' => true, //enable the WP REST API integration //启用WP REST API集成
                'version' => 'wc/v3',//WooCommerce WP REST API version //WooCommerce WP REST API版本
                'verify_ssl' =>false,
            ]
        );
    }

    //
    protected function execute(Input $input,Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source)
        {
          $this->processPage($source);  
        }
    }

    #爬取网页内容，返回getContents格式
    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);#

            $this->processProductList($crawler);

        //判断是否有翻页语句
        $nextNode = $crawler->filter('[rel="next"]');  ///javascript: void(0)翻页链接格式不对
            if($nextNode->count()){
                $this ->processPage($nextNode->attr('href'));
            }
            // echo $nextNode->text();exit;        
        // });
    }


    //采集分类，类名
    protected function processProductList(Crawler $crawler)
    {
        #进入产品购买页界面
        // print_r($breadcrumbs);exit;
        $crawler->filter('.product-item-name a')->each(function(Crawler $node,$i){
            try{
                $this->discount = rand(65,80)/100;  #rand(65,80)/100;

                $this->crawlerProduct([
                    'name' => "[{$i}]".$node->text(),
                    'url'  => sprintf('%s',$node->attr('href')),
                ]);
            }catch(\Exception $exception){
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s',$exception->getLine(),$exception->getFile(),$exception->getMessage()));
            }
        });
    }

    //商品详细页面采集
    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name']).'>>>'.$item['url'];
        $response = $this->guzzleHttpClient->request('GET',$item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        //其他-------------------------------------------------------------------------------------------------------------
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory'] = ' ';
        $product['variations']=[];
        $product['description'] = ' ';

        //顶部菜单栏
        if($crawler->filter('.product-subtitle')->count()){
            $product['short_description'] = $crawler->filter('.product-subtitle')->text();
        };
        // print_r($product['short_description']);exit;
        //颜色
        if($crawler->filter('[data-th="Coloris"]')->count()){
            $product['color'] = $crawler->filter('[data-th="Coloris"]')->text();
        }
        // print_r($product['color']);exit;
        //SKU
        $product['sku'] = $crawler->filter('[itemprop="sku"]')->text();
        // print_r($product['sku']);exit;
        //标题
        $product['title'] = $crawler->filter('.base')->text();
        // print_r($product['title']);exit;
        //价格
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr('content');
        // print_r($product['price']);exit;
        //品牌
        $product['brand'] = "KLEMAN";
        //类型""
        $product['type'] = 'simple';
        //
        $product['id'] = "";
        
        //图片------------------------------------------------------------------------------------------------------------------
        $images =[];
        $crawler->filter('[type="text/x-magento-init"]')->each(function(Crawler $node,$i)use(&$images){
            if(strstr($node->text(),"mage/gallery/gallery")){
                $images = json_decode($node->text(),true);
            }
        });
        // print_r($images);exit;
        $arr = $images["[data-gallery-role=gallery-placeholder]"]['mage/gallery/gallery']['data'];
        for ($i=0;$i<count($arr);$i++)
        {
            $data[$i]=$arr[$i]['img'];
        }
        // print_r($data);exit;
        foreach ($data as $image) {
            // echo $image."\r\n";
            $img[]= [
                'name' => $product['title'],
                'src' => $image,
            ];
        }
        $product['images'] = $this->toEncryptImage($img);
        // print_r($product['images']);exit;
        
        //子分类----------------------------------------------------------------------------------------------------------
        // $product['categories']="";
        $breadcrumbs = $crawler->filter('.breadcrumbs li')->each(function(Crawler $node,$i){
            return $node->text();
        });
        // print_r($breadcrumbs);exit;
        $breadcrumbs = array_slice($breadcrumbs,1,-1);
        $parent = 0;
        $parentCategory = '';
        foreach ($breadcrumbs as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['categories']);exit;
        //标签-------------------------------------------------------------------------------------------------------------
        $tags = array_slice($breadcrumbs,2);
        // print_r($tags);exit;
        array_push($tags,$product['brand'],$product['color']);
        $product['tags'] = $tags;
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
        // print_r($product['tags']);exit;

        //选项--------------------------------------------------------------------------------------------
        //尺寸739
        if(strstr($crawler->filter('[type="text/x-magento-init"]')->eq(11)->text(),"739")){
            $variation = json_decode($crawler->filter('[type="text/x-magento-init"]')->eq(11)->text(),true)['#product_addtocart_form']['configurable']['spConfig']['attributes']['739']['options'];
            for ($i=0;$i<count($variation);$i++){
                $var[$i]=$variation[$i]['label'];
            }
            // print_r($var);exit;
            $product['variations'][]= [
                'name' => "Taille",
                'options' => $var,
            ];
            $product['type'] = 'variable';
        }
        // //尺寸632
        if(strstr($crawler->filter('[type="text/x-magento-init"]')->eq(11)->text(),"632")){
            $variation = json_decode($crawler->filter('[type="text/x-magento-init"]')->eq(11)->text(),true)['#product_addtocart_form']['configurable']['spConfig']['attributes']['632']['options'];
            for ($i=0;$i<count($variation);$i++){
                $var[$i]=$variation[$i]['label'];
            }
            // print_r($var);exit;
            $product['variations'][]= [
                'name' => "Taille",
                'options' => $var,
            ];
            $product['type'] = 'variable';
        }
        // print_r($product['variations']);exit;

        //产品描述-----------------------------------------------------------------------------------------------------------------------
        // $product['description'] = $crawler->filter('[id="description"]')->text();
        // print_r($product['description']);exit;
        // 产品规格---------------------------------------------------------------------------------------------------------------------
        // $product['attributes'] = [];
        if($crawler->filter('.additional-attributes-wrapper tbody')->count()){
            $count = $crawler->filter('.additional-attributes-wrapper tbody tr')->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter('.additional-attributes-wrapper tbody tr')->eq($i)->filter('th')->text(),
                    'options' => [$crawler->filter('.additional-attributes-wrapper tbody tr')->eq($i)->filter('td')->text()],
                ];
            }   
        }
        // print_r($product['attributes']);exit;

        //print_r($product);exit;
        // $this->createProduct($product);
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        echo "\r\n";
        echo "NextOne".PHP_EOL;
    }

    // 图片格式函数
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}   