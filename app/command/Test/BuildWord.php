<?php

namespace app\command;


use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use function vierbergenlars\SemVer\Internal\eq;

class BuildWord extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:buydvdsonline')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源buydvdsonline.co.uk');
    }

    protected function initialize(Input $input, Output $output)
    {
       # $this->sites = [
      #      'thebuydvdsonline' => ['source' => ['shop/page/6'], 'target' => 'www.thebuydvdsonline.com'],
       # ];

       # $this->processSite = $this->sites[$input->getOption('site')];

        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.climatedoctors.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://mkzy',
            'ck_f51d8ee1eb6b472ab4cd0eb3f86639bb3baefe86',
            'cs_745b66f86fdb959646fefb0fd62b1a5c90ec5881',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
                'query_string_auth' => true,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
//        $this->crawlerProduct([
//            'name' => 'Hart To Hart – Complete Series DVD',
//            'url' => 'https://buydvdsonline.co.uk/dvd-releases-uk/hart-to-hart-complete-series/',
//        ]);
//        exit;
        try {
            $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
            foreach ($sources as $source) {
                $this->processPage("https://www.climatedoctors.com/indoor-air-quality-iaq/{$source}");
            }
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            echo PHP_EOL;
            echo $exception->getFile();
            echo PHP_EOL;
            echo $exception->getCode();

        }

    }

    protected function processPage($url)
    {
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('.next a')->last();
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('strong a')->each(function (Crawler $node, $i) {
            if (!$node->attr('href')) return;

            try {
                $this->crawlerProduct([
                    'name' => $node->text(),
                    'url' => sprintf('%s', $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);

        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $product['sku'] = $crawler->filter('.VariationProductSKU')->text();
        $product['title'] = $crawler->filter('.title')->text();
        $product['price'] = trim($crawler->filter('.VariationProductPrice')->text(), '£');
        $product['brand'] = null;
        $product['type'] = 'simple';
        $product['attributes'] = [];
        $product['variations'] = [];
        $product['categories'] = [];
        $product['breadcrumbs'] = [];
        $product['keywords'] = [];
        $product['image'] = implode(';', $crawler->filter("a[rel='prodImage']")->each(function (Crawler $node, $i) {
            return ($node->attr("href"));
        }));

//        $show = false;
        $product['description'] = $crawler->filter('.prodAccordionContent')->text();
//            ->closest('.wpb_wrapper');
//        $desc = array_filter($descriptionNode->children()->each(function (Crawler $node, $i) use (&$show) {
//            if ($node->filter('li')->count()) {
//                $show = $i;
//            }
//            if ($i > $show) return $node->outerHtml();
//        }));

//        $descriptionNode->filter('li')->each(function (Crawler $node) use (&$product) {
//            $data = explode(':', $node->text());
//            $options = isset($data[1]) ? explode(',', $data[1]) : [];
//
//            $product['attributes'][] = [
//                'name' => $data[0],
//                'options' => $options,
//            ];
//
//            $product[$data[0]] = $options;
//
//            if (in_array($data[0], ['Genre', 'Starring', 'Studio', 'Release Date'])) {
//                $product['keywords'] = array_merge($product['keywords'], $options);
//            }
//        });

        $product['images'] = array_unique($crawler->filter('.product-images .woocommerce-product-gallery__image img')->each(function (Crawler $node) {
            return $node->attr('src');
        }));
//        foreach ($images as $image) {
//            $product['images'][]['src'] = $image;
//        }

//        $product['description'] = implode(PHP_EOL, $desc);

//        $product['breadcrumbs'] = $crawler->filter('.posted_in a')->each(function (Crawler $node) {
//            return $node->text();
//        });
//
//        foreach ($product['breadcrumbs'] as $breadcrumb) {
//            $parent = 0;
//            $parentCategory = '';
//            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
//        }

//        $tags = $crawler->filter('.tagged_as a')->each(function (Crawler $node) {
//            return $node->text();
//        });
        $product['tags'] = [];
//            $this->createProductTag($tags);

        $this->createProduct($product);
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $keywords = ""; //implode(', ', array_slice($product['keywords'], 0, 2));

        $year = isset($product['Release Date'][0]) ? sprintf('(%s)', $product['Release Date'][0]) : '';

        return ucfirst(preg_replace('/\s+/', ' ', sprintf('%s %s - %s', $name, $year, $keywords)));
    }

    protected function salePrice($price): string
    {
        return (string)$price;
//        return (string)($price > 130 ? rand(84, 94) : $price * rand(70, 85) / 100);
    }
}
