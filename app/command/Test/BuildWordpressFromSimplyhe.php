<?php

namespace app\command;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;

class BuildWordpressFromSimplyhe extends Command
{
    use Common;

    /**
     * @var array
     */
    private $collections = array();
    /**
     * @var array
     */
    private $categories = array();

    protected function configure()
    {
        $this->setName('build:wordpress:kassidiaris')
            ->setDescription('创建Wordpress站点，数据源https://shop.kassidiaris.gr/');
    }

    protected function execute(Input $input, Output $output)
    {
        $collectionUrls = [
            'simplyhe' => [
                'DVD' => '/pneumatic-hydraulic-equipment',
            ]
        ];

        foreach ($collectionUrls['simplyhe'] as $source) {

            $this->processCollection("https://shop.kassidiaris.gr/{$source}");
        }
    }

    protected function processCollection($url, $categories = [])
    {
        $this->output->highlight($url);

        $response = $this->guzzleHttpClient->request('GET', $url, [
            'headers' => [
                'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36',
            ],
        ]);
        $contents = $response->getBody()->getContents();
        $crawler = new  Crawler($contents);

        $categoryNode = $crawler->filter('.woocommerce-loop-product__link > a');
        if ($categoryNode->count()) {
            $collections = $categoryNode->each(function (Crawler $node) {
                return [
                    'name' => $node->text(),
                    'url' => $node->attr('href'),
                ];
            });
            foreach ($collections as $collection) {
                $categories2 = $categories;
                $categories2[] = $collection['name'];

                $this->processCollection($collection['url'], $categories2);
            }
        } else {
            $this->crawlerProduct($url, $categories);
        }
    }

    protected function crawlerProduct($url, $categories)
    {
        $page = 1;
        while (true) {
            $response = $this->guzzleHttpClient->request('GET', $url);
            $products = json_decode($response->getBody()->getContents(), true)['products'];
            print_r($response);
            if (!$products) break;

            $this->output->writeln(sprintf('Page: %s', $page));

            foreach ($products as $product) {
                $product['tags'] = array_unique($product['tags']);

                $parent = 0;
                $parentCategory = '';
                $categoryIds = [];
                foreach ($categories as $categoryName) {


                    $parent = $this->createCategory($categoryName, $parentCategory, $parent);
                    $parentCategory = $categoryName;
                }
                $categoryIds[]['id'] = $parent;

                $this->output->info(sprintf('%s [%s]', $product['title'], "https://{$this->processSite['source']}/products/{$product['handle']}"));

                try {
                    $product['create_type'] = count($product['variants']) === 1 && $product['variants'][0]['title'] === 'Default Title' ? 'simple' : 'variable';
                    $this->createProduct($product, $categoryIds);
                } catch (\Exception $exception) {
                    $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
                }
            }

            $page++;
        }
    }

    protected function createProduct($product, &$categoryIds = [])
    {
        $wooProduct = $this->woocommerce->get('products', ['slug' => $product['handle']]);
        if ($wooProduct) {
            $this->output->info('>>> Product Exist');

            $this->output->info('>>> Update Product Categoryies');
            foreach ($wooProduct[0]->categories as $category) {
                $categoryIds[]['id'] = $category->id;
            }
            $this->woocommerce->put('products/' . $wooProduct[0]->id, [
                'categories' => $categoryIds,
            ]);

            return;
        }

        $data = [
            'name' => $product['title'],
            'slug' => $product['handle'],
            'type' => $product['create_type'],
            'description' => $product['body_html'],
            'categories' => $categoryIds,
            'attributes' => [],
            'images' => [],
        ];

        $data['tags'] = $this->createProductTag($product['tags']);

        if ($product['create_type'] === 'variable') {
            foreach ($product['options'] as $option) {
                $data['attributes'][] = [
                    'name' => $option['name'],
                    'position' => $option['position'],
                    'visible' => true,
                    'variation' => true,
                    'options' => $option['values']
                ];
            }
        }

        foreach ($product['images'] as $i => $image) {
            $data['images'][$i] = ['src' => $image['src']];
            if (isset($image['alt'])) $data['images'][$i] = ['alt' => $image['alt']];
        }

        $regular_price = $product['variants'][0]['compare_at_price'] ?? $product['variants'][0]['price'];
        $sale_price = $product['variants'][0]['price'];
        $data['regular_price'] = (string)$regular_price;
        $data['sale_price'] = (string)$sale_price;

        $this->output->info('>>> Create Product');
        $wooProduct = $this->woocommerce->post('products', $data);

        if ($product['create_type'] === 'simple') {
            $this->output->info('>>> Simple Product');
            return;
        }

        $imageMap = [];
        foreach ($wooProduct->images as $key => $image) {
            $imageId = $product['images'][$key]['id'];
            $imageMap[$imageId] = $image->id;
        }

        $variationData = [
            'create' => []
        ];

        foreach ($product['variants'] as $key => $variant) {
            $variationData['create'][$key] = [
                'regular_price' => $regular_price,
                'sale_price' => $sale_price,
                'sku' => $variant['id'],
            ];

            if (isset($variant['featured_image']['id'])) {
                $variationData['create'][$key]['image'] = ['id' => $imageMap[$variant['featured_image']['id']]];
            }

            if ($variant['option1']) {
                $variationData['create'][$key]['attributes'][] = [
                    'name' => $product['options'][0]['name'],
                    'option' => $variant['option1']
                ];
            }
            if ($variant['option2']) {
                $variationData['create'][$key]['attributes'][] = [
                    'name' => $product['options'][1]['name'],
                    'option' => $variant['option2']
                ];
            }
            if ($variant['option3']) {
                $variationData['create'][$key]['attributes'][] = [
                    'name' => $product['options'][2]['name'],
                    'option' => $variant['option3']
                ];
            }
        }

        $this->output->info('>>> Create Product Variations');
        $wooProductVariations = $this->woocommerce->post(sprintf('products/%u/variations/batch', $wooProduct->id), $variationData);
    }
}
