<?php

namespace app\command\Test;

use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use function vierbergenlars\SemVer\Internal\eq;

class BuildWordpressFrom6pms extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:6pm')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源www.6pm.com');
    }

    protected function initialize(Input $input, Output $output)
    {
        $this->sites = [
            'x' => ['source' => '/u-s-polo-assn/UgLAF-ICAQo.zso', 'target' => 'mkzy'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];

        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.6pm.com',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_f51d8ee1eb6b472ab4cd0eb3f86639bb3baefe86',
            'cs_745b66f86fdb959646fefb0fd62b1a5c90ec5881',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
//        $this->crawlerProduct([
//            'name' => 'Jefries',
////            'url' => 'https://www.6pm.com/a/the-style-room/p/kate-spade-new-york-knott-woven-leather-medium-satchel-blazer-blue-multi/product/9698405/color/795351',
//            'url' => 'https://www.6pm.com/a/the-style-room/p/kate-spade-new-york-tianna-medium-biscotti/product/9727092/color/992871',
//        ]);
//        exit;

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];

        foreach ($sources as $source) {
            $this->processPage($source . '?s=isNew/desc/goLiveDate/desc/recentSalesStyle/desc/');
        }
    }

    protected function processPage($uri)
    {
        $url = "https://www.6pm.com{$uri}";


        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('.next');
        if ($href = $nextNode->attr('href')) {
            $href = strpos($href, $nextNode) === false ? $href . '&s=isNew/desc/goLiveDate/desc/recentSalesStyle/desc/' : $href;

            $this->processPage($href);
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('article')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;

                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->filter('[itemprop="name"]')->text(),
                    'url' => sprintf('%s', 'https://www.6pm.com' . $node->filter('[itemprop="url"]')->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);

        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();

        preg_match('/<script>window.__INITIAL_STATE__ =(.*?);<\/script>/', $contents, $matches);

//        if (!$matches) {
//            $this->crawlerProduct2($contents);
//            return;
//        }

//        file_put_contents('6pm1.json', $matches[1]);
//        exit;
        $jsonData = json_decode($matches[1], true);
        $detailData = $jsonData['product']['detail'];
        $pixelServerData = $jsonData['pixelServer']['data']['product'];

        $product['id'] = $detailData['productId'];
        $product['title'] = $pixelServerData['name'];
        $product['price'] = $pixelServerData['price'];
        $product['brand'] = str_replace([' Supply Co.', ' Co.'], '', $pixelServerData['brand']);
        $product['type'] = 'variable';
        $product['attributes'] = [];
        $product['variations'] = [];
        $product['categories'] = [];
        $product['keywords'] = [];
        $product['gender'] = $pixelServerData['gender'] ?: '';
        $product['tags'] = [$product['gender']];
        $product['breadcrumbs'] = [
            $pixelServerData['category'],
            $pixelServerData['subCategory']
        ];
        $product['subCategory'] = $pixelServerData['subCategory'];

        $colors = [];
        foreach ($jsonData['product']['styleThumbnails'] as $styleThumbnail) {
            $colors[$styleThumbnail['colorId']] = $styleThumbnail['color'];
        }
        $product['color'] = $colors[$jsonData['product']['colorId']];

        foreach ($detailData['styles'] as $style) {
            if ($style['colorId'] === $jsonData['product']['colorId']) {
                foreach ($style['images'] as $image) {
                    $product['images'][]['src'] = sprintf('https://m.media-amazon.com/images/I/%s._AC_SR736,920_.jpg', $image['imageId']);
                }
                $product['tags'][] = $style['color'];
                $product['style_id'] = $style['styleId'];
            }
        }

        foreach ($detailData['sizing']['dimensions'] as $dimension) {
            $product['variations'][] = [
                'name' => $dimension['name'],
                'options' => array_column($dimension['units'][0]['values'], 'value'),
            ];
        }

        $product['description'] = '<ul>';
        foreach ($detailData['description']['bulletPoints'] as $bulletPoint) {
            $product['description'] .= '<li>' . $bulletPoint . '</li>';
        }
        $product['description'] .= '</ul>';

        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
            $parentCategory = $breadcrumb;
        }

        $this->createAttributes($product['variations']);

        $product['tags'] = $this->createProductTag($product['tags']);

        $product['sku'] = slugify(sprintf('%s %s %s', end($product['breadcrumbs']), $product['id'], $product['style_id']));
        print_r($product);
        exit();
        $this->createProduct($product);
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '';

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s, %s', $brand, $gender, $category, $name, $product['color'])));
    }
}
