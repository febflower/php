<?php
//declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;


include("D:\Shilian/vendor/autoload.php");

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

class reptiles
{
    function pagehref($url)
    {
        $headers = [
            'user-agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
        ];
        $client = new Client([
            'base_uri' => $url,
            'timeout' => 100,
//            'headers' => $headers,
            'verify' => false,
//            'proxy' => [
//                'http' => '127.0.0.1:7890', // Use this proxy with "http"
//                'https' => '127.0.0.1:7890', // Use this proxy with "https",
//                'no' => ['.mit.edu', 'foo.com']    // Don't use a proxy with these
//            ]
//        "REMOTE_ADDR"=>'https:127.0.0.1:7890'
        ]);

        //发送请求获取页面内容
        $response = $client->request('GET')->getBody()->getContents();
        $crawler = new Crawler();
        foreach ($crawler as $domElement) {
            var_dump($domElement->nodeName);
        }
        $crawler->addHtmlContent($response);

        $hrefs = $crawler->filter('.woocommerce-loop-product__link')->each(function (Crawler $node, $i) {
            return ($node->attr("href"));
        });
        return $hrefs;

    }

    function details($url)
    {
        $headers = [
            'user-agent' => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
        ];
        $client = new Client([
            'base_uri' => $url,
            'timeout' => 50,
//            'headers' => $headers,
            'verify' => false,
//            'proxy' => [
//                'http' => '127.0.0.1:7890', // Use this proxy with "http"
//                'https' => '127.0.0.1:7890', // Use this proxy with "https",
//                'no' => ['.mit.edu', 'foo.com']    // Don't use a proxy with these
//            ]
//        "REMOTE_ADDR"=>'https:127.0.0.1:7890'
        ]);
        //发送请求获取页面内容
        $response = $client->request('GET')->getBody()->getContents();
        $crawler = new Crawler();

        foreach ($crawler as $domElement) {
            var_dump($domElement->nodeName);
        }
        $crawler->addHtmlContent($response);

        try {
            $data ['categories'] = implode('||',$crawler->filter('.woocommerce-breadcrumb')->each(function (Crawler $node, $i) {
                return $node->text();
            }));

            $data ['images'] = implode(' ;',$crawler->filter('.wp-post-image')->each(function (Crawler $node, $i) {
                return $node->attr('src');
            }));

            $data ['title'] = $crawler->filter('.entry-title')->text();

            $data ['price'] = str_replace("$","",implode(',',$crawler->filter('.amount')->eq(0)->each(function (Crawler $node, $i) {
                return $node->text();
            })));

            $data ['sku']= implode('|',$crawler->filter('.variations')->eq(0)->each(function (Crawler $node, $i) {
                return $node->text();
            }));
//
            $data ['description'] = $crawler->filter('.desc-layout-boxed')->text();
            echo json_encode($data);

            echo json_encode($data);

        } catch (Exception $e) {
//            echo $e->getMessage();
        }

    }
}

function main()
{
    $data = new reptiles();
//$pages=$data->page("https://www.lampshoponline.com/led-lights.html?p=2");
    for ($x = 0; $x < 1; $x++) {
        $pageHrefs = $data->pagehref("https://ledshoponline.be/producten/led-lampen/");
        for ($i = 0; $i < count($pageHrefs); $i++) {
            echo $i;
            try {
                $data->details($pageHrefs[$i]);
            } catch (Exception $e) {
//                echo $e->getMessage();
            }
            echo "\r\n";
        }
    }
}

class reptile extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('reptile')
            ->addArgument('reptiles',Argument::OPTIONAL,main())
            ->setDescription('the reptile command');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $name = trim($input->getArgument('reptiles'));
        try {
            $output->writeln($name);
        }catch (\Exception $e){

        }



    }
}
