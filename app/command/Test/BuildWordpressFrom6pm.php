<?php

namespace app\command\Test;
//ini_set("error_reporting","E_ALL & ~E_NOTICE");
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use function vierbergenlars\SemVer\Internal\eq;

class BuildWordpressFrom6pm extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:kassidiaris')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源https://shop.kassidiaris.gr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=["https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/air-regulators/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/coils-for-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/mechanic-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/pneumatic-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/pneumatic-components/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/pneumatic-cylinders/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/repair-kits-for-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/solenoid-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/manifold/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/spare-parts-accessories-pneumatic-hydraulic-equipment/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/bridge-navigational-watch-alarm-system-bnwas/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/fire-alarm-system/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/gas-detectors-o2-analyzers/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/marine-indicator-instruments/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/navigation-light-panels/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/oil-mist-detectors/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/recorders/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/salinometers/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/15ppm-bilge-alarm/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/power-management/","https://shop.kassidiaris.gr/product-category/electrical-equipment/air-circuit-breakers-acb/","https://shop.kassidiaris.gr/product-category/electrical-equipment/contactor-relays/","https://shop.kassidiaris.gr/product-category/electrical-equipment/control-relays/","https://shop.kassidiaris.gr/product-category/electrical-equipment/miniatyre-circuit-breakers-mcb/","https://shop.kassidiaris.gr/product-category/electrical-equipment/molded-case-circuit-breakers-mccb/","https://shop.kassidiaris.gr/product-category/electrical-equipment/thermal-relays/","https://shop.kassidiaris.gr/product-category/electrical-equipment/pid-controllers/","https://shop.kassidiaris.gr/product-category/electrical-equipment/analog-instruments/","https://shop.kassidiaris.gr/product-category/electrical-equipment/digital-instruments/","https://shop.kassidiaris.gr/product-category/electrical-equipment/ex-products/","https://shop.kassidiaris.gr/product-category/electrical-equipment/inverters/","https://shop.kassidiaris.gr/product-category/electrical-equipment/miscellaneous/","https://shop.kassidiaris.gr/product-category/electrical-equipment/power-analyzer/","https://shop.kassidiaris.gr/product-category/electrical-equipment/timers/","https://shop.kassidiaris.gr/product-category/electrical-equipment/converters/","https://shop.kassidiaris.gr/product-category/electrical-equipment/current-transformers/","https://shop.kassidiaris.gr/product-category/electrical-equipment/float-level-switches/","https://shop.kassidiaris.gr/product-category/electrical-equipment/pressure-switches/","https://shop.kassidiaris.gr/product-category/electrical-equipment/pressure-transmitters/","https://shop.kassidiaris.gr/product-category/electrical-equipment/temperature-switches/","https://shop.kassidiaris.gr/product-category/electrical-equipment/temperature-sensors/","https://shop.kassidiaris.gr/product-category/electrical-equipment/plc/","https://shop.kassidiaris.gr/product-category/electrical-equipment/power-supply-module/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/air-regulators/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/coils-for-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/mechanic-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/pneumatic-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/pneumatic-components/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/pneumatic-cylinders/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/repair-kits-for-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/solenoid-valves/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/manifold/","https://shop.kassidiaris.gr/product-category/pneumatic-hydraulic-equipment/spare-parts-accessories-pneumatic-hydraulic-equipment/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/bridge-navigational-watch-alarm-system-bnwas/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/fire-alarm-system/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/gas-detectors-o2-analyzers/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/marine-indicator-instruments/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/navigation-light-panels/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/oil-mist-detectors/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/recorders/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/salinometers/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/15ppm-bilge-alarm/","https://shop.kassidiaris.gr/product-category/special-marine-equipment/power-management/","https://shop.kassidiaris.gr/product-category/electrical-equipment/air-circuit-breakers-acb/","https://shop.kassidiaris.gr/product-category/electrical-equipment/contactor-relays/","https://shop.kassidiaris.gr/product-category/electrical-equipment/control-relays/","https://shop.kassidiaris.gr/product-category/electrical-equipment/miniatyre-circuit-breakers-mcb/","https://shop.kassidiaris.gr/product-category/electrical-equipment/molded-case-circuit-breakers-mccb/","https://shop.kassidiaris.gr/product-category/electrical-equipment/thermal-relays/","https://shop.kassidiaris.gr/product-category/electrical-equipment/pid-controllers/","https://shop.kassidiaris.gr/product-category/electrical-equipment/analog-instruments/","https://shop.kassidiaris.gr/product-category/electrical-equipment/digital-instruments/","https://shop.kassidiaris.gr/product-category/electrical-equipment/ex-products/","https://shop.kassidiaris.gr/product-category/electrical-equipment/inverters/","https://shop.kassidiaris.gr/product-category/electrical-equipment/miscellaneous/","https://shop.kassidiaris.gr/product-category/electrical-equipment/power-analyzer/","https://shop.kassidiaris.gr/product-category/electrical-equipment/timers/","https://shop.kassidiaris.gr/product-category/electrical-equipment/converters/","https://shop.kassidiaris.gr/product-category/electrical-equipment/current-transformers/","https://shop.kassidiaris.gr/product-category/electrical-equipment/float-level-switches/","https://shop.kassidiaris.gr/product-category/electrical-equipment/pressure-switches/","https://shop.kassidiaris.gr/product-category/electrical-equipment/pressure-transmitters/","https://shop.kassidiaris.gr/product-category/electrical-equipment/temperature-switches/","https://shop.kassidiaris.gr/product-category/electrical-equipment/temperature-sensors/","https://shop.kassidiaris.gr/product-category/electrical-equipment/plc/","https://shop.kassidiaris.gr/product-category/electrical-equipment/power-supply-module/"];
        $this->sites = [
            'x' => ['source' => $array, 'target' => 'mkzy'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];

        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://shop.kassidiaris.gr',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_f51d8ee1eb6b472ab4cd0eb3f86639bb3baefe86',
            'cs_745b66f86fdb959646fefb0fd62b1a5c90ec5881',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {
//        $this->crawlerProduct([
//            'name' => 'Jefries',
////            'url' => 'https://www.6pm.com/a/the-style-room/p/kate-spade-new-york-knott-woven-leather-medium-satchel-blazer-blue-multi/product/9698405/color/795351',
//            'url' => 'https://www.6pm.com/a/the-style-room/p/kate-spade-new-york-tianna-medium-biscotti/product/9727092/color/992871',
//        ]);
//        exit;

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    protected function processPage($uri)
    {
        $url = $uri;

        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('.next');
//        echo $nextNode->attr("href");
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {
        $crawler->filter('.custom-button')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;

                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',  $node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' . $item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);

        $contents = $response->getBody()->getContents();
        $crawler = new Crawler();

        foreach ($crawler as $domElement) {
            var_dump($domElement->nodeName);
        }
        $crawler->addHtmlContent($contents);

//        preg_match('/<html>window.__INITIAL_STATE__ =(.*?);<\/script>/', $contents, $matches);


//        if (!$matches) {
//            $this->crawlerProduct2($contents);
//            return;
//        }

//        file_put_contents('6pm1.json', $matches[1]);
//        exit;
//        $jsonData = json_decode($crawler, true);
//        $detailData = $jsonData['product']['detail'];
//        $pixelServerData = $jsonData['pixelServer']['data']['product'];

//        $product['id'] = [];
        $product['title'] = implode(',',$crawler->filter('.product_title')->each(function (Crawler $node, $i) {
            return $node->text();
        }));
        $product['price'] =str_replace("€","",implode(',',$crawler->filter('[aria-hidden="true"]')->eq(0)->each(function (Crawler $node, $i) {
            return $node->text();
        })));

        $product['brand'] = implode(',',$crawler->filter('[id="tab-pwb_tab-content"]')->each(function (Crawler $node, $i) {
            return $node->text();
        }));

        $product['type'] = 'variable';
        $product['images'][]['src'] = implode(' ;',$crawler->filter('.wp-post-image')->each(function (Crawler $node, $i) {
            return $node->attr('src');
        }));
        $product['attributes'] = [];
        $product['variations'] = [];
        $breadcrumbs = $crawler->filter('.breadcrumbs__item-link')->each(function (Crawler $node) {
            return $node->text();
        });
        $product['breadcrumbs'] = array_slice($breadcrumbs,-2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }

        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=implode(',',$crawler->filter('.posted_in')->each(function (Crawler $node, $i) {
            return $node->text();
        }));
//        $product['gender'] = $pixelServerData['gender'] ?: '';
        $product['tags'][] =$crawler->filter('.tagged_as')->filter('a')->each(function (Crawler $node, $i) {
            return $node->text();
        });
        $product['breadcrumbs'] = [];

//        $colors = [];
//        foreach ($jsonData['product']['styleThumbnails'] as $styleThumbnail) {
//            $colors[$styleThumbnail['colorId']] = $styleThumbnail['color'];
//        }
//        $product['color'] = $colors[$jsonData['product']['colorId']];
//
//        foreach ($detailData['styles'] as $style) {
//            if ($style['colorId'] === $jsonData['product']['colorId']) {
//                foreach ($style['images'] as $image) {
//                    $product['images'][]['src'] = sprintf('https://m.media-amazon.com/images/I/%s._AC_SR736,920_.jpg', $image['imageId']);
//                }
//                $product['tags'][] = $style['color'];
//                $product['style_id'] = $style['styleId'];
//            }
//        }
//
//        foreach ($detailData['sizing']['dimensions'] as $dimension) {
//            $product['variations'][] = [
//                'name' => $dimension['name'],
//                'options' => array_column($dimension['units'][0]['values'], 'value'),
//            ];
//        }
//
        $product['description'] = implode(',',$crawler->filter('.wc-tab')->each(function (Crawler $node, $i) {
            return $node->text();
        }));
//        $parent = 0;
//        $parentCategory = '';
//        foreach ($product['breadcrumbs'] as $breadcrumb) {
//            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
//            $parentCategory = $breadcrumb;
//        }
//
//        $this->createAttributes($product['variations']);
//
//        $product['tags'] = $this->createProductTag($product['tags']);
//
        $product['sku'] =implode(',',$crawler->filter('.sku')->each(function (Crawler $node, $i) {
            return $node->text();
        }));
//        print_r($product);
//        exit();
       $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s, %s', $brand, $gender, $category, $name, $product['color'])));
    }

}
