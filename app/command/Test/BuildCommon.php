<?php

namespace app\command\Test;

trait BuildCommon
{
    /**
     * @var array
     */
    private $categories;
    /**
     * @var array
     */
    private $tags;
    /**
     * @var array
     */
    private $attributes = array();

    protected $discount = 0.7;

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = $product['brand'] && strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        //        $color = strpos(strtolower($name), strtolower($product['color'])) === false ? $product['color'] : '';

        //        $category = end($product['breadcrumbs']);
        //        $category = strpos(strtolower($name), strtolower($category)) === false ? $category : '';

        return ucfirst(preg_replace('/\s+/', ' ', sprintf('%s %s', $brand, $name)));
    }

    protected function regularPrice($price): string
    {
        return (string)($price > 600 ? rand(400, 600) : $price);
    }

    protected function salePrice($price): string
    {
        return (string)($price > 120 ? rand(88, 98) . '.99' : $price * $this->discount);
    }

    protected function createProduct($product)
    {
        $data = [
            'sku' => $product['sku'] ?? sprintf('%s %s', $product['brand'], $product['id']),
            'type' => $product['type'],
            'description' => preg_replace("/<a[^>]*>(.*?)<\/a>/is", "$1", $product['description']),
            'short_description' => $product['short_description'] ?? null,
            'categories' => $product['categories'],
            'tags' => $product['tags'],
            'attributes' => [],
            'images' => $product['images'],
        ];

        $data['name'] = $this->generateName($product);
        $data['slug'] = slugify($data['name']);

        $wooProduct = $this->woocommerce->get('products', ['sku' => $data['sku']]);
        if ($wooProduct) {
            $this->output->info('>>> Product Exist');

            $this->output->info('>>> Update Product Categoryies');
            foreach ($wooProduct[0]->categories as $category) {
                //                if ($category->id === 15) continue;
                $product['categories'][]['id'] = $category->id;
            }
            $this->woocommerce->put('products/' . $wooProduct[0]->id, [
                'categories' => $product['categories'],
                'name' => $data['name'],
                'slug' => $data['slug'],
                'tags' => $data['tags'],
            ]);

            return;
        }

        foreach ($product['variations'] as $k => $variation) {
            $data['attributes'][$k] = [
                'name' => $variation['name'],
                'position' => 1,
                'visible' => true,
                'variation' => true,
                'options' => $variation['options']
            ];

            if (isset($this->attributes[strtolower($variation['name'])])) {
                $data['attributes'][$k]['id'] = $this->attributes[strtolower($variation['name'])];
            }
        }

        foreach ($product['attributes'] as $attribute) {
            $data['attributes'][] = [
                'name' => $attribute['name'],
                'position' => 1,
                'visible' => true,
                'variation' => false,
                'options' => $attribute['options']
            ];
        }

        $data['regular_price'] = $this->regularPrice($product['price']);
        $data['sale_price'] = $this->salePrice($product['price']);
        $data['meta_data'][] = ['key' => 'wc_brand', 'value' => $product['brand']];

        $this->output->info('>>> Create Product');
        $wooProduct = $this->woocommerce->post('products', $data);

        if ($product['type'] === 'simple') {
            $this->output->info('>>> Simple Product');
            return;
        }

        $variationData = [
            'create' => []
        ];

        $variationData['create'][0] = [
            'regular_price' => $data['regular_price'],
            'sale_price' => $data['sale_price'],
        ];
        foreach ($product['variations'] as $k => $variation) {
            if (!$variation['options']) continue;
            $variationData['create'][0]['attributes'][$k] = [
                'name' => $variation['name']
            ];

            if (isset($this->attributes[strtolower($variation['name'])])) {
                $variationData['create'][0]['attributes'][$k]['id'] = $this->attributes[strtolower($variation['name'])];
            }
        }

        $this->output->info('>>> Create Product Variations');
        $wooProductVariations = $this->woocommerce->post(sprintf('products/%u/variations/batch', $wooProduct->id), $variationData);
    }

    protected function createAttributes($variations)
    {
        if (!$this->attributes) {
            $wooAttributes = $this->woocommerce->get('products/attributes');
            foreach ($wooAttributes as $attribute) {
                $this->attributes[strtolower($attribute->name)] = $attribute->id;
            }
        }

        foreach ($variations as $variation) {
            if (isset($this->attributes[strtolower($variation['name'])])) continue;

            $wooAttribute = $this->woocommerce->post('products/attributes', ['name' => $variation['name']]);
            $this->attributes[strtolower($variation['name'])] = $wooAttribute->id;
        }
    }

    protected function createProductTag($tags): array
    {
        $tagIds = $data = [];

        foreach ($tags as $tagName) {
            $key = slugify($tagName);

            if (isset($this->tags[$key])) {
                $tagIds[] = ['id' => $this->tags[$key]];
            } else {
                $data['create'][] = ['name' => ucfirst($tagName)];
            }
        }

        if ($data) {
            $wooProductTag = $this->woocommerce->post('products/tags/batch', $data);
            foreach ($wooProductTag->create as $tag) {
                if (isset($tag->error)) {
                    $tagIds[] = ['id' => $tag->error->data->resource_id];
                } else {
                    $key = slugify($tag->name);
                    $this->tags[$key] = $tag->id;
                    $tagIds[] = ['id' => $tag->id];
                }
            }
        }

        return $tagIds;
    }

    protected function createCategory($categoryName, $parentCategory, $parent = 0)
    {
        $this->output->info($categoryName);

        if (!isset($this->categoryIds[$parent])) $this->categoryIds[$parent] = [];

        $slug = slugify(trim($categoryName));
        if (isset($this->categories[$slug])) {
            if (in_array($this->categories[$slug], $this->categoryIds[$parent])) {
                return $this->categories[$slug];
            }
            $slug = slugify(trim($parentCategory . ' ' . $categoryName));
            if (isset($this->categories[$slug]) && in_array($this->categories[$slug], $this->categoryIds[$parent])) {
                return $this->categories[$slug];
            }
        } else {
            $slug2 = slugify(trim($parentCategory . ' ' . $categoryName));
            $checkCategory = $this->woocommerce->get('products/categories', ['slug' => $slug2]);
            if ($checkCategory) {
                $slug = $slug2;
                $category = $checkCategory;
            }
        }

        if (!isset($category)) $category = $this->woocommerce->get('products/categories', ['slug' => $slug]);

        if (empty($category)) {
            $this->output->info('Creat Category');
            $category = $this->woocommerce->post('products/categories', [
                'name' => ucfirst($categoryName),
                'slug' => $slug,
                'parent' => $parent,
            ]);
            $this->categoryIds[$parent][] = $category->id;
            return $this->categories[$slug] = $category->id;
        } else {
            $this->categoryIds[$parent][] = $category[0]->id;
            return $this->categories[$slug] = $category[0]->id;
        }
    }
}
