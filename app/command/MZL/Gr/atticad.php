<?php
namespace app\command\MZL\Gr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class atticad extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:atticad')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.atticadps.gr');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['https://www.atticadps.gr/gunaikeia-moda/rouxa/foremata/mini/?_gl=1*7bnv70*_up*MQ..*_ga*MTUzMzA0MDU5OS4xNjY3OTYxNDQ3*_ga_DH5MPKVHLL*MTY2Nzk2MTQ0Ni4xLjEuMTY2Nzk2MTQ2OS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/foremata/midi/?_gl=1*1dxks0b*_up*MQ..*_ga*MTUzMzA0MDU5OS4xNjY3OTYxNDQ3*_ga_DH5MPKVHLL*MTY2Nzk2MTQ0Ni4xLjEuMTY2Nzk2MTQ3OS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/foustes/?_gl=1*1tlieyi*_up*MQ..*_ga*MTUzMzA0MDU5OS4xNjY3OTYxNDQ3*_ga_DH5MPKVHLL*MTY2Nzk2MTQ0Ni4xLjEuMTY2Nzk2MTU2NS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/pantelonia/?_gl=1*1w4nlv*_up*MQ..*_ga*MTUzMzA0MDU5OS4xNjY3OTYxNDQ3*_ga_DH5MPKVHLL*MTY2Nzk2MTQ0Ni4xLjEuMTY2Nzk2MTU3Ny4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/pantelonia/?pg=2','https://www.atticadps.gr/gunaikeia-moda/rouxa/poukamisa/?_gl=1*1b02bnm*_up*MQ..*_ga*MTUzMzA0MDU5OS4xNjY3OTYxNDQ3*_ga_DH5MPKVHLL*MTY2Nzk2MTQ0Ni4xLjEuMTY2Nzk2MTY3Ni4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/sakakia/?_gl=1*1gi856f*_up*MQ..*_ga*MjEzNjg0MzU1Mi4xNjY3OTYxODk3*_ga_DH5MPKVHLL*MTY2Nzk2MTg5Ni4xLjAuMTY2Nzk2MTg5Ni4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/mplouzes-top/?_gl=1*1bfiif*_up*MQ..*_ga*MjEzNjg0MzU1Mi4xNjY3OTYxODk3*_ga_DH5MPKVHLL*MTY2Nzk2MTg5Ni4xLjEuMTY2Nzk2MTkwNC4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/mplouzes-top/?pg=2','https://www.atticadps.gr/gunaikeia-moda/rouxa/mplouzes-top/?pg=3','https://www.atticadps.gr/gunaikeia-moda/rouxa/mplouzes-top/?pg=4','https://www.atticadps.gr/gunaikeia-moda/rouxa/mplouzes-top/?pg=5','https://www.atticadps.gr/gunaikeia-moda/rouxa/zaketes-poulover/?_gl=1*155onh0*_up*MQ..*_ga*MjEzNjg0MzU1Mi4xNjY3OTYxODk3*_ga_DH5MPKVHLL*MTY2Nzk2MTg5Ni4xLjEuMTY2Nzk2MTkxNS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/zaketes-poulover/?pg=2','https://www.atticadps.gr/gunaikeia-moda/rouxa/palto-mpoufan/?_gl=1*1b7esun*_up*MQ..*_ga*MjEzNjg0MzU1Mi4xNjY3OTYxODk3*_ga_DH5MPKVHLL*MTY2Nzk2MTg5Ni4xLjEuMTY2Nzk2MTk3Ny4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/palto-mpoufan/?pg=2','https://www.atticadps.gr/gunaikeia-moda/rouxa/athlitika/?_gl=1*11199sd*_up*MQ..*_ga*MjEzNjg0MzU1Mi4xNjY3OTYxODk3*_ga_DH5MPKVHLL*MTY2Nzk2MTg5Ni4xLjEuMTY2Nzk2MjAwNS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/rouxa/olosomes-formes/?_gl=1*1efw5s3*_up*MQ..*_ga*MjEzNjg0MzU1Mi4xNjY3OTYxODk3*_ga_DH5MPKVHLL*MTY2Nzk2MTg5Ni4xLjEuMTY2Nzk2MjAzMS4wLjAuMA..',];
       $array2=['https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-heiros/?_gl=1*1xvtggr*_up*MQ..*_ga*NTMzMzEwNjYuMTY2Nzk2MzYyNQ..*_ga_DH5MPKVHLL*MTY2Nzk2MzYyNS4xLjEuMTY2Nzk2Mzg1Ny4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-heiros/?pg=2','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-heiros/?pg=3','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-heiros/?pg=4','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?_gl=1*18dok6s*_up*MQ..*_ga*NTMzMzEwNjYuMTY2Nzk2MzYyNQ..*_ga_DH5MPKVHLL*MTY2Nzk2MzYyNS4xLjEuMTY2Nzk2Mzg5OS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=2','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=3','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=4','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=5','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=6','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=7','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-wmou/?pg=8','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?_gl=1*akwqfk*_up*MQ..*_ga*NTMzMzEwNjYuMTY2Nzk2MzYyNQ..*_ga_DH5MPKVHLL*MTY2Nzk2MzYyNS4xLjEuMTY2Nzk2NDAwNi4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?pg=2','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?pg=3','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?pg=4','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?pg=5','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?pg=6','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-xiasti-crossbody/?pg=7','https://www.atticadps.gr/gunaikeia-moda/tsantes/sakidia-platis/?_gl=1*1rx82qb*_up*MQ..*_ga*NTMzMzEwNjYuMTY2Nzk2MzYyNQ..*_ga_DH5MPKVHLL*MTY2Nzk2MzYyNS4xLjEuMTY2Nzk2NDA4My4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/tsantes/sakidia-platis/?pg=2','https://www.atticadps.gr/gunaikeia-moda/tsantes/sakidia-platis/?pg=3','https://www.atticadps.gr/gunaikeia-moda/tsantes/sakidia-platis/?pg=4','https://www.atticadps.gr/gunaikeia-moda/tsantes/tsantes-mesis/?_gl=1*17mcifa*_up*MQ..*_ga*NTMzMzEwNjYuMTY2Nzk2MzYyNQ..*_ga_DH5MPKVHLL*MTY2Nzk2MzYyNS4xLjEuMTY2Nzk2NDE2MC4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/tsantes/clutches-tsantes-fakeloi/?_gl=1*164v6jy*_up*MQ..*_ga*NTMzMzEwNjYuMTY2Nzk2MzYyNQ..*_ga_DH5MPKVHLL*MTY2Nzk2MzYyNS4xLjEuMTY2Nzk2NDE5NC4wLjAuMA..'];
       $array3=['https://www.atticadps.gr/gunaikeia-moda/axesouar/portofolia/?_gl=1*k8tmdd*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDgzOS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/axesouar/portofolia/?pg=2','https://www.atticadps.gr/gunaikeia-moda/axesouar/portofolia/?pg=3','https://www.atticadps.gr/gunaikeia-moda/axesouar/zwnes/?_gl=1*ng3k9v*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDg0NS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/axesouar/foularia-kaskol/?_gl=1*1vyta8o*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDg5Mi4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/axesouar/ompreles/?_gl=1*1opoct0*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDg5OS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/axesouar/thikes-kinitou/?_gl=1*cr1kpl*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDkwOS4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/axesouar/kaltses/?_gl=1*1vzyj2o*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDkxNy4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/valitses/?_gl=1*1f7qb37*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDk1My4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/valitses/?pg=2','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/neseser/?_gl=1*1nhkwzy*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDk1Ny4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/thikes-laptop/?_gl=1*5144di*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDk4Mi4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/pilot-cases/?_gl=1*1bl93hs*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDk5MC4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/thikes-valitsas/?_gl=1*1f3k0nn*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NDk5Ni4wLjAuMA..','https://www.atticadps.gr/gunaikeia-moda/eidi-taxidiou/backpack-taxidiou/?_gl=1*kckl73*_up*MQ..*_ga*NTEwODY0MzQ4LjE2Njc5NjQ4Mzc.*_ga_DH5MPKVHLL*MTY2Nzk2NDgzNi4xLjEuMTY2Nzk2NTAwMi4wLjAuMA..'];
       $array4=['https://www.atticadps.gr/andrikh-moda/rouxa/sakakia/?_gl=1*da2hnm*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NTkxMi4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/zaketes-poulover/?_gl=1*ipkw4z*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjEwMC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/zaketes-poulover/?pg=2','https://www.atticadps.gr/andrikh-moda/rouxa/zaketes-poulover/?pg=3','https://www.atticadps.gr/andrikh-moda/rouxa/zaketes-poulover/?pg=4','https://www.atticadps.gr/andrikh-moda/rouxa/poukamisa/?_gl=1*gbtnr*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjEwOS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/poukamisa/?pg=2','https://www.atticadps.gr/andrikh-moda/rouxa/poukamisa/?pg=3','https://www.atticadps.gr/andrikh-moda/rouxa/pantelonia/?_gl=1*123lvb2*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjE0Ni4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/pantelonia/?pg=2','https://www.atticadps.gr/andrikh-moda/rouxa/pantelonia/?pg=3','https://www.atticadps.gr/andrikh-moda/rouxa/palto-mpoufan/?_gl=1*hbyme0*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjE4Mi4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/palto-mpoufan/?pg=2','https://www.atticadps.gr/andrikh-moda/rouxa/mplouzes-t-shirts/?_gl=1*15ia7xh*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjIxMy4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/mplouzes-t-shirts/?pg=2','https://www.atticadps.gr/andrikh-moda/rouxa/mplouzes-t-shirts/?pg=3','https://www.atticadps.gr/andrikh-moda/rouxa/mplouzes-t-shirts/?pg=4','https://www.atticadps.gr/andrikh-moda/rouxa/mplouzes-t-shirts/?pg=5','https://www.atticadps.gr/andrikh-moda/rouxa/fouter/?_gl=1*qs3fvb*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjI4Mi4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/rouxa/fouter/?pg=2','https://www.atticadps.gr/andrikh-moda/rouxa/gileka/?_gl=1*5ipheh*_up*MQ..*_ga*MTcwNjI1MDUxOS4xNjY3OTY1ODY0*_ga_DH5MPKVHLL*MTY2Nzk2NTg2My4xLjEuMTY2Nzk2NjI5My4wLjAuMA..'];
       $array5=['https://www.atticadps.gr/andrikh-moda/axesouar/portofolia/?_gl=1*5e5vzd*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzI2Ny4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/axesouar/zwnes/?_gl=1*1own9w2*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzI5MC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/axesouar/kapela-skoufoi/?_gl=1*zzu9v0*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzMzNS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/axesouar/kaltses/?_gl=1*1ksgb5l*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzM0Ni4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/axesouar/ompreles/?_gl=1*1a4177w*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzM2MC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/axesouar/thikes-laptop-kinhtou/?_gl=1*kjlsur*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzM2OC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/axesouar/thikes-gia-kartes/?_gl=1*162g0ra*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzM3OC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/tsantes/sakidia-plaths/?_gl=1*sbse7w*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzM5Ny4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/tsantes/sakidia-plaths/?pg=2','https://www.atticadps.gr/andrikh-moda/tsantes/sakidia-plaths/?pg=3','https://www.atticadps.gr/andrikh-moda/tsantes/sakidia-plaths/?pg=4','https://www.atticadps.gr/andrikh-moda/tsantes/tsantes-xiasti-crossbody/?_gl=1*1tr0xf9*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzQwMS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/tsantes/tsantes-xeiros/?_gl=1*c7rsg6*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzQ0MS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/tsantes/tsantes-meshs/?_gl=1*bd87eg*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzQ1NC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/papoutsia/athlhtika/?_gl=1*1dcqzpe*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzUzNS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/papoutsia/athlhtika/?pg=2','https://www.atticadps.gr/andrikh-moda/papoutsia/klasika/?_gl=1*1hi374b*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzYzNS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/papoutsia/mpotakia/?_gl=1*1trp0z7*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzY1MS4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/papoutsia/mokasinia/?_gl=1*1wiii2l*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzY2MC4wLjAuMA..','https://www.atticadps.gr/andrikh-moda/papoutsia/padofles/?_gl=1*1dzck2x*_up*MQ..*_ga*NDIzOTMwNTAuMTY2Nzk3MzIzNg..*_ga_DH5MPKVHLL*MTY2Nzk3MzIzNi4xLjEuMTY2Nzk3MzY2OS4wLjAuMA..'];



        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'atticad001.seo070.site'],
            'x2' => ['source' => $array2, 'target' =>'atticad002.seo070.site'],
            'x3' => ['source' => $array3, 'target' =>'atticad003.seo070.site'],
            'x4' => ['source' => $array4, 'target' =>'atticad004.seo070.site'],
            'x5' => ['source' => $array5, 'target' =>'atticad005.seo070.site'],


        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.atticadps.gr',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));exit;
//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.product__link')->each(function (Crawler $node, $i)  {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.productInfo__title')->text();  //标题（必须）
        // print_r($product['title']);exit;

        $pricedel=[',','€','$'];
        $product['price'] = str_replace($pricedel,'',$crawler->filter('.priceContainer__price')->text()); 
        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.priceContainer__price')->text())));  //价格（必须）

        // print_r($product['price']);exit;

        // $product['brand']='';
       //品牌（必须）
       if($crawler->filter('.productInfo__subtitle')->count()){
           $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
       }else{
        $product['brand']=' ';
       }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb  a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        $del=['Κωδικός προϊόντος:'];
        $product['sku'] =str_replace($del,'',$crawler->filter('.productInfo__code')->text());  //产品编号（必须）
        // print_r($product['sku']);exit;

        $product['short_description']='';  // 简短描述
        // print_r($product['short_description']);exit;

        $product['description']=$crawler->filter('[class="accordions__content active"] p')->html();//描述
        // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
        
        // print_r($product['description']);exit;
        $product['attributes'] = [];
        //属性   x1 x2 使用
        // $del=[':'];
        // if($crawler->filter('.d-md-none')->count()) {
        //     $count = $crawler->filter('[class="blue-info d-md-none "] ul li')->count();
        //     // echo $count;exit;
        //     if($crawler->filter('[class="color-red"]')->count()){
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }       
        //     }else{
        //         for($i=0;$i<$count;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }

         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];
        $product['gender'] = '';

        $del=['Χρώμα'];
        if($crawler->filter('.attribute')->count()){
            $product['color'] =str_replace($del,'',$crawler->filter('.attribute')->text());
        }else{
            $product['color']='';
        }
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'][] =[];
        $product['images'] =[];
        //图片（必须）
        $images=$crawler->filter('.productDetails__item--image img')->each(function(Crawler $node,$i){
            return 'https://www.atticadps.gr'. $node->attr('data-src');
        });
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    
    //    print_r($images);exit;
    //   商品的尺寸
        $product['variations']=[];
        // print_r($product);exit;
        if($crawler->filter('.options__row--mobileBreak')->count()>0){$product['variations'][]= [
                'name'=>$crawler->filter('[class="options__label"]')->text(),
                'options'=>$crawler->filter('.options__field')->filter('.inline-fields')->filter('.radio-container label')->each(function (Crawler $node, $i) {
                    return $node->text();})];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }
        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}