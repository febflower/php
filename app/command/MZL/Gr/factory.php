<?php
namespace app\command\MZL\Gr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class factory extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:factory')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:http://www.factoryoutlet.gr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/beachwear#Sub2_133=Sub2_133','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/beachwear#Sub2_94=Sub2_94','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/beachwear#Sub2_95=Sub2_95','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/beachwear#Sub2_93=Sub2_93','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/beachwear#Sub2_13=Sub2_13','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/beachwear#Sub2_4=Sub2_4','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/athlhtika#Cat3_334=Cat3_334','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/athlhtika#Cat3_337=Cat3_337','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/athlhtika#Cat3_1251=Cat3_1251','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/athlhtika#Cat3_2799=Cat3_2799','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/athlhtika#Cat3_335=Cat3_335','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/athlhtika#Cat3_336=Cat3_336','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/esoroyxa#Cat3_440=Cat3_440','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/esoroyxa#Cat3_443=Cat3_443','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/esoroyxa#Cat3_442=Cat3_442','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/esoroyxa#Cat3_438=Cat3_438','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/esoroyxa#Cat3_437=Cat3_437','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/kolan#Sub2_11=Sub2_11','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/kolan#Sub2_33=Sub2_33','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/kolan#Sub2_54=Sub2_54','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/kolan#Sub2_6=Sub2_6'];
       $array2=['http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/magio#Cat3_135=Cat3_135','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/magio#Cat3_136=Cat3_136','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/magio#Cat3_137=Cat3_137','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/magio#Cat3_138=Cat3_138','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/mployzes#Cat3_119=Cat3_119','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/mployzes#Cat3_116=Cat3_116','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/mployzes#Cat3_117=Cat3_117','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/mployzes#Cat3_118=Cat3_118','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/mployzes#Cat3_435=Cat3_435','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/olosomesformes#Sub2_11=Sub2_11','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/olosomesformes#Sub2_33=Sub2_33','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/olosomesformes#Sub2_54=Sub2_54','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/plektazaketes#Cat3_142=Cat3_142','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/plektazaketes#Cat3_141=Cat3_141'];
       $array3=['http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/pantelonia#','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_74=Sub2_74','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_127=Sub2_127','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_75=Sub2_75','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_76=Sub2_76','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_77=Sub2_77','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_57=Sub2_57','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/panoforia#Sub2_79=Sub2_79','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/poykamisa#Cat3_133=Cat3_133','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/poykamisa#Cat3_134=Cat3_134','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/poykamisa#Cat3_331=Cat3_331','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/sortsvermoydes#Cat3_131=Cat3_131','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/sortsvermoydes#Cat3_130=Cat3_130'];
       $array4=['http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/tzin#Cat3_1755=Cat3_1755','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/tzin#Cat3_1752=Cat3_1752','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/tzin#Cat3_1753=Cat3_1753','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/tzin#Cat3_1751=Cat3_1751','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/tzin#Cat3_1754=Cat3_1754','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foremata#Cat3_328=Cat3_328','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foremata#Cat3_123=Cat3_123','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foremata#Cat3_327=Cat3_327','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foremata#Cat3_122=Cat3_122','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foystes#Cat3_125=Cat3_125','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foystes#Cat3_126=Cat3_126','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foystes#Cat3_124=Cat3_124','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foyter#Cat3_1659=Cat3_1659','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/royxa/foyter#Cat3_1658=Cat3_1658'];
    
        //    可能分类会不对，
       $array5=['http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_2225=Cat3_2225','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_39=Cat3_39','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_38=Cat3_38','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_315=Cat3_315','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_763=Cat3_763','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_41=Cat3_41','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_40=Cat3_40','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_42=Cat3_42','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_44=Cat3_44','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_45=Cat3_45','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/papoytsia#Cat3_43=Cat3_43'];
       $array6=['http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_52=Cat3_52','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_50=Cat3_50','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_61=Cat3_61','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_51=Cat3_51','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_1979=Cat3_1979','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_60=Cat3_60','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_57=Cat3_57','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_55=Cat3_55','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_54=Cat3_54','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_2235=Cat3_2235','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_49=Cat3_49','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_53=Cat3_53','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_3956=Cat3_3956','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_59=Cat3_59','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_48=Cat3_48','http://www.factoryoutlet.gr/gr-el/online-shop/gynaikeia/aksesoyar#Cat3_47=Cat3_47'];
       
       $array7=['http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/athlhtika#Cat3_348=Cat3_348','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/athlhtika#Cat3_351=Cat3_351','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/athlhtika#Cat3_349=Cat3_349','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/athlhtika#Cat3_350=Cat3_350','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/esoroyxa#Cat3_514=Cat3_514','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/esoroyxa#Cat3_515=Cat3_515','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/esoroyxa#Cat3_516=Cat3_516','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/magio#Cat3_1255=Cat3_1255','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/magio#Cat3_181=Cat3_181','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/magio#Cat3_182=Cat3_182','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/tzin#Cat3_1749=Cat3_1749','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/tzin#Cat3_1747=Cat3_1747','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/tzin#Cat3_1748=Cat3_1748','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/tzin#Cat3_1746=Cat3_1746'];
       
       $array8=['http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_1743=Cat3_1743','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_177=Cat3_177','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_1717=Cat3_1717','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_342=Cat3_342','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_176=Cat3_176','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_1744=Cat3_1744','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_1718=Cat3_1718','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/pantelonia#Cat3_1745=Cat3_1745','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_913=Cat3_913','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_472=Cat3_472','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_2228=Cat3_2228','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_345=Cat3_345','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_183=Cat3_183','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_344=Cat3_344','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_915=Cat3_915','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_184=Cat3_184','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/panoforia#Cat3_914=Cat3_914'];
       
       $array9=['http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/plektazaketes#Cat3_347=Cat3_347','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/plektazaketes#Cat3_346=Cat3_346','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/poykamisa#Cat3_180=Cat3_180','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/poykamisa#Cat3_343=Cat3_343','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/sortsvermoydes#Cat3_179=Cat3_179','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/sortsvermoydes#Cat3_178=Cat3_178','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/foyter#Cat3_1654=Cat3_1654','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/royxa/foyter#Cat3_1653=Cat3_1653'];
       $array10=['http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/sneakers','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/athlhtika','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/deta','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/espantrigies','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/mokasinialoafers','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/mpotesmpotakia','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/pantofles','http://www.factoryoutlet.gr/gr-el/online-shop/andrika/papoytsia/sagionaresslides'];
       $array11=['http://www.factoryoutlet.gr/gr-el/online-shop/paidika/boys/aksesoyar','http://www.factoryoutlet.gr/gr-el/online-shop/paidika/boys/papoytsia','http://www.factoryoutlet.gr/gr-el/online-shop/paidika/boys/royxa'];
       $array12=['http://www.factoryoutlet.gr/gr-el/online-shop/paidika/girls/aksesoyar','http://www.factoryoutlet.gr/gr-el/online-shop/paidika/girls/papoytsia','http://www.factoryoutlet.gr/gr-el/online-shop/paidika/girls/royxa'];
       $array13=['http://www.factoryoutlet.gr/gr-el/online-shop/paidika/baby/aksesoyar','http://www.factoryoutlet.gr/gr-el/online-shop/paidika/baby/papoytsia','http://www.factoryoutlet.gr/gr-el/online-shop/paidika/baby/royxa'];
       $array14=['http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/vermoydessorts','http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/kolan','http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/mployzes','http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/mployzesfoyter','http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/mpoystakia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/panoforia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/treksimo/papoytsia'];
       $array15=['http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/vermoydessorts','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/kolan','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/mployzes','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/mployzesfoyter','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/mpoystakia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/panteloniaformas','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/panoforia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/proponhsh/papoytsia',''];
       $array16=['http://www.factoryoutlet.gr/gr-el/online-shop/sports/mpasket/emfaniseisomadon','http://www.factoryoutlet.gr/gr-el/online-shop/sports/mpasket/mployzes','http://www.factoryoutlet.gr/gr-el/online-shop/sports/mpasket/papoytsia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/podosfairo/mployzes','http://www.factoryoutlet.gr/gr-el/online-shop/sports/podosfairo/papoytsia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/yoga/kolan','http://www.factoryoutlet.gr/gr-el/online-shop/sports/yoga/mployzes','http://www.factoryoutlet.gr/gr-el/online-shop/sports/yoga/mpoystakia','http://www.factoryoutlet.gr/gr-el/online-shop/sports/tenis/foremata'];
     



        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'factory001.seo072.site'],
            'x2' => ['source' => $array2, 'target' =>'factory002.seo072.site'],
            'x3' => ['source' => $array3, 'target' =>'factory003.seo072.site'],
            'x4' => ['source' => $array4, 'target' =>'factory004.seo073.site'],
            'x5' => ['source' => $array5, 'target' =>'factory005.seo073.site'],
            'x6' => ['source' => $array6, 'target' =>'factory006.seo073.site'],
            'x7' => ['source' => $array7, 'target' =>'factory007.seo073.site'],
            'x8' => ['source' => $array8, 'target' =>'factory008.seo073.site'],
            'x9' => ['source' => $array9, 'target' =>'factory009.seo073.site'],
            'x10' => ['source' => $array10, 'target' =>'factory0010.seo073.site'],
            'x11' => ['source' => $array11, 'target' =>'factory0011.seo075.site'],
            'x12' => ['source' => $array12, 'target' =>'factory0012.seo076.site'],
            'x13' => ['source' => $array13, 'target' =>'factory0013.seo076.site'],
            'x14' => ['source' => $array14, 'target' =>'factory0014.seo076.site'],
            'x15' => ['source' => $array15, 'target' =>'factory0015.seo077.site'],
            'x16' => ['source' => $array16, 'target' =>'factory0016.seo077.site'],
       
            
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'http://www.factoryoutlet.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[class="icon arrow-big simple next paging"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage('http://www.factoryoutlet.gr/'.$nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
        // $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li a')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // 进入详情页爬取数据
        $crawler->filter('[class="prod-holder bordered"] a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s','http://www.factoryoutlet.gr'.$node->attr('href')),
                   
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.descr')->text();  //标题（必须）
        // print_r($product['title']);exit;

        // $pricedel=[',','€','$'];
        // $product['price'] = str_replace($pricedel,'',$crawler->filter('.price')->text()); 

        $del= [';'];
        $int = $crawler->filter('script')->eq(29)->text();  //价格（必须）
        $index = strpos($int,'=');
        $it=str_replace($del,'',substr($int,$index+1));
        $product['price']=json_decode($it,true)['price'];

        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('[itemprop="price"]')->text())));  //价格（必须）

        // print_r($product['price']);exit;



        $product['brand']=$crawler->filter('.main-prod-info h1')->text();
       //品牌（必须）

    //    $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.bread li a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
   
        // $product['sku'] =json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['sku']; 
        $del= [';'];
        $int = $crawler->filter('script')->eq(29)->text();  //产品编号（必须）
        $index = strpos($int,'=');
        $it=str_replace($del,'',substr($int,$index+1));
        $product['sku']=json_decode($it,true)['id'];
        // print_r($product['sku']);exit;
       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];
        $product['description']=str_replace($descr,'',$crawler->filter('.detailInfo')->html());

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        
        // if($crawler->filter('.detailInfo')->count()) {
        //     $count = $crawler->filter('.detailInfo div')->eq(1)->count();
        //     // echo $count;exit;
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>$crawler->filter('.product-specification-item')->eq($i)->filter('dt')->text(),
        //                 'options' =>$crawler->filter('.product-specification-item')->eq($i)->filter('dd')->text(),
        //             ];
        //         }       
            
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        // $product['gender']='';  x8使用
        $product['gender'] = '';
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $img=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$img){
        //     if(strstr($node->text(),"image")){
        //         $img = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('#prod-gal a')->each(function(Crawler $node,$i){
            return  $node->attr('data-zoom-image');
        })));
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    //    print_r($images);exit;
    //   商品的尺寸
        $product['variations']=[];
        // print_r($product);exit;
      
        if($crawler->filter('#colorsTable')->count()>0){$product['variations'][]= [
                'name'=>$crawler->filter('.mobile-select')->filter('option')->eq(0)->text(),
                'options'=>$crawler->filter('.mobile-select')->filter('option')->eq(1)->each(function (Crawler $node, $i) {
                    return $node->attr('value');
                })
            ];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}