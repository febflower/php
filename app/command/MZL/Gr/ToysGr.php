<?php
namespace app\command\MZL\Gr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ToysGr extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:ToysGr')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.toys24.gr');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
        $array=['https://www.toys24.gr/toys/christougenniatika/dentra-christougenniatika/','https://www.toys24.gr/toys/christougenniatika/christougenniatikes-mpales/','https://www.toys24.gr/toys/christougenniatika/christougenniatika-stolidia-kai-aksesouar/','https://www.toys24.gr/toys/christougenniatika/diakosmhtika-fwta-led-fwtoswlhnes/','https://www.toys24.gr/toys/christougenniatika/koryfes-dentrwn/','https://www.toys24.gr/toys/christougenniatika/ah-vasilhs-christougenniatikos/','https://www.toys24.gr/toys/christougenniatika/keramika-keria-diakosmhtika/','https://www.toys24.gr/toys/christougenniatika/christougenniatikes-kaltses-loutrina-kapela/','https://www.toys24.gr/toys/christougenniatika/christougenniatikes-stoles/' ];
        $array1=['https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/diadrastika-ekpaideftika/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/ta-prwta-tou-paichnidia/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/malaka-paichnidia/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/koudounistres/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/paichnidia-kounias-karotsiou/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/paichnidia-mpaniou/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/vrefika-paichnidia/trenakia/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/ksylina-paichnidia/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/touvlakia/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/strates-perpatoures-kounista/','https://www.toys24.gr/toys/proscholika-vrefika-paichnidia/gymnasthria-gia-mwra/'];
        $array2=['https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/command/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/nerf/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/alla-paichnidia-drashs/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/diafora-opla-kai-spathia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/kapsoulia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/opla-cowboy/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/opla-astynomika/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/nerf-paichnidia-me-opla-kai-spathia/opla-peiratwn/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/roucha-kai-aksesouar-gia-koukles/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/eidh-super-market/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/ergaleia-mastorematos/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/iatrika-ergaleia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/karotsakia-krevatakia-mwrou/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/kataskopeia-diaswsh/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/kouzinika-kai-eidh-spitiou/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paichnidia-rolwn-mimhshs/set-koritsistika-omorfias/'];
        $array3=['https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/grifoi-kai-spazokefalies/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/ekpaideftika/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/enhlikwn/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/oikogeneiaka/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/paidika-epitrapezia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/podosfairakia-fliper-chokey/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/proscholika/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/tavli-skaki/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/taksidiou/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/epitrapezia-paichnidia-kartes/trapoules-kai-markes/'];
        $array4=['https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/agrotika-epangelmatika-chwmatourgika/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/aftokinhtakia-gkaraz-pistes/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/aftokinhtodromoi/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/elikoptera-drones-kai-petoumena/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/metallika-ochhmata-montela/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/ochhmata-me-kinhsh-trivhs/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/thlekatefthhnomena/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/aftokinhta-petoumena-ochhmata/trena-kai-sidhrodromoi/'];
       
        $array5=['https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/figoures-drashs','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/figoures-drashs/'];
        
        $array6=['https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/pazl-puzzle/aksesouar-pazl/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/pazl-puzzle/kyvoi/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/pazl-puzzle/pazl-3d/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/pazl-puzzle/pazl-diafora/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/pazl-puzzle/pazl-enhlikwn/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/pazl-puzzle/pazl-paidika/'];
        $array7=['https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/diafora/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/armonia-piano-akornteon/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/kithares-kai-violia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/mikrofwna/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/pnefsta/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/set/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mousika-paichnidia/tympana-kai-krousta/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/ekpaideftika-computers-laptops-kai-tablets/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/hlektronika-cheiros/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/konsoles/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/paichnidia-gia-pc/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/paichnidia-gia-konsoles/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/perifereiaka/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/hlektronika-paichnidia/forhtes-paichnidomhchanes/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/paichnidia-antistress/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/playsets/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/robots/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/agoristika-paichnidia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/koritsistika-paichnidia/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/paichnidia-unisex/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/svoures-kai-aksesouar/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/paidika-paichnidia/syromena/','https://www.toys24.gr/toys/paichnidia-epitrapezia-pazl/mpataries/'];
        $array8=['https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-jurassic-world/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-juniors/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-harry-potter/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-vidiyo/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-super-mario/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-dots/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-trolls/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-city/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-duplo/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-friends/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-ninjago/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-classic/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-creator/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-disney/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-star-wars/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-super-heroes/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/lego/lego-technic/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/play-doh-plastelines/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/kataskeves-kai-episthmh/'];
        $array9=['https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-asterix/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-wiltopia/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-adventures-of-ayuma/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-dino-rise/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-stunt-show/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/everdreamerz/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-scooby-doo/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-heidi/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-novelmore/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-123/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-camping/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-city-action/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-city-life/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-country/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-dollhouse/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-friends/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-family-fun/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-figures/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-friends/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-history/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-princess/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-princess/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-special-plus/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-sports-action/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-valitsaki-eksarthmata/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/playmobil/playmobil-ksenodocheio/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/cheirotechnia-kai-kallitechnikes-drasthriothtes/','https://www.toys24.gr/toys/playmobil-lego-kataskeves/kataskeves-me-chantres-kai-kosmhmata/'];
        $array10=['https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/loutrina/loutrina-paschalina/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/loutrina/loutrina-agiou-valentinou/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/loutrina/loutrina-klassika/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/loutrina/loutrina-me-mpataries/'];
        $array11=['https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/mikrokosmos-koritsiwn/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/koukles-modas/'];
        $array12=['https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/spastes-koukles/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/zwakia-figoures/'];
        $array13=['https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/paichnidia-me-mwra/idolls/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/paichnidia-me-mwra/nenuco-koukles/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/paichnidia-me-mwra/koukles-mwra-diafores/','https://www.toys24.gr/toys/koukles-loutrina-mikrokosmos/miniatoures/'];
        $array14=['https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/podhlata-trikykla-rollers-scooters/podhlata-paidika-kai-enhlikwn/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/podhlata-trikykla-rollers-scooters/trikykla-podhlatakia/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/podhlata-trikykla-rollers-scooters/scooters-paidika/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/podhlata-trikykla-rollers-scooters/rollers-kai-patinia/'];
        $array15=['https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/hlektrokinhta-aftokinhta-mhchanes/mpatariokinhta-aftokinhta-kai-jeep/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/hlektrokinhta-aftokinhta-mhchanes/mpatariokinhtes-mhchanes-scooter-kai-gourounes/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/trakter-paidika/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/spitia-khpou-paidika/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/kounies-paidikes/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/tsoulhthres/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/trampolino/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/trampales/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/kareklakia-paidika/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/kentra-drasthriothtwn-outdoor/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paichnidia-ekswterikou-chwrou/ammodochoi/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/mpales/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/podosfairo-kai-termata/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/mpasketes/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/bowling-kai-korines/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/finger-skates/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/varakia-kai-organa-gymnastikhs/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/velakia-stochoi-toksa/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/mpoks-kai-sakoi/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/paichnidia-athlhshs-diafora/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/schoinakia/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/athlhtika-paichnidia-kai-aksesouar/choula-choup/'];
        $array16=['https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/aksesouar-gia-podhlata-kai-podhlates/prostateftika-kranh/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/aksesouar-gia-podhlata-kai-podhlates/gantia-epigwnatides-epiagkwnides/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/magiw/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/camping-kai-eksoplismos-paralias/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/aksesouar-kolymvhshs/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/varkes-kai-aksesouar/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/mpratsakia-swsivia-paidika/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/petsetes-kai-pontso-paralias/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paralia-kai-kolymvhsh/chrhsima-aksesouar-gia-th-thalassa/',];
        $array17=['https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/pisines/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/kouvadakia-thalasshs/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/neropistola/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/paidika-ergaleia-karotsia-paralias-khpou/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/strwmata-thalasshs/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/strwmata-thalasshs/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/forthga-kai-ekskafeis-paralias/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/kalokairina-paichnidia-fouskwta/fouskwta-paichnidia-thalasshs/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paidika-rologia-gyalia-hliou-ompreles-tsantakia/rologia-gia-paidia/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paidika-rologia-gyalia-hliou-ompreles-tsantakia/tsantakia-portofolakia-mprelok-paidika-rologia-gyalia-hliou-ompreles-tsantakia/','https://www.toys24.gr/toys/outdoor-podhlata-hlektrokinhta/paidika-rologia-gyalia-hliou-ompreles-tsantakia/tsantakia-meshs-mpananes-portofolia/'];
        $array18=['https://www.toys24.gr/toys/scholika-vivlia-dvd-chartika/tsantes-sakoi-kasetines-faghtodocheia/scholikoi-sakoi-kai-sakidia/','https://www.toys24.gr/toys/scholika-vivlia-dvd-chartika/tsantes-sakoi-kasetines-faghtodocheia/scholikes-tsantes-sakoi-trolley/','https://www.toys24.gr/toys/scholika-vivlia-dvd-chartika/tsantes-sakoi-kasetines-faghtodocheia/scholikes-tsantes-nhpiwn/','https://www.toys24.gr/toys/scholika-vivlia-dvd-chartika/tsantes-sakoi-kasetines-faghtodocheia/taperakia-faghtou-nhpiou/'];




        $this->sites = [
            'x1' => ['source' => $array, 'target' =>'toysgr001.seo065.site'],
            'x2' => ['source' => $array1, 'target' =>'toysgr002.seo065.site'],
            'x3' => ['source' => $array2, 'target' =>'toysgr003.seo065.site'],
            'x4' => ['source' => $array3, 'target' =>'toysgr004.seo065.site'],
            'x5' => ['source' => $array4, 'target' =>'toysgr005.seo070.site'],
            'x6' => ['source' => $array5, 'target' =>'toysgr006.seo070.site'],
            'x7' => ['source' => $array6, 'target' =>'toysgr007.seo070.site'],
            'x8' => ['source' => $array7, 'target' =>'toysgr008.seo070.site'],
            'x9' => ['source' => $array8, 'target' =>'toysgr009.seo070.site'],
            'x10' => ['source' => $array9, 'target' =>'toysgr0010.seo070.site'],
            'x11' => ['source' => $array10, 'target' =>'toysgr0011.seo070.site'],
            'x12' => ['source' => $array11, 'target' =>'toysgr0012.seo070.site'],
            'x13' => ['source' => $array12, 'target' =>'toysgr0013.seo071.site'],
            'x14' => ['source' => $array13, 'target' =>'toysgr0014.seo071.site'],
            'x15' => ['source' => $array14, 'target' =>'toysgr0015.seo071.site'],
            'x16' => ['source' => $array15, 'target' =>'toysgr0016.seo071.site'],
            'x17' => ['source' => $array16, 'target' =>'toysgr0017.seo071.site'],
            'x18' => ['source' => $array17, 'target' =>'toysgr0018.seo071.site'],
            'x19' => ['source' => $array18, 'target' =>'toysgr0019.seo071.site'],
            





        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.toys24.gr',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));exit;
//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.woocommerce-LoopProduct-link')->each(function (Crawler $node, $i)  {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.product_title')->text();  //标题（必须）
        // print_r($product['title']);exit;


        $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('[property="product:price:amount"]')->attr('content'))));  //价格（必须）

        // print_r($product['price']);exit;

        // $product['brand']='';
       //品牌（必须）
       if($crawler->filter('[property="product:brand"]')->count()){
           $product['brand']=$crawler->filter('[property="product:brand"]')->attr('content');
       }else{
        $product['brand']=' ';
       }
        
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('#breadcrumbs a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;

     

        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        $product['sku'] = $crawler->filter('[property="product:retailer_item_id"]')->attr('content');  //产品编号（必须）
        // print_r($product['sku']);exit;

        
        
        $product['short_description']=$crawler->filter('#toggle-content')->html();  // 简短描述
        // print_r($product['short_description']);exit;

        // $product['description']=$crawler->filter('.prod-content')->html();//描述
         $int =$crawler->filter('.prod-content')->html(); //描述  
        $index = strpos($int,'<div class="blue-info d-md-none blue-bg">');
        $product['description'] = substr($int,0,$index);
        
        // print_r($product['description']);exit;
      
        //属性   x1 x2 使用
        // $del=[':'];
        // if($crawler->filter('.d-md-none')->count()) {
        //     $count = $crawler->filter('[class="blue-info d-md-none "] ul li')->count();
        //     // echo $count;exit;
        //     if($crawler->filter('[class="color-red"]')->count()){
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }       
        //     }else{
        //         for($i=0;$i<$count;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }

            // x3使用  
        $del=[':'];
        if($crawler->filter('.d-md-none')->count()) {
            $count = $crawler->filter('[class="col-md-5 d-md-block d-none"] ul li')->count();
            // echo $count;exit;
            if($crawler->filter('[class="color-red"]')->count()){
                for($i=0;$i<$count-1;$i++){
                    $product['attributes'][] = [
                        'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
                        'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
                    ];
                }       
            }else{
                for($i=0;$i<$count;$i++){
                    $product['attributes'][] = [
                        'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
                        'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
                    ];
                }
            }
        }else{
            $product['attributes'] = [];
        }
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];
        $product['gender'] = '';
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][] =[];
        $product['images'] =[];
        //图片（必须）
        $images=$crawler->filter('.MagicToolboxContainer a')->each(function(Crawler $node,$i){
            return $node->attr('href');
        });
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    
    //    print_r($images);exit;
    //   商品的尺寸
        $product['variations']=[];

        // $product['variantions']="";
        // print_r($product);exit;
        // // if($crawler->filter('.attribute_radio')->count()>0){$product['variations'][]= [
        //         'name'=>'sizes',
        //         'options' =>$crawler->filter('.attribute_radio')->each(function (Crawler $node, $i) {
        //             return $node->attr('data-attname');})];  
        // }else{$product['variations'][]=[
        //         'name'=>'',
        //         'options' =>""];
        // }
        // if ($product['variations'][0]['options']){
        //     $product['type'] = 'variable';
        // }
        // print_r($product);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}