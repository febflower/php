<?php
namespace app\command\MZL\Gr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class politikos extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:politikos')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.politikos-shop.gr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['https://www.politikos-shop.gr/el-gr/andras-rouxa-panoforia-gileka','https://www.politikos-shop.gr/el-gr/andras-rouxa-panoforia-imipalto','https://www.politikos-shop.gr/el-gr/andras-rouxa-panoforia-mpoufan','https://www.politikos-shop.gr/el-gr/andras-rouxa-panoforia-palto','https://www.politikos-shop.gr/el-gr/andras-rouxa-panoforia-sakakia',];
       $array2=['https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/ascott-1,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/big-star,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/fred-perry,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/funky-buddha,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/mont,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/mont,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/the-bostonians,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/armani-exchange,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/barbour,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/bizzaro,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/bizzaro,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/boss,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/calvin-klein,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/camel-active,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/colins,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/colins,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/columbia,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/diesel,1-4/?lastselectionid=19911','politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/edward,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/emporio-armani,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/fynch-hatton,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/gant,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/guy-laroche,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/hugo,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/jack-jones,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/lacoste,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/lamartina,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/ralp-lauren,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/staff,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/timberland,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/tommy-hilfiger,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/vans,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/versace-jeans,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-poukamisa-1/wrangler,1-4/?lastselectionid=19911'];
       $array3=['https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-polo-1','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-t-shirt-1','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-kazakes'];
       $array4=['https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/funky-buddha,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/paul-shark,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/the-bostonians,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/armani-exchange,the-bostonians,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/barbour,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/boss,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/colins,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/ellemme,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/fynch-hatton,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/fynch-hatton,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/gant,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/hugo,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/jack-jones,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/lacoste,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/nautica,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/navy-green,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/oxford-company,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/ralp-lauren,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-plekta-1/tommy-hilfiger,1-4/?lastselectionid=19911'];
       $array5=['https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/funky-buddha,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/iceberg,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/karl-lagerfeld,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/paul-shark,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/the-bostonians,paul-shark,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/adidas,paul-shark,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/armani-exchange,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/body-action,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/boss,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/calvin-klein,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/columbia,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/diesel,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/ellesse,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/emporio-armani,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/gant,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/hugo,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/jack-jones,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/lacoste,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/levis,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/napapirji,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/nautica,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/ralp-lauren,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/timberland,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/superdry,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/tommy-hilfiger,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-rouxa-mplouzes-fouter-1/vans,1-4/?lastselectionid=19911'];
       $array6=['https://www.politikos-shop.gr/el-gr/andras-papoutsia-sneakers-1','https://www.politikos-shop.gr/el-gr/andras-papoutsia-mpotakia-1','https://www.politikos-shop.gr/el-gr/andras-papoutsia-oxfords','https://www.politikos-shop.gr/el-gr/andras-papoutsia-mokasinia','https://www.politikos-shop.gr/el-gr/andras-papoutsia-sagionares'];
       $array7=['https://www.politikos-shop.gr/el-gr/andras-esorouxa/ralph-lauren-underwear,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/apple,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/armani-exchange,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/boss,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/calvin-klein,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/diesel,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/gant,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/helios,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/jack-jones,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/lacoste,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/minerva,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/sloggi,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/tommy-hilfiger,1-4/?lastselectionid=19911','https://www.politikos-shop.gr/el-gr/andras-esorouxa/walk,1-4/?lastselectionid=19911'];
       $array8=['https://www.politikos-shop.gr/el-gr/andras-athlitika-athlitika-aksesouar-aksesouar-kolymvisis','https://www.politikos-shop.gr/el-gr/andras-athlitika-athlitika-aksesouar-kaltses','https://www.politikos-shop.gr/el-gr/andras-athlitika-athlitika-aksesouar-mpoukalia-thermos','https://www.politikos-shop.gr/el-gr/andras-athlitika-ano-endysi-t-shirt','https://www.politikos-shop.gr/el-gr/andras-athlitika-ano-endysi-mpoufan','https://www.politikos-shop.gr/el-gr/andras-athlitika-ano-endysi-fouter-fouter-zaketa-1','https://www.politikos-shop.gr/el-gr/andras-athlitika-kato-endysi','https://www.politikos-shop.gr/el-gr/andras-athlitika-papoutsia',];
       
       $array9=['https://www.politikos-shop.gr/el-gr/gynaika-rouxa-panoforia-imipalto','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-panoforia-mpoufan-mpoufan-amaniko-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-panoforia-mpoufan-mpoufan-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-panoforia-palto-kamparntines-kamparntines','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-panoforia-palto-kamparntines-palto-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-panoforia-sakakia'];
       $array10=['https://www.politikos-shop.gr/el-gr/gynaika-rouxa-magio-magio-vikini','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-magio-magio-olosomo','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-magio-magio-slip','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-foustes-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-foustes-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-sorts-vermoudes-vermoudes','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-sorts-vermoudes-sorts'];
       $array11=['https://www.politikos-shop.gr/el-gr/gynaika-rouxa-pantelonia-chinos-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-pantelonia-jean-5p-cargo-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-pantelonia-kolan-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-pantelonia-panteloni-formas-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-pantelonia-yfasmatina',];
       $array12=['https://www.politikos-shop.gr/el-gr/gynaika-rouxa-mplouzes-polo','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-mplouzes-plekta','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-mplouzes-t-shirt-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-mplouzes-top-kormakia-1','https://www.politikos-shop.gr/el-gr/gynaika-rouxa-mplouzes-fouter'];
       $array13=['https://www.politikos-shop.gr/el-gr/gynaika-papoutsia-sneakers-1','https://www.politikos-shop.gr/el-gr/gynaika-papoutsia-mokasinia-mpalarines','https://www.politikos-shop.gr/el-gr/gynaika-papoutsia-goves','https://www.politikos-shop.gr/el-gr/gynaika-papoutsia-mpotes-mpotakia-1','https://www.politikos-shop.gr/el-gr/gynaika-papoutsia-pedila','https://www.politikos-shop.gr/el-gr/gynaika-papoutsia-sagionares'];
       $array14=['https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-soutien','https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-faneles','https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-kalson','https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-kaltses-1','https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-kormakia','https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-lasteks','https://www.politikos-shop.gr/el-gr/gynaika-esorouxa-slip'];
       $array15=['https://www.politikos-shop.gr/el-gr/gynaika-athlitika-athlitika-aksesouar-aksesouar-kolymvisis','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-athlitika-aksesouar-kaltses','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-ano-endysi-mpoustakia','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-ano-endysi-mpoufan-1','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-ano-endysi-t-shirt','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-ano-endysi-fouter-fouter-zaketa','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-kato-endysi-kolan-1','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-kato-endysi-panteloni-formas-1','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-kato-endysi-sorts-vermouda','https://www.politikos-shop.gr/el-gr/gynaika-athlitika-papoutsia'];
       $array16=['https://www.politikos-shop.gr/el-gr/gynaika-aksesouar-tsantes','https://www.politikos-shop.gr/el-gr/gynaika-aksesouar-zones','https://www.politikos-shop.gr/el-gr/gynaika-aksesouar-kaskol-foularia','https://www.politikos-shop.gr/el-gr/gynaika-aksesouar-portofolia','https://www.politikos-shop.gr/el-gr/gynaika-aksesouar-skoufoi-kapela-1'];
       $array17=['https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-vrefika-eidi','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-gileka','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-zaketes','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-mplouzes','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-mpoufan','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-panteloni','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-poukamisa','https://www.politikos-shop.gr/el-gr/paidi-agori-rouxa-set','https://www.politikos-shop.gr/el-gr/paidi-agori-papoutsia-flat','https://www.politikos-shop.gr/el-gr/paidi-agori-papoutsia-sneakers-1','https://www.politikos-shop.gr/el-gr/paidi-agori-papoutsia-sagionares','https://www.politikos-shop.gr/el-gr/paidi-agori-athlitika-kato-endysi','https://www.politikos-shop.gr/el-gr/paidi-agori-athlitika-set','https://www.politikos-shop.gr/el-gr/paidi-agori-athlitika-papoutsia-1'];
       $array18=['https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-vrefika','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-gileka','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-zaketes','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-mplouzes','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-mpoufan','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-palto','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-panteloni','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-poulover','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-sorts-vermoudes','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-foremata','https://www.politikos-shop.gr/el-gr/paidi-koritsi-rouxa-foustes'];
       
        // 规则改变，不需要性别
       $array19=['https://www.politikos-shop.gr/el-gr/spiti-eidi-spitiou-kourtines','https://www.politikos-shop.gr/el-gr/spiti-eidi-spitiou-rixtaria','https://www.politikos-shop.gr/el-gr/spiti-kouzina-traverses','https://www.politikos-shop.gr/el-gr/spiti-kouzina-trapezomantila','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-paploma','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-paplomatothikes','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-sentonia','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-vrefika-eidi','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-kouverli','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-kouvertes','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-maksilaria-ypnou','https://www.politikos-shop.gr/el-gr/spiti-krevatokamara-maksilarothikes','https://www.politikos-shop.gr/el-gr/spiti-mpanio-mpournouzia','https://www.politikos-shop.gr/el-gr/spiti-mpanio-patakia-mpaniou','https://www.politikos-shop.gr/el-gr/spiti-mpanio-petsetes'];



        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'politikos001.seo071.site'],
            'x2' => ['source' => $array2, 'target' =>'politikos002.seo071.site'],
            'x3' => ['source' => $array3, 'target' =>'politikos003.seo071.site'],
            'x4' => ['source' => $array4, 'target' =>'politikos004.seo071.site'],
            'x5' => ['source' => $array5, 'target' =>'politikos005.seo071.site'],
            'x6' => ['source' => $array6, 'target' =>'politikos006.seo072.site'],
            'x7' => ['source' => $array7, 'target' =>'politikos007.seo072.site'],
            'x8' => ['source' => $array8, 'target' =>'politikos008.seo072.site'],
            'x9' => ['source' => $array9, 'target' =>'politikos009.seo072.site'],
            'x10' => ['source' => $array10, 'target' =>'politikos0010.seo072.site'],
            'x11' => ['source' => $array11, 'target' =>'politikos0011.seo072.site'],
            'x12' => ['source' => $array12, 'target' =>'politikos0012.seo072.site'],
            'x13' => ['source' => $array13, 'target' =>'politikos0013.seo072.site'],
            'x14' => ['source' => $array14, 'target' =>'politikos0014.seo072.site'],
            'x15' => ['source' => $array15, 'target' =>'politikos0015.seo072.site'],
            'x16' => ['source' => $array16, 'target' =>'politikos0016.seo072.site'],
            'x17' => ['source' => $array17, 'target' =>'politikos0017.seo072.site'],
            'x18' => ['source' => $array18, 'target' =>'politikos0018.seo072.site'],
            'x19' => ['source' => $array19, 'target' =>'politikos0019.seo073.site'],
 
             //hxfaa
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sneaker10.gr',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('.nextAjax');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // 进入详情页爬取数据
        $crawler->filter('.product-item-image a')->each(function (Crawler $node, $i) use($breadcrumbs)  {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('[class="product-title d-sm-none"]')->text();  //标题（必须）
        // print_r($product['title']);exit;

        $pricedel=[',','€','$'];
        $product['price'] = str_replace($pricedel,'',$crawler->filter('.product-sale-price')->text()); 
        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.priceContainer__price')->text())));  //价格（必须）

        // print_r($product['price']);exit;

        $product['brand']=$crawler->filter('.product-brand--text')->text();
       //品牌（必须）

    //    $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('.breadcrumb  span a')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],1);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
        $product['sku'] =json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['sku'];  //产品编号（必须）
        // print_r($product['sku']);exit;
       
         $product['short_description']='';
    
        
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        
        $product['description']=$crawler->filter('[class="product-tab-content jsProductTabContent active d-sm-none"]')->html();
        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        // $del=[':'];
        // if($crawler->filter('.d-md-none')->count()) {
        //     $count = $crawler->filter('[class="blue-info d-md-none "] ul li')->count();
        //     // echo $count;exit;
        //     if($crawler->filter('[class="color-red"]')->count()){
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }       
        //     }else{
        //         for($i=0;$i<$count;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']=''; 
        // $product['gender'] = $crawler->filter('.site-main-categories-nav')->filter('li')->eq(0)->filter('a')->text();
        // print_r($product['gender']);exit;

        $product['color'] =$crawler->filter('[class="variation-thumb-item is_active"]')->attr('title');
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'][] =[];
        $product['images'] =[];
        //图片（必须）
        $images=array_unique(array_filter($crawler->filter('.zoom-img')->each(function(Crawler $node,$i){
            return  $node->attr('src');
        })));
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    
    //    print_r($images);exit;
    //   商品的尺寸
        // $product['variations']=[];
        // print_r($product);exit;
      
        if($crawler->filter('.custom-dropdown-options')->count()>0){$product['variations'][]= [
                'name'=>$crawler->filter('[class="custom-dropdown-selection jsCustomDropdownSelection"] span')->text(),
                'options'=>$crawler->filter('[class="custom-dropdown-option jsCustomDropdownOption"]')->each(function (Crawler $node, $i) {
                    return $node->attr('data-value');
                })
            ];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}