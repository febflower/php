<?php
namespace app\command\MZL\Gr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class thefash extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:thefash')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.thefashionproject.gr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['https://www.thefashionproject.gr/gynaikeia-wedge-sneakers/ps/3/?category_id=552','https://www.thefashionproject.gr/gynaikeia-sneakers-mpotakia/ps/3/?category_id=475','https://www.thefashionproject.gr/gynaikeia-xamila-sneakers/ps/3/?category_id=476','https://www.thefashionproject.gr/gynaikeia-xamila-sneakers/ps/3/?category_id=476&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-chunky-sneakers/ps/3/?category_id=477','https://www.thefashionproject.gr/gynaikeia-flatform-sneakers/ps/3/?category_id=478','https://www.thefashionproject.gr/gynaikeia-slip-on-sneakers/ps/3/?category_id=479','https://www.thefashionproject.gr/gynaikeia-panina-sneakers/ps/3/?category_id=481','https://www.thefashionproject.gr/gynaikeia-flat-loafers/ps/3/?category_id=557','https://www.thefashionproject.gr/gynaikeia-loafers-me-takouni/ps/3/?category_id=558','https://www.thefashionproject.gr/gynaikeia-suede-loafers/ps/3/?category_id=580','https://www.thefashionproject.gr/gynaikeia-chunky-loafers/ps/3/?category_id=581','https://www.thefashionproject.gr/gynaikeia-loafers-loustrini/ps/3/?category_id=582','https://www.thefashionproject.gr/gynaikeies-mpalarines-/ps/3/?category_id=19','https://www.thefashionproject.gr/gynaikeies-flat-mpotes/ps/3/?category_id=31','https://www.thefashionproject.gr/gynaikeies-mpotes-kaltsa/ps/3/?category_id=559','https://www.thefashionproject.gr/gynaikeies-mpotes-ippasias/ps/3/?category_id=560','https://www.thefashionproject.gr/gynaikeies-mpotes-chunky/ps/3/?category_id=561','https://www.thefashionproject.gr/gynaikeies-mpotes-me-kordonia/ps/3/?category_id=572','https://www.thefashionproject.gr/gynaikeies-mpotes-cowboy/ps/3/?category_id=586','https://www.thefashionproject.gr/gynaikeies-mpotes-me-takouni/ps/3/?category_id=32','https://www.thefashionproject.gr/gynaikeies-mpotes-panw-apo-to-gonato/ps/3/?category_id=26'];
       $array2=['https://www.thefashionproject.gr/gynaikeia-flat-mpotakia/ps/3/?category_id=543','https://www.thefashionproject.gr/gynaikeia-flat-mpotakia/ps/3/?category_id=543&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-flat-mpotakia/ps/3/?category_id=543&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-biker-mpotakia/ps/3/?category_id=544','https://www.thefashionproject.gr/gynaikeia-mpotakia-kaltsa/ps/3/?category_id=57','https://www.thefashionproject.gr/gynaikeia-mpotakia-me-takouni/ps/3/?category_id=58','https://www.thefashionproject.gr/gynaikeia-mpotakia-me-takouni/ps/3/?category_id=58&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-mpotakia-me-takouni/ps/3/?category_id=58&pagediv=3&is_m=1','https://www.thefashionproject.gr/gynaikeia-mpotakia-me-gouna/ps/3/?category_id=589','https://www.thefashionproject.gr/gynaikeia-arvulakia/ps/3/?category_id=59','https://www.thefashionproject.gr/gynaikeia-arvulakia/ps/3/?category_id=59&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-arvulakia/ps/3/?category_id=59&pagediv=3&is_m=1','https://www.thefashionproject.gr/gynaikeia-mpotakia-astragalou/ps/3/?category_id=60','https://www.thefashionproject.gr/gynaikeia-mpotakia-astragalou/ps/3/?category_id=60&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-mpotakia-chelsea/ps/3/?category_id=584'];
       $array3=['https://www.thefashionproject.gr/gynaikeies-goves-xamilo-takouni/ps/3/?category_id=545','https://www.thefashionproject.gr/gynaikeies-goves-psilo-takouni/ps/3/?category_id=546','https://www.thefashionproject.gr/gynaikeies-goves-psilo-takouni/ps/3/?category_id=546&pagediv=3&is_m=1','https://www.thefashionproject.gr/gynaikeies-goves-xontro-takouni/ps/3/?category_id=547','https://www.thefashionproject.gr/gynaikeies-goves-lepto-takouni/ps/3/?category_id=548','https://www.thefashionproject.gr/gynaikeies-goves-lepto-takouni/ps/3/?category_id=548&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeies-goves-me-strass/ps/3/?category_id=575','https://www.thefashionproject.gr/gynaikeia-pedila/ps/3/?category_id=23','https://www.thefashionproject.gr/gynaikeia-pedila/ps/3/?category_id=23&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-pedila/ps/3/?category_id=23&pagediv=3&is_m=1','https://www.thefashionproject.gr/gynaikeia-pedila/ps/3/?category_id=23&pagediv=4&is_m=1','https://www.thefashionproject.gr/gynaikeia-flat-mules/ps/3/?category_id=470','https://www.thefashionproject.gr/gynaikeia-mules-me-xamilo-takouni/ps/3/?category_id=471','https://www.thefashionproject.gr/gynaikeia-mules-me-psilo-takouni/ps/3/?category_id=472','https://www.thefashionproject.gr/gynaikeia-mules-me-psilo-takouni/ps/3/?category_id=472&pagediv=2&is_m=1'];
       $array4=['https://www.thefashionproject.gr/gynaikeia-flatforms/ps/3/?category_id=18','https://www.thefashionproject.gr/gynaikeia-flatforms/ps/3/?category_id=18&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeies-pantofles-slides-flip-flop/ps/3/?category_id=108','https://www.thefashionproject.gr/gynaikeies-platformes/ps/3/?category_id=24','https://www.thefashionproject.gr/gynaikeies-platformes/ps/3/?category_id=24&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeies-espantrigies/ps/3/?category_id=17'];
       $array5=['https://www.thefashionproject.gr/dermatina-xeiropoiita-sandalia/ps/3/?category_id=248','https://www.thefashionproject.gr/dermatina-xeiropoiita-sandalia/ps/3/?category_id=248&pagediv=2&is_m=1','https://www.thefashionproject.gr/dermatina-xeiropoiita-sandalia/ps/3/?category_id=248&pagediv=3&is_m=1','https://www.thefashionproject.gr/dermatina-xeiropoiita-sandalia/ps/3/?category_id=248&pagediv=4&is_m=1','https://www.thefashionproject.gr/dermatina-xeiropoiita-sandalia/ps/3/?category_id=248&pagediv=5&is_m=1','https://www.thefashionproject.gr/dermatina-xeiropoiita-sandalia/ps/3/?category_id=248&pagediv=6&is_m=1','https://www.thefashionproject.gr/gynaikeia-sandalia-me-kordonia/ps/3/?category_id=412','https://www.thefashionproject.gr/gynaikeia-sandalia-me-stras/ps/3/?category_id=413','https://www.thefashionproject.gr/gynaikeia-sandalia-me-stras/ps/3/?category_id=413&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-sandalia-me-trouks/ps/3/?category_id=414','https://www.thefashionproject.gr/gynaikeia-crossover-sandalia/ps/3/?category_id=417','https://www.thefashionproject.gr/gynaikeia-crossover-sandalia/ps/3/?category_id=417&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-ankle-strap-sandalia/ps/3/?category_id=418','https://www.thefashionproject.gr/gynaikeia-ankle-strap-sandalia/ps/3/?category_id=418&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-sandalia-gia-gamo/ps/3/?category_id=420'];
       $array6=['https://www.thefashionproject.gr/gynaikeia-parka-mpoufan/ps/4/?category_id=70','https://www.thefashionproject.gr/gynaikeia-palto/ps/4/?category_id=71','https://www.thefashionproject.gr/gynaikeia-jean-panwforia/ps/4/?category_id=72','https://www.thefashionproject.gr/gynaikeia-mpoufan/ps/4/?category_id=73','https://www.thefashionproject.gr/gynaikeies-zaketes/ps/4/?category_id=74','https://www.thefashionproject.gr/gynaikeia-gileka/ps/4/?category_id=76','https://www.thefashionproject.gr/gynaikeia-jackets/ps/4/?category_id=135','https://www.thefashionproject.gr/gynaikeia-kimono-tounik/ps/4/?category_id=120','https://www.thefashionproject.gr/gynaikeia-plekta-top/ps/4/?category_id=563','https://www.thefashionproject.gr/gynaikeia-zivago/ps/4/?category_id=564','https://www.thefashionproject.gr/gynaikeia-plekta-psilos-giakas/ps/4/?category_id=570','https://www.thefashionproject.gr/gynaikeia-poulover/ps/4/?category_id=77','https://www.thefashionproject.gr/gynaikeies-plektes-mplouzes/ps/4/?category_id=585','https://www.thefashionproject.gr/gynaikeies-plektes-zaketes/ps/4/?category_id=78','https://www.thefashionproject.gr/gynaikeia-crop-tops/ps/4/?category_id=490','https://www.thefashionproject.gr/gynaikeia-kormakia/ps/4/?category_id=111','https://www.thefashionproject.gr/gynaikeia-saten-poukamisa/ps/4/?category_id=498','https://www.thefashionproject.gr/gynaikeia-poukamisa-me-print/ps/4/?category_id=499','https://www.thefashionproject.gr/gynaikeia-ufasmatina-poukamisa/ps/4/?category_id=500'];
       $array7=['https://www.thefashionproject.gr/gynaikeia-tzin-pantelonia/ps/4/?category_id=36','https://www.thefashionproject.gr/gynaikeia-tzin-pantelonia/ps/4/?category_id=36&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-skinny-pantelonia/ps/4/?category_id=39','https://www.thefashionproject.gr/gynaikeia-boyfriend-pantelonia/ps/4/?category_id=40','https://www.thefashionproject.gr/gynaikeia-chino-pantelonia/ps/4/?category_id=483','https://www.thefashionproject.gr/gynaikeia-ufasmatina-pantelonia/ps/4/?category_id=485','https://www.thefashionproject.gr/gynaikeia-ufasmatina-pantelonia/ps/4/?category_id=485&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-pantelonia-dermatini/ps/4/?category_id=486','https://www.thefashionproject.gr/gynaikeies-pantelones/ps/4/?category_id=488','https://www.thefashionproject.gr/gynaikeia-kolan/ps/4/?category_id=66','https://www.thefashionproject.gr/gynaikeia-cargo-pantelonia/ps/4/?category_id=68','https://www.thefashionproject.gr/gynaikeia-loungewear-tops/ps/4/?category_id=501','https://www.thefashionproject.gr/gynaikeia-loungewear-bottoms/ps/4/?category_id=502','https://www.thefashionproject.gr/gynaikeia-loungewear-sets/ps/4/?category_id=503','https://www.thefashionproject.gr/gynaikeies-oloswmes-formes-kontes/ps/4/?category_id=504','https://www.thefashionproject.gr/gynaikeies-oloswmes-formes-makries/ps/4/?category_id=505','https://www.thefashionproject.gr/gynaikeies-oloswmes-formes-me-print/ps/4/?category_id=506','https://www.thefashionproject.gr/gynaikeies-foustes/ps/4/?category_id=35','https://www.thefashionproject.gr/gynaikeia-sortsakia/ps/4/?category_id=64','https://www.thefashionproject.gr/gynaikeia-sortsakia/ps/4/?category_id=64&pagediv=2&is_m=1'];
       $array8=['https://www.thefashionproject.gr/gynaikeies-midi-foustes/ps/4/?category_id=533','https://www.thefashionproject.gr/gynaikeies-mini-foustes/ps/4/?category_id=534','https://www.thefashionproject.gr/gynaikeies-maxi-foustes/ps/4/?category_id=535','https://www.thefashionproject.gr/gynaikeia-plekta-foremata/ps/4/?category_id=562','https://www.thefashionproject.gr/gynaikeia-emprime-foremata/ps/4/?category_id=590','https://www.thefashionproject.gr/gynaikeia-vradina-foremata/ps/4/?category_id=137','https://www.thefashionproject.gr/gynaikeia-vradina-foremata/ps/4/?category_id=137&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-mini-foremata/ps/4/?category_id=446','https://www.thefashionproject.gr/gynaikeia-mini-foremata/ps/4/?category_id=446&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-maxi-foremata/ps/4/?category_id=447','https://www.thefashionproject.gr/gynaikeia-midi-foremata/ps/4/?category_id=448','https://www.thefashionproject.gr/gynaikeia-asummetra-foremata/ps/4/?category_id=449','https://www.thefashionproject.gr/gynaikeia-floral-foremata/ps/4/?category_id=452','https://www.thefashionproject.gr/gynaikeia-shirts/ps/4/?category_id=565','https://www.thefashionproject.gr/gynaikeia-fouter/ps/4/?category_id=566','https://www.thefashionproject.gr/gynaikeia-t-shirts/ps/4/?category_id=489','https://www.thefashionproject.gr/gynaikeia-mplouzakia-me-stampa/ps/4/?category_id=82','https://www.thefashionproject.gr/gynaikeia-set-rouxwn/ps/4/?category_id=104','https://www.thefashionproject.gr/gynaikeia-set-rouxwn/ps/4/?category_id=104&pagediv=2&is_m=1'];
       $array9=['https://www.thefashionproject.gr/gynaikeia-see-through-eswrouxa/ps/358/?category_id=540','https://www.thefashionproject.gr/gynaikeia-eswrouxa-me-dantela/ps/358/?category_id=542','https://www.thefashionproject.gr/gynaikeia-eswrouxa-me-dantela/ps/358/?category_id=542&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-set-eswrouxa/ps/358/?category_id=359','https://www.thefashionproject.gr/gynaikeies-pitzames/ps/358/?category_id=591','https://www.thefashionproject.gr/gynaikeia-bodies/ps/358/?category_id=360','https://www.thefashionproject.gr/gynaikeia-soutien/ps/358/?category_id=384','https://www.thefashionproject.gr/gynaikeia-velvet-eswrouxa/ps/358/?category_id=571','https://www.thefashionproject.gr/gynaikeia-set-eswrouxa-me-zartieres/ps/358/?category_id=403'];
       $array10=['https://www.thefashionproject.gr/gynaikeia-oloswma-magio/ps/91/?category_id=121','https://www.thefashionproject.gr/gynaikeia-bikini-bottoms/ps/91/?category_id=123','https://www.thefashionproject.gr/gynaikeia-3-pieces-swimsuits/ps/91/?category_id=421','https://www.thefashionproject.gr/gynaikeia-basic-magio/ps/91/?category_id=424','https://www.thefashionproject.gr/gynaikeia-basic-magio/ps/91/?category_id=424&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-basic-magio/ps/91/?category_id=424&pagediv=3&is_m=1','https://www.thefashionproject.gr/gynaikeia-strapless-magio/ps/91/?category_id=425','https://www.thefashionproject.gr/gynaikeia-animal-print-magio/ps/91/?category_id=426'];
       $array11=['https://www.thefashionproject.gr/gynaikeia-ribbed-trend-magio/ps/91/?category_id=427','https://www.thefashionproject.gr/gynaikeia-ribbed-trend-magio/ps/91/?category_id=427&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-psilomesa-magio/ps/91/?category_id=428','https://www.thefashionproject.gr/gynaikeia-psilomesa-magio/ps/91/?category_id=428&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-gualistera-magio/ps/91/?category_id=433','https://www.thefashionproject.gr/gynaikeia-floral-magio/ps/91/?category_id=437','https://www.thefashionproject.gr/gynaikeia-magio-me-stathero-v/ps/91/?category_id=432','https://www.thefashionproject.gr/gynaikeia-one-shoulder-magio/ps/91/?category_id=438'];
       $array12=['https://www.thefashionproject.gr/gynaikeia-sandalia-me-kordonia/ps/3/?category_id=412','https://www.thefashionproject.gr/gynaikeia-sandalia-me-stras/ps/3/?category_id=413','https://www.thefashionproject.gr/gynaikeia-sandalia-me-stras/ps/3/?category_id=413&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-sandalia-me-trouks/ps/3/?category_id=414','https://www.thefashionproject.gr/gynaikeia-sandalia-me-petres/ps/3/?category_id=415','https://www.thefashionproject.gr/gynaikeia-crossover-sandalia/ps/3/?category_id=417','https://www.thefashionproject.gr/gynaikeia-crossover-sandalia/ps/3/?category_id=417&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-crossover-sandalia/ps/3/?category_id=417&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-sporty-sandalia/ps/3/?category_id=416','https://www.thefashionproject.gr/gynaikeia-ankle-strap-sandalia/ps/3/?category_id=418','https://www.thefashionproject.gr/gynaikeia-ankle-strap-sandalia/ps/3/?category_id=418&pagediv=2&is_m=1','https://www.thefashionproject.gr/gynaikeia-sandalia-gia-gamo/ps/3/?category_id=420','https://www.thefashionproject.gr/gynaikeia-sandalia-gia-gamo/ps/3/?category_id=420&pagediv=2&is_m=1'];
    //    $array13=['https://www.aboutyou.gr/c/ginaikes/roukha/mpoyfan/xeimerina-mpoyfan-81541'];


        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'thefash01.seo077.site'],
            'x2' => ['source' => $array2, 'target' =>'thefash02.seo077.site'],
            'x3' => ['source' => $array3, 'target' =>'thefash03.seo077.site'],
            'x4' => ['source' => $array4, 'target' =>'thefash04.seo077.site'],
            'x5' => ['source' => $array5, 'target' =>'thefash05.seo077.site'],
            'x6' => ['source' => $array6, 'target' =>'thefash06.seo077.site'],
            'x7' => ['source' => $array7, 'target' =>'thefash07.seo077.site'],
            'x8' => ['source' => $array8, 'target' =>'thefash08.seo077.site'],
            'x9' => ['source' => $array9, 'target' =>'thefash09.seo077.site'],
            'x10' => ['source' => $array10, 'target' =>'thefash10.seo077.site'],
            'x11' => ['source' => $array11, 'target' =>'thefash11.seo077.site'],
            'x12' => ['source' => $array12, 'target' =>'thefash12.seo077.site'],
            // 'x13' => ['source' => $array13, 'target' =>'hxflla'],

      
            
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.thefashionproject.gr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);

        
            $nextNode = $crawler->filter('[rel="next"]');
        //    echo $nextNode;exit;
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
       
        $breadcrumbs = array_filter($crawler->filter('.title_path')->eq(0)->filter('h2 a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;use($breadcrumbs)
        // 进入详情页爬取数据
        $crawler->filter('.product_item_image a')->each(function (Crawler $node, $i) use($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                   
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.product_area_title')->text();  //标题（必须）
        // print_r($product['title']);exit;

        // $pricedel=[',','€','$'];
        // $product['price'] = str_replace($pricedel,'',$crawler->filter('.products_price_new')->text()); 

        $product['price']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(3)->text(),true)['offers']['price'];

        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.products_price_new')->text())));  //价格（必须）

        // print_r($product['price']);exit;



        // $product['brand']=$crawler->filter('.main-prod-info h1')->text();
       //品牌（必须）

       $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(3)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('.bread li a')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
   
        $product['sku'] =$crawler->filter('.product_code')->text(); 
        // $del= [';'];
        // $int = $crawler->filter('script')->eq(29)->text();  //产品编号（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['sku']=json_decode($it,true)['id'];
        // print_r($product['sku']);exit;
       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];
        $product['description']=$crawler->filter('.product_text')->html();

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        $del=['•',':'];
        if($crawler->filter('.product_f_details_area')->count()) {
            $count = $crawler->filter('.product_f_details_area')->filter('.product_f_details_item')->count();
            // echo $count;exit;
                for($i=0;$i<$count-1;$i++){
                    $product['attributes'][] = [
                        'name' =>str_replace($del,'',$crawler->filter('[class="product_f_det_item bold"]')->eq($i)->text()),
                        'options' =>$crawler->filter('[class="product_f_det_item"]')->eq($i)->text(),
                    ];
                }       
            
        }else{
            $product['attributes'] = [];
        }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        // $product['gender']='';  x8使用
        $product['gender'] = '';
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $img=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$img){
        //     if(strstr($node->text(),"image")){
        //         $img = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('[class="owl-lazy img-responsive"]')->each(function(Crawler $node,$i){
            return  $node->attr('data-src');
        })));
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    //    print_r($images);exit;
    //   商品的尺寸
        $product['variations']=[];
        // print_r($product);exit;
      
        if($crawler->filter('.product_sizes')->count()>0){$product['variations'][]= [
                'name'=>'μέγεθος',
                'options'=>$crawler->filter('.number_item_in_in')->each(function (Crawler $node, $i) {
                    return $node->text();
                })
            ];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}