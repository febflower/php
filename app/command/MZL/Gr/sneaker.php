<?php
namespace app\command\MZL\Gr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class sneaker extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:sneaker')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.sneaker10.gr');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['https://www.sneaker10.gr/el/10829-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1','https://www.sneaker10.gr/el/10829-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%84%CE%B1%CE%BA%CE%B9%CE%B1','https://www.sneaker10.gr/el/10829-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=slides','https://www.sneaker10.gr/el/10829-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CE%BD%CF%84%CE%BF%CF%86%CE%BB%CE%B5%CF%83','https://www.sneaker10.gr/el/10829-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%B1%CE%BD%CE%B4%CE%B1%CE%BB%CE%B9%CE%B1'];
       $array2=['https://www.sneaker10.gr/el/10832-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1','https://www.sneaker10.gr/el/10832-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%84%CE%B1%CE%BA%CE%B9%CE%B1','https://www.sneaker10.gr/el/10832-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=slides','https://www.sneaker10.gr/el/10832-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CE%BD%CF%84%CE%BF%CF%86%CE%BB%CE%B5%CF%83','https://www.sneaker10.gr/el/10832-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%B1%CE%BD%CE%B4%CE%B1%CE%BB%CE%B9%CE%B1'];
       $array3=['https://www.sneaker10.gr/el/10842-andrika?brand=nike&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=adidas-originals&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=jordan&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=alpha-industries&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=carhartt-wip&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=reebok-classics&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=levi-s&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=dickies&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=grimey&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=huf&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=hurley&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=lacoste&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=obey&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=polo-ralph-lauren&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=slaps&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=superdry&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=the-dudes&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=timberland&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10842-andrika?brand=tommy-jeans&%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts'];
       $array4=['https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B6%CE%B1%CE%BA%CE%B5%CF%84%CE%B5%CF%83','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CF%86%CE%BF%CF%85%CF%84%CE%B5%CF%81','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1'];
       $array5=['https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%BC%CE%B1%CE%B3%CE%B9%CE%BF','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=cargo-%CF%83%CE%BF%CF%81%CF%84%CF%83','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B5%CF%83%CF%89%CF%81%CE%BF%CF%85%CF%87%CE%B1','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B1%CE%BA%CF%81%CF%85-%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B9',];
       $array6=['https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%85%CF%86%CE%B1%CE%BD','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=cargo-pants','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=chinos','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=loungewear','https://www.sneaker10.gr/el/10842-andrika?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=chinos'];
       $array7=['https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B6%CE%B1%CE%BA%CE%B5%CF%84%CE%B5%CF%83','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CF%86%CE%BF%CF%85%CF%84%CE%B5%CF%81','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CE%B1%CE%B3%CE%B9%CE%BF','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=biker-shorts','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BA%CE%BF%CE%BB%CE%B1%CE%BD','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%B4%CE%B9%CE%B1%CE%B2%CF%81%CE%BF%CF%87%CE%B1','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B5%CF%83%CF%89%CF%81%CE%BF%CF%85%CF%87%CE%B1','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B1%CE%BA%CF%81%CF%85-%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B9','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%85%CF%83%CF%84%CE%B1%CE%BA%CE%B9%CE%B1','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%85%CF%86%CE%B1%CE%BD','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%85%CF%86%CE%B1%CE%BD,%CF%84%CE%B6%CE%B9%CE%BD','https://www.sneaker10.gr/el/10838-gunaikeia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%85%CF%86%CE%B1%CE%BD,%CF%86%CE%BF%CF%81%CE%B5%CE%BC%CE%B1%CF%84%CE%B1',];
      
       //  需要更改个别规则  
       $array8=['https://www.sneaker10.gr/el/10788-basketball?%CE%BA%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%B9%CE%B1=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1','https://www.sneaker10.gr/el/10788-basketball?%CE%BA%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%B9%CE%B1=%CF%81%CE%BF%CF%85%CF%87%CE%B1','https://www.sneaker10.gr/el/10788-basketball?%CE%BA%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%B9%CE%B1=%CE%B1%CE%BE%CE%B5%CF%83%CE%BF%CF%85%CE%B1%CF%81','https://www.sneaker10.gr/el/10788-basketball?%CE%BA%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%B9%CE%B1=%CE%B1%CE%B8%CE%BB%CE%B7%CF%84%CE%B9%CE%BA%CE%BF%CF%83-%CE%B5%CE%BE%CE%BF%CF%80%CE%BB%CE%B9%CF%83%CE%BC%CE%BF%CF%83'];
      //
       
       $array9=['https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=4','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=5','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B6%CE%B1%CE%BA%CE%B5%CF%84%CE%B5%CF%83','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1&p=2','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=2','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=3','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=4','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BA%CE%B1%CE%BB%CF%84%CF%83%CE%B5%CF%83','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BA%CE%B1%CE%BB%CF%84%CF%83%CE%B5%CF%83&p=2','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%B1%CE%BB%CE%B5%CF%83-basketball','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%B1%CE%BB%CE%B5%CF%83-basketball&p=2','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%85%CF%86%CE%B1%CE%BD','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CF%81%CE%B5%CE%BB%CE%BF%CE%BA','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=2','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=3','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=4','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=5','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=miniatures','https://www.slamdunk.gr/el/44348-nba?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=miniatures&p=2'];
       $array10=['https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1&p=2','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1&p=3','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%80%CE%B1%CF%80%CE%BF%CF%85%CF%84%CF%83%CE%B9%CE%B1&p=4','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%84%CE%B1%CE%BA%CE%B9%CE%B1','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%84%CE%B1%CE%BA%CE%B9%CE%B1&p=2','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%84%CE%B1%CE%BA%CE%B9%CE%B1&p=3','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BF%CF%84%CE%B1%CE%BA%CE%B9%CE%B1&p=4','https://www.slamdunk.gr/el/32843-papoutsia?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=slides','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1&p=3','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B1%CE%BC%CE%B1%CE%BD%CE%B9%CE%BA%CE%B1&p=4','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=3','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=4','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=5','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=6','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=7','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=8','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%83%CE%BF%CF%81%CF%84%CF%83-%CE%B2%CE%B5%CF%81%CE%BC%CE%BF%CF%85%CE%B4%CE%B5%CF%83&p=9'];
       $array11=['https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=3','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=4','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=5','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=6','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=7','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=8','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=9','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=10','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=11','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=12','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=13','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=14','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=t-shirts&p=15','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83&p=3','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83&p=4','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CF%86%CE%BF%CF%81%CE%BC%CE%B5%CF%83&p=5','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B6%CE%B1%CE%BA%CE%B5%CF%84%CE%B5%CF%83','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%B6%CE%B1%CE%BA%CE%B5%CF%84%CE%B5%CF%83&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CF%86%CE%BF%CF%85%CF%84%CE%B5%CF%81','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CF%86%CE%BF%CF%85%CF%84%CE%B5%CF%81&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1&p=3','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1&p=4','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1&p=5','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=2','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=3','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=4','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=5','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=6','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=7','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=8','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=9','https://www.slamdunk.gr/el/32846-rouxa?%CE%B5%CE%B9%CE%B4%CE%BF%CF%83=%CE%BC%CF%80%CE%BB%CE%BF%CF%85%CE%B6%CE%B1-%CE%BC%CE%B5-%CE%BA%CE%BF%CF%85%CE%BA%CE%BF%CF%85%CE%BB%CE%B1,%CF%86%CE%B1%CE%BD%CE%B5%CE%BB%CE%B5%CF%83-%CE%BF%CE%BC%CE%B1%CE%B4%CF%89%CE%BD&p=10'];



        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'sneaker001.seo070.site'],
            'x2' => ['source' => $array2, 'target' =>'sneaker002.seo071.site'],
            'x3' => ['source' => $array3, 'target' =>'sneaker003.seo071.site'],
            'x4' => ['source' => $array4, 'target' =>'sneaker004.seo071.site'],
            'x5' => ['source' => $array5, 'target' =>'sneaker005.seo071.site'],
            'x6' => ['source' => $array6, 'target' =>'sneaker006.seo071.site'],
            'x7' => ['source' => $array7, 'target' =>'sneaker007.seo071.site'],
            'x8' => ['source' => $array8, 'target' =>'sneaker008.seo071.site'],
            'x9' => ['source' => $array9, 'target' =>'sneaker009.seo071.site'],
            'x10' => ['source' => $array10, 'target' =>'sneaker0010.seo072.site'],
            'x11' => ['source' => $array11, 'target' =>'sneaker0011.seo072.site'],
            


        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sneaker10.gr',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));exit;
//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.product_img_link')->each(function (Crawler $node, $i)  {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('[class="pb-right-column col-xs-12 col-sm-4 col-md-3"] h1')->text();  //标题（必须）
        // print_r($product['title']);exit;

        $pricedel=[',','€','$'];
        $product['price'] = str_replace($pricedel,'',$crawler->filter('.stroke_initial_price')->text()); 
        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.priceContainer__price')->text())));  //价格（必须）

        // print_r($product['price']);exit;

        // $product['brand']='';
       //品牌（必须）

       $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb span a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
        $product['sku'] =$crawler->filter('.descriptionTabProductSKU span')->text();  //产品编号（必须）
        // print_r($product['sku']);exit;
       
         $product['short_description']='';
    
        
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
          if($crawler->filter('[class="active section_native_menu tab_content_1 page-product-box tab-pane"] p')->count()>0){
        $product['description']=$crawler->filter('[class="active section_native_menu tab_content_1 page-product-box tab-pane"]')->filter('.descriptionTabProductName')->html().$crawler->filter('[class="active section_native_menu tab_content_1 page-product-box tab-pane"] div p')->html();
          }else{
            $product['description']=$crawler->filter('[class="active section_native_menu tab_content_1 page-product-box tab-pane"]')->filter('.descriptionTabProductName')->html(); //描述  
          }
        
        // print_r($product['description']);exit;
        $product['attributes'] = [];
        //属性   x1 x2 使用
        // $del=[':'];
        // if($crawler->filter('.d-md-none')->count()) {
        //     $count = $crawler->filter('[class="blue-info d-md-none "] ul li')->count();
        //     // echo $count;exit;
        //     if($crawler->filter('[class="color-red"]')->count()){
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }       
        //     }else{
        //         for($i=0;$i<$count;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>str_replace($del,'',$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('strong')->text()),
        //                 'options' =>$crawler->filter('[class="list-inline attributes-list"] li')->eq($i)->filter('td')->text(),
        //             ];
        //         }
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']='';  
        // $product['gender'] = $crawler->filter('[class="breadcrumb clearfix"]')->filter('span')->eq(5)->filter('a')->attr('title');
        // print_r($product['gender']);exit;
        if($crawler->filter('[class="color_pick selected"]')->count()>0){
            $product['color'] =$crawler->filter('[class="color_pick selected"]')->attr('title');
        }else{
            $product['color']='';
        }
       
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'][] =[];
        $product['images'] =[];
        //图片（必须）
        $images=array_unique(array_filter($crawler->filter('.bigPic')->each(function(Crawler $node,$i){
            return  $node->attr('src');
        })));
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    
    //    print_r($images);exit;
    //   商品的尺寸
        // $product['variations']=[];
        // print_r($product);exit;
        $del=['- Last Piece!'];
        if($crawler->filter('.attribute_list')->count()>0){$product['variations'][]= [
                'name'=>$crawler->filter('[class="attribute_radio defaultSizeValue"]')->attr('value'),
                'options'=>array_unique(array_filter(str_replace($del,'',$crawler->filter('.radioPickList li')->nextAll()->filter('span')->each(function (Crawler $node, $i) {
                    return $node->text();
                }))))
            ];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}