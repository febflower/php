<?php
namespace app\command\MZL\Fr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class labonne extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:labonne')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.labonnepointure.fr/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
         $array1=['https://www.labonnepointure.fr/femme_89_botte-pluie-neige_.html','https://www.labonnepointure.fr/femme_95_chaussure-montante_.html','https://www.labonnepointure.fr/femme_83_bottine_.html','https://www.labonnepointure.fr/femme_36_botte_.html','https://www.labonnepointure.fr/femme_44_chaussure-basse_.html','https://www.labonnepointure.fr/basket.html'];
         $array2=['https://www.labonnepointure.fr/femme_52_basket-et-sport_.html','https://www.labonnepointure.fr/femme_19_escarpin_.html','https://www.labonnepointure.fr/femme_43_ballerine_.html','https://www.labonnepointure.fr/femme_53_chaussure-en-toile_.html','https://www.labonnepointure.fr/femme_54_chaussure-aeree_.html','https://www.labonnepointure.fr/femme_31_boots_.html'];
         $array3=['https://www.labonnepointure.fr/femme_84_pantoufle-chausson_.html','https://www.labonnepointure.fr/femme_78_sabot-woz_.html','https://www.labonnepointure.fr/femme_27_entre-doigts_.html','https://www.labonnepointure.fr/femme_24_sabot-mode_.html','https://www.labonnepointure.fr/femme_28_nu-pieds_.html','https://www.labonnepointure.fr/femme_20_mule_.html'];
         $array4=['https://www.labonnepointure.fr/homme_12_boots_.html','https://www.labonnepointure.fr/homme_55_ville-travail_.html','https://www.labonnepointure.fr/homme_9_chaussure-de-sport_.html','https://www.labonnepointure.fr/homme_17_basket_.html'];
         $array5=['https://www.labonnepointure.fr/homme_85_pantoufle-et-chausson_.html','https://www.labonnepointure.fr/homme_81_mule-sabot-et-tong_.html','https://www.labonnepointure.fr/homme_81_mule-sabot-et-tong_.html','https://www.labonnepointure.fr/homme_10_mocassin_.html','https://www.labonnepointure.fr/homme_11_chaussure-habillee_.html',''];
         $array6=['https://www.labonnepointure.fr/junior_90_bottillon_.html','https://www.labonnepointure.fr/junior_39_sport_.html','https://www.labonnepointure.fr/junior_60_basket-ville_.html','https://www.labonnepointure.fr/junior_61_sandale_.html','https://www.labonnepointure.fr/chausson-et-pantoufle.html'];
         $array7=['https://www.labonnepointure.fr/enfant_57_botte-et-bottillon_.html','https://www.labonnepointure.fr/enfant_59_basket_.html','https://www.labonnepointure.fr/enfant_71_chaussure-en-toile_.html','https://www.labonnepointure.fr/enfant_33_nu-pieds_.html','https://www.labonnepointure.fr/enfant_62_chaussure-de-ville_.html'];
         $array8=['https://www.labonnepointure.fr/bebe_32_bottillon_.html','https://www.labonnepointure.fr/bebe_93_chaussure_.html','https://www.labonnepointure.fr/bebe_65_basket_.html','https://www.labonnepointure.fr/bebe_77_sandale_.html','https://www.labonnepointure.fr/bebe_69_chausson-toile_.html'];
        //  $array9=['https://www.eram.fr/chaussures-femme/chaussures-femmes'];
         

        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'placede01.xms008.site'],
            'x2' => ['source' => $array2, 'target' =>'labonne02.xms009.site'],
            'x3' => ['source' => $array3, 'target' =>'labonne03.xms009.site'],
            'x4' => ['source' => $array4, 'target' =>'labonne04.xms009.site'],
            'x5' => ['source' => $array5, 'target' =>'labonne05.xms009.site'],
            'x6' => ['source' => $array6, 'target' =>'labonne06.xms009.site'],
            'x7' => ['source' => $array7, 'target' =>'labonne07.xms010.site'],
            'x8' => ['source' => $array8, 'target' =>'labonne08.xms010.site'],
           
            
            
            
            
            
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.labonnepointure.fr/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36','time.sleep(random.randint(1,3))',
            ],
        ]);
            // wordpress的密钥  time.sleep(random.randint(1,3)) html_file = requests.get(url)
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri) 
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[class="text-center pagination lg:text-left"] a');
            // $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
        // $gender = $crawler->filter('[itemprop="item"]')->eq(1)->filter('[itemprop="name"]')->text();
        // print_r($gender);exit;

         // $breadcrumbs=$crawler->filter('[class="row align-items-stretch"] a')->attr('href');
        // $a=explode("=",$breadcrumbs);
        // $a=explode("&",$a[1]);
        
        // $breadcrumbs =$crawler->filter('[rel="canonical"]')->attr('href');
        // $a=explode("p/",$breadcrumbs);
        // $a=explode('/',$a[1]);
        // print_r($a);exit;


        // 进入详情页爬取数据                                                                 // use ($a,$gender)
        $crawler->filter('.CategoryProducts a')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                    // 'breadcrumbs' => $a,
                    // 'gender'      => $gender
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        // $product['title']=json_decode($crawler->filter('')->text(),true)['name'];
        $product['title'] = json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['name'];  //标题（必须）
        // print_r($product['title']);exit;

        // $pricedel=[',','€','$'];
        // $product['price'] = str_replace($pricedel,'',$crawler->filter('.b-price-section')->text()); 
        $product['price']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['offers']['price'];

        // print_r($product['price']);exit;

        // $product['brand']=$crawler->filter('.b-pdp-brand')->text();
       //品牌（必须）

       $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

  
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.list-reset li a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->attr('title');
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        // array_push($product['breadcrumbs'],implode('',$item['breadcrumbs']));
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
   
        $product['sku'] =json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['sku']; 
        
        // print_r( $product['sku']);exit;  //产品编号（必须）
       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];   
        $product['description']=$crawler->filter('.wysiwyg')->filter('p')->eq(0)->html();

        // print_r($product['description']);exit;str_replace($descr,'',

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        
        if($crawler->filter('[class="grid overflow-hidden text-lg Accordion-expend sm:grid-cols-2 sm:gap-4"]')->count()) {
            $count = $crawler->filter('[class="grid overflow-hidden text-lg Accordion-expend sm:grid-cols-2 sm:gap-4"] dd')->count();
            // echo $count;exit;
                for($i=0;$i<$count;$i++){
                    $product['attributes'][] = [
                        'name' =>$crawler->filter('[class="grid overflow-hidden text-lg Accordion-expend sm:grid-cols-2 sm:gap-4"]')->filter('dt')->eq($i)->text(),
                        'options' =>$crawler->filter('[class="grid overflow-hidden text-lg Accordion-expend sm:grid-cols-2 sm:gap-4"]')->filter('dd')->eq($i)->text(),
                    ];
                }       
            
        }else{
            $product['attributes'] = [];
        }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']=$crawler->filter('.BreadCrumb-item a')->eq(1)->attr('title');  //性别
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $images=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$images){
        //     if(strstr($node->text(),"image")){
        //         $images = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('.glightbox')->each(function(Crawler $node,$i){
            return  $node->attr('href');
        })));
        // // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
        // print_r($product['images']);exit;
    //    print_r($images);exit;$crawler->filter('[class="selected default size-choice"]')->filter('.value')->text()
    //   商品的尺寸

 
        // $int = $crawler->filter('script')->eq(29)->text();  //价格（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['price']=json_decode($it,true)['price'];
       
        // print_r($it);exit;
        $io=[];

        $del= [';'];
            $int = $crawler->filter('[type="text/javascript"]')->eq(1)->text();
            $index = strpos($int,'UTES = ');
            $it=str_replace($del,'',substr($int,$index+6));


        if($crawler->filter('[type="text/javascript"]')->eq(1)->count()){
            $count =json_decode($it,true)[0]['values'];
            // print_r(count($count));exit;
              for($i=0;$i<count($count);$i++){
                   $io[]=json_decode($it,true)[0]['values'][$i]['label'];
              }
       } 
                // print_r($io);exit;
              if($crawler->filter('[type="text/javascript"]')->eq(1)->count()){
               $product['variations'][]= [
                    
                'name'=>json_decode($it,true)[0]['title'],
                'options'=>$io,

            ];   
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}