<?php
namespace app\command\MZL\Fr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class bessec extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:bessec')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.bessec-chaussures.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
        //  $array1=['https://www.placedestendances.com/fr/fr/american-vintage-femme/cat0/Manteau','https://www.placedestendances.com/fr/fr/american-vintage-femme/cat0/Pull','https://www.placedestendances.com/fr/fr/american-vintage-femme/cat0/Tee-Shirt','https://www.placedestendances.com/fr/fr/les-iconiques-av-femme'];
         $array1=['https://www.bessec-chaussures.com/chaussures-femme-ballerines-et-babies','https://www.bessec-chaussures.com/chaussures-femme-bottes','https://www.bessec-chaussures.com/chaussures-femme-chaussons','https://www.bessec-chaussures.com/chaussures-femme-chaussures-bateau','https://www.bessec-chaussures.com/chaussures-femme-derbies'];
         $array2=['https://www.bessec-chaussures.com/chaussures-femme-boots-et-bottines','https://www.bessec-chaussures.com/chaussures-femme-escarpins','https://www.bessec-chaussures.com/chaussures-femme-espadrilles'];
         $array3=['https://www.bessec-chaussures.com/chaussures-femme-mocassins','https://www.bessec-chaussures.com/chaussures-femme-sandales','https://www.bessec-chaussures.com/chaussures-femme-tongs-et-crocs'];
        //  切出来
         $array4=['https://www.bessec-chaussures.com/chaussures-femme-baskets-caoutchouc','https://www.bessec-chaussures.com/chaussures-femme-baskets-cuir','https://www.bessec-chaussures.com/chaussures-femme-baskets-nubuck','https://www.bessec-chaussures.com/chaussures-femme-baskets-synthetique','https://www.bessec-chaussures.com/chaussures-femme-baskets-textile'];
         
         $array5=['https://www.bessec-chaussures.com/chaussures-homme-baskets','https://www.bessec-chaussures.com/chaussures-homme-tongs-et-crocs','https://www.bessec-chaussures.com/chaussures-homme-mocassins','https://www.bessec-chaussures.com/chaussures-homme-sandales'];
         $array6=['https://www.bessec-chaussures.com/chaussures-homme-chaussons','https://www.bessec-chaussures.com/chaussures-homme-boots-et-bottines','https://www.bessec-chaussures.com/chaussures-homme-chaussures-a-lacets','https://www.bessec-chaussures.com/chaussures-homme-chaussures-bateau'];
         $array7=['https://www.bessec-chaussures.com/chaussures-fille-ballerines-et-babies','https://www.bessec-chaussures.com/chaussures-fille-baskets','https://www.bessec-chaussures.com/chaussures-fille-bottes','https://www.bessec-chaussures.com/chaussures-fille-chaussons','https://www.bessec-chaussures.com/chaussures-fille-tongs-et-crocs'];
         $array8=['https://www.bessec-chaussures.com/chaussures-fille-boots-et-bottines','https://www.bessec-chaussures.com/chaussures-fille-chaussures-basses','https://www.bessec-chaussures.com/chaussures-fille-chaussures-montantes','https://www.bessec-chaussures.com/chaussures-fille-sandales'];
         $array9=['https://www.bessec-chaussures.com/chaussures-fille-baskets','https://www.bessec-chaussures.com/chaussures-garcon-baskets','https://www.bessec-chaussures.com/chaussures-garcon-bottes','https://www.bessec-chaussures.com/chaussures-garcon-chaussons','https://www.bessec-chaussures.com/chaussures-garcon-chaussures-basses'];
         $array10=['https://www.bessec-chaussures.com/chaussures-garcon-chaussures-montantes','https://www.bessec-chaussures.com/chaussures-garcon-sandales','https://www.bessec-chaussures.com/chaussures-garcon-tongs-et-crocs','https://www.bessec-chaussures.com/chaussures-garcon-bottes'];

        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'bessec01.xms010.site'],
            'x2' => ['source' => $array2, 'target' =>'bessec02.xms010.site'],
            'x3' => ['source' => $array3, 'target' =>'bessec03.xms010.site'],
            'x4' => ['source' => $array4, 'target' =>'bessec04.xms010.site'],
            'x5' => ['source' => $array5, 'target' =>'bessec05.xms011.site'],
            'x6' => ['source' => $array6, 'target' =>'bessec06.xms011.site'],
            'x7' => ['source' => $array7, 'target' =>'thefash06.xms011.site'],
            'x8' => ['source' => $array8, 'target' =>'thefash07.xms011.site'],
            'x9' => ['source' => $array9, 'target' =>'bessec09.xms011.site'],
            'x10' => ['source' => $array10, 'target' =>'bessec10.xms011.site'],
            
            
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.bessec-chaussures.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36','time.sleep(random.randint(1,3))',
            ],
        ]);
            // wordpress的密钥  time.sleep(random.randint(1,3)) html_file = requests.get(url)
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri) 
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage('https://www.bessec-chaussures.com/'.$nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
        
        //  $breadcrumbs=$crawler->filter('[rel="canonical"]')->attr('href');
        // $a=explode("ts-",$breadcrumbs);
        // $a=explode("?",$a[1]);
        // print_r($a);exit;
        
        // $breadcrumbs =$crawler->filter('[rel="canonical"]')->attr('href');
        // $a=explode("p/",$breadcrumbs);
        // $a=explode('/',$a[1]);
        // print_r($a);exit;


        // 进入详情页爬取数据                                                                 //  use ($a)
        $crawler->filter('.product-card-summary a')->each(function (Crawler $node, $i){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " .$node->text(),
                    'url' => sprintf('%s','https://www.bessec-chaussures.com'.$node->attr('href')),
                    // 'breadcrumbs' => $a,
                    // 'gender'      => $gender
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        // $product['title']=json_decode($crawler->filter('')->text(),true)['name'];
        $product['title'] =$crawler->filter('[class="cell auto"]')->filter('[class="name"]')->text();  //标题（必须）
        // print_r($product['title']);exit;

        // $pricedel=[',','€','$'];
        // $product['price'] = str_replace($pricedel,'',$crawler->filter('.ins-price')->text()); 
        $product['price']=$crawler->filter('[class="lead price selling"]')->attr('content');

        // $del= [';'];
        // $int = $crawler->filter('script')->eq(29)->text();  //价格（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['price']=json_decode($it,true)['price'];

        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.ins-price')->text())));  //价格（必须）

        // print_r($product['price']);exit;

        // $product['brand']=$crawler->filter('.b-pdp-brand')->text();
       //品牌（必须）

    //    $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];
          $product['brand']=$crawler->filter('[class="cell auto text-right"] img')->attr('alt');
    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb li a')->filter('[itemprop="name"]')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        // array_push($product['breadcrumbs'],$item['breadcrumbs'][0]);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        

        $int =$crawler->filter('script')->eq(19)->text();
        $index=explode( '"',$int);
        $index=explode('";',$index[1]);
        $product['sku'] =$index[0]; 

        // $product['sku']=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['sku'];
        
        // print_r($product['sku']);exit;
       
        // print_r( $product['sku']);exit;  //产品编号（必须）
       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];   
        if($crawler->filter('#description p span')->count()){
            $product['description']=$crawler->filter('#description p span')->html();
        }else{
            $product['description']='';
        }
        

        // print_r($product['description']);exit;str_replace($descr,'',

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        
        if($crawler->filter('[class="compo-wrapper grid-x"]')->count()) {
            $count = $crawler->filter('[class="compo-wrapper grid-x"]')->filter('[class="compo-item cell small-12 medium-6 grid-x grid-padding-x grid-padding-y align-justify"]')->count();
            // echo $count;exit;
                for($i=0;$i<$count;$i++){
                    $product['attributes'][] = [
                        'name' =>$crawler->filter('[class="compo-item cell small-12 medium-6 grid-x grid-padding-x grid-padding-y align-justify"]')->eq($i)->filter('div')->eq(1)->text(),
                        'options' =>$crawler->filter('[class="compo-item cell small-12 medium-6 grid-x grid-padding-x grid-padding-y align-justify"]')->eq($i)->filter('div')->eq(2)->text(),
                    ];
                }       
            
        }else{
            $product['attributes'] = [];
        }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']=$crawler->filter('.breadcrumb')->filter('li a span')->eq(1)->text();  //性别
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $images=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$images){
        //     if(strstr($node->text(),"image")){
        //         $images = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('.orbit-slide img')->each(function(Crawler $node,$i){
            return 'https://www.bessec-chaussures.com'. $node->attr('src');
        })));
        // // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);// 用于图片上传失败
        // print_r($images);exit;
    //   商品的尺寸
        $product['variations']=[];
        if($crawler->filter('.shoes-size-field')->count()){
                 $product['variations'][]= [
                    
                'name'=>'Pointures',
                'options'=>$crawler->filter('[class="radio-field out-of-stock disabled"]')->filter('label')->each(function (Crawler $node,$i){
                    return $node->text();
                }),
            ];   
            $product['type'] = 'variable';
        }



        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}