<?php
namespace app\command\MZL\Fr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class intersport extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:intersport')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.lulli-sur-la-toile.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=VESTES&lulli_category_family%5B1%5D=GILETS+%2F+PONCHOS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_type%5B0%5D=CHEMISES&lulli_category_type%5B1%5D=BLOUSES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=MANTEAUX'];
       $array2=['https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=PULLS&lulli_category_family%5B1%5D=SWEATSHIRTS&lulli_category_type%5B0%5D=PULLS+COL+BATEAU','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=PULLS&lulli_category_family%5B1%5D=SWEATSHIRTS&lulli_category_type%5B0%5D=PULLS+COL+ROND','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=PULLS&lulli_category_family%5B1%5D=SWEATSHIRTS&lulli_category_type%5B0%5D=PULLS+COL+ROUL%C3%89+%2F+CHEMIN%C3%89E','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=PULLS&lulli_category_family%5B1%5D=SWEATSHIRTS&lulli_category_type%5B0%5D=PULLS+COL+V','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=PULLS&lulli_category_family%5B1%5D=SWEATSHIRTS&lulli_category_type%5B0%5D=SWEATSHIRTS','https://www.lulli-sur-la-toile.com/tendance/ensemble.html','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_type%5B0%5D=TOPS&lulli_category_type%5B1%5D=TEE-SHIRTS'];
       $array3=['https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JEANS&lulli_category_family%5B1%5D=PANTALONS&lulli_category_type%5B0%5D=JEANS+BOYFRIEND','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JEANS&lulli_category_family%5B1%5D=PANTALONS&lulli_category_type%5B0%5D=JEANS+DROITS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JEANS&lulli_category_family%5B1%5D=PANTALONS&lulli_category_type%5B0%5D=JEANS+FLARE+%2F+BOOTCUT','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JEANS&lulli_category_family%5B1%5D=PANTALONS&lulli_category_type%5B0%5D=JEANS+SLIM+%2F+SKINNY','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JEANS&lulli_category_family%5B1%5D=PANTALONS&lulli_category_type%5B0%5D=JOGGINGS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JEANS&lulli_category_family%5B1%5D=PANTALONS&lulli_category_type%5B0%5D=PANTALONS+DROITS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=ROBES&lulli_category_family%5B1%5D=COMBINAISONS&lulli_category_type%5B0%5D=COMBINAISONS+PANTALONS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=ROBES&lulli_category_family%5B1%5D=COMBINAISONS&lulli_category_type%5B0%5D=COMBINAISONS+SHORTS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=ROBES&lulli_category_family%5B1%5D=COMBINAISONS&lulli_category_type%5B0%5D=ROBES+COURTES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=ROBES&lulli_category_family%5B1%5D=COMBINAISONS&lulli_category_type%5B0%5D=ROBES+LONGUES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=ROBES&lulli_category_family%5B1%5D=COMBINAISONS&lulli_category_type%5B0%5D=ROBES+MI-LONGUES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JUPES&lulli_category_family%5B1%5D=SHORTS&lulli_category_type%5B0%5D=BERMUDAS','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JUPES&lulli_category_family%5B1%5D=SHORTS&lulli_category_type%5B0%5D=JUPES+COURTES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JUPES&lulli_category_family%5B1%5D=SHORTS&lulli_category_type%5B0%5D=JUPES+LONGUES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JUPES&lulli_category_family%5B1%5D=SHORTS&lulli_category_type%5B0%5D=JUPES+MI-LONGUES','https://www.lulli-sur-la-toile.com/vetements.html?lulli_category_family%5B0%5D=JUPES&lulli_category_family%5B1%5D=SHORTS&lulli_category_type%5B0%5D=SHORTS'];
       $array4=['https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BASKETS&lulli_category_type%5B0%5D=BASKETS+BASSES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BASKETS&lulli_category_type%5B0%5D=BASKETS+MONTANTES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BASKETS&lulli_category_type%5B0%5D=RUNNINGS','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_type%5B0%5D=SABOTS','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family[0]=BOTTINES&lulli_category_family[1]=BOTTES&lulli_category_type[0]=BOTTINES%20FOURR%C3%89ES&lulli_category_type[1]=BOTTES%20FOURR%C3%89ES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_type%5B0%5D=ESCARPINS&lulli_category_type%5B1%5D=SANDALES+COMPENS%C3%89ES&lulli_category_type%5B2%5D=SANDALES+%C3%80+TALONS'];
       $array5=['https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BOTTINES&lulli_category_type%5B0%5D=BOTTINES+%C3%80+TALONS','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BOTTINES&lulli_category_type%5B0%5D=BOTTINES+FOURR%C3%89ES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BOTTINES&lulli_category_type%5B0%5D=BOTTINES+PLATES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BOTTINES&lulli_category_type%5B0%5D=BOTTINES+WESTERN','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=BOTTES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_type%5B0%5D=BOTTES+WESTERN&lulli_category_type%5B1%5D=BOTTINES+WESTERN','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_type%5B0%5D=MULES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=MOCASSINS','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=SANDALES&lulli_category_type%5B0%5D=MULES','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=SANDALES&lulli_category_type%5B0%5D=NU-PIEDS','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=SANDALES&lulli_category_type%5B0%5D=SABOTS','https://www.lulli-sur-la-toile.com/chaussures.html?lulli_category_family%5B0%5D=SANDALES&lulli_category_type%5B0%5D=SANDALES+%C3%80+TALONS'];
       $array6=['https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=PANIERS','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=CABAS','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=SACS+BANDOULIERE&lulli_category_type%5B1%5D=BESACES&lulli_category_type%5B2%5D=SACS+BOWLING&lulli_category_type%5B3%5D=SACS+SEAU&lulli_category_type%5B4%5D=BANDOULI%C3%88RES&lulli_category_type%5B5%5D=SACS+BANDOULI%C3%88RE','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=SACS+%C3%80+MAIN'];
       $array7=['https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=SACS+%C3%80+DOS','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=SACS+BANANE','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=POCHETTES','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=PORTEFEUILLES&lulli_category_type%5B1%5D=PORTE-MONNAIE','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=SACS+BANANE','https://www.lulli-sur-la-toile.com/sacs.html?lulli_category_type%5B0%5D=SACS+BANDOULI%C3%88RE'];
       
       //
       $array8=['https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=COLLIERS&lulli_category_type%5B0%5D=CHA%C3%8ENES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=COLLIERS&lulli_category_type%5B0%5D=COLLIERS+MI-LONGS','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=COLLIERS&lulli_category_type%5B0%5D=RAS-DE-COU'];
       $array9=['https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BRACELETS&lulli_category_type%5B0%5D=BRACELETS+CHA%C3%8ENE','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BRACELETS&lulli_category_type%5B0%5D=BRACELETS+CORDON','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BRACELETS&lulli_category_type%5B0%5D=BRACELETS+CUIR','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BRACELETS&lulli_category_type%5B0%5D=BRACELETS+PERLES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BRACELETS&lulli_category_type%5B0%5D=JONCS','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=COLLIERS&lulli_category_type%5B0%5D=COLLIERS+MI-LONGS','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=COLLIERS&lulli_category_type%5B0%5D=RAS-DE-COU'];
       $array10=['https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=AND...PARIS&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=ATELIER+PAULIN&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=AURELIE+BIDERMANN&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=CABIROL&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=CHARLET&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=GIGI+CLOZEAU&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=GINETTE+NY&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=GOOSSENS+PARIS&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=LA+BRUNE+%26+LA+BLONDE&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=LES+F%C3%89LICIT%C3%89S&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=LITTLE+ONES&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=LSONGE&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=NAVA+JOAILLERIE&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VANRYCKE&lulli_marque%5B1%5D=PAMELA+LOVE&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BAGUES&lulli_marque%5B0%5D=SERGE+THORAVAL','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=STONE+PARIS&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=TITYARAVY&lulli_category_family%5B0%5D=BAGUES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_marque%5B0%5D=VADI+JEWELS&lulli_category_family%5B0%5D=BAGUES'];
       $array11=['https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BOUCLES+D%27OREILLES&lulli_category_type%5B0%5D=BIJOUX+D%27OREILLES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BOUCLES+D%27OREILLES&lulli_category_type%5B0%5D=CR%C3%89OLES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BOUCLES+D%27OREILLES&lulli_category_type%5B0%5D=PENDANTES','https://www.lulli-sur-la-toile.com/bijoux.html?lulli_category_family%5B0%5D=BOUCLES+D%27OREILLES&lulli_category_type%5B0%5D=PUCES'];
      
       $array12=['https://www.lulli-sur-la-toile.com/accessoires.html?lulli_category_type%5B0%5D=CASQUETTES&lulli_category_type%5B1%5D=CHAPEAUX','https://www.lulli-sur-la-toile.com/accessoires.html?lulli_category_type%5B0%5D=BOB','https://www.lulli-sur-la-toile.com/accessoires.html?lulli_category_type%5B0%5D=BONNETS','https://www.lulli-sur-la-toile.com/accessoires.html?lulli_category_type%5B0%5D=%C3%89CHARPES+%26+FOULARDS','https://www.lulli-sur-la-toile.com/accessoires.html?lulli_category_type%5B0%5D=CEINTURES'];
  
     



        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'build001.xms005.site'],
            'x2' => ['source' => $array2, 'target' =>'intersport002.xms002.site'],
            'x3' => ['source' => $array3, 'target' =>'intersport003.xms002.site'],
            'x4' => ['source' => $array4, 'target' =>'intersport004.xms002.site'],
            'x5' => ['source' => $array5, 'target' =>'intersport005.xms002.site'],
            'x6' => ['source' => $array6, 'target' =>'intersport006.xms002.site'],
            'x7' => ['source' => $array7, 'target' =>'intersport007.xms002.site'],
            'x8' => ['source' => $array8, 'target' =>'intersport008.xms002.site'],
            'x9' => ['source' => $array9, 'target' =>'intersport009.xms002.site'],
            'x10' => ['source' => $array10, 'target' =>'intersport0010.xms003.site'],
            'x11' => ['source' => $array11, 'target' =>'intersport0011.xms007.site'],
            'x12' => ['source' => $array12, 'target' =>'intersport0012.xms008.site'],
            
            
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.lulli-sur-la-toile.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[class="next i-next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
      
        $breadcrumbs= $crawler->filter('.currently li')->filter('.value')->each(function (Crawler $node,$i){
          return $node->text();
        });
 
    
        // print_r($breadcrumbs);exit;
        // 进入详情页爬取数据
        $crawler->filter('.item-wrapper a')->each(function (Crawler $node, $i) use ($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                   
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
        
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.product-name h1')->text();  //标题（必须）
        // print_r($product['title']);exit;

        $pricedel=[',','€','$'];
        $product['price'] = str_replace($pricedel,'',$crawler->filter('.price')->text()); 

        // $del= [';'];
        // $int = $crawler->filter('script')->eq(29)->text();  //价格（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['price']=json_decode($it,true)['price'];

        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.price')->text())));  //价格（必须）

        // print_r($product['price']);exit;

        $product['brand']=$crawler->filter('[itemprop="brand"]')->attr('content');
       //品牌（必须）

    //    $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,1,-1);
        foreach ($item['breadcrumbs'] as $breadcrumb) {
            array_push($product['breadcrumbs'],$breadcrumb);
        }
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        
    }
        // print_r($product['breadcrumbs']);exit;
        
   
        // $product['sku'] =json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['sku']; 
        $del= [';',')'];
        $int = $crawler->filter('[orig_index="15"]')->text();
        $index = strpos($int,'(');
        $it=str_replace($del,'',substr($int,$index+1));
        $product['sku']=json_decode($it,true)['productId'];
        // print_r( $product['sku']);exit;  //产品编号（必须）
       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];   
        $product['description']=$crawler->filter('.std p')->html();

        // print_r($product['description']);exit;str_replace($descr,'',

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        
        // if($crawler->filter('.detailInfo')->count()) {
        //     $count = $crawler->filter('.detailInfo div')->eq(1)->count();
        //     // echo $count;exit;
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>$crawler->filter('.product-specification-item')->eq($i)->filter('dt')->text(),
        //                 'options' =>$crawler->filter('.product-specification-item')->eq($i)->filter('dd')->text(),
        //             ];
        //         }       
            
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        // $product['gender']='';  x8使用
        $product['gender'] = '';
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $img=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$img){
        //     if(strstr($node->text(),"image")){
        //         $img = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('.product-img-carousel a')->each(function(Crawler $node,$i){
            return  $node->attr('href');
        })));
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($images);exit;
    //   商品的尺寸
         
         

        $xx=[];
        $product['variations']=[];

        if($crawler->filter('.gt__content')->filter('.gt__table')->count()){
            $count = $crawler->filter('.gt__table')->eq(0)->filter('tbody tr')->count();
            //  echo $count;exit;
            for($i=1;$i<$count;$i++){
            $xx[]=$crawler->filter('.gt__table tbody')->filter('tr')->eq($i)->filter('td')->eq(1)->text();
         }
        //  print_r($xx);exit;
       }
      
        if($crawler->filter('.gt__table')->count()){
                 $product['variations'][]= [
                    
                'name'=>'Attention ',
                'options'=>$xx

            ];  
       
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}