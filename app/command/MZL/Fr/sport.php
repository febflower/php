<?php
namespace app\command\MZL\Fr;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class sport extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:sport')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.go-sport.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
        //1-3分类为4
       $array1=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9013','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9014','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=10749','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9009','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9010','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=10413','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9023','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9024','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=10750','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8881','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9011','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9012','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9017','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9018','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=10751',''];
       $array2=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8781','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8784','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8785','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8841','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3857','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8766','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8759','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8695','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8698','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=7922','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8762','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9863',''];
       $array3=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8964','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8965','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8966','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8847','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8973','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8974','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8975','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8970','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8971','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8972','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8976','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8977','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8978','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8967','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8968','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8969',''];
       //改分类成3
       $array4=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3956','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3955','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3945','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3953','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8860','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9874','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3893','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8863','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8865','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3920','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8873','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8872','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9872'];
       $array5=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8935','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8936','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8939','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8940','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8931','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8932'];
       $array6=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8937','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8938','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8930','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3751','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3970','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3968'];
       //分类改为3
       $array7=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8943','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8944','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8945','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8948',];
       //分类改为5      这个特殊，放到8-9后面，但是分类改为4 'https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=AN00011'
       $array8=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3155','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3156','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3157',];
       $array9=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3163','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3164','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3165',];
       //分类改为5
       $array10=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3292','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3300','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3302','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=12392',''];
       $array11=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8011','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3298','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3297','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3301'];
       $array12=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3254','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3299','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3305','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3289'];
       $array13=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8010','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3273','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=12393','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3278'];
       $array14=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3283','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3282','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3253','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3279'];
       $array15=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3287','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=12393','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3277','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3273',];
       $array16=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3270','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3264','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3268','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3267','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3256'];
       //分类改为3
       $array17=['https://www.go-sport.com/sports/randonnee/sac-a-dos/sac-a-dos-randonnee/?cgid=3230','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3212','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3213',];
       //改为2
       $array18=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3125','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3129','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3215','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3121','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3137'];
       //改为3
       $array19=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3136','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3124','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3179','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3178','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3204','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3209','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3251'];
       //改为4
       $array20=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3358','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3359','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3360','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3377','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3379'];
       $array21=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3355','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3356','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3357','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3376','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3378'];
       // 品牌
    //    $array22=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11179','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11176','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11180','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11178','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11184','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11181','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11177',];
       //改为4
       $array23=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3392','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3393','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3396','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3397','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3394','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3395'];
       $array24=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3398','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11150','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11151','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11148','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3401','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11149','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3402','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3399'];
       $array25=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3385','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3382','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3383','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3386','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3387','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11199'];
       //改为3
       $array26=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11152','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11153','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3327','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3326','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11033','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3332','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3314','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3322','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8557','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3330','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3321','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3324',''];
       $array27=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11204','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3391','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=8561','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3316','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3328','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3329','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3320','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3343','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3344','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3371','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=7718'];
       $array28=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11196','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11198','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=7719','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11197'];
       // 分类改为4
       $array29=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4105','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4108','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4113','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4128','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4101','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4130','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4118','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4129','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4135','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4134','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4110','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4111','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4112'];
        // 分类改为5
       $array30=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4192','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4188','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4187','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4189','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4186'];
       $array31=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4116','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4189','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4193'];
       $array32=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4185','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4186','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4190'];
       $array33=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4183','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4179','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4180','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4158'];
       $array34=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4160','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4122','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4157','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4184'];
       $array35=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4161','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4162','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4166','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4423'];
       $array36=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4173','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4171','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4163','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4172'];
      //分类改为4
       $array37=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4087','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4088','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4090','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4089','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4086'];
       $array38=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4136','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4138','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4137','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=10085','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4131','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4117','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4098','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4139','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4071','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4052','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4051','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4082'];
       //分类改为3
       $array39=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4044','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4043','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4042','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4032','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4035','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4078','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4079','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=4083'];
       $array40=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2858','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11241','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11250','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2870','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2871','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2873','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2863','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2864'];
       $array41=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2886','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2887','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9900','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=7764','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11294','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2901',''];
       //分类改为4
       $array42=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3486','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3485','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3484','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3432','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3460','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3459','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11987','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3435','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3461','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3462','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11094','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11095','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11096','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3488','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3433','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3472','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3473','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3474',];
       //分类改为3
       $array43=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3411','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11080','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3413','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11638','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3439','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3440','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3442','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3407','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9809','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3419','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3421','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3420'];
        //分类改为4
       $array44=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2970','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11363','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11364','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11362','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2957','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2956','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2954'];
       $array45=['https://www.go-sport.com/sports/sport-de-raquette/tennis/raquettes/raquette-tennis-prets-homme/?cgid=3486','https://www.go-sport.com/sports/sport-de-raquette/tennis/raquettes/raquette-tennis-prets-femme/?cgid=3485','https://www.go-sport.com/sports/sport-de-raquette/tennis/raquettes/raquette-tennis-prets-enfant/?cgid=3484','https://www.go-sport.com/sports/sport-de-raquette/tennis/balles/?cgid=3432','https://www.go-sport.com/sports/sport-de-raquette/tennis/accessoires/grip/?cgid=3460','https://www.go-sport.com/sports/sport-de-raquette/tennis/accessoires/bandeau-poignees-ten/?cgid=3459','https://www.go-sport.com/sports/sport-de-raquette/tennis/accessoires/balles/?cgid=11987','https://www.go-sport.com/sports/sport-de-raquette/tennis/accessoires/cordage/?cgid=3435','https://www.go-sport.com/sports/sport-de-raquette/tennis/bagagerie-tennis/?cgid=3431','https://www.go-sport.com/sports/sport-de-raquette/tennis/tenues/short-tennis/?cgid=11094','https://www.go-sport.com/sports/sport-de-raquette/tennis/tenues/t-shirt/?cgid=11095','https://www.go-sport.com/sports/sport-de-raquette/tennis/tenues/sweat-veste/?cgid=11096','https://www.go-sport.com/sports/sport-de-raquette/tennis/tenues/robes-jupes-tennis-femme/?cgid=3488','https://www.go-sport.com/sports/sport-de-raquette/tennis/chaussettes/?cgid=3433','https://www.go-sport.com/sports/sport-de-raquette/tennis/chaussures-de-tennis/regulier-tennis-enfant/?cgid=3472','https://www.go-sport.com/sports/sport-de-raquette/tennis/chaussures-de-tennis/regulier-tennis-femme/?cgid=3473','https://www.go-sport.com/sports/sport-de-raquette/tennis/chaussures-de-tennis/regulier-tennis-homme/?cgid=3474'];
       //分类改为3
       $array46=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2940','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2941','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2942','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2922','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2915','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2916','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2929','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2930','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9089','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=9090'];
       //分类改为4
       $array47=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11377','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2908','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2919','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2918','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11388','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11387','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=2920'];
       //分类改为3
       $array48=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3680','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3679','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3678','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3513','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3512','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3511','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=7739','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=7741','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=11139'];
       $array49=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3658','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3659','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3503','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3507',''];
        //分类改为4
       $array50=['https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3620','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3622','https://www.go-sport.com/on/demandware.store/Sites-GoSport_FR-Site/default/Search-Show?cgid=3621'];
       //分类改为3
       $array51=['https://www.go-sport.com/football/clubs/maillots-clubs-football/?cgid=3762','https://www.go-sport.com/football/clubs/liga-espagnole/atletico-madrid/?cgid=8856','https://www.go-sport.com/football/clubs/liga-espagnole/barcelone/?cgid=8854','https://www.go-sport.com/football/clubs/liga-espagnole/betis-seville/?cgid=8857','https://www.go-sport.com/football/clubs/liga-espagnole/real-madrid/?cgid=8855','https://www.go-sport.com/football/clubs/serie-a-italienne/roma/?cgid=8863','https://www.go-sport.com/football/clubs/serie-a-italienne/juventus/?cgid=8860','https://www.go-sport.com/football/clubs/serie-a-italienne/lazio-rome/?cgid=9874','https://www.go-sport.com/football/clubs/serie-a-italienne/sampdoria-genes/?cgid=3893','https://www.go-sport.com/football/clubs/bundesliga-allemande/?cgid=8806','https://www.go-sport.com/football/clubs/autres-clubs/?cgid=8808'];
       $array52=['https://www.go-sport.com/football/clubs/ligue1/as-monaco/maillots/?cgid=8781','https://www.go-sport.com/football/clubs/ligue1/as-monaco/survetements/?cgid=8784','https://www.go-sport.com/football/clubs/ligue1/as-monaco/accesoires-supporter/?cgid=8785','https://www.go-sport.com/football/clubs/ligue1/metz/?cgid=8841','https://www.go-sport.com/football/clubs/ligue1/fcgb/?cgid=3857','https://www.go-sport.com/football/clubs/ligue1/olympique-lyonnais/?cgid=8763','https://www.go-sport.com/football/clubs/ligue1/psg/maillots/?cgid=8695','https://www.go-sport.com/football/clubs/ligue1/psg/survetements/?cgid=8698','https://www.go-sport.com/football/clubs/ligue1/psg/psg3/?cgid=7922','https://www.go-sport.com/football/clubs/ligue1/psg/accesoires-supporter/?cgid=8762','https://www.go-sport.com/football/clubs/ligue1/srfc/?cgid=3855',''];
       $array53=['https://www.go-sport.com/football/clubs/premier-league-anglaise/arsenal-fc/?cgid=8842','https://www.go-sport.com/football/clubs/premier-league-anglaise/aston-villa/?cgid=8847','https://www.go-sport.com/football/clubs/premier-league-anglaise/chelsea/?cgid=8845','https://www.go-sport.com/football/clubs/premier-league-anglaise/liverpool/maillots/?cgid=8970','https://www.go-sport.com/football/clubs/premier-league-anglaise/liverpool/survetement/?cgid=8971','https://www.go-sport.com/football/clubs/premier-league-anglaise/liverpool/accessoires-supporter/?cgid=8972','https://www.go-sport.com/football/clubs/premier-league-anglaise/manchester-city/?cgid=8846','https://www.go-sport.com/football/clubs/premier-league-anglaise/manchester-united/maillots/?cgid=8967','https://www.go-sport.com/football/clubs/premier-league-anglaise/manchester-united/survetements/?cgid=8968','https://www.go-sport.com/football/clubs/premier-league-anglaise/manchester-united/accessoires-supporter/?cgid=8969',];
       $array54=['https://www.go-sport.com/football/nations/europe/belgique/?cgid=8885','https://www.go-sport.com/football/nations/europe/france/?cgid=8878','https://www.go-sport.com/football/nations/europe/espagne/?cgid=8888','https://www.go-sport.com/football/nations/europe/italie/?cgid=8879','https://www.go-sport.com/football/nations/europe/pays-bas/?cgid=8881','https://www.go-sport.com/football/nations/europe/portugal/?cgid=8882','https://www.go-sport.com/football/nations/amerique-du-sud/?cgid=8815','https://www.go-sport.com/football/nations/afrique/?cgid=8819','https://www.go-sport.com/football/nations/maillots-nations-23/?cgid=MD028'];
       $array55=['https://www.go-sport.com/football/chaussures/chaussure-indoor/chaussures-futsal-adulte/?cgid=3785','https://www.go-sport.com/football/chaussures/chaussure-indoor/chaussures-futsal-enfant/?cgid=3786','https://www.go-sport.com/football/chaussures/futsal/terrain-synthetique/?cgid=3784','https://www.go-sport.com/football/chaussures/chaussures-vissees/?cgid=7704','https://www.go-sport.com/football/chaussures/chaussures-stabilisees/chaussures-stabilisees-adulte/?cgid=3779','https://www.go-sport.com/football/chaussures/chaussure-moulees/chaussures-moulees-adulte/?cgid=3777'];
       //分类改为2
       $array56=['https://www.go-sport.com/football/chaussures/chaussure-indoor/chaussures-foot-indoor-adulte/?cgid=3775','https://www.go-sport.com/football/chaussures/chaussure-indoor/chaussures-indoor-foot-enfant/?cgid=3776','https://www.go-sport.com/football/equipements/shorts-et-collants/?cgid=8927','https://www.go-sport.com/football/equipements/chaussettes/?cgid=8929','https://www.go-sport.com/football/equipements/maillots-de-compression/?cgid=8928','https://www.go-sport.com/football/equipements/survetements/?cgid=8925','https://www.go-sport.com/football/equipements/gardien/?cgid=3751','https://www.go-sport.com/football/feminin/clubs/?cgid=13079','https://www.go-sport.com/football/feminin/nations/?cgid=13081','https://www.go-sport.com/football/accessoires-foot/ballons/?cgid=8943','https://www.go-sport.com/football/accessoires-foot/proteges-tibias/?cgid=8944','https://www.go-sport.com/football/accessoires-foot/pompes-a-ballon/?cgid=8945','https://www.go-sport.com/football/accessoires-foot/sacs-entrainement/?cgid=8948'];
       //分类改为4
       $array57=['https://www.go-sport.com/sports/sports-d-equipe/basket/tenues/chaussettes-casquette-basket-adulte/?cgid=3709','https://www.go-sport.com/sports/sports-d-equipe/basket/tenues/maillot-basket-adulte/?cgid=3738','https://www.go-sport.com/sports/sports-d-equipe/basket/tenues/maillot-basket-enfant/?cgid=3739','https://www.go-sport.com/sports/sports-d-equipe/basket/tenues/short-basket-adulte/?cgid=3740','https://www.go-sport.com/sports/sports-d-equipe/basket/tenues/short-basket-enfant/?cgid=3741','https://www.go-sport.com/sports/sports-d-equipe/basket/tenues/sweat-shirt-veste-basket/?cgid=3745'];
       $array58=['https://www.go-sport.com/sports/sports-d-equipe/basket/chaussures/chaussures-basket-homme/?cgid=3714','https://www.go-sport.com/sports/sports-d-equipe/basket/chaussures/chaussures-basket-enfant/?cgid=3715','https://www.go-sport.com/sports/sports-d-equipe/basket/chaussures/chaussures-basket-femme/?cgid=3716'];
       $array59=['https://www.go-sport.com/sports/sports-d-equipe/rugby/accessoires/?cgid=3981','https://www.go-sport.com/sports/sports-d-equipe/rugby/ballon/?cgid=3982','https://www.go-sport.com/sports/sports-d-equipe/rugby/chaussures/?cgid=3983','https://www.go-sport.com/sports/sports-d-equipe/rugby/maillots-nations/?cgid=3984','https://www.go-sport.com/sports/sports-d-equipe/rugby/maillots-top-14/?cgid=3985','https://www.go-sport.com/sports/sports-d-equipe/rugby/tenue/?cgid=3988'];
       $array60=['https://www.go-sport.com/sports/sports-d-equipe/hand-volley/accessoires/accessoires-handball-volley/?cgid=5269','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/accessoires/protection-handball-volley/?cgid=5270','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/ballons/ballons-volley/?cgid=5273','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/ballons/ballons-handball/?cgid=5274','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/chaussures/chaussures-indoor-hand-volley-adulte/?cgid=5275','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/chaussures/chaussures-hand-volley-femme/?cgid=5276','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/chaussures/chaussures-indoor-hand-volley-enfant/?cgid=5277','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/tenues/shorts-hand-volley/?cgid=5280','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/tenues/maillots-hand-volley/?cgid=5281','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/tenues/chaussettes-hand-volley/?cgid=5282','https://www.go-sport.com/sports/sports-d-equipe/hand-volley/tenues/survetements-hand-volley/?cgid=5283'];
       $array61=['https://www.go-sport.com/sports/sports-d-equipe/football-americain/nfl-national-football-league/?cgid=10035','https://www.go-sport.com/sports/sports-d-equipe/football-americain/ballons/?cgid=4026','https://www.go-sport.com/sports/sports-d-equipe/football-americain/equipements/?cgid=4027','https://www.go-sport.com/sports/sports-d-equipe/football-americain/equipements/protections/?cgid=9999'];
       $array62=['https://www.go-sport.com/sports/sports-d-equipe/baseball/mlb-major-league-baseball/?cgid=10012','https://www.go-sport.com/sports/sports-d-equipe/baseball/protections/?cgid=4023','https://www.go-sport.com/sports/sports-d-equipe/baseball/replique/?cgid=4024','https://www.go-sport.com/sports/sports-d-equipe/baseball/battes/?cgid=9989'];
       $array63=['https://www.go-sport.com/sports/autres-sports/sport-de-combat/boxe/protections-153/?cgid=10091','https://www.go-sport.com/sports/autres-sports/sport-de-combat/boxe/chaussures-boxe/?cgid=2504','https://www.go-sport.com/sports/autres-sports/sport-de-combat/boxe/sac-de-frappe/?cgid=2506','https://www.go-sport.com/sports/autres-sports/sport-de-combat/boxe/textile-boxe/?cgid=2509','https://www.go-sport.com/sports/autres-sports/sport-de-combat/boxe/gants/?cgid=2511'];
       $array64=['https://www.go-sport.com/homme/vetements/vestes-parkas/vestes/velo/?cgid=5165','https://www.go-sport.com/homme/vetements/vestes-parkas/vestes/randonnee/?cgid=5173','https://www.go-sport.com/homme/vetements/vestes-parkas/vestes/running/?cgid=5202','https://www.go-sport.com/homme/vetements/vestes-parkas/vestes/ski/?cgid=5256','https://www.go-sport.com/homme/vetements/vestes-parkas/parkas/capuche/?cgid=11537','https://www.go-sport.com/homme/vetements/vestes-parkas/parkas/?cgid=5120','https://www.go-sport.com/homme/vetements/vestes-parkas/coupes-vent/running/?cgid=12739','https://www.go-sport.com/homme/vetements/vestes-parkas/coupes-vent/impermeables/?cgid=5192','https://www.go-sport.com/homme/vetements/vestes-parkas/doudounes/?cgid=5122'];
       $array65=['https://www.go-sport.com/homme/vetements/t-shirts/manches-longues/sportswear/?cgid=5109','https://www.go-sport.com/homme/vetements/t-shirts/manches-longues/running/?cgid=5204','https://www.go-sport.com/homme/vetements/t-shirts/manches-courtes/fitness-et-training/?cgid=12689','https://www.go-sport.com/homme/vetements/t-shirts/manches-courtes/maillots-cycle-homme/?cgid=5164','https://www.go-sport.com/homme/vetements/t-shirts/manches-courtes/randonnee/?cgid=5188','https://www.go-sport.com/homme/vetements/t-shirts/manches-courtes/running/?cgid=5203','https://www.go-sport.com/homme/vetements/t-shirts/manches-courtes/football/?cgid=5230','https://www.go-sport.com/homme/vetements/t-shirts/manches-courtes/sportswear/?cgid=7770','https://www.go-sport.com/homme/vetements/t-shirts/debardeurs/training/?cgid=12445','https://www.go-sport.com/homme/vetements/t-shirts/debardeurs/sportswear/?cgid=5118','https://www.go-sport.com/homme/vetements/t-shirts/debardeurs/basketball/?cgid=5226','https://www.go-sport.com/homme/vetements/t-shirts/compression/?cgid=5114'];
       $array66=['https://www.go-sport.com/homme/vetements/shorts/sportswear/?cgid=5069','https://www.go-sport.com/homme/vetements/shorts/training/?cgid=5084','https://www.go-sport.com/homme/vetements/shorts/randonnee/?cgid=5181','https://www.go-sport.com/homme/vetements/shorts/running/?cgid=5201','https://www.go-sport.com/homme/vetements/shorts/basketball/?cgid=5227','https://www.go-sport.com/homme/vetements/shorts/football/?cgid=5231'];
       //分类改为5 注意顺序
       $array67=['https://www.go-sport.com/homme/vetements/sous-vetements/chaussettes/running/','https://www.go-sport.com/homme/vetements/sous-vetements/chaussettes/randonnee/','https://www.go-sport.com/homme/vetements/sous-vetements/chaussettes/sportswear/','https://www.go-sport.com/homme/vetements/sous-vetements/chaussettes/basketball/','https://www.go-sport.com/homme/vetements/sous-vetements/chaussettes/football/','https://www.go-sport.com/homme/vetements/sous-vetements/chaussettes/ski/','https://www.go-sport.com/homme/vetements/sous-vetements/thermiques/randonnee/','https://www.go-sport.com/homme/vetements/sous-vetements/thermiques/football/','https://www.go-sport.com/homme/vetements/sous-vetements/thermiques/sports-hiver/','https://www.go-sport.com/homme/vetements/sous-vetements/boxers/?cgid=5092','https://www.go-sport.com/homme/vetements/sous-vetements/slips/?cgid=5088'];
       $array68=['https://www.go-sport.com/homme/chaussures/chaussures-sport/randonnee/hiking/','https://www.go-sport.com/homme/chaussures/chaussures-sport/randonnee/escalade/','https://www.go-sport.com/homme/chaussures/chaussures-sport/randonnee/alpinisme/','https://www.go-sport.com/homme/chaussures/chaussures-sport/randonnee/randonnee/'];
       $array70=['https://www.go-sport.com/homme/chaussures/chaussures-sport/football/stabilises/?cgid=596','https://www.go-sport.com/homme/chaussures/chaussures-sport/football/moules/?cgid=597','https://www.go-sport.com/homme/chaussures/chaussures-sport/football/futsal/?cgid=598','https://www.go-sport.com/homme/chaussures/chaussures-sport/football/visses/?cgid=599'];
       //分类改为4  品牌 71-72
       $array69=['https://www.go-sport.com/homme/chaussures/chaussures-sport/sport-de-raquettes/','https://www.go-sport.com/homme/chaussures/chaussures-sport/velo/','https://www.go-sport.com/homme/chaussures/chaussures-sport/trail/','https://www.go-sport.com/homme/chaussures/chaussures-sport/basketball/','https://www.go-sport.com/homme/chaussures/chaussures-sport/hand-volley/'];
       $array71=['https://www.go-sport.com/homme/chaussures/baskets/adidas/continental/','https://www.go-sport.com/homme/chaussures/baskets/adidas/supercourt/','https://www.go-sport.com/homme/chaussures/baskets/adidas/nmd/','https://www.go-sport.com/homme/chaussures/baskets/adidas/boost/','https://www.go-sport.com/homme/chaussures/baskets/adidas/adidas-gazelle/','https://www.go-sport.com/homme/chaussures/baskets/adidas/adidas-stan-smith/','https://www.go-sport.com/homme/chaussures/baskets/adidas/superstar/','https://www.go-sport.com/homme/chaussures/baskets/adidas/campus/','https://www.go-sport.com/homme/chaussures/baskets/le-coq-sportif/?cgid=293','https://www.go-sport.com/homme/chaussures/baskets/nike/air-max/','https://www.go-sport.com/homme/chaussures/baskets/nike/internationalist/','https://www.go-sport.com/homme/chaussures/baskets/nike/flyknit/','https://www.go-sport.com/homme/chaussures/baskets/nike/mdrunner/','https://www.go-sport.com/homme/chaussures/baskets/puma/?cgid=7867','https://www.go-sport.com/homme/chaussures/baskets/asics/asics-quantum/','https://www.go-sport.com/homme/chaussures/baskets/asics/gel-lyte/','https://www.go-sport.com/homme/chaussures/baskets/asics/gel-kayano-trainer/'];
       $array72=['https://www.go-sport.com/homme/chaussures/baskets/le-coq-sportif/','https://www.go-sport.com/homme/chaussures/baskets/timberland/','https://www.go-sport.com/homme/chaussures/baskets/vans/authentic/','https://www.go-sport.com/homme/chaussures/baskets/vans/sk8/','https://www.go-sport.com/homme/chaussures/baskets/vans/oldskool/','https://www.go-sport.com/homme/chaussures/baskets/vans/era/','https://www.go-sport.com/homme/chaussures/baskets/vans/michoacan/','https://www.go-sport.com/homme/chaussures/baskets/reebok/classic-leather/','https://www.go-sport.com/homme/chaussures/baskets/reebok/club-c-85/','https://www.go-sport.com/homme/chaussures/baskets/reebok/classic-nylon/','https://www.go-sport.com/homme/chaussures/baskets/reebok/npc/'];
       $array73=['https://www.go-sport.com/femme/vetements/sweats-pulls/sweats-capuche/?cgid=12537','https://www.go-sport.com/femme/vetements/sweats-pulls/sweats/?cgid=4894','https://www.go-sport.com/femme/vetements/sweats-pulls/polaires/?cgid=4895'];
       $array74=['https://www.go-sport.com/femme/vetements/t-shirts/manches-courtes/','https://www.go-sport.com/femme/vetements/t-shirts/debardeurs/','https://www.go-sport.com/femme/vetements/t-shirts/manches-longues/'];
       $array75=['https://www.go-sport.com/femme/vetements/brassieres/maintien-leger/','https://www.go-sport.com/femme/vetements/brassieres/maintien-fort/','https://www.go-sport.com/femme/vetements/brassieres/maintien-moyen/'];
       $array76=['https://www.go-sport.com/femme/vetements/shorts/sportswear/','https://www.go-sport.com/femme/vetements/shorts/danse/','https://www.go-sport.com/femme/vetements/shorts/fitness/','https://www.go-sport.com/femme/vetements/shorts/randonnee/','https://www.go-sport.com/femme/vetements/shorts/running/'];
       //分类改为3   
       $array77=['https://www.go-sport.com/femme/vetements/vestes-parkas/vestes/?cgid=12619','https://www.go-sport.com/femme/vetements/vestes-parkas/parkas/?cgid=4910','https://www.go-sport.com/femme/vetements/vestes-parkas/coupes-vent/?cgid=4911','https://www.go-sport.com/femme/vetements/vestes-parkas/doudounes/?cgid=4912','https://www.go-sport.com/femme/vetements/pantalons-joggings/','https://www.go-sport.com/femme/vetements/survetements/','https://www.go-sport.com/femme/vetements/robes/','https://www.go-sport.com/femme/vetements/sous-vetements/','https://www.go-sport.com/femme/vetements/polos/'];
       $array78=['https://www.go-sport.com/femme/chaussures/baskets/nike/','https://www.go-sport.com/femme/chaussures/baskets/new-balance/','https://www.go-sport.com/femme/chaussures/baskets/vans/','https://www.go-sport.com/femme/chaussures/baskets/reebok/','https://www.go-sport.com/femme/chaussures/baskets/coq-sportif/'];
       $array79=['https://www.go-sport.com/femme/chaussures/baskets/puma/','https://www.go-sport.com/femme/chaussures/baskets/timberland/','https://www.go-sport.com/femme/chaussures/baskets/asics/','https://www.go-sport.com/femme/chaussures/baskets/adidas/','https://www.go-sport.com/femme/chaussures/baskets/converse/'];
       $array80=['https://www.go-sport.com/femme/chaussures/sandales/','https://www.go-sport.com/femme/chaussures/tongs-claquettes/','https://www.go-sport.com/femme/chaussures/apres-ski/','https://www.go-sport.com/femme/chaussures/sneakers/'];
       //分类改为4
       $array81=['https://www.go-sport.com/femme/chaussures/bottes/neige/','https://www.go-sport.com/femme/chaussures/bottes/randonnee/','https://www.go-sport.com/femme/chaussures/bottes/equitation/','https://www.go-sport.com/femme/chaussures/bottes/pluie/'];
       $array82=['https://www.go-sport.com/femme/chaussures/chaussures-sport/equitation-golf/','https://www.go-sport.com/femme/chaussures/chaussures-sport/piscine/','https://www.go-sport.com/femme/chaussures/chaussures-sport/sport-de-raquette/','https://www.go-sport.com/femme/chaussures/chaussures-sport/hand-volley/'];
       $array83=['https://www.go-sport.com/femme/chaussures/chaussures-sport/danse/','https://www.go-sport.com/femme/chaussures/chaussures-sport/velo/','https://www.go-sport.com/femme/chaussures/chaussures-sport/equitation-golf/','https://www.go-sport.com/femme/chaussures/chaussures-sport/tennis/'];
       $array84=['https://www.go-sport.com/femme/chaussures/chaussures-sport/randonnee/alpinisme/?cgid=476','https://www.go-sport.com/femme/chaussures/chaussures-sport/randonnee/hiking/?cgid=477','https://www.go-sport.com/femme/chaussures/chaussures-sport/randonnee/randonnee/?cgid=478','https://www.go-sport.com/femme/chaussures/chaussures-sport/sport-de-raquette/','https://www.go-sport.com/femme/chaussures/chaussures-sport/basketball/','https://www.go-sport.com/femme/chaussures/chaussures-sport/trail/'];
       $array85=['https://www.go-sport.com/accessoires/accessoires-textile-adulte/chapeau-casquette/femme/?cgid=114','https://www.go-sport.com/accessoires/accessoires-textile-adulte/chapeau-casquette/homme/?cgid=115','https://www.go-sport.com/accessoires/accessoires-textile-adulte/chapeau-casquette/mixte/?cgid=116','https://www.go-sport.com/accessoires/accessoires-textile-adulte/chaussettes/femme/?cgid=121','https://www.go-sport.com/accessoires/accessoires-textile-adulte/chaussettes/homme/?cgid=122','https://www.go-sport.com/accessoires/accessoires-textile-adulte/bonnet-chapka/?cgid=96','https://www.go-sport.com/accessoires/accessoires-textile-adulte/echarpe/?cgid=100','https://www.go-sport.com/accessoires/accessoires-textile-adulte/gants/?cgid=101','https://www.go-sport.com/accessoires/accessoires-textile-adulte/autres-accessoires/?cgid=95'];
       $array86=['https://www.go-sport.com/enfant/vetements-garcon/t-shirts/debardeurs/','https://www.go-sport.com/enfant/vetements-garcon/t-shirts/manches-courtes/sportswear/','https://www.go-sport.com/enfant/vetements-garcon/t-shirts/manches-courtes/football/','https://www.go-sport.com/enfant/vetements-garcon/t-shirts/manches-courtes/randonnee/','https://www.go-sport.com/enfant/vetements-garcon/t-shirts/manches-longues/'];
       $array87=['https://www.go-sport.com/enfant/vetements-garcon/shorts/sportswear/','https://www.go-sport.com/enfant/vetements-garcon/shorts/tennis/','https://www.go-sport.com/enfant/vetements-garcon/shorts/basketball/','https://www.go-sport.com/enfant/vetements-garcon/shorts/football/','https://www.go-sport.com/enfant/vetements-garcon/shorts/hand-volley/','https://www.go-sport.com/enfant/vetements-garcon/shorts/rugby/'];
       $array88=['https://www.go-sport.com/enfant/vetements-garcon/sweats-pulls/sweat-polaire/','https://www.go-sport.com/enfant/vetements-garcon/sweats-pulls/sweats/','https://www.go-sport.com/enfant/vetements-garcon/sweats-pulls/sweats-capuche/'];
       $array89=['https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/vestes/ski/?cgid=4840','https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/parkas/?cgid=4735','https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/coupes-vent/?cgid=4736','https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/doudounes/legeres/','https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/doudounes/sans-manches/','https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/doudounes/longues/','https://www.go-sport.com/enfant/vetements-garcon/vestes-parkas/doudounes/ski/'];
       $array90=['https://www.go-sport.com/enfant/vetements-garcon/survetements/','https://www.go-sport.com/enfant/vetements-garcon/pantalons-joggings/','https://www.go-sport.com/enfant/vetements-garcon/maillots-de-bain/','https://www.go-sport.com/enfant/vetements-garcon/polos/','https://www.go-sport.com/enfant/vetements-garcon/sous-vetements/'];  
       $array91=['https://www.go-sport.com/enfant/vetements-fille/t-shirts/debardeurs/?cgid=4705','https://www.go-sport.com/enfant/vetements-fille/t-shirts/manches-courtes/?cgid=4706','https://www.go-sport.com/enfant/vetements-fille/t-shirts/manches-longues/?cgid=4707','https://www.go-sport.com/enfant/vetements-fille/maillots-de-bain/anti-uv/?cgid=4694','https://www.go-sport.com/enfant/vetements-fille/maillots-de-bain/plage/?cgid=4695','https://www.go-sport.com/enfant/vetements-fille/maillots-de-bain/natation/?cgid=4696','https://www.go-sport.com/enfant/vetements-fille/shorts/fitness/?cgid=12608','https://www.go-sport.com/enfant/vetements-fille/shorts/basketball/?cgid=12610','https://www.go-sport.com/enfant/vetements-fille/shorts/sportswear/?cgid=4699','https://www.go-sport.com/enfant/vetements-fille/leggings-cuissards/?cgid=4680'];
       $array92=['https://www.go-sport.com/enfant/vetements-fille/sous-vetements/?cgid=4684','https://www.go-sport.com/enfant/vetements-fille/brassieres/?cgid=4679','https://www.go-sport.com/enfant/vetements-fille/robes/?cgid=10459','https://www.go-sport.com/enfant/vetements-fille/survetements/?cgid=4703','https://www.go-sport.com/enfant/vetements-fille/vestes-parkas/?cgid=4687'];




        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'sport001.xms011.site'],
            'x2' => ['source' => $array2, 'target' =>'sport002.xms011.site'],
            'x3' => ['source' => $array3, 'target' =>'sport003.xms011.site'],
            'x4' => ['source' => $array4, 'target' =>'sport004.xms011.site'],
            'x5' => ['source' => $array5, 'target' =>'sport005.xms011.site'],
            'x6' => ['source' => $array6, 'target' =>'sport006.xms011.site'],
            'x7' => ['source' => $array7, 'target' =>'sport007.xms011.site'],
            'x8' => ['source' => $array8, 'target' =>'sport008.xms011.site'],
            'x9' => ['source' => $array9, 'target' =>'sport009.xms011.site'],
            'x10' => ['source' => $array10, 'target' =>'sport010.xms011.site'],
            'x11' => ['source' => $array11, 'target' =>'sport011.xms011.site'],
            'x12' => ['source' => $array12, 'target' =>'sport012.xms011.site'],
            'x13' => ['source' => $array13, 'target' =>'sport013.xms011.site'],
            'x14' => ['source' => $array14, 'target' =>'sport014.xms011.site'],
            'x15' => ['source' => $array15, 'target' =>'sport015.xms011.site'],
            'x16' => ['source' => $array16, 'target' =>'sport016.xms011.site'],
            'x17' => ['source' => $array17, 'target' =>'sport017.xms011.site'],
            'x18' => ['source' => $array18, 'target' =>'sport018.xms011.site'],
            'x19' => ['source' => $array19, 'target' =>'sport019.xms011.site'],
            'x20' => ['source' => $array20, 'target' =>'sport020.xms011.site'],
            'x21' => ['source' => $array21, 'target' =>'sport021.xms011.site'],
            // 'x22' => ['source' => $array22, 'target' =>'hxflla'],
            'x23' => ['source' => $array23, 'target' =>'sport023.xms011.site'],
            'x24' => ['source' => $array24, 'target' =>'sport024.xms011.site'],
            'x25' => ['source' => $array25, 'target' =>'sport025.xms011.site'],
            'x26' => ['source' => $array26, 'target' =>'sport026.xms011.site'],
            'x27' => ['source' => $array27, 'target' =>'sport027.xms011.site'],
            'x28' => ['source' => $array28, 'target' =>'sport028.xms011.site'],
            'x29' => ['source' => $array29, 'target' =>'sport029.xms012.site'],
            'x30' => ['source' => $array30, 'target' =>'sport030.xms012.site'],
            'x31' => ['source' => $array31, 'target' =>'sport031.xms012.site'],
            'x32' => ['source' => $array32, 'target' =>'sport032.xms012.site'],
            'x33' => ['source' => $array33, 'target' =>'sport033.xms012.site'],
            'x34' => ['source' => $array34, 'target' =>'sport034.xms012.site'],
            'x35' => ['source' => $array35, 'target' =>'sport035.xms012.site'],
            'x36' => ['source' => $array36, 'target' =>'sport036.xms012.site'],
            'x37' => ['source' => $array37, 'target' =>'sport037.xms012.site'],
            'x38' => ['source' => $array38, 'target' =>'sport038.xms012.site'],
            'x39' => ['source' => $array39, 'target' =>'sport039.xms012.site'],
            'x40' => ['source' => $array40, 'target' =>'sport040.xms012.site'],
            'x41' => ['source' => $array41, 'target' =>'sport041.xms012.site'],
            'x42' => ['source' => $array42, 'target' =>'sport042.xms012.site'],
            'x43' => ['source' => $array43, 'target' =>'sport043.xms012.site'],
            'x44' => ['source' => $array44, 'target' =>'sport044.xms012.site'],
            'x45' => ['source' => $array45, 'target' =>'sport045.xms012.site'],
            'x46' => ['source' => $array46, 'target' =>'sport046.xms012.site'],
            'x47' => ['source' => $array47, 'target' =>'sport047.xms012.site'],
            'x48' => ['source' => $array48, 'target' =>'sport048.xms013.site'],
            'x49' => ['source' => $array49, 'target' =>'sport049.xms013.site'],
            'x50' => ['source' => $array50, 'target' =>'sport050.xms013.site'],
            'x51' => ['source' => $array51, 'target' =>'sport051.xms013.site'],
            'x52' => ['source' => $array52, 'target' =>'sport052.xms013.site'],
            'x53' => ['source' => $array53, 'target' =>'sport053.xms013.site'],
            'x54' => ['source' => $array54, 'target' =>'sport054.xms013.site'],
            'x55' => ['source' => $array55, 'target' =>'hxfaa'],
            'x56' => ['source' => $array56, 'target' =>'hxfaa'],
            'x57' => ['source' => $array57, 'target' =>'hxfaa'],
            'x58' => ['source' => $array58, 'target' =>'hxfaa'],
            'x59' => ['source' => $array59, 'target' =>'hxfaa'],
            'x60' => ['source' => $array60, 'target' =>'hxfaa'],
            'x61' => ['source' => $array61, 'target' =>'hxfaa'],
            'x62' => ['source' => $array62, 'target' =>'hxfaa'],
            'x63' => ['source' => $array63, 'target' =>'hxfaa'],
            'x64' => ['source' => $array64, 'target' =>'hxfaa'],
            'x65' => ['source' => $array65, 'target' =>'hxfaa'],
            'x66' => ['source' => $array66, 'target' =>'hxfaa'],
            'x67' => ['source' => $array67, 'target' =>'hxfaa'],
            'x68' => ['source' => $array68, 'target' =>'hxfaa'],
            'x70' => ['source' => $array70, 'target' =>'hxfaa'],

            'x69' => ['source' => $array69, 'target' =>'hxfaa'],
            'x71' => ['source' => $array71, 'target' =>'hxfaa'],
            'x72' => ['source' => $array72, 'target' =>'hxfaa'],
            'x73' => ['source' => $array73, 'target' =>'hxfaa'],
            'x74' => ['source' => $array74, 'target' =>'hxfaa'],
            'x75' => ['source' => $array75, 'target' =>'hxfaa'],
            'x76' => ['source' => $array76, 'target' =>'hxfaa'],
            'x77' => ['source' => $array77, 'target' =>'hxfaa'],
            'x78' => ['source' => $array78, 'target' =>'hxfaa'],
            'x79' => ['source' => $array79, 'target' =>'hxfaa'],
            'x80' => ['source' => $array80, 'target' =>'hxfaa'],
            'x81' => ['source' => $array81, 'target' =>'hxfaa'],
            'x82' => ['source' => $array82, 'target' =>'hxfaa'],
            'x83' => ['source' => $array83, 'target' =>'hxfaa'],
            'x84' => ['source' => $array84, 'target' =>'hxfaa'],
            'x85' => ['source' => $array85, 'target' =>'hxfaa'],
            'x86' => ['source' => $array86, 'target' =>'hxfaa'],
            'x87' => ['source' => $array87, 'target' =>'hxfaa'],
            'x88' => ['source' => $array88, 'target' =>'hxfaa'],
            'x89' => ['source' => $array89, 'target' =>'hxfaa'],
            'x90' => ['source' => $array90, 'target' =>'hxfaa'],
            'x91' => ['source' => $array91, 'target' =>'hxfaa'],
            'x92' => ['source' => $array92, 'target' =>'hxfaa'],
   
       
            
 
             //hxfaa
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.go-sport.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            // 小皮密钥
            // 'ck_8e87e9eaf6d75a3c969473d60e14480835b8aae6',
            // 'cs_66331b14ef92978de54f0ed1efdcae1e284e8187',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  
        $breadcrumbs = array_filter($crawler->filter('.c-breadcrumb li')->filter('.c-breadcrumb__text')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        // 进入详情页爬取数据

        $crawler->filter('.c-product-tile__pdp-link a')->each(function (Crawler $node, $i) use($breadcrumbs) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s','https://www.go-sport.com'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,
                
                   
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;    
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.c-product-detail__name')->text();  //标题（必须）
        // print_r($product['title']);exit;

        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('[itemprop="price"]')->text())));  //价格（必须）
        $product['price']=$crawler->filter('.c-prices__sales')->filter('.value')->attr('content');
        // print_r($product['price']);exit;



        $product['brand']=$crawler->filter('.c-product-detail__brand')->text();
       //品牌（必须）

    //    $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('.c-breadcrumb li')->filter('.c-breadcrumb__text')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;

        $product['breadcrumbs'] =array_slice($item['breadcrumbs'],4);
        // foreach ($item['breadcrumbs'] as $breadcrumb) {
        //     array_push($product['breadcrumbs'],$breadcrumb);
        // }
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }


        // print_r($product['breadcrumbs']);exit;
        
   
        $product['sku'] =json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['sku']; 
        // $del= [';'];
        // $int = $crawler->filter('script')->eq(29)->text();  //产品编号（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['sku']=json_decode($it,true)['id'];
        // print_r($product['sku']);exit;
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
       if($crawler->filter('.js-expandable-description')->count()>0){
            $product['description']=$crawler->filter('.js-expandable-description')->html();
       }else{
        $product['description']='';
       }
        

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        
        $del=[':'];
        if($crawler->filter('.c-product-detail__features')->count()) {
            $count = $crawler->filter('[class="c-attribute col-12 col-xmd-6"]')->count();
            // echo $count;exit;
                for($i=0;$i<$count;$i++){
                    $product['attributes'][] = [
                        'name' =>str_replace($del,'',$crawler->filter('[class="c-attribute col-12 col-xmd-6"]')->eq($i)->filter('.c-attribute__label')->text()),
                        'options' =>[$crawler->filter('[class="c-attribute col-12 col-xmd-6"]')->eq($i)->filter('.js-expandable-attribute')->text()],
                    ];
                }       
            
        }else{
            $product['attributes'] = [];
        }
        // print_r($product);
        // exit();
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']='';  

       // x7，x8  使用
        // $gen=['Chaussures'];
        // $product['gender'] = str_replace($gen,'',$crawler->filter('.c-link--no-underline')->filter('.c-breadcrumb__text')->eq(4)->text());
        // print_r($product['gender']);exit;
        // $product['color'] ='';
        
        if($crawler->filter('.c-variation__color-text')->count()>0){
            $product['color'] =$crawler->filter('.c-variation__color-text')->text();
        }else{
            $product['color'] ='';
        }
        
        
        // print_r($product['color']);exit;


        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $img=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$img){
        //     if(strstr($node->text(),"image")){
        //         $img = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=str_replace(' 1300w','',array_unique(array_filter($crawler->filter('.c-pdp__main-image__main--bg')->each(function(Crawler $node,$i){
            return  $node->attr('srcset');
        }))));
        // 这里if是判断图片是否有多张
        $x=1;
        
            foreach ($images as $image) {
                if($x>6)break;
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
                $x++;
            }
        
        // $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($product['images']);exit;
    //   商品的尺寸
        $product['variations']=[];
        // print_r($product);exit;
      
        if($crawler->filter('.c-variation__container-wrapper')->count()>0){$product['variations'][]= [
                'name'=>$crawler->filter('.c-variation__label')->text(),
                'options'=>$crawler->filter('.c-variation__attr-tile')->each(function (Crawler $node, $i) {
                    return $node->attr('title');
                })
            ];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }
      // 用于图片上传失败
      public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}