<?php
namespace app\command\MZL\De;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Promise\Create;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class ambie extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:ambie')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.ambiendo.de/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       $array1=['https://www.ambiendo.de/category/deko-kissen?&sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=Hotelverschluss','https://www.ambiendo.de/category/deko-kissen?&sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=verdeckter+Rei%C3%9Fverschluss','https://www.ambiendo.de/category/deko-kissen?&sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=zugen%C3%A4ht','https://www.ambiendo.de/category/deko-kissen?&sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=Knopf-Verschluss'];
       $array2=['https://www.ambiendo.de/category/deko-kissen?sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=Knopf-Verschluss','https://www.ambiendo.de/category/deko-kissen?sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=Zierkordel+%2F+Schleife','https://www.ambiendo.de/category/deko-kissen?sortpopularity=desc&followSearch=10000&filterAMB_kissen_verschluss=zugen%C3%A4ht','https://www.ambiendo.de/category/deko-kissen?filterAMB_kissen_verschluss=Rei%C3%9Fverschluss&sortpopularity=desc&followSearch=10000'];
       
        // 不需要切
       $array3=['https://www.ambiendo.de/category/innenkissen','https://www.ambiendo.de/category/bodenkissen','https://www.ambiendo.de/category/koernerkissen','https://www.ambiendo.de/category/sitzkissen'];
       $array4=['https://www.ambiendo.de/category/nackenrollen','https://www.ambiendo.de/category/kissen-fuer-tiere','https://www.ambiendo.de/category/kinderkissen','https://www.ambiendo.de/category/koernerkissen'];
       $array5=['https://www.ambiendo.de/category/wohnstuehle','https://www.ambiendo.de/category/sessel','https://www.ambiendo.de/category/buerostuehle','https://www.ambiendo.de/category/stuehle-fuer-kinder'];
       $array6=['https://www.ambiendo.de/category/barhocker','https://www.ambiendo.de/category/sitzsaecke','https://www.ambiendo.de/category/schaukelstuehle','https://www.ambiendo.de/category/sonstige-sitzmoebel'];
       $array7=['https://www.ambiendo.de/category/sessel','https://www.ambiendo.de/category/sitzhocker-poufs','https://www.ambiendo.de/category/stuehle-fuer-kinder'];
       $array8=['https://www.ambiendo.de/category/wohnteppiche','https://www.ambiendo.de/category/fellteppiche-kuhfelle','https://www.ambiendo.de/category/outdoor-teppiche',''];
       $array9=['https://www.ambiendo.de/category/teppichlaeufer','https://www.ambiendo.de/category/fussmatten','https://www.ambiendo.de/category/kinderteppiche','https://www.ambiendo.de/category/stuehle-fuer-kinder'];
       $array10=['https://www.ambiendo.de/category/beistelltische','https://www.ambiendo.de/category/sideboards-konsolen','https://www.ambiendo.de/category/schreibtische-sekretaere','https://www.ambiendo.de/category/fernsehtische','https://www.ambiendo.de/category/esszimmertische'];
       $array11=['https://www.ambiendo.de/category/vorhaenge-gardinen','https://www.ambiendo.de/category/alu-und-metall-fotorahmen','https://www.ambiendo.de/category/holz-fotorahmen','https://www.ambiendo.de/category/passepartout-bilderrahmen','https://www.ambiendo.de/category/fotocollage-ideen','https://www.ambiendo.de/category/rahmen-mit-tiefe','https://www.ambiendo.de/category/spiegel'];
       $array12=['https://www.ambiendo.de/category/tischlampen','https://www.ambiendo.de/category/leuchten-zubehoer','https://www.ambiendo.de/category/deckenstrahler','https://www.ambiendo.de/category/schreibtischlampen'];
       $array13=['https://www.ambiendo.de/category/haengelampen','https://www.ambiendo.de/category/bodenleuchten','https://www.ambiendo.de/category/leuchten-zubehoer'];
       $array14=['https://www.ambiendo.de/category/stehleuchten','https://www.ambiendo.de/category/outdoor-leuchten','https://www.ambiendo.de/category/wandlampen','https://www.ambiendo.de/category/kinderlampen'];
       $array15=['https://www.ambiendo.de/category/bodenvasen','https://www.ambiendo.de/category/tischvasen','https://www.ambiendo.de/category/haengetoepfe','https://www.ambiendo.de/category/vasen-blumentopf-sets'];
       $array16=['https://www.ambiendo.de/category/wandgefaesse','https://www.ambiendo.de/category/toepfe-mit-bewaesserungssystem','https://www.ambiendo.de/category/haengetoepfe',];
       $array17=['https://www.ambiendo.de/category/wanduhren','https://www.ambiendo.de/category/wecker','https://www.ambiendo.de/category/tischuhren','https://www.ambiendo.de/category/pendeluhren',];

    //   跟x1一样 在=号前面加个n
       $array18=['https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=Andere','https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=gestreift','https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=kariert',''];
       $array19=['https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=Ornament','https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/decken-plaids?&sortpopularity=desc&followSearch=9972&filterpattern=uni'];
       
       $array20=['https://www.ambiendo.de/category/decken-fuer-tiere','https://www.ambiendo.de/category/kinderdecken','https://www.ambiendo.de/category/decken-plaids'];
       $array21=['https://www.ambiendo.de/category/weinglaeser','https://www.ambiendo.de/category/trinkglaeser','https://www.ambiendo.de/category/sektglaeser','https://www.ambiendo.de/category/spirituosenglaeser-likoerglaeser','https://www.ambiendo.de/category/dekanter-schwenker','https://www.ambiendo.de/category/kinderglaeser'];
       $array22=['https://www.ambiendo.de/category/kinderglaeser','https://www.ambiendo.de/category/spirituosenglaeser-likoerglaeser','https://www.ambiendo.de/category/longdrink-glaeser','https://www.ambiendo.de/category/cocktail-glaeser','https://www.ambiendo.de/category/glaeser-sets','https://www.ambiendo.de/category/karaffen-kruege'];
       $array23=['https://www.ambiendo.de/category/hundebetten','https://www.ambiendo.de/category/hundenaepfe','https://www.ambiendo.de/category/hundespielzeug','https://www.ambiendo.de/category/hunde-unterwegs','https://www.ambiendo.de/category/futteraufbewahrung','https://www.ambiendo.de/category/katzenbetten','https://www.ambiendo.de/category/katzennaepfe'];
       $array24=['https://www.ambiendo.de/category/wandhaken-tuerhaken','https://www.ambiendo.de/category/garderobenleisten','https://www.ambiendo.de/category/garderobenstaender','https://www.ambiendo.de/category/schirmstaender','https://www.ambiendo.de/category/garderoben-seile','https://www.ambiendo.de/category/kleiderbuegel','https://www.ambiendo.de/category/schirme'];
       $array25=['https://www.ambiendo.de/category/tischdecken','https://www.ambiendo.de/category/mitteldecken','https://www.ambiendo.de/category/servietten','https://www.ambiendo.de/category/serviettenringe'];
    //    跟x1一样，不加其他
       $array26=['https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=oval','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=quadratisch','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=rechteckig','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=rund','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=Sonstige'];
       $array27=['https://www.ambiendo.de/category/untersetzer?&sortpopularity=desc&followSearch=9832&filterAMB_tw_shape=quadratisch','https://www.ambiendo.de/category/untersetzer?&sortpopularity=desc&followSearch=9832&filterAMB_tw_shape=rund','https://www.ambiendo.de/category/untersetzer?&sortpopularity=desc&followSearch=9832&filterAMB_tw_shape=Sonstige'];
          //加n
       $array28=['https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Andere','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=gestreift','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=kariert','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Motiv','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Ornament','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Motiv'];
       
       $array29=['https://www.ambiendo.de/category/duft-diffuser','https://www.ambiendo.de/category/duftkerzen','https://www.ambiendo.de/category/nachfueller','https://www.ambiendo.de/category/seifen-cremes','https://www.ambiendo.de/category/duftkapseln','https://www.ambiendo.de/category/deko-kerzen','https://www.ambiendo.de/category/duftspray','https://www.ambiendo.de/category/duftsaeckchen','https://www.ambiendo.de/category/duft-zubehoer'];
       $array30=['https://www.ambiendo.de/category/kerzenhalter','https://www.ambiendo.de/category/wandkerzenhalter','https://www.ambiendo.de/category/adventskraenze','https://www.ambiendo.de/category/laternen'];
       $array31=['https://www.ambiendo.de/category/windlichter','https://www.ambiendo.de/category/teelichthalter','https://www.ambiendo.de/category/kerzenstaender','https://www.ambiendo.de/category/lichthaeuser'];
       
    //    跟x1一样 在=号前面加个n
       $array32=['https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=uni','https://www.ambiendo.de/category/bettwaesche-sets?sortpopularity=desc&followSearch=9943&filterpattern=gepunktet','https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=gebl%C3%BCmt'];
       $array33=['https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=gestreift','https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=kariert','https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=Motiv','https://www.ambiendo.de/category/bettwaesche-sets?&sortpopularity=desc&followSearch=9943&filterpattern=Ornament'];
       $array34=['https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=Andere','https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=gestreift','https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=kariert','https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=Motiv','https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=Ornament','https://www.ambiendo.de/category/kopfkissenbezuege?&sortpopularity=desc&followSearch=9827&filterpattern=uni'];
       $array35=['https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=Andere','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=gepunktet','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=gestreift','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=kariert','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=Motiv','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=Ornament','https://www.ambiendo.de/category/satin-bettwaesche?&sortpopularity=desc&followSearch=9817&filterpattern=uni'];
       
       $array36=['https://www.ambiendo.de/category/bettdecken-bezuege','https://www.ambiendo.de/category/jersey-bettwaesche','https://www.ambiendo.de/category/kinderbettwaesche','https://www.ambiendo.de/category/flanell-bettwaesche'];
       //    跟x1一样，加s
       $array37=['https://www.ambiendo.de/category/schlafzimmerteppiche?&sortpopularity=desc&followSearch=9782&filtermaterials=Kunstfaser%2FPolyester','https://www.ambiendo.de/category/schlafzimmerteppiche?&sortpopularity=desc&followSearch=9782&filtermaterials=Baumwolle','https://www.ambiendo.de/category/schlafzimmerteppiche?&sortpopularity=desc&followSearch=9782&filtermaterials=Viscose','https://www.ambiendo.de/category/schlafzimmerteppiche?&sortpopularity=desc&followSearch=9782&filtermaterials=Wolle','https://www.ambiendo.de/category/schlafzimmerteppiche?&sortpopularity=desc&followSearch=9782&filtermaterials=Leder'];
    //    按品牌分，跟x1一样，加个d
       $array38=['https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Bassetti','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Beddinghouse','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Bella+Donna','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Bella+Gracia','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Cinderella','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=elegante','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Essenza','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=fleuresse','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=Irisette','https://www.ambiendo.de/category/spannbettlaken?&sortpopularity=desc&followSearch=9905&filterbrand=schlafgut',''];
       
       $array39=['https://www.ambiendo.de/category/topper','https://www.ambiendo.de/category/split-topper','https://www.ambiendo.de/category/boxspring-bettlaken','https://www.ambiendo.de/category/haustuecher'];
       //    跟x1一样 在=号前面加个n
       $array40=['https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=gestreift','https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=Andere','https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=kariert','https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=Motiv','https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=Ornament','https://www.ambiendo.de/category/handtuecher-standardmass?&sortpopularity=desc&followSearch=9846&filterpattern=uni'];
       $array41=['https://www.ambiendo.de/category/gaestetuecher?&sortpopularity=desc&followSearch=9822&filterpattern=Andere','https://www.ambiendo.de/category/gaestetuecher?&sortpopularity=desc&followSearch=9822&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/gaestetuecher?&sortpopularity=desc&followSearch=9822&filterpattern=gestreift','https://www.ambiendo.de/category/gaestetuecher?&sortpopularity=desc&followSearch=9822&filterpattern=kariert','https://www.ambiendo.de/category/gaestetuecher?&sortpopularity=desc&followSearch=9822&filterpattern=Ornament','https://www.ambiendo.de/category/gaestetuecher?&sortpopularity=desc&followSearch=9822&filterpattern=uni'];
       $array42=['https://www.ambiendo.de/category/duschtuecher?&sortpopularity=desc&followSearch=9944&filterpattern=Andere','https://www.ambiendo.de/category/duschtuecher?&sortpopularity=desc&followSearch=9944&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/duschtuecher?&sortpopularity=desc&followSearch=9944&filterpattern=gestreift','https://www.ambiendo.de/category/duschtuecher?&sortpopularity=desc&followSearch=9944&filterpattern=kariert','https://www.ambiendo.de/category/duschtuecher?&sortpopularity=desc&followSearch=9944&filterpattern=Ornament','https://www.ambiendo.de/category/duschtuecher?&sortpopularity=desc&followSearch=9944&filterpattern=uni'];
       $array43=['https://www.ambiendo.de/category/seiftuecher?&sortpopularity=desc&followSearch=9785&filterpattern=gestreift','https://www.ambiendo.de/category/seiftuecher?&sortpopularity=desc&followSearch=9785&filterpattern=Andere','https://www.ambiendo.de/category/seiftuecher?&sortpopularity=desc&followSearch=9785&filterpattern=kariert','https://www.ambiendo.de/category/seiftuecher?&sortpopularity=desc&followSearch=9785&filterpattern=Ornament','https://www.ambiendo.de/category/seiftuecher?&sortpopularity=desc&followSearch=9785&filterpattern=uni'];
      
       $array44=['https://www.ambiendo.de/category/kinder-handtuecher','https://www.ambiendo.de/category/saunatuecher','https://www.ambiendo.de/category/badetuecher','https://www.ambiendo.de/category/strandlaken','https://www.ambiendo.de/category/waschhandschuhe',];
       //    跟x1一样 在=号前面加个n
       $array45=['https://www.ambiendo.de/category/badematten?&sortpopularity=desc&followSearch=9997&filterpattern=Andere','https://www.ambiendo.de/category/badematten?&sortpopularity=desc&followSearch=9997&filterpattern=gestreift','https://www.ambiendo.de/category/badematten?&sortpopularity=desc&followSearch=9997&filterpattern=kariert','https://www.ambiendo.de/category/badematten?&sortpopularity=desc&followSearch=9997&filterpattern=Motiv','https://www.ambiendo.de/category/badematten?&sortpopularity=desc&followSearch=9997&filterpattern=Ornament','https://www.ambiendo.de/category/badematten?&sortpopularity=desc&followSearch=9997&filterpattern=uni'];
       
       $array46=['https://www.ambiendo.de/category/frauen-bademaentel','https://www.ambiendo.de/category/herren-bademaentel','https://www.ambiendo.de/category/kinderbademaentel','https://www.ambiendo.de/category/schalkragen-bademaentel','https://www.ambiendo.de/category/kapuzen-bademaentel','https://www.ambiendo.de/category/kimonos','https://www.ambiendo.de/category/kapuzentuecher','https://www.ambiendo.de/category/kilt'];
       $array47=['https://www.ambiendo.de/category/seifenspender','https://www.ambiendo.de/category/duschablagen-duschkoerbe','https://www.ambiendo.de/category/wattepad-dosen','https://www.ambiendo.de/category/bad-ablageschalen','https://www.ambiendo.de/category/zahnputzbecher'];
       $array48=['https://www.ambiendo.de/category/badeimer','https://www.ambiendo.de/category/toilettenbuersten','https://www.ambiendo.de/category/schmuck-aufbewahrung','https://www.ambiendo.de/category/duschabzieher','https://www.ambiendo.de/category/kosmetikspiegel'];
       $array49=['https://www.ambiendo.de/category/waeschekoerbe','https://www.ambiendo.de/category/toilettenpapierhalter','https://www.ambiendo.de/category/kosmetiktaschen-kulturbeutel','https://www.ambiendo.de/category/kosmetikspiegel','https://www.ambiendo.de/category/handtuchhalter'];
    //    跟x1一样 在=号前面加个s
       $array50=['https://www.ambiendo.de/category/tassen-becher?&sortpopularity=desc&followSearch=9997&filtermaterials=Kunststoff','https://www.ambiendo.de/category/tassen-becher?&sortpopularity=desc&followSearch=9997&filtermaterials=Keramik','https://www.ambiendo.de/category/tassen-becher?&sortpopularity=desc&followSearch=9997&filtermaterials=Kunststoff','https://www.ambiendo.de/category/tassen-becher?&sortpopularity=desc&followSearch=9997&filtermaterials=Polyester','https://www.ambiendo.de/category/tassen-becher?&sortpopularity=desc&followSearch=9997&filtermaterials=Porzellan','https://www.ambiendo.de/category/tassen-becher?sortpopularity=desc&followSearch=9997&filtermaterials=Edelstahl'];
       
       $array51=['https://www.ambiendo.de/category/speiseteller-platzteller','https://www.ambiendo.de/category/gabeln','https://www.ambiendo.de/category/suppenteller-pastateller','https://www.ambiendo.de/category/messer'];
       $array52=['https://www.ambiendo.de/category/kuchenteller-fruehstuecksteller','https://www.ambiendo.de/category/loeffel','https://www.ambiendo.de/category/milchkaennchen-zuckerdosen','https://www.ambiendo.de/category/salatbesteck','https://www.ambiendo.de/category/eierbecher'];
       $array53=['https://www.ambiendo.de/category/besteck-sets','https://www.ambiendo.de/category/teekannen','https://www.ambiendo.de/category/untertassen','https://www.ambiendo.de/category/etagere','https://www.ambiendo.de/category/essen-servieren'];
         //    跟x1一样 在=号前面加个s
       $array54=['https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Edelstahl','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Glas','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Keramik','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Kunststoff','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Metall','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Polyester','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Stahl','https://www.ambiendo.de/category/schalen-schuesseln?&sortpopularity=desc&followSearch=10000&filtermaterials=Porzellan'];
       
       $array55=['https://www.ambiendo.de/category/brotkaesten-brotkoerbe','https://www.ambiendo.de/category/vorratsbehaelter','https://www.ambiendo.de/category/kaffeekannen','https://www.ambiendo.de/category/kuechenregale','https://www.ambiendo.de/category/servierwagen','https://www.ambiendo.de/category/flaschenregale','https://www.ambiendo.de/category/abtropfgestelle','https://www.ambiendo.de/category/lunchboxen','https://www.ambiendo.de/category/serviettenhalter'];
       $array56=['https://www.ambiendo.de/category/kochtoepfe','https://www.ambiendo.de/category/bratpfannen','https://www.ambiendo.de/category/kasserollen','https://www.ambiendo.de/category/kochtopf-sets','https://www.ambiendo.de/category/braeter','https://www.ambiendo.de/category/auflaufformen-backformen','https://www.ambiendo.de/category/topfuntersetzer','https://www.ambiendo.de/category/wasserkocher-kessel','https://www.ambiendo.de/category/kaffeemuehlen','https://www.ambiendo.de/category/kaffeebereiter','https://www.ambiendo.de/category/mixer','https://www.ambiendo.de/category/teezubereiter','https://www.ambiendo.de/category/fondues-raclettes','https://www.ambiendo.de/category/schuerzen','https://www.ambiendo.de/category/geschirrtuecher','https://www.ambiendo.de/category/topfhandschuhe','https://www.ambiendo.de/category/topflappen','https://www.ambiendo.de/category/spueltuecher','https://www.ambiendo.de/category/kinder-kuechentextilien'];
       $array57=['https://www.ambiendo.de/category/gewuerzmuehlen-streuer','https://www.ambiendo.de/category/kuechentimer','https://www.ambiendo.de/category/kuechenrollenhalter','https://www.ambiendo.de/category/flaschenoeffner-korkenzieher','https://www.ambiendo.de/category/oel-essigspender',];
       $array58=['https://www.ambiendo.de/category/schneidebretter-arbeitsbretter','https://www.ambiendo.de/category/spuel-utensilien','https://www.ambiendo.de/category/bar-zubehoer','https://www.ambiendo.de/category/messer-zubehoer','https://www.ambiendo.de/category/kaesemesser-kaesehobel','https://www.ambiendo.de/category/schneidemesser','https://www.ambiendo.de/category/brotmesser','https://www.ambiendo.de/category/kochutensilien'];
       $array59=['https://www.ambiendo.de/category/gartenkissen','https://www.ambiendo.de/category/bbq-grills','https://www.ambiendo.de/category/grillzangen-grillwender','https://www.ambiendo.de/category/grill-erweiterungen','https://www.ambiendo.de/category/barbecue-werkzeuge','https://www.ambiendo.de/category/strandtaschen-einkaufstaschen','https://www.ambiendo.de/category/feuerschalen-gartenfackeln','https://www.ambiendo.de/category/liegenauflagen'];
       $array60=['https://www.ambiendo.de/category/tischdecken','https://www.ambiendo.de/category/servietten','https://www.ambiendo.de/category/mitteldecken','https://www.ambiendo.de/category/serviettenringe'];
   //    跟x1一样 在=号前面加个e
       $array61=['https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=oval','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=quadratisch','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=rechteckig','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=rund','https://www.ambiendo.de/category/tischsets?&sortpopularity=desc&followSearch=10000&filterAMB_tw_shape=Sonstige'];
       $array62=['https://www.ambiendo.de/category/untersetzer?&sortpopularity=desc&followSearch=9824&filterAMB_tw_shape=quadratisch','https://www.ambiendo.de/category/untersetzer?&sortpopularity=desc&followSearch=9824&filterAMB_tw_shape=rund','https://www.ambiendo.de/category/untersetzer?&sortpopularity=desc&followSearch=9824&filterAMB_tw_shape=Sonstige'];
       //    跟x1一样 在=号前面加个n
       $array63=['https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Andere','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=gebl%C3%BCmt','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=gestreift','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=kariert','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Motiv','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=Ornament','https://www.ambiendo.de/category/tischlaeufer?&sortpopularity=desc&followSearch=9831&filterpattern=uni'];
       
       
       $array64=['https://www.ambiendo.de/category/thermosflaschen','https://www.ambiendo.de/category/trinkflaschen-ohne-isolierung','https://www.ambiendo.de/category/isolierbecher','https://www.ambiendo.de/category/thermoskannen','https://www.ambiendo.de/category/trinkflaschen-zubehoer'];






        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'snipes001.xms002.site'],
            'x2' => ['source' => $array2, 'target' =>'snipes002.xms002.site'],
            'x3' => ['source' => $array3, 'target' =>'ambie003.xms007.site'],
            'x4' => ['source' => $array4, 'target' =>'ambie003.xms008.site'],
            'x5' => ['source' => $array5, 'target' =>'ambie004.xms008.site'],
            'x6' => ['source' => $array6, 'target' =>'ambie006.xms008.site'],
            'x7' => ['source' => $array7, 'target' =>'ambie007.xms008.site'],
            'x8' => ['source' => $array8, 'target' =>'ambie008.xms008.site'],
            'x9' => ['source' => $array9, 'target' =>'ambie009.xms008.site'],
            'x10' => ['source' => $array10, 'target' =>'ambie0010.xms008.site'],
            'x11' => ['source' => $array11, 'target' =>'ambie0011.xms008.site'],
            'x12' => ['source' => $array12, 'target' =>'ambie0012.xms008.site'],
            'x13' => ['source' => $array13, 'target' =>'ambie0013.xms008.site'],
            'x14' => ['source' => $array14, 'target' =>'ambie0014.xms008.site'],
            'x15' => ['source' => $array15, 'target' =>'ambie0015.xms008.site'],
            'x16' => ['source' => $array16, 'target' =>'ambie0016.xms008.site'],
            'x17' => ['source' => $array17, 'target' =>'ambie0017.xms008.site'],
            'x18' => ['source' => $array18, 'target' =>'ambie0018.xms008.site'],
            'x19' => ['source' => $array19, 'target' =>'ambie0019.xms008.site'],
            'x20' => ['source' => $array20, 'target' =>'ambie0020.xms009.site'],
            'x21' => ['source' => $array21, 'target' =>'ambie0021.xms009.site'],
            'x22' => ['source' => $array22, 'target' =>'ambie0022.xms009.site'],
            'x23' => ['source' => $array23, 'target' =>'ambie0023.xms009.site'],
            'x24' => ['source' => $array24, 'target' =>'ambie0024.xms009.site'],
            'x25' => ['source' => $array25, 'target' =>'ambie0025.xms009.site'],
            'x26' => ['source' => $array26, 'target' =>'ambie0026.xms009.site'],
            'x27' => ['source' => $array27, 'target' =>'ambie0027.xms009.site'],
            'x28' => ['source' => $array28, 'target' =>'ambie0028.xms009.site'],
            'x29' => ['source' => $array29, 'target' =>'ambie0029.xms009.site'],
            'x30' => ['source' => $array30, 'target' =>'ambie0030.xms009.site'],
            'x31' => ['source' => $array31, 'target' =>'ambie0031.xms009.site'],
            'x32' => ['source' => $array32, 'target' =>'ambie0032.xms009.site'],
            'x33' => ['source' => $array33, 'target' =>'ambie0033.xms009.site'],
            'x34' => ['source' => $array34, 'target' =>'ambie0034.xms009.site'],
            'x35' => ['source' => $array35, 'target' =>'ambie0035.xms009.site'],
            'x36' => ['source' => $array36, 'target' =>'ambie0036.xms009.site'],
            'x37' => ['source' => $array37, 'target' =>'ambie0037.xms009.site'],
            'x38' => ['source' => $array38, 'target' =>'ambie0038.xms009.site'],
            'x39' => ['source' => $array39, 'target' =>'ambie0039.xms009.site'],
            'x40' => ['source' => $array40, 'target' =>'ambie0040.xms009.site'],
            'x41' => ['source' => $array41, 'target' =>'ambie0041.xms009.site'],
            'x42' => ['source' => $array42, 'target' =>'ambie0042.xms009.site'],
            'x43' => ['source' => $array43, 'target' =>'ambie0043.xms009.site'],
            'x44' => ['source' => $array44, 'target' =>'ambie0044.xms009.site'],
            'x45' => ['source' => $array45, 'target' =>'ambie0045.xms009.site'],
            'x46' => ['source' => $array46, 'target' =>'ambie0046.xms009.site'],
            'x47' => ['source' => $array47, 'target' =>'ambie0047.xms009.site'],
            'x48' => ['source' => $array48, 'target' =>'ambie0048.xms009.site'],
            'x49' => ['source' => $array49, 'target' =>'ambie0049.xms009.site'],
            'x50' => ['source' => $array50, 'target' =>'ambie0050.xms010.site'],
            'x51' => ['source' => $array51, 'target' =>'ambie0051.xms010.site'],
            'x52' => ['source' => $array52, 'target' =>'ambie0052.xms010.site'],
            'x53' => ['source' => $array53, 'target' =>'ambie0053.xms010.site'],
            'x54' => ['source' => $array54, 'target' =>'ambie0054.xms010.site'],
            'x55' => ['source' => $array55, 'target' =>'ambie0055.xms010.site'],
            'x56' => ['source' => $array56, 'target' =>'ambie0056.xms010.site'],
            'x57' => ['source' => $array57, 'target' =>'ambie0057.xms010.site'],
            'x58' => ['source' => $array58, 'target' =>'ambie0058.xms011.site'],
            'x59' => ['source' => $array59, 'target' =>'ambie0059.xms011.site'],
            'x60' => ['source' => $array60, 'target' =>'ambie0060.xms011.site'],
            'x61' => ['source' => $array61, 'target' =>'ambie0061.xms011.site'],
            'x62' => ['source' => $array62, 'target' =>'ambie0062.xms011.site'],
            'x63' => ['source' => $array63, 'target' =>'ambie0063.xms011.site'],
            'x64' => ['source' => $array64, 'target' =>'ambie0064.xms011.site'],



            
             
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.ambiendo.de/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage('https://www.ambiendo.de'.$nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  

        $breadcrumbs=$crawler->filter('[class="row align-items-stretch"] a')->attr('href');
        $a=explode("n=",$breadcrumbs);
        $a=explode("&",$a[1]);
        // print_r($a[0]);exit;
        // print_r($product['breadcrumbs'][0]);

        // $breadcrumbs = $crawler->filter('.currently')->filter('.value')->text();
         
        // 进入详情页爬取数据                                                                       
        $crawler->filter('[class="click product-item-link"]')->each(function (Crawler $node, $i) use ($a) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s','https://www.ambiendo.de'.$node->attr('href')),
                    'breadcrumbs'=>$a,
                   
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
        
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        // $product['title'] = $crawler->filter('.detail-title')->text();  //标题（必须）
        $product['title'] =json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['name']; 
        // print_r($product['title']);exit;
        $product['price'] =json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['offers']['price']; 

        // print_r($product['price']);exit;

        $product['brand']=$crawler->filter('.producer-logo')->filter('.lazyload')->attr('alt');
       //品牌（必须）

        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.breadcrumb ul li a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->attr('title');
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2,-1);
        array_push($product['breadcrumbs'],$item['breadcrumbs'][0]);

        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
        // $product['sku']=$crawler->filter('[id="produit_id" ]')->attr('value');
        $product['sku'] =json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['sku']; 

        // print_r( $product['sku']);exit;  //产品编号（必须）
       

         $product['short_description']=$crawler->filter('[class="block mb-4"] p')->eq(0)->html();
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
      if($crawler->filter('.product-description-additional p')->count()){
        $product['description']=$crawler->filter('.product-description-additional p')->html().$crawler->filter('.product-description-details')->html();
      }else{
        $product['description']=$crawler->filter('.product-description-details')->html();
      }
       
        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        $del=[':'];
        if($crawler->filter('[class="product-description mb-2"]')->count()) {
            $count = $crawler->filter('[class="product-description mb-2"]')->filter('[class="col"]')->count();
            // echo $count;
                for($i=0;$i<$count;$i++){
                    $product['attributes'][] = [
                        'name' =>str_replace($del,'',$crawler->filter('[class="col text-bold"]')->eq($i)->text()),
                        'options' =>$crawler->filter('[class="product-description mb-2"]')->filter('[class="col"]')->eq($i)->filter('[class="col"]')->text(),
                    ];
                }       
            
        }else{
            $product['attributes'] = [];
        }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        // $product['gender']='';  x8使用
        $product['gender'] = '';
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

    
        $images=array_unique(array_filter($crawler->filter('.detail-media-item-controller')->each(function(Crawler $node,$i){
            return  $node->attr('href');
        })));
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($images);exit;
    //   商品的尺寸
         
         

        // $xx=[];
        $product['variations']=[];

        // if($crawler->filter('.select-variant')->count()){
        //          $product['variations'][]= [
                    
        //         'name'=>$crawler->filter('[class="selected default size-choice"]')->filter('.value')->text(),
        //         'options'=>$crawler->filter('.variant-options li')->filter('.size-label')->each(function (Crawler $node,$i){
        //             return $node->text();
        //         })

        //     ];  
       
        // }else{$product['variations'][]=[
        //         'name'=>'',
        //         'options' =>""];
        // }
        // if ($product['variations'][0]['options']){
        //     $product['type'] = 'variable';
        // }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
       
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}