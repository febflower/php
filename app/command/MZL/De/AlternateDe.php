<?php

namespace app\command\LSC\De;


use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\Tests\Compiler\E;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class AlternateDe extends Command
{

    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:lsc:alternate')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.alternate.de');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=['https://www.alternate.de/listing_ajax.xhtml?_=1665135834793&t=16377&af=true&page=6&_=1665135849109'];
        $pmeq043=["https://www.alternate.de/listing_ajax.xhtml?_=1665194565282&t=16216&af=true&page=1&_=1665194576523","https://www.alternate.de/listing_ajax.xhtml?_=1665194565282&t=16216&af=true&page=2&_=1665194576523","https://www.alternate.de/listing_ajax.xhtml?_=1665194565282&t=16216&af=true&page=3&_=1665194576523","https://www.alternate.de/listing_ajax.xhtml?_=1665194565282&t=16216&af=true&page=4&_=1665194576523","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=1&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=2&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=3&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=4&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=5&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=6&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=7&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=8&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=9&_=1665194674007","https://www.alternate.de/listing_ajax.xhtml?_=1665194662135&t=16217&af=true&page=10&_=1665194674007",'https://www.alternate.de/Dunstabzugshauben/Zwischenbauhauben',"https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=1&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=2&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=3&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=4&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=5&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=6&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=7&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194749885&t=16220&af=true&page=8&_=1665194758031","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=1&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=2&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=3&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=4&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=5&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=6&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=7&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=8&_=1665194817561","https://www.alternate.de/listing_ajax.xhtml?_=1665194807706&t=23496&af=true&page=9&_=1665194817561",];
        $pmeq049=["https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=1&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=2&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=3&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=4&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=5&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=6&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=7&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=8&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=9&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=10&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=11&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=12&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=13&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=14&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=15&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=16&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=17&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=18&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=19&_=1665199633956","https://www.alternate.de/listing_ajax.xhtml?_=1665199629026&t=16192&af=true&page=20&_=1665199633956",'https://www.alternate.de/listing_ajax.xhtml?_=1665199739828&t=16193&af=true&page=1&_=1665199747950','https://www.alternate.de/listing_ajax.xhtml?_=1665199739828&t=16193&af=true&page=2&_=1665199747950','https://www.alternate.de/listing_ajax.xhtml?_=1665199739828&t=16193&af=true&page=3&_=1665199747950',"https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=1&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=2&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=3&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=4&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=5&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=6&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=7&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=8&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=9&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199789627&t=16190&af=true&page=10&_=1665199798509","https://www.alternate.de/listing_ajax.xhtml?_=1665199879982&t=16194&af=true&page=1&_=1665199889899","https://www.alternate.de/listing_ajax.xhtml?_=1665199879982&t=16194&af=true&page=2&_=1665199889899","https://www.alternate.de/listing_ajax.xhtml?_=1665199879982&t=16194&af=true&page=3&_=1665199889899","https://www.alternate.de/listing_ajax.xhtml?_=1665199879982&t=16194&af=true&page=4&_=1665199889899","https://www.alternate.de/listing_ajax.xhtml?_=1665199879982&t=16194&af=true&page=5&_=1665199889899",];
        $pmeq050=["https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=1&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=2&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=3&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=4&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=5&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=6&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=7&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=8&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=9&_=1665200355500","https://www.alternate.de/listing_ajax.xhtml?_=1665200340079&t=16222&af=true&page=10&_=1665200355500",'https://www.alternate.de/listing_ajax.xhtml?_=1665200425621&t=16223&af=true&page=2&_=1665200437582','https://www.alternate.de/listing_ajax.xhtml?_=1665200425621&t=16223&af=true&page=1&_=1665200437582','https://www.alternate.de/Einbauherde','https://www.alternate.de/Einbau-Dampfgarer','https://www.alternate.de/listing_ajax.xhtml?_=1665200501366&t=16230&af=true&page=2&_=1665200510560','https://www.alternate.de/listing_ajax.xhtml?_=1665200501366&t=16230&af=true&page=1&_=1665200510560','https://www.alternate.de/listing_ajax.xhtml?_=1665200501366&t=16230&af=true&page=3&_=1665200510560',"https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=1&_=1665200547561","https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=2&_=1665200547561","https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=3&_=1665200547561","https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=4&_=1665200547561","https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=5&_=1665200547561","https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=6&_=1665200547561","https://www.alternate.de/listing_ajax.xhtml?_=1665200540181&t=16224&af=true&page=7&_=1665200547561",'https://www.alternate.de/listing_ajax.xhtml?_=1665200590482&t=16234&af=true&page=2&_=1665200598309','https://www.alternate.de/listing_ajax.xhtml?_=1665200590482&t=16234&af=true&page=1&_=1665200598309','https://www.alternate.de/listing_ajax.xhtml?_=1665200590482&t=16234&af=true&page=3&_=1665200598309','https://www.alternate.de/W%C3%A4rmeschubladen','https://www.alternate.de/Gasherde',"https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=1&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=2&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=3&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=4&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=5&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=6&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=7&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=8&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=9&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=10&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=11&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=12&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=13&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=14&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=15&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=16&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=17&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=18&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=19&_=1665200687541","https://www.alternate.de/listing_ajax.xhtml?_=1665200680009&t=16232&af=true&page=20&_=1665200687541",];
        $pmeq051=["https://www.alternate.de/listing_ajax.xhtml?_=1665200749854&t=16208&af=true&page=5&_=1665200764058","https://www.alternate.de/listing_ajax.xhtml?_=1665200749854&t=16208&af=true&page=6&_=1665200764058","https://www.alternate.de/listing_ajax.xhtml?_=1665200749854&t=16208&af=true&page=7&_=1665200764058","https://www.alternate.de/listing_ajax.xhtml?_=1665200749854&t=16208&af=true&page=8&_=1665200764058","https://www.alternate.de/listing_ajax.xhtml?_=1665200749854&t=16208&af=true&page=9&_=1665200764058","https://www.alternate.de/listing_ajax.xhtml?_=1665200749854&t=16208&af=true&page=10&_=1665200764058",'https://www.alternate.de/Gefriertruhen',"https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=1&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=2&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=3&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=4&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=5&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=6&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=7&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=8&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=9&_=1665200864630","https://www.alternate.de/listing_ajax.xhtml?_=1665200858036&t=16226&af=true&page=10&_=1665200864630",];
        $pmeq052=["https://www.alternate.de/listing_ajax.xhtml?_=1665201007783&t=16197&af=true&page=6&_=1665201023881","https://www.alternate.de/listing_ajax.xhtml?_=1665201007783&t=16197&af=true&page=7&_=1665201023881","https://www.alternate.de/listing_ajax.xhtml?_=1665201007783&t=16197&af=true&page=8&_=1665201023881","https://www.alternate.de/listing_ajax.xhtml?_=1665201007783&t=16197&af=true&page=9&_=1665201023881","https://www.alternate.de/listing_ajax.xhtml?_=1665201007783&t=16197&af=true&page=10&_=1665201023881",'https://www.alternate.de/Getr%C3%A4nkek%C3%BChlschr%C3%A4nke',"https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=1&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=2&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=3&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=4&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=5&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=6&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=7&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=8&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=9&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201163436&t=16201&af=true&page=10&_=1665201169403","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=1&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=2&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=3&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=4&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=5&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=6&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=7&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=8&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=9&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201217346&t=16202&af=true&page=10&_=1665201223304","https://www.alternate.de/listing_ajax.xhtml?_=1665201254120&t=16206&af=true&page=1&_=1665201262663","https://www.alternate.de/listing_ajax.xhtml?_=1665201254120&t=16206&af=true&page=2&_=1665201262663","https://www.alternate.de/listing_ajax.xhtml?_=1665201254120&t=16206&af=true&page=3&_=1665201262663","https://www.alternate.de/listing_ajax.xhtml?_=1665201254120&t=16206&af=true&page=4&_=1665201262663","https://www.alternate.de/listing_ajax.xhtml?_=1665201254120&t=16206&af=true&page=5&_=1665201262663",'https://www.alternate.de/listing_ajax.xhtml?_=1665201291340&t=29764&af=true&page=2&_=1665201300330','https://www.alternate.de/listing_ajax.xhtml?_=1665201291340&t=29764&af=true&page=2&_=1665201300330',"https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=1&_=1665201333702","https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=2&_=1665201333702","https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=3&_=1665201333702","https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=4&_=1665201333702","https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=5&_=1665201333702","https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=6&_=1665201333702","https://www.alternate.de/listing_ajax.xhtml?_=1665201327064&t=16198&af=true&page=7&_=1665201333702",'https://www.alternate.de/Unterbau-K%C3%BChlschr%C3%A4nke','https://www.alternate.de/Weink%C3%BChlschr%C3%A4nke',"https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=1&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=2&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=3&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=4&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=5&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=6&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201397810&t=30119&af=true&page=7&_=1665201403315","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=1&_=1665201437404","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=2&_=1665201437404","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=3&_=1665201437404","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=4&_=1665201437404","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=5&_=1665201437404","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=6&_=1665201437404","https://www.alternate.de/listing_ajax.xhtml?_=1665201432184&t=16210&af=true&page=7&_=1665201437404",];
        $pmeq053=["https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=1&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=2&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=3&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=4&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=5&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=6&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=7&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=8&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=9&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209078568&t=16236&af=true&page=10&_=1665209087602","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=1&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=2&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=3&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=4&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=5&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=6&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=7&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=8&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=9&_=1665209260069","https://www.alternate.de/listing_ajax.xhtml?_=1665209232114&t=16239&af=true&page=10&_=1665209260069",'https://www.alternate.de/Gefriertruhen'];
        $pmeq054=['https://www.alternate.de/Bohnermaschine','https://www.alternate.de/listing_ajax.xhtml?_=1665209408566&t=16350&af=true&page=2&_=1665209415560','https://www.alternate.de/listing_ajax.xhtml?_=1665209408566&t=16350&af=true&page=1&_=1665209415560','https://www.alternate.de/listing_ajax.xhtml?_=1665209408566&t=16350&af=true&page=3&_=1665209415560','https://www.alternate.de/CO2-Messger%C3%A4te','https://www.alternate.de/listing_ajax.xhtml?_=1665209460784&t=16322&af=true&page=2&_=1665209467109','https://www.alternate.de/listing_ajax.xhtml?_=1665209460784&t=16322&af=true&page=3&_=1665209467109','https://www.alternate.de/listing_ajax.xhtml?_=1665209460784&t=16322&af=true&page=1&_=1665209467109','https://www.alternate.de/listing_ajax.xhtml?_=1665209498576&t=16324&af=true&page=2&_=1665209505526','https://www.alternate.de/listing_ajax.xhtml?_=1665209498576&t=16324&af=true&page=1&_=1665209505526',"https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=1&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=2&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=3&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=4&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=5&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=6&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=7&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=8&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=9&_=1665209538090","https://www.alternate.de/listing_ajax.xhtml?_=1665209530903&t=16328&af=true&page=10&_=1665209538090",'https://www.alternate.de/Haushaltskleinger%C3%A4te/Hygrometer','https://www.alternate.de/Klimager%C3%A4te','https://www.alternate.de/Luftbefeuchter','https://www.alternate.de/Luftbefeuchter','https://www.alternate.de/listing_ajax.xhtml?_=1665209634297&t=16336&af=true&page=2&_=1665209643247','https://www.alternate.de/listing_ajax.xhtml?_=1665209634297&t=16336&af=true&page=1&_=1665209643247','https://www.alternate.de/listing_ajax.xhtml?_=1665209634297&t=16336&af=true&page=3&_=1665209643247','https://www.alternate.de/N%C3%A4hmaschinen','https://www.alternate.de/Personenwaagen',"https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=1&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=2&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=3&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=4&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=5&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=6&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=7&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=8&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=9&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=10&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=11&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=12&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=13&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=14&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=15&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=16&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=17&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=18&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=19&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=20&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=21&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=22&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=23&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=24&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=25&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=26&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=27&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=28&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=29&_=1665209780066","https://www.alternate.de/listing_ajax.xhtml?_=1665209765240&t=18622&af=true&page=30&_=1665209780066",'https://www.alternate.de/Ventilatoren'];
        $pmeq055=["https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=1&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=2&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=3&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=4&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=5&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=6&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=7&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=8&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=9&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=10&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=11&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=12&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=13&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=14&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210187741&t=16246&af=true&page=15&_=1665210194480","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=1&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=2&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=3&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=4&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=5&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=6&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=7&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=8&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=9&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=10&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=11&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=12&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=13&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=14&_=1665210255274","https://www.alternate.de/listing_ajax.xhtml?_=1665210250381&t=22592&af=true&page=15&_=1665210255274",'https://www.alternate.de/listing_ajax.xhtml?_=1665210329628&t=16299&af=true&page=2&_=1665210340924','https://www.alternate.de/listing_ajax.xhtml?_=1665210329628&t=16299&af=true&page=1&_=1665210340924','https://www.alternate.de/listing_ajax.xhtml?_=1665210329628&t=16299&af=true&page=3&_=1665210340924','https://www.alternate.de/SodaStream','https://www.alternate.de/listing_ajax.xhtml?_=1665210411167&t=16281&af=true&page=2&_=1665210420270','https://www.alternate.de/listing_ajax.xhtml?_=1665210411167&t=16281&af=true&page=1&_=1665210420270'];
        $pmeq056=['https://www.alternate.de/listing_ajax.xhtml?_=1665210411167&t=16281&af=true&page=2&_=1665210420270','https://www.alternate.de/listing_ajax.xhtml?_=1665210411167&t=16281&af=true&page=1&_=1665210420270','https://www.alternate.de/listing_ajax.xhtml?_=1665210411167&t=16281&af=true&page=3&_=1665210420270','https://www.alternate.de/listing_ajax.xhtml?_=1665210411167&t=16281&af=true&page=4&_=1665210420270',"https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=1&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=2&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=3&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=4&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=5&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=6&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=7&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=8&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=9&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=10&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=11&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=12&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=13&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=14&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=15&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=16&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=17&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=18&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=19&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=20&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=21&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=22&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=23&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=24&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=25&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=26&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=27&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=28&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=29&_=1665210519078","https://www.alternate.de/listing_ajax.xhtml?_=1665210512470&t=22591&af=true&page=30&_=1665210519078",'https://www.alternate.de/Teebereiter','https://www.alternate.de/listing_ajax.xhtml?_=1665210561662&t=16284&af=true&page=2&_=1665210566982','https://www.alternate.de/listing_ajax.xhtml?_=1665210561662&t=16284&af=true&page=1&_=1665210566982','https://www.alternate.de/listing_ajax.xhtml?_=1665210561662&t=16284&af=true&page=3&_=1665210566982','https://www.alternate.de/listing_ajax.xhtml?_=1665210561662&t=16284&af=true&page=4&_=1665210566982',"https://www.alternate.de/listing_ajax.xhtml?_=1665210613141&t=16300&af=true&page=1&_=1665210620434","https://www.alternate.de/listing_ajax.xhtml?_=1665210613141&t=16300&af=true&page=2&_=1665210620434","https://www.alternate.de/listing_ajax.xhtml?_=1665210613141&t=16300&af=true&page=3&_=1665210620434","https://www.alternate.de/listing_ajax.xhtml?_=1665210613141&t=16300&af=true&page=4&_=1665210620434","https://www.alternate.de/listing_ajax.xhtml?_=1665210613141&t=16300&af=true&page=5&_=1665210620434","https://www.alternate.de/listing_ajax.xhtml?_=1665210613141&t=16300&af=true&page=6&_=1665210620434",];

        $this->sites = [
            'x' => ['source' => $array, 'target' => 'power1121'],
            'pmeq043' => ['source' => $pmeq043, 'target' => 'pmeq043.seo062.site'],
            'pmeq049' => ['source' => $pmeq049, 'target' => 'pmeq049.seo062.site'],
            'pmeq050' => ['source' => $pmeq050, 'target' => 'pmeq050.seo062.site'],
            'pmeq051' => ['source' => $pmeq051, 'target' => 'pmeq051.seo062.site'],
            'pmeq052' => ['source' => $pmeq052, 'target' => 'pmeq052.seo062.site'],
            'pmeq053' => ['source' => $pmeq053, 'target' => 'pmeq053.seo062.site'],
            'pmeq054' => ['source' => $pmeq054, 'target' => 'pmeq054.seo062.site'],
            'pmeq055' => ['source' => $pmeq055, 'target' => 'pmeq055.seo062.site'],
            'pmeq056' => ['source' => $pmeq056, 'target' => 'pmeq056.seo062.site'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
//            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.alternate.de',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);

        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_a7072feb200173bbe09f0c880e4ba90d66683d65',
            'cs_68abd59b70e7f30cccaf133cfaf54093408ccf21',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }

    protected function execute(Input $input, Output $output)
    {

        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;

        $this->output->writeln($url);

        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
//        print_r($contents);exit();

        $this->processProductList($crawler);

        $nextNode = $crawler->filter('[rel="next"]');
//        echo $nextNode->attr("href");
        if ($nextNode->count()) {
            $this->processPage($nextNode->attr('href'));
        }
    }

    protected function processProductList(Crawler $crawler)
    {

        $crawler->filter('.productBox')->each(function (Crawler $node, $i)  {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }


    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

        $product['title'] = $crawler->filter('.product-name')->text();
        $product['price'] = $crawler->filter('[itemprop="price"]')->attr('content');
        $product['brand'] = $crawler->filter('[itemprop="brand"] span')->text();
        $product['type'] = 'simple';


        $images = array_unique(array_filter($crawler->filter('#gallery-images span')->each(function (Crawler $node, $i) {
            return str_replace('Medisana_MG_500_Massagegun_Pro__Massageger_t@@','','https://www.alternate.de'.$node->attr("data-var"));
        })));

        $x=1;
        foreach ($images as $image) {
            if ($x>6) break;
            $product['images'][] = [
                'src' => $image,
                'name' => $product['title'],
            ];
            $x++;
        }
        $breadcrumbs = array_filter($crawler->filter('[itemprop="itemListElement"]')->last()->each(function (Crawler $node) {
            return $node->text();
        }));
        $product['breadcrumbs'] = array_slice($breadcrumbs,0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        $product['attributes'] = [];
        $product['keywords'] = [];
        $product['gender'] = ' ';
        $product['color']='';
//        $product['color'] = $crawler->filter('[property="product:color"]')->attr('content');
        $product['subCategory']=end($product['breadcrumbs']);
//        $product['tags'][]=$product['color'];
        $product['tags'][] =$product['brand'];
        $product['tags'] = $this->createProductTag($product['tags']);
        $product['variations']=[];


//        $dels=['color.name = ',"'",'[',']'];



        if ($crawler->filter('.sizes')->count()){
            $product['variations'][]= [
                'name'=>'taille',
                'options' =>$crawler->filter('.sizes ul li')->each(function (Crawler $node, $i) {
                    return $node->text();
                })
            ];
            $product['type'] = 'variable';
        }

        if ($crawler->filter('.product-variants-item')->count()){
            $product['variations'][]= [
                'name'=>'Taille',
                'options' =>$crawler->filter('.product-variants-item select option')->each(function (Crawler $node, $i) {
                    return $node->text();
                })
            ];
            $product['type'] = 'variable';
        }
        if($crawler->filter("#product-details div table tr")->count()) {
            $count = $crawler->filter("#product-details div table tr")->count();
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter("#product-details div table tr")->eq($i)->filter('td')->text(),
                    'options' => $crawler->filter("#product-details div table tr")->eq($i)->filter('td')->eq(1)->text(),
                ];
            }
        }
        $del=['1. Achetez une machine de la sélection VIPros :','Cumulez 15 points VIPros','2. Créditez vos points VIPros sur ','3. Et choisissez parmi une sélection de plusieurs milliers de cadeaux','Bosch met en place le VERY IMPORTANT PROMOS','http://www.vipros.fr','Description du','Description'];
        $product['description'] = str_replace($del,'',$crawler->filter('[itemprop="description"]')->html());
        $product['sku'] =$crawler->filter('[itemprop="sku"]')->attr('content');

//        print_r($product);
//        exit();
        $this->createProduct($product);
        echo "\r\n";
        echo "1";
    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
}