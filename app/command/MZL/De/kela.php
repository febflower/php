<?php
namespace app\command\MZL\De;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class kela extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:kela')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.kela.de');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       
         $array1=['https://www.kela.de/de/bad-accessoires/seifenspender/','https://www.kela.de/de/bad-accessoires/seifenschalen/','https://www.kela.de/de/bad-accessoires/zahnputzbecher/','https://www.kela.de/de/bad-accessoires/wattedose/','https://www.kela.de/de/bad-accessoires/spiegel/','https://www.kela.de/de/bad-accessoires/seifenspenderpumpen/','https://www.kela.de/de/bad-accessoires/desinfektionsspender/'];
         $array2=['https://www.kela.de/de/muelleimer/treteimer/','https://www.kela.de/de/muelleimer/schwingdeckeleimer/','https://www.kela.de/de/muelleimer/papierkorb-abfalleimer/','https://www.kela.de/de/muelleimer/ersatzeimer/category-1-250.html','https://www.kela.de/de/badeinrichtung/toilettengarnitur/','https://www.kela.de/de/badeinrichtung/handtuchhalter/','https://www.kela.de/de/badeinrichtung/wandaccessoires/','https://www.kela.de/de/badeinrichtung/moebel/','https://www.kela.de/de/badeinrichtung/personenwaage/','https://www.kela.de/de/toilettenausstattung/wc-garnitur/','https://www.kela.de/de/toilettenausstattung/wc-buerste-mit-griff/','https://www.kela.de/de/toilettenausstattung/wc-ersatzbuerste/','https://www.kela.de/de/toilettenausstattung/toilettenpapierhalter/','https://www.kela.de/de/toilettenausstattung/ersatzrollenhalter/','https://www.kela.de/de/badtextilien/waschlappen/','https://www.kela.de/de/badtextilien/gaestehandtuch/','https://www.kela.de/de/badtextilien/handtuch-badetuch/','https://www.kela.de/de/badtextilien/badematten/','https://www.kela.de/de/baden-duschen/duschvorhang/','https://www.kela.de/de/baden-duschen/wanneneinlagen/','https://www.kela.de/de/baden-duschen/duschkorb/','https://www.kela.de/de/baden-duschen/zubehoer/','https://www.kela.de/de/ordnung-im-bad/waeschebox/','https://www.kela.de/de/ordnung-im-bad/korb/','https://www.kela.de/de/ordnung-im-bad/kosmetiktuchbox/','https://www.kela.de/de/ordnung-im-bad/aufbewahrung-im-bad/','https://www.kela.de/de/bad/badezimmer-sets/'];
         $array3=['https://www.kela.de/de/kochen-und-braten/toepfe/','https://www.kela.de/de/kochen-und-braten/pfannen/','https://www.kela.de/de/kochen-und-braten/woks/','https://www.kela.de/de/kochen-und-braten/braeter/','https://www.kela.de/de/kochen-und-braten/auflaufformen/','https://www.kela.de/de/kochen-und-braten/wasserkessel/','https://www.kela.de/de/kochen-und-braten/deckel/','https://www.kela.de/de/kochen-und-braten/kochhilfen/','https://www.kela.de/de/kochen-mit-freunden/fondue/','https://www.kela.de/de/kochen-mit-freunden/kaesefondue/','https://www.kela.de/de/kochen-mit-freunden/schokofondue/','https://www.kela.de/de/kochen-mit-freunden/raclette-heisser-stein/','https://www.kela.de/de/kochen-mit-freunden/elektrogeraete/','https://www.kela.de/de/kochen-mit-freunden/feuerzangenbowle-zubehoer/','https://www.kela.de/de/kochen-mit-freunden/zubehoer/','https://www.kela.de/de/ordnung-in-der-kueche/ordnungssysteme/','https://www.kela.de/de/ordnung-in-der-kueche/vorratsdosen/','https://www.kela.de/de/ordnung-in-der-kueche/koerbe/','https://www.kela.de/de/ordnung-in-der-kueche/kuechenrollenhalter/','https://www.kela.de/de/ordnung-in-der-kueche/brotkasten/','https://www.kela.de/de/ordnung-in-der-kueche/abtropfkorb-matte/','https://www.kela.de/de/muelleimer/treteimer/category-1-235.html','https://www.kela.de/de/muelleimer/schwingdeckeleimer/category-1-236.html','https://www.kela.de/de/muelleimer/papierkorb-abfalleimer/category-1-237.html','https://www.kela.de/de/muelleimer/ersatzeimer/','https://www.kela.de/de/spezialzubehoer/haushalt/','https://www.kela.de/de/spezialzubehoer/fleisch-fischzubehoer/','https://www.kela.de/de/spezialzubehoer/grillzubehoer/','https://www.kela.de/de/spezialzubehoer/kaesezubehoer/','https://www.kela.de/de/spezialzubehoer/nudelzubehoer/','https://www.kela.de/de/spezialzubehoer/obst-gemuesezubehoer/'];
         $array4=['https://www.kela.de/de/kuechenhelfer/kuechenhelfer/','https://www.kela.de/de/kuechenhelfer/messer-schneiden/','https://www.kela.de/de/kuechenhelfer/reiben-hobel/','https://www.kela.de/de/kuechenhelfer/schneidebretter/','https://www.kela.de/de/kuechenhelfer/pressen-hacken-stampfen/','https://www.kela.de/de/kuechenhelfer/schuesseln-siebe/','https://www.kela.de/de/kuechenhelfer/backutensilien/','https://www.kela.de/de/kuechenhelfer/schuetzen-reinigen/','https://www.kela.de/de/kuechentextilien/geschirrtuecher/','https://www.kela.de/de/kuechentextilien/kuechenschuerzen/','https://www.kela.de/de/kuechentextilien/topflappen/','https://www.kela.de/de/kuechentextilien/topfhandschuhe/','https://www.kela.de/de/gedeckter-tisch/tischsets-laeufer/','https://www.kela.de/de/gedeckter-tisch/tischartikel/','https://www.kela.de/de/gedeckter-tisch/servieren-am-tisch/','https://www.kela.de/de/gedeckter-tisch/kaffee-tee/','https://www.kela.de/de/wohnaccessoires/sitzhocker/','https://www.kela.de/de/wohnaccessoires/schirmstaender/','https://www.kela.de/de/wohnaccessoires/rollwagen-regale/','https://www.kela.de/de/kueche/ersatzteile/','https://www.kela.de/de/kueche/kuechensets/'];

         
         $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'kela001.xms013.site'],
            'x2' => ['source' => $array2, 'target' =>'kela002.xms013.site'],
            'x3' => ['source' => $array3, 'target' =>'kela003.xms013.site'],
            'x4' => ['source' => $array4, 'target' =>'kela004.xms013.site'],
            
            
            
 
             //hxfaa
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.kela.de',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36','time.sleep(random.randint(1,3))',
            ],
        ]);
            // wordpress的密钥  time.sleep(random.randint(1,3)) html_file = requests.get(url)
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8e87e9eaf6d75a3c969473d60e14480835b8aae6',
            // 'cs_66331b14ef92978de54f0ed1efdcae1e284e8187',
            // // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri) 
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        
            $this->processProductList($crawler);
            $nextNode = $crawler->filter('[title="Eine Seite vor"]');
            //        echo $nextNode->attr("href");
                    if ($nextNode->count()) {
                        $this->processPage('https://www.kela.de'.$nextNode->attr('href'));
                    }
            // print_r($nextNode->attr("href"));
        
    }
    

    protected function processProductList(Crawler $crawler)
    {  
  
        // 进入详情页爬取数据                                                          
        $crawler->filter('.itemWrapper a')->each(function (Crawler $node, $i){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " .$node->text(),
                    'url' => sprintf('%s','https://www.kela.de'.$node->attr('href')),
                  
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.itemDetailWrapper h1')->text();  //标题（必须）
        // print_r($product['title']);exit;


        // $product['price'] =$crawler->filter('.price')->filter('.amount')->text();
        $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.price')->filter('.amount')->text())));
        // print_r($product['price']);exit;

       //品牌（必须）
        
            $product['brand']=' ';
         
          
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs ul')->filter('li a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->attr('title');
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2,-1);
        // array_push($product['breadcrumbs'],$item['breadcrumbs'][0]);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        print_r($product['breadcrumbs']);exit;
        
        
        $product['sku']=$crawler->filter('[name="itemId"]')->attr('value').$crawler->filter('[class="additionalAttribute farbe"]')->filter('.additionalAttribute-value')->text();
        // print_r( $product['sku']);exit;  //产品编号（必须）

       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];   
       if($crawler->filter('.moretext-content')->count()){
           $product['description']=$crawler->filter('.moretext-content')->html();
       }else{
            $product['description']=' ';
       }
        
        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
      
        if($crawler->filter('.additionalAttributes-table')->count()) {
            $count = $crawler->filter('.additionalAttributes-table div')->count();
            // echo $count;exit;
                for($i=0;$i<$count;$i++){
                    $product['attributes'][] = [
                        'name' =>$crawler->filter('.additionalAttribute')->eq($i)->filter('.additionalAttribute-label')->text(),
                        'options'=>$crawler->filter('.additionalAttribute')->eq($i)->filter('.additionalAttribute-value')->text(),
                    ];
                }       
            
        }else{
            $product['attributes'] = [];
        }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']='';  //性别
        // print_r($product['gender']);exit;
     if($crawler->filter('[class="additionalAttribute farbe"]')->filter('.additionalAttribute-value')->count()){
        $product['color'] =$crawler->filter('[class="additionalAttribute farbe"]')->filter('.additionalAttribute-value')->text();
     }else{
        $product['color']='';
     }
       
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        $product['type'] = 'variable';
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $images_ls=$crawler->filter('[type="text/x-magento-init"]')->eq(8)->each(function (Crawler $node, $i) {
        //     if(strstr($node->text(),"mage/gallery/gallery"))
        //         return json_decode($node->text(),true)["[data-gallery-role=gallery-placeholder]"]["mage/gallery/gallery"]["data"];
        // });
        // $images_ls=array_filter($images_ls);
        // foreach ($images_ls as $image_ls) {
        //     foreach ($image_ls as $image) {
        //         $product['images'][] = [
        //             'src' => $image['img'],
        //             'name' => $product['title'],
        //         ];
        //     }
        // }
        // print_r($product['images']);exit;
        $images=array_unique(array_filter($crawler->filter('.linkToLImage')->each(function(Crawler $node,$i){
            return  $node->attr('href');
        })));
        // // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);// 用于图片上传失败
        // print_r($product['images']);exit;
    //   商品的尺寸
    // $xx=[];
    $product['variations']=[];
//     $options_ls=$crawler->filter('[type="text/x-magento-init"]')->eq(5)->each(function (Crawler $node, $i) {
//         if(strstr($node->text(),"Magento_Swatches/js/swatch-renderer"))
//             return json_decode($node->text(),true)["[data-role=swatch-options]"]["Magento_Swatches/js/swatch-renderer"]["jsonConfig"]["attributes"][169]["options"];
//     });
//     print_r($options_ls);exit;

//     $options_ls=array_filter($options_ls);
//     foreach ($options_ls as $options_ls) {
//         foreach ($options_ls as $label) {
//             $xx[]=$label['label'];
//         }
//     }
//   print_r($xx);exit;
       
//         if($crawler->filter('[type="text/x-magento-init"]')->eq(5)->count()){
//                  $product['variations'][]= [
                    
//                 'name'=>'Size',
//                 'options'=>$xx
//                 ,
//             ];   
//             $product['type'] = 'variable';
//         }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}