<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class smyth extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:smyth')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://ozgameshop.com');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
       
         $array1=['https://www.toysrus.ca/en/toysrus/Category/Action-Figures/Military-Figures','https://www.toysrus.ca/en/toysrus/Category/Action-Figures/Movie-and-TV-Characters','https://www.toysrus.ca/en/toysrus/Category/Action-Figures/Video-Game-Characters','https://www.toysrus.ca/en/toysrus/Category/Building-Sets-and-Blocks','https://www.toysrus.ca/en/toysrus/Category/Kids-Furniture-and-Decor'];
         $array2=["https://www.toysrus.ca/en/toysrus/Category/Arts-and-Crafts/Drawing-and-Colouring",'https://www.toysrus.ca/en/toysrus/Category/Arts-and-Crafts/Beads--Jewelry-and-Fashion','https://www.toysrus.ca/en/toysrus/Category/Arts-and-Crafts/Clay-and-Dough','https://www.toysrus.ca/en/toysrus/Category/Arts-and-Crafts/Craft-Kits-and-Books','https://www.toysrus.ca/en/toysrus/Category/Arts-and-Crafts/Tween-Shop'];
         $array3=['https://www.toysrus.ca/en/toysrus/Category/Kids-Clothes','https://www.toysrus.ca/en/toysrus/Category/Interactive-Pets-and-Toys','https://www.toysrus.ca/en/toysrus/Category/Kids-Furniture-and-Decor','https://www.toysrus.ca/en/toysrus/Category/Plush-and-Stuffed-Animals'];
         
        //  分类问题，暂时不爬取
         $array4=['https://www.toysrus.ca/en/toysrus/Category/Dolls-and-Playsets/18-inch-Dolls-and-Accessories','https://www.toysrus.ca/en/toysrus/Category/Dolls-and-Playsets/Baby-Dolls-and-Accessories','https://www.toysrus.ca/en/toysrus/Category/Dolls-and-Playsets/Doll-Houses','https://www.toysrus.ca/en/toysrus/Category/Dolls-and-Playsets/Fashion-Dolls-and-Accessories','https://www.toysrus.ca/en/toysrus/Category/Dolls-and-Playsets/Mini-Dolls-and-Collectibles',];
         $array5=['https://www.toysrus.ca/en/toysrus/Category/Electronics/Batteries','https://www.toysrus.ca/en/toysrus/Category/Electronics/Headphones--Speakers-and-Audio','https://www.toysrus.ca/en/toysrus/Category/Electronics/Home-Electronics','https://www.toysrus.ca/en/toysrus/Category/Electronics/Karaoke-and-Musical-Instruments','https://www.toysrus.ca/en/toysrus/Category/Electronics/Robotics','https://www.toysrus.ca/en/toysrus/Category/Electronics/Video-Games',"https://www.toysrus.ca/en/toysrus/Category/Electronics/Video-Games#7","https://www.toysrus.ca/en/toysrus/Category/Electronics/Video-Games#11",];
         $array6=['https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Puzzles','https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Strategy-Games','https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Card-Games','https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/French-Games',];
         $array7=['https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Family-Games','https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Preschool-Games','https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Table-Games','https://www.toysrus.ca/en/toysrus/Category/Games-and-Puzzles/Party-Games',];
         $array8=['https://www.toysrus.ca/en/toysrus/Category/Outdoor/Playground/Swing-Sets-and-Accessories','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Scooters-and-Skateboards/3-Wheel-Scooters','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Scooters-and-Skateboards/Skateboards','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Helmets-and-Bike-Accessories','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Battery-Powered-Vehicles','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Sports-and-Recreation'];
         $array9=['https://www.toysrus.ca/en/toysrus/Category/Outdoor/Sports-and-Recreation','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Scooters-and-Skateboards/2-Wheel-Scooters','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Scooters-and-Skateboards/3-Wheel-Scooters','https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Scooters-and-Skateboards/Skateboards',"https://www.toysrus.ca/en/toysrus/Category/Outdoor/Wheels/Helmets-and-Bike-Accessories","https://www.toysrus.ca/en/toysrus/Category/Outdoor/Playground"];
         $array10=['https://www.toysrus.ca/en/toysrus/Category/Party/Balloons','https://www.toysrus.ca/en/toysrus/Category/Party/Decorations-and-Party-Favours','https://www.toysrus.ca/en/toysrus/Category/Party/Tableware','https://www.toysrus.ca/en/toysrus/Category/Party/Loot-Bags',];
         $array11=['https://www.toysrus.ca/en/toysrus/Category/Plush-and-Stuffed-Animals','https://www.toysrus.ca/en/toysrus/Category/Pretend-Play-and-Dress-up/Dress-up-Costumes','https://www.toysrus.ca/en/toysrus/Category/Pretend-Play-and-Dress-up/Kitchen-Sets','https://www.toysrus.ca/en/toysrus/Category/School-Supplies/Backpacks','https://www.toysrus.ca/en/toysrus/Category/School-Supplies/Lunch-and-Bento-Boxes','https://www.toysrus.ca/en/toysrus/Category/School-Supplies/School-Themed-Books','https://www.toysrus.ca/en/toysrus/Category/School-Supplies/Water-Bottles','https://www.toysrus.ca/en/toysrus/Category/School-Supplies/Workbooks-and-Flashcards','https://www.toysrus.ca/en/toysrus/Category/School-Supplies/Writing-and-Colouring'];
         $array12=['https://www.toysrus.ca/en/toysrus/Category/Trading-Cards-and-Collectibles/Trading-Cards','https://www.toysrus.ca/en/toysrus/Category/Trading-Cards-and-Collectibles/Battling-Toys','https://www.toysrus.ca/en/toysrus/Category/Trading-Cards-and-Collectibles/Novelty-Toys','https://www.toysrus.ca/en/toysrus/Category/Trading-Cards-and-Collectibles/Statues'];
         
         $array13=['https://www.toysrus.ca/en/toysrus/Category/Vehicles--Trains-and-Playsets/Construction-and-Farm','https://www.toysrus.ca/en/toysrus/Category/Vehicles--Trains-and-Playsets/Remote-Control-Vehicles','https://www.toysrus.ca/en/toysrus/Category/Vehicles--Trains-and-Playsets/Road-Race','https://www.toysrus.ca/en/toysrus/Category/Vehicles--Trains-and-Playsets/Trains','https://www.toysrus.ca/en/toysrus/Category/Toddler-and-Preschool-Toys',''];


         $array14=['https://www.dresscodeclothing.com/mens/bottoms-athletic'];

        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'smyth001.xms011.site'],
            'x2' => ['source' => $array2, 'target' =>'smyth002.xms011.site'],
            'x3' => ['source' => $array3, 'target' =>'smyth003.xms011.site'],
            'x4' => ['source' => $array4, 'target' =>'smyth004.xms011.site'],
            'x5' => ['source' => $array5, 'target' =>'smyth005.xms011.site'],
            'x6' => ['source' => $array6, 'target' =>'smyth006.xms011.site'],
            'x7' => ['source' => $array7, 'target' =>'smyth007.xms011.site'],
            'x8' => ['source' => $array8, 'target' =>'smyth008.xms011.site'],
            'x9' => ['source' => $array9, 'target' =>'smyth009.xms011.site'],
            'x10' => ['source' => $array10, 'target' =>'smyth010.xms011.site'],
            'x11' => ['source' => $array11, 'target' =>'smyth011.xms011.site'],
            'x12' => ['source' => $array12, 'target' =>'smyth012.xms011.site'],
            'x13' => ['source' => $array13, 'target' =>'smyth013.xms011.site'],
            'x14' => ['source' => $array14, 'target' =>'hxflla'],
            

            
            
 
             //hxflla
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.dresscodeclothing.com',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36','time.sleep(random.randint(1,3))',
            ],
        ]);
            // wordpress的密钥  time.sleep(random.randint(1,3)) html_file = requests.get(url)
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri) 
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        
            $this->processProductList($crawler);
            $nextNode = $crawler->filter('[class="b-button b-load_more-button js-show-more-btn js-dataLayer-plp"]');
            //        echo $nextNode->attr("href");
                    if ($nextNode->count()) {
                        $this->processPage($nextNode->attr('data-url'));
                    }
            // print_r($nextNode->attr("href"));
        
    }
    

    protected function processProductList(Crawler $crawler)
    {  

        // $breadcrumbs = array_filter($crawler->filter('.b-breadcrumbs-list ')->filter('.b-breadcrumbs-item')->filter('a span')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
            //  print_r($breadcrumbs);exit;
         
        // 进入详情页爬取数据                                                                use($breadcrumbs)
        $crawler->filter('[class="b-product_tile-title_link js-pdp-link"]')->each(function (Crawler $node, $i) {
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " .$node->text(),
                    'url' => sprintf('%s','https://www.toysrus.ca/'.$node->attr('href')),
                    // 'breadcrumbs' => $breadcrumbs,
                    // 'gender'      => $gender
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.b-product_details-name')->text();  //标题（必须）
        // print_r($product['title']);exit;


        $product['price']=$crawler->filter('[class="b-price-value js-sales-price-value"]')->attr('content');
        // print_r($product['price']);exit;

       //品牌（必须）
          $product['brand']=$crawler->filter('.b-product_details-brand')->text();
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.b-breadcrumbs-list ')->filter('.b-breadcrumbs-item')->filter('a span')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        // array_push($product['breadcrumbs'],$item['breadcrumbs'][0]);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
        $product['sku']=$crawler->filter('.b-product_details-sku strong')->text();
        // print_r( $product['sku']); exit; //产品编号（必须）
       

         $product['short_description']='';
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];   
       
        $product['description']=$crawler->filter('.b-product_description')->html();
        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
      
        // if($crawler->filter('[class="col-sm-12 col-md-8 col-lg-9 value content"]')->count()) {
        //     $count = $crawler->filter('[class="col-sm-12 col-md-8 col-lg-9 value content"] li')->count();
        //     // echo $count;exit;
        //         for($i=0;$i<$count;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>$crawler->filter('[class="col-sm-12 col-md-8 col-lg-9 value content"] li')->eq($i)->text(),
        //                 'options' =>$crawler->filter('[class="col-sm-12 col-md-8 col-lg-9 value content"] li')->eq($i)->filter('strong')->text(),
        //             ];
        //         }       
            
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']='';  //性别
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $images=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$images){
        //     if(strstr($node->text(),"image")){
        //         $images = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('.js-product-image')->each(function(Crawler $node,$i){
            return  $node->attr('src');
        })));
        // // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);// 用于图片上传失败
        // print_r($images);exit;
    //   商品的尺寸
        $product['variations']=[];
        // if($crawler->filter('.shoes-size-field')->count()){
        //          $product['variations'][]= [
                    
        //         'name'=>'Pointures',
        //         'options'=>$crawler->filter('[class="radio-field out-of-stock disabled"]')->filter('label')->each(function (Crawler $node,$i){
        //             return $node->text();
        //         }),
        //     ];   
        //     $product['type'] = 'variable';
        // }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}