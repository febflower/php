<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class freepeople extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;

    protected function configure()
    {
        $this->setName('build:wordpress:mzl:freepeople')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.freepeople.com/');
    }
    protected function initialize(Input $input, Output $output)
    {                   
        //   是要爬取的网页
       
         $array1=['https://www.freepeople.com/mini-skirts/','https://www.freepeople.com/midi-skirts/','https://www.freepeople.com/maxi-skirts/','https://www.freepeople.com/womens-skorts/','https://www.freepeople.com/overalls/?feature-product-ids=FP-78116126-000&topper=1','https://www.freepeople.com/boyfriend-jeans/?feature-product-ids=FP-69644094-000&topper=1','https://www.freepeople.com/flare-jeans/?feature-product-ids=FP-58182312-000&topper=1','https://www.freepeople.com/the-crvy-collection-denim/?feature-product-ids=FP-78014131-000&topper=1','https://www.freepeople.com/cozy-jackets/?feature-product-ids=FP-79243648-000&topper=1','https://www.freepeople.com/vests/?feature-product-ids=FP-79244646-000&topper=1','https://www.freepeople.com/puffer-jackets/?feature-product-ids=FP-79096376-000&topper=1','https://www.freepeople.com/denim-jackets/?feature-product-ids=FP-68349745-000&topper=1','https://www.freepeople.com/thermals-henleys/?feature-product-ids=FP-59642660-000&topper=1','https://www.freepeople.com/graphic-tees/?feature-product-ids=FP-80703085-000&topper=1','https://www.freepeople.com/tees/?feature-product-ids=FP-80694771-000&topper=1','https://www.freepeople.com/blouses/?feature-product-ids=FP-80363161-000&topper=1'];
         $array2=['https://www.freepeople.com/jumpsuits-dresses/','https://www.freepeople.com/lounge-tops/','https://www.freepeople.com/lounge-pants/','https://www.freepeople.com/lounge-dresses/','https://www.freepeople.com/wide-leg-pants/','https://www.freepeople.com/structured-pants/','https://www.freepeople.com/utility-bottoms/','https://www.freepeople.com/harem-pants/','https://www.freepeople.com/rompers/'];
         $array3=['https://www.freepeople.com/sets-dresses/','https://www.freepeople.com/shorts/','https://www.freepeople.com/suits/','https://www.freepeople.com/sweaters/','https://www.freepeople.com/all-swimwear/'];
         $array4=['https://www.freepeople.com/ankle-boots/','https://www.freepeople.com/knee-high-boots/','https://www.freepeople.com/heeled-boots/','https://www.freepeople.com/stompy-boots-trend/','https://www.freepeople.com/weather-boots/','https://www.freepeople.com/western-boots/','https://www.freepeople.com/moccasins/','https://www.freepeople.com/clogs-mules/','https://www.freepeople.com/flats/','https://www.freepeople.com/slippers/'];
         $array5=['https://www.freepeople.com/heels-wedges/','https://www.freepeople.com/menswear-loafers/','https://www.freepeople.com/sandals/','https://www.freepeople.com/low-top-sneakers/','https://www.freepeople.com/high-top-sneakers/','https://www.freepeople.com/slip-on-sneakers/','https://www.freepeople.com/athletic-sneakers/'];
         
         
         $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'freepeople001.xms013.site'],
            'x2' => ['source' => $array2, 'target' =>'freepeople002.xms013.site'],
            'x3' => ['source' => $array3, 'target' =>'freepeople003.xms013.site'],
            'x4' => ['source' => $array4, 'target' =>'freepeople004.xms013.site'],
            'x5' => ['source' => $array5, 'target' =>'freepeople005.xms013.site'],

      
            
            
            
 
             //hxfaa
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            // 'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.freepeople.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36','time.sleep(random.randint(1,3))',
            ],
        ]);
            // wordpress的密钥  time.sleep(random.randint(1,3)) html_file = requests.get(url)
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8e87e9eaf6d75a3c969473d60e14480835b8aae6',
            // 'cs_66331b14ef92978de54f0ed1efdcae1e284e8187',
            // // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

    // 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri) 
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
        // print_r($contents);exit;
        
            $this->processProductList($crawler);
            $nextNode = $crawler->filter('[data-focus-target="tile-grid-top-next-button"]');
            //        echo $nextNode->attr("href");
                    if ($nextNode->count()) {
                        $this->processPage('https://www.freepeople.com'.$nextNode->attr('href'));
                    }
            // print_r($nextNode->attr("href"));
        
    }
    

    protected function processProductList(Crawler $crawler)
    {  
  
        // 进入详情页爬取数据                                                          
        $crawler->filter('.o-pwa-product-tile__link')->each(function (Crawler $node, $i){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " .$node->text(),
                    'url' => sprintf('%s','https://www.freepeople.com/'.$node->attr('href')),
                  
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        $product['title'] =$crawler->filter('.c-pwa-product-meta-heading')->text();  //标题（必须）
        // print_r($product['title']);exit;


        // $product['price'] =$crawler->filter('.price')->filter('.amount')->text();
        $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.c-pwa-product-price__current')->text())));
        // print_r($product['price']);exit;

       //品牌（必须）
        
            $product['brand']=' ';
         
          
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        $breadcrumbs = array_filter($crawler->filter('.o-pwa-breadcrumbs__list')->filter('li a')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        // print_r($breadcrumbs);exit;
        $product['breadcrumbs'] = array_slice($breadcrumbs,0);
        // array_push($product['breadcrumbs'],$item['breadcrumbs'][0]);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
        
        $product['sku']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(2)->text(),true)['offers']['offers'][0]['sku'];
        // print_r( $product['sku']);exit;  //产品编号（必须）

       
     if($crawler->filter('[class="s-pwa-cms c-pwa-markdown"]')->eq(1)->count()){
        $product['short_description']=$crawler->filter('[class="s-pwa-cms c-pwa-markdown"]')->eq(1)->html();
     }else{
        $product['short_description']='';
     }
        
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        // $descr=['<h6>ΠΕΡΙΓΡΑΦΗ</h6>'];   
        
         $product['description']=$crawler->filter('[class="s-pwa-cms c-pwa-markdown"]')->html();
     
        
        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
      
        // if($crawler->filter('.additionalAttributes-table')->count()) {
        //     $count = $crawler->filter('.additionalAttributes-table div')->count();
        //     // echo $count;exit;
        //         for($i=0;$i<$count;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>$crawler->filter('.additionalAttribute')->eq($i)->filter('.additionalAttribute-label')->text(),
        //                 'options'=>$crawler->filter('.additionalAttribute')->eq($i)->filter('.additionalAttribute-value')->text(),
        //             ];
        //         }       
            
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        $product['gender']='';  //性别
        // print_r($product['gender']);exit;
     if($crawler->filter('[class="c-pwa-legend"]')->filter('[class="c-pwa-sku-selection__color-value"]')->count()){
        $product['color'] =$crawler->filter('[class="c-pwa-legend"]')->filter('[class="c-pwa-sku-selection__color-value"]')->text();
     }else{
        $product['color']='';
     }
       
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        $product['type'] = 'variable';
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $images_ls=$crawler->filter('[type="text/x-magento-init"]')->eq(8)->each(function (Crawler $node, $i) {
        //     if(strstr($node->text(),"mage/gallery/gallery"))
        //         return json_decode($node->text(),true)["[data-gallery-role=gallery-placeholder]"]["mage/gallery/gallery"]["data"];
        // });
        // $images_ls=array_filter($images_ls);
        // foreach ($images_ls as $image_ls) {
        //     foreach ($image_ls as $image) {
        //         $product['images'][] = [
        //             'src' => $image['img'],
        //             'name' => $product['title'],
        //         ];
        //     }
        // }
        // print_r($product['images']);exit;
        $images=array_unique(array_filter($crawler->filter('[class="o-pwa-image__img c-pwa-image-viewer__img js-pwa-faceout-image"]')->each(function(Crawler $node,$i){
            return str_replace('=100','=600',$node->attr('src'));
        })));
        // // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);// 用于图片上传失败
        // print_r($product['images']);exit;
    //   商品的尺寸
    // $xx=[];
    $product['variations']=[];
        if($crawler->filter('[class="u-pwa-form-field c-pwa-enhanced-input c-pwa-sku-selection__size u-pwa-form-field-wrapped"]')->count()){
                 $product['variations'][]= [
                    
                'name'=>'Size',
                'options'=>$crawler->filter('.c-pwa-radio-boxes__list--default')->filter('.c-pwa-radio-boxes__item--default')->filter('label')->each(function (Crawler $node,$i){
                    return $node->text();
                }),
            ];   
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}