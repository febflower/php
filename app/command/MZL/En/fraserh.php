<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class fraserh extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:fraserh')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://fraserhillfarm.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //万圣节  

        $array=['https://fraserhillfarm.com/halloween/shop-by-theme/halloween-sider-decorations','https://fraserhillfarm.com/halloween/shop-by-theme/clowns/','https://fraserhillfarm.com/halloween/shop-by-theme/cornfield/','https://fraserhillfarm.com/halloween/shop-by-theme/creepy-kids/','https://fraserhillfarm.com/halloween/shop-by-theme/day-of-the-dead/','https://fraserhillfarm.com/halloween/shop-by-theme/halloween-ghost-decorations/','https://fraserhillfarm.com/halloween/shop-by-theme/graveyard/','https://fraserhillfarm.com/halloween/shop-by-theme/kid-friendly/','https://fraserhillfarm.com/halloween/shop-by-theme/occupational-horror/','https://fraserhillfarm.com/halloween/shop-by-theme/halloween-pumpkin-decorations/','https://fraserhillfarm.com/halloween/shop-by-theme/skulls-and-skeletons/','https://fraserhillfarm.com/halloween/shop-by-theme/til-death-do-us-part/','https://fraserhillfarm.com/halloween/shop-by-theme/halloween-witches/','https://fraserhillfarm.com/halloween/shop-by-theme/zombies/','https://fraserhillfarm.com/halloween/animatronics/life-size/','https://fraserhillfarm.com/halloween/animatronics/new-category-2/','https://fraserhillfarm.com/halloween/animatronics/groundbreakers/','https://fraserhillfarm.com/halloween/animatronics/crawlers/','https://fraserhillfarm.com/halloween/animatronics/pop-up-creatures/','https://fraserhillfarm.com/halloween/animatronics/halloween-animatronic-heads/','https://fraserhillfarm.com/halloween/animatronics/tabletop/','https://fraserhillfarm.com/halloween/animatronics/walking-animatronics/','https://fraserhillfarm.com/halloween/home-decor/indoor-decor/','https://fraserhillfarm.com/halloween/home-decor/pillows/','https://fraserhillfarm.com/halloween/home-decor/wreaths-garland-bouquets/','https://fraserhillfarm.com/halloween/home-decor/new-category-1/','https://fraserhillfarm.com/halloween/outdoor-decor/inflatables/','https://fraserhillfarm.com/halloween/outdoor-decor/led-light-shapes/'];
        //圣诞节 
        $array1=['https://fraserhillfarm.com/christmas/christmas-trees/best-sellers/','https://fraserhillfarm.com/christmas/christmas-trees/green-trees/','https://fraserhillfarm.com/christmas/artificial-christmas-trees/flocked-christmas-trees/','https://fraserhillfarm.com/christmas/christmas-trees/under-6-feet/','https://fraserhillfarm.com/christmas/christmas-trees/9-12-feet/','https://fraserhillfarm.com/christmas/christmas-trees/unlit/','https://fraserhillfarm.com/christmas/christmas-trees/pre-lit/','https://fraserhillfarm.com/christmas/artificial-christmas-trees/potted-christmas-trees','https://fraserhillfarm.com/christmas/christmas-trees/fiber-optic/','https://fraserhillfarm.com/christmas/artificial-christmas-trees/unique-christmas-trees/','https://fraserhillfarm.com/christmas/christmas-trees/storage-bags/','https://fraserhillfarm.com/christmas/wreaths-and-garland/shop-all-garland/','https://fraserhillfarm.com/christmas/wreaths-and-garland/shop-all-wreaths/','https://fraserhillfarm.com/christmas/wreaths-and-garland/unlit/','https://fraserhillfarm.com/christmas/wreaths-and-garland/pre-lit/','https://fraserhillfarm.com/christmas/wreaths-and-garland/decorated/','https://fraserhillfarm.com/christmas/wreaths-and-garland/storage-bags/','https://fraserhillfarm.com/christmas/christmas-trees/shop-all-christmas-trees/','https://fraserhillfarm.com/christmas/home-decor/pillows/','https://fraserhillfarm.com/christmas/home-decor/candle-holders/','https://fraserhillfarm.com/christmas/shop-all-christmas/african-american/','https://fraserhillfarm.com/christmas/home-decor/animated-figurines/','https://fraserhillfarm.com/christmas/home-decor/resin-figurines/','https://fraserhillfarm.com/christmas/home-decor/musical-figurines/','https://fraserhillfarm.com/christmas/home-decor/life-size-figurines/','https://fraserhillfarm.com/christmas/shop-all-christmas/nutcrackers/','https://fraserhillfarm.com/christmas/home-decor/decorating-sets/','https://fraserhillfarm.com/christmas/home-decor/new-category-5/','https://fraserhillfarm.com/christmas/outdoor-decor/inflatables/','https://fraserhillfarm.com/christmas/outdoor-decor/nutcrackers/',];


        $this->sites = [
            'x' => ['source' => $array, 'target' =>'fraserh002.xms005.site'],
            'x1'=> ['source' => $array1, 'target' =>'fraserh003.xms005.site'],
            // 'x2'=> ['source' => $array1, 'target' => 'hxflla'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.gearwest.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥 
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
               //oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('.pagination-item--next a');
        //    echo $nextNode->attr("href");exit;
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }

            // print_r($nextNode->attr("href"));exit;

//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.card-image-link--slider')->each(function (Crawler $node, $i)  {
            
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
                
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            // print_r($node->attr('href'));exit;
        });
 
    }

// 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.productView-title')->text();  //标题（必须）
        // print_r($product['title']);exit;

        

        $product['price'] = $crawler->filter('.productView')->attr('data-product-price');
        //价格（必须）
        // $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['brand']['name'];
     
        // json_decode($crawler->filter('[type="application/ld+json"]')->last()->text(),true)["@graph"][1]['offers'][0]['price'];
        // print_r($product['price']);exit;

       //品牌（必须）

       $product['brand']=$crawler->filter('[property="og:site_name"]')->attr('content');
    //    $brand = explode('Brand: "',$crawler->filter('[id="viewed_product"]')->text());
    //    $brands = explode(',',$brand[1]);
    //    $product['brand']=str_replace('"','',$brands[0]);

        // print_r($product['brand']);exit;

        // $product['sku'] = $crawler->filter('.productView-info-value')->text();  //产品编号（必须）
        $product['sku'] = $crawler->filter('.productView')->attr('data-entity-id');  //产品编号（必须）
        // $sku=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)["productID"];



        // print_r($product['sku']);exit;

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        
        //分类类名
       
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li a')->each(function (Crawler $node) {
            return $node->text();
         }));
        
        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        // print_r($product['breadcrumbs']);exit();
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
    //    print_r($product['breadcrumbs']);exit;

      
        // print_r($product['sku']);exit;
        
        $product['short_description']=''; //简短描述

        // print_r($product['short_description']);exit;
       
        $product['description']=$crawler->filter('.accordion-detail')->html();//描述
       

        // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
       
        // print_r($product['description']);exit;
        $product['attributes'] = [];
        //属性
        // if($crawler->filter(".accordion-detail ul")->count()) {
        //     $count = $crawler->filter(".accordion-detail ul")->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name' => '',
        //             'options' =>$crawler->filter(".accordion-detail")->eq($i)->filter('ul')->text(),
        //         ];
        //     }
        // }
        // print_r(  $product['attributes']);exit;

        // $product['attributes']=$crawler->filter('.product-description')->html();
        //   print_r($product['attributes']);exit; 
        //标签
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));

       
        // print_r($product['tags']);exit;

      // 其他
        $product['keywords'] = [];
        $product['gender'] = '';//性别
        // print_r($product['gender']);exit;
        $product['color'] = '';
        $product['variations']=[];
        $product['subCategory']=' ';
       
        $product['images'] =[];
        //图片（必须）
    
        $images=$crawler->filter('.productView-img-container a')->each(function(Crawler $node,$i){
            return str_replace('1024x1024','600x600',$node->attr('href'));
        });
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($product['images']);exit;
    
    //   商品的尺寸或者颜色
    // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
      
     $product['variations']=[];
    //  删除多余的字节，把他们设为空
     $del=[':','options','Required'];
        
    if($crawler->filter('[data-product-attribute="set-select"]')->eq(0)->count()){$product['variations'][]= [
        'name'=>str_replace($del,'',$crawler->filter('.form-label--inlineSmall')->text()),
        'options' =>$crawler->filter('[data-product-attribute="set-select"]')->eq(0)->filter('.form-select option')->eq(0)->nextAll()->each(function (Crawler $node, $i) {
                return $node->text();
            })];  
            $product['type'] = 'variable';  
    }
    if($crawler->filter('[data-product-attribute="set-select"]')->eq(1)->count()){$product['variations'][]= [
            'name'=>str_replace($del,'',$crawler->filter('.form-label--inlineSmall')->eq(1)->text()),
            'options' =>$crawler->filter('[data-product-attribute="set-select"]')->eq(1)->filter('.form-select option')->eq(0)->nextAll()->each(function (Crawler $node, $i) {
                return $node->text();})]; 
                $product['type'] = 'variable';
    }
// print_r($product['variations']);exit;

            

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}