<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class gearwest extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:gearwest')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.gearwest.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
            // 越野滑雪
            "https://www.gearwest.com/collections/cross-country-skiing?page=1","https://www.gearwest.com/collections/cross-country-skiing?page=2","https://www.gearwest.com/collections/cross-country-skiing?page=3","https://www.gearwest.com/collections/cross-country-skiing?page=4","https://www.gearwest.com/collections/cross-country-skiing?page=5","https://www.gearwest.com/collections/cross-country-skiing?page=6","https://www.gearwest.com/collections/cross-country-skiing?page=7","https://www.gearwest.com/collections/cross-country-skiing?page=8","https://www.gearwest.com/collections/cross-country-skiing?page=9","https://www.gearwest.com/collections/cross-country-skiing?page=10","https://www.gearwest.com/collections/cross-country-skiing?page=11","https://www.gearwest.com/collections/cross-country-skiing?page=12","https://www.gearwest.com/collections/cross-country-skiing?page=13","https://www.gearwest.com/collections/cross-country-skiing?page=14","https://www.gearwest.com/collections/cross-country-skiing?page=15","https://www.gearwest.com/collections/cross-country-skiing?page=16","https://www.gearwest.com/collections/cross-country-skiing?page=17","https://www.gearwest.com/collections/cross-country-skiing?page=18","https://www.gearwest.com/collections/cross-country-skiing?page=19","https://www.gearwest.com/collections/cross-country-skiing?page=20","https://www.gearwest.com/collections/cross-country-skiing?page=21","https://www.gearwest.com/collections/cross-country-skiing?page=22","https://www.gearwest.com/collections/cross-country-skiing?page=23","https://www.gearwest.com/collections/cross-country-skiing?page=24","https://www.gearwest.com/collections/cross-country-skiing?page=25","https://www.gearwest.com/collections/cross-country-skiing?page=26","https://www.gearwest.com/collections/cross-country-skiing?page=27","https://www.gearwest.com/collections/cross-country-skiing?page=28","https://www.gearwest.com/collections/cross-country-skiing?page=29","https://www.gearwest.com/collections/cross-country-skiing?page=30","https://www.gearwest.com/collections/cross-country-skiing?page=31","https://www.gearwest.com/collections/cross-country-skiing?page=32","https://www.gearwest.com/collections/cross-country-skiing?page=33","https://www.gearwest.com/collections/cross-country-skiing?page=34","https://www.gearwest.com/collections/cross-country-skiing?page=35","https://www.gearwest.com/collections/cross-country-skiing?page=36","https://www.gearwest.com/collections/cross-country-skiing?page=37","https://www.gearwest.com/collections/cross-country-skiing?page=38","https://www.gearwest.com/collections/cross-country-skiing?page=39","https://www.gearwest.com/collections/cross-country-skiing?page=40","https://www.gearwest.com/collections/cross-country-skiing?page=41","https://www.gearwest.com/collections/cross-country-skiing?page=42","https://www.gearwest.com/collections/cross-country-skiing?page=43","https://www.gearwest.com/collections/cross-country-skiing?page=44","https://www.gearwest.com/collections/cross-country-skiing?page=45","https://www.gearwest.com/collections/cross-country-skiing?page=46","https://www.gearwest.com/collections/cross-country-skiing?page=47","https://www.gearwest.com/collections/cross-country-skiing?page=48","https://www.gearwest.com/collections/cross-country-skiing?page=49","https://www.gearwest.com/collections/cross-country-skiing?page=50","https://www.gearwest.com/collections/cross-country-skiing?page=51","https://www.gearwest.com/collections/cross-country-skiing?page=52",
            

           ];
        $array1=[
            // 骑自行车
            "https://www.gearwest.com/collections/cycling?page=1","https://www.gearwest.com/collections/cycling?page=2","https://www.gearwest.com/collections/cycling?page=3","https://www.gearwest.com/collections/cycling?page=4","https://www.gearwest.com/collections/cycling?page=5","https://www.gearwest.com/collections/cycling?page=6","https://www.gearwest.com/collections/cycling?page=7","https://www.gearwest.com/collections/cycling?page=8","https://www.gearwest.com/collections/cycling?page=9","https://www.gearwest.com/collections/cycling?page=10","https://www.gearwest.com/collections/cycling?page=11","https://www.gearwest.com/collections/cycling?page=12","https://www.gearwest.com/collections/cycling?page=13","https://www.gearwest.com/collections/cycling?page=14","https://www.gearwest.com/collections/cycling?page=15","https://www.gearwest.com/collections/cycling?page=16","https://www.gearwest.com/collections/cycling?page=17","https://www.gearwest.com/collections/cycling?page=18","https://www.gearwest.com/collections/cycling?page=19","https://www.gearwest.com/collections/cycling?page=20","https://www.gearwest.com/collections/cycling?page=21","https://www.gearwest.com/collections/cycling?page=22","https://www.gearwest.com/collections/cycling?page=23","https://www.gearwest.com/collections/cycling?page=24","https://www.gearwest.com/collections/cycling?page=25","https://www.gearwest.com/collections/cycling?page=26","https://www.gearwest.com/collections/cycling?page=27","https://www.gearwest.com/collections/cycling?page=28","https://www.gearwest.com/collections/cycling?page=29","https://www.gearwest.com/collections/cycling?page=30","https://www.gearwest.com/collections/cycling?page=31","https://www.gearwest.com/collections/cycling?page=32","https://www.gearwest.com/collections/cycling?page=33","https://www.gearwest.com/collections/cycling?page=34","https://www.gearwest.com/collections/cycling?page=35","https://www.gearwest.com/collections/cycling?page=36","https://www.gearwest.com/collections/cycling?page=37","https://www.gearwest.com/collections/cycling?page=38","https://www.gearwest.com/collections/cycling?page=39",
            
        ];
            
        $array2=[
                // 跑步
                "https://www.gearwest.com/collections/running?page=1","https://www.gearwest.com/collections/running?page=2","https://www.gearwest.com/collections/running?page=3","https://www.gearwest.com/collections/running?page=4","https://www.gearwest.com/collections/running?page=5","https://www.gearwest.com/collections/running?page=6","https://www.gearwest.com/collections/running?page=7","https://www.gearwest.com/collections/running?page=8","https://www.gearwest.com/collections/running?page=9","https://www.gearwest.com/collections/running?page=10","https://www.gearwest.com/collections/running?page=11","https://www.gearwest.com/collections/running?page=12","https://www.gearwest.com/collections/running?page=13","https://www.gearwest.com/collections/running?page=14","https://www.gearwest.com/collections/running?page=15","https://www.gearwest.com/collections/running?page=16","https://www.gearwest.com/collections/running?page=17","https://www.gearwest.com/collections/running?page=18","https://www.gearwest.com/collections/running?page=19","https://www.gearwest.com/collections/running?page=20",
               
                ];
        $array3=[
             // boards
           "https://www.gearwest.com/collections/apparel-1?page=1","https://www.gearwest.com/collections/apparel-1?page=2","https://www.gearwest.com/collections/apparel-1?page=3","https://www.gearwest.com/collections/apparel-1?page=4","https://www.gearwest.com/collections/apparel-1?page=5","https://www.gearwest.com/collections/apparel-1?page=6","https://www.gearwest.com/collections/apparel-1?page=7","https://www.gearwest.com/collections/apparel-1?page=8","https://www.gearwest.com/collections/apparel-1?page=9","https://www.gearwest.com/collections/apparel-1?page=10","https://www.gearwest.com/collections/apparel-1?page=11","https://www.gearwest.com/collections/apparel-1?page=12","https://www.gearwest.com/collections/apparel-1?page=13","https://www.gearwest.com/collections/apparel-1?page=14","https://www.gearwest.com/collections/apparel-1?page=15","https://www.gearwest.com/collections/apparel-1?page=16","https://www.gearwest.com/collections/apparel-1?page=17","https://www.gearwest.com/collections/apparel-1?page=18","https://www.gearwest.com/collections/apparel-1?page=19","https://www.gearwest.com/collections/apparel-1?page=20","https://www.gearwest.com/collections/apparel-1?page=21","https://www.gearwest.com/collections/apparel-1?page=22","https://www.gearwest.com/collections/apparel-1?page=23","https://www.gearwest.com/collections/apparel-1?page=24","https://www.gearwest.com/collections/apparel-1?page=25","https://www.gearwest.com/collections/apparel-1?page=26","https://www.gearwest.com/collections/apparel-1?page=27","https://www.gearwest.com/collections/apparel-1?page=28","https://www.gearwest.com/collections/apparel-1?page=29","https://www.gearwest.com/collections/apparel-1?page=30","https://www.gearwest.com/collections/apparel-1?page=31","https://www.gearwest.com/collections/apparel-1?page=32","https://www.gearwest.com/collections/apparel-1?page=33","https://www.gearwest.com/collections/apparel-1?page=34","https://www.gearwest.com/collections/apparel-1?page=35","https://www.gearwest.com/collections/apparel-1?page=36","https://www.gearwest.com/collections/apparel-1?page=37","https://www.gearwest.com/collections/apparel-1?page=38","https://www.gearwest.com/collections/apparel-1?page=39","https://www.gearwest.com/collections/apparel-1?page=40","https://www.gearwest.com/collections/apparel-1?page=41","https://www.gearwest.com/collections/apparel-1?page=42","https://www.gearwest.com/collections/apparel-1?page=43","https://www.gearwest.com/collections/apparel-1?page=44","https://www.gearwest.com/collections/apparel-1?page=45","https://www.gearwest.com/collections/apparel-1?page=46","https://www.gearwest.com/collections/apparel-1?page=47","https://www.gearwest.com/collections/apparel-1?page=48","https://www.gearwest.com/collections/apparel-1?page=49","https://www.gearwest.com/collections/apparel-1?page=50","https://www.gearwest.com/collections/apparel-1?page=51","https://www.gearwest.com/collections/apparel-1?page=52","https://www.gearwest.com/collections/apparel-1?page=53","https://www.gearwest.com/collections/apparel-1?page=54","https://www.gearwest.com/collections/apparel-1?page=55","https://www.gearwest.com/collections/apparel-1?page=56","https://www.gearwest.com/collections/apparel-1?page=57","https://www.gearwest.com/collections/apparel-1?page=58","https://www.gearwest.com/collections/apparel-1?page=59","https://www.gearwest.com/collections/apparel-1?page=60","https://www.gearwest.com/collections/apparel-1?page=61","https://www.gearwest.com/collections/apparel-1?page=62","https://www.gearwest.com/collections/apparel-1?page=63"
               
               
        ];
          // 服饰 "


        $this->sites = [
            'x' => ['source' => $array, 'target' => 'gearwest001.xms005.site'],
            'x1'=> ['source' => $array1, 'target' => 'gearwest002.xms005.site'],
            'x2'=> ['source' => $array2, 'target' => 'gearwest003.xms005.site'],
            'x3'=> ['source' => $array3, 'target' => 'hxflla'],
            // 'x2'=> ['source' => $array1, 'target' => 'hxflla'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.gearwest.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            // 'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            // 'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
        //    echo $nextNode->attr("aria-label");exit;
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));exit;

//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.small--one-half>a')->each(function (Crawler $node, $i)  {
            
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
                
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            // print_r($node->attr('href'));exit;
        });
 
    }

// 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.product-single__title')->text();  //标题（必须）
        // print_r($product['title']);exit;

        $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('[class="product-single__price"]')->attr('content'))));  //价格（必须）

        // print_r($product['price']);exit;
       
       //品牌（必须）
       $brand = explode('Brand: "',$crawler->filter('[id="viewed_product"]')->text());
       $brands = explode(',',$brand[1]);
       $product['brand']=str_replace('"','',$brands[0]);

        // print_r($product['brand']);exit;

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        
        //分类类名
        $breadcrumbs = array_filter($crawler->filter('#ProductSection-product-template p a')->each(function (Crawler $node) {
            return $node->text();
         }));

        $product['breadcrumbs'] = array_slice($breadcrumbs,2);
        // print_r($product['breadcrumbs']);exit();
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;

        $product['sku'] = $crawler->filter('.product-single__meta p')->text();  //产品编号（必须）
        // print_r($product['sku']);exit;
        
        $product['short_description']=''; //简短描述

        // print_r($product['short_description']);exit;
       
        $product['description']=$crawler->filter('.product-single__description ')->html();//描述

        // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
       
        // print_r($product['description']);exit;
       
        
        //属性
        // if($crawler->filter(".attributes-list>li")->count()) {
        //     $count = $crawler->filter(".attributes-list>li")->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name' => $crawler->filter(".attributes-list>li")->eq($i)->text(),
        //             'options' =>'',
        //         ];
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }
        $product['attributes'] = [];
        // $product['attributes']=$crawler->filter('.product-description')->html();
        //   print_r($product['attributes']);exit; 
        //标签
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));
       
        // print_r($product['tags']);exit;

      // 其他
        $product['keywords'] = [];
        $product['gender'] = '';//性别
        // print_r($product['gender']);exit;
        $product['color'] = '';
        $product['variations']=[];
        $product['subCategory']=' ';
       
        $product['images'] =[];
        //图片（必须）
     
        $images=$crawler->filter('.product-single__photo a')->each(function(Crawler $node,$i){
            return 'https:'.str_replace('1024x1024','600x600',$node->attr('href'));
        });
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败

    
    //    print_r($product['images']);exit;
    //   商品的尺寸或者颜色
        // $product['variations']=[];
        
        if($crawler->filter('#SingleOptionSelector-0')->count()){$product['variations'][]= [
            'name'=>$crawler->filter('#SingleOptionSelector-0')->attr('data-name'),
            'options' =>$crawler->filter('#SingleOptionSelector-0 option')->each(function (Crawler $node, $i) {
                return $node->attr('value');})];  
                $product['type'] = 'variable';
        }

        if($crawler->filter('[data-name="Color"]')->count()){$product['variations'][]= [
                'name'=>$crawler->filter('[data-name="Color"]')->attr('data-name'),
                'options' =>$crawler->filter('[data-name="Color"] option')->each(function (Crawler $node, $i) {
                    return $node->attr('value');})]; 
                    $product['type'] = 'variable';
        }
        


        // print_r($product['variations']);

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}