<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class silkfred extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:silkfred')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.silkfred.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //   是要爬取的网页
        //1-2分类为2
       $array1=['https://www.silkfred.com/collection/long-sleeve-midi-dresses','https://www.silkfred.com/collection/floral-midi-dresses','https://www.silkfred.com/collection/midaxi-dresses','https://www.silkfred.com/collection/short-sleeve-midi-dresses'];
       $array2=['https://www.silkfred.com/collection/midaxi-dresses','https://www.silkfred.com/collection/navy-midi-dresses','https://www.silkfred.com/collection/white-midi-dresses','https://www.silkfred.com/collection/midi-shirt-dresses'];
       //    切链接，然后把分类改为3
       $array3=['https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Bodycon+Dresses','https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Day+Dresses','https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Lace+Dresses','https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Long+Sleeve+Dresses','https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Off+the+Shoulder+Dresses','https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Shift+Dresses','https://www.silkfred.com/collection/party-midi-dresses?filter=1&styles%5B%5D=Skater+Dresses'];
       $array4=['https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Bodycon+Dresses','https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Wrap+Dresses','https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Shift+Dresses','https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Lace+Dresses','https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Long+Sleeve+Dresses','https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Skater+Dresses','https://www.silkfred.com/collection/occasion-midi-dresses?filter=1&styles%5B%5D=Day+Dresses'];
       $array5=['https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Off+the+Shoulder+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Bodycon+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Day+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Sequin+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Wrap+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Shirt+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Sequin+Dresses','https://www.silkfred.com/collection/midi-wedding-guest-dresses?filter=1&styles%5B%5D=Lace+Dresses'];
       $array6=['https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Bodycon+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Day+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Lace+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Off+the+Shoulder+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Long+Sleeve+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Wrap+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Shirt+Dresses','https://www.silkfred.com/collection/black-midi-dresses?filter=1&styles%5B%5D=Skater+Dresses'];
       $array7=['https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Bodycon+Dresses','https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Day+Dresses','https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Lace+Dresses','https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Off+the+Shoulder+Dresses','https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Wrap+Dresses','https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Shirt+Dresses','https://www.silkfred.com/womens/clothing/dresses/maxi-dresses?filter=1&styles%5B%5D=Long+Sleeve+Dresses'];
       $array8=['https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=Blouses','https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=Cami+Tops','https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=Long+Sleeve+Tops','https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=Oversized+Tops','https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=Shirts','https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=T-Shirts','https://www.silkfred.com/collection/going-out-tops?filter=1&styles%5B%5D=Tunics'];
       //不用切  //1-2分类为2 改类别在进入详情页时增加类别
       $array9=['https://www.silkfred.com/collection/long-sleeve-blouses','https://www.silkfred.com/collection/white-blouses','https://www.silkfred.com/collection/black-blouses','https://www.silkfred.com/collection/satin-blouses'];
       $array10=['https://www.silkfred.com/collection/high-neck-blouses','https://www.silkfred.com/collection/pink-blouses','https://www.silkfred.com/collection/leopard-print-blouses','https://www.silkfred.com/collection/green-blouses','https://www.silkfred.com/collection/party-tops'];
       $array11=['https://www.silkfred.com/collection/blouses-for-work','https://www.silkfred.com/collection/satin-blouses','https://www.silkfred.com/collection/grey-blouses','https://www.silkfred.com/collection/short-sleeve-blouses'];  
       $array12=['https://www.silkfred.com/collection/black-cami-tops','https://www.silkfred.com/collection/white-cami-tops','https://www.silkfred.com/collection/tank-tops','https://www.silkfred.com/collection/white-cami-tops','https://www.silkfred.com/collection/white-vest-tops','https://www.silkfred.com/collection/sleeveless-tops','https://www.silkfred.com/collection/crop-tops','https://www.silkfred.com/collection/bralettes','https://www.silkfred.com/collection/white-crop-tops','https://www.silkfred.com/collection/black-crop-tops','https://www.silkfred.com/collection/party-tops','https://www.silkfred.com/collection/bardot-tops','https://www.silkfred.com/collection/cold-shoulder-tops','https://www.silkfred.com/collection/one-shoulder-tops','https://www.silkfred.com/collection/long-sleeve-off-the-shoulder-tops','https://www.silkfred.com/collection/strapless-tops'];
       $array13=['https://www.silkfred.com/collection/black-long-sleeve-tops','https://www.silkfred.com/collection/long-sleeve-going-out-tops','https://www.silkfred.com/collection/long-sleeve-shirts','https://www.silkfred.com/collection/long-sleeve-leopard-print-tops','https://www.silkfred.com/collection/long-sleeve-tshirts',''];
       $array14=['https://www.silkfred.com/collection/oversized-tshirts','https://www.silkfred.com/collection/oversized-hoodies','https://www.silkfred.com/collection/oversized-shirts','https://www.silkfred.com/collection/oversized-dresses','https://www.silkfred.com/collection/oversized-tshirt-dresses','https://www.silkfred.com/collection/oversized-cardigans'];
       $array15=['https://www.silkfred.com/collection/long-sleeve-shirts','https://www.silkfred.com/collection/black-shirts','https://www.silkfred.com/collection/oversized-shirts','https://www.silkfred.com/womens/clothing/dresses/shirt-dresses','https://www.silkfred.com/collection/white-shirts','https://www.silkfred.com/collection/printed-shirts'];
       //分类改为3 改类别在进入详情页时增加类别
       $array16=['https://www.silkfred.com/collection/white-tshirts','https://www.silkfred.com/collection/black-tshirts','https://www.silkfred.com/collection/oversized-tshirts','https://www.silkfred.com/collection/slogan-tshirts','https://www.silkfred.com/collection/cropped-tshirts'];
       $array17=['https://www.silkfred.com/collection/pink-tshirts','https://www.silkfred.com/collection/grey-tshirts','https://www.silkfred.com/collection/v-neck-tshirts','https://www.silkfred.com/collection/long-sleeve-tshirts','https://www.silkfred.com/collection/striped-tshirts'];
       $array18=['https://www.silkfred.com/womens/clothing/jumpsuits-and-playsuits/culotte-jumpsuits','https://www.silkfred.com/womens/clothing/jumpsuits-and-playsuits/jumpsuits','https://www.silkfred.com/womens/clothing/jumpsuits-and-playsuits/playsuits'];
       $array19=['https://www.silkfred.com/womens/clothing/skirts/denim-skirts','https://www.silkfred.com/womens/clothing/skirts/leather-skirts','https://www.silkfred.com/womens/clothing/skirts/leather-skirts','https://www.silkfred.com/womens/clothing/skirts/maxi-skirts','https://www.silkfred.com/womens/clothing/skirts/midi-skirts','silkfred.com/womens/clothing/skirts/mini-skirts','https://www.silkfred.com/womens/clothing/skirts/pencil-skirts'];
       $array20=['https://www.silkfred.com/womens/clothing/jackets-and-coats/blazers','https://www.silkfred.com/womens/clothing/jackets-and-coats/bomber-jackets','https://www.silkfred.com/womens/clothing/jackets-and-coats/denim-jackets','https://www.silkfred.com/womens/clothing/jackets-and-coats/duster-coats','https://www.silkfred.com/womens/clothing/jackets-and-coats/faux-fur-coats'];
       $array21=['https://www.silkfred.com/womens/clothing/jackets-and-coats/leather-jackets','https://www.silkfred.com/womens/clothing/jackets-and-coats/parkas','https://www.silkfred.com/womens/clothing/jackets-and-coats/trench-coats-and-macs','https://www.silkfred.com/womens/clothing/jackets-and-coats/wool-coats'];
       $array22=['https://www.silkfred.com/collection/black-cardigans','https://www.silkfred.com/collection/long-cardigans','https://www.silkfred.com/collection/chunky-knit-cardigans','https://www.silkfred.com/collection/christmas-cardigans','https://www.silkfred.com/collection/red-cardigans'];
       $array23=['https://www.silkfred.com/collection/white-cardigans','https://www.silkfred.com/collection/green-cardigans','https://www.silkfred.com/collection/navy-cardigans','https://www.silkfred.com/collection/grey-cardigans','https://www.silkfred.com/collection/knitwear-outlet'];
       $array24=[''];
       

       


        $this->sites = [
            'x1' => ['source' => $array1, 'target' =>'silkfred001.xms011.site'],
            'x2' => ['source' => $array2, 'target' =>'silkfred002.xms011.site'],
            'x3' => ['source' => $array3, 'target' =>'silkfred003.xms011.site'],
            'x4' => ['source' => $array4, 'target' =>'silkfred004.xms011.site'],
            'x5' => ['source' => $array5, 'target' =>'silkfred005.xms011.site'],
            'x6' => ['source' => $array6, 'target' =>'silkfred006.xms011.site'],
            'x7' => ['source' => $array7, 'target' =>'silkfred007.xms011.site'],
            'x8' => ['source' => $array8, 'target' =>'silkfred008.xms011.site'],
            'x9' => ['source' => $array9, 'target' =>'silkfred009.xms011.site'],
            'x10' => ['source' => $array10, 'target' =>'silkfred010.xms011.site'],
            'x11' => ['source' => $array11, 'target' =>'silkfred011.xms013.site'],
            'x12' => ['source' => $array12, 'target' =>'silkfred012.xms013.site'],
            'x13' => ['source' => $array13, 'target' =>'silkfred013.xms013.site'],
            'x14' => ['source' => $array14, 'target' =>'silkfred014.xms013.site'],
            'x15' => ['source' => $array15, 'target' =>'silkfred015.xms013.site'],
            'x16' => ['source' => $array16, 'target' =>'silkfred016.xms013.site'],
            'x17' => ['source' => $array17, 'target' =>'hxflla'],
            'x18' => ['source' => $array18, 'target' =>'hxflla'],
            'x19' => ['source' => $array19, 'target' =>'hxflla'],
            'x20' => ['source' => $array20, 'target' =>'hxflla'],
            'x21' => ['source' => $array21, 'target' =>'hxflla'],
            'x22' => ['source' => $array22, 'target' =>'hxflla'],
            'x23' => ['source' => $array23, 'target' =>'hxflla'],
            
            
 
             //hxfaa
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.silkfred.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8e87e9eaf6d75a3c969473d60e14480835b8aae6',
            // 'cs_66331b14ef92978de54f0ed1efdcae1e284e8187',
            // oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }

       // 用于图片上传失败
       public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);

            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('.pagination__next');
    //        echo $nextNode->attr("href");
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));
//     
    }

    protected function processProductList(Crawler $crawler)
    {  

        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs__list')->eq(0)->filter('li [itemprop="name"]')->each(function (Crawler $node) {  //分类类名（必须）
            return $node->text();
        }));
        //  print_r($breadcrumbs);exit;

        // $breadcrumbs=$crawler->filter('[property="og:url"]')->attr('content');
        // $a=explode("D=",$breadcrumbs);
        // $a=explode("+",$a[1]);
        // print_r($a);exit;
        // 进入详情页爬取数据                                         use ($a) $breadcrumbs use()
        $crawler->filter('.product-image-wrapper a')->each(function (Crawler $node, $i)use ($breadcrumbs){
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s','https://www.silkfred.com'.$node->attr('href')),
                    'breadcrumbs'=>$breadcrumbs,    //$breadcrumbs
                   
                ]);
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
        });
    }

//    要爬取的数据

    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.product-heading__name')->text();  //标题（必须）
        // print_r($product['title']);exit;

        // $pricedel=[',','€','$'];
        // $product['price'] = str_replace($pricedel,'',$crawler->filter('.price')->text()); 

        // $del= [';'];
        // $int = $crawler->filter('script')->eq(29)->text();  //价格（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['price']=json_decode($it,true)['price'];
        $product['price']=$crawler->filter('[itemprop="price"]')->attr('content');
        // $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('[itemprop="price"]')->text())));  //价格（必须）

        // print_r($product['price']);exit;

        $product['brand']=$crawler->filter('.product-heading__brand a')->text();
       //品牌（必须）

    //    $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)['brand']['name'];

    //    if($crawler->filter('.productInfo__subtitle')->count()){
    //        $product['brand']=$crawler->filter('.productInfo__subtitle')->text();
    //    }else{
    //     $product['brand']=' ';
    //    }
        // print_r($product['brand']);exit;
        

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('.breadcrumbs ul')->filter('.breadcrumbs__link')->filter('[itemprop="name"]')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;   $item['breadcrumbs'],3 
        $product['breadcrumbs'] = array_slice($item['breadcrumbs'],2);
        // array_push($product['breadcrumbs'],$item['breadcrumbs'][0]);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($product['breadcrumbs']);exit;
        
   
        $product['sku'] =$crawler->filter('[itemprop="sku"]')->attr('content'); 
        // $del= [';'];
        // $int = $crawler->filter('script')->eq(29)->text();  //产品编号（必须）
        // $index = strpos($int,'=');
        // $it=str_replace($del,'',substr($int,$index+1));
        // $product['sku']=json_decode($it,true)['id'];
        // print_r($product['sku']);exit;
       

         $product['short_description']=$crawler->filter('.list--unordered')->html();
          // 简短描述
        // print_r($product['short_description']);exit;
       
        // $product['description']='';//描述
  
        $product['description']=$crawler->filter('.accordion__content')->eq(1)->html();

        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性   x1 x2 使用
        
        // if($crawler->filter('.detailInfo')->count()) {
        //     $count = $crawler->filter('.detailInfo div')->eq(1)->count();
        //     // echo $count;exit;
        //         for($i=0;$i<$count-1;$i++){
        //             $product['attributes'][] = [
        //                 'name' =>$crawler->filter('.product-specification-item')->eq($i)->filter('dt')->text(),
        //                 'options' =>$crawler->filter('.product-specification-item')->eq($i)->filter('dd')->text(),
        //             ];
        //         }       
            
        // }else{
        //     $product['attributes'] = [];
        // }
         
        // print_r($product['attributes']);exit;
        
          // 其他
        $product['keywords'] = [];

        // $product['gender']='';  x8使用
        $product['gender'] = '';
        // print_r($product['gender']);exit;

        $product['color'] ='';
        
        // print_r($product['color']);exit;

        $product['subCategory']=' ';
        $product['tags'] =[];
        $product['images'] =[];
        //图片（必须）

        // 在json中获取图片，【判断一下，位置在哪里】
        // $img=[];
        // $crawler->filter('[type="application/ld+json"]')->each(function(Crawler $node)use(&$img){
        //     if(strstr($node->text(),"image")){
        //         $img = json_decode($node->text(),true)['image'];
        //     }
        // });
        // print_r($img);exit;
        $images=array_unique(array_filter($crawler->filter('.product-image-zoom img')->each(function(Crawler $node,$i){
            return  $node->attr('src');
        })));   
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' =>$image,
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($product['images']);exit;
    //   商品的尺寸
        $product['variations']=[];
        // print_r($product);exit;
       $del=['<option value="" >Select a size</option>','- Out of Stock'];
        if($crawler->filter('.product-size')->count()>0){$product['variations'][]= [
                'name'=>'Size',
                'options'=>array_unique(array_filter(str_replace($del,'',$crawler->filter('.product-size')->eq(0)->filter('option')->each(function (Crawler $node, $i) {
                    return $node->attr('value');
                }))))
            ];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}