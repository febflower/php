<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PDO;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class sunandski extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:sunandski')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.sunandski.com');
    }
    protected function initialize(Input $input, Output $output)
    {
       $array=[
        // 滑雪靴
        'https://www.sunandski.com/c/ski-boots','https://www.sunandski.com/c/ski-boots?startIndex=24','https://www.sunandski.com/c/ski-boots?startIndex=48','https://www.sunandski.com/c/ski-boots?startIndex=72','https://www.sunandski.com/c/ski-boots?startIndex=96','https://www.sunandski.com/c/ski-boots?startIndex=120','https://www.sunandski.com/c/ski-boots?startIndex=144','https://www.sunandski.com/c/ski-boots?startIndex=168',
       //滑雪板
        'https://www.sunandski.com/c/skis','https://www.sunandski.com/c/skis?startIndex=24','https://www.sunandski.com/c/skis?startIndex=48','https://www.sunandski.com/c/skis?startIndex=72','https://www.sunandski.com/c/skis?startIndex=96','https://www.sunandski.com/c/skis?startIndex=120','https://www.sunandski.com/c/skis?startIndex=144','https://www.sunandski.com/c/skis?startIndex=168','https://www.sunandski.com/c/skis?startIndex=192',
    ];   
      $array1=[
        
        'https://www.sunandski.com/c/snow-goggles','https://www.sunandski.com/c/snow-goggles?startIndex=24','https://www.sunandski.com/c/snow-goggles?startIndex=48','https://www.sunandski.com/c/snow-goggles?startIndex=72',
        'https://www.sunandski.com/c/snow-helmets','https://www.sunandski.com/c/snow-helmets?startIndex=24','https://www.sunandski.com/c/snow-helmets?startIndex=48','https://www.sunandski.com/c/snow-helmets?startIndex=72','https://www.sunandski.com/c/snow-helmets?startIndex=96',
        'https://www.sunandski.com/c/snowboard-boots','https://www.sunandski.com/c/snowboard-boots?startIndex=24','https://www.sunandski.com/c/snowboard-boots?startIndex=48','https://www.sunandski.com/c/snowboard-boots?startIndex=72','https://www.sunandski.com/c/snowboard-boots?startIndex=96',
        'https://www.sunandski.com/c/snowboard-bindings','https://www.sunandski.com/c/snowboard-bindings?startIndex=24','https://www.sunandski.com/c/snowboard-bindings?startIndex=48','https://www.sunandski.com/c/snowboard-bindings?startIndex=72',
    ];
      $array2=[
        // Swimwear
         'https://www.sunandski.com/c/swimsuits-boardshorts','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=24','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=48','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=72','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=96','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=120','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=144','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=168','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=192','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=216','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=240','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=264','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=288','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=312','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=336','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=360','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=384','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=408','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=432','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=456','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=480','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=504','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=528','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=552','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=576','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=600','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=624','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=648','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=672','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=696','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=720','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=744','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=768','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=792','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=816','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=840','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=864','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=888','https://www.sunandski.com/c/swimsuits-boardshorts?startIndex=912'
      ];
      $array3=[
        // Sandals & Flip-Flops  和  Water Shoes
         'https://www.sunandski.com/c/sandals','https://www.sunandski.com/c/sandals?startIndex=24','https://www.sunandski.com/c/sandals?startIndex=48','https://www.sunandski.com/c/sandals?startIndex=72','https://www.sunandski.com/c/sandals?startIndex=96','https://www.sunandski.com/c/sandals?startIndex=120','https://www.sunandski.com/c/sandals?startIndex=144','https://www.sunandski.com/c/sandals?startIndex=168','https://www.sunandski.com/c/sandals?startIndex=192','https://www.sunandski.com/c/sandals?startIndex=216','https://www.sunandski.com/c/sandals?startIndex=240','https://www.sunandski.com/c/sandals?startIndex=264','https://www.sunandski.com/c/sandals?startIndex=288','https://www.sunandski.com/c/sandals?startIndex=312','https://www.sunandski.com/c/sandals?startIndex=336','https://www.sunandski.com/c/sandals?startIndex=360','https://www.sunandski.com/c/water-shoes'
      ];
      $array4=[
        // Run 跑步Sunglasses
        'https://www.sunandski.com/c/running-fitness-apparel','https://www.sunandski.com/c/running-fitness-apparel?startIndex=24','https://www.sunandski.com/c/running-fitness-apparel?startIndex=48','https://www.sunandski.com/c/running-fitness-apparel?startIndex=72','https://www.sunandski.com/c/running-fitness-apparel?startIndex=96',
        'https://www.sunandski.com/c/running-shoes','https://www.sunandski.com/c/running-shoes?startIndex=24','https://www.sunandski.com/c/running-shoes?startIndex=48','https://www.sunandski.com/c/running-shoes?startIndex=72','https://www.sunandski.com/c/running-shoes?startIndex=96','https://www.sunandski.com/c/running-shoes?startIndex=120','https://www.sunandski.com/c/running-shoes?startIndex=144','https://www.sunandski.com/c/running-shoes?startIndex=168','https://www.sunandski.com/c/running-shoes?startIndex=192','https://www.sunandski.com/c/running-shoes?startIndex=216','https://www.sunandski.com/c/running-shoes?startIndex=240','https://www.sunandski.com/c/running-shoes?startIndex=264',
        'https://www.sunandski.com/c/trail-running-shoes','https://www.sunandski.com/c/trail-running-shoes?startIndex=24','https://www.sunandski.com/c/trail-running-shoes?startIndex=48','https://www.sunandski.com/c/trail-running-shoes?startIndex=72',
        'https://www.sunandski.com/c/fitness-trackers','https://www.sunandski.com/c/fitness-trackers?startIndex=24','https://www.sunandski.com/c/relief-recovery-accessories','https://www.sunandski.com/c/relief-recovery-accessories?startIndex=24','https://www.sunandski.com/c/sport-sunglasses','https://www.sunandski.com/c/sport-sunglasses?startIndex=24','https://www.sunandski.com/c/hydration-packs','https://www.sunandski.com/c/hydration-packs?startIndex=24',
      ];
      $array5=[
        //Backpacks, Bags & Luggage 和 Sunglasses
        'https://www.sunandski.com/c/sunglasses','https://www.sunandski.com/c/sunglasses?startIndex=24','https://www.sunandski.com/c/sunglasses?startIndex=48','https://www.sunandski.com/c/sunglasses?startIndex=72','https://www.sunandski.com/c/sunglasses?startIndex=96','https://www.sunandski.com/c/sunglasses?startIndex=120','https://www.sunandski.com/c/sunglasses?startIndex=144','https://www.sunandski.com/c/sunglasses?startIndex=168','https://www.sunandski.com/c/sunglasses?startIndex=192','https://www.sunandski.com/c/sunglasses?startIndex=216',
        'https://www.sunandski.com/c/backpacks','https://www.sunandski.com/c/backpacks?startIndex=24','https://www.sunandski.com/c/backpacks?startIndex=48','https://www.sunandski.com/c/backpacks?startIndex=72','https://www.sunandski.com/c/duffel-bags','https://www.sunandski.com/c/duffel-bags?startIndex=24','https://www.sunandski.com/c/wheeled-luggage','https://www.sunandski.com/c/ski-snowboard-travel-bags','https://www.sunandski.com/c/tote-bags','https://www.sunandski.com/c/waist-packs-hip-packs','https://www.sunandski.com/c/travel-accessories','https://www.sunandski.com/c/travel-accessories?startIndex=24','https://www.sunandski.com/c/hydration-packs-outdoor','https://www.sunandski.com/c/hydration-packs-outdoor?startIndex=24',
      ];
      $array6=[
        'https://www.sunandski.com/c/skateboards-longboards','https://www.sunandski.com/c/skateboards-longboards?startIndex=24','https://www.sunandski.com/c/skateboards-longboards?startIndex=48','https://www.sunandski.com/c/skateboards-longboards?startIndex=72',
        'https://www.sunandski.com/c/skate-clothing','https://www.sunandski.com/c/skate-clothing?startIndex=24','https://www.sunandski.com/c/skate-clothing?startIndex=48','https://www.sunandski.com/c/skate-clothing?startIndex=72','https://www.sunandski.com/c/skate-clothing?startIndex=96','https://www.sunandski.com/c/skate-clothing?startIndex=120','https://www.sunandski.com/c/skate-clothing?startIndex=144','https://www.sunandski.com/c/skate-clothing?startIndex=168','https://www.sunandski.com/c/skate-clothing?startIndex=192','https://www.sunandski.com/c/skate-clothing?startIndex=216','https://www.sunandski.com/c/skate-clothing?startIndex=240','https://www.sunandski.com/c/skate-clothing?startIndex=264','https://www.sunandski.com/c/skate-clothing?startIndex=288','https://www.sunandski.com/c/skate-clothing?startIndex=312','https://www.sunandski.com/c/skate-clothing?startIndex=336','https://www.sunandski.com/c/skate-clothing?startIndex=360','https://www.sunandski.com/c/skate-clothing?startIndex=384','https://www.sunandski.com/c/skate-clothing?startIndex=408','https://www.sunandski.com/c/skate-clothing?startIndex=432','https://www.sunandski.com/c/skate-clothing?startIndex=456','https://www.sunandski.com/c/skate-clothing?startIndex=480','https://www.sunandski.com/c/skate-clothing?startIndex=504','https://www.sunandski.com/c/skate-clothing?startIndex=528','https://www.sunandski.com/c/skate-clothing?startIndex=552','https://www.sunandski.com/c/skate-clothing?startIndex=576','https://www.sunandski.com/c/skate-clothing?startIndex=600','https://www.sunandski.com/c/skate-clothing?startIndex=624','https://www.sunandski.com/c/skate-clothing?startIndex=648',
        'https://www.sunandski.com/c/skate-shoes','https://www.sunandski.com/c/skate-shoes?startIndex=24','https://www.sunandski.com/c/skate-shoes?startIndex=48','https://www.sunandski.com/c/skate-shoes?startIndex=72',
      ];
      $array7=[
        // Men's Ski & Snowboard Jackets
        'https://www.sunandski.com/c/mens-snow-jackets','https://www.sunandski.com/c/mens-snow-jackets?startIndex=24','https://www.sunandski.com/c/mens-snow-jackets?startIndex=48','https://www.sunandski.com/c/mens-snow-jackets?startIndex=72','https://www.sunandski.com/c/mens-snow-jackets?startIndex=96','https://www.sunandski.com/c/mens-snow-jackets?startIndex=120','https://www.sunandski.com/c/mens-snow-jackets?startIndex=144','https://www.sunandski.com/c/mens-snow-jackets?startIndex=168','https://www.sunandski.com/c/mens-snow-jackets?startIndex=192','https://www.sunandski.com/c/mens-snow-jackets?startIndex=216',
        //Men's Ski & Snowboard Pants
        'https://www.sunandski.com/c/mens-ski-snowboard-pants','https://www.sunandski.com/c/mens-ski-snowboard-pants?startIndex=24','https://www.sunandski.com/c/mens-ski-snowboard-pants?startIndex=48','https://www.sunandski.com/c/mens-ski-snowboard-pants?startIndex=72','https://www.sunandski.com/c/mens-ski-snowboard-pants?startIndex=96','https://www.sunandski.com/c/mens-ski-snowboard-pants?startIndex=120',
        //Men's Mid Layers
        'https://www.sunandski.com/c/men-s-mid-layers','https://www.sunandski.com/c/men-s-mid-layers?startIndex=24','https://www.sunandski.com/c/men-s-mid-layers?startIndex=48','https://www.sunandski.com/c/men-s-mid-layers?startIndex=72','https://www.sunandski.com/c/men-s-mid-layers?startIndex=96','https://www.sunandski.com/c/men-s-mid-layers?startIndex=120','https://www.sunandski.com/c/men-s-mid-layers?startIndex=144','https://www.sunandski.com/c/men-s-mid-layers?startIndex=168','https://www.sunandski.com/c/men-s-mid-layers?startIndex=192','https://www.sunandski.com/c/men-s-mid-layers?startIndex=216','https://www.sunandski.com/c/men-s-mid-layers?startIndex=240','https://www.sunandski.com/c/men-s-mid-layers?startIndex=264','https://www.sunandski.com/c/men-s-mid-layers?startIndex=288',
        //Men's Fleece
        'https://www.sunandski.com/c/men-s-fleece','https://www.sunandski.com/c/men-s-fleece?startIndex=24','https://www.sunandski.com/c/men-s-fleece?startIndex=48','https://www.sunandski.com/c/men-s-fleece?startIndex=72','https://www.sunandski.com/c/men-s-fleece?startIndex=96',
        //Men's Insulators
        'https://www.sunandski.com/c/men-s-insulators','https://www.sunandski.com/c/men-s-insulators?startIndex=24','https://www.sunandski.com/c/men-s-insulators?startIndex=48',
        //Men's Hoodies & Sweatshirts
        'https://www.sunandski.com/c/mens-hoodies-sweaters','https://www.sunandski.com/c/mens-hoodies-sweaters?startIndex=24','https://www.sunandski.com/c/mens-hoodies-sweaters?startIndex=48','https://www.sunandski.com/c/mens-hoodies-sweaters?startIndex=72','https://www.sunandski.com/c/mens-hoodies-sweaters?startIndex=96','https://www.sunandski.com/c/mens-hoodies-sweaters?startIndex=120',
        //Men's Casual Jackets
        'https://www.sunandski.com/c/men-s-casual-jackets','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=24','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=48','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=72','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=96','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=120','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=144','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=168','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=192','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=216','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=240','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=264','https://www.sunandski.com/c/men-s-casual-jackets?startIndex=288',
      ];
      $array8=[
        //Men's Button Down & Polo Shirts
        'https://www.sunandski.com/c/men-s-button-down-polo-shirts','https://www.sunandski.com/c/men-s-button-down-polo-shirts?startIndex=24','https://www.sunandski.com/c/men-s-button-down-polo-shirts?startIndex=48','https://www.sunandski.com/c/men-s-button-down-polo-shirts?startIndex=72','https://www.sunandski.com/c/men-s-button-down-polo-shirts?startIndex=96','https://www.sunandski.com/c/men-s-button-down-polo-shirts?startIndex=120','https://www.sunandski.com/c/men-s-button-down-polo-shirts?startIndex=144',
        //Men's T-Shirts
        'https://www.sunandski.com/c/mens-t-shirts','https://www.sunandski.com/c/mens-t-shirts?startIndex=24','https://www.sunandski.com/c/mens-t-shirts?startIndex=48','https://www.sunandski.com/c/mens-t-shirts?startIndex=72','https://www.sunandski.com/c/mens-t-shirts?startIndex=96','https://www.sunandski.com/c/mens-t-shirts?startIndex=120','https://www.sunandski.com/c/mens-t-shirts?startIndex=144','https://www.sunandski.com/c/mens-t-shirts?startIndex=168','https://www.sunandski.com/c/mens-t-shirts?startIndex=192','https://www.sunandski.com/c/mens-t-shirts?startIndex=216','https://www.sunandski.com/c/mens-t-shirts?startIndex=240','https://www.sunandski.com/c/mens-t-shirts?startIndex=264','https://www.sunandski.com/c/mens-t-shirts?startIndex=288','https://www.sunandski.com/c/mens-t-shirts?startIndex=312','https://www.sunandski.com/c/mens-t-shirts?startIndex=336','https://www.sunandski.com/c/mens-t-shirts?startIndex=360','https://www.sunandski.com/c/mens-t-shirts?startIndex=384','https://www.sunandski.com/c/mens-t-shirts?startIndex=408','https://www.sunandski.com/c/mens-t-shirts?startIndex=432','https://www.sunandski.com/c/mens-t-shirts?startIndex=456','https://www.sunandski.com/c/mens-t-shirts?startIndex=480','https://www.sunandski.com/c/mens-t-shirts?startIndex=504','https://www.sunandski.com/c/mens-t-shirts?startIndex=528','https://www.sunandski.com/c/mens-t-shirts?startIndex=552',
      ];
      $array9=[
        //Women's Swimwear
        'https://www.sunandski.com/c/womens-swimsuits','https://www.sunandski.com/c/womens-swimsuits?startIndex=24','https://www.sunandski.com/c/womens-swimsuits?startIndex=48','https://www.sunandski.com/c/womens-swimsuits?startIndex=72','https://www.sunandski.com/c/womens-swimsuits?startIndex=96','https://www.sunandski.com/c/womens-swimsuits?startIndex=120','https://www.sunandski.com/c/womens-swimsuits?startIndex=144','https://www.sunandski.com/c/womens-swimsuits?startIndex=168','https://www.sunandski.com/c/womens-swimsuits?startIndex=192','https://www.sunandski.com/c/womens-swimsuits?startIndex=216','https://www.sunandski.com/c/womens-swimsuits?startIndex=240','https://www.sunandski.com/c/womens-swimsuits?startIndex=264','https://www.sunandski.com/c/womens-swimsuits?startIndex=288','https://www.sunandski.com/c/womens-swimsuits?startIndex=312','https://www.sunandski.com/c/womens-swimsuits?startIndex=336','https://www.sunandski.com/c/womens-swimsuits?startIndex=360','https://www.sunandski.com/c/womens-swimsuits?startIndex=384','https://www.sunandski.com/c/womens-swimsuits?startIndex=408','https://www.sunandski.com/c/womens-swimsuits?startIndex=432','https://www.sunandski.com/c/womens-swimsuits?startIndex=456','https://www.sunandski.com/c/womens-swimsuits?startIndex=480','https://www.sunandski.com/c/womens-swimsuits?startIndex=504',
        // Women's Rashguards和Women's Boardshorts和Women's Boardshorts和Cover Ups
        'https://www.sunandski.com/c/womens-rashguards','https://www.sunandski.com/c/womens-rashguards?startIndex=24','https://www.sunandski.com/c/womens-boardshorts','https://www.sunandski.com/c/womens-boardshorts?startIndex=24','https://www.sunandski.com/c/womens-boardshorts','https://www.sunandski.com/c/womens-boardshorts?startIndex=24','https://www.sunandski.com/c/cover-ups','https://www.sunandski.com/c/cover-ups?startIndex=24',
      ];
      $array10=[
        'https://www.sunandski.com/c/womens-running-shoes','https://www.sunandski.com/c/womens-running-shoes?startIndex=24','https://www.sunandski.com/c/womens-running-shoes?startIndex=48','https://www.sunandski.com/c/womens-running-shoes?startIndex=72','https://www.sunandski.com/c/womens-running-shoes?startIndex=96',
        'https://www.sunandski.com/c/womens-trail-running-shoes','https://www.sunandski.com/c/womens-trail-running-shoes?startIndex=24','https://www.sunandski.com/c/womens-hiking-boots-shoes','https://www.sunandski.com/c/womens-hiking-boots-shoes?startIndex=24','https://www.sunandski.com/c/womens-hiking-boots-shoes?startIndex=48',
        'https://www.sunandski.com/c/womens-casual-shoes','https://www.sunandski.com/c/womens-casual-shoes?startIndex=24','https://www.sunandski.com/c/womens-casual-shoes?startIndex=48','https://www.sunandski.com/c/womens-casual-shoes?startIndex=72','https://www.sunandski.com/c/womens-casual-shoes?startIndex=96','https://www.sunandski.com/c/womens-casual-shoes?startIndex=120','https://www.sunandski.com/c/womens-casual-shoes?startIndex=144','https://www.sunandski.com/c/womens-casual-shoes?startIndex=168','https://www.sunandski.com/c/womens-casual-shoes?startIndex=192','https://www.sunandski.com/c/womens-casual-shoes?startIndex=216','https://www.sunandski.com/c/womens-casual-shoes?startIndex=240'
      ];
      $array11=[
        'https://www.sunandski.com/c/womens-snow-boots','https://www.sunandski.com/c/womens-snow-boots?startIndex=24','https://www.sunandski.com/c/womens-snow-boots?startIndex=48','https://www.sunandski.com/c/womens-snow-boots?startIndex=72','https://www.sunandski.com/c/womens-snow-boots?startIndex=96','https://www.sunandski.com/c/womens-snow-boots?startIndex=120',
        'https://www.sunandski.com/c/womens-slippers','https://www.sunandski.com/c/womens-slippers?startIndex=24','https://www.sunandski.com/c/women-s-sandals','https://www.sunandski.com/c/women-s-sandals?startIndex=24','https://www.sunandski.com/c/women-s-sandals?startIndex=48','https://www.sunandski.com/c/women-s-sandals?startIndex=72','https://www.sunandski.com/c/women-s-sandals?startIndex=96','https://www.sunandski.com/c/women-s-sandals?startIndex=120','https://www.sunandski.com/c/women-s-sandals?startIndex=144','https://www.sunandski.com/c/women-s-sandals?startIndex=168','https://www.sunandski.com/c/women-s-sandals?startIndex=192','https://www.sunandski.com/c/women-s-socks','https://www.sunandski.com/c/women-s-socks?startIndex=24','https://www.sunandski.com/c/women-s-socks?startIndex=48','https://www.sunandski.com/c/women-s-socks?startIndex=72','https://www.sunandski.com/c/women-s-socks?startIndex=96','https://www.sunandski.com/c/women-s-socks?startIndex=120','https://www.sunandski.com/c/women-s-socks?startIndex=144','https://www.sunandski.com/c/women-s-socks?startIndex=168','https://www.sunandski.com/c/women-s-socks?startIndex=192'
      ];
        $this->sites = [
            'x' => ['source' => $array, 'target' =>'sunandski.xms005.site'],
            'x1'=> ['source' => $array1, 'target' =>'sunand002.xms005.site'],
            'x2'=> ['source' => $array2, 'target' =>'www.dressespopularshop.com'],
            'x3'=> ['source' => $array3, 'target' =>'sunand004.xms005.site'], 
            'x4'=> ['source' => $array4, 'target' =>'www.bikeaccessoriessale.com'],
            'x5'=> ['source' => $array5, 'target' =>'www.backpackonsale.com'],
            'x6'=> ['source' => $array6, 'target' =>'sunand007.xms005.site'],
            'x7'=> ['source' => $array7, 'target' =>'sunand008.xms005.site'],
            'x8'=> ['source' => $array8, 'target' =>'sunand009.xms005.site'],
            'x9'=> ['source' => $array9, 'target' =>'www.activeshortsshop.com'],
            'x10'=> ['source' => $array10, 'target' =>'sunand011.xms005.site'],
            'x11'=> ['source' => $array11, 'target' =>'sunand012.xms005.site'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.sunandski.com',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥 
        $this->woocommerce = new Client(
            'https://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
               //oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
        //    echo $nextNode->attr("href");exit;
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }

            // print_r($nextNode->attr("href"));exit;

//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.products__wrapper')->filter('.product__name-wrap a')->each(function (Crawler $node, $i)  {
            
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
                
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            // print_r($node->attr('href'));exit;
        });
 
    }

// 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('[property="og:title"]')->attr('content');  //标题（必须）
        // print_r($product['title']);exit;

        $product['price'] = $crawler->filter('.product__price')->attr('content');
        //价格（必须）
        // $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['brand']['name'];
     
        // json_decode($crawler->filter('[type="application/ld+json"]')->last()->text(),true)["@graph"][1]['offers'][0]['price'];
        // print_r($product['price']);exit;

       //品牌（必须）

       $product['brand'] = $crawler->filter('[itemprop="brand"]')->filter('meta')->attr('content');
    //    $brand = explode('Brand: "',$crawler->filter('[id="viewed_product"]')->text());
    //    $brands = explode(',',$brand[1]);
    //    $product['brand']=str_replace('"','',$brands[0]);

        // print_r($product['brand']);exit;

        // $product['sku'] = $crawler->filter('.productView-info-value')->text();  //产品编号（必须）
        $product['sku'] = $crawler->filter('.product__code dd')->text();  //产品编号（必须）
        // $sku=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)["productID"];
        // print_r($product['sku']);exit;

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        
        //分类类名
       
        $breadcrumbs = array_filter($crawler->filter('.breadcrumb__item a span')->each(function (Crawler $node) {
            return $node->text();
         }));
        
        $product['breadcrumbs'] = array_slice($breadcrumbs,3);
        // print_r($product['breadcrumbs']);exit();
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
    //    print_r($product['breadcrumbs']);exit;

      
        // print_r($product['sku']);exit;
        
        $product['short_description']=''; //简短描述

        // print_r($product['short_description']);exit;
       
        // $product['description']=$crawler->filter('.product-details')->filter('.container p')->last()->html();//描述
        
         $product['description']=$crawler->filter('.product-details')->filter('.product-details p')->last()->html();//描述
            // $index = strpos($int,'<strong>UPC:</strong>');
            // $product['description'] = substr($int,0,$index);

        // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
       
        // print_r($product['description']);exit;

        $product['attributes'] = [];
        //属性
        // if($crawler->filter(".characteristics")->count()) {
        //     $count = $crawler->filter(".characteristics ul li")->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name' => $crawler->filter(".characteristics ul li")->eq($i)->filter('span')->text(),
        //             'options' => [$crawler->filter(".characteristics ul li")->eq($i)->filter('span')->eq(1)->text()],
        //         ];
        //     }
        // }


        // print_r(  $product['attributes']);exit;

        // $product['attributes']=$crawler->filter('.product-description')->html();
        //   print_r($product['attributes']);exit; 
        //标签
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));

       
        // print_r($product['tags']);exit;

      // 其他
        $product['keywords'] = [];
        $product['gender'] = '';//性别
        // print_r($product['gender']);exit;
        $product['color'] = '';    
        
        // print_r($product['color']);exit;
        $product['variations']=[];
        $product['subCategory']=' ';
       
        $product['images'] =[];
        //图片（必须）

        $images_ls=$crawler->filter('[id="data-mz-preload-product"]')->each(function (Crawler $node, $i) {
            return json_decode($node->text(),true)["content"]["productImages"];
        });
        $images_ls=array_filter($images_ls);

        foreach ($images_ls as $image_ls) {
            foreach ($image_ls as $image) {
                $product['images'][] = [
                    'src' => 'https:'.$image['src'],
                    'name' => $product['title'],
                ];
            }
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败

// print_r($product['images']);exit;

    // if($images=$crawler->filter('.product-config__images a')->count()>0){
    //     $images=$crawler->filter('.product-config__images a')->each(function(Crawler $node,$i){
    //         return 'https:'.$node->attr('href');
    //     });
    // }else{
    //     $images=$crawler->filter('.product__image')->each(function(Crawler $node,$i){
    //         return 'https:'.$node->attr('src');
    //     });
    // }
    //     // 这里if是判断图片是否有多张
    //     if(count($images)>0){
    //         foreach ($images as $image) {
    //             $product['images'][] = [
    //                 'src' => $image,
    //                 'name' => $product['title'],
    //             ];
    //         }
    //     }
   
    //     $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    // //    print_r($product['images']);exit;
    
    //   商品的尺寸或者颜色
    // $colors = $crawler->filter('')
    // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
      
    //  $product['variations']=[];



     if($crawler->filter('.form-group--stacked')->eq(0)->count()){
        $product['variations'][]= [
            'name'=>$crawler->filter('.position-relative')->filter('label')->eq(0)->attr('for'),
            'options'=>$crawler->filter('.option-swatches')->eq(0)->filter('.option-swatches__item span')->each(function (Crawler $node, $i) {
                return $node->text();
            })
        ]; 
        $product['type'] = 'variable';
    }
    if($crawler->filter('.form-group--stacked')->eq(1)->count()){
        $product['variations'][]= [
            'name'=>$crawler->filter('.position-relative')->filter('label')->eq(1)->attr('for'),
            'options'=>$crawler->filter('.option-swatches')->eq(1)->filter('.option-swatches__item span')->each(function (Crawler $node, $i) {
                return $node->text();})]; 
            $product['type'] = 'variable';
    }

   
    // print_r($product['variations']);exit;    



        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {
            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}