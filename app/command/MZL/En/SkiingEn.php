<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class SkiingEn extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:SkiingEn')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.utahskigear.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        $array=[
                'https://www.utahskigear.com/collections/snowboard-boots?view=no-usf',
        ];


        $this->sites = [
            'x' => ['source' => $array, 'target' => 'hxflla'],

        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.utahskigear.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('.next');
        //    echo $nextNode->attr("href");exit;
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }
            // print_r($nextNode->attr("href"));exit;

//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.grid-product__link')->each(function (Crawler $node, $i)  {
            
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
                
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            // print_r($node->attr('href'));exit;
        });
 
    }


    protected function crawlerProduct($item)
    {
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('.product-single__title')->text();  //标题（必须）
        // print_r($product['title']);exit;

        $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('.product__price')->text())));  //价格（必须）

        // print_r($product['price']);exit;
       
       //品牌（必须）
        $product['brand']=$crawler->filter('.product-single__vendor a')->text();
        // print_r($product['brand']);exit;
        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));

       
        // print_r($breadcrumbs);exit;
        $breadcrumbs=[];
        $product['breadcrumbs'] = array_slice(['breadcrumbs'],0);
        // $product['breadcrumbs'] = array_slice($breadcrumbs,0);
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
        // print_r($breadcrumbs);exit;

        $product['sku'] = $crawler->filter('.product-single__sku')->text();  //产品编号（必须）
        // print_r($product['sku']);exit;
        
        $product['short_description']=''; //简短描述

        // print_r($product['short_description']);exit;
       
        $product['description']=$crawler->filter('.product-single__description-full ')->html();//描述

        // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
       
        // print_r($product['description']);exit;
       
        
        //属性
        // if($crawler->filter(".attributes-list>li")->count()) {
        //     $count = $crawler->filter(".attributes-list>li")->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name' => $crawler->filter(".attributes-list>li")->eq($i)->text(),
        //             'options' =>'',
        //         ];
        //     }
        // }else{
        //     $product['attributes'] = [];
        // }
        $product['attributes'] = [];
        // $product['attributes']=$crawler->filter('.product-description')->html();
        //   print_r($product['attributes']);exit;
      // 其他
        $product['keywords'] = [];
        $product['gender'] = '';//性别
        // print_r($product['gender']);exit;
        $product['color'] = '';
        $product['subCategory']=' ';
        $product['tags'][] =[];
        $product['images'] =[];
        //图片（必须）
        $images=$crawler->filter('.product-image-main img')->each(function(Crawler $node,$i){
            return 'https:'.$node->attr('data-photoswipe-src');
        });
        // 这里if是判断图片是否有多张
        if(count($images)>0){
            foreach ($images as $image) {
                $product['images'][] = [
                    'src' => $image,
                    'name' => $product['title'],
                ];
            }
        }
    
    //    print_r($images);exit;
    //   商品的尺寸
        // $product['variations']=[];

        if($crawler->filter('.variant-input-wrap ')->count()>0){$product['variations'][]= [
                'name'=>'Size',
                'options' =>$crawler->filter('.variant-input')->each(function (Crawler $node, $i) {
                    return $node->attr('data-value');})];  
        }else{$product['variations'][]=[
                'name'=>'',
                'options' =>""];
        }
        if ($product['variations'][0]['options']){
            $product['type'] = 'variable';
        }
        // print_r($product['variations']);exit;

        
        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}