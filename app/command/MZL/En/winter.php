<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class winter extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:winter')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.wintermen.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
        //

        $array=[
            'https://www.wintermen.com/mens-jackets/ski-jackets','https://www.wintermen.com/mens-jackets/snowboard-jackets','https://www.wintermen.com/mens-jackets/down-synthetic-jackets','https://www.wintermen.com/mens-jackets/fleece-jackets','https://www.wintermen.com/mens-jackets/lifestyle-jackets','https://www.wintermen.com/mens-jackets/shell-softshell-jackets','https://www.wintermen.com/mens-jackets/one-piece-suits','https://www.wintermen.com/mens-jackets/vests',
            'https://www.wintermen.com/pants-bibs/ski-pants','https://www.wintermen.com/pants-bibs/snowboard-pants-bibs','https://www.wintermen.com/pants-bibs/shell-pants',
        ];
        //	Men's Winter Ski & Snowboard Accessories
        $array1=[
            'https://www.wintermen.com/accessories/hats-beanies','https://www.wintermen.com/accessories/socks','https://www.wintermen.com/accessories/goggles','https://www.wintermen.com/accessories/helmets','https://www.wintermen.com/accessories/helmet-accessories-covers','https://www.wintermen.com/accessories/belts','https://www.wintermen.com/accessories/suspenders','https://www.wintermen.com/accessories/body-hand-foot-warmers','https://www.wintermen.com/accessories/face-masks-neck-ear-warmers','https://www.wintermen.com/accessories/cameras-accessories','https://www.wintermen.com/accessories/camping-outdoor','https://www.wintermen.com/accessories/lip-balm-sunscreen-oxygen',''
        ];
        $array2=[
            'https://www.wintermen.com/mens-equipment/skis','https://www.wintermen.com/mens-equipment/ski-boots','https://www.wintermen.com/mens-equipment/ski-poles','https://www.wintermen.com/mens-equipment/ski-bindings','https://www.wintermen.com/mens-equipment/snowboards','https://www.wintermen.com/mens-equipment/snowboard-boots','https://www.wintermen.com/mens-equipment/snowboard-bindings','https://www.wintermen.com/mens-equipment/ski-snowboard-accessories','https://www.wintermen.com/mens-equipment/packs-travel-bags','https://www.wintermen.com/mens-equipment/ski-snowboard-bags','https://www.wintermen.com/mens-equipment/ski-snowboard-boot-bags','https://www.wintermen.com/mens-equipment/sleds-toys','https://www.wintermen.com/mens-equipment/wall-rack-mounts'
        ];
        $array3=[
            'https://www.winterwomen.com/womens-jackets/lifestyle-jackets','https://www.winterwomen.com/womens-jackets/vests'
        ];
        $array4=[
            'https://www.winterwomen.com/accessories', 'https://www.winterwomen.com/skis-snowboards/snow-sleds-and-toys'
        ];

      
        $array5=[
              'https://www.winterkids.com/skis-snowboards/kids-ski-snowboard-accessories?p=4','https://www.winterkids.com/sleds-snow-toys'
        ];  //                  'https://www.winterkids.com/skis-snowboards/kids-ski-snowboard-accessories?p=3',       'https://www.winterkids.com/skis-snowboards/kids-ski-snowboard-accessories?p=2',  'https://www.winterkids.com/street-wear/kids-gloves-and-mittens',
        // 'https://www.winterkids.com/skis-snowboards/kids-skis','https://www.winterkids.com/skis-snowboards/kids-ski-boots','https://www.winterkids.com/skis-snowboards/kids-snowboards','https://www.winterkids.com/skis-snowboards/kids-snowboard-boots','https://www.winterkids.com/skis-snowboards/kids-ski-snowboard-accessories',

        $this->sites = [
            'x' => ['source' => $array, 'target' =>'winter001.xms005.site'],
            'x1'=> ['source' => $array1, 'target' =>'winter002.xms005.site'],
            'x2'=> ['source' => $array2, 'target' =>'winter003.xms005.site'],
            'x3'=> ['source' => $array3, 'target' =>'winter004.xms005.site'],
            'x4'=> ['source' => $array4, 'target' =>'winter005.xms005.site'],
            'x5'=> ['source' => $array5, 'target' =>'winter006.xms005.site'],
            

            // 'x2'=> ['source' => $array1, 'target' => 'hxflla'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' =>'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.wintermen.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥 
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
               //oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('[rel="next"]');
        //    echo $nextNode->attr("href");exit;
            if ($nextNode->count()) {
                $this->processPage('https://www.wintermen.com'.$nextNode->attr('href'));
            }

            // print_r($nextNode->attr("href"));exit;

//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('.OSCAR_item a')->each(function (Crawler $node, $i)  {
            
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s',$node->attr('href')),
                ]);
                
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            // print_r($node->attr('href'));exit;
        });
 
    }

// 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
        
        $product['title'] = $crawler->filter('[property="og:title"]')->attr('content');  //标题（必须）
        // print_r($product['title']);exit;

        $pricedel=[',','€','$'];
        $product['price'] = str_replace($pricedel,'',$crawler->filter('.sel_lblActualPriceLow')->text());  //价格（必须）
        // $product['price'] = $crawler->filter('.sel_lblActualPriceLow')->text();
        //价格（必须）
        // $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['brand']['name'];
     
        // json_decode($crawler->filter('[type="application/ld+json"]')->last()->text(),true)["@graph"][1]['offers'][0]['price'];
        // print_r($product['price']);exit;

       //品牌（必须）

       $product['brand']=$crawler->filter('[property="product:brand"]')->attr('content');
    //    $brand = explode('Brand: "',$crawler->filter('[id="viewed_product"]')->text());
    //    $brands = explode(',',$brand[1]);
    //    $product['brand']=str_replace('"','',$brands[0]);

        // print_r($product['brand']);exit;

        // $product['sku'] = $crawler->filter('.productView-info-value')->text();  //产品编号（必须）
        // $product['sku'] = $crawler->filter('')->attr('content');  //产品编号（必须）
        // $sku=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)[0]["sku"];

        $product['sku']=json_decode($crawler->filter('[type="application/ld+json"]')->eq(1)->text(),true)["sku"];


        // print_r($product['sku']);exit;   

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        
        //分类类名
       
        $breadcrumbs = array_filter($crawler->filter('.breadcrumbs li a')->each(function (Crawler $node) {
            return $node->text();
         }));
        
        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        // print_r($product['breadcrumbs']);exit();
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
    //    print_r($product['breadcrumbs']);exit;

      
        // print_r($product['sku']);exit;
        
        $product['short_description']=''; //简短描述

        // print_r($product['short_description']);exit;
       
        $product['description']=$crawler->filter('.exco-target')->html();//描述
       

        // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
       
        // print_r($product['description']);exit;
        $product['attributes'] = [];
        //属性
        // if($crawler->filter(".accordion-detail ul")->count()) {
        //     $count = $crawler->filter(".accordion-detail ul")->count();
        //     for($i=0;$i<$count;$i++){
        //         $product['attributes'][] = [
        //             'name' => '',
        //             'options' =>$crawler->filter(".accordion-detail")->eq($i)->filter('ul')->text(),
        //         ];
        //     }
        // }
        // print_r(  $product['attributes']);exit;

        // $product['attributes']=$crawler->filter('.product-description')->html();
        //   print_r($product['attributes']);exit; 
        //标签
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));

       
        // print_r($product['tags']);exit;
       


      // 其他
        $product['keywords'] = [];
        $product['gender'] = '';//性别
        // print_r($product['gender']);exit;
        $product['color'] = '';
        $product['variations']=[];
        $product['subCategory']=' ';
       
        //图片（必须）


        // $images_ls=$crawler->filter('.elastislide-image img')->each(function (Crawler $node, $i) {
        //     return $node->attr('src');
        // });
        // $images_ls=array_filter($images_ls);

        // foreach ($images_ls as $image_ls) {
        //     foreach ($image_ls as $image) {
        //         $product['images'][] = [
        //             'src' => 'https:'.$image['src'],
        //             'name' => $product['title'],
        //         ];
        //     }
        // }
        // $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
            if($crawler->filter('.alt-image-gallery img')->count()){
                $images= $crawler->filter('.alt-image-gallery img')->each(function(Crawler $node,$i){
                    return 'https://'.'www.wintermen.com'.str_replace('/md/','/lg/',$node->attr('src'));
                });
                array_pop($images);//删除最后一张多余的图片
                // 这里if是判断图片是否有多张
                if(count($images)>0){
                    foreach ($images as $image) {
                        $product['images'][] = [
                            'src' => $image,
                            'name' => $product['title'],
                        ];
                    }
                }
            }   
       
        // $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($product['images']);


    
    //   商品的尺寸或者颜色
    // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
      
    //  $product['variations']=[];
    //  删除多余的字节，把他们设为空

            if($crawler->filter('.selectable-list')->eq(0)->count()){$product['variations'][]= [
                'name'=>$crawler->filter('.optionCategory')->text(),
                'options' =>$crawler->filter('.selectable-list')->eq(0)->filter('.trans-button')->each(function (Crawler $node, $i) {
                        return $node->attr('title');
                    })];  
                    $product['type'] = 'variable';  
            }
            if($crawler->filter('.selectable-list')->eq(1)->count()){$product['variations'][]= [
                    'name'=>$crawler->filter('.optionCategory')->eq(1)->text(),
                    'options' =>$crawler->filter('.selectable-list')->eq(1)->filter('.trans-button')->each(function (Crawler $node, $i) {
                        return $node->attr('title');})]; 
                        $product['type'] = 'variable';
            }
        // print_r($product['variations']);exit;


        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}