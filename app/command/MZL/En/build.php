<?php
namespace app\command\MZL\En;

use app\command\BuildCommon;
use Automattic\WooCommerce\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;
use think\console\Command;
use think\console\Input;
use think\console\input\Option;
use think\console\Output;
use Twig\Error\RuntimeError;

use function GuzzleHttp\Promise\each;
use function GuzzleHttp\Psr7\str;
use function vierbergenlars\SemVer\Internal\eq;

class build extends Command
{
    use BuildCommon;

    /**
     * @var \GuzzleHttp\Client
     */
    private $guzzleHttpClient;
    /**
     * @var Client
     */
    private $woocommerce;
    /**
     * @var mixed|string[]
     */
    private $processSite;
    
    protected function configure()
    {
        $this->setName('build:wordpress:mzl:build')
            ->addOption('site', 's', Option::VALUE_OPTIONAL, '站点')
            ->setDescription('创建Wordpress站点，数据源:https://www.build.com/');
    }
    protected function initialize(Input $input, Output $output)
    {
     

        $array1=["https://www.build.com/single-hole-sink-faucet/c109954?page=1","https://www.build.com/single-hole-sink-faucet/c109954?page=2","https://www.build.com/single-hole-sink-faucet/c109954?page=3","https://www.build.com/single-hole-sink-faucet/c109954?page=4","https://www.build.com/single-hole-sink-faucet/c109954?page=5","https://www.build.com/single-hole-sink-faucet/c109954?page=6","https://www.build.com/single-hole-sink-faucet/c109954?page=7","https://www.build.com/single-hole-sink-faucet/c109954?page=8","https://www.build.com/single-hole-sink-faucet/c109954?page=9","https://www.build.com/single-hole-sink-faucet/c109954?page=10","https://www.build.com/single-hole-sink-faucet/c109954?page=11","https://www.build.com/single-hole-sink-faucet/c109954?page=12","https://www.build.com/single-hole-sink-faucet/c109954?page=13","https://www.build.com/single-hole-sink-faucet/c109954?page=14","https://www.build.com/single-hole-sink-faucet/c109954?page=15","https://www.build.com/single-hole-sink-faucet/c109954?page=16","https://www.build.com/single-hole-sink-faucet/c109954?page=17","https://www.build.com/single-hole-sink-faucet/c109954?page=18","https://www.build.com/single-hole-sink-faucet/c109954?page=19","https://www.build.com/single-hole-sink-faucet/c109954?page=20","https://www.build.com/single-hole-sink-faucet/c109954?page=21","https://www.build.com/single-hole-sink-faucet/c109954?page=22","https://www.build.com/single-hole-sink-faucet/c109954?page=23","https://www.build.com/single-hole-sink-faucet/c109954?page=24","https://www.build.com/single-hole-sink-faucet/c109954?page=25","https://www.build.com/single-hole-sink-faucet/c109954?page=26","https://www.build.com/single-hole-sink-faucet/c109954?page=27","https://www.build.com/single-hole-sink-faucet/c109954?page=28","https://www.build.com/single-hole-sink-faucet/c109954?page=29","https://www.build.com/single-hole-sink-faucet/c109954?page=30","https://www.build.com/single-hole-sink-faucet/c109954?page=31","https://www.build.com/single-hole-sink-faucet/c109954?page=32","https://www.build.com/single-hole-sink-faucet/c109954?page=33","https://www.build.com/single-hole-sink-faucet/c109954?page=34","https://www.build.com/single-hole-sink-faucet/c109954?page=35","https://www.build.com/single-hole-sink-faucet/c109954?page=36","https://www.build.com/single-hole-sink-faucet/c109954?page=37","https://www.build.com/single-hole-sink-faucet/c109954?page=38","https://www.build.com/single-hole-sink-faucet/c109954?page=39","https://www.build.com/single-hole-sink-faucet/c109954?page=40","https://www.build.com/single-hole-sink-faucet/c109954?page=41","https://www.build.com/single-hole-sink-faucet/c109954?page=42","https://www.build.com/single-hole-sink-faucet/c109954?page=43","https://www.build.com/single-hole-sink-faucet/c109954?page=44","https://www.build.com/single-hole-sink-faucet/c109954?page=45","https://www.build.com/single-hole-sink-faucet/c109954?page=46","https://www.build.com/single-hole-sink-faucet/c109954?page=47","https://www.build.com/single-hole-sink-faucet/c109954?page=48","https://www.build.com/single-hole-sink-faucet/c109954?page=49","https://www.build.com/single-hole-sink-faucet/c109954?page=50","https://www.build.com/single-hole-sink-faucet/c109954?page=51","https://www.build.com/single-hole-sink-faucet/c109954?page=52","https://www.build.com/single-hole-sink-faucet/c109954?page=53","https://www.build.com/single-hole-sink-faucet/c109954?page=54","https://www.build.com/single-hole-sink-faucet/c109954?page=55","https://www.build.com/single-hole-sink-faucet/c109954?page=56","https://www.build.com/single-hole-sink-faucet/c109954?page=57","https://www.build.com/single-hole-sink-faucet/c109954?page=58","https://www.build.com/single-hole-sink-faucet/c109954?page=59","https://www.build.com/single-hole-sink-faucet/c109954?page=60","https://www.build.com/single-hole-sink-faucet/c109954?page=61","https://www.build.com/single-hole-sink-faucet/c109954?page=62","https://www.build.com/single-hole-sink-faucet/c109954?page=63","https://www.build.com/single-hole-sink-faucet/c109954?page=64","https://www.build.com/single-hole-sink-faucet/c109954?page=65","https://www.build.com/single-hole-sink-faucet/c109954?page=66","https://www.build.com/single-hole-sink-faucet/c109954?page=67","https://www.build.com/single-hole-sink-faucet/c109954?page=68","https://www.build.com/single-hole-sink-faucet/c109954?page=69","https://www.build.com/single-hole-sink-faucet/c109954?page=70","https://www.build.com/single-hole-sink-faucet/c109954?page=71","https://www.build.com/single-hole-sink-faucet/c109954?page=72","https://www.build.com/single-hole-sink-faucet/c109954?page=73","https://www.build.com/single-hole-sink-faucet/c109954?page=74","https://www.build.com/single-hole-sink-faucet/c109954?page=75","https://www.build.com/single-hole-sink-faucet/c109954?page=76","https://www.build.com/single-hole-sink-faucet/c109954?page=77","https://www.build.com/single-hole-sink-faucet/c109954?page=78","https://www.build.com/single-hole-sink-faucet/c109954?page=79","https://www.build.com/single-hole-sink-faucet/c109954?page=80","https://www.build.com/single-hole-sink-faucet/c109954?page=81","https://www.build.com/single-hole-sink-faucet/c109954?page=82","https://www.build.com/single-hole-sink-faucet/c109954?page=83","https://www.build.com/single-hole-sink-faucet/c109954?page=84","https://www.build.com/single-hole-sink-faucet/c109954?page=85","https://www.build.com/single-hole-sink-faucet/c109954?page=86","https://www.build.com/single-hole-sink-faucet/c109954?page=87","https://www.build.com/single-hole-sink-faucet/c109954?page=88","https://www.build.com/single-hole-sink-faucet/c109954?page=89","https://www.build.com/single-hole-sink-faucet/c109954?page=90","https://www.build.com/single-hole-sink-faucet/c109954?page=91","https://www.build.com/single-hole-sink-faucet/c109954?page=92","https://www.build.com/single-hole-sink-faucet/c109954?page=93","https://www.build.com/single-hole-sink-faucet/c109954?page=94","https://www.build.com/single-hole-sink-faucet/c109954?page=95","https://www.build.com/single-hole-sink-faucet/c109954?page=96","https://www.build.com/single-hole-sink-faucet/c109954?page=97","https://www.build.com/single-hole-sink-faucet/c109954?page=98","https://www.build.com/single-hole-sink-faucet/c109954?page=99","https://www.build.com/single-hole-sink-faucet/c109954?page=100",];
        $array2=["https://www.build.com/centerset-sink-faucets/c109943?page=1","https://www.build.com/centerset-sink-faucets/c109943?page=2","https://www.build.com/centerset-sink-faucets/c109943?page=3","https://www.build.com/centerset-sink-faucets/c109943?page=4","https://www.build.com/centerset-sink-faucets/c109943?page=5","https://www.build.com/centerset-sink-faucets/c109943?page=6","https://www.build.com/centerset-sink-faucets/c109943?page=7","https://www.build.com/centerset-sink-faucets/c109943?page=8","https://www.build.com/centerset-sink-faucets/c109943?page=9","https://www.build.com/centerset-sink-faucets/c109943?page=10","https://www.build.com/centerset-sink-faucets/c109943?page=11","https://www.build.com/centerset-sink-faucets/c109943?page=12","https://www.build.com/centerset-sink-faucets/c109943?page=13","https://www.build.com/centerset-sink-faucets/c109943?page=14","https://www.build.com/centerset-sink-faucets/c109943?page=15","https://www.build.com/centerset-sink-faucets/c109943?page=16","https://www.build.com/centerset-sink-faucets/c109943?page=17","https://www.build.com/centerset-sink-faucets/c109943?page=18","https://www.build.com/centerset-sink-faucets/c109943?page=19","https://www.build.com/centerset-sink-faucets/c109943?page=20","https://www.build.com/centerset-sink-faucets/c109943?page=21","https://www.build.com/centerset-sink-faucets/c109943?page=22","https://www.build.com/centerset-sink-faucets/c109943?page=23","https://www.build.com/centerset-sink-faucets/c109943?page=24","https://www.build.com/centerset-sink-faucets/c109943?page=25","https://www.build.com/centerset-sink-faucets/c109943?page=26","https://www.build.com/centerset-sink-faucets/c109943?page=27","https://www.build.com/centerset-sink-faucets/c109943?page=28","https://www.build.com/centerset-sink-faucets/c109943?page=29","https://www.build.com/centerset-sink-faucets/c109943?page=30","https://www.build.com/centerset-sink-faucets/c109943?page=31","https://www.build.com/centerset-sink-faucets/c109943?page=32","https://www.build.com/centerset-sink-faucets/c109943?page=33","https://www.build.com/centerset-sink-faucets/c109943?page=34","https://www.build.com/centerset-sink-faucets/c109943?page=35","https://www.build.com/centerset-sink-faucets/c109943?page=36","https://www.build.com/centerset-sink-faucets/c109943?page=37","https://www.build.com/centerset-sink-faucets/c109943?page=38","https://www.build.com/centerset-sink-faucets/c109943?page=39","https://www.build.com/centerset-sink-faucets/c109943?page=40","https://www.build.com/centerset-sink-faucets/c109943?page=41","https://www.build.com/centerset-sink-faucets/c109943?page=42","https://www.build.com/centerset-sink-faucets/c109943?page=43","https://www.build.com/centerset-sink-faucets/c109943?page=44","https://www.build.com/centerset-sink-faucets/c109943?page=45","https://www.build.com/centerset-sink-faucets/c109943?page=46","https://www.build.com/centerset-sink-faucets/c109943?page=47","https://www.build.com/centerset-sink-faucets/c109943?page=48","https://www.build.com/centerset-sink-faucets/c109943?page=49","https://www.build.com/centerset-sink-faucets/c109943?page=50","https://www.build.com/centerset-sink-faucets/c109943?page=51","https://www.build.com/centerset-sink-faucets/c109943?page=52","https://www.build.com/centerset-sink-faucets/c109943?page=53","https://www.build.com/centerset-sink-faucets/c109943?page=54","https://www.build.com/centerset-sink-faucets/c109943?page=55","https://www.build.com/centerset-sink-faucets/c109943?page=56","https://www.build.com/centerset-sink-faucets/c109943?page=57","https://www.build.com/centerset-sink-faucets/c109943?page=58","https://www.build.com/centerset-sink-faucets/c109943?page=59","https://www.build.com/centerset-sink-faucets/c109943?page=60","https://www.build.com/centerset-sink-faucets/c109943?page=61","https://www.build.com/centerset-sink-faucets/c109943?page=62","https://www.build.com/centerset-sink-faucets/c109943?page=63","https://www.build.com/centerset-sink-faucets/c109943?page=64","https://www.build.com/centerset-sink-faucets/c109943?page=65","https://www.build.com/centerset-sink-faucets/c109943?page=66","https://www.build.com/centerset-sink-faucets/c109943?page=67","https://www.build.com/centerset-sink-faucets/c109943?page=68","https://www.build.com/centerset-sink-faucets/c109943?page=69","https://www.build.com/centerset-sink-faucets/c109943?page=70","https://www.build.com/centerset-sink-faucets/c109943?page=71","https://www.build.com/centerset-sink-faucets/c109943?page=72","https://www.build.com/centerset-sink-faucets/c109943?page=73","https://www.build.com/centerset-sink-faucets/c109943?page=74","https://www.build.com/centerset-sink-faucets/c109943?page=75","https://www.build.com/centerset-sink-faucets/c109943?page=76","https://www.build.com/centerset-sink-faucets/c109943?page=77","https://www.build.com/centerset-sink-faucets/c109943?page=78","https://www.build.com/centerset-sink-faucets/c109943?page=79","https://www.build.com/centerset-sink-faucets/c109943?page=80","https://www.build.com/centerset-sink-faucets/c109943?page=81","https://www.build.com/centerset-sink-faucets/c109943?page=82","https://www.build.com/centerset-sink-faucets/c109943?page=83","https://www.build.com/centerset-sink-faucets/c109943?page=84","https://www.build.com/centerset-sink-faucets/c109943?page=85","https://www.build.com/centerset-sink-faucets/c109943?page=86","https://www.build.com/centerset-sink-faucets/c109943?page=87","https://www.build.com/centerset-sink-faucets/c109943?page=88","https://www.build.com/centerset-sink-faucets/c109943?page=89","https://www.build.com/centerset-sink-faucets/c109943?page=90","https://www.build.com/centerset-sink-faucets/c109943?page=91","https://www.build.com/centerset-sink-faucets/c109943?page=92","https://www.build.com/centerset-sink-faucets/c109943?page=93","https://www.build.com/centerset-sink-faucets/c109943?page=94","https://www.build.com/centerset-sink-faucets/c109943?page=95","https://www.build.com/centerset-sink-faucets/c109943?page=96","https://www.build.com/centerset-sink-faucets/c109943?page=97","https://www.build.com/centerset-sink-faucets/c109943?page=98","https://www.build.com/centerset-sink-faucets/c109943?page=99","https://www.build.com/centerset-sink-faucets/c109943?page=100"];
        $array3=["https://www.build.com/wall-mounted-sink-faucets/c109947?page=1","https://www.build.com/wall-mounted-sink-faucets/c109947?page=2","https://www.build.com/wall-mounted-sink-faucets/c109947?page=3","https://www.build.com/wall-mounted-sink-faucets/c109947?page=4","https://www.build.com/wall-mounted-sink-faucets/c109947?page=5","https://www.build.com/wall-mounted-sink-faucets/c109947?page=6","https://www.build.com/wall-mounted-sink-faucets/c109947?page=7","https://www.build.com/wall-mounted-sink-faucets/c109947?page=8","https://www.build.com/wall-mounted-sink-faucets/c109947?page=9","https://www.build.com/wall-mounted-sink-faucets/c109947?page=10","https://www.build.com/wall-mounted-sink-faucets/c109947?page=11","https://www.build.com/wall-mounted-sink-faucets/c109947?page=12","https://www.build.com/wall-mounted-sink-faucets/c109947?page=13","https://www.build.com/wall-mounted-sink-faucets/c109947?page=14","https://www.build.com/wall-mounted-sink-faucets/c109947?page=15","https://www.build.com/wall-mounted-sink-faucets/c109947?page=16","https://www.build.com/wall-mounted-sink-faucets/c109947?page=17","https://www.build.com/wall-mounted-sink-faucets/c109947?page=18","https://www.build.com/wall-mounted-sink-faucets/c109947?page=19","https://www.build.com/wall-mounted-sink-faucets/c109947?page=20","https://www.build.com/wall-mounted-sink-faucets/c109947?page=21","https://www.build.com/wall-mounted-sink-faucets/c109947?page=22","https://www.build.com/wall-mounted-sink-faucets/c109947?page=23","https://www.build.com/wall-mounted-sink-faucets/c109947?page=24","https://www.build.com/wall-mounted-sink-faucets/c109947?page=25","https://www.build.com/wall-mounted-sink-faucets/c109947?page=26","https://www.build.com/wall-mounted-sink-faucets/c109947?page=27","https://www.build.com/wall-mounted-sink-faucets/c109947?page=28","https://www.build.com/wall-mounted-sink-faucets/c109947?page=29","https://www.build.com/wall-mounted-sink-faucets/c109947?page=30","https://www.build.com/wall-mounted-sink-faucets/c109947?page=31","https://www.build.com/wall-mounted-sink-faucets/c109947?page=32","https://www.build.com/wall-mounted-sink-faucets/c109947?page=33","https://www.build.com/wall-mounted-sink-faucets/c109947?page=34","https://www.build.com/wall-mounted-sink-faucets/c109947?page=35","https://www.build.com/wall-mounted-sink-faucets/c109947?page=36","https://www.build.com/wall-mounted-sink-faucets/c109947?page=37","https://www.build.com/wall-mounted-sink-faucets/c109947?page=38","https://www.build.com/wall-mounted-sink-faucets/c109947?page=39","https://www.build.com/wall-mounted-sink-faucets/c109947?page=40","https://www.build.com/wall-mounted-sink-faucets/c109947?page=41","https://www.build.com/wall-mounted-sink-faucets/c109947?page=42","https://www.build.com/wall-mounted-sink-faucets/c109947?page=43","https://www.build.com/wall-mounted-sink-faucets/c109947?page=44","https://www.build.com/wall-mounted-sink-faucets/c109947?page=45","https://www.build.com/wall-mounted-sink-faucets/c109947?page=46","https://www.build.com/wall-mounted-sink-faucets/c109947?page=47","https://www.build.com/wall-mounted-sink-faucets/c109947?page=48","https://www.build.com/wall-mounted-sink-faucets/c109947?page=49","https://www.build.com/wall-mounted-sink-faucets/c109947?page=50",];
        $array4=["https://www.build.com/vessel-sink-faucets/c109946?page=1","https://www.build.com/vessel-sink-faucets/c109946?page=2","https://www.build.com/vessel-sink-faucets/c109946?page=3","https://www.build.com/vessel-sink-faucets/c109946?page=4","https://www.build.com/vessel-sink-faucets/c109946?page=5","https://www.build.com/vessel-sink-faucets/c109946?page=6","https://www.build.com/vessel-sink-faucets/c109946?page=7","https://www.build.com/vessel-sink-faucets/c109946?page=8","https://www.build.com/vessel-sink-faucets/c109946?page=9","https://www.build.com/vessel-sink-faucets/c109946?page=10","https://www.build.com/sensor-bathroom-faucets/c82045296?page=1","https://www.build.com/sensor-bathroom-faucets/c82045296?page=2","https://www.build.com/sensor-bathroom-faucets/c82045296?page=3","https://www.build.com/sensor-bathroom-faucets/c82045296?page=4","https://www.build.com/sensor-bathroom-faucets/c82045296?page=5","https://www.build.com/sensor-bathroom-faucets/c82045296?page=6","https://www.build.com/sensor-bathroom-faucets/c82045296?page=7","https://www.build.com/sensor-bathroom-faucets/c82045296?page=8","https://www.build.com/sensor-bathroom-faucets/c82045296?page=9","https://www.build.com/sensor-bathroom-faucets/c82045296?page=10","https://www.build.com/sensor-bathroom-faucets/c82045296?page=11","https://www.build.com/sensor-bathroom-faucets/c82045296?page=12","https://www.build.com/sensor-bathroom-faucets/c82045296?page=13","https://www.build.com/sensor-bathroom-faucets/c82045296?page=14","https://www.build.com/sensor-bathroom-faucets/c82045296?page=15","https://www.build.com/sensor-bathroom-faucets/c82045296?page=16","https://www.build.com/sensor-bathroom-faucets/c82045296?page=17","https://www.build.com/sensor-bathroom-faucets/c82045296?page=18","https://www.build.com/sensor-bathroom-faucets/c82045296?page=19","https://www.build.com/sensor-bathroom-faucets/c82045296?page=20","https://www.build.com/sensor-bathroom-faucets/c82045296?page=21","https://www.build.com/sensor-bathroom-faucets/c82045296?page=22","https://www.build.com/sensor-bathroom-faucets/c82045296?page=23","https://www.build.com/sensor-bathroom-faucets/c82045296?page=24","https://www.build.com/sensor-bathroom-faucets/c82045296?page=25",];
        $array5=["https://www.build.com/waterfall-faucets/c82041954?page=1","https://www.build.com/waterfall-faucets/c82041954?page=2","https://www.build.com/waterfall-faucets/c82041954?page=3","https://www.build.com/waterfall-faucets/c82041954?page=4","https://www.build.com/waterfall-faucets/c82041954?page=5",];
        $array6=["https://www.build.com/wide-spread-sink-faucets/c109950?page=1","https://www.build.com/wide-spread-sink-faucets/c109950?page=2","https://www.build.com/wide-spread-sink-faucets/c109950?page=3","https://www.build.com/wide-spread-sink-faucets/c109950?page=4","https://www.build.com/wide-spread-sink-faucets/c109950?page=5","https://www.build.com/wide-spread-sink-faucets/c109950?page=6","https://www.build.com/wide-spread-sink-faucets/c109950?page=7","https://www.build.com/wide-spread-sink-faucets/c109950?page=8","https://www.build.com/wide-spread-sink-faucets/c109950?page=9","https://www.build.com/wide-spread-sink-faucets/c109950?page=10","https://www.build.com/wide-spread-sink-faucets/c109950?page=11","https://www.build.com/wide-spread-sink-faucets/c109950?page=12","https://www.build.com/wide-spread-sink-faucets/c109950?page=13","https://www.build.com/wide-spread-sink-faucets/c109950?page=14","https://www.build.com/wide-spread-sink-faucets/c109950?page=15","https://www.build.com/wide-spread-sink-faucets/c109950?page=16","https://www.build.com/wide-spread-sink-faucets/c109950?page=17","https://www.build.com/wide-spread-sink-faucets/c109950?page=18","https://www.build.com/wide-spread-sink-faucets/c109950?page=19","https://www.build.com/wide-spread-sink-faucets/c109950?page=20","https://www.build.com/wide-spread-sink-faucets/c109950?page=21","https://www.build.com/wide-spread-sink-faucets/c109950?page=22","https://www.build.com/wide-spread-sink-faucets/c109950?page=23","https://www.build.com/wide-spread-sink-faucets/c109950?page=24","https://www.build.com/wide-spread-sink-faucets/c109950?page=25","https://www.build.com/wide-spread-sink-faucets/c109950?page=26","https://www.build.com/wide-spread-sink-faucets/c109950?page=27","https://www.build.com/wide-spread-sink-faucets/c109950?page=28","https://www.build.com/wide-spread-sink-faucets/c109950?page=29","https://www.build.com/wide-spread-sink-faucets/c109950?page=30","https://www.build.com/wide-spread-sink-faucets/c109950?page=31","https://www.build.com/wide-spread-sink-faucets/c109950?page=32","https://www.build.com/wide-spread-sink-faucets/c109950?page=33","https://www.build.com/wide-spread-sink-faucets/c109950?page=34","https://www.build.com/wide-spread-sink-faucets/c109950?page=35","https://www.build.com/wide-spread-sink-faucets/c109950?page=36","https://www.build.com/wide-spread-sink-faucets/c109950?page=37","https://www.build.com/wide-spread-sink-faucets/c109950?page=38","https://www.build.com/wide-spread-sink-faucets/c109950?page=39","https://www.build.com/wide-spread-sink-faucets/c109950?page=40","https://www.build.com/wide-spread-sink-faucets/c109950?page=41","https://www.build.com/wide-spread-sink-faucets/c109950?page=42","https://www.build.com/wide-spread-sink-faucets/c109950?page=43","https://www.build.com/wide-spread-sink-faucets/c109950?page=44","https://www.build.com/wide-spread-sink-faucets/c109950?page=45","https://www.build.com/wide-spread-sink-faucets/c109950?page=46","https://www.build.com/wide-spread-sink-faucets/c109950?page=47","https://www.build.com/wide-spread-sink-faucets/c109950?page=48","https://www.build.com/wide-spread-sink-faucets/c109950?page=49","https://www.build.com/wide-spread-sink-faucets/c109950?page=50","https://www.build.com/wide-spread-sink-faucets/c109950?page=51","https://www.build.com/wide-spread-sink-faucets/c109950?page=52","https://www.build.com/wide-spread-sink-faucets/c109950?page=53","https://www.build.com/wide-spread-sink-faucets/c109950?page=54","https://www.build.com/wide-spread-sink-faucets/c109950?page=55","https://www.build.com/wide-spread-sink-faucets/c109950?page=56","https://www.build.com/wide-spread-sink-faucets/c109950?page=57","https://www.build.com/wide-spread-sink-faucets/c109950?page=58","https://www.build.com/wide-spread-sink-faucets/c109950?page=59","https://www.build.com/wide-spread-sink-faucets/c109950?page=60",];

        $array7=["https://www.build.com/bathroom-sinks-undermount/c131351?page=1","https://www.build.com/bathroom-sinks-undermount/c131351?page=2","https://www.build.com/bathroom-sinks-undermount/c131351?page=3","https://www.build.com/bathroom-sinks-undermount/c131351?page=4","https://www.build.com/bathroom-sinks-undermount/c131351?page=5","https://www.build.com/bathroom-sinks-undermount/c131351?page=6","https://www.build.com/bathroom-sinks-undermount/c131351?page=7","https://www.build.com/bathroom-sinks-undermount/c131351?page=8","https://www.build.com/bathroom-sinks-undermount/c131351?page=9","https://www.build.com/bathroom-sinks-undermount/c131351?page=10","https://www.build.com/bathroom-sinks-undermount/c131351?page=11","https://www.build.com/bathroom-sinks-undermount/c131351?page=12","https://www.build.com/bathroom-sinks-undermount/c131351?page=13","https://www.build.com/bathroom-sinks-undermount/c131351?page=14","https://www.build.com/bathroom-sinks-undermount/c131351?page=15",];
        $array8=["https://www.build.com/bathroom-sinks-vessels/c131354?page=1","https://www.build.com/bathroom-sinks-vessels/c131354?page=2","https://www.build.com/bathroom-sinks-vessels/c131354?page=3","https://www.build.com/bathroom-sinks-vessels/c131354?page=4","https://www.build.com/bathroom-sinks-vessels/c131354?page=5","https://www.build.com/bathroom-sinks-vessels/c131354?page=6","https://www.build.com/bathroom-sinks-vessels/c131354?page=7","https://www.build.com/bathroom-sinks-vessels/c131354?page=8","https://www.build.com/bathroom-sinks-vessels/c131354?page=9","https://www.build.com/bathroom-sinks-vessels/c131354?page=10","https://www.build.com/bathroom-sinks-vessels/c131354?page=11","https://www.build.com/bathroom-sinks-vessels/c131354?page=12","https://www.build.com/bathroom-sinks-vessels/c131354?page=13","https://www.build.com/bathroom-sinks-vessels/c131354?page=14","https://www.build.com/bathroom-sinks-vessels/c131354?page=15","https://www.build.com/bathroom-sinks-vessels/c131354?page=16","https://www.build.com/bathroom-sinks-vessels/c131354?page=17","https://www.build.com/bathroom-sinks-vessels/c131354?page=18","https://www.build.com/bathroom-sinks-vessels/c131354?page=19","https://www.build.com/bathroom-sinks-vessels/c131354?page=20","https://www.build.com/bathroom-sinks-vessels/c131354?page=21","https://www.build.com/bathroom-sinks-vessels/c131354?page=22","https://www.build.com/bathroom-sinks-vessels/c131354?page=23","https://www.build.com/bathroom-sinks-vessels/c131354?page=24","https://www.build.com/bathroom-sinks-vessels/c131354?page=25","https://www.build.com/bathroom-sinks-vessels/c131354?page=26","https://www.build.com/bathroom-sinks-vessels/c131354?page=27","https://www.build.com/bathroom-sinks-vessels/c131354?page=28","https://www.build.com/bathroom-sinks-vessels/c131354?page=29","https://www.build.com/bathroom-sinks-vessels/c131354?page=30","https://www.build.com/bathroom-sinks-vessels/c131354?page=31","https://www.build.com/bathroom-sinks-vessels/c131354?page=32","https://www.build.com/bathroom-sinks-vessels/c131354?page=33","https://www.build.com/bathroom-sinks-vessels/c131354?page=34","https://www.build.com/bathroom-sinks-vessels/c131354?page=35","https://www.build.com/bathroom-sinks-vessels/c131354?page=36","https://www.build.com/bathroom-sinks-vessels/c131354?page=37","https://www.build.com/bathroom-sinks-vessels/c131354?page=38","https://www.build.com/bathroom-sinks-vessels/c131354?page=39","https://www.build.com/bathroom-sinks-vessels/c131354?page=40","https://www.build.com/bathroom-sinks-vessels/c131354?page=41","https://www.build.com/bathroom-sinks-vessels/c131354?page=42","https://www.build.com/bathroom-sinks-vessels/c131354?page=43","https://www.build.com/bathroom-sinks-vessels/c131354?page=44","https://www.build.com/bathroom-sinks-vessels/c131354?page=45",];
        $array9=["https://www.build.com/bathroom-sinks-drop-in/c131353?page=1","https://www.build.com/bathroom-sinks-drop-in/c131353?page=2","https://www.build.com/bathroom-sinks-drop-in/c131353?page=3","https://www.build.com/bathroom-sinks-drop-in/c131353?page=4","https://www.build.com/bathroom-sinks-drop-in/c131353?page=5","https://www.build.com/bathroom-sinks-drop-in/c131353?page=6","https://www.build.com/bathroom-sinks-drop-in/c131353?page=7","https://www.build.com/bathroom-sinks-drop-in/c131353?page=8","https://www.build.com/bathroom-sinks-drop-in/c131353?page=9","https://www.build.com/bathroom-sinks-drop-in/c131353?page=10","https://www.build.com/bathroom-sinks-drop-in/c131353?page=11","https://www.build.com/bathroom-sinks-drop-in/c131353?page=12","https://www.build.com/bathroom-sinks-drop-in/c131353?page=13","https://www.build.com/bathroom-sinks-drop-in/c131353?page=14","https://www.build.com/bathroom-sinks-drop-in/c131353?page=15","https://www.build.com/bathroom-sinks-drop-in/c131353?page=16","https://www.build.com/bathroom-sinks-drop-in/c131353?page=17","https://www.build.com/bathroom-sinks-drop-in/c131353?page=18",];
        $array10=["https://www.build.com/bathroom-sinks-wall-mount/c131356?page=1","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=2","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=3","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=4","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=5","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=6","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=7","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=8","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=9","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=10","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=11","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=12","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=13","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=14","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=15","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=16","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=17","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=18","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=19","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=20","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=21","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=22","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=23","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=24","https://www.build.com/bathroom-sinks-wall-mount/c131356?page=25",];
        $array11=["https://www.build.com/bathroom-sinks-pedestal/c131355?page=1","https://www.build.com/bathroom-sinks-pedestal/c131355?page=2","https://www.build.com/bathroom-sinks-pedestal/c131355?page=3","https://www.build.com/bathroom-sinks-pedestal/c131355?page=4","https://www.build.com/bathroom-sinks-pedestal/c131355?page=5","https://www.build.com/bathroom-sinks-pedestal/c131355?page=6","https://www.build.com/bathroom-sinks-pedestal/c131355?page=7","https://www.build.com/bathroom-sinks-pedestal/c131355?page=8","https://www.build.com/bathroom-sinks-pedestal/c131355?page=9","https://www.build.com/bathroom-sinks-pedestal/c131355?page=10",];
        $array12=["https://www.build.com/bathroom-sinks-console/c131357?page=1","https://www.build.com/bathroom-sinks-console/c131357?page=2","https://www.build.com/bathroom-sinks-console/c131357?page=3","https://www.build.com/bathroom-sinks-console/c131357?page=4","https://www.build.com/bathroom-sinks-console/c131357?page=5","https://www.build.com/bathroom-sinks-console/c131357?page=6","https://www.build.com/bathroom-sinks-console/c131357?page=7","https://www.build.com/bathroom-sinks-console/c131357?page=8","https://www.build.com/bathroom-sinks-console/c131357?page=9","https://www.build.com/bathroom-sinks-console/c131357?page=10",''];
        
        $array13=["https://www.build.com/toilet-one-piece/c132482?page=1","https://www.build.com/toilet-one-piece/c132482?page=2","https://www.build.com/toilet-one-piece/c132482?page=3","https://www.build.com/toilet-one-piece/c132482?page=4","https://www.build.com/toilet-one-piece/c132482?page=5","https://www.build.com/toilet-one-piece/c132482?page=6","https://www.build.com/toilet-one-piece/c132482?page=7","https://www.build.com/toilet-one-piece/c132482?page=8","https://www.build.com/toilet-one-piece/c132482?page=9","https://www.build.com/toilet-one-piece/c132482?page=10",];
        $array14=["https://www.build.com/toilet-two-piece/c132483?page=1","https://www.build.com/toilet-two-piece/c132483?page=2","https://www.build.com/toilet-two-piece/c132483?page=3","https://www.build.com/toilet-two-piece/c132483?page=4","https://www.build.com/toilet-two-piece/c132483?page=5","https://www.build.com/toilet-two-piece/c132483?page=6","https://www.build.com/toilet-two-piece/c132483?page=7","https://www.build.com/toilet-two-piece/c132483?page=8","https://www.build.com/toilet-two-piece/c132483?page=9","https://www.build.com/toilet-two-piece/c132483?page=10","https://www.build.com/toilet-two-piece/c132483?page=11","https://www.build.com/toilet-two-piece/c132483?page=12","https://www.build.com/toilet-two-piece/c132483?page=13","https://www.build.com/toilet-two-piece/c132483?page=14","https://www.build.com/toilet-two-piece/c132483?page=15","https://www.build.com/toilet-two-piece/c132483?page=16","https://www.build.com/toilet-two-piece/c132483?page=17","https://www.build.com/toilet-two-piece/c132483?page=18","https://www.build.com/toilet-two-piece/c132483?page=19","https://www.build.com/toilet-two-piece/c132483?page=20",];
        $array15=["https://www.build.com/toilet-elongated/c132486?page=1","https://www.build.com/toilet-elongated/c132486?page=2","https://www.build.com/toilet-elongated/c132486?page=3","https://www.build.com/toilet-elongated/c132486?page=4","https://www.build.com/toilet-elongated/c132486?page=5","https://www.build.com/toilet-elongated/c132486?page=6","https://www.build.com/toilet-elongated/c132486?page=7","https://www.build.com/toilet-elongated/c132486?page=8","https://www.build.com/toilet-elongated/c132486?page=9","https://www.build.com/toilet-elongated/c132486?page=10","https://www.build.com/toilet-elongated/c132486?page=11","https://www.build.com/toilet-elongated/c132486?page=12","https://www.build.com/toilet-elongated/c132486?page=13","https://www.build.com/toilet-elongated/c132486?page=14","https://www.build.com/toilet-elongated/c132486?page=15","https://www.build.com/toilet-elongated/c132486?page=16","https://www.build.com/toilet-elongated/c132486?page=17","https://www.build.com/toilet-elongated/c132486?page=18","https://www.build.com/toilet-elongated/c132486?page=19","https://www.build.com/toilet-elongated/c132486?page=20",];
        $array16=["https://www.build.com/toilet-round/c132487?page=1","https://www.build.com/toilet-round/c132487?page=2","https://www.build.com/toilet-round/c132487?page=3","https://www.build.com/toilet-round/c132487?page=4","https://www.build.com/toilet-round/c132487?page=5","https://www.build.com/toilet-chair-height/c82042986?page=1","https://www.build.com/toilet-chair-height/c82042986?page=2","https://www.build.com/toilet-chair-height/c82042986?page=3","https://www.build.com/toilet-chair-height/c82042986?page=4","https://www.build.com/toilet-chair-height/c82042986?page=5","https://www.build.com/toilet-chair-height/c82042986?page=6","https://www.build.com/toilet-chair-height/c82042986?page=7","https://www.build.com/toilet-chair-height/c82042986?page=8","https://www.build.com/toilet-chair-height/c82042986?page=9","https://www.build.com/toilet-chair-height/c82042986?page=10","https://www.build.com/toilet-chair-height/c82042986?page=11","https://www.build.com/toilet-chair-height/c82042986?page=12","https://www.build.com/toilet-chair-height/c82042986?page=13","https://www.build.com/toilet-chair-height/c82042986?page=14","https://www.build.com/toilet-chair-height/c82042986?page=15",];



        $this->sites = [
            'x1'=> ['source' => $array1, 'target' =>'build001.xms005.site'],
            'x2'=> ['source' => $array2, 'target' =>'hxflla'],
            'x3'=> ['source' => $array3, 'target' =>'hxflla'],
            'x4'=> ['source' => $array4, 'target' =>'hxflla'],
            'x5'=> ['source' => $array5, 'target' =>'hxflla'],
            'x6'=> ['source' => $array6, 'target' =>'hxflla'],
            'x7'=> ['source' => $array7, 'target' =>'hxflla'],
            'x8'=> ['source' => $array8, 'target' =>'hxflla'],
            'x9'=> ['source' => $array9, 'target' =>'hxflla'],
            'x10'=> ['source' => $array10, 'target' =>'hxflla'],
            'x11'=> ['source' => $array11, 'target' =>'hxflla'],
            'x12'=> ['source' => $array12, 'target' =>'hxflla'],
            'x13'=> ['source' => $array13, 'target' =>'hxflla'],
            'x14'=> ['source' => $array14, 'target' =>'hxflla'],
            'x15'=> ['source' => $array15, 'target' =>'hxflla'],
            'x16'=> ['source' => $array16, 'target' =>'hxflla'],
            // 'x17'=> ['source' => $array17, 'target' =>'hxflla'],

            // 'x2'=> ['source' => $array1, 'target' => 'hxflla'],
        ];

        $this->processSite = $this->sites[$input->getOption('site')];
        $this->cookieJar = new CookieJar;
        $this->guzzleHttpClient = new \GuzzleHttp\Client([
            'debug' => false,
            'verify' => false,
            'cookies' => $this->cookieJar,
            'proxy' => 'socks5h://127.0.0.1:7890',
            'base_uri' => 'https://www.build.com/',
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
            ],
        ]);
            // wordpress的密钥 
        $this->woocommerce = new Client(
            'http://' . $this->processSite['target'],
            //小皮密钥
            // 'ck_8d6fcda15402440c168d03686f5026e1b733aa18',
            // 'cs_8ffd4f558ad87fd7469f6eab856d647a15c3d4aa',
               //oa
            'ck_eda5cc1c661c038a8aa02276b5c0d708fd135618',
            'cs_3ef17880456fe03098e0fb0c347869de458fc8ba',
            [
                'timeout' => 600,
                'wp_api' => true, // Enable the WP REST API integration
                'version' => 'wc/v3', // WooCommerce WP REST API version
                'verify_ssl' => false,
            ]
        );
    }
    
    protected function execute(Input $input, Output $output)
    {
        $sources = is_array($this->processSite['source']) ? $this->processSite['source'] : [$this->processSite['source']];
        foreach ($sources as $source) {
            $this->processPage($source);
        }
    }


    protected function processPage($uri)
    {
        $url = $uri;
        $this->output->writeln($url);
        $response = $this->guzzleHttpClient->request('GET', $url);
        $contents = $response->getBody()->getContents();
        $crawler = new Crawler($contents);
       
            $this->processProductList($crawler);
    
            $nextNode = $crawler->filter('.pagination-item--next a');
        //    echo $nextNode->attr("href");exit;
            if ($nextNode->count()) {
                $this->processPage($nextNode->attr('href'));
            }

            // print_r($nextNode->attr("href"));exit;

//     
    }

    protected function processProductList(Crawler $crawler)
    {
        // 进入详情页爬取数据
        $crawler->filter('[class="flex  flex-wrap ml3-l"] a')->each(function (Crawler $node, $i)  {
            
            try {
                $this->discount = rand(65, 80) / 100;
                $this->crawlerProduct([
                    'name' => "[ {$i} ] " . $node->text(),
                    'url' => sprintf('%s','https://www.build.com'. $node->attr('href')),
                ]);
                
            } catch (\Exception $exception) {
                $this->output->error(sprintf('>>>>>>>>> [ Error ] %s %s : %s', $exception->getLine(), $exception->getFile(), $exception->getMessage()));
            }
            // print_r($node->attr('href'));exit;
        });
 
    }

// 用于图片上传失败
    public function toEncryptImage($images, $original = 'original') {
        foreach ($images as $key => $image) {
            $base64Str = base64_encode(openssl_encrypt($image['src'], 'DES-ECB', 'dot_custom'));
            $images[$key]['src'] = sprintf('%s/image/%s/%s/%s', 'http://media.reverbmall.com', $original, $base64Str, '.jpg');
        }
        return $images;
    }


    protected function crawlerProduct($item)
    {
        // print_r($item);exit;
        $this->output->info($item['name'] . ' >>> ' .$item['url']);
        $response = $this->guzzleHttpClient->request('GET', $item['url']);
        $contents = $response->getBody()->getContents();
        // print_r($contents);exit;
        $crawler = new Crawler($contents);
       
        
        $product['title'] = $crawler->filter('[class="ma0 fw6 lh-title di f5 f3-ns"]')->text();  //标题（必须）
        // print_r($product['title']);exit;

        

        // $product['price'] = $crawler->filter('[class="b lh-copy"]')->text();
        $product['price'] = str_replace(',','.',str_replace('€',"",str_replace('$',"",$crawler->filter('[class="b lh-copy"]')->text())));  //价格（必须）
        //价格（必须）
        // $product['brand']=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)['brand']['name'];
     
        // json_decode($crawler->filter('[type="application/ld+json"]')->last()->text(),true)["@graph"][1]['offers'][0]['price'];
        // print_r($product['price']);exit;

       //品牌（必须）

       $product['brand']=$crawler->filter('[class="ma0 fw6 lh-title di f5 f3-ns"] span')->eq(0)->text();
    //    $brand = explode('Brand: "',$crawler->filter('[id="viewed_product"]')->text());
    //    $brands = explode(',',$brand[1]);
    //    $product['brand']=str_replace('"','',$brands[0]);

        // print_r($product['brand']);exit;

        // $product['sku'] = $crawler->filter('.productView-info-value')->text();  //产品编号（必须）
        $product['sku'] = $crawler->filter('[class="f7 fw4 mt1 lh-title theme-grey-medium mt2 mt0-ns mb0"] .b')->text();  //产品编号（必须）
        // $sku=json_decode($crawler->filter('[type="application/ld+json"]')->text(),true)["productID"];



        // print_r($product['sku']);exit;

        $product['type'] = 'simple';

        // $breadcrumbs = array_filter($crawler->filter('')->each(function (Crawler $node) {  //分类类名（必须）
        //     return $node->text();
        // }));
        // print_r($breadcrumbs);exit;
        
        //分类类名
       
        $breadcrumbs = array_filter($crawler->filter('[class="flex flex-wrap list ma0 pa0 f6 items-center"] li a')->each(function (Crawler $node) {
            return $node->text();
         }));
        
        $product['breadcrumbs'] = array_slice($breadcrumbs,1);
        // print_r($product['breadcrumbs']);exit();
        $parent = 0;
        $parentCategory = '';
        foreach ($product['breadcrumbs'] as $breadcrumb) {
            $product['categories'][]['id'] = $parent = $this->createCategory($breadcrumb, $parentCategory, $parent);
        }
    //    print_r($product['breadcrumbs']);exit;

      
        // print_r($product['sku']);exit;
        
        $product['short_description']=''; //简短描述

        // print_r($product['short_description']);exit;
       
        // $product['description']=$crawler->filter('[class="lh-copy fxOgM"] p')->eq(3)->html().$crawler->filter('[class="lh-copy fxOgM"] ul')->html().$crawler->filter('[class="lh-copy fxOgM"] p')->eq(4)->html().$crawler->filter('[class="lh-copy fxOgM"] ul')->eq(1)->html().$crawler->filter('[class="lh-copy fxOgM"] p')->eq(5)->html().$crawler->filter('[class="lh-copy fxOgM"] ul')->eq(2)->html();//描述
        // $product['description']=$crawler->filter('[class="lh-copy fxOgM"]')->html();

        $int =$crawler->filter('[class="lh-copy fxOgM"]')->html(); //描述  
        $index = strpos($int,'<strong>Variations:</strong>');
        $product['description'] = substr($int,0,$index);

        // print_r($product['description']);exit;
        $product['attributes'] = [];
        // 属性
        if($crawler->filter('[class="dn db-ns"]')->eq(0)->count()) {
            $count = $crawler->filter('[class="dn db-ns"]')->eq(0)->filter('table tr')->count();
            // echo $count;exit;
            for($i=0;$i<$count;$i++){
                $product['attributes'][] = [
                    'name' => $crawler->filter('[class="pa2 flex justify-between items-center"]')->eq($i)->filter('[class="b lh-copy"]')->text(),
                    'options' =>$crawler->filter('[class="w-100 striped--grey-light"]')->eq($i)->filter('td')->eq(1)->text(),
                ];
            }
        }
        // print_r($product['attributes']);exit;

        // $product['attributes']=$crawler->filter('.product-description')->html();
        //   print_r($product['attributes']);exit; 
        //标签
        $product['tags'][]= $product['brand'];
        $product['tags'] = $this->createProductTag(array_unique(array_filter($product['tags'])));

       
        // print_r($product['tags']);exit;

      // 其他
        $product['keywords'] = [];
        $product['gender'] = '';//性别
        // print_r($product['gender']);exit;
        $product['color'] = '';
        $product['variations']=[];
        $product['subCategory']=' ';
       
        // $product['images'] =[];
        //图片（必须）
    //    $brand = explode('Brand: "',$crawler->filter('[id="viewed_product"]')->text());
    //    $brands = explode(',',$brand[1]);
    //    $product['brand']=str_replace('"','',$brands[0]);
        $arr= explode(',\"description\":\"Alternate View\"}":{"__typename":"Image","id":"',$crawler->filter('script')->eq(35)->text());
        print_r($arr);exit;
        $img=[];
        for ($i=1;$i<count($arr);$i++){
            if(strstr($arr[$i],'","imageType":"private","description":"Alternate View"},"Image:{\"id\":')){
                $arrs = explode('","imageType":"private","description":"Alternate View"},"Image:{\"id\":',$arr[$i]);
                    $img[]=$arrs[0];
                } 
            }

        // $arrs= explode('');
        foreach ($img as $image) {
            $product['images'][] = [
                'src' =>'https://s3.img-b.com/image/private/t_base,c_pad,f_auto,dpr_2,w_450,h_450/'. $image,
                'name' => $product['title'],
            ];
        }
        $product['images'] = $this->toEncryptImage($product['images']);  // 用于图片上传失败
    //    print_r($product['images']);exit;
    
    //   商品的尺寸或者颜色
    // $int =$crawler->filter('.prod-content')->html(); //描述  
        // $index = strpos($int,'<div class="blue-info d-md-none ">');
        // $product['description'] = substr($int,0,$index);
      
     $product['variations']=[];
    //  删除多余的字节，把他们设为空
     
       $del=[':']; 
    if($crawler->filter('[class="dn db-ns f5-ns lh-copy pt3-ns"]')->count()){$product['variations'][]= [
        'name'=>str_replace($del,'',$crawler->filter('[class="dn db-ns f5-ns lh-copy pt3-ns"] strong')->text()),
        'options' =>$crawler->filter('[class="dib outline-0 ma0 mr2 mt2"]')->filter('[class="relative"] img')->each(function (Crawler $node, $i) {
                return $node->attr('alt');
            })];  
            $product['type'] = 'variable';  
    }
    // print_r($product['variations']);exit;

            

        // print_r($product['variations']);exit;

        try {
            $this->createProduct($product);
        } catch (\Throwable $th) {

            var_dump($th->getMessage());
            var_dump($th->getLine());
            var_dump($th->getFile());
            //throw $th;
        }
        // print_r($product);
        // exit();
        echo "\r\n";
        // echo "1";

    }

    protected function generateName($product): string
    {
        $name = $product['title'];

        $brand = strpos(strtolower($name), strtolower($product['brand'])) === false ? $product['brand'] : '';

        $gender = isset($product['gender'])&& $product['gender']!=''?strpos(strtolower($name), strtolower($product['gender'])) === false ? $product['gender'] : '' : '';;

        $category = strpos(strtolower($name), strtolower($product['subCategory'])) === false ? $product['subCategory'] : '';

        return ucwords(preg_replace('/\s+/', ' ', sprintf('%s %s %s %s %s', $brand, $gender, $category, $name, $product['color'])));
    }
    

}