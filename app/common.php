<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * array unique_rand( int $min, int $max, int $num )
 * 生成一定数量的不重复随机数，指定的范围内整数的数量必须
 * 比要生成的随机数数量大
 * $min 和 $max: 指定随机数的范围
 * $num: 指定生成数量
 * @param $min
 * @param $max
 * @param $num
 * @return array
 */
function unique_rand($min, $max, $num)
{
    $count = 0;
    $return = array();
    while ($count < $num) {
        $return[] = mt_rand($min, $max);
        $return = array_flip(array_flip($return));
        $count = count($return);
    }
    //打乱数组，重新赋予数组新的下标
    shuffle($return);
    return $return;
}

function randomDate($beginTime, $endTime, $now = true)
{
    $begin = strtotime($beginTime);
    $end = $endTime ? strtotime($endTime) : mktime();

    $timestamp = rand($begin, $end);

    return $now ? date('Y-m-d H:i:s', $timestamp) : $timestamp;
}

function createPublishDateArray($time, $num = 5)
{
    $i = 0;
    $date_array = array();

    while ($i < $num) {
        $date = randomDate($time, $time . ' 23:59:59');
        $date_array[$i] = $date;
        $i++;
    }
    sort($date_array);

    return $date_array;
}

/**
 * PHP计算笛卡儿积
 * @param array $input 需要计算笛卡儿积的多维数组
 * @param array $next 辅助变量用于取数据（可不填）
 * @return array
 */
function cartesian_product($input, $next = array())
{
    // 取出第一个元素
    $first = array_shift($input);

    // 判断是否是第一次进行拼接
    if (count($next) == 0) {
        // 第一次拼接
        foreach ($first as $ikey => $ival) {
            $result[] = $ival;
        }
    } else {
        // 之后的拼接
        foreach ($next as $nkey => $nval) {
            foreach ($first as $fkey => $fval) {
                // 用冒号拼接起来
                $result[] = $nval . ':' . $fval;
            }
        }
    }

    // 递归进行拼接
    if (count($input) > 0) {
        $result = cartesian_product($input, $result);
    }

    // 返回最终的笛卡尔积
    return $result;
}
function slugify($text)
{
    $slugify = Cocur\Slugify\Slugify::create();
    return $slugify->slugify($text);
}
function strip_html_tags($tags, $str)
{
    $html = array();
    foreach ($tags as $tag) {
        $html[] = '/<' . $tag . '.*?>[\s|\S]*?<\/' . $tag . '>/';
        $html[] = '/<' . $tag . '.*?>/';
    }
    $data = preg_replace($html, '', $str);
    return $data;
}

function assignArrayByPath(&$arr, $path, $deep = 0, $separator = '.')
{
    $keys = explode($separator, $path);

    foreach ($keys as $i => $key) {
        if ($i <= $deep) continue;
        $arr = &$arr[$key];
    }
}
